#!/usr/bin/bash
BIN_BASE=/vol/ngscomparison/AssemblyComp/scripts/gatherScripts
wd=$1

function gatherBench() {
	local name=$1
	local both=$2
	local spades=$3

	elapsed=0
	size=0
	gsize=0
	if [ "${both}" != "SPADES" -a "q${both}" != "q" ]; then
	    statfileA=${name}.velveth_avg_stats.txt
	    statfileB=${name}.velvetg_avg_stats.txt
	    if [ -r $statfileA ]; then

		systemA=`grep System ${statfileA} | awk '{print $NF}'`
		systemB=`grep System ${statfileB} | awk '{print $NF}'`
		userA=`grep User ${statfileA} | awk '{print $NF}'`
		userB=`grep User ${statfileB} | awk '{print $NF}'`
		total=`echo "$systemA + $systemB + $userA + $userB" | bc`   


		elapsedA=`grep Elapsed ${statfileA} | awk '{print $NF}'`
		sizeA=`grep Maximum ${statfileA} | awk '{print $NF}'`
		gsizeA=`perl -ne '{printf "%.4f",($_/1024/1024)}' <<< $sizeA`
		elapsedB=`grep Elapsed ${statfileB} | awk '{print $NF}'`
		sizeB=`grep Maximum ${statfileB} | awk '{print $NF}'`
		gsizeB=`perl -ne '{printf "%.4f",($_/1024/1024)}' <<< $sizeB`
		if [ $sizeA > $sizeB ]; then
		    size=$sizeA
		    gsize=$gsizeA
		else
		    size=$sizeB
		    gsize=$gsizeB
		fi
		elapsed=`${BIN_BASE}/convertTimeSeconds.pl "${elapsedA}" "${elapsedB}"`
		ratio=`echo "$total/$elapsed" | bc -l`	
	    else
		statfile=${name}_avg_stats.txt
		system=`grep System ${statfile} | awk '{print $NF}'`
		user=`grep User ${statfile} | awk '{print $NF}'`
		total=`echo "$system + $user" | bc`   

		elapsed=`grep Elapsed ${statfile} | awk '{print $NF}'`
		elapsed=`${BIN_BASE}/convertTimeSeconds.pl "${elapsed}"`
		size=`grep Maximum ${statfile} | awk '{print $NF}'`
		gsize=`perl -ne '{printf "%.4f",($_/1024/1024)}' <<< $size`
		ratio=`echo "$total/$elapsed" | bc -l`	
	    fi 
	else
	    statfile=${name}_avg_stats.txt
		system=`grep System ${statfile} | awk '{print $NF}'`
		user=`grep User ${statfile} | awk '{print $NF}'`
		total=`echo "$system + $user" | bc`   
	    elapsed=`grep Elapsed ${statfile} | awk '{print $NF}'`
	    if [ "${both}" == "SPADES" ]; then
		elapsed=`${BIN_BASE}/convertTimeSeconds.pl  "${elapsed}" "0:0:0"`
	    else
		elapsed=`${BIN_BASE}/convertTimeSeconds.pl  "${elapsed}"`
	    fi
		ratio=`echo "$total/$elapsed" | bc -l`	
            size=`grep Maximum ${statfile} | awk '{print $NF}'`
	    gsize=`perl -ne '{printf "%.4f",($_/1024/1024)}' <<< $size`
	fi
	#gsize=`perl -e "{printf(\"%.4f\", ($gsize/4))}"`
	#size=`perl -e "{printf(\"%.4f\", ($size/4))}"`
	gsize=`perl -e "{printf(\"%.4f\", ($gsize))}"`
	size=`perl -e "{printf(\"%.4f\", ($size))}"`
	echo -e "${name}\t${elapsed}\t${gsize}\t${size}\t${ratio}"
}

echo -e "name\telapsed\tgsize\tsize"

for as in "CLC" "CELERA" "ABYSS_PE" "MIRA" "VELVET" "SOAP2" "SEQMAN" "NEWBLER" "SPADES"
do
    if   [ "${as}" == "CLC" ]; then
	res=(`find ${wd}/${as} -name "*result.fas" -exec realpath {} \;`)
	for re in ${res[@]}
	do
	    path=`dirname $re`
	    cd ${path}
	    re=`basename $re`
	    name=${re%.result.fas}
	    gatherBench $name
	    cd $wd
	done
    elif [ "${as}" == "CELERA" ]; then
	res=(`find ${wd}/${as} -name "*scf.fasta" -exec realpath {} \;`)
	for re in ${res[@]}
	do
	    path=`dirname $re`
	    cd ${path}/../
	    re=`basename $re`
	    name=${re%.scf.fasta}
	    gatherBench $name
	    cd ${wd}
	done
    elif [ "${as}" == "ABYSS_PE" ]; then
	res=(`find ${wd}/${as} -name "*final.fa"`)
	for re in ${res[@]}
	do
	    path=`dirname $re`
	    cd ${path}
	    re=`basename $re`
	    name=${re%.final.fa}
	    gatherBench $name
	    cd $wd
	done
    elif [ "${as}" == "MIRA" ]; then
	res=(`find ${wd}/${as} -name "*unpadded.fasta" -exec realpath {} \;`)
	for re in ${res[@]}
	do
	    path=`dirname $re`
	    cd ${path}/../../
	    re=`basename $re`
	    name=${re%_out.unpadded.fasta}
	    gatherBench $name
	    cd ${wd}
	done
    elif [ "${as}" == "VELVET" ]; then
	res=(`find ${wd}/${as} -name "*final.contigs.fa" -exec realpath {} \;`)
	for re in ${res[@]}
	do
	    path=`dirname ${re}`
	    cd ${path}/../
	    name=`basename $path`
	    gatherBench $name 1
	    cd $wd
	done
    elif [[ "${as}" == "SOAP"* ]]; then
	res1=(`find ${wd}/${as} -name "*final.contig.fa"`)
	res2=(`find ${wd}/${as} -name "*final.scaf.fa"`)
	res=(${res1[@]} ${res2[@]})
	for re in ${res[@]}
	do
	    path=`dirname $re`
	    cd ${path}
	    re=`basename $re`
	    name=${re%.final.contig.fa}
	    name=${name%.final.scaf.fa}
	    gatherBench $name 
	    cd $wd
	done
    elif [ "${as}" == "SEQMAN" ]; then
	res=(`find ${wd}/${as} -name "*.fas" -not -name "*.unasm.fas" -exec realpath {} \;`)
	for re in ${res[@]}
	do
	    path=`dirname $re`
	    cd ${path}
	    re=`basename $re`
	    name=${re%.fas}
	    gatherBench $name 
	    cd $wd
	done
    elif [ "${as}" == "NEWBLER" ]; then
	res=(`find ${wd}/${as} -name "454AllContigs.fna" -exec realpath {} \;`)
	for re in ${res[@]}
	do
	    path=`dirname ${re}`
	    cd ${path}/../
	    name=`basename $PWD`
	    gatherBench $name
	    cd $wd
	done
    elif [ "${as}" == "SPADES" ]; then
	cd ${as}
	subdirs=`ls`
	for subdir in ${subdirs[@]}
	do
	    cd $subdir
	    if [ "${subdir}" == "MIS_PE" ]; then
		res=(`find . -name "scaffolds.fasta" -exec realpath {} \;`)

	    else
		res=(`find . -name "contigs.fasta" -exec realpath {} \;`)
	    fi
	    for re in ${res[@]}
	    do
		if [[ ! $re =~ "/K" ]]; then
		    path=`dirname ${re}`
		    cd ${path}/../
		    name=`basename $PWD`
		    name=${name%.scaffolds.fasta}
		    name=${name%.contigs.fasta}
		    gatherBench $name "SPADES"
		    cd ${wd}/${as}/${subdir}
		fi
	    done
	    cd ${wd}/${as}
	done
	cd $wd
    fi
done


