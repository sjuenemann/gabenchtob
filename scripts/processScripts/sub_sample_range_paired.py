#!/usr/bin/env python

#under construction! -< add check for equality of fastq headers in both files

import random
import sys
import os
import pprint
import subprocess


def write_random_records(fname, fname2, minCov, maxCov, step, gsize, isize):
    
    # first sub-sample inpit fastq to maximum coverage
    orgName, pSuffix = os.path.splitext(fname)    
    orgName2, pSuffix = os.path.splitext(fname2)    
    downrate = 1
    records = (sum(1 for _ in open(fname)) / 4) + (sum(1 for _ in open(fname2)) /4 )
    if (maxCov != 0):
	N = 1-(1/ (float(float(isize)/int(gsize))/int(maxCov)))
	if (N > 0):
	    fname, fname2 = subSample(fname, fname2, N, orgName, orgName2, maxCov)
	else:
	    maxCov = int(int(isize)/int(gsize))
	    N = 1-(1/ (float(float(isize)/int(gsize))/maxCov))
	    print(N)
	    fname, fname2 = subSample(fname, fname2, N, orgName, orgName2, int(maxCov))
    else: 
	maxCov = int(int(isize)/int(gsize))

    if minCov < 1:
	minCov = 1
    
    subRanges = range(int(minCov), int(maxCov), int(step))
    subRanges.reverse()
    print("Subsampling at following coverage ranges:")
    print(subRanges)
    subRates = []
    for subRange in subRanges:
	rate = 1-(float(subRange) / int(maxCov))
	maxCov = subRange	
	subRates.append(rate)
    print(subRates)
    counter = 0
    for subRate in subRates:
	fname, fname2 = subSample(fname, fname2, subRate, orgName, orgName2, subRanges[counter])
	counter+=1
	


def subSample(fname, fname2, N, orgName, orgName2, origionalCov):
    records = (sum(1 for _ in open(fname)) / 4) + (sum(1 for _ in open(fname2)) / 4)
    percentage = int(round(records * N))	
    rand_records = sorted(random.sample(xrange(records), int(percentage)))
    
    pName = "%s.%sCoverage.fastq" % (orgName, origionalCov)
    pName2 = "%s.%sCoverage.fastq" % (orgName2, origionalCov)
    fha = open(fname)
    fh2 = open(fname2)
    suba = open(pName, "w")
    sub2 = open(pName2, "w")
    rec_no = -1
    for rr in rand_records:
        while rec_no < rr:
            rec_no += 1       
            for i in range(4):
		suba.write(fha.readline())
		sub2.write(fh2.readline())
        for i in range(4):
	    fha.readline()
	    fh2.readline()
	rec_no += 1 

    print >>sys.stderr, "wrote to %s, %s" % (suba.name, sub2.name)
    suba.close()
    sub2.close()
    fha.close()
    fh2.close()
    subprocess.Popen("/vol/ngscomparison/AssemblyComp/scripts/shuffleSequences_fastq.pl "+suba.name+" "+sub2.name+" interme."+str(origionalCov)+".fastq", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
    subprocess.Popen("/vol/ngscomparison/AssemblyComp/scripts/convert_PE_new2old_format interme."+str(origionalCov)+".fastq > "+orgName+"__"+orgName2+"."+str(origionalCov)+"coverage.fastq", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
    return pName, pName2

if __name__ == "__main__":
    #N = 0.5 if len(sys.argv) < 3 elise float(sys.argv[2])
    write_random_records(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7])
