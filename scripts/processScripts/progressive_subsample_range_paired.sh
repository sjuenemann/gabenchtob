#!/usr/bin/env bash

if [ $# -lt 2 ]; then
    echo -e "\n  Usage: $0 <first input.fastq> <second input.fastq> <ref.fas> <trim>"
    echo -e "  first input.fastq: path to forward reads fastq"
    echo -e "  second input.fastq: path to reverse reads fastq"
    echo -e "  ref.fas: path to reference genome fasta file"
    exit 1
fi
execDir=$(dirname `readlink -f $0`)

inputF=$1
inputB=$2
ref=$3
trim=$4

if [ ! -r ${inputF} ]; then
    echo -e "Cannot read seqeuncing file: ${inputF}."
    exit 1
fi
if [ ! -r ${inputB} ]; then
    echo -e "Cannot read seqeuncing file: ${inputB}."
    exit 1
fi

suffixF=`echo "${inputF}" |awk -F . '{print $NF}'`
prefixF=`basename ${inputF} .${suffixF}`
suffixB=`echo "${inputB}" |awk -F . '{print $NF}'`
prefixB=`basename ${inputB} .${suffixB}`

if [ ! -r ${ref} ]; then
    echo -e "Cannot read reference file ${ref}."
    exit 1
fi

echo -e "Computing genome size"
grepPlain=`which grep`
genome_sizeA=`${grepPlain} -hv \> ${ref} | wc -m`
genome_sizeB=`${grepPlain} -hv \> ${ref} | wc -l`
genome_size=`echo ${genome_sizeA} -${genome_sizeB} | bc`
echo -e "Genome size is: ${genome_size}(bp)\n"

echo -e "Computing input size"
in_sizeAF=`perl -lnwe '($.%4==2)&&print' ${inputF} | wc -m`
in_sizeAB=`perl -lnwe '($.%4==2)&&print' ${inputB} | wc -m`

in_sizeBF=` cat ${inputF} | wc -l `
in_sizeBF=$((${in_sizeBF}/4))
in_sizeBB=` cat ${inputB} | wc -l `
in_sizeBB=$((${in_sizeBB}/4))
in_size=`echo ${in_sizeAF} + ${in_sizeAB} - ${in_sizeBF} -${in_sizeBB} | bc`
echo -e "    Input size is: ${in_size}(bp)\n"

${execDir}/sub_sample_range_paired.py ${inputF} ${inputB} 1 100 5 ${genome_size} ${in_size}


