#!/usr/bin/env python

import random
import sys
import os

def write_random_records(fqa, fqb, N=0.5):
    records = sum(1 for _ in open(fqa)) / 4
    percentage = int(round(records * N))	
    rand_records = sorted(random.sample(xrange(records), int(percentage)))

    fha, fhb = open(fqa),  open(fqb)
    aName, aSuffix = os.path.splitext(fqa)
    bName, bSuffix = os.path.splitext(fqb)
    aName = "%s.%.3fpercent.fastq" % (aName, N)
    bName = "%s.%.3fpercent.fastq" % (bName, N)
    suba, subb = open(aName, "w"), open(bName, "w")
    rec_no = -1
    for rr in rand_records:
        while rec_no < rr:
            rec_no += 1       
            for i in range(4): fha.readline()
            for i in range(4): fhb.readline()
        for i in range(4):
            suba.write(fha.readline())
            subb.write(fhb.readline())
        rec_no += 1 

    print >>sys.stderr, "wrote to %s, %s" % (suba.name, subb.name)
    suba.close()
    subb.close()
    fha.close()
    fhb.close()

if __name__ == "__main__":
    N = 0.5 if len(sys.argv) < 4 else float(sys.argv[3])
    write_random_records(sys.argv[1], sys.argv[2], N)
