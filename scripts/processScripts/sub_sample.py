#!/usr/bin/env python

#under construction! -< add check for equality of fastq headers in both files

import random
import sys
import os
import pprint

def write_random_records(fname, N=0.5):
    """ get N random headers from a fastq file without reading the
    whole thing into memory"""
    records = sum(1 for _ in open(fname)) / 4
    percentage = int(round(records * N))	
    rand_records = sorted(random.sample(xrange(records), int(percentage)))
    
    pName, pSuffix = os.path.splitext(fname)    
    pName = "%s.%.3fpercent.fastq" % (pName, N)
    fha = open(fname)
    suba = open(pName, "w")
    rec_no = -1
    for rr in rand_records:
        while rec_no < rr:
            rec_no += 1       
            for i in range(4): fha.readline()
        for i in range(4):
	    #print "%d" % rec_no
            suba.write(fha.readline())
	rec_no += 1 

    print >>sys.stderr, "wrote to %s" % (suba.name)
    suba.close()
    fha.close()

if __name__ == "__main__":
    N = 0.5 if len(sys.argv) < 3 else float(sys.argv[2])
    write_random_records(sys.argv[1], N)
