#! /usr/bin/bash

execDir=$(dirname $0)
fq1=${1}
fq2=${2}
p1=`basename ${fq1}`
p1=`echo ${fq1} | sed 's/\.fastq$//'`
p2=`basename ${fq2}`
p2=`echo ${fq2} | sed 's/\.fastq$//'`
pb=${p1}__${p2}
range="0.025 0.050 0.075 0.100 0.125 0.150 0.175 0.200 0.250 0.300 0.350 0.400 0.450 0.500 0.550 0.600 0.650 0.700 0.750 0.800 0.850 0.900 0.925 0.950 0.975 0.990 1.000"

for step in $range
do
    ${execDir}/sub_sample_paired.py ${fq1} ${fq2} $step
    echo "${execDir}/shuffleSequences_fastq.pl ${p1}.${step}percent.fastq ${p2}.${step}percent.fastq  ${p1}.${p2}.${step}percent.fastq"
    ${execDir}/shuffleSequences_fastq.pl ${p1}.${step}percent.fastq ${p2}.${step}percent.fastq  ${p1}.${p2}.${step}percent.fastq
    echo "${execDir}/convert_PE_new2old_format ${p1}.${p2}.${step}percent.fastq > ${pb}.${step}percent.fastq"
    ${execDir}/convert_PE_new2old_format ${p1}.${p2}.${step}percent.fastq > ${pb}.${step}percent.fastq
    rm ${p1}.${p2}.${step}percent.fastq
    rm ${p1}.${step}percent.fastq 
    rm ${p2}.${step}percent.fastq 

done


