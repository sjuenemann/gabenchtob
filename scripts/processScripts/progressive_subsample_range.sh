#!/usr/bin/env bash

if [ $# -lt 2 ]; then
    echo -e "\n  Usage: $0 <input.fas> <ref.fas> <UP>"
    echo -e "  input.fas: path to reference genome fasta file"
    echo -e "  ref.fas: path to reference genome fasta file"
    echo -e "  UP: if set to true, only upper case letters are considered in the fastq"
    exit 1
fi
execDir=$(dirname `readlink -f $0`)

input=$1
ref=$2
up=$3

if [ ! -r ${input} ]; then
    echo -e "Cannot read seqeuncing file: ${input}."
    exit 1
fi

#input=`realpath ${input}`

if [ ! -r ${ref} ]; then
    echo -e "Cannot read reference file ${ref}."
    exit 1
fi

echo -e "Computing genome size"
grepPlain=`which grep`
genome_sizeA=`${grepPlain} -hv \> ${ref} | wc -m`
genome_sizeB=`${grepPlain} -hv \> ${ref} | wc -l`
genome_size=`echo ${genome_sizeA} -${genome_sizeB} | bc`
echo -e "Genome size is: ${genome_size}(bp)\n"

echo -e "Computing input size"
if [ -n $up ]; then
    in_sizeA=`perl -lnwe '($.%4==2)&&print' ${input} | sed 's/[a-z]//g' | wc -m`
else
    in_sizeA=`perl -lnwe '($.%4==2)&&print' ${input} | wc -m`
fi
in_sizeB=` cat ${input} | wc -l `
in_sizeB=$((${in_sizeB}/4))
in_size=`echo ${in_sizeA} -${in_sizeB} | bc`
echo -e "Input size is: ${in_size}(bp)\n"

${execDir}/sub_sample_range.py ${input} 1 100 5 ${genome_size} ${in_size}


