#!/bin/bash

# performs a fast paired end mapping of paired end reads and calculates the insert size distributions
# the path to all related scripts and executables
execDir=$(dirname `readlink -f $0`)
Rcmd="R --vanilla -q --slave"

if [ $# -lt 2 ]; then
    echo -e "\n  Usage: $0 <input1.faq> <input2.faq> <ref.fas> <outpath> "
    echo -e "  input1.fas: path to first sequencing output file (fastq)"
    echo -e "  input2.fas: path to second sequencing output file (fastq)"
    echo -e "  ref.fas: path to reference genome fasta file"
    echo -e "  outpath: path where the output is stored (default .) "
    exit 1
fi

input1=$1
input2=$2
ref=$3
outpath=$4
refStrain=`basename ${ref}`
refStrain=`echo -e ${refStrain} | sed 's/\.fasta$//'` #just in case

if [ ! -r ${input1} ]; then
    echo -e "Cannot read seqeuncing file: ${input1}."
    exit 1
fi
if [ ! -r ${input2} ]; then
    echo -e "Cannot read seqeuncing file: ${input2}."
    exit 1
fi

if [ ! -r ${ref} ]; then
    echo -e "Cannot read reference file ${ref}."
    exit 1
fi

if [ -z "${outpath}" ]; then
    outpath=.
    echo -e "[0] No output folder defined, using: ./"
fi

suffix=`echo "${input1}" |awk -F . '{print $NF}'`
prefix1=`basename ${input1} .${suffix}`
suffix=`echo "${input2}" |awk -F . '{print $NF}'`
prefix2=`basename ${input2} .${suffix}`
prefix=${prefix1}_${prefix2}
echo -e "[0] Using: ${prefix} for naming."

if [ ! -r ${outpath}/${prefix}_PEmapping ]; then
    echo -e "[0] Creating output base directory ${outpath}/${prefix}_PEmapping"
    mkdir -p ${outpath}/${prefix}_PEmapping
fi

outpath=${outpath}/${prefix}_PEmapping

#### sub sample
echo -e "[1] Start subsampling to 10% of inout data"
wd=$PWD
input1=`realpath ${input1}`
input2=`realpath ${input2}`
ref=`realpath ${ref}`
echo "    subsampling: $input1"
echo "    subsampling: $input2"
cd ${outpath}
ln -s ${input1} INPUT1
ln -s ${input2} INPUT2
cp ${ref} REFERENCE
downRate=0.1 # 10 percent sufficient for PE size estimate
${execDir}/sub_sample_paired.py INPUT1 INPUT2 ${downRate}
input1=INPUT1.0.100percent.fastq
input2=INPUT2.0.100percent.fastq
echo -e "    finished subsampling."

####start the mapping
echo -e "[2] Start mapping."
threads=8
bwa="bwa0_5_10"
$bwa index -a is REFERENCE 
$bwa aln -t $threads -f INPUT1.sai REFERENCE $input1
$bwa aln -t $threads -f INPUT2.sai REFERENCE $input2
$bwa sampe -f $prefix.sam REFERENCE INPUT1.sai INPUT2.sai $input1 $input2
samtools view -bS ${prefix}.sam > ${prefix}.bam
samtools sort ${prefix}.bam ${prefix}.sorted
samtools index ${prefix}.sorted.bam
echo -e "    finished mapping."

echo -e "[3] Reading paired end mapping data."
samtools view ${prefix}.sorted.bam | cut -f9 > ${prefix}.inserts
bases=`perl -lnwe '($.%4==2)&!chomp($_)&&print' INPUT1 | wc -m`
reads=`cat INPUT1 | wc -l `
reads=$((${reads}/4))
mean=$((($bases-$reads)/$reads))
echo -e "    done."

echo -e "[4] Caluclating insert size distributions"
cat ${execDir}/insert_summary.R | ${Rcmd} --args ${prefix}.inserts ${prefix} $mean
echo -e "    done."

echo -e "[5] Finishing results and cleaning directory"
cd $wd
mv ${outpath}/${prefix}_insertHist.png ${wd}/${prefix}_insertHist.png 
mv ${outpath}/${prefix}_insertHist.tsv ${wd}/${prefix}_insertHist.tsv 
mv ${outpath}/${prefix}_insertHist.txt ${wd}/${prefix}_insertHist.txt 
#rm -rf ${outpath}
echo -e "    Insertsize results were written to ${wd}/${prefix}_insertHist"
echo -e "\nFinished."

