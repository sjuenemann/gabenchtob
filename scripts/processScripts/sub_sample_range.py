#!/usr/bin/env python

#under construction! -< add check for equality of fastq headers in both files

import random
import sys
import os
import pprint

def write_random_records(fname, minCov, maxCov, step, gsize, isize):
    
    # first sub-sample inpit fastq to maximum coverage
    orgName, pSuffix = os.path.splitext(fname)    
    downrate = 1
    if (maxCov != 0):
	N = 1-(1/ (float(int(isize)/int(gsize))/int(maxCov)))
	if (N > 0):
	    fname = subSample(fname, N, orgName, maxCov)
	else: 
	    maxCov = int(int(isize)/int(gsize))
    else: 
	maxCov = int(int(isize)/int(gsize))

    if minCov < 1:
	minCov = 1
   
    subRanges = range(int(minCov), int(maxCov), int(step))
    subRanges.reverse()
    print("Subsampling at following coverage ranges:")
    print(subRanges)
    subRates = []
    for subRange in subRanges:
	rate = 1-(float(subRange) / int(maxCov))
	maxCov = subRange	
	subRates.append(rate)
    print(subRates)
    counter = 0
    for subRate in subRates:
	fname = subSample(fname, subRate, orgName, subRanges[counter])
	counter+=1
	


def subSample(fname, N, orgName, origionalCov):
    print(N)
    print(origionalCov)
    records = sum(1 for _ in open(fname)) / 4
    percentage = int(round(records * N))	
    rand_records = sorted(random.sample(xrange(records), int(percentage)))
    
    pName = "%s.%scoverage.fastq" % (orgName, origionalCov)
    fha = open(fname)
    suba = open(pName, "w")
    rec_no = -1
    for rr in rand_records:
        while rec_no < rr:
            rec_no += 1       
            for i in range(4):
		 #fha.readline()
		suba.write(fha.readline())
        for i in range(4):
            #suba.write(fha.readline())
	    fha.readline()
	rec_no += 1 

    print >>sys.stderr, "wrote to %s" % (suba.name)
    suba.close()
    fha.close()
    return pName

if __name__ == "__main__":
    #N = 0.5 if len(sys.argv) < 3 elise float(sys.argv[2])
    write_random_records(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
