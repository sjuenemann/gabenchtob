#!/usr/bin/env bash

execDir=$(dirname `readlink -f $0`)
fq1=${1}
range="0.025 0.050 0.075 0.100 0.125 0.150 0.175 0.200 0.250 0.300 0.350 0.400 0.450 0.500 0.550 0.600 0.650 0.700 0.750 0.800 0.850 0.900 0.925 0.950 0.975 0.990 1.000"

for step in $range
do
    $execDir/sub_sample.py ${fq1} $step
done


