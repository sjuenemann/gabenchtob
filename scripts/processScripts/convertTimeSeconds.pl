#!/usr/bin/env perl
use strict;
use warnings;

my $date1 = $ARGV[0];
my $date2 = $ARGV[1];
my $denom = $ARGV[2];


$date1 =~ s/^\s+|\s+$//g;

my $seconds;
if( $date1 =~ m/^([\d\.]+):([\d\.]+):([\d\.]+)$/ ) {
    $seconds = ($1*60*60)+($2*60)+$3;
}
elsif( $date1 =~ m/^([\d\.]+):([\d\.]+)$/ ) {
    $seconds = ($1*60)+$2;
}

if (defined $date2 && $date2 ne "") {
    $date2 =~ s/^\s+|\s+$//g;
    my $seconds2;
    if( $date2 =~ m/^([\d\.]+):([\d\.]+):([\d\.]+)$/ ) {
	$seconds2 = ($1*60*60)+($2*60)+$3;
    }
    elsif( $date2 =~ m/^([\d\.]+):([\d\.]+)$/ ) {
	$seconds2 = ($1*60)+$2;
    }
    $seconds+=$seconds2;
}
if (defined $denom && $denom > 0) {
    $seconds=$seconds/$denom;
}
my $final = $seconds;
#print $seconds."\n";
printf("%.4f", $final)
