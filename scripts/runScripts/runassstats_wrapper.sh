#!/usr/bin/env bash
inplist=${1}
outbase=${2}
#tmp=`mktemp -d -p /var/scratch/juene/`
tmp=`mktemp -d -p /vol/codine-tmp/juene/`

if [ ! -d ${tmp} ]; then
    exit 1;
fi

echo "Running on host: $HOSTNAME"

if [ ! -e ${inplist} ]; then
    echo "Input File ${inplist} not present. Aborting."
    exit 1;
fi
outbase=`readlink -f ${outbase}`
input_line=`sed -n ${SGE_TASK_ID}p ${inplist}`
input=`echo "${input_line}" | awk '{printf("%s",$1)}'`
ref=`echo "${input_line}" | awk '{printf("%s", $(NF))}'`
inbase=`basename ${input}`
echo "[0] Copying data to ${tmp}"
cp ${input} ${tmp}
cd ${tmp}
echo "[1] Running mauve: /vol/ngscomparison/AssemblyComp/scripts/mauvego ${tmp}/${inbase} ${ref} &>${tmp}/${inbase}_mauve.log" 
/vol/ngscomparison/AssemblyComp/scripts/mauvego ${tmp}/${inbase} ${ref} &>${tmp}/${inbase}_mauve.log 
if [ $? -ne 0 ]; then
    echo "Error: $!. Aborting."
    cd ${outbase}
    rm -rf $tmp
    exit 1
fi

mkdir -p ${outbase}/${inbase}_mauveResults
cp -R ${tmp}/* ${outbase}/${inbase}_mauveResults/
cd ${outbase}
rm -rf ${tmp}

exit 0
