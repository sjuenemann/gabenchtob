#!/usr/bin/env bash
inlist=${1}
workdir=${2}
outbase=${3}

jobN=`wc -l ${inlist} | awk '{print $1}'`

if [ ! -d ${outbase} ]; then
    echo "Creating output base directory"
    mkdir -p ${outbase}
fi

tmpout=`mktemp -p /vol/codine-tmp/`
cmd="/vol/ngscomparison/AssemblyComp/GABenchToB/scripts/runQuast_wrapper.sh"
cmdS="qsub -N quast -wd ${workdir} -o ${tmpout} -cwd -j y -l arch=lx24-amd64 -pe multislot 4 -t 1-${jobN}:1 ${cmd} ${inlist} ${outbase}" 
#cmdS="qsub -N mauve -wd ${workdir} -o ${tmpout} -cwd -j y -q *@@juene -t 1-${jobN}:1 ${cmd} ${inlist} ${outbase}" 
echo "Executing: ${cmdS}"
exec ${cmdS} 2>&1 || exit $!
