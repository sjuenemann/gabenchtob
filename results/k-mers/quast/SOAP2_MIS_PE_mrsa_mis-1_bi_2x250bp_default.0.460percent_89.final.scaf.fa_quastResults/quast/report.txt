All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_89.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_89.final.scaf
#Contigs (>= 0 bp)             2041                                                                          1983                                                                 
#Contigs (>= 1000 bp)          229                                                                           203                                                                  
Total length (>= 0 bp)         3060323                                                                       3062024                                                              
Total length (>= 1000 bp)      2740345                                                                       2753173                                                              
#Contigs                       330                                                                           281                                                                  
Largest contig                 63888                                                                         80236                                                                
Total length                   2779671                                                                       2782833                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         32.69                                                                         32.69                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            19590                                                                         21453                                                                
NG50                           19352                                                                         21049                                                                
N75                            10354                                                                         11638                                                                
NG75                           10211                                                                         11572                                                                
#misassemblies                 2                                                                             4                                                                    
#local misassemblies           19                                                                            55                                                                   
#unaligned contigs             10 + 2 part                                                                   10 + 2 part                                                          
Unaligned contigs length       9500                                                                          9615                                                                 
Genome fraction (%)            98.118                                                                        98.195                                                               
Duplication ratio              1.002                                                                         1.002                                                                
#N's per 100 kbp               3.42                                                                          64.54                                                                
#mismatches per 100 kbp        2.17                                                                          2.28                                                                 
#indels per 100 kbp            0.76                                                                          6.30                                                                 
#genes                         2482 + 211 part                                                               2489 + 202 part                                                      
#predicted genes (unique)      2850                                                                          2832                                                                 
#predicted genes (>= 0 bp)     2852                                                                          2834                                                                 
#predicted genes (>= 300 bp)   2348                                                                          2342                                                                 
#predicted genes (>= 1500 bp)  277                                                                           278                                                                  
#predicted genes (>= 3000 bp)  29                                                                            29                                                                   
Largest alignment              63887                                                                         80191                                                                
NA50                           19352                                                                         21049                                                                
NGA50                          19163                                                                         20645                                                                
NA75                           10257                                                                         11617                                                                
NGA75                          10194                                                                         11562                                                                
