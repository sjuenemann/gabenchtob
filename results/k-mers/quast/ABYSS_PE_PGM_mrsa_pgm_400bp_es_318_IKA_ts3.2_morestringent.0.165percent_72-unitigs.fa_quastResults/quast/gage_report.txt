All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_72-unitigs
GAGE_Contigs #                   464                                                                               
GAGE_Min contig                  201                                                                               
GAGE_Max contig                  52484                                                                             
GAGE_N50                         13367 COUNT: 66                                                                   
GAGE_Genome size                 2813862                                                                           
GAGE_Assembly size               2748985                                                                           
GAGE_Chaff bases                 0                                                                                 
GAGE_Missing reference bases     56760(2.02%)                                                                      
GAGE_Missing assembly bases      9(0.00%)                                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                                          
GAGE_Duplicated reference bases  466                                                                               
GAGE_Compressed reference bases  18862                                                                             
GAGE_Bad trim                    9                                                                                 
GAGE_Avg idy                     99.99                                                                             
GAGE_SNPs                        35                                                                                
GAGE_Indels < 5bp                103                                                                               
GAGE_Indels >= 5                 2                                                                                 
GAGE_Inversions                  0                                                                                 
GAGE_Relocation                  1                                                                                 
GAGE_Translocation               0                                                                                 
GAGE_Corrected contig #          465                                                                               
GAGE_Corrected assembly size     2748906                                                                           
GAGE_Min correct contig          201                                                                               
GAGE_Max correct contig          52484                                                                             
GAGE_Corrected N50               13368 COUNT: 66                                                                   
