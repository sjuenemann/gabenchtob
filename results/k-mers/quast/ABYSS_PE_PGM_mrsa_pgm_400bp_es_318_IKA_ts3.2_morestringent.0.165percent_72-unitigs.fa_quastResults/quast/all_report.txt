All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_72-unitigs
#Contigs                                   464                                                                               
#Contigs (>= 0 bp)                         4451                                                                              
#Contigs (>= 1000 bp)                      328                                                                               
Largest contig                             52484                                                                             
Total length                               2748985                                                                           
Total length (>= 0 bp)                     3172173                                                                           
Total length (>= 1000 bp)                  2687801                                                                           
Reference length                           2813862                                                                           
N50                                        13844                                                                             
NG50                                       13367                                                                             
N75                                        7081                                                                              
NG75                                       6791                                                                              
L50                                        63                                                                                
LG50                                       66                                                                                
L75                                        131                                                                               
LG75                                       138                                                                               
#local misassemblies                       2                                                                                 
#misassemblies                             1                                                                                 
#misassembled contigs                      1                                                                                 
Misassembled contigs length                11652                                                                             
Misassemblies inter-contig overlap         281                                                                               
#unaligned contigs                         0 + 0 part                                                                        
Unaligned contigs length                   0                                                                                 
#ambiguously mapped contigs                9                                                                                 
Extra bases in ambiguously mapped contigs  -2948                                                                             
#N's                                       0                                                                                 
#N's per 100 kbp                           0.00                                                                              
Genome fraction (%)                        97.424                                                                            
Duplication ratio                          1.002                                                                             
#genes                                     2399 + 257 part                                                                   
#predicted genes (unique)                  2886                                                                              
#predicted genes (>= 0 bp)                 2889                                                                              
#predicted genes (>= 300 bp)               2375                                                                              
#predicted genes (>= 1500 bp)              261                                                                               
#predicted genes (>= 3000 bp)              24                                                                                
#mismatches                                36                                                                                
#indels                                    111                                                                               
Indels length                              112                                                                               
#mismatches per 100 kbp                    1.31                                                                              
#indels per 100 kbp                        4.05                                                                              
GC (%)                                     32.59                                                                             
Reference GC (%)                           32.81                                                                             
Average %IDY                               99.487                                                                            
Largest alignment                          52484                                                                             
NA50                                       13844                                                                             
NGA50                                      13367                                                                             
NA75                                       7081                                                                              
NGA75                                      6540                                                                              
LA50                                       63                                                                                
LGA50                                      66                                                                                
LA75                                       131                                                                               
LGA75                                      139                                                                               
#Mis_misassemblies                         1                                                                                 
#Mis_relocations                           1                                                                                 
#Mis_translocations                        0                                                                                 
#Mis_inversions                            0                                                                                 
#Mis_misassembled contigs                  1                                                                                 
Mis_Misassembled contigs length            11652                                                                             
#Mis_local misassemblies                   2                                                                                 
#Mis_short indels (<= 5 bp)                111                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                 
#Una_fully unaligned contigs               0                                                                                 
Una_Fully unaligned length                 0                                                                                 
#Una_partially unaligned contigs           0                                                                                 
#Una_with misassembly                      0                                                                                 
#Una_both parts are significant            0                                                                                 
Una_Partially unaligned length             0                                                                                 
GAGE_Contigs #                             464                                                                               
GAGE_Min contig                            201                                                                               
GAGE_Max contig                            52484                                                                             
GAGE_N50                                   13367 COUNT: 66                                                                   
GAGE_Genome size                           2813862                                                                           
GAGE_Assembly size                         2748985                                                                           
GAGE_Chaff bases                           0                                                                                 
GAGE_Missing reference bases               56760(2.02%)                                                                      
GAGE_Missing assembly bases                9(0.00%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                          
GAGE_Duplicated reference bases            466                                                                               
GAGE_Compressed reference bases            18862                                                                             
GAGE_Bad trim                              9                                                                                 
GAGE_Avg idy                               99.99                                                                             
GAGE_SNPs                                  35                                                                                
GAGE_Indels < 5bp                          103                                                                               
GAGE_Indels >= 5                           2                                                                                 
GAGE_Inversions                            0                                                                                 
GAGE_Relocation                            1                                                                                 
GAGE_Translocation                         0                                                                                 
GAGE_Corrected contig #                    465                                                                               
GAGE_Corrected assembly size               2748906                                                                           
GAGE_Min correct contig                    201                                                                               
GAGE_Max correct contig                    52484                                                                             
GAGE_Corrected N50                         13368 COUNT: 66                                                                   
