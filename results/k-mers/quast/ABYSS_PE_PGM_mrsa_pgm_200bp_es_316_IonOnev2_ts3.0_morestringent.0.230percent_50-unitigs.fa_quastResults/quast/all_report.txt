All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_50-unitigs
#Contigs                                   418                                                                                    
#Contigs (>= 0 bp)                         3659                                                                                   
#Contigs (>= 1000 bp)                      304                                                                                    
Largest contig                             69773                                                                                  
Total length                               2742361                                                                                
Total length (>= 0 bp)                     2984787                                                                                
Total length (>= 1000 bp)                  2690889                                                                                
Reference length                           2813862                                                                                
N50                                        13201                                                                                  
NG50                                       13064                                                                                  
N75                                        7805                                                                                   
NG75                                       6883                                                                                   
L50                                        61                                                                                     
LG50                                       64                                                                                     
L75                                        128                                                                                    
LG75                                       135                                                                                    
#local misassemblies                       4                                                                                      
#misassemblies                             1                                                                                      
#misassembled contigs                      1                                                                                      
Misassembled contigs length                30162                                                                                  
Misassemblies inter-contig overlap         676                                                                                    
#unaligned contigs                         0 + 0 part                                                                             
Unaligned contigs length                   0                                                                                      
#ambiguously mapped contigs                10                                                                                     
Extra bases in ambiguously mapped contigs  -5014                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        97.105                                                                                 
Duplication ratio                          1.002                                                                                  
#genes                                     2449 + 184 part                                                                        
#predicted genes (unique)                  2850                                                                                   
#predicted genes (>= 0 bp)                 2851                                                                                   
#predicted genes (>= 300 bp)               2345                                                                                   
#predicted genes (>= 1500 bp)              276                                                                                    
#predicted genes (>= 3000 bp)              21                                                                                     
#mismatches                                94                                                                                     
#indels                                    286                                                                                    
Indels length                              294                                                                                    
#mismatches per 100 kbp                    3.44                                                                                   
#indels per 100 kbp                        10.47                                                                                  
GC (%)                                     32.58                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.900                                                                                 
Largest alignment                          69773                                                                                  
NA50                                       13185                                                                                  
NGA50                                      13064                                                                                  
NA75                                       7644                                                                                   
NGA75                                      6807                                                                                   
LA50                                       62                                                                                     
LGA50                                      64                                                                                     
LA75                                       129                                                                                    
LGA75                                      136                                                                                    
#Mis_misassemblies                         1                                                                                      
#Mis_relocations                           1                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  1                                                                                      
Mis_Misassembled contigs length            30162                                                                                  
#Mis_local misassemblies                   4                                                                                      
#Mis_short indels (<= 5 bp)                286                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             418                                                                                    
GAGE_Min contig                            207                                                                                    
GAGE_Max contig                            69773                                                                                  
GAGE_N50                                   13064 COUNT: 64                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2742361                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               61193(2.17%)                                                                           
GAGE_Missing assembly bases                12(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            213                                                                                    
GAGE_Compressed reference bases            17501                                                                                  
GAGE_Bad trim                              12                                                                                     
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  61                                                                                     
GAGE_Indels < 5bp                          282                                                                                    
GAGE_Indels >= 5                           4                                                                                      
GAGE_Inversions                            0                                                                                      
GAGE_Relocation                            2                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    422                                                                                    
GAGE_Corrected assembly size               2743096                                                                                
GAGE_Min correct contig                    207                                                                                    
GAGE_Max correct contig                    69783                                                                                  
GAGE_Corrected N50                         12602 COUNT: 65                                                                        
