All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_85.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_85.final.scaf
#Contigs                                   2063                                                                             716                                                                     
#Contigs (>= 0 bp)                         7910                                                                             6324                                                                    
#Contigs (>= 1000 bp)                      1215                                                                             352                                                                     
Largest contig                             27190                                                                            82857                                                                   
Total length                               5211461                                                                          5322347                                                                 
Total length (>= 0 bp)                     6128429                                                                          6204549                                                                 
Total length (>= 1000 bp)                  4829886                                                                          5184845                                                                 
Reference length                           5594470                                                                          5594470                                                                 
N50                                        4922                                                                             28082                                                                   
NG50                                       4560                                                                             26562                                                                   
N75                                        2618                                                                             15146                                                                   
NG75                                       2174                                                                             12826                                                                   
L50                                        316                                                                              59                                                                      
LG50                                       357                                                                              64                                                                      
L75                                        675                                                                              123                                                                     
LG75                                       795                                                                              138                                                                     
#local misassemblies                       7                                                                                292                                                                     
#misassemblies                             5                                                                                36                                                                      
#misassembled contigs                      5                                                                                29                                                                      
Misassembled contigs length                11691                                                                            251275                                                                  
Misassemblies inter-contig overlap         5                                                                                121                                                                     
#unaligned contigs                         22 + 3 part                                                                      29 + 33 part                                                            
Unaligned contigs length                   26172                                                                            97559                                                                   
#ambiguously mapped contigs                188                                                                              157                                                                     
Extra bases in ambiguously mapped contigs  -66239                                                                           -53605                                                                  
#N's                                       463                                                                              76583                                                                   
#N's per 100 kbp                           8.88                                                                             1438.90                                                                 
Genome fraction (%)                        91.388                                                                           91.349                                                                  
Duplication ratio                          1.001                                                                            1.012                                                                   
#genes                                     3687 + 1396 part                                                                 4408 + 620 part                                                         
#predicted genes (unique)                  6488                                                                             6148                                                                    
#predicted genes (>= 0 bp)                 6495                                                                             6156                                                                    
#predicted genes (>= 300 bp)               4814                                                                             4770                                                                    
#predicted genes (>= 1500 bp)              469                                                                              502                                                                     
#predicted genes (>= 3000 bp)              27                                                                               31                                                                      
#mismatches                                272                                                                              366                                                                     
#indels                                    151                                                                              5369                                                                    
Indels length                              5756                                                                             49278                                                                   
#mismatches per 100 kbp                    5.32                                                                             7.16                                                                    
#indels per 100 kbp                        2.95                                                                             105.06                                                                  
GC (%)                                     50.21                                                                            50.24                                                                   
Reference GC (%)                           50.48                                                                            50.48                                                                   
Average %IDY                               99.389                                                                           98.839                                                                  
Largest alignment                          27190                                                                            78890                                                                   
NA50                                       4914                                                                             27097                                                                   
NGA50                                      4559                                                                             26219                                                                   
NA75                                       2609                                                                             13488                                                                   
NGA75                                      2152                                                                             11461                                                                   
LA50                                       317                                                                              61                                                                      
LGA50                                      357                                                                              66                                                                      
LA75                                       677                                                                              128                                                                     
LGA75                                      799                                                                              144                                                                     
#Mis_misassemblies                         5                                                                                36                                                                      
#Mis_relocations                           5                                                                                36                                                                      
#Mis_translocations                        0                                                                                0                                                                       
#Mis_inversions                            0                                                                                0                                                                       
#Mis_misassembled contigs                  5                                                                                29                                                                      
Mis_Misassembled contigs length            11691                                                                            251275                                                                  
#Mis_local misassemblies                   7                                                                                292                                                                     
#Mis_short indels (<= 5 bp)                67                                                                               4360                                                                    
#Mis_long indels (> 5 bp)                  84                                                                               1009                                                                    
#Una_fully unaligned contigs               22                                                                               29                                                                      
Una_Fully unaligned length                 24532                                                                            46169                                                                   
#Una_partially unaligned contigs           3                                                                                33                                                                      
#Una_with misassembly                      0                                                                                1                                                                       
#Una_both parts are significant            0                                                                                20                                                                      
Una_Partially unaligned length             1640                                                                             51390                                                                   
GAGE_Contigs #                             2063                                                                             716                                                                     
GAGE_Min contig                            200                                                                              200                                                                     
GAGE_Max contig                            27190                                                                            82857                                                                   
GAGE_N50                                   4560 COUNT: 357                                                                  26562 COUNT: 64                                                         
GAGE_Genome size                           5594470                                                                          5594470                                                                 
GAGE_Assembly size                         5211461                                                                          5322347                                                                 
GAGE_Chaff bases                           0                                                                                0                                                                       
GAGE_Missing reference bases               254724(4.55%)                                                                    235198(4.20%)                                                           
GAGE_Missing assembly bases                8506(0.16%)                                                                      91844(1.73%)                                                            
GAGE_Missing assembly contigs              9(0.44%)                                                                         12(1.68%)                                                               
GAGE_Duplicated reference bases            14399                                                                            26903                                                                   
GAGE_Compressed reference bases            198285                                                                           209212                                                                  
GAGE_Bad trim                              106                                                                              7885                                                                    
GAGE_Avg idy                               99.99                                                                            99.96                                                                   
GAGE_SNPs                                  268                                                                              181                                                                     
GAGE_Indels < 5bp                          63                                                                               144                                                                     
GAGE_Indels >= 5                           99                                                                               1484                                                                    
GAGE_Inversions                            1                                                                                4                                                                       
GAGE_Relocation                            7                                                                                25                                                                      
GAGE_Translocation                         0                                                                                0                                                                       
GAGE_Corrected contig #                    2092                                                                             2026                                                                    
GAGE_Corrected assembly size               5185395                                                                          5173812                                                                 
GAGE_Min correct contig                    200                                                                              200                                                                     
GAGE_Max correct contig                    27190                                                                            27190                                                                   
GAGE_Corrected N50                         4345 COUNT: 374                                                                  4404 COUNT: 369                                                         
