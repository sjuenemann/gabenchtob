All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_77.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_77.final.scaf
#Contigs (>= 0 bp)             95007                                                                   94933                                                          
#Contigs (>= 1000 bp)          881                                                                     890                                                            
Total length (>= 0 bp)         25322910                                                                25327765                                                       
Total length (>= 1000 bp)      1231759                                                                 1244237                                                        
#Contigs                       86906                                                                   86873                                                          
Largest contig                 4275                                                                    4275                                                           
Total length                   24160110                                                                24171541                                                       
Reference length               4411532                                                                 4411532                                                        
GC (%)                         70.25                                                                   70.25                                                          
Reference GC (%)               65.61                                                                   65.61                                                          
N50                            251                                                                     251                                                            
NG50                           691                                                                     697                                                            
N75                            250                                                                     250                                                            
NG75                           446                                                                     453                                                            
#misassemblies                 17                                                                      17                                                             
#local misassemblies           2                                                                       23                                                             
#unaligned contigs             68267 + 4210 part                                                       68287 + 4211 part                                              
Unaligned contigs length       17471017                                                                17487927                                                       
Genome fraction (%)            96.922                                                                  96.826                                                         
Duplication ratio              1.558                                                                   1.559                                                          
#N's per 100 kbp               0.05                                                                    20.14                                                          
#mismatches per 100 kbp        397.71                                                                  399.15                                                         
#indels per 100 kbp            9.80                                                                    13.74                                                          
#genes                         839 + 3207 part                                                         839 + 3207 part                                                
#predicted genes (unique)      60302                                                                   60306                                                          
#predicted genes (>= 0 bp)     60466                                                                   60471                                                          
#predicted genes (>= 300 bp)   5479                                                                    5486                                                           
#predicted genes (>= 1500 bp)  39                                                                      39                                                             
#predicted genes (>= 3000 bp)  0                                                                       0                                                              
Largest alignment              4275                                                                    4275                                                           
NA50                           None                                                                    None                                                           
NGA50                          690                                                                     692                                                            
NA75                           None                                                                    None                                                           
NGA75                          444                                                                     446                                                            
