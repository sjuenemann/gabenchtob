All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_120-unitigs
#Contigs                                   2799                                                                                    
#Contigs (>= 0 bp)                         4208                                                                                    
#Contigs (>= 1000 bp)                      1013                                                                                    
Largest contig                             9054                                                                                    
Total length                               2909895                                                                                 
Total length (>= 0 bp)                     3104745                                                                                 
Total length (>= 1000 bp)                  2057137                                                                                 
Reference length                           2813862                                                                                 
N50                                        1645                                                                                    
NG50                                       1686                                                                                    
N75                                        888                                                                                     
NG75                                       950                                                                                     
L50                                        541                                                                                     
LG50                                       512                                                                                     
L75                                        1147                                                                                    
LG75                                       1068                                                                                    
#local misassemblies                       1                                                                                       
#misassemblies                             24                                                                                      
#misassembled contigs                      24                                                                                      
Misassembled contigs length                29918                                                                                   
Misassemblies inter-contig overlap         264                                                                                     
#unaligned contigs                         0 + 22 part                                                                             
Unaligned contigs length                   690                                                                                     
#ambiguously mapped contigs                78                                                                                      
Extra bases in ambiguously mapped contigs  -21459                                                                                  
#N's                                       0                                                                                       
#N's per 100 kbp                           0.00                                                                                    
Genome fraction (%)                        95.610                                                                                  
Duplication ratio                          1.073                                                                                   
#genes                                     1457 + 1213 part                                                                        
#predicted genes (unique)                  4578                                                                                    
#predicted genes (>= 0 bp)                 4597                                                                                    
#predicted genes (>= 300 bp)               2840                                                                                    
#predicted genes (>= 1500 bp)              129                                                                                     
#predicted genes (>= 3000 bp)              7                                                                                       
#mismatches                                98                                                                                      
#indels                                    1436                                                                                    
Indels length                              1462                                                                                    
#mismatches per 100 kbp                    3.64                                                                                    
#indels per 100 kbp                        53.38                                                                                   
GC (%)                                     32.69                                                                                   
Reference GC (%)                           32.81                                                                                   
Average %IDY                               99.604                                                                                  
Largest alignment                          9054                                                                                    
NA50                                       1644                                                                                    
NGA50                                      1686                                                                                    
NA75                                       884                                                                                     
NGA75                                      944                                                                                     
LA50                                       541                                                                                     
LGA50                                      512                                                                                     
LA75                                       1149                                                                                    
LGA75                                      1070                                                                                    
#Mis_misassemblies                         24                                                                                      
#Mis_relocations                           11                                                                                      
#Mis_translocations                        0                                                                                       
#Mis_inversions                            13                                                                                      
#Mis_misassembled contigs                  24                                                                                      
Mis_Misassembled contigs length            29918                                                                                   
#Mis_local misassemblies                   1                                                                                       
#Mis_short indels (<= 5 bp)                1436                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                       
#Una_fully unaligned contigs               0                                                                                       
Una_Fully unaligned length                 0                                                                                       
#Una_partially unaligned contigs           22                                                                                      
#Una_with misassembly                      0                                                                                       
#Una_both parts are significant            0                                                                                       
Una_Partially unaligned length             690                                                                                     
GAGE_Contigs #                             2799                                                                                    
GAGE_Min contig                            200                                                                                     
GAGE_Max contig                            9054                                                                                    
GAGE_N50                                   1686 COUNT: 512                                                                         
GAGE_Genome size                           2813862                                                                                 
GAGE_Assembly size                         2909895                                                                                 
GAGE_Chaff bases                           0                                                                                       
GAGE_Missing reference bases               82782(2.94%)                                                                            
GAGE_Missing assembly bases                1092(0.04%)                                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                                                
GAGE_Duplicated reference bases            81377                                                                                   
GAGE_Compressed reference bases            43796                                                                                   
GAGE_Bad trim                              1092                                                                                    
GAGE_Avg idy                               99.92                                                                                   
GAGE_SNPs                                  57                                                                                      
GAGE_Indels < 5bp                          875                                                                                     
GAGE_Indels >= 5                           2                                                                                       
GAGE_Inversions                            1                                                                                       
GAGE_Relocation                            0                                                                                       
GAGE_Translocation                         0                                                                                       
GAGE_Corrected contig #                    2462                                                                                    
GAGE_Corrected assembly size               2828487                                                                                 
GAGE_Min correct contig                    202                                                                                     
GAGE_Max correct contig                    9055                                                                                    
GAGE_Corrected N50                         1686 COUNT: 512                                                                         
