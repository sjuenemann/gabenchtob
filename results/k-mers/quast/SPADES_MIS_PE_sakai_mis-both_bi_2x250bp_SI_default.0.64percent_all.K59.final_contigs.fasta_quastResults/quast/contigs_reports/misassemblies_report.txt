All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_all.K59.final_contigs_broken  SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_all.K59.final_contigs
#Mis_misassemblies               3                                                                                            3                                                                                   
#Mis_relocations                 3                                                                                            3                                                                                   
#Mis_translocations              0                                                                                            0                                                                                   
#Mis_inversions                  0                                                                                            0                                                                                   
#Mis_misassembled contigs        3                                                                                            3                                                                                   
Mis_Misassembled contigs length  79359                                                                                        79359                                                                               
#Mis_local misassemblies         1                                                                                            1                                                                                   
#mismatches                      307                                                                                          307                                                                                 
#indels                          16                                                                                           16                                                                                  
#Mis_short indels (<= 5 bp)      16                                                                                           16                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                            0                                                                                   
Indels length                    17                                                                                           17                                                                                  
