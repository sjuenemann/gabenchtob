All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_75.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_75.final.scaf
#Contigs                                   630                                                                            365                                                                   
#Contigs (>= 0 bp)                         1677                                                                           1224                                                                  
#Contigs (>= 1000 bp)                      237                                                                            128                                                                   
Largest contig                             211551                                                                         374884                                                                
Total length                               5325625                                                                        5376642                                                               
Total length (>= 0 bp)                     5471037                                                                        5494969                                                               
Total length (>= 1000 bp)                  5180478                                                                        5284734                                                               
Reference length                           5594470                                                                        5594470                                                               
N50                                        56290                                                                          148245                                                                
NG50                                       54593                                                                          145355                                                                
N75                                        25191                                                                          78659                                                                 
NG75                                       21431                                                                          73999                                                                 
L50                                        27                                                                             10                                                                    
LG50                                       29                                                                             11                                                                    
L75                                        62                                                                             22                                                                    
LG75                                       71                                                                             24                                                                    
#local misassemblies                       1                                                                              110                                                                   
#misassemblies                             6                                                                              43                                                                    
#misassembled contigs                      6                                                                              28                                                                    
Misassembled contigs length                205550                                                                         1734889                                                               
Misassemblies inter-contig overlap         8                                                                              359                                                                   
#unaligned contigs                         1 + 0 part                                                                     11 + 11 part                                                          
Unaligned contigs length                   5461                                                                           27434                                                                 
#ambiguously mapped contigs                166                                                                            141                                                                   
Extra bases in ambiguously mapped contigs  -85474                                                                         -66510                                                                
#N's                                       53                                                                             23986                                                                 
#N's per 100 kbp                           1.00                                                                           446.11                                                                
Genome fraction (%)                        93.526                                                                         93.762                                                                
Duplication ratio                          1.000                                                                          1.007                                                                 
#genes                                     4782 + 274 part                                                                4905 + 195 part                                                       
#predicted genes (unique)                  5456                                                                           5412                                                                  
#predicted genes (>= 0 bp)                 5460                                                                           5415                                                                  
#predicted genes (>= 300 bp)               4411                                                                           4417                                                                  
#predicted genes (>= 1500 bp)              659                                                                            661                                                                   
#predicted genes (>= 3000 bp)              66                                                                             66                                                                    
#mismatches                                49                                                                             204                                                                   
#indels                                    24                                                                             1946                                                                  
Indels length                              86                                                                             5810                                                                  
#mismatches per 100 kbp                    0.94                                                                           3.89                                                                  
#indels per 100 kbp                        0.46                                                                           37.10                                                                 
GC (%)                                     50.26                                                                          50.28                                                                 
Reference GC (%)                           50.48                                                                          50.48                                                                 
Average %IDY                               99.171                                                                         99.018                                                                
Largest alignment                          211548                                                                         361994                                                                
NA50                                       56290                                                                          145355                                                                
NGA50                                      54593                                                                          144815                                                                
NA75                                       24738                                                                          76231                                                                 
NGA75                                      20692                                                                          73999                                                                 
LA50                                       27                                                                             12                                                                    
LGA50                                      29                                                                             13                                                                    
LA75                                       63                                                                             25                                                                    
LGA75                                      72                                                                             27                                                                    
#Mis_misassemblies                         6                                                                              43                                                                    
#Mis_relocations                           6                                                                              43                                                                    
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  6                                                                              28                                                                    
Mis_Misassembled contigs length            205550                                                                         1734889                                                               
#Mis_local misassemblies                   1                                                                              110                                                                   
#Mis_short indels (<= 5 bp)                20                                                                             1806                                                                  
#Mis_long indels (> 5 bp)                  4                                                                              140                                                                   
#Una_fully unaligned contigs               1                                                                              11                                                                    
Una_Fully unaligned length                 5461                                                                           17090                                                                 
#Una_partially unaligned contigs           0                                                                              11                                                                    
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            0                                                                              5                                                                     
Una_Partially unaligned length             0                                                                              10344                                                                 
GAGE_Contigs #                             630                                                                            365                                                                   
GAGE_Min contig                            200                                                                            200                                                                   
GAGE_Max contig                            211551                                                                         374884                                                                
GAGE_N50                                   54593 COUNT: 29                                                                145355 COUNT: 11                                                      
GAGE_Genome size                           5594470                                                                        5594470                                                               
GAGE_Assembly size                         5325625                                                                        5376642                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               99036(1.77%)                                                                   65099(1.16%)                                                          
GAGE_Missing assembly bases                5537(0.10%)                                                                    30839(0.57%)                                                          
GAGE_Missing assembly contigs              1(0.16%)                                                                       2(0.55%)                                                              
GAGE_Duplicated reference bases            4719                                                                           7395                                                                  
GAGE_Compressed reference bases            225994                                                                         249271                                                                
GAGE_Bad trim                              36                                                                             5557                                                                  
GAGE_Avg idy                               100.00                                                                         99.98                                                                 
GAGE_SNPs                                  58                                                                             67                                                                    
GAGE_Indels < 5bp                          17                                                                             79                                                                    
GAGE_Indels >= 5                           5                                                                              236                                                                   
GAGE_Inversions                            3                                                                              8                                                                     
GAGE_Relocation                            6                                                                              25                                                                    
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    620                                                                            599                                                                   
GAGE_Corrected assembly size               5314856                                                                        5315888                                                               
GAGE_Min correct contig                    200                                                                            200                                                                   
GAGE_Max correct contig                    211547                                                                         211547                                                                
GAGE_Corrected N50                         54593 COUNT: 29                                                                54995 COUNT: 28                                                       
