All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_75.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_75.final.scaf
GAGE_Contigs #                   630                                                                            365                                                                   
GAGE_Min contig                  200                                                                            200                                                                   
GAGE_Max contig                  211551                                                                         374884                                                                
GAGE_N50                         54593 COUNT: 29                                                                145355 COUNT: 11                                                      
GAGE_Genome size                 5594470                                                                        5594470                                                               
GAGE_Assembly size               5325625                                                                        5376642                                                               
GAGE_Chaff bases                 0                                                                              0                                                                     
GAGE_Missing reference bases     99036(1.77%)                                                                   65099(1.16%)                                                          
GAGE_Missing assembly bases      5537(0.10%)                                                                    30839(0.57%)                                                          
GAGE_Missing assembly contigs    1(0.16%)                                                                       2(0.55%)                                                              
GAGE_Duplicated reference bases  4719                                                                           7395                                                                  
GAGE_Compressed reference bases  225994                                                                         249271                                                                
GAGE_Bad trim                    36                                                                             5557                                                                  
GAGE_Avg idy                     100.00                                                                         99.98                                                                 
GAGE_SNPs                        58                                                                             67                                                                    
GAGE_Indels < 5bp                17                                                                             79                                                                    
GAGE_Indels >= 5                 5                                                                              236                                                                   
GAGE_Inversions                  3                                                                              8                                                                     
GAGE_Relocation                  6                                                                              25                                                                    
GAGE_Translocation               0                                                                              0                                                                     
GAGE_Corrected contig #          620                                                                            599                                                                   
GAGE_Corrected assembly size     5314856                                                                        5315888                                                               
GAGE_Min correct contig          200                                                                            200                                                                   
GAGE_Max correct contig          211547                                                                         211547                                                                
GAGE_Corrected N50               54593 COUNT: 29                                                                54995 COUNT: 28                                                       
