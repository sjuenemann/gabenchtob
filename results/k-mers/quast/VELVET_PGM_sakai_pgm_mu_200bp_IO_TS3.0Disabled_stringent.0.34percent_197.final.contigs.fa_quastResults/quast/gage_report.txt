All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_197.final.contigs
GAGE_Contigs #                   73                                                                                    
GAGE_Min contig                  591                                                                                   
GAGE_Max contig                  1139                                                                                  
GAGE_N50                         0 COUNT: 0                                                                            
GAGE_Genome size                 5594470                                                                               
GAGE_Assembly size               49284                                                                                 
GAGE_Chaff bases                 0                                                                                     
GAGE_Missing reference bases     5493782(98.20%)                                                                       
GAGE_Missing assembly bases      54(0.11%)                                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                              
GAGE_Duplicated reference bases  0                                                                                     
GAGE_Compressed reference bases  51899                                                                                 
GAGE_Bad trim                    54                                                                                    
GAGE_Avg idy                     99.94                                                                                 
GAGE_SNPs                        5                                                                                     
GAGE_Indels < 5bp                25                                                                                    
GAGE_Indels >= 5                 0                                                                                     
GAGE_Inversions                  0                                                                                     
GAGE_Relocation                  0                                                                                     
GAGE_Translocation               0                                                                                     
GAGE_Corrected contig #          73                                                                                    
GAGE_Corrected assembly size     49233                                                                                 
GAGE_Min correct contig          590                                                                                   
GAGE_Max correct contig          1140                                                                                  
GAGE_Corrected N50               0 COUNT: 0                                                                            
