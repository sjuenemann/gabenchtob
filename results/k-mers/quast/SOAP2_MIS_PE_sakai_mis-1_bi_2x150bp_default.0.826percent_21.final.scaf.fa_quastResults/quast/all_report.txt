All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_21.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_21.final.scaf
#Contigs                                   6435                                                                           2805                                                                  
#Contigs (>= 0 bp)                         48413                                                                          36875                                                                 
#Contigs (>= 1000 bp)                      68                                                                             1201                                                                  
Largest contig                             2842                                                                           20643                                                                 
Total length                               2193116                                                                        3994643                                                               
Total length (>= 0 bp)                     7560159                                                                        8334887                                                               
Total length (>= 1000 bp)                  87999                                                                          3304672                                                               
Reference length                           5594470                                                                        5594470                                                               
N50                                        342                                                                            2708                                                                  
NG50                                       None                                                                           1664                                                                  
N75                                        257                                                                            1371                                                                  
NG75                                       None                                                                           None                                                                  
L50                                        2145                                                                           428                                                                   
LG50                                       None                                                                           807                                                                   
L75                                        4008                                                                           938                                                                   
LG75                                       None                                                                           None                                                                  
#local misassemblies                       5                                                                              2257                                                                  
#misassemblies                             1                                                                              2                                                                     
#misassembled contigs                      1                                                                              2                                                                     
Misassembled contigs length                221                                                                            5886                                                                  
Misassemblies inter-contig overlap         0                                                                              0                                                                     
#unaligned contigs                         84 + 32 part                                                                   448 + 492 part                                                        
Unaligned contigs length                   25105                                                                          1234787                                                               
#ambiguously mapped contigs                6                                                                              5                                                                     
Extra bases in ambiguously mapped contigs  -1661                                                                          -1458                                                                 
#N's                                       626                                                                            775446                                                                
#N's per 100 kbp                           28.54                                                                          19412.15                                                              
Genome fraction (%)                        38.681                                                                         21.534                                                                
Duplication ratio                          1.001                                                                          2.290                                                                 
#genes                                     189 + 3523 part                                                                130 + 2533 part                                                       
#predicted genes (unique)                  6894                                                                           8908                                                                  
#predicted genes (>= 0 bp)                 6894                                                                           8908                                                                  
#predicted genes (>= 300 bp)               1861                                                                           2588                                                                  
#predicted genes (>= 1500 bp)              3                                                                              5                                                                     
#predicted genes (>= 3000 bp)              0                                                                              0                                                                     
#mismatches                                155                                                                            87                                                                    
#indels                                    230                                                                            2657                                                                  
Indels length                              562                                                                            5983                                                                  
#mismatches per 100 kbp                    7.16                                                                           7.34                                                                  
#indels per 100 kbp                        10.63                                                                          224.17                                                                
GC (%)                                     47.66                                                                          48.83                                                                 
Reference GC (%)                           50.48                                                                          50.48                                                                 
Average %IDY                               99.971                                                                         99.801                                                                
Largest alignment                          2842                                                                           7646                                                                  
NA50                                       340                                                                            None                                                                  
NGA50                                      None                                                                           None                                                                  
NA75                                       255                                                                            None                                                                  
NGA75                                      None                                                                           None                                                                  
LA50                                       2153                                                                           None                                                                  
LGA50                                      None                                                                           None                                                                  
LA75                                       4027                                                                           None                                                                  
LGA75                                      None                                                                           None                                                                  
#Mis_misassemblies                         1                                                                              2                                                                     
#Mis_relocations                           1                                                                              2                                                                     
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  1                                                                              2                                                                     
Mis_Misassembled contigs length            221                                                                            5886                                                                  
#Mis_local misassemblies                   5                                                                              2257                                                                  
#Mis_short indels (<= 5 bp)                193                                                                            2502                                                                  
#Mis_long indels (> 5 bp)                  37                                                                             155                                                                   
#Una_fully unaligned contigs               84                                                                             448                                                                   
Una_Fully unaligned length                 23382                                                                          425930                                                                
#Una_partially unaligned contigs           32                                                                             492                                                                   
#Una_with misassembly                      0                                                                              52                                                                    
#Una_both parts are significant            0                                                                              224                                                                   
Una_Partially unaligned length             1723                                                                           808857                                                                
GAGE_Contigs #                             6435                                                                           2805                                                                  
GAGE_Min contig                            200                                                                            200                                                                   
GAGE_Max contig                            2842                                                                           20643                                                                 
GAGE_N50                                   0 COUNT: 0                                                                     1664 COUNT: 807                                                       
GAGE_Genome size                           5594470                                                                        5594470                                                               
GAGE_Assembly size                         2193116                                                                        3994643                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               3422000(61.17%)                                                                2665384(47.64%)                                                       
GAGE_Missing assembly bases                20694(0.94%)                                                                   1065792(26.68%)                                                       
GAGE_Missing assembly contigs              73(1.13%)                                                                      88(3.14%)                                                             
GAGE_Duplicated reference bases            0                                                                              0                                                                     
GAGE_Compressed reference bases            1869                                                                           4173                                                                  
GAGE_Bad trim                              2002                                                                           131300                                                                
GAGE_Avg idy                               99.99                                                                          99.93                                                                 
GAGE_SNPs                                  151                                                                            233                                                                   
GAGE_Indels < 5bp                          108                                                                            265                                                                   
GAGE_Indels >= 5                           62                                                                             9589                                                                  
GAGE_Inversions                            0                                                                              1                                                                     
GAGE_Relocation                            1                                                                              5                                                                     
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    6357                                                                           5869                                                                  
GAGE_Corrected assembly size               2162127                                                                        2019304                                                               
GAGE_Min correct contig                    200                                                                            200                                                                   
GAGE_Max correct contig                    2842                                                                           2842                                                                  
GAGE_Corrected N50                         0 COUNT: 0                                                                     113 COUNT: 11021                                                      
