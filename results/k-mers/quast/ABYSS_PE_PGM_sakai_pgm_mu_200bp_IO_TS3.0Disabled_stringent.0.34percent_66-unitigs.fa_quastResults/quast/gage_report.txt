All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_66-unitigs
GAGE_Contigs #                   1261                                                                             
GAGE_Min contig                  200                                                                              
GAGE_Max contig                  43123                                                                            
GAGE_N50                         9255 COUNT: 180                                                                  
GAGE_Genome size                 5594470                                                                          
GAGE_Assembly size               5269255                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     192459(3.44%)                                                                    
GAGE_Missing assembly bases      56(0.00%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                         
GAGE_Duplicated reference bases  3987                                                                             
GAGE_Compressed reference bases  163168                                                                           
GAGE_Bad trim                    56                                                                               
GAGE_Avg idy                     99.97                                                                            
GAGE_SNPs                        115                                                                              
GAGE_Indels < 5bp                1353                                                                             
GAGE_Indels >= 5                 5                                                                                
GAGE_Inversions                  0                                                                                
GAGE_Relocation                  2                                                                                
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          1252                                                                             
GAGE_Corrected assembly size     5267063                                                                          
GAGE_Min correct contig          200                                                                              
GAGE_Max correct contig          43138                                                                            
GAGE_Corrected N50               9087 COUNT: 181                                                                  
