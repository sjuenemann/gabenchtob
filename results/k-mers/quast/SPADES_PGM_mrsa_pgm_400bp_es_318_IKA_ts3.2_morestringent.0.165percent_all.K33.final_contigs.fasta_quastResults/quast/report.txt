All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K33.final_contigs
#Contigs (>= 0 bp)             1263                                                                                       
#Contigs (>= 1000 bp)          133                                                                                        
Total length (>= 0 bp)         2814692                                                                                    
Total length (>= 1000 bp)      2696503                                                                                    
#Contigs                       299                                                                                        
Largest contig                 99544                                                                                      
Total length                   2756779                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.63                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            35949                                                                                      
NG50                           35389                                                                                      
N75                            19682                                                                                      
NG75                           19358                                                                                      
#misassemblies                 1                                                                                          
#local misassemblies           14                                                                                         
#unaligned contigs             60 + 1 part                                                                                
Unaligned contigs length       18122                                                                                      
Genome fraction (%)            96.980                                                                                     
Duplication ratio              1.001                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        8.54                                                                                       
#indels per 100 kbp            6.60                                                                                       
#genes                         2556 + 73 part                                                                             
#predicted genes (unique)      2764                                                                                       
#predicted genes (>= 0 bp)     2766                                                                                       
#predicted genes (>= 300 bp)   2292                                                                                       
#predicted genes (>= 1500 bp)  283                                                                                        
#predicted genes (>= 3000 bp)  25                                                                                         
Largest alignment              99544                                                                                      
NA50                           35949                                                                                      
NGA50                          35389                                                                                      
NA75                           19479                                                                                      
NGA75                          18523                                                                                      
