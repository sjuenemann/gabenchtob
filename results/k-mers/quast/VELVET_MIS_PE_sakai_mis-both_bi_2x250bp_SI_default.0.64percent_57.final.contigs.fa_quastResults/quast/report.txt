All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_57.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_57.final.contigs
#Contigs (>= 0 bp)             844                                                                                     619                                                                            
#Contigs (>= 1000 bp)          346                                                                                     174                                                                            
Total length (>= 0 bp)         5381745                                                                                 5396740                                                                        
Total length (>= 1000 bp)      5234458                                                                                 5272945                                                                        
#Contigs                       613                                                                                     395                                                                            
Largest contig                 131547                                                                                  314222                                                                         
Total length                   5347396                                                                                 5363300                                                                        
Reference length               5594470                                                                                 5594470                                                                        
GC (%)                         50.28                                                                                   50.28                                                                          
Reference GC (%)               50.48                                                                                   50.48                                                                          
N50                            28124                                                                                   80466                                                                          
NG50                           27233                                                                                   76609                                                                          
N75                            16060                                                                                   38929                                                                          
NG75                           13755                                                                                   35674                                                                          
#misassemblies                 7                                                                                       12                                                                             
#local misassemblies           14                                                                                      134                                                                            
#unaligned contigs             1 + 0 part                                                                              3 + 1 part                                                                     
Unaligned contigs length       5442                                                                                    8580                                                                           
Genome fraction (%)            93.912                                                                                  93.923                                                                         
Duplication ratio              1.001                                                                                   1.003                                                                          
#N's per 100 kbp               0.00                                                                                    279.59                                                                         
#mismatches per 100 kbp        9.78                                                                                    9.74                                                                           
#indels per 100 kbp            0.84                                                                                    10.98                                                                          
#genes                         4774 + 348 part                                                                         4850 + 266 part                                                                
#predicted genes (unique)      5494                                                                                    5450                                                                           
#predicted genes (>= 0 bp)     5496                                                                                    5452                                                                           
#predicted genes (>= 300 bp)   4522                                                                                    4511                                                                           
#predicted genes (>= 1500 bp)  646                                                                                     650                                                                            
#predicted genes (>= 3000 bp)  59                                                                                      61                                                                             
Largest alignment              131547                                                                                  307007                                                                         
NA50                           27510                                                                                   76326                                                                          
NGA50                          26937                                                                                   72016                                                                          
NA75                           15693                                                                                   36836                                                                          
NGA75                          13470                                                                                   35364                                                                          
