All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_99.final.contigs
#Contigs                                   31074                                                                      
#Contigs (>= 0 bp)                         34120                                                                      
#Contigs (>= 1000 bp)                      0                                                                          
Largest contig                             759                                                                        
Total length                               7423409                                                                    
Total length (>= 0 bp)                     8025626                                                                    
Total length (>= 1000 bp)                  0                                                                          
Reference length                           5594470                                                                    
N50                                        234                                                                        
NG50                                       247                                                                        
N75                                        214                                                                        
NG75                                       228                                                                        
L50                                        13743                                                                      
LG50                                       9937                                                                       
L75                                        22070                                                                      
LG75                                       15843                                                                      
#local misassemblies                       6                                                                          
#misassemblies                             986                                                                        
#misassembled contigs                      986                                                                        
Misassembled contigs length                243601                                                                     
Misassemblies inter-contig overlap         1652                                                                       
#unaligned contigs                         247 + 108 part                                                             
Unaligned contigs length                   71583                                                                      
#ambiguously mapped contigs                880                                                                        
Extra bases in ambiguously mapped contigs  -204417                                                                    
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        71.793                                                                     
Duplication ratio                          1.780                                                                      
#genes                                     131 + 4775 part                                                            
#predicted genes (unique)                  28160                                                                      
#predicted genes (>= 0 bp)                 28233                                                                      
#predicted genes (>= 300 bp)               374                                                                        
#predicted genes (>= 1500 bp)              0                                                                          
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                1726                                                                       
#indels                                    42018                                                                      
Indels length                              42696                                                                      
#mismatches per 100 kbp                    42.97                                                                      
#indels per 100 kbp                        1046.16                                                                    
GC (%)                                     49.82                                                                      
Reference GC (%)                           50.48                                                                      
Average %IDY                               98.773                                                                     
Largest alignment                          759                                                                        
NA50                                       230                                                                        
NGA50                                      244                                                                        
NA75                                       210                                                                        
NGA75                                      224                                                                        
LA50                                       13911                                                                      
LGA50                                      10043                                                                      
LA75                                       22381                                                                      
LGA75                                      16045                                                                      
#Mis_misassemblies                         986                                                                        
#Mis_relocations                           911                                                                        
#Mis_translocations                        74                                                                         
#Mis_inversions                            1                                                                          
#Mis_misassembled contigs                  986                                                                        
Mis_Misassembled contigs length            243601                                                                     
#Mis_local misassemblies                   6                                                                          
#Mis_short indels (<= 5 bp)                42016                                                                      
#Mis_long indels (> 5 bp)                  2                                                                          
#Una_fully unaligned contigs               247                                                                        
Una_Fully unaligned length                 59200                                                                      
#Una_partially unaligned contigs           108                                                                        
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             12383                                                                      
GAGE_Contigs #                             31074                                                                      
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            759                                                                        
GAGE_N50                                   247 COUNT: 9937                                                            
GAGE_Genome size                           5594470                                                                    
GAGE_Assembly size                         7423409                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               1281043(22.90%)                                                            
GAGE_Missing assembly bases                6046(0.08%)                                                                
GAGE_Missing assembly contigs              7(0.02%)                                                                   
GAGE_Duplicated reference bases            2293138                                                                    
GAGE_Compressed reference bases            305704                                                                     
GAGE_Bad trim                              4180                                                                       
GAGE_Avg idy                               98.92                                                                      
GAGE_SNPs                                  1438                                                                       
GAGE_Indels < 5bp                          36579                                                                      
GAGE_Indels >= 5                           8                                                                          
GAGE_Inversions                            139                                                                        
GAGE_Relocation                            58                                                                         
GAGE_Translocation                         19                                                                         
GAGE_Corrected contig #                    20270                                                                      
GAGE_Corrected assembly size               4880197                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    759                                                                        
GAGE_Corrected N50                         229 COUNT: 10473                                                           
