All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent.K33.final_contigs
#Contigs (>= 0 bp)             2040                                                                                  
#Contigs (>= 1000 bp)          348                                                                                   
Total length (>= 0 bp)         5342450                                                                               
Total length (>= 1000 bp)      5099735                                                                               
#Contigs                       692                                                                                   
Largest contig                 168571                                                                                
Total length                   5246356                                                                               
Reference length               5594470                                                                               
GC (%)                         50.23                                                                                 
Reference GC (%)               50.48                                                                                 
N50                            27906                                                                                 
NG50                           25137                                                                                 
N75                            13959                                                                                 
NG75                           10999                                                                                 
#misassemblies                 3                                                                                     
#local misassemblies           10                                                                                    
#unaligned contigs             0 + 1 part                                                                            
Unaligned contigs length       15                                                                                    
Genome fraction (%)            92.129                                                                                
Duplication ratio              1.001                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        3.98                                                                                  
#indels per 100 kbp            23.34                                                                                 
#genes                         4697 + 234 part                                                                       
#predicted genes (unique)      5820                                                                                  
#predicted genes (>= 0 bp)     5820                                                                                  
#predicted genes (>= 300 bp)   4660                                                                                  
#predicted genes (>= 1500 bp)  541                                                                                   
#predicted genes (>= 3000 bp)  45                                                                                    
Largest alignment              168571                                                                                
NA50                           27906                                                                                 
NGA50                          25137                                                                                 
NA75                           13470                                                                                 
NGA75                          10854                                                                                 
