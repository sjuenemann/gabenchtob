All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_73.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_73.final.scaf
#Una_fully unaligned contigs      1                                                                              6                                                                     
Una_Fully unaligned length        5459                                                                           8536                                                                  
#Una_partially unaligned contigs  0                                                                              13                                                                    
#Una_with misassembly             0                                                                              1                                                                     
#Una_both parts are significant   0                                                                              7                                                                     
Una_Partially unaligned length    0                                                                              13667                                                                 
#N's                              32                                                                             26298                                                                 
