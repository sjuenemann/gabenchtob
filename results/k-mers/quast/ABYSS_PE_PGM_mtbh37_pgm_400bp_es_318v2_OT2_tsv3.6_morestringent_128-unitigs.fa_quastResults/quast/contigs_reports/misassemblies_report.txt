All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_128-unitigs
#Mis_misassemblies               6                                                                          
#Mis_relocations                 6                                                                          
#Mis_translocations              0                                                                          
#Mis_inversions                  0                                                                          
#Mis_misassembled contigs        6                                                                          
Mis_Misassembled contigs length  3530                                                                       
#Mis_local misassemblies         6                                                                          
#mismatches                      130                                                                        
#indels                          1626                                                                       
#Mis_short indels (<= 5 bp)      1624                                                                       
#Mis_long indels (> 5 bp)        2                                                                          
Indels length                    1756                                                                       
