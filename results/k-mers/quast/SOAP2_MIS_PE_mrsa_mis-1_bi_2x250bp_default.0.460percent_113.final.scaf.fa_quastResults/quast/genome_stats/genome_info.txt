reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_113.final.scaf_broken  | 98.6361804523       | 1.05359313648     | 32          | 2544      | 164       | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_113.final.scaf  | 98.6494362552       | 1.05424555066     | 28          | 2544      | 161       | None      | None      |
