All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_79.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_79.final.scaf
GAGE_Contigs #                   62624                                                                            62605                                                                   
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  7181                                                                             7181                                                                    
GAGE_N50                         1151 COUNT: 1468                                                                 1153 COUNT: 1467                                                        
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               19495475                                                                         19504951                                                                
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     52934(0.95%)                                                                     52020(0.93%)                                                            
GAGE_Missing assembly bases      10588085(54.31%)                                                                 10591555(54.30%)                                                        
GAGE_Missing assembly contigs    41128(65.67%)                                                                    41127(65.69%)                                                           
GAGE_Duplicated reference bases  3142462                                                                          3146875                                                                 
GAGE_Compressed reference bases  299087                                                                           299089                                                                  
GAGE_Bad trim                    309253                                                                           310514                                                                  
GAGE_Avg idy                     99.84                                                                            99.85                                                                   
GAGE_SNPs                        4365                                                                             4299                                                                    
GAGE_Indels < 5bp                55                                                                               58                                                                      
GAGE_Indels >= 5                 2                                                                                34                                                                      
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  2                                                                                2                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          7536                                                                             7525                                                                    
GAGE_Corrected assembly size     5736138                                                                          5733435                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          7181                                                                             7181                                                                    
GAGE_Corrected N50               1149 COUNT: 1472                                                                 1149 COUNT: 1472                                                        
