All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_93.final.contigs
GAGE_Contigs #                   25562                                                                      
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  674                                                                        
GAGE_N50                         235 COUNT: 10485                                                           
GAGE_Genome size                 5594470                                                                    
GAGE_Assembly size               6036554                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     1687527(30.16%)                                                            
GAGE_Missing assembly bases      7607(0.13%)                                                                
GAGE_Missing assembly contigs    9(0.04%)                                                                   
GAGE_Duplicated reference bases  1517972                                                                    
GAGE_Compressed reference bases  301742                                                                     
GAGE_Bad trim                    5271                                                                       
GAGE_Avg idy                     98.82                                                                      
GAGE_SNPs                        1309                                                                       
GAGE_Indels < 5bp                35880                                                                      
GAGE_Indels >= 5                 8                                                                          
GAGE_Inversions                  139                                                                        
GAGE_Relocation                  65                                                                         
GAGE_Translocation               16                                                                         
GAGE_Corrected contig #          17897                                                                      
GAGE_Corrected assembly size     4253450                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          674                                                                        
GAGE_Corrected N50               221 COUNT: 10948                                                           
