All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_56-unitigs
#Contigs (>= 0 bp)             7678                                                                   
#Contigs (>= 1000 bp)          713                                                                    
Total length (>= 0 bp)         5842896                                                                
Total length (>= 1000 bp)      5011674                                                                
#Contigs                       1219                                                                   
Largest contig                 48419                                                                  
Total length                   5234177                                                                
Reference length               5594470                                                                
GC (%)                         50.24                                                                  
Reference GC (%)               50.48                                                                  
N50                            10828                                                                  
NG50                           9837                                                                   
N75                            5602                                                                   
NG75                           4529                                                                   
#misassemblies                 2                                                                      
#local misassemblies           4                                                                      
#unaligned contigs             0 + 0 part                                                             
Unaligned contigs length       0                                                                      
Genome fraction (%)            92.415                                                                 
Duplication ratio              1.001                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        1.04                                                                   
#indels per 100 kbp            4.37                                                                   
#genes                         4271 + 726 part                                                        
#predicted genes (unique)      5810                                                                   
#predicted genes (>= 0 bp)     5812                                                                   
#predicted genes (>= 300 bp)   4569                                                                   
#predicted genes (>= 1500 bp)  574                                                                    
#predicted genes (>= 3000 bp)  50                                                                     
Largest alignment              48419                                                                  
NA50                           10828                                                                  
NGA50                          9837                                                                   
NA75                           5602                                                                   
NGA75                          4473                                                                   
