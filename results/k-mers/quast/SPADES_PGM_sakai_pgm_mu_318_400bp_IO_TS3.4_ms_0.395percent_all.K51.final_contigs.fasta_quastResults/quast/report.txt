All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K51.final_contigs
#Contigs (>= 0 bp)             1367                                                                            
#Contigs (>= 1000 bp)          202                                                                             
Total length (>= 0 bp)         5387698                                                                         
Total length (>= 1000 bp)      5149667                                                                         
#Contigs                       653                                                                             
Largest contig                 268203                                                                          
Total length                   5320323                                                                         
Reference length               5594470                                                                         
GC (%)                         50.28                                                                           
Reference GC (%)               50.48                                                                           
N50                            91164                                                                           
NG50                           84626                                                                           
N75                            33393                                                                           
NG75                           28313                                                                           
#misassemblies                 3                                                                               
#local misassemblies           21                                                                              
#unaligned contigs             68 + 0 part                                                                     
Unaligned contigs length       17357                                                                           
Genome fraction (%)            93.118                                                                          
Duplication ratio              1.002                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        4.86                                                                            
#indels per 100 kbp            6.68                                                                            
#genes                         4781 + 231 part                                                                 
#predicted genes (unique)      5518                                                                            
#predicted genes (>= 0 bp)     5520                                                                            
#predicted genes (>= 300 bp)   4455                                                                            
#predicted genes (>= 1500 bp)  636                                                                             
#predicted genes (>= 3000 bp)  62                                                                              
Largest alignment              268203                                                                          
NA50                           91164                                                                           
NGA50                          84626                                                                           
NA75                           33393                                                                           
NGA75                          28313                                                                           
