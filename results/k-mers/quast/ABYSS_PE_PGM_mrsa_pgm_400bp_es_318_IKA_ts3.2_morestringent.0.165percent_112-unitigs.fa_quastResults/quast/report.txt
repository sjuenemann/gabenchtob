All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_112-unitigs
#Contigs (>= 0 bp)             2968                                                                               
#Contigs (>= 1000 bp)          340                                                                                
Total length (>= 0 bp)         3172358                                                                            
Total length (>= 1000 bp)      2691617                                                                            
#Contigs                       1345                                                                               
Largest contig                 51543                                                                              
Total length                   2951669                                                                            
Reference length               2813862                                                                            
GC (%)                         32.79                                                                              
Reference GC (%)               32.81                                                                              
N50                            12008                                                                              
NG50                           12361                                                                              
N75                            5519                                                                               
NG75                           6366                                                                               
#misassemblies                 10                                                                                 
#local misassemblies           1                                                                                  
#unaligned contigs             0 + 12 part                                                                        
Unaligned contigs length       393                                                                                
Genome fraction (%)            98.310                                                                             
Duplication ratio              1.055                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        1.27                                                                               
#indels per 100 kbp            6.87                                                                               
#genes                         2437 + 263 part                                                                    
#predicted genes (unique)      3638                                                                               
#predicted genes (>= 0 bp)     3670                                                                               
#predicted genes (>= 300 bp)   2388                                                                               
#predicted genes (>= 1500 bp)  254                                                                                
#predicted genes (>= 3000 bp)  25                                                                                 
Largest alignment              51543                                                                              
NA50                           11575                                                                              
NGA50                          12361                                                                              
NA75                           5519                                                                               
NGA75                          6341                                                                               
