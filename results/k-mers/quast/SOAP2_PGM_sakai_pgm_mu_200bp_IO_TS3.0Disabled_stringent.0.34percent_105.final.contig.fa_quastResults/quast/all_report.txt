All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_105.final.contig
#Contigs                                   23983                                                                               
#Contigs (>= 0 bp)                         31393                                                                               
#Contigs (>= 1000 bp)                      1409                                                                                
Largest contig                             4458                                                                                
Total length                               8543521                                                                             
Total length (>= 0 bp)                     9628172                                                                             
Total length (>= 1000 bp)                  2104886                                                                             
Reference length                           5594470                                                                             
N50                                        409                                                                                 
NG50                                       790                                                                                 
N75                                        209                                                                                 
NG75                                       426                                                                                 
L50                                        4778                                                                                
LG50                                       2190                                                                                
L75                                        13681                                                                               
LG75                                       4596                                                                                
#local misassemblies                       0                                                                                   
#misassemblies                             18                                                                                  
#misassembled contigs                      18                                                                                  
Misassembled contigs length                5975                                                                                
Misassemblies inter-contig overlap         213                                                                                 
#unaligned contigs                         0 + 4 part                                                                          
Unaligned contigs length                   154                                                                                 
#ambiguously mapped contigs                870                                                                                 
Extra bases in ambiguously mapped contigs  -200697                                                                             
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        93.921                                                                              
Duplication ratio                          1.588                                                                               
#genes                                     1377 + 3779 part                                                                    
#predicted genes (unique)                  25720                                                                               
#predicted genes (>= 0 bp)                 26124                                                                               
#predicted genes (>= 300 bp)               5926                                                                                
#predicted genes (>= 1500 bp)              69                                                                                  
#predicted genes (>= 3000 bp)              0                                                                                   
#mismatches                                91                                                                                  
#indels                                    2429                                                                                
Indels length                              2508                                                                                
#mismatches per 100 kbp                    1.73                                                                                
#indels per 100 kbp                        46.23                                                                               
GC (%)                                     50.69                                                                               
Reference GC (%)                           50.48                                                                               
Average %IDY                               99.613                                                                              
Largest alignment                          4458                                                                                
NA50                                       405                                                                                 
NGA50                                      788                                                                                 
NA75                                       209                                                                                 
NGA75                                      423                                                                                 
LA50                                       4790                                                                                
LGA50                                      2192                                                                                
LA75                                       13780                                                                               
LGA75                                      4607                                                                                
#Mis_misassemblies                         18                                                                                  
#Mis_relocations                           5                                                                                   
#Mis_translocations                        0                                                                                   
#Mis_inversions                            13                                                                                  
#Mis_misassembled contigs                  18                                                                                  
Mis_Misassembled contigs length            5975                                                                                
#Mis_local misassemblies                   0                                                                                   
#Mis_short indels (<= 5 bp)                2429                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                   
#Una_fully unaligned contigs               0                                                                                   
Una_Fully unaligned length                 0                                                                                   
#Una_partially unaligned contigs           4                                                                                   
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             154                                                                                 
GAGE_Contigs #                             23983                                                                               
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            4458                                                                                
GAGE_N50                                   790 COUNT: 2190                                                                     
GAGE_Genome size                           5594470                                                                             
GAGE_Assembly size                         8543521                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               50475(0.90%)                                                                        
GAGE_Missing assembly bases                414(0.00%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                            
GAGE_Duplicated reference bases            2623957                                                                             
GAGE_Compressed reference bases            279031                                                                              
GAGE_Bad trim                              414                                                                                 
GAGE_Avg idy                               99.96                                                                               
GAGE_SNPs                                  48                                                                                  
GAGE_Indels < 5bp                          1352                                                                                
GAGE_Indels >= 5                           0                                                                                   
GAGE_Inversions                            1                                                                                   
GAGE_Relocation                            3                                                                                   
GAGE_Translocation                         0                                                                                   
GAGE_Corrected contig #                    11474                                                                               
GAGE_Corrected assembly size               5918988                                                                             
GAGE_Min correct contig                    200                                                                                 
GAGE_Max correct contig                    4458                                                                                
GAGE_Corrected N50                         789 COUNT: 2191                                                                     
