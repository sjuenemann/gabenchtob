reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  VELVET_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default_95.final.contigs_broken  | 96.9660199677       | 1.00569209482     | 224         | 3817      | 227       | None      | None      |
  VELVET_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default_95.final.contigs  | 96.9821821535       | 1.00948134817     | 221         | 3837      | 208       | None      | None      |
