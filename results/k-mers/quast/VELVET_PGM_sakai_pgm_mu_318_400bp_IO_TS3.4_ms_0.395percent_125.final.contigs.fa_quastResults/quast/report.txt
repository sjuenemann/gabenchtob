All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_125.final.contigs
#Contigs (>= 0 bp)             21350                                                                       
#Contigs (>= 1000 bp)          0                                                                           
Total length (>= 0 bp)         6035763                                                                     
Total length (>= 1000 bp)      0                                                                           
#Contigs                       21350                                                                       
Largest contig                 881                                                                         
Total length                   6035763                                                                     
Reference length               5594470                                                                     
GC (%)                         50.24                                                                       
Reference GC (%)               50.48                                                                       
N50                            273                                                                         
NG50                           277                                                                         
N75                            252                                                                         
NG75                           256                                                                         
#misassemblies                 706                                                                         
#local misassemblies           4                                                                           
#unaligned contigs             8 + 14 part                                                                 
Unaligned contigs length       3923                                                                        
Genome fraction (%)            62.617                                                                      
Duplication ratio              1.662                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        69.45                                                                       
#indels per 100 kbp            736.75                                                                      
#genes                         150 + 4529 part                                                             
#predicted genes (unique)      22316                                                                       
#predicted genes (>= 0 bp)     22397                                                                       
#predicted genes (>= 300 bp)   604                                                                         
#predicted genes (>= 1500 bp)  0                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              881                                                                         
NA50                           269                                                                         
NGA50                          273                                                                         
NA75                           249                                                                         
NGA75                          252                                                                         
