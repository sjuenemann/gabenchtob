All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_119.final.contig
#Mis_misassemblies               11                                                                                        
#Mis_relocations                 8                                                                                         
#Mis_translocations              0                                                                                         
#Mis_inversions                  3                                                                                         
#Mis_misassembled contigs        11                                                                                        
Mis_Misassembled contigs length  7852                                                                                      
#Mis_local misassemblies         0                                                                                         
#mismatches                      81                                                                                        
#indels                          1465                                                                                      
#Mis_short indels (<= 5 bp)      1465                                                                                      
#Mis_long indels (> 5 bp)        0                                                                                         
Indels length                    1491                                                                                      
