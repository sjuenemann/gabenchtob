All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_96-unitigs
#Contigs                                   460                                                                               
#Contigs (>= 0 bp)                         3396                                                                              
#Contigs (>= 1000 bp)                      314                                                                               
Largest contig                             51541                                                                             
Total length                               2759743                                                                           
Total length (>= 0 bp)                     3171002                                                                           
Total length (>= 1000 bp)                  2690048                                                                           
Reference length                           2813862                                                                           
N50                                        13517                                                                             
NG50                                       13406                                                                             
N75                                        7634                                                                              
NG75                                       6977                                                                              
L50                                        62                                                                                
LG50                                       64                                                                                
L75                                        129                                                                               
LG75                                       135                                                                               
#local misassemblies                       2                                                                                 
#misassemblies                             1                                                                                 
#misassembled contigs                      1                                                                                 
Misassembled contigs length                18671                                                                             
Misassemblies inter-contig overlap         395                                                                               
#unaligned contigs                         0 + 0 part                                                                        
Unaligned contigs length                   0                                                                                 
#ambiguously mapped contigs                11                                                                                
Extra bases in ambiguously mapped contigs  -3824                                                                             
#N's                                       0                                                                                 
#N's per 100 kbp                           0.00                                                                              
Genome fraction (%)                        97.698                                                                            
Duplication ratio                          1.003                                                                             
#genes                                     2443 + 242 part                                                                   
#predicted genes (unique)                  2893                                                                              
#predicted genes (>= 0 bp)                 2894                                                                              
#predicted genes (>= 300 bp)               2383                                                                              
#predicted genes (>= 1500 bp)              258                                                                               
#predicted genes (>= 3000 bp)              24                                                                                
#mismatches                                36                                                                                
#indels                                    140                                                                               
Indels length                              143                                                                               
#mismatches per 100 kbp                    1.31                                                                              
#indels per 100 kbp                        5.09                                                                              
GC (%)                                     32.60                                                                             
Reference GC (%)                           32.81                                                                             
Average %IDY                               99.140                                                                            
Largest alignment                          51541                                                                             
NA50                                       13406                                                                             
NGA50                                      13394                                                                             
NA75                                       7576                                                                              
NGA75                                      6948                                                                              
LA50                                       63                                                                                
LGA50                                      65                                                                                
LA75                                       130                                                                               
LGA75                                      136                                                                               
#Mis_misassemblies                         1                                                                                 
#Mis_relocations                           1                                                                                 
#Mis_translocations                        0                                                                                 
#Mis_inversions                            0                                                                                 
#Mis_misassembled contigs                  1                                                                                 
Mis_Misassembled contigs length            18671                                                                             
#Mis_local misassemblies                   2                                                                                 
#Mis_short indels (<= 5 bp)                140                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                 
#Una_fully unaligned contigs               0                                                                                 
Una_Fully unaligned length                 0                                                                                 
#Una_partially unaligned contigs           0                                                                                 
#Una_with misassembly                      0                                                                                 
#Una_both parts are significant            0                                                                                 
Una_Partially unaligned length             0                                                                                 
GAGE_Contigs #                             460                                                                               
GAGE_Min contig                            200                                                                               
GAGE_Max contig                            51541                                                                             
GAGE_N50                                   13406 COUNT: 64                                                                   
GAGE_Genome size                           2813862                                                                           
GAGE_Assembly size                         2759743                                                                           
GAGE_Chaff bases                           0                                                                                 
GAGE_Missing reference bases               51078(1.82%)                                                                      
GAGE_Missing assembly bases                10(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                          
GAGE_Duplicated reference bases            1324                                                                              
GAGE_Compressed reference bases            17321                                                                             
GAGE_Bad trim                              10                                                                                
GAGE_Avg idy                               99.99                                                                             
GAGE_SNPs                                  32                                                                                
GAGE_Indels < 5bp                          124                                                                               
GAGE_Indels >= 5                           2                                                                                 
GAGE_Inversions                            0                                                                                 
GAGE_Relocation                            1                                                                                 
GAGE_Translocation                         0                                                                                 
GAGE_Corrected contig #                    457                                                                               
GAGE_Corrected assembly size               2759129                                                                           
GAGE_Min correct contig                    200                                                                               
GAGE_Max correct contig                    51543                                                                             
GAGE_Corrected N50                         13371 COUNT: 65                                                                   
