All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_32-unitigs
#Contigs (>= 0 bp)             5368                                                                                   
#Contigs (>= 1000 bp)          236                                                                                    
Total length (>= 0 bp)         2966173                                                                                
Total length (>= 1000 bp)      2675069                                                                                
#Contigs                       358                                                                                    
Largest contig                 57089                                                                                  
Total length                   2724155                                                                                
Reference length               2813862                                                                                
GC (%)                         32.58                                                                                  
Reference GC (%)               32.81                                                                                  
N50                            18377                                                                                  
NG50                           17755                                                                                  
N75                            10357                                                                                  
NG75                           9510                                                                                   
#misassemblies                 1                                                                                      
#local misassemblies           1                                                                                      
#unaligned contigs             0 + 0 part                                                                             
Unaligned contigs length       0                                                                                      
Genome fraction (%)            96.538                                                                                 
Duplication ratio              1.001                                                                                  
#N's per 100 kbp               0.00                                                                                   
#mismatches per 100 kbp        1.91                                                                                   
#indels per 100 kbp            9.13                                                                                   
#genes                         2472 + 146 part                                                                        
#predicted genes (unique)      2799                                                                                   
#predicted genes (>= 0 bp)     2799                                                                                   
#predicted genes (>= 300 bp)   2320                                                                                   
#predicted genes (>= 1500 bp)  270                                                                                    
#predicted genes (>= 3000 bp)  22                                                                                     
Largest alignment              57089                                                                                  
NA50                           18308                                                                                  
NGA50                          17755                                                                                  
NA75                           10094                                                                                  
NGA75                          9414                                                                                   
