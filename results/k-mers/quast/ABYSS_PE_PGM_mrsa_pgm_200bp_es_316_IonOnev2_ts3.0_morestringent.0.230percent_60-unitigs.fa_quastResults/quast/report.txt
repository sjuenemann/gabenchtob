All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_60-unitigs
#Contigs (>= 0 bp)             3003                                                                                   
#Contigs (>= 1000 bp)          345                                                                                    
Total length (>= 0 bp)         2974960                                                                                
Total length (>= 1000 bp)      2692381                                                                                
#Contigs                       476                                                                                    
Largest contig                 61574                                                                                  
Total length                   2750723                                                                                
Reference length               2813862                                                                                
GC (%)                         32.59                                                                                  
Reference GC (%)               32.81                                                                                  
N50                            11499                                                                                  
NG50                           11384                                                                                  
N75                            6605                                                                                   
NG75                           5922                                                                                   
#misassemblies                 1                                                                                      
#local misassemblies           5                                                                                      
#unaligned contigs             0 + 0 part                                                                             
Unaligned contigs length       0                                                                                      
Genome fraction (%)            97.372                                                                                 
Duplication ratio              1.002                                                                                  
#N's per 100 kbp               0.00                                                                                   
#mismatches per 100 kbp        2.88                                                                                   
#indels per 100 kbp            11.13                                                                                  
#genes                         2428 + 214 part                                                                        
#predicted genes (unique)      2888                                                                                   
#predicted genes (>= 0 bp)     2889                                                                                   
#predicted genes (>= 300 bp)   2357                                                                                   
#predicted genes (>= 1500 bp)  272                                                                                    
#predicted genes (>= 3000 bp)  23                                                                                     
Largest alignment              61574                                                                                  
NA50                           11499                                                                                  
NGA50                          11344                                                                                  
NA75                           6546                                                                                   
NGA75                          5922                                                                                   
