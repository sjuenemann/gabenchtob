All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K99.final_contigs
#Contigs (>= 0 bp)             8283                                                                                       
#Contigs (>= 1000 bp)          62                                                                                         
Total length (>= 0 bp)         5268339                                                                                    
Total length (>= 1000 bp)      2759055                                                                                    
#Contigs                       7930                                                                                       
Largest contig                 236821                                                                                     
Total length                   5220540                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.08                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            17396                                                                                      
NG50                           88650                                                                                      
N75                            313                                                                                        
NG75                           39835                                                                                      
#misassemblies                 33                                                                                         
#local misassemblies           8                                                                                          
#unaligned contigs             1752 + 211 part                                                                            
Unaligned contigs length       554291                                                                                     
Genome fraction (%)            98.603                                                                                     
Duplication ratio              1.676                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        6.60                                                                                       
#indels per 100 kbp            20.18                                                                                      
#genes                         2658 + 53 part                                                                             
#predicted genes (unique)      8223                                                                                       
#predicted genes (>= 0 bp)     8230                                                                                       
#predicted genes (>= 300 bp)   2331                                                                                       
#predicted genes (>= 1500 bp)  290                                                                                        
#predicted genes (>= 3000 bp)  29                                                                                         
Largest alignment              236821                                                                                     
NA50                           16652                                                                                      
NGA50                          88650                                                                                      
NA75                           291                                                                                        
NGA75                          39835                                                                                      
