All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K99.final_contigs
GAGE_Contigs #                   7930                                                                                       
GAGE_Min contig                  202                                                                                        
GAGE_Max contig                  236821                                                                                     
GAGE_N50                         88650 COUNT: 9                                                                             
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               5220540                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     1776(0.06%)                                                                                
GAGE_Missing assembly bases      253633(4.86%)                                                                              
GAGE_Missing assembly contigs    620(7.82%)                                                                                 
GAGE_Duplicated reference bases  2180601                                                                                    
GAGE_Compressed reference bases  37789                                                                                      
GAGE_Bad trim                    56679                                                                                      
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        46                                                                                         
GAGE_Indels < 5bp                307                                                                                        
GAGE_Indels >= 5                 8                                                                                          
GAGE_Inversions                  1                                                                                          
GAGE_Relocation                  3                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          144                                                                                        
GAGE_Corrected assembly size     2787953                                                                                    
GAGE_Min correct contig          202                                                                                        
GAGE_Max correct contig          207893                                                                                     
GAGE_Corrected N50               74287 COUNT: 11                                                                            
