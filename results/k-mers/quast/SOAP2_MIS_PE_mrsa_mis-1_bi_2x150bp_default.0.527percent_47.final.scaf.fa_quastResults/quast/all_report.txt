All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_47.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_47.final.scaf
#Contigs                                   1026                                                                          136                                                                  
#Contigs (>= 0 bp)                         7990                                                                          6906                                                                 
#Contigs (>= 1000 bp)                      678                                                                           88                                                                   
Largest contig                             16951                                                                         275155                                                               
Total length                               2718889                                                                       2780225                                                              
Total length (>= 0 bp)                     3750926                                                                       3790458                                                              
Total length (>= 1000 bp)                  2542305                                                                       2761943                                                              
Reference length                           2813862                                                                       2813862                                                              
N50                                        4597                                                                          53117                                                                
NG50                                       4476                                                                          53117                                                                
N75                                        2526                                                                          37888                                                                
NG75                                       2280                                                                          37888                                                                
L50                                        179                                                                           15                                                                   
LG50                                       189                                                                           15                                                                   
L75                                        376                                                                           31                                                                   
LG75                                       406                                                                           31                                                                   
#local misassemblies                       1                                                                             91                                                                   
#misassemblies                             1                                                                             1                                                                    
#misassembled contigs                      1                                                                             1                                                                    
Misassembled contigs length                5508                                                                          52562                                                                
Misassemblies inter-contig overlap         0                                                                             0                                                                    
#unaligned contigs                         9 + 0 part                                                                    12 + 8 part                                                          
Unaligned contigs length                   6885                                                                          14003                                                                
#ambiguously mapped contigs                8                                                                             8                                                                    
Extra bases in ambiguously mapped contigs  -3546                                                                         -3546                                                                
#N's                                       489                                                                           40032                                                                
#N's per 100 kbp                           17.99                                                                         1439.88                                                              
Genome fraction (%)                        96.187                                                                        96.862                                                               
Duplication ratio                          1.001                                                                         1.014                                                                
#genes                                     1950 + 673 part                                                               2518 + 109 part                                                      
#predicted genes (unique)                  3337                                                                          3075                                                                 
#predicted genes (>= 0 bp)                 3337                                                                          3075                                                                 
#predicted genes (>= 300 bp)               2540                                                                          2471                                                                 
#predicted genes (>= 1500 bp)              205                                                                           233                                                                  
#predicted genes (>= 3000 bp)              13                                                                            19                                                                   
#mismatches                                54                                                                            46                                                                   
#indels                                    312                                                                           13848                                                                
Indels length                              935                                                                           24437                                                                
#mismatches per 100 kbp                    2.00                                                                          1.69                                                                 
#indels per 100 kbp                        11.53                                                                         508.08                                                               
GC (%)                                     32.59                                                                         32.61                                                                
Reference GC (%)                           32.81                                                                         32.81                                                                
Average %IDY                               99.951                                                                        99.137                                                               
Largest alignment                          16951                                                                         274506                                                               
NA50                                       4588                                                                          53117                                                                
NGA50                                      4476                                                                          53117                                                                
NA75                                       2516                                                                          37888                                                                
NGA75                                      2277                                                                          36112                                                                
LA50                                       179                                                                           15                                                                   
LGA50                                      189                                                                           15                                                                   
LA75                                       377                                                                           31                                                                   
LGA75                                      407                                                                           32                                                                   
#Mis_misassemblies                         1                                                                             1                                                                    
#Mis_relocations                           1                                                                             1                                                                    
#Mis_translocations                        0                                                                             0                                                                    
#Mis_inversions                            0                                                                             0                                                                    
#Mis_misassembled contigs                  1                                                                             1                                                                    
Mis_Misassembled contigs length            5508                                                                          52562                                                                
#Mis_local misassemblies                   1                                                                             91                                                                   
#Mis_short indels (<= 5 bp)                283                                                                           13420                                                                
#Mis_long indels (> 5 bp)                  29                                                                            428                                                                  
#Una_fully unaligned contigs               9                                                                             12                                                                   
Una_Fully unaligned length                 6885                                                                          8382                                                                 
#Una_partially unaligned contigs           0                                                                             8                                                                    
#Una_with misassembly                      0                                                                             0                                                                    
#Una_both parts are significant            0                                                                             5                                                                    
Una_Partially unaligned length             0                                                                             5621                                                                 
GAGE_Contigs #                             1026                                                                          136                                                                  
GAGE_Min contig                            200                                                                           209                                                                  
GAGE_Max contig                            16951                                                                         275155                                                               
GAGE_N50                                   4476 COUNT: 189                                                               53117 COUNT: 15                                                      
GAGE_Genome size                           2813862                                                                       2813862                                                              
GAGE_Assembly size                         2718889                                                                       2780225                                                              
GAGE_Chaff bases                           0                                                                             0                                                                    
GAGE_Missing reference bases               95683(3.40%)                                                                  73618(2.62%)                                                         
GAGE_Missing assembly bases                6452(0.24%)                                                                   46983(1.69%)                                                         
GAGE_Missing assembly contigs              8(0.78%)                                                                      7(5.15%)                                                             
GAGE_Duplicated reference bases            207                                                                           716                                                                  
GAGE_Compressed reference bases            9112                                                                          12250                                                                
GAGE_Bad trim                              66                                                                            956                                                                  
GAGE_Avg idy                               99.99                                                                         99.91                                                                
GAGE_SNPs                                  54                                                                            49                                                                   
GAGE_Indels < 5bp                          148                                                                           303                                                                  
GAGE_Indels >= 5                           55                                                                            1018                                                                 
GAGE_Inversions                            0                                                                             0                                                                    
GAGE_Relocation                            1                                                                             10                                                                   
GAGE_Translocation                         0                                                                             0                                                                    
GAGE_Corrected contig #                    1066                                                                          1026                                                                 
GAGE_Corrected assembly size               2711231                                                                       2714533                                                              
GAGE_Min correct contig                    200                                                                           200                                                                  
GAGE_Max correct contig                    16951                                                                         19941                                                                
GAGE_Corrected N50                         4333 COUNT: 197                                                               4434 COUNT: 190                                                      
