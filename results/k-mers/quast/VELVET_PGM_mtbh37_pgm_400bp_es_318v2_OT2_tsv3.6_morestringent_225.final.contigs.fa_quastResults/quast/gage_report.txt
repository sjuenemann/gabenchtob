All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_225.final.contigs
GAGE_Contigs #                   1903                                                                           
GAGE_Min contig                  449                                                                            
GAGE_Max contig                  7720                                                                           
GAGE_N50                         982 COUNT: 1268                                                                
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               2725151                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     1743021(39.51%)                                                                
GAGE_Missing assembly bases      1323(0.05%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  6945                                                                           
GAGE_Compressed reference bases  23761                                                                          
GAGE_Bad trim                    1293                                                                           
GAGE_Avg idy                     99.92                                                                          
GAGE_SNPs                        75                                                                             
GAGE_Indels < 5bp                1731                                                                           
GAGE_Indels >= 5                 1                                                                              
GAGE_Inversions                  0                                                                              
GAGE_Relocation                  4                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          1905                                                                           
GAGE_Corrected assembly size     2718909                                                                        
GAGE_Min correct contig          323                                                                            
GAGE_Max correct contig          7717                                                                           
GAGE_Corrected N50               977 COUNT: 1276                                                                
