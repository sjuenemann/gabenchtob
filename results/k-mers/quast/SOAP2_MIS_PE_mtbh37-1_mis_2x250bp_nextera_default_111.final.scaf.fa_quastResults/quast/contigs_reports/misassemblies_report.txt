All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_111.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_111.final.scaf
#Mis_misassemblies               16                                                                       17                                                              
#Mis_relocations                 4                                                                        5                                                               
#Mis_translocations              0                                                                        0                                                               
#Mis_inversions                  12                                                                       12                                                              
#Mis_misassembled contigs        16                                                                       17                                                              
Mis_Misassembled contigs length  15353                                                                    153523                                                          
#Mis_local misassemblies         19                                                                       499                                                             
#mismatches                      491                                                                      482                                                             
#indels                          59                                                                       1436                                                            
#Mis_short indels (<= 5 bp)      51                                                                       1353                                                            
#Mis_long indels (> 5 bp)        8                                                                        83                                                              
Indels length                    286                                                                      3419                                                            
