All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_175.final.contigs
#Contigs (>= 0 bp)             44                                                                                          
#Contigs (>= 1000 bp)          1                                                                                           
Total length (>= 0 bp)         26433                                                                                       
Total length (>= 1000 bp)      1147                                                                                        
#Contigs                       44                                                                                          
Largest contig                 1147                                                                                        
Total length                   26433                                                                                       
Reference length               2813862                                                                                     
GC (%)                         35.97                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            591                                                                                         
NG50                           None                                                                                        
N75                            547                                                                                         
NG75                           None                                                                                        
#misassemblies                 1                                                                                           
#local misassemblies           0                                                                                           
#unaligned contigs             0 + 0 part                                                                                  
Unaligned contigs length       0                                                                                           
Genome fraction (%)            0.861                                                                                       
Duplication ratio              1.012                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        4.13                                                                                        
#indels per 100 kbp            61.91                                                                                       
#genes                         4 + 40 part                                                                                 
#predicted genes (unique)      47                                                                                          
#predicted genes (>= 0 bp)     47                                                                                          
#predicted genes (>= 300 bp)   36                                                                                          
#predicted genes (>= 1500 bp)  0                                                                                           
#predicted genes (>= 3000 bp)  0                                                                                           
Largest alignment              1147                                                                                        
NA50                           577                                                                                         
NGA50                          None                                                                                        
NA75                           529                                                                                         
NGA75                          None                                                                                        
