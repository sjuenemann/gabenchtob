All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_33.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_33.final.scaf
GAGE_Contigs #                   30859                                                                            30861                                                                   
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  1594                                                                             1594                                                                    
GAGE_N50                         251 COUNT: 8346                                                                  251 COUNT: 8344                                                         
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               7892408                                                                          7893478                                                                 
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     3202714(57.25%)                                                                  3202255(57.24%)                                                         
GAGE_Missing assembly bases      5476126(69.38%)                                                                  5476737(69.38%)                                                         
GAGE_Missing assembly contigs    23663(76.68%)                                                                    23664(76.68%)                                                           
GAGE_Duplicated reference bases  2592                                                                             2592                                                                    
GAGE_Compressed reference bases  5641                                                                             5641                                                                    
GAGE_Bad trim                    2246                                                                             2452                                                                    
GAGE_Avg idy                     99.97                                                                            99.97                                                                   
GAGE_SNPs                        640                                                                              644                                                                     
GAGE_Indels < 5bp                13                                                                               13                                                                      
GAGE_Indels >= 5                 1                                                                                3                                                                       
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  1                                                                                1                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          7163                                                                             7163                                                                    
GAGE_Corrected assembly size     2411090                                                                          2411090                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          1594                                                                             1594                                                                    
GAGE_Corrected N50               0 COUNT: 0                                                                       0 COUNT: 0                                                              
