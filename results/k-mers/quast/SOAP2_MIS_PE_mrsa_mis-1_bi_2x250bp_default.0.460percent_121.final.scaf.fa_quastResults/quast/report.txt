All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_121.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_121.final.scaf
#Contigs (>= 0 bp)             1284                                                                           1262                                                                  
#Contigs (>= 1000 bp)          87                                                                             75                                                                    
Total length (>= 0 bp)         2987415                                                                        2988048                                                               
Total length (>= 1000 bp)      2768329                                                                        2771502                                                               
#Contigs                       566                                                                            546                                                                   
Largest contig                 237775                                                                         237775                                                                
Total length                   2891280                                                                        2892260                                                               
Reference length               2813862                                                                        2813862                                                               
GC (%)                         32.71                                                                          32.71                                                                 
Reference GC (%)               32.81                                                                          32.81                                                                 
N50                            57519                                                                          71054                                                                 
NG50                           57519                                                                          74359                                                                 
N75                            30021                                                                          33827                                                                 
NG75                           31362                                                                          35844                                                                 
#misassemblies                 14                                                                             16                                                                    
#local misassemblies           4                                                                              14                                                                    
#unaligned contigs             3 + 9 part                                                                     3 + 9 part                                                            
Unaligned contigs length       6195                                                                           6195                                                                  
Genome fraction (%)            98.678                                                                         98.691                                                                
Duplication ratio              1.032                                                                          1.032                                                                 
#N's per 100 kbp               1.18                                                                           23.06                                                                 
#mismatches per 100 kbp        1.76                                                                           1.84                                                                  
#indels per 100 kbp            0.50                                                                           1.87                                                                  
#genes                         2640 + 70 part                                                                 2644 + 67 part                                                        
#predicted genes (unique)      3048                                                                           3044                                                                  
#predicted genes (>= 0 bp)     3058                                                                           3052                                                                  
#predicted genes (>= 300 bp)   2306                                                                           2304                                                                  
#predicted genes (>= 1500 bp)  297                                                                            297                                                                   
#predicted genes (>= 3000 bp)  31                                                                             31                                                                    
Largest alignment              237774                                                                         237774                                                                
NA50                           57519                                                                          71054                                                                 
NGA50                          57519                                                                          74359                                                                 
NA75                           30021                                                                          33790                                                                 
NGA75                          31362                                                                          35844                                                                 
