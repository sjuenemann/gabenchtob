All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_203.final.contigs
#Contigs                                   1768                                                                        
#Contigs (>= 0 bp)                         1768                                                                        
#Contigs (>= 1000 bp)                      1363                                                                        
Largest contig                             18995                                                                       
Total length                               4653548                                                                     
Total length (>= 0 bp)                     4653548                                                                     
Total length (>= 1000 bp)                  4359102                                                                     
Reference length                           5594470                                                                     
N50                                        3936                                                                        
NG50                                       3071                                                                        
N75                                        2126                                                                        
NG75                                       1205                                                                        
L50                                        371                                                                         
LG50                                       507                                                                         
L75                                        778                                                                         
LG75                                       1215                                                                        
#local misassemblies                       38                                                                          
#misassemblies                             59                                                                          
#misassembled contigs                      59                                                                          
Misassembled contigs length                333988                                                                      
Misassemblies inter-contig overlap         2976                                                                        
#unaligned contigs                         0 + 0 part                                                                  
Unaligned contigs length                   0                                                                           
#ambiguously mapped contigs                82                                                                          
Extra bases in ambiguously mapped contigs  -54229                                                                      
#N's                                       880                                                                         
#N's per 100 kbp                           18.91                                                                       
Genome fraction (%)                        79.991                                                                      
Duplication ratio                          1.028                                                                       
#genes                                     3123 + 1493 part                                                            
#predicted genes (unique)                  5841                                                                        
#predicted genes (>= 0 bp)                 5860                                                                        
#predicted genes (>= 300 bp)               4376                                                                        
#predicted genes (>= 1500 bp)              410                                                                         
#predicted genes (>= 3000 bp)              28                                                                          
#mismatches                                133                                                                         
#indels                                    1005                                                                        
Indels length                              1026                                                                        
#mismatches per 100 kbp                    2.97                                                                        
#indels per 100 kbp                        22.46                                                                       
GC (%)                                     51.22                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.252                                                                      
Largest alignment                          18995                                                                       
NA50                                       3847                                                                        
NGA50                                      3014                                                                        
NA75                                       2073                                                                        
NGA75                                      1170                                                                        
LA50                                       377                                                                         
LGA50                                      516                                                                         
LA75                                       793                                                                         
LGA75                                      1244                                                                        
#Mis_misassemblies                         59                                                                          
#Mis_relocations                           59                                                                          
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  59                                                                          
Mis_Misassembled contigs length            333988                                                                      
#Mis_local misassemblies                   38                                                                          
#Mis_short indels (<= 5 bp)                1004                                                                        
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           0                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             0                                                                           
GAGE_Contigs #                             1768                                                                        
GAGE_Min contig                            405                                                                         
GAGE_Max contig                            18995                                                                       
GAGE_N50                                   3071 COUNT: 507                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         4653548                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               912026(16.30%)                                                              
GAGE_Missing assembly bases                988(0.02%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            32221                                                                       
GAGE_Compressed reference bases            227713                                                                      
GAGE_Bad trim                              608                                                                         
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  54                                                                          
GAGE_Indels < 5bp                          889                                                                         
GAGE_Indels >= 5                           29                                                                          
GAGE_Inversions                            1                                                                           
GAGE_Relocation                            14                                                                          
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    1789                                                                        
GAGE_Corrected assembly size               4624476                                                                     
GAGE_Min correct contig                    201                                                                         
GAGE_Max correct contig                    18994                                                                       
GAGE_Corrected N50                         2936 COUNT: 531                                                             
