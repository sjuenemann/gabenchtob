All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K113.final_contigs
#Contigs (>= 0 bp)             5634                                                                             
#Contigs (>= 1000 bp)          161                                                                              
Total length (>= 0 bp)         6955066                                                                          
Total length (>= 1000 bp)      5280211                                                                          
#Contigs                       5468                                                                             
Largest contig                 352335                                                                           
Total length                   6930843                                                                          
Reference length               5594470                                                                          
GC (%)                         49.82                                                                            
Reference GC (%)               50.48                                                                            
N50                            91024                                                                            
NG50                           140567                                                                           
N75                            2466                                                                             
NG75                           54832                                                                            
#misassemblies                 93                                                                               
#local misassemblies           16                                                                               
#unaligned contigs             308 + 82 part                                                                    
Unaligned contigs length       98420                                                                            
Genome fraction (%)            94.829                                                                           
Duplication ratio              1.266                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        5.98                                                                             
#indels per 100 kbp            14.04                                                                            
#genes                         4958 + 220 part                                                                  
#predicted genes (unique)      9819                                                                             
#predicted genes (>= 0 bp)     9838                                                                             
#predicted genes (>= 300 bp)   4682                                                                             
#predicted genes (>= 1500 bp)  630                                                                              
#predicted genes (>= 3000 bp)  60                                                                               
Largest alignment              352335                                                                           
NA50                           91024                                                                            
NGA50                          140567                                                                           
NA75                           1654                                                                             
NGA75                          54720                                                                            
