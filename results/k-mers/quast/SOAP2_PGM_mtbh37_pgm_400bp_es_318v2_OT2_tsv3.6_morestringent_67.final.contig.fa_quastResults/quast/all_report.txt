All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_67.final.contig
#Contigs                                   7183                                                                        
#Contigs (>= 0 bp)                         219359                                                                      
#Contigs (>= 1000 bp)                      1                                                                           
Largest contig                             1144                                                                        
Total length                               1861130                                                                     
Total length (>= 0 bp)                     24070442                                                                    
Total length (>= 1000 bp)                  1144                                                                        
Reference length                           4411532                                                                     
N50                                        244                                                                         
NG50                                       None                                                                        
N75                                        217                                                                         
NG75                                       None                                                                        
L50                                        2918                                                                        
LG50                                       None                                                                        
L75                                        4951                                                                        
LG75                                       None                                                                        
#local misassemblies                       3                                                                           
#misassemblies                             16                                                                          
#misassembled contigs                      16                                                                          
Misassembled contigs length                3756                                                                        
Misassemblies inter-contig overlap         319                                                                         
#unaligned contigs                         839 + 31 part                                                               
Unaligned contigs length                   200841                                                                      
#ambiguously mapped contigs                20                                                                          
Extra bases in ambiguously mapped contigs  -4633                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        34.085                                                                      
Duplication ratio                          1.101                                                                       
#genes                                     34 + 2856 part                                                              
#predicted genes (unique)                  5466                                                                        
#predicted genes (>= 0 bp)                 5467                                                                        
#predicted genes (>= 300 bp)               728                                                                         
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                619                                                                         
#indels                                    13942                                                                       
Indels length                              14389                                                                       
#mismatches per 100 kbp                    41.17                                                                       
#indels per 100 kbp                        927.21                                                                      
GC (%)                                     66.20                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               98.821                                                                      
Largest alignment                          1144                                                                        
NA50                                       238                                                                         
NGA50                                      None                                                                        
NA75                                       211                                                                         
NGA75                                      None                                                                        
LA50                                       2959                                                                        
LGA50                                      None                                                                        
LA75                                       5051                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         16                                                                          
#Mis_relocations                           16                                                                          
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  16                                                                          
Mis_Misassembled contigs length            3756                                                                        
#Mis_local misassemblies                   3                                                                           
#Mis_short indels (<= 5 bp)                13942                                                                       
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               839                                                                         
Una_Fully unaligned length                 197909                                                                      
#Una_partially unaligned contigs           31                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             2932                                                                        
GAGE_Contigs #                             7183                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1144                                                                        
GAGE_N50                                   0 COUNT: 0                                                                  
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         1861130                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               2796793(63.40%)                                                             
GAGE_Missing assembly bases                62417(3.35%)                                                                
GAGE_Missing assembly contigs              198(2.76%)                                                                  
GAGE_Duplicated reference bases            81597                                                                       
GAGE_Compressed reference bases            12090                                                                       
GAGE_Bad trim                              15516                                                                       
GAGE_Avg idy                               98.84                                                                       
GAGE_SNPs                                  643                                                                         
GAGE_Indels < 5bp                          15575                                                                       
GAGE_Indels >= 5                           23                                                                          
GAGE_Inversions                            12                                                                          
GAGE_Relocation                            7                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    6298                                                                        
GAGE_Corrected assembly size               1653054                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1144                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
