All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K75.final_contigs
#Contigs                                   615                                                                                
#Contigs (>= 0 bp)                         678                                                                                
#Contigs (>= 1000 bp)                      147                                                                                
Largest contig                             182942                                                                             
Total length                               4418551                                                                            
Total length (>= 0 bp)                     4425361                                                                            
Total length (>= 1000 bp)                  4279953                                                                            
Reference length                           4411532                                                                            
N50                                        66014                                                                              
NG50                                       66014                                                                              
N75                                        33563                                                                              
NG75                                       33563                                                                              
L50                                        21                                                                                 
LG50                                       21                                                                                 
L75                                        45                                                                                 
LG75                                       45                                                                                 
#local misassemblies                       29                                                                                 
#misassemblies                             11                                                                                 
#misassembled contigs                      7                                                                                  
Misassembled contigs length                121229                                                                             
Misassemblies inter-contig overlap         3849                                                                               
#unaligned contigs                         228 + 8 part                                                                       
Unaligned contigs length                   61538                                                                              
#ambiguously mapped contigs                23                                                                                 
Extra bases in ambiguously mapped contigs  -16406                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.397                                                                             
Duplication ratio                          1.011                                                                              
#genes                                     3938 + 105 part                                                                    
#predicted genes (unique)                  4652                                                                               
#predicted genes (>= 0 bp)                 4655                                                                               
#predicted genes (>= 300 bp)               3759                                                                               
#predicted genes (>= 1500 bp)              476                                                                                
#predicted genes (>= 3000 bp)              48                                                                                 
#mismatches                                379                                                                                
#indels                                    1328                                                                               
Indels length                              1663                                                                               
#mismatches per 100 kbp                    8.82                                                                               
#indels per 100 kbp                        30.91                                                                              
GC (%)                                     65.38                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.316                                                                             
Largest alignment                          182942                                                                             
NA50                                       64011                                                                              
NGA50                                      64011                                                                              
NA75                                       32267                                                                              
NGA75                                      32267                                                                              
LA50                                       22                                                                                 
LGA50                                      22                                                                                 
LA75                                       46                                                                                 
LGA75                                      46                                                                                 
#Mis_misassemblies                         11                                                                                 
#Mis_relocations                           11                                                                                 
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  7                                                                                  
Mis_Misassembled contigs length            121229                                                                             
#Mis_local misassemblies                   29                                                                                 
#Mis_short indels (<= 5 bp)                1321                                                                               
#Mis_long indels (> 5 bp)                  7                                                                                  
#Una_fully unaligned contigs               228                                                                                
Una_Fully unaligned length                 61228                                                                              
#Una_partially unaligned contigs           8                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             310                                                                                
GAGE_Contigs #                             615                                                                                
GAGE_Min contig                            208                                                                                
GAGE_Max contig                            182942                                                                             
GAGE_N50                                   66014 COUNT: 21                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4418551                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               51656(1.17%)                                                                       
GAGE_Missing assembly bases                20918(0.47%)                                                                       
GAGE_Missing assembly contigs              64(10.41%)                                                                         
GAGE_Duplicated reference bases            86425                                                                              
GAGE_Compressed reference bases            58371                                                                              
GAGE_Bad trim                              3927                                                                               
GAGE_Avg idy                               99.96                                                                              
GAGE_SNPs                                  287                                                                                
GAGE_Indels < 5bp                          1347                                                                               
GAGE_Indels >= 5                           22                                                                                 
GAGE_Inversions                            3                                                                                  
GAGE_Relocation                            17                                                                                 
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    256                                                                                
GAGE_Corrected assembly size               4316206                                                                            
GAGE_Min correct contig                    208                                                                                
GAGE_Max correct contig                    182973                                                                             
GAGE_Corrected N50                         52837 COUNT: 27                                                                    
