All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_67.final.contigs
GAGE_Contigs #                   6603                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  3327                                                                          
GAGE_N50                         220 COUNT: 5631                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               2409733                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     2009027(45.54%)                                                               
GAGE_Missing assembly bases      195(0.01%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  1782                                                                          
GAGE_Compressed reference bases  9454                                                                          
GAGE_Bad trim                    195                                                                           
GAGE_Avg idy                     99.93                                                                         
GAGE_SNPs                        124                                                                           
GAGE_Indels < 5bp                1580                                                                          
GAGE_Indels >= 5                 4                                                                             
GAGE_Inversions                  7                                                                             
GAGE_Relocation                  4                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          6585                                                                          
GAGE_Corrected assembly size     2405282                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          3328                                                                          
GAGE_Corrected N50               220 COUNT: 5634                                                               
