All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_64-unitigs
GAGE_Contigs #                   2408                                                                      
GAGE_Min contig                  200                                                                       
GAGE_Max contig                  13194                                                                     
GAGE_N50                         2558 COUNT: 509                                                           
GAGE_Genome size                 4411532                                                                   
GAGE_Assembly size               4136173                                                                   
GAGE_Chaff bases                 0                                                                         
GAGE_Missing reference bases     262402(5.95%)                                                             
GAGE_Missing assembly bases      857(0.02%)                                                                
GAGE_Missing assembly contigs    2(0.08%)                                                                  
GAGE_Duplicated reference bases  0                                                                         
GAGE_Compressed reference bases  20482                                                                     
GAGE_Bad trim                    67                                                                        
GAGE_Avg idy                     99.98                                                                     
GAGE_SNPs                        75                                                                        
GAGE_Indels < 5bp                718                                                                       
GAGE_Indels >= 5                 8                                                                         
GAGE_Inversions                  0                                                                         
GAGE_Relocation                  8                                                                         
GAGE_Translocation               0                                                                         
GAGE_Corrected contig #          2421                                                                      
GAGE_Corrected assembly size     4137351                                                                   
GAGE_Min correct contig          200                                                                       
GAGE_Max correct contig          13199                                                                     
GAGE_Corrected N50               2526 COUNT: 514                                                           
