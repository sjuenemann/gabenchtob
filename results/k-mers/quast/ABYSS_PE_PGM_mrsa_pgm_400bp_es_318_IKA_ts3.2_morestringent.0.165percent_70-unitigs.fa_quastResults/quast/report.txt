All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_70-unitigs
#Contigs (>= 0 bp)             4523                                                                              
#Contigs (>= 1000 bp)          333                                                                               
Total length (>= 0 bp)         3168565                                                                           
Total length (>= 1000 bp)      2689354                                                                           
#Contigs                       469                                                                               
Largest contig                 52484                                                                             
Total length                   2747937                                                                           
Reference length               2813862                                                                           
GC (%)                         32.59                                                                             
Reference GC (%)               32.81                                                                             
N50                            13560                                                                             
NG50                           13419                                                                             
N75                            6975                                                                              
NG75                           6476                                                                              
#misassemblies                 1                                                                                 
#local misassemblies           2                                                                                 
#unaligned contigs             0 + 0 part                                                                        
Unaligned contigs length       0                                                                                 
Genome fraction (%)            97.391                                                                            
Duplication ratio              1.002                                                                             
#N's per 100 kbp               0.00                                                                              
#mismatches per 100 kbp        1.31                                                                              
#indels per 100 kbp            4.05                                                                              
#genes                         2396 + 256 part                                                                   
#predicted genes (unique)      2889                                                                              
#predicted genes (>= 0 bp)     2892                                                                              
#predicted genes (>= 300 bp)   2369                                                                              
#predicted genes (>= 1500 bp)  263                                                                               
#predicted genes (>= 3000 bp)  25                                                                                
Largest alignment              52484                                                                             
NA50                           13560                                                                             
NGA50                          13419                                                                             
NA75                           6975                                                                              
NGA75                          6441                                                                              
