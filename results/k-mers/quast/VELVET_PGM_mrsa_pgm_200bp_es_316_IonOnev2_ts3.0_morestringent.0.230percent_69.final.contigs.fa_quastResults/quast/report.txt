All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_69.final.contigs
#Contigs (>= 0 bp)             934                                                                                        
#Contigs (>= 1000 bp)          586                                                                                        
Total length (>= 0 bp)         2761374                                                                                    
Total length (>= 1000 bp)      2607503                                                                                    
#Contigs                       867                                                                                        
Largest contig                 30445                                                                                      
Total length                   2750629                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.62                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            5886                                                                                       
NG50                           5710                                                                                       
N75                            3038                                                                                       
NG75                           2911                                                                                       
#misassemblies                 17                                                                                         
#local misassemblies           18                                                                                         
#unaligned contigs             1 + 7 part                                                                                 
Unaligned contigs length       1698                                                                                       
Genome fraction (%)            96.766                                                                                     
Duplication ratio              1.007                                                                                      
#N's per 100 kbp               8.36                                                                                       
#mismatches per 100 kbp        4.77                                                                                       
#indels per 100 kbp            27.99                                                                                      
#genes                         2187 + 456 part                                                                            
#predicted genes (unique)      3182                                                                                       
#predicted genes (>= 0 bp)     3187                                                                                       
#predicted genes (>= 300 bp)   2474                                                                                       
#predicted genes (>= 1500 bp)  230                                                                                        
#predicted genes (>= 3000 bp)  20                                                                                         
Largest alignment              30445                                                                                      
NA50                           5830                                                                                       
NGA50                          5667                                                                                       
NA75                           3027                                                                                       
NGA75                          2906                                                                                       
