All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_29.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_29.final.scaf
#Contigs                                   2992                                                                          422                                                                  
#Contigs (>= 0 bp)                         11291                                                                         7321                                                                 
#Contigs (>= 1000 bp)                      828                                                                           215                                                                  
Largest contig                             6995                                                                          67426                                                                
Total length                               2515784                                                                       2816255                                                              
Total length (>= 0 bp)                     3553937                                                                       3716473                                                              
Total length (>= 1000 bp)                  1463272                                                                       2740022                                                              
Reference length                           2813862                                                                       2813862                                                              
N50                                        1180                                                                          23441                                                                
NG50                                       1045                                                                          23441                                                                
N75                                        656                                                                           11799                                                                
NG75                                       491                                                                           11799                                                                
L50                                        639                                                                           38                                                                   
LG50                                       773                                                                           38                                                                   
L75                                        1355                                                                          81                                                                   
LG75                                       1748                                                                          81                                                                   
#local misassemblies                       8                                                                             268                                                                  
#misassemblies                             0                                                                             1                                                                    
#misassembled contigs                      0                                                                             1                                                                    
Misassembled contigs length                0                                                                             13620                                                                
Misassemblies inter-contig overlap         0                                                                             0                                                                    
#unaligned contigs                         41 + 7 part                                                                   74 + 62 part                                                         
Unaligned contigs length                   13412                                                                         452173                                                               
#ambiguously mapped contigs                5                                                                             5                                                                    
Extra bases in ambiguously mapped contigs  -1666                                                                         -1666                                                                
#N's                                       1762                                                                          164331                                                               
#N's per 100 kbp                           70.04                                                                         5835.09                                                              
Genome fraction (%)                        88.724                                                                        63.054                                                               
Duplication ratio                          1.002                                                                         1.332                                                                
#genes                                     967 + 1597 part                                                               1401 + 444 part                                                      
#predicted genes (unique)                  4717                                                                          4139                                                                 
#predicted genes (>= 0 bp)                 4717                                                                          4139                                                                 
#predicted genes (>= 300 bp)               2728                                                                          2744                                                                 
#predicted genes (>= 1500 bp)              48                                                                            82                                                                   
#predicted genes (>= 3000 bp)              1                                                                             2                                                                    
#mismatches                                57                                                                            41                                                                   
#indels                                    900                                                                           16810                                                                
Indels length                              2227                                                                          43370                                                                
#mismatches per 100 kbp                    2.28                                                                          2.31                                                                 
#indels per 100 kbp                        36.05                                                                         947.73                                                               
GC (%)                                     32.44                                                                         32.52                                                                
Reference GC (%)                           32.81                                                                         32.81                                                                
Average %IDY                               99.917                                                                        98.422                                                               
Largest alignment                          6995                                                                          63562                                                                
NA50                                       1180                                                                          8327                                                                 
NGA50                                      1045                                                                          8327                                                                 
NA75                                       654                                                                           None                                                                 
NGA75                                      490                                                                           None                                                                 
LA50                                       639                                                                           70                                                                   
LGA50                                      773                                                                           70                                                                   
LA75                                       1355                                                                          None                                                                 
LGA75                                      1750                                                                          None                                                                 
#Mis_misassemblies                         0                                                                             1                                                                    
#Mis_relocations                           0                                                                             1                                                                    
#Mis_translocations                        0                                                                             0                                                                    
#Mis_inversions                            0                                                                             0                                                                    
#Mis_misassembled contigs                  0                                                                             1                                                                    
Mis_Misassembled contigs length            0                                                                             13620                                                                
#Mis_local misassemblies                   8                                                                             268                                                                  
#Mis_short indels (<= 5 bp)                792                                                                           15800                                                                
#Mis_long indels (> 5 bp)                  108                                                                           1010                                                                 
#Una_fully unaligned contigs               41                                                                            74                                                                   
Una_Fully unaligned length                 13076                                                                         189307                                                               
#Una_partially unaligned contigs           7                                                                             62                                                                   
#Una_with misassembly                      0                                                                             1                                                                    
#Una_both parts are significant            0                                                                             47                                                                   
Una_Partially unaligned length             336                                                                           262866                                                               
GAGE_Contigs #                             2992                                                                          422                                                                  
GAGE_Min contig                            200                                                                           201                                                                  
GAGE_Max contig                            6995                                                                          67426                                                                
GAGE_N50                                   1045 COUNT: 773                                                               23441 COUNT: 38                                                      
GAGE_Genome size                           2813862                                                                       2813862                                                              
GAGE_Assembly size                         2515784                                                                       2816255                                                              
GAGE_Chaff bases                           0                                                                             0                                                                    
GAGE_Missing reference bases               310602(11.04%)                                                                218729(7.77%)                                                        
GAGE_Missing assembly bases                11427(0.45%)                                                                  220341(7.82%)                                                        
GAGE_Missing assembly contigs              32(1.07%)                                                                     32(7.58%)                                                            
GAGE_Duplicated reference bases            0                                                                             0                                                                    
GAGE_Compressed reference bases            2541                                                                          3704                                                                 
GAGE_Bad trim                              1431                                                                          12541                                                                
GAGE_Avg idy                               99.98                                                                         99.83                                                                
GAGE_SNPs                                  55                                                                            57                                                                   
GAGE_Indels < 5bp                          474                                                                           787                                                                  
GAGE_Indels >= 5                           168                                                                           3500                                                                 
GAGE_Inversions                            0                                                                             0                                                                    
GAGE_Relocation                            0                                                                             5                                                                    
GAGE_Translocation                         0                                                                             0                                                                    
GAGE_Corrected contig #                    3050                                                                          2907                                                                 
GAGE_Corrected assembly size               2493621                                                                       2478775                                                              
GAGE_Min correct contig                    200                                                                           200                                                                  
GAGE_Max correct contig                    5936                                                                          5936                                                                 
GAGE_Corrected N50                         1000 COUNT: 815                                                               1043 COUNT: 785                                                      
