All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_29.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_29.final.scaf
#Mis_misassemblies               0                                                                             1                                                                    
#Mis_relocations                 0                                                                             1                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        0                                                                             1                                                                    
Mis_Misassembled contigs length  0                                                                             13620                                                                
#Mis_local misassemblies         8                                                                             268                                                                  
#mismatches                      57                                                                            41                                                                   
#indels                          900                                                                           16810                                                                
#Mis_short indels (<= 5 bp)      792                                                                           15800                                                                
#Mis_long indels (> 5 bp)        108                                                                           1010                                                                 
Indels length                    2227                                                                          43370                                                                
