All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_88-unitigs
#Contigs (>= 0 bp)             5206                                                                   
#Contigs (>= 1000 bp)          679                                                                    
Total length (>= 0 bp)         5884478                                                                
Total length (>= 1000 bp)      5081843                                                                
#Contigs                       1215                                                                   
Largest contig                 46543                                                                  
Total length                   5312881                                                                
Reference length               5594470                                                                
GC (%)                         50.27                                                                  
Reference GC (%)               50.48                                                                  
N50                            12389                                                                  
NG50                           11183                                                                  
N75                            5978                                                                   
NG75                           5094                                                                   
#misassemblies                 2                                                                      
#local misassemblies           7                                                                      
#unaligned contigs             0 + 0 part                                                             
Unaligned contigs length       0                                                                      
Genome fraction (%)            93.458                                                                 
Duplication ratio              1.002                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        1.59                                                                   
#indels per 100 kbp            5.18                                                                   
#genes                         4402 + 700 part                                                        
#predicted genes (unique)      5946                                                                   
#predicted genes (>= 0 bp)     5953                                                                   
#predicted genes (>= 300 bp)   4618                                                                   
#predicted genes (>= 1500 bp)  581                                                                    
#predicted genes (>= 3000 bp)  53                                                                     
Largest alignment              46543                                                                  
NA50                           12389                                                                  
NGA50                          11171                                                                  
NA75                           5922                                                                   
NGA75                          5084                                                                   
