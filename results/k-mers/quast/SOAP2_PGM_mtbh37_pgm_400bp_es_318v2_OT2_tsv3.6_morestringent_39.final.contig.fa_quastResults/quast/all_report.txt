All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_39.final.contig
#Contigs                                   1603                                                                        
#Contigs (>= 0 bp)                         346125                                                                      
#Contigs (>= 1000 bp)                      3                                                                           
Largest contig                             1083                                                                        
Total length                               437341                                                                      
Total length (>= 0 bp)                     22335673                                                                    
Total length (>= 1000 bp)                  3196                                                                        
Reference length                           4411532                                                                     
N50                                        260                                                                         
NG50                                       None                                                                        
N75                                        223                                                                         
NG75                                       None                                                                        
L50                                        628                                                                         
LG50                                       None                                                                        
L75                                        1083                                                                        
LG75                                       None                                                                        
#local misassemblies                       1                                                                           
#misassemblies                             0                                                                           
#misassembled contigs                      0                                                                           
Misassembled contigs length                0                                                                           
Misassemblies inter-contig overlap         37                                                                          
#unaligned contigs                         82 + 4 part                                                                 
Unaligned contigs length                   19170                                                                       
#ambiguously mapped contigs                3                                                                           
Extra bases in ambiguously mapped contigs  -737                                                                        
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        9.446                                                                       
Duplication ratio                          1.002                                                                       
#genes                                     6 + 1099 part                                                               
#predicted genes (unique)                  1486                                                                        
#predicted genes (>= 0 bp)                 1486                                                                        
#predicted genes (>= 300 bp)               247                                                                         
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                45                                                                          
#indels                                    264                                                                         
Indels length                              274                                                                         
#mismatches per 100 kbp                    10.80                                                                       
#indels per 100 kbp                        63.35                                                                       
GC (%)                                     69.41                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               99.926                                                                      
Largest alignment                          1083                                                                        
NA50                                       258                                                                         
NGA50                                      None                                                                        
NA75                                       221                                                                         
NGA75                                      None                                                                        
LA50                                       631                                                                         
LGA50                                      None                                                                        
LA75                                       1090                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         0                                                                           
#Mis_relocations                           0                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  0                                                                           
Mis_Misassembled contigs length            0                                                                           
#Mis_local misassemblies                   1                                                                           
#Mis_short indels (<= 5 bp)                264                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               82                                                                          
Una_Fully unaligned length                 18837                                                                       
#Una_partially unaligned contigs           4                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             333                                                                         
GAGE_Contigs #                             1603                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1083                                                                        
GAGE_N50                                   0 COUNT: 0                                                                  
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         437341                                                                      
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               3989240(90.43%)                                                             
GAGE_Missing assembly bases                14668(3.35%)                                                                
GAGE_Missing assembly contigs              58(3.62%)                                                                   
GAGE_Duplicated reference bases            483                                                                         
GAGE_Compressed reference bases            1077                                                                        
GAGE_Bad trim                              1085                                                                        
GAGE_Avg idy                               99.88                                                                       
GAGE_SNPs                                  46                                                                          
GAGE_Indels < 5bp                          440                                                                         
GAGE_Indels >= 5                           1                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            0                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    1529                                                                        
GAGE_Corrected assembly size               419940                                                                      
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1083                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
