All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_107.final.contigs
GAGE_Contigs #                   34074                                                                       
GAGE_Min contig                  213                                                                         
GAGE_Max contig                  623                                                                         
GAGE_N50                         256 COUNT: 9600                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               8294208                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1232399(22.03%)                                                             
GAGE_Missing assembly bases      4183(0.05%)                                                                 
GAGE_Missing assembly contigs    4(0.01%)                                                                    
GAGE_Duplicated reference bases  3050961                                                                     
GAGE_Compressed reference bases  310513                                                                      
GAGE_Bad trim                    3016                                                                        
GAGE_Avg idy                     99.02                                                                       
GAGE_SNPs                        3400                                                                        
GAGE_Indels < 5bp                32094                                                                       
GAGE_Indels >= 5                 8                                                                           
GAGE_Inversions                  126                                                                         
GAGE_Relocation                  59                                                                          
GAGE_Translocation               13                                                                          
GAGE_Corrected contig #          20924                                                                       
GAGE_Corrected assembly size     5092461                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          623                                                                         
GAGE_Corrected N50               230 COUNT: 10312                                                            
