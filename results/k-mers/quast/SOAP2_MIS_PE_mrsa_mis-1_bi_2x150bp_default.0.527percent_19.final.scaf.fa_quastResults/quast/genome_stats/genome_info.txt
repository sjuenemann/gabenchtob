reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_19.final.scaf_broken  | 81.2468059912       | 1.00119107347     | 1805        | 646       | 1864      | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_19.final.scaf  | 39.2129749078       | 1.70780923311     | 684         | 717       | 606       | None      | None      |
