All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_19.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_19.final.scaf
#Una_fully unaligned contigs      58                                                                            135                                                                  
Una_Fully unaligned length        22456                                                                         415179                                                               
#Una_partially unaligned contigs  31                                                                            103                                                                  
#Una_with misassembly             0                                                                             12                                                                   
#Una_both parts are significant   0                                                                             71                                                                   
Una_Partially unaligned length    1551                                                                          509610                                                               
#N's                              1527                                                                          259100                                                               
