All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                         #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's 
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_97.final.scaf_broken  6                             7458                        1                                 0                      0                                18                              301  
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_97.final.scaf         12                            12191                       8                                 0                      7                                4920                            37849
