All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_77.final.contigs
#Mis_misassemblies               264                                                                                   
#Mis_relocations                 244                                                                                   
#Mis_translocations              15                                                                                    
#Mis_inversions                  5                                                                                     
#Mis_misassembled contigs        264                                                                                   
Mis_Misassembled contigs length  61858                                                                                 
#Mis_local misassemblies         1                                                                                     
#mismatches                      2565                                                                                  
#indels                          39581                                                                                 
#Mis_short indels (<= 5 bp)      39581                                                                                 
#Mis_long indels (> 5 bp)        0                                                                                     
Indels length                    40692                                                                                 
