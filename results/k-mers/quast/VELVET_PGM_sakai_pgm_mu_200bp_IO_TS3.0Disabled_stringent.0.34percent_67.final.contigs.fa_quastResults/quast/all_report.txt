All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_67.final.contigs
#Contigs                                   1418                                                                                 
#Contigs (>= 0 bp)                         1674                                                                                 
#Contigs (>= 1000 bp)                      811                                                                                  
Largest contig                             60167                                                                                
Total length                               5336219                                                                              
Total length (>= 0 bp)                     5379534                                                                              
Total length (>= 1000 bp)                  5069810                                                                              
Reference length                           5594470                                                                              
N50                                        9043                                                                                 
NG50                                       8675                                                                                 
N75                                        4917                                                                                 
NG75                                       4312                                                                                 
L50                                        183                                                                                  
LG50                                       198                                                                                  
L75                                        380                                                                                  
LG75                                       422                                                                                  
#local misassemblies                       40                                                                                   
#misassemblies                             23                                                                                   
#misassembled contigs                      22                                                                                   
Misassembled contigs length                193867                                                                               
Misassemblies inter-contig overlap         1436                                                                                 
#unaligned contigs                         0 + 12 part                                                                          
Unaligned contigs length                   1444                                                                                 
#ambiguously mapped contigs                190                                                                                  
Extra bases in ambiguously mapped contigs  -83789                                                                               
#N's                                       1030                                                                                 
#N's per 100 kbp                           19.30                                                                                
Genome fraction (%)                        93.405                                                                               
Duplication ratio                          1.005                                                                                
#genes                                     4320 + 754 part                                                                      
#predicted genes (unique)                  6754                                                                                 
#predicted genes (>= 0 bp)                 6756                                                                                 
#predicted genes (>= 300 bp)               5013                                                                                 
#predicted genes (>= 1500 bp)              430                                                                                  
#predicted genes (>= 3000 bp)              27                                                                                   
#mismatches                                235                                                                                  
#indels                                    2992                                                                                 
Indels length                              4244                                                                                 
#mismatches per 100 kbp                    4.50                                                                                 
#indels per 100 kbp                        57.26                                                                                
GC (%)                                     50.29                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               99.357                                                                               
Largest alignment                          60167                                                                                
NA50                                       8817                                                                                 
NGA50                                      8477                                                                                 
NA75                                       4824                                                                                 
NGA75                                      4175                                                                                 
LA50                                       187                                                                                  
LGA50                                      202                                                                                  
LA75                                       387                                                                                  
LGA75                                      430                                                                                  
#Mis_misassemblies                         23                                                                                   
#Mis_relocations                           19                                                                                   
#Mis_translocations                        0                                                                                    
#Mis_inversions                            4                                                                                    
#Mis_misassembled contigs                  22                                                                                   
Mis_Misassembled contigs length            193867                                                                               
#Mis_local misassemblies                   40                                                                                   
#Mis_short indels (<= 5 bp)                2965                                                                                 
#Mis_long indels (> 5 bp)                  27                                                                                   
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           12                                                                                   
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            2                                                                                    
Una_Partially unaligned length             1444                                                                                 
GAGE_Contigs #                             1418                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            60167                                                                                
GAGE_N50                                   8675 COUNT: 198                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5336219                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               100247(1.79%)                                                                        
GAGE_Missing assembly bases                2163(0.04%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            6079                                                                                 
GAGE_Compressed reference bases            227162                                                                               
GAGE_Bad trim                              1154                                                                                 
GAGE_Avg idy                               99.95                                                                                
GAGE_SNPs                                  129                                                                                  
GAGE_Indels < 5bp                          2332                                                                                 
GAGE_Indels >= 5                           59                                                                                   
GAGE_Inversions                            1                                                                                    
GAGE_Relocation                            33                                                                                   
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1486                                                                                 
GAGE_Corrected assembly size               5330763                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    60186                                                                                
GAGE_Corrected N50                         7785 COUNT: 217                                                                      
