All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_189.final.contigs
#Mis_misassemblies               22                                                                                     
#Mis_relocations                 21                                                                                     
#Mis_translocations              1                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        22                                                                                     
Mis_Misassembled contigs length  72186                                                                                  
#Mis_local misassemblies         10                                                                                     
#mismatches                      104                                                                                    
#indels                          634                                                                                    
#Mis_short indels (<= 5 bp)      634                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    655                                                                                    
