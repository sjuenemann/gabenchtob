All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_121.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_121.final.contigs
GAGE_Contigs #                   681                                                                                      496                                                                             
GAGE_Min contig                  219                                                                                      241                                                                             
GAGE_Max contig                  99095                                                                                    215769                                                                          
GAGE_N50                         24857 COUNT: 69                                                                          50421 COUNT: 35                                                                 
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               5434893                                                                                  5456181                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     22543(0.40%)                                                                             22316(0.40%)                                                                    
GAGE_Missing assembly bases      5637(0.10%)                                                                              26294(0.48%)                                                                    
GAGE_Missing assembly contigs    2(0.29%)                                                                                 1(0.20%)                                                                        
GAGE_Duplicated reference bases  13885                                                                                    14659                                                                           
GAGE_Compressed reference bases  252934                                                                                   252935                                                                          
GAGE_Bad trim                    33                                                                                       874                                                                             
GAGE_Avg idy                     99.99                                                                                    99.99                                                                           
GAGE_SNPs                        91                                                                                       90                                                                              
GAGE_Indels < 5bp                16                                                                                       23                                                                              
GAGE_Indels >= 5                 3                                                                                        147                                                                             
GAGE_Inversions                  0                                                                                        6                                                                               
GAGE_Relocation                  3                                                                                        10                                                                              
GAGE_Translocation               0                                                                                        0                                                                               
GAGE_Corrected contig #          651                                                                                      646                                                                             
GAGE_Corrected assembly size     5417767                                                                                  5417979                                                                         
GAGE_Min correct contig          241                                                                                      241                                                                             
GAGE_Max correct contig          99095                                                                                    99095                                                                           
GAGE_Corrected N50               24201 COUNT: 70                                                                          24923 COUNT: 69                                                                 
