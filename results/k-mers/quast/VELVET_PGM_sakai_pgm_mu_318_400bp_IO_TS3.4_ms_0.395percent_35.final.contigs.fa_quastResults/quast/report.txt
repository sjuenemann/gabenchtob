All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_35.final.contigs
#Contigs (>= 0 bp)             29127                                                                      
#Contigs (>= 1000 bp)          14                                                                         
Total length (>= 0 bp)         4887789                                                                    
Total length (>= 1000 bp)      16315                                                                      
#Contigs                       7514                                                                       
Largest contig                 1585                                                                       
Total length                   2365212                                                                    
Reference length               5594470                                                                    
GC (%)                         51.65                                                                      
Reference GC (%)               50.48                                                                      
N50                            314                                                                        
NG50                           None                                                                       
N75                            245                                                                        
NG75                           None                                                                       
#misassemblies                 0                                                                          
#local misassemblies           0                                                                          
#unaligned contigs             0 + 2 part                                                                 
Unaligned contigs length       45                                                                         
Genome fraction (%)            41.902                                                                     
Duplication ratio              1.006                                                                      
#N's per 100 kbp               0.00                                                                       
#mismatches per 100 kbp        3.11                                                                       
#indels per 100 kbp            26.70                                                                      
#genes                         97 + 3677 part                                                             
#predicted genes (unique)      7787                                                                       
#predicted genes (>= 0 bp)     7787                                                                       
#predicted genes (>= 300 bp)   2299                                                                       
#predicted genes (>= 1500 bp)  0                                                                          
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              1585                                                                       
NA50                           313                                                                        
NGA50                          None                                                                       
NA75                           245                                                                        
NGA75                          None                                                                       
