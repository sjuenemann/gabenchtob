All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_115.final.contig
GAGE_Contigs #                   39038                                                                                
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  1746                                                                                 
GAGE_N50                         410 COUNT: 2861                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               11513940                                                                             
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     15215(0.54%)                                                                         
GAGE_Missing assembly bases      208657(1.81%)                                                                        
GAGE_Missing assembly contigs    321(0.82%)                                                                           
GAGE_Duplicated reference bases  7621153                                                                              
GAGE_Compressed reference bases  36998                                                                                
GAGE_Bad trim                    84395                                                                                
GAGE_Avg idy                     99.85                                                                                
GAGE_SNPs                        124                                                                                  
GAGE_Indels < 5bp                2960                                                                                 
GAGE_Indels >= 5                 4                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  0                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          13098                                                                                
GAGE_Corrected assembly size     3680307                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          1746                                                                                 
GAGE_Corrected N50               297 COUNT: 3217                                                                      
