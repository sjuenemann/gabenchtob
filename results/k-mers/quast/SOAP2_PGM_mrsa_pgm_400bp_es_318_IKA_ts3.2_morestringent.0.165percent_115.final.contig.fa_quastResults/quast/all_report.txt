All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_115.final.contig
#Contigs                                   39038                                                                                
#Contigs (>= 0 bp)                         69154                                                                                
#Contigs (>= 1000 bp)                      34                                                                                   
Largest contig                             1746                                                                                 
Total length                               11513940                                                                             
Total length (>= 0 bp)                     15891965                                                                             
Total length (>= 1000 bp)                  39485                                                                                
Reference length                           2813862                                                                              
N50                                        348                                                                                  
NG50                                       410                                                                                  
N75                                        227                                                                                  
NG75                                       396                                                                                  
L50                                        14468                                                                                
LG50                                       2861                                                                                 
L75                                        26026                                                                                
LG75                                       4607                                                                                 
#local misassemblies                       2                                                                                    
#misassemblies                             187                                                                                  
#misassembled contigs                      187                                                                                  
Misassembled contigs length                67474                                                                                
Misassemblies inter-contig overlap         387                                                                                  
#unaligned contigs                         759 + 351 part                                                                       
Unaligned contigs length                   315125                                                                               
#ambiguously mapped contigs                463                                                                                  
Extra bases in ambiguously mapped contigs  -116143                                                                              
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        98.324                                                                               
Duplication ratio                          4.006                                                                                
#genes                                     388 + 2322 part                                                                      
#predicted genes (unique)                  38477                                                                                
#predicted genes (>= 0 bp)                 39251                                                                                
#predicted genes (>= 300 bp)               1985                                                                                 
#predicted genes (>= 1500 bp)              1                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                    
#mismatches                                2100                                                                                 
#indels                                    30339                                                                                
Indels length                              31231                                                                                
#mismatches per 100 kbp                    75.90                                                                                
#indels per 100 kbp                        1096.57                                                                              
GC (%)                                     32.58                                                                                
Reference GC (%)                           32.81                                                                                
Average %IDY                               99.034                                                                               
Largest alignment                          1746                                                                                 
NA50                                       312                                                                                  
NGA50                                      408                                                                                  
NA75                                       226                                                                                  
NGA75                                      393                                                                                  
LA50                                       14644                                                                                
LGA50                                      2875                                                                                 
LA75                                       26812                                                                                
LGA75                                      4634                                                                                 
#Mis_misassemblies                         187                                                                                  
#Mis_relocations                           179                                                                                  
#Mis_translocations                        8                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  187                                                                                  
Mis_Misassembled contigs length            67474                                                                                
#Mis_local misassemblies                   2                                                                                    
#Mis_short indels (<= 5 bp)                30339                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                    
#Una_fully unaligned contigs               759                                                                                  
Una_Fully unaligned length                 291651                                                                               
#Una_partially unaligned contigs           351                                                                                  
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            2                                                                                    
Una_Partially unaligned length             23474                                                                                
GAGE_Contigs #                             39038                                                                                
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            1746                                                                                 
GAGE_N50                                   410 COUNT: 2861                                                                      
GAGE_Genome size                           2813862                                                                              
GAGE_Assembly size                         11513940                                                                             
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               15215(0.54%)                                                                         
GAGE_Missing assembly bases                208657(1.81%)                                                                        
GAGE_Missing assembly contigs              321(0.82%)                                                                           
GAGE_Duplicated reference bases            7621153                                                                              
GAGE_Compressed reference bases            36998                                                                                
GAGE_Bad trim                              84395                                                                                
GAGE_Avg idy                               99.85                                                                                
GAGE_SNPs                                  124                                                                                  
GAGE_Indels < 5bp                          2960                                                                                 
GAGE_Indels >= 5                           4                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            0                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    13098                                                                                
GAGE_Corrected assembly size               3680307                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    1746                                                                                 
GAGE_Corrected N50                         297 COUNT: 3217                                                                      
