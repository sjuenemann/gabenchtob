All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_57.final.contigs
#Contigs (>= 0 bp)             45061                                                                                 
#Contigs (>= 1000 bp)          0                                                                                     
Total length (>= 0 bp)         7074019                                                                               
Total length (>= 1000 bp)      0                                                                                     
#Contigs                       4130                                                                                  
Largest contig                 420                                                                                   
Total length                   931638                                                                                
Reference length               2813862                                                                               
GC (%)                         31.52                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            220                                                                                   
NG50                           None                                                                                  
N75                            208                                                                                   
NG75                           None                                                                                  
#misassemblies                 0                                                                                     
#local misassemblies           0                                                                                     
#unaligned contigs             4108 + 0 part                                                                         
Unaligned contigs length       926481                                                                                
Genome fraction (%)            0.182                                                                                 
Duplication ratio              1.008                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        175.92                                                                                
#indels per 100 kbp            390.93                                                                                
#genes                         1 + 16 part                                                                           
#predicted genes (unique)      2585                                                                                  
#predicted genes (>= 0 bp)     2585                                                                                  
#predicted genes (>= 300 bp)   0                                                                                     
#predicted genes (>= 1500 bp)  0                                                                                     
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              306                                                                                   
NA50                           None                                                                                  
NGA50                          None                                                                                  
NA75                           None                                                                                  
NGA75                          None                                                                                  
