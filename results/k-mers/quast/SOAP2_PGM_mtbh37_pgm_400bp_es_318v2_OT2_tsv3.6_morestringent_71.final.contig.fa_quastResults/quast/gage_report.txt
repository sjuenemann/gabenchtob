All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_71.final.contig
GAGE_Contigs #                   7428                                                                        
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  1211                                                                        
GAGE_N50                         0 COUNT: 0                                                                  
GAGE_Genome size                 4411532                                                                     
GAGE_Assembly size               2003558                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     2666361(60.44%)                                                             
GAGE_Missing assembly bases      53203(2.66%)                                                                
GAGE_Missing assembly contigs    154(2.07%)                                                                  
GAGE_Duplicated reference bases  94120                                                                       
GAGE_Compressed reference bases  19031                                                                       
GAGE_Bad trim                    14753                                                                       
GAGE_Avg idy                     98.92                                                                       
GAGE_SNPs                        641                                                                         
GAGE_Indels < 5bp                15489                                                                       
GAGE_Indels >= 5                 25                                                                          
GAGE_Inversions                  12                                                                          
GAGE_Relocation                  11                                                                          
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          6691                                                                        
GAGE_Corrected assembly size     1817834                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          1211                                                                        
GAGE_Corrected N50               0 COUNT: 0                                                                  
