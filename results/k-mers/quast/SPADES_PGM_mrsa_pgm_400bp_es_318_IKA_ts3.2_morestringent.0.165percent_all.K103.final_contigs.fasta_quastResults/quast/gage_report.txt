All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K103.final_contigs
GAGE_Contigs #                   9512                                                                                        
GAGE_Min contig                  200                                                                                         
GAGE_Max contig                  236830                                                                                      
GAGE_N50                         145120 COUNT: 8                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               5759993                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     1747(0.06%)                                                                                 
GAGE_Missing assembly bases      269012(4.67%)                                                                               
GAGE_Missing assembly contigs    632(6.64%)                                                                                  
GAGE_Duplicated reference bases  2704130                                                                                     
GAGE_Compressed reference bases  37019                                                                                       
GAGE_Bad trim                    66572                                                                                       
GAGE_Avg idy                     99.98                                                                                       
GAGE_SNPs                        47                                                                                          
GAGE_Indels < 5bp                304                                                                                         
GAGE_Indels >= 5                 8                                                                                           
GAGE_Inversions                  0                                                                                           
GAGE_Relocation                  2                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          141                                                                                         
GAGE_Corrected assembly size     2788396                                                                                     
GAGE_Min correct contig          200                                                                                         
GAGE_Max correct contig          207913                                                                                      
GAGE_Corrected N50               75657 COUNT: 10                                                                             
