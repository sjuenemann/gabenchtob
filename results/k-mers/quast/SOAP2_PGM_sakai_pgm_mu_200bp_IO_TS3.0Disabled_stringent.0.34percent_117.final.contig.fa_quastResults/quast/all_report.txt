All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_117.final.contig
#Contigs                                   16883                                                                               
#Contigs (>= 0 bp)                         20126                                                                               
#Contigs (>= 1000 bp)                      1814                                                                                
Largest contig                             5054                                                                                
Total length                               7791438                                                                             
Total length (>= 0 bp)                     8286026                                                                             
Total length (>= 1000 bp)                  3023426                                                                             
Reference length                           5594470                                                                             
N50                                        709                                                                                 
NG50                                       1086                                                                                
N75                                        234                                                                                 
NG75                                       602                                                                                 
L50                                        2844                                                                                
LG50                                       1596                                                                                
L75                                        8418                                                                                
LG75                                       3302                                                                                
#local misassemblies                       0                                                                                   
#misassemblies                             8                                                                                   
#misassembled contigs                      8                                                                                   
Misassembled contigs length                3537                                                                                
Misassemblies inter-contig overlap         70                                                                                  
#unaligned contigs                         0 + 12 part                                                                         
Unaligned contigs length                   481                                                                                 
#ambiguously mapped contigs                844                                                                                 
Extra bases in ambiguously mapped contigs  -216825                                                                             
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        93.414                                                                              
Duplication ratio                          1.449                                                                               
#genes                                     1879 + 3271 part                                                                    
#predicted genes (unique)                  20197                                                                               
#predicted genes (>= 0 bp)                 20585                                                                               
#predicted genes (>= 300 bp)               5993                                                                                
#predicted genes (>= 1500 bp)              127                                                                                 
#predicted genes (>= 3000 bp)              1                                                                                   
#mismatches                                81                                                                                  
#indels                                    2252                                                                                
Indels length                              2331                                                                                
#mismatches per 100 kbp                    1.55                                                                                
#indels per 100 kbp                        43.09                                                                               
GC (%)                                     50.80                                                                               
Reference GC (%)                           50.48                                                                               
Average %IDY                               99.561                                                                              
Largest alignment                          5054                                                                                
NA50                                       706                                                                                 
NGA50                                      1082                                                                                
NA75                                       233                                                                                 
NGA75                                      598                                                                                 
LA50                                       2849                                                                                
LGA50                                      1598                                                                                
LA75                                       8528                                                                                
LGA75                                      3311                                                                                
#Mis_misassemblies                         8                                                                                   
#Mis_relocations                           3                                                                                   
#Mis_translocations                        0                                                                                   
#Mis_inversions                            5                                                                                   
#Mis_misassembled contigs                  8                                                                                   
Mis_Misassembled contigs length            3537                                                                                
#Mis_local misassemblies                   0                                                                                   
#Mis_short indels (<= 5 bp)                2252                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                   
#Una_fully unaligned contigs               0                                                                                   
Una_Fully unaligned length                 0                                                                                   
#Una_partially unaligned contigs           12                                                                                  
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             481                                                                                 
GAGE_Contigs #                             16883                                                                               
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            5054                                                                                
GAGE_N50                                   1086 COUNT: 1596                                                                    
GAGE_Genome size                           5594470                                                                             
GAGE_Assembly size                         7791438                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               87530(1.56%)                                                                        
GAGE_Missing assembly bases                876(0.01%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                            
GAGE_Duplicated reference bases            2080831                                                                             
GAGE_Compressed reference bases            271888                                                                              
GAGE_Bad trim                              876                                                                                 
GAGE_Avg idy                               99.96                                                                               
GAGE_SNPs                                  47                                                                                  
GAGE_Indels < 5bp                          1553                                                                                
GAGE_Indels >= 5                           0                                                                                   
GAGE_Inversions                            0                                                                                   
GAGE_Relocation                            1                                                                                   
GAGE_Translocation                         0                                                                                   
GAGE_Corrected contig #                    7977                                                                                
GAGE_Corrected assembly size               5710575                                                                             
GAGE_Min correct contig                    200                                                                                 
GAGE_Max correct contig                    5055                                                                                
GAGE_Corrected N50                         1086 COUNT: 1596                                                                    
