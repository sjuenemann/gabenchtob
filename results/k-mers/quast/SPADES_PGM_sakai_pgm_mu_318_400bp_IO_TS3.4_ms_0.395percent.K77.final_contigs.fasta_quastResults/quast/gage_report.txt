All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent.K77.final_contigs
GAGE_Contigs #                   503                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  268249                                                                      
GAGE_N50                         105765 COUNT: 17                                                            
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               5419104                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     15101(0.27%)                                                                
GAGE_Missing assembly bases      29(0.00%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  68422                                                                       
GAGE_Compressed reference bases  282423                                                                      
GAGE_Bad trim                    29                                                                          
GAGE_Avg idy                     99.99                                                                       
GAGE_SNPs                        181                                                                         
GAGE_Indels < 5bp                344                                                                         
GAGE_Indels >= 5                 12                                                                          
GAGE_Inversions                  1                                                                           
GAGE_Relocation                  8                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          516                                                                         
GAGE_Corrected assembly size     5353579                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          224195                                                                      
GAGE_Corrected N50               86042 COUNT: 21                                                             
