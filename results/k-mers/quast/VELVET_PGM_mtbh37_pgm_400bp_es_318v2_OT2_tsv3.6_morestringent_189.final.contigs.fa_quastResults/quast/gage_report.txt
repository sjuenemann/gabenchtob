All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_189.final.contigs
GAGE_Contigs #                   2015                                                                           
GAGE_Min contig                  377                                                                            
GAGE_Max contig                  7676                                                                           
GAGE_N50                         1169 COUNT: 1030                                                               
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               2977746                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     1525321(34.58%)                                                                
GAGE_Missing assembly bases      580(0.02%)                                                                     
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  30307                                                                          
GAGE_Compressed reference bases  26208                                                                          
GAGE_Bad trim                    430                                                                            
GAGE_Avg idy                     99.94                                                                          
GAGE_SNPs                        49                                                                             
GAGE_Indels < 5bp                1616                                                                           
GAGE_Indels >= 5                 14                                                                             
GAGE_Inversions                  0                                                                              
GAGE_Relocation                  2                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          1988                                                                           
GAGE_Corrected assembly size     2949003                                                                        
GAGE_Min correct contig          248                                                                            
GAGE_Max correct contig          7679                                                                           
GAGE_Corrected N50               1152 COUNT: 1049                                                               
