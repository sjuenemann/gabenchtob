All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K57.final_contigs
#Mis_misassemblies               3                                                                                         
#Mis_relocations                 3                                                                                         
#Mis_translocations              0                                                                                         
#Mis_inversions                  0                                                                                         
#Mis_misassembled contigs        3                                                                                         
Mis_Misassembled contigs length  304065                                                                                    
#Mis_local misassemblies         12                                                                                        
#mismatches                      161                                                                                       
#indels                          1422                                                                                      
#Mis_short indels (<= 5 bp)      1421                                                                                      
#Mis_long indels (> 5 bp)        1                                                                                         
Indels length                    1475                                                                                      
