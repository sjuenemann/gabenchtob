All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_75.final.contig
#Contigs                                   7683                                                                        
#Contigs (>= 0 bp)                         191829                                                                      
#Contigs (>= 1000 bp)                      6                                                                           
Largest contig                             1159                                                                        
Total length                               2145235                                                                     
Total length (>= 0 bp)                     23660424                                                                    
Total length (>= 1000 bp)                  6601                                                                        
Reference length                           4411532                                                                     
N50                                        264                                                                         
NG50                                       None                                                                        
N75                                        236                                                                         
NG75                                       None                                                                        
L50                                        3093                                                                        
LG50                                       None                                                                        
L75                                        5252                                                                        
LG75                                       None                                                                        
#local misassemblies                       7                                                                           
#misassemblies                             24                                                                          
#misassembled contigs                      24                                                                          
Misassembled contigs length                6105                                                                        
Misassemblies inter-contig overlap         731                                                                         
#unaligned contigs                         525 + 31 part                                                               
Unaligned contigs length                   141636                                                                      
#ambiguously mapped contigs                21                                                                          
Extra bases in ambiguously mapped contigs  -5270                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        40.144                                                                      
Duplication ratio                          1.129                                                                       
#genes                                     55 + 3029 part                                                              
#predicted genes (unique)                  6294                                                                        
#predicted genes (>= 0 bp)                 6297                                                                        
#predicted genes (>= 300 bp)               978                                                                         
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                650                                                                         
#indels                                    14777                                                                       
Indels length                              15252                                                                       
#mismatches per 100 kbp                    36.70                                                                       
#indels per 100 kbp                        834.40                                                                      
GC (%)                                     66.10                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               98.966                                                                      
Largest alignment                          1159                                                                        
NA50                                       259                                                                         
NGA50                                      None                                                                        
NA75                                       230                                                                         
NGA75                                      None                                                                        
LA50                                       3126                                                                        
LGA50                                      None                                                                        
LA75                                       5333                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         24                                                                          
#Mis_relocations                           24                                                                          
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  24                                                                          
Mis_Misassembled contigs length            6105                                                                        
#Mis_local misassemblies                   7                                                                           
#Mis_short indels (<= 5 bp)                14776                                                                       
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               525                                                                         
Una_Fully unaligned length                 138687                                                                      
#Una_partially unaligned contigs           31                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             2949                                                                        
GAGE_Contigs #                             7683                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1159                                                                        
GAGE_N50                                   0 COUNT: 0                                                                  
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         2145235                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               2552925(57.87%)                                                             
GAGE_Missing assembly bases                42889(2.00%)                                                                
GAGE_Missing assembly contigs              110(1.43%)                                                                  
GAGE_Duplicated reference bases            124332                                                                      
GAGE_Compressed reference bases            22167                                                                       
GAGE_Bad trim                              14255                                                                       
GAGE_Avg idy                               99.00                                                                       
GAGE_SNPs                                  639                                                                         
GAGE_Indels < 5bp                          15183                                                                       
GAGE_Indels >= 5                           22                                                                          
GAGE_Inversions                            15                                                                          
GAGE_Relocation                            11                                                                          
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    6920                                                                        
GAGE_Corrected assembly size               1946835                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1159                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
