All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_201.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_201.final.contigs
#Mis_misassemblies               7                                                                                        15                                                                              
#Mis_relocations                 7                                                                                        15                                                                              
#Mis_translocations              0                                                                                        0                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        7                                                                                        13                                                                              
Mis_Misassembled contigs length  32863                                                                                    105068                                                                          
#Mis_local misassemblies         3                                                                                        584                                                                             
#mismatches                      282                                                                                      283                                                                             
#indels                          23                                                                                       710                                                                             
#Mis_short indels (<= 5 bp)      22                                                                                       682                                                                             
#Mis_long indels (> 5 bp)        1                                                                                        28                                                                              
Indels length                    31                                                                                       2260                                                                            
