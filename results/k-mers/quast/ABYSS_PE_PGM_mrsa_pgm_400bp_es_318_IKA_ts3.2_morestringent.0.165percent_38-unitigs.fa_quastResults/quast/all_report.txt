All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_38-unitigs
#Contigs                                   599                                                                               
#Contigs (>= 0 bp)                         7857                                                                              
#Contigs (>= 1000 bp)                      399                                                                               
Largest contig                             83589                                                                             
Total length                               2716740                                                                           
Total length (>= 0 bp)                     3138219                                                                           
Total length (>= 1000 bp)                  2626626                                                                           
Reference length                           2813862                                                                           
N50                                        10134                                                                             
NG50                                       9705                                                                              
N75                                        5062                                                                              
NG75                                       4569                                                                              
L50                                        76                                                                                
LG50                                       81                                                                                
L75                                        169                                                                               
LG75                                       184                                                                               
#local misassemblies                       3                                                                                 
#misassemblies                             1                                                                                 
#misassembled contigs                      1                                                                                 
Misassembled contigs length                4978                                                                              
Misassemblies inter-contig overlap         416                                                                               
#unaligned contigs                         0 + 0 part                                                                        
Unaligned contigs length                   0                                                                                 
#ambiguously mapped contigs                3                                                                                 
Extra bases in ambiguously mapped contigs  -1198                                                                             
#N's                                       0                                                                                 
#N's per 100 kbp                           0.00                                                                              
Genome fraction (%)                        96.424                                                                            
Duplication ratio                          1.001                                                                             
#genes                                     2295 + 326 part                                                                   
#predicted genes (unique)                  2902                                                                              
#predicted genes (>= 0 bp)                 2902                                                                              
#predicted genes (>= 300 bp)               2390                                                                              
#predicted genes (>= 1500 bp)              254                                                                               
#predicted genes (>= 3000 bp)              18                                                                                
#mismatches                                55                                                                                
#indels                                    88                                                                                
Indels length                              91                                                                                
#mismatches per 100 kbp                    2.03                                                                              
#indels per 100 kbp                        3.24                                                                              
GC (%)                                     32.58                                                                             
Reference GC (%)                           32.81                                                                             
Average %IDY                               99.951                                                                            
Largest alignment                          83589                                                                             
NA50                                       10134                                                                             
NGA50                                      9705                                                                              
NA75                                       5062                                                                              
NGA75                                      4565                                                                              
LA50                                       76                                                                                
LGA50                                      81                                                                                
LA75                                       169                                                                               
LGA75                                      184                                                                               
#Mis_misassemblies                         1                                                                                 
#Mis_relocations                           1                                                                                 
#Mis_translocations                        0                                                                                 
#Mis_inversions                            0                                                                                 
#Mis_misassembled contigs                  1                                                                                 
Mis_Misassembled contigs length            4978                                                                              
#Mis_local misassemblies                   3                                                                                 
#Mis_short indels (<= 5 bp)                88                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                 
#Una_fully unaligned contigs               0                                                                                 
Una_Fully unaligned length                 0                                                                                 
#Una_partially unaligned contigs           0                                                                                 
#Una_with misassembly                      0                                                                                 
#Una_both parts are significant            0                                                                                 
Una_Partially unaligned length             0                                                                                 
GAGE_Contigs #                             599                                                                               
GAGE_Min contig                            201                                                                               
GAGE_Max contig                            83589                                                                             
GAGE_N50                                   9705 COUNT: 81                                                                    
GAGE_Genome size                           2813862                                                                           
GAGE_Assembly size                         2716740                                                                           
GAGE_Chaff bases                           0                                                                                 
GAGE_Missing reference bases               95555(3.40%)                                                                      
GAGE_Missing assembly bases                7(0.00%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                          
GAGE_Duplicated reference bases            0                                                                                 
GAGE_Compressed reference bases            4335                                                                              
GAGE_Bad trim                              7                                                                                 
GAGE_Avg idy                               99.99                                                                             
GAGE_SNPs                                  43                                                                                
GAGE_Indels < 5bp                          84                                                                                
GAGE_Indels >= 5                           3                                                                                 
GAGE_Inversions                            0                                                                                 
GAGE_Relocation                            1                                                                                 
GAGE_Translocation                         0                                                                                 
GAGE_Corrected contig #                    601                                                                               
GAGE_Corrected assembly size               2716914                                                                           
GAGE_Min correct contig                    201                                                                               
GAGE_Max correct contig                    83591                                                                             
GAGE_Corrected N50                         9693 COUNT: 81                                                                    
