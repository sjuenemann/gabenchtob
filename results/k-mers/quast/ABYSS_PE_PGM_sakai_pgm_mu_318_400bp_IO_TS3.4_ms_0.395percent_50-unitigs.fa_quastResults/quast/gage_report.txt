All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_50-unitigs
GAGE_Contigs #                   1154                                                                   
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  41337                                                                  
GAGE_N50                         11168 COUNT: 150                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5225705                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     272363(4.87%)                                                          
GAGE_Missing assembly bases      58(0.00%)                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  688                                                                    
GAGE_Compressed reference bases  110177                                                                 
GAGE_Bad trim                    56                                                                     
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        92                                                                     
GAGE_Indels < 5bp                230                                                                    
GAGE_Indels >= 5                 3                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  3                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          1155                                                                   
GAGE_Corrected assembly size     5225019                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          41338                                                                  
GAGE_Corrected N50               11071 COUNT: 151                                                       
