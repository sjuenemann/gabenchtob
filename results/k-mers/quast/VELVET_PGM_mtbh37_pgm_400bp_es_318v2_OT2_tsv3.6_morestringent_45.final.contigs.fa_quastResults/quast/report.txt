All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_45.final.contigs
#Contigs (>= 0 bp)             15442                                                                         
#Contigs (>= 1000 bp)          210                                                                           
Total length (>= 0 bp)         4010152                                                                       
Total length (>= 1000 bp)      273167                                                                        
#Contigs                       7115                                                                          
Largest contig                 2478                                                                          
Total length                   2897965                                                                       
Reference length               4411532                                                                       
GC (%)                         66.54                                                                         
Reference GC (%)               65.61                                                                         
N50                            437                                                                           
NG50                           293                                                                           
N75                            298                                                                           
NG75                           None                                                                          
#misassemblies                 0                                                                             
#local misassemblies           1                                                                             
#unaligned contigs             1 + 9 part                                                                    
Unaligned contigs length       706                                                                           
Genome fraction (%)            65.025                                                                        
Duplication ratio              1.008                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        5.44                                                                          
#indels per 100 kbp            58.22                                                                         
#genes                         249 + 3381 part                                                               
#predicted genes (unique)      7931                                                                          
#predicted genes (>= 0 bp)     7931                                                                          
#predicted genes (>= 300 bp)   3179                                                                          
#predicted genes (>= 1500 bp)  5                                                                             
#predicted genes (>= 3000 bp)  0                                                                             
Largest alignment              2478                                                                          
NA50                           437                                                                           
NGA50                          293                                                                           
NA75                           297                                                                           
NGA75                          None                                                                          
