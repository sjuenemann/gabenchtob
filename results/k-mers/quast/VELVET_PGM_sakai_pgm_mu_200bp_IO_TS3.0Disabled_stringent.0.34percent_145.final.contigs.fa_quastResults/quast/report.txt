All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_145.final.contigs
#Contigs (>= 0 bp)             3765                                                                                  
#Contigs (>= 1000 bp)          1791                                                                                  
Total length (>= 0 bp)         4709258                                                                               
Total length (>= 1000 bp)      3403677                                                                               
#Contigs                       3765                                                                                  
Largest contig                 11044                                                                                 
Total length                   4709258                                                                               
Reference length               5594470                                                                               
GC (%)                         51.04                                                                                 
Reference GC (%)               50.48                                                                                 
N50                            1547                                                                                  
NG50                           1304                                                                                  
N75                            943                                                                                   
NG75                           649                                                                                   
#misassemblies                 25                                                                                    
#local misassemblies           12                                                                                    
#unaligned contigs             0 + 60 part                                                                           
Unaligned contigs length       1781                                                                                  
Genome fraction (%)            80.268                                                                                
Duplication ratio              1.033                                                                                 
#N's per 100 kbp               5.52                                                                                  
#mismatches per 100 kbp        9.80                                                                                  
#indels per 100 kbp            65.87                                                                                 
#genes                         2146 + 2643 part                                                                      
#predicted genes (unique)      7517                                                                                  
#predicted genes (>= 0 bp)     7528                                                                                  
#predicted genes (>= 300 bp)   5158                                                                                  
#predicted genes (>= 1500 bp)  194                                                                                   
#predicted genes (>= 3000 bp)  2                                                                                     
Largest alignment              11044                                                                                 
NA50                           1535                                                                                  
NGA50                          1293                                                                                  
NA75                           923                                                                                   
NGA75                          633                                                                                   
