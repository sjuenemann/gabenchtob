All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_121.final.contigs
#Contigs                                   2956                                                                                  
#Contigs (>= 0 bp)                         2956                                                                                  
#Contigs (>= 1000 bp)                      1742                                                                                  
Largest contig                             14694                                                                                 
Total length                               5142255                                                                               
Total length (>= 0 bp)                     5142255                                                                               
Total length (>= 1000 bp)                  4431981                                                                               
Reference length                           5594470                                                                               
N50                                        2602                                                                                  
NG50                                       2367                                                                                  
N75                                        1446                                                                                  
NG75                                       1186                                                                                  
L50                                        609                                                                                   
LG50                                       700                                                                                   
L75                                        1268                                                                                  
LG75                                       1527                                                                                  
#local misassemblies                       92                                                                                    
#misassemblies                             105                                                                                   
#misassembled contigs                      98                                                                                    
Misassembled contigs length                356373                                                                                
Misassemblies inter-contig overlap         1703                                                                                  
#unaligned contigs                         0 + 16 part                                                                           
Unaligned contigs length                   665                                                                                   
#ambiguously mapped contigs                228                                                                                   
Extra bases in ambiguously mapped contigs  -88259                                                                                
#N's                                       1840                                                                                  
#N's per 100 kbp                           35.78                                                                                 
Genome fraction (%)                        87.899                                                                                
Duplication ratio                          1.028                                                                                 
#genes                                     2960 + 2032 part                                                                      
#predicted genes (unique)                  7598                                                                                  
#predicted genes (>= 0 bp)                 7609                                                                                  
#predicted genes (>= 300 bp)               5274                                                                                  
#predicted genes (>= 1500 bp)              311                                                                                   
#predicted genes (>= 3000 bp)              9                                                                                     
#mismatches                                196                                                                                   
#indels                                    2000                                                                                  
Indels length                              2059                                                                                  
#mismatches per 100 kbp                    3.99                                                                                  
#indels per 100 kbp                        40.67                                                                                 
GC (%)                                     50.65                                                                                 
Reference GC (%)                           50.48                                                                                 
Average %IDY                               99.341                                                                                
Largest alignment                          14694                                                                                 
NA50                                       2552                                                                                  
NGA50                                      2324                                                                                  
NA75                                       1409                                                                                  
NGA75                                      1162                                                                                  
LA50                                       620                                                                                   
LGA50                                      713                                                                                   
LA75                                       1296                                                                                  
LGA75                                      1561                                                                                  
#Mis_misassemblies                         105                                                                                   
#Mis_relocations                           102                                                                                   
#Mis_translocations                        0                                                                                     
#Mis_inversions                            3                                                                                     
#Mis_misassembled contigs                  98                                                                                    
Mis_Misassembled contigs length            356373                                                                                
#Mis_local misassemblies                   92                                                                                    
#Mis_short indels (<= 5 bp)                2000                                                                                  
#Mis_long indels (> 5 bp)                  0                                                                                     
#Una_fully unaligned contigs               0                                                                                     
Una_Fully unaligned length                 0                                                                                     
#Una_partially unaligned contigs           16                                                                                    
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             665                                                                                   
GAGE_Contigs #                             2956                                                                                  
GAGE_Min contig                            241                                                                                   
GAGE_Max contig                            14694                                                                                 
GAGE_N50                                   2367 COUNT: 700                                                                       
GAGE_Genome size                           5594470                                                                               
GAGE_Assembly size                         5142255                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               424573(7.59%)                                                                         
GAGE_Missing assembly bases                3478(0.07%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                              
GAGE_Duplicated reference bases            40016                                                                                 
GAGE_Compressed reference bases            253716                                                                                
GAGE_Bad trim                              1968                                                                                  
GAGE_Avg idy                               99.95                                                                                 
GAGE_SNPs                                  130                                                                                   
GAGE_Indels < 5bp                          1848                                                                                  
GAGE_Indels >= 5                           63                                                                                    
GAGE_Inversions                            5                                                                                     
GAGE_Relocation                            22                                                                                    
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    2988                                                                                  
GAGE_Corrected assembly size               5102880                                                                               
GAGE_Min correct contig                    214                                                                                   
GAGE_Max correct contig                    14698                                                                                 
GAGE_Corrected N50                         2251 COUNT: 740                                                                       
