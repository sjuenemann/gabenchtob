All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_51.final.contig
#Mis_misassemblies               1                                                                                  
#Mis_relocations                 1                                                                                  
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        1                                                                                  
Mis_Misassembled contigs length  406                                                                                
#Mis_local misassemblies         0                                                                                  
#mismatches                      31                                                                                 
#indels                          347                                                                                
#Mis_short indels (<= 5 bp)      347                                                                                
#Mis_long indels (> 5 bp)        0                                                                                  
Indels length                    359                                                                                
