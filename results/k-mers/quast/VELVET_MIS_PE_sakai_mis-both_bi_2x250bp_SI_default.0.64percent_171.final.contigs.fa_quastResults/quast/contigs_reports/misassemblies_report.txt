All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_171.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_171.final.contigs
#Mis_misassemblies               6                                                                                        13                                                                              
#Mis_relocations                 5                                                                                        12                                                                              
#Mis_translocations              1                                                                                        1                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        6                                                                                        10                                                                              
Mis_Misassembled contigs length  100401                                                                                   273981                                                                          
#Mis_local misassemblies         3                                                                                        119                                                                             
#mismatches                      119                                                                                      183                                                                             
#indels                          20                                                                                       303                                                                             
#Mis_short indels (<= 5 bp)      20                                                                                       291                                                                             
#Mis_long indels (> 5 bp)        0                                                                                        12                                                                              
Indels length                    20                                                                                       806                                                                             
