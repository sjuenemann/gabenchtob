reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_105.final.scaf_broken  | 98.2509024076       | 1.04770612569     | 203         | 3297      | 767       | None      | None      |
  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_105.final.scaf  | 98.4124109266       | 1.05604866526     | 150         | 3378      | 688       | None      | None      |
