All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_121.final.contig
#Contigs (>= 0 bp)             90163                                                                     
#Contigs (>= 1000 bp)          188                                                                       
Total length (>= 0 bp)         20084039                                                                  
Total length (>= 1000 bp)      231431                                                                    
#Contigs                       53366                                                                     
Largest contig                 2964                                                                      
Total length                   14453087                                                                  
Reference length               5594470                                                                   
GC (%)                         50.51                                                                     
Reference GC (%)               50.48                                                                     
N50                            240                                                                       
NG50                           376                                                                       
N75                            237                                                                       
NG75                           265                                                                       
#misassemblies                 129                                                                       
#local misassemblies           2                                                                         
#unaligned contigs             28 + 26 part                                                              
Unaligned contigs length       12302                                                                     
Genome fraction (%)            94.047                                                                    
Duplication ratio              2.671                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        5.26                                                                      
#indels per 100 kbp            192.95                                                                    
#genes                         556 + 4620 part                                                           
#predicted genes (unique)      52729                                                                     
#predicted genes (>= 0 bp)     53957                                                                     
#predicted genes (>= 300 bp)   4542                                                                      
#predicted genes (>= 1500 bp)  1                                                                         
#predicted genes (>= 3000 bp)  0                                                                         
Largest alignment              2964                                                                      
NA50                           240                                                                       
NGA50                          374                                                                       
NA75                           237                                                                       
NGA75                          260                                                                       
