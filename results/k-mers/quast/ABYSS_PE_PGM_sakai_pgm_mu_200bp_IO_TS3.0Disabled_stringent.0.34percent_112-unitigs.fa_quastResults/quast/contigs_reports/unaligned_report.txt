All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_112-unitigs
#Una_fully unaligned contigs      0                                                                                 
Una_Fully unaligned length        0                                                                                 
#Una_partially unaligned contigs  4                                                                                 
#Una_with misassembly             0                                                                                 
#Una_both parts are significant   0                                                                                 
Una_Partially unaligned length    209                                                                               
#N's                              0                                                                                 
