All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_112-unitigs
#Contigs (>= 0 bp)             3639                                                                              
#Contigs (>= 1000 bp)          813                                                                               
Total length (>= 0 bp)         5766910                                                                           
Total length (>= 1000 bp)      5084483                                                                           
#Contigs                       2557                                                                              
Largest contig                 40568                                                                             
Total length                   5606703                                                                           
Reference length               5594470                                                                           
GC (%)                         50.41                                                                             
Reference GC (%)               50.48                                                                             
N50                            8786                                                                              
NG50                           8786                                                                              
N75                            4174                                                                              
NG75                           4199                                                                              
#misassemblies                 7                                                                                 
#local misassemblies           1                                                                                 
#unaligned contigs             0 + 4 part                                                                        
Unaligned contigs length       209                                                                               
Genome fraction (%)            94.394                                                                            
Duplication ratio              1.032                                                                             
#N's per 100 kbp               0.00                                                                              
#mismatches per 100 kbp        0.97                                                                              
#indels per 100 kbp            36.04                                                                             
#genes                         4343 + 838 part                                                                   
#predicted genes (unique)      7776                                                                              
#predicted genes (>= 0 bp)     7814                                                                              
#predicted genes (>= 300 bp)   5020                                                                              
#predicted genes (>= 1500 bp)  434                                                                               
#predicted genes (>= 3000 bp)  28                                                                                
Largest alignment              40568                                                                             
NA50                           8786                                                                              
NGA50                          8786                                                                              
NA75                           4174                                                                              
NGA75                          4199                                                                              
