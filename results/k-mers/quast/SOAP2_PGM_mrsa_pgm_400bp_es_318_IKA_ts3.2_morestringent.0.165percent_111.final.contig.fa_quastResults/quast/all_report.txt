All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_111.final.contig
#Contigs                                   40831                                                                                
#Contigs (>= 0 bp)                         77465                                                                                
#Contigs (>= 1000 bp)                      13                                                                                   
Largest contig                             1326                                                                                 
Total length                               11732645                                                                             
Total length (>= 0 bp)                     16948534                                                                             
Total length (>= 1000 bp)                  14321                                                                                
Reference length                           2813862                                                                              
N50                                        337                                                                                  
NG50                                       402                                                                                  
N75                                        219                                                                                  
NG75                                       388                                                                                  
L50                                        15260                                                                                
LG50                                       3040                                                                                 
L75                                        27216                                                                                
LG75                                       4821                                                                                 
#local misassemblies                       1                                                                                    
#misassemblies                             202                                                                                  
#misassembled contigs                      202                                                                                  
Misassembled contigs length                71329                                                                                
Misassemblies inter-contig overlap         255                                                                                  
#unaligned contigs                         913 + 373 part                                                                       
Unaligned contigs length                   368049                                                                               
#ambiguously mapped contigs                471                                                                                  
Extra bases in ambiguously mapped contigs  -116280                                                                              
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        98.363                                                                               
Duplication ratio                          4.064                                                                                
#genes                                     355 + 2357 part                                                                      
#predicted genes (unique)                  39533                                                                                
#predicted genes (>= 0 bp)                 40212                                                                                
#predicted genes (>= 300 bp)               1722                                                                                 
#predicted genes (>= 1500 bp)              0                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                    
#mismatches                                2250                                                                                 
#indels                                    33374                                                                                
Indels length                              34362                                                                                
#mismatches per 100 kbp                    81.29                                                                                
#indels per 100 kbp                        1205.79                                                                              
GC (%)                                     32.54                                                                                
Reference GC (%)                           32.81                                                                                
Average %IDY                               98.976                                                                               
Largest alignment                          1326                                                                                 
NA50                                       315                                                                                  
NGA50                                      399                                                                                  
NA75                                       219                                                                                  
NGA75                                      384                                                                                  
LA50                                       15451                                                                                
LGA50                                      3058                                                                                 
LA75                                       28160                                                                                
LGA75                                      4855                                                                                 
#Mis_misassemblies                         202                                                                                  
#Mis_relocations                           192                                                                                  
#Mis_translocations                        9                                                                                    
#Mis_inversions                            1                                                                                    
#Mis_misassembled contigs                  202                                                                                  
Mis_Misassembled contigs length            71329                                                                                
#Mis_local misassemblies                   1                                                                                    
#Mis_short indels (<= 5 bp)                33373                                                                                
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               913                                                                                  
Una_Fully unaligned length                 342814                                                                               
#Una_partially unaligned contigs           373                                                                                  
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            1                                                                                    
Una_Partially unaligned length             25235                                                                                
GAGE_Contigs #                             40831                                                                                
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            1326                                                                                 
GAGE_N50                                   402 COUNT: 3040                                                                      
GAGE_Genome size                           2813862                                                                              
GAGE_Assembly size                         11732645                                                                             
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               15022(0.53%)                                                                         
GAGE_Missing assembly bases                238009(2.03%)                                                                        
GAGE_Missing assembly contigs              381(0.93%)                                                                           
GAGE_Duplicated reference bases            7782575                                                                              
GAGE_Compressed reference bases            37858                                                                                
GAGE_Bad trim                              94057                                                                                
GAGE_Avg idy                               99.80                                                                                
GAGE_SNPs                                  145                                                                                  
GAGE_Indels < 5bp                          4110                                                                                 
GAGE_Indels >= 5                           1                                                                                    
GAGE_Inversions                            1                                                                                    
GAGE_Relocation                            1                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    13904                                                                                
GAGE_Corrected assembly size               3706822                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    1326                                                                                 
GAGE_Corrected N50                         277 COUNT: 3548                                                                      
