All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_121.final.contigs
#Contigs (>= 0 bp)             23824                                                                       
#Contigs (>= 1000 bp)          0                                                                           
Total length (>= 0 bp)         6522310                                                                     
Total length (>= 1000 bp)      0                                                                           
#Contigs                       23824                                                                       
Largest contig                 881                                                                         
Total length                   6522310                                                                     
Reference length               5594470                                                                     
GC (%)                         50.26                                                                       
Reference GC (%)               50.48                                                                       
N50                            265                                                                         
NG50                           273                                                                         
N75                            244                                                                         
NG75                           251                                                                         
#misassemblies                 758                                                                         
#local misassemblies           3                                                                           
#unaligned contigs             12 + 22 part                                                                
Unaligned contigs length       5972                                                                        
Genome fraction (%)            64.847                                                                      
Duplication ratio              1.734                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        74.01                                                                       
#indels per 100 kbp            780.21                                                                      
#genes                         165 + 4591 part                                                             
#predicted genes (unique)      24382                                                                       
#predicted genes (>= 0 bp)     24461                                                                       
#predicted genes (>= 300 bp)   490                                                                         
#predicted genes (>= 1500 bp)  0                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              881                                                                         
NA50                           261                                                                         
NGA50                          269                                                                         
NA75                           241                                                                         
NGA75                          248                                                                         
