All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_29.final.contig
#Contigs                                   3425                                                                               
#Contigs (>= 0 bp)                         274538                                                                             
#Contigs (>= 1000 bp)                      0                                                                                  
Largest contig                             830                                                                                
Total length                               907934                                                                             
Total length (>= 0 bp)                     16237347                                                                           
Total length (>= 1000 bp)                  0                                                                                  
Reference length                           5594470                                                                            
N50                                        258                                                                                
NG50                                       None                                                                               
N75                                        223                                                                                
NG75                                       None                                                                               
L50                                        1399                                                                               
LG50                                       None                                                                               
L75                                        2348                                                                               
LG75                                       None                                                                               
#local misassemblies                       0                                                                                  
#misassemblies                             0                                                                                  
#misassembled contigs                      0                                                                                  
Misassembled contigs length                0                                                                                  
Misassemblies inter-contig overlap         0                                                                                  
#unaligned contigs                         8 + 3 part                                                                         
Unaligned contigs length                   1979                                                                               
#ambiguously mapped contigs                1                                                                                  
Extra bases in ambiguously mapped contigs  -327                                                                               
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        16.177                                                                             
Duplication ratio                          1.001                                                                              
#genes                                     26 + 2307 part                                                                     
#predicted genes (unique)                  3416                                                                               
#predicted genes (>= 0 bp)                 3416                                                                               
#predicted genes (>= 300 bp)               517                                                                                
#predicted genes (>= 1500 bp)              0                                                                                  
#predicted genes (>= 3000 bp)              0                                                                                  
#mismatches                                10                                                                                 
#indels                                    169                                                                                
Indels length                              173                                                                                
#mismatches per 100 kbp                    1.10                                                                               
#indels per 100 kbp                        18.67                                                                              
GC (%)                                     50.59                                                                              
Reference GC (%)                           50.48                                                                              
Average %IDY                               99.979                                                                             
Largest alignment                          830                                                                                
NA50                                       258                                                                                
NGA50                                      None                                                                               
NA75                                       223                                                                                
NGA75                                      None                                                                               
LA50                                       1399                                                                               
LGA50                                      None                                                                               
LA75                                       2349                                                                               
LGA75                                      None                                                                               
#Mis_misassemblies                         0                                                                                  
#Mis_relocations                           0                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  0                                                                                  
Mis_Misassembled contigs length            0                                                                                  
#Mis_local misassemblies                   0                                                                                  
#Mis_short indels (<= 5 bp)                169                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               8                                                                                  
Una_Fully unaligned length                 1814                                                                               
#Una_partially unaligned contigs           3                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             165                                                                                
GAGE_Contigs #                             3425                                                                               
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            830                                                                                
GAGE_N50                                   0 COUNT: 0                                                                         
GAGE_Genome size                           5594470                                                                            
GAGE_Assembly size                         907934                                                                             
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               4688842(83.81%)                                                                    
GAGE_Missing assembly bases                2057(0.23%)                                                                        
GAGE_Missing assembly contigs              8(0.23%)                                                                           
GAGE_Duplicated reference bases            0                                                                                  
GAGE_Compressed reference bases            327                                                                                
GAGE_Bad trim                              243                                                                                
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  10                                                                                 
GAGE_Indels < 5bp                          169                                                                                
GAGE_Indels >= 5                           0                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            0                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    3414                                                                               
GAGE_Corrected assembly size               905452                                                                             
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    830                                                                                
GAGE_Corrected N50                         0 COUNT: 0                                                                         
