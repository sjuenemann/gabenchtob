All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_96-unitigs
#Contigs (>= 0 bp)             10396                                                                     
#Contigs (>= 1000 bp)          1366                                                                      
Total length (>= 0 bp)         5444796                                                                   
Total length (>= 1000 bp)      3518481                                                                   
#Contigs                       2560                                                                      
Largest contig                 14671                                                                     
Total length                   4132847                                                                   
Reference length               4411532                                                                   
GC (%)                         65.33                                                                     
Reference GC (%)               65.61                                                                     
N50                            2576                                                                      
NG50                           2388                                                                      
N75                            1436                                                                      
NG75                           1221                                                                      
#misassemblies                 2                                                                         
#local misassemblies           12                                                                        
#unaligned contigs             2 + 1 part                                                                
Unaligned contigs length       823                                                                       
Genome fraction (%)            92.883                                                                    
Duplication ratio              1.006                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        3.17                                                                      
#indels per 100 kbp            23.21                                                                     
#genes                         2299 + 1727 part                                                          
#predicted genes (unique)      5966                                                                      
#predicted genes (>= 0 bp)     5970                                                                      
#predicted genes (>= 300 bp)   4210                                                                      
#predicted genes (>= 1500 bp)  262                                                                       
#predicted genes (>= 3000 bp)  16                                                                        
Largest alignment              14671                                                                     
NA50                           2576                                                                      
NGA50                          2388                                                                      
NA75                           1436                                                                      
NGA75                          1221                                                                      
