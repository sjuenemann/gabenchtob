All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_159.final.contigs
#Mis_misassemblies               54                                                                                     
#Mis_relocations                 51                                                                                     
#Mis_translocations              3                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        54                                                                                     
Mis_Misassembled contigs length  47613                                                                                  
#Mis_local misassemblies         0                                                                                      
#mismatches                      46                                                                                     
#indels                          649                                                                                    
#Mis_short indels (<= 5 bp)      649                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    666                                                                                    
