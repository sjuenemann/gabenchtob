All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_81.final.contig
GAGE_Contigs #                   12525                                                                    
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  1216                                                                     
GAGE_N50                         244 COUNT: 9228                                                          
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               3524960                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     2616459(46.77%)                                                          
GAGE_Missing assembly bases      48173(1.37%)                                                             
GAGE_Missing assembly contigs    109(0.87%)                                                               
GAGE_Duplicated reference bases  336833                                                                   
GAGE_Compressed reference bases  139845                                                                   
GAGE_Bad trim                    16656                                                                    
GAGE_Avg idy                     99.17                                                                    
GAGE_SNPs                        701                                                                      
GAGE_Indels < 5bp                19278                                                                    
GAGE_Indels >= 5                 13                                                                       
GAGE_Inversions                  44                                                                       
GAGE_Relocation                  24                                                                       
GAGE_Translocation               7                                                                        
GAGE_Corrected contig #          10852                                                                    
GAGE_Corrected assembly size     3073991                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          1216                                                                     
GAGE_Corrected N50               217 COUNT: 9524                                                          
