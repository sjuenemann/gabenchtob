All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_111.final.contig
GAGE_Contigs #                   55657                                                                     
GAGE_Min contig                  200                                                                       
GAGE_Max contig                  2386                                                                      
GAGE_N50                         342 COUNT: 6092                                                           
GAGE_Genome size                 5594470                                                                   
GAGE_Assembly size               13956839                                                                  
GAGE_Chaff bases                 0                                                                         
GAGE_Missing reference bases     67825(1.21%)                                                              
GAGE_Missing assembly bases      13827(0.10%)                                                              
GAGE_Missing assembly contigs    20(0.04%)                                                                 
GAGE_Duplicated reference bases  6919715                                                                   
GAGE_Compressed reference bases  262143                                                                    
GAGE_Bad trim                    6831                                                                      
GAGE_Avg idy                     99.89                                                                     
GAGE_SNPs                        77                                                                        
GAGE_Indels < 5bp                4404                                                                      
GAGE_Indels >= 5                 3                                                                         
GAGE_Inversions                  2                                                                         
GAGE_Relocation                  2                                                                         
GAGE_Translocation               0                                                                         
GAGE_Corrected contig #          25748                                                                     
GAGE_Corrected assembly size     7005207                                                                   
GAGE_Min correct contig          200                                                                       
GAGE_Max correct contig          2386                                                                      
GAGE_Corrected N50               265 COUNT: 6592                                                           
