All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_211.final.contigs
#Contigs                                   1109                                                                                   
#Contigs (>= 0 bp)                         1109                                                                                   
#Contigs (>= 1000 bp)                      834                                                                                    
Largest contig                             12663                                                                                  
Total length                               2273187                                                                                
Total length (>= 0 bp)                     2273187                                                                                
Total length (>= 1000 bp)                  2047358                                                                                
Reference length                           2813862                                                                                
N50                                        2524                                                                                   
NG50                                       1993                                                                                   
N75                                        1486                                                                                   
NG75                                       916                                                                                    
L50                                        266                                                                                    
LG50                                       385                                                                                    
L75                                        556                                                                                    
LG75                                       900                                                                                    
#local misassemblies                       1                                                                                      
#misassemblies                             7                                                                                      
#misassembled contigs                      7                                                                                      
Misassembled contigs length                36553                                                                                  
Misassemblies inter-contig overlap         2523                                                                                   
#unaligned contigs                         0 + 1 part                                                                             
Unaligned contigs length                   14                                                                                     
#ambiguously mapped contigs                3                                                                                      
Extra bases in ambiguously mapped contigs  -1734                                                                                  
#N's                                       60                                                                                     
#N's per 100 kbp                           2.64                                                                                   
Genome fraction (%)                        79.017                                                                                 
Duplication ratio                          1.023                                                                                  
#genes                                     1375 + 937 part                                                                        
#predicted genes (unique)                  2929                                                                                   
#predicted genes (>= 0 bp)                 2930                                                                                   
#predicted genes (>= 300 bp)               2143                                                                                   
#predicted genes (>= 1500 bp)              148                                                                                    
#predicted genes (>= 3000 bp)              17                                                                                     
#mismatches                                109                                                                                    
#indels                                    840                                                                                    
Indels length                              855                                                                                    
#mismatches per 100 kbp                    4.90                                                                                   
#indels per 100 kbp                        37.78                                                                                  
GC (%)                                     32.98                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.573                                                                                 
Largest alignment                          12663                                                                                  
NA50                                       2517                                                                                   
NGA50                                      1980                                                                                   
NA75                                       1486                                                                                   
NGA75                                      913                                                                                    
LA50                                       269                                                                                    
LGA50                                      388                                                                                    
LA75                                       559                                                                                    
LGA75                                      905                                                                                    
#Mis_misassemblies                         7                                                                                      
#Mis_relocations                           7                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  7                                                                                      
Mis_Misassembled contigs length            36553                                                                                  
#Mis_local misassemblies                   1                                                                                      
#Mis_short indels (<= 5 bp)                840                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           1                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             14                                                                                     
GAGE_Contigs #                             1109                                                                                   
GAGE_Min contig                            421                                                                                    
GAGE_Max contig                            12663                                                                                  
GAGE_N50                                   1993 COUNT: 385                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2273187                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               576483(20.49%)                                                                         
GAGE_Missing assembly bases                251(0.01%)                                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            2088                                                                                   
GAGE_Compressed reference bases            17794                                                                                  
GAGE_Bad trim                              241                                                                                    
GAGE_Avg idy                               99.95                                                                                  
GAGE_SNPs                                  63                                                                                     
GAGE_Indels < 5bp                          748                                                                                    
GAGE_Indels >= 5                           0                                                                                      
GAGE_Inversions                            0                                                                                      
GAGE_Relocation                            2                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    1111                                                                                   
GAGE_Corrected assembly size               2273967                                                                                
GAGE_Min correct contig                    421                                                                                    
GAGE_Max correct contig                    12662                                                                                  
GAGE_Corrected N50                         1986 COUNT: 387                                                                        
