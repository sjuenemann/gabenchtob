All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_97.final.contigs
#Mis_misassemblies               928                                                                        
#Mis_relocations                 861                                                                        
#Mis_translocations              66                                                                         
#Mis_inversions                  1                                                                          
#Mis_misassembled contigs        928                                                                        
Mis_Misassembled contigs length  227868                                                                     
#Mis_local misassemblies         5                                                                          
#mismatches                      1607                                                                       
#indels                          41517                                                                      
#Mis_short indels (<= 5 bp)      41516                                                                      
#Mis_long indels (> 5 bp)        1                                                                          
Indels length                    42141                                                                      
