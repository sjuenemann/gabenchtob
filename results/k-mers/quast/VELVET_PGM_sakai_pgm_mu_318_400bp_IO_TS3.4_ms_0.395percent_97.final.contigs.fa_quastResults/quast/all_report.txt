All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_97.final.contigs
#Contigs                                   29121                                                                      
#Contigs (>= 0 bp)                         34215                                                                      
#Contigs (>= 1000 bp)                      0                                                                          
Largest contig                             732                                                                        
Total length                               6933084                                                                    
Total length (>= 0 bp)                     7929723                                                                    
Total length (>= 1000 bp)                  0                                                                          
Reference length                           5594470                                                                    
N50                                        233                                                                        
NG50                                       243                                                                        
N75                                        214                                                                        
NG75                                       224                                                                        
L50                                        12929                                                                      
LG50                                       10116                                                                      
L75                                        20713                                                                      
LG75                                       16121                                                                      
#local misassemblies                       5                                                                          
#misassemblies                             928                                                                        
#misassembled contigs                      928                                                                        
Misassembled contigs length                227868                                                                     
Misassemblies inter-contig overlap         1605                                                                       
#unaligned contigs                         279 + 118 part                                                             
Unaligned contigs length                   79555                                                                      
#ambiguously mapped contigs                786                                                                        
Extra bases in ambiguously mapped contigs  -181652                                                                    
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        69.321                                                                     
Duplication ratio                          1.721                                                                      
#genes                                     129 + 4713 part                                                            
#predicted genes (unique)                  26112                                                                      
#predicted genes (>= 0 bp)                 26175                                                                      
#predicted genes (>= 300 bp)               334                                                                        
#predicted genes (>= 1500 bp)              0                                                                          
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                1607                                                                       
#indels                                    41517                                                                      
Indels length                              42141                                                                      
#mismatches per 100 kbp                    41.44                                                                      
#indels per 100 kbp                        1070.54                                                                    
GC (%)                                     49.77                                                                      
Reference GC (%)                           50.48                                                                      
Average %IDY                               98.762                                                                     
Largest alignment                          732                                                                        
NA50                                       229                                                                        
NGA50                                      240                                                                        
NA75                                       210                                                                        
NGA75                                      220                                                                        
LA50                                       13081                                                                      
LGA50                                      10226                                                                      
LA75                                       20997                                                                      
LGA75                                      16326                                                                      
#Mis_misassemblies                         928                                                                        
#Mis_relocations                           861                                                                        
#Mis_translocations                        66                                                                         
#Mis_inversions                            1                                                                          
#Mis_misassembled contigs                  928                                                                        
Mis_Misassembled contigs length            227868                                                                     
#Mis_local misassemblies                   5                                                                          
#Mis_short indels (<= 5 bp)                41516                                                                      
#Mis_long indels (> 5 bp)                  1                                                                          
#Una_fully unaligned contigs               279                                                                        
Una_Fully unaligned length                 65915                                                                      
#Una_partially unaligned contigs           118                                                                        
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             13640                                                                      
GAGE_Contigs #                             29121                                                                      
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            732                                                                        
GAGE_N50                                   243 COUNT: 10116                                                           
GAGE_Genome size                           5594470                                                                    
GAGE_Assembly size                         6933084                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               1419445(25.37%)                                                            
GAGE_Missing assembly bases                6415(0.09%)                                                                
GAGE_Missing assembly contigs              8(0.03%)                                                                   
GAGE_Duplicated reference bases            2011036                                                                    
GAGE_Compressed reference bases            302015                                                                     
GAGE_Bad trim                              4272                                                                       
GAGE_Avg idy                               98.90                                                                      
GAGE_SNPs                                  1382                                                                       
GAGE_Indels < 5bp                          36301                                                                      
GAGE_Indels >= 5                           10                                                                         
GAGE_Inversions                            143                                                                        
GAGE_Relocation                            64                                                                         
GAGE_Translocation                         20                                                                         
GAGE_Corrected contig #                    19460                                                                      
GAGE_Corrected assembly size               4664960                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    732                                                                        
GAGE_Corrected N50                         226 COUNT: 10636                                                           
