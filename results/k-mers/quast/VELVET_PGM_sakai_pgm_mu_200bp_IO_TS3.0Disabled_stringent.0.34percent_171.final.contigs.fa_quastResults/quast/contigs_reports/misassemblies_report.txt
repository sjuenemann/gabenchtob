All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_171.final.contigs
#Mis_misassemblies               4                                                                                     
#Mis_relocations                 3                                                                                     
#Mis_translocations              0                                                                                     
#Mis_inversions                  1                                                                                     
#Mis_misassembled contigs        4                                                                                     
Mis_Misassembled contigs length  3951                                                                                  
#Mis_local misassemblies         0                                                                                     
#mismatches                      236                                                                                   
#indels                          1661                                                                                  
#Mis_short indels (<= 5 bp)      1661                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                     
Indels length                    1687                                                                                  
