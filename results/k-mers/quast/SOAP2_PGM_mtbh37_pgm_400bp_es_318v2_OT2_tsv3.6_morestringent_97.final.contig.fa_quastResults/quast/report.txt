All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_97.final.contig
#Contigs (>= 0 bp)             129139                                                                      
#Contigs (>= 1000 bp)          30                                                                          
Total length (>= 0 bp)         21096828                                                                    
Total length (>= 1000 bp)      35408                                                                       
#Contigs                       9153                                                                        
Largest contig                 1976                                                                        
Total length                   2945876                                                                     
Reference length               4411532                                                                     
GC (%)                         66.01                                                                       
Reference GC (%)               65.61                                                                       
N50                            319                                                                         
NG50                           266                                                                         
N75                            265                                                                         
NG75                           None                                                                        
#misassemblies                 34                                                                          
#local misassemblies           5                                                                           
#unaligned contigs             140 + 21 part                                                               
Unaligned contigs length       48114                                                                       
Genome fraction (%)            55.563                                                                      
Duplication ratio              1.180                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        21.42                                                                       
#indels per 100 kbp            508.73                                                                      
#genes                         136 + 3393 part                                                             
#predicted genes (unique)      8651                                                                        
#predicted genes (>= 0 bp)     8675                                                                        
#predicted genes (>= 300 bp)   2010                                                                        
#predicted genes (>= 1500 bp)  1                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              1976                                                                        
NA50                           316                                                                         
NGA50                          259                                                                         
NA75                           258                                                                         
NGA75                          None                                                                        
