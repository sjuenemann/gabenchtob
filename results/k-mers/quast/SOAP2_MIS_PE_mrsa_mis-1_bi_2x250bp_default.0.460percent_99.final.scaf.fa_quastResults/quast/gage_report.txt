All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_99.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_99.final.scaf
GAGE_Contigs #                   225                                                                           192                                                                  
GAGE_Min contig                  200                                                                           200                                                                  
GAGE_Max contig                  116063                                                                        116063                                                               
GAGE_N50                         34011 COUNT: 25                                                               41078 COUNT: 21                                                      
GAGE_Genome size                 2813862                                                                       2813862                                                              
GAGE_Assembly size               2787671                                                                       2789421                                                              
GAGE_Chaff bases                 0                                                                             0                                                                    
GAGE_Missing reference bases     22248(0.79%)                                                                  22092(0.79%)                                                         
GAGE_Missing assembly bases      8006(0.29%)                                                                   9099(0.33%)                                                          
GAGE_Missing assembly contigs    8(3.56%)                                                                      8(4.17%)                                                             
GAGE_Duplicated reference bases  1736                                                                          1910                                                                 
GAGE_Compressed reference bases  24941                                                                         25408                                                                
GAGE_Bad trim                    10                                                                            239                                                                  
GAGE_Avg idy                     100.00                                                                        99.99                                                                
GAGE_SNPs                        38                                                                            38                                                                   
GAGE_Indels < 5bp                20                                                                            35                                                                   
GAGE_Indels >= 5                 12                                                                            32                                                                   
GAGE_Inversions                  0                                                                             1                                                                    
GAGE_Relocation                  1                                                                             3                                                                    
GAGE_Translocation               0                                                                             0                                                                    
GAGE_Corrected contig #          223                                                                           220                                                                  
GAGE_Corrected assembly size     2777598                                                                       2777833                                                              
GAGE_Min correct contig          200                                                                           200                                                                  
GAGE_Max correct contig          116063                                                                        116063                                                               
GAGE_Corrected N50               34011 COUNT: 26                                                               34011 COUNT: 26                                                      
