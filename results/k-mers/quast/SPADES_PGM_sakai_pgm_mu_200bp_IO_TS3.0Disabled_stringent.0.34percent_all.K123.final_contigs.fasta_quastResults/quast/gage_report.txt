All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K123.final_contigs
GAGE_Contigs #                   1094                                                                                       
GAGE_Min contig                  225                                                                                        
GAGE_Max contig                  259871                                                                                     
GAGE_N50                         79908 COUNT: 21                                                                            
GAGE_Genome size                 5594470                                                                                    
GAGE_Assembly size               5596907                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     19906(0.36%)                                                                               
GAGE_Missing assembly bases      6170(0.11%)                                                                                
GAGE_Missing assembly contigs    1(0.09%)                                                                                   
GAGE_Duplicated reference bases  260943                                                                                     
GAGE_Compressed reference bases  279834                                                                                     
GAGE_Bad trim                    5835                                                                                       
GAGE_Avg idy                     99.93                                                                                      
GAGE_SNPs                        577                                                                                        
GAGE_Indels < 5bp                1995                                                                                       
GAGE_Indels >= 5                 10                                                                                         
GAGE_Inversions                  7                                                                                          
GAGE_Relocation                  12                                                                                         
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          239                                                                                        
GAGE_Corrected assembly size     5349960                                                                                    
GAGE_Min correct contig          237                                                                                        
GAGE_Max correct contig          224401                                                                                     
GAGE_Corrected N50               72454 COUNT: 23                                                                            
