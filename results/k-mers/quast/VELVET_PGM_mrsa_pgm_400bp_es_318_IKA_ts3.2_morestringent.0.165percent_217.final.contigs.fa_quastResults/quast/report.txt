All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_217.final.contigs
#Contigs (>= 0 bp)             1177                                                                                   
#Contigs (>= 1000 bp)          862                                                                                    
Total length (>= 0 bp)         2233162                                                                                
Total length (>= 1000 bp)      1976399                                                                                
#Contigs                       1177                                                                                   
Largest contig                 10677                                                                                  
Total length                   2233162                                                                                
Reference length               2813862                                                                                
GC (%)                         33.06                                                                                  
Reference GC (%)               32.81                                                                                  
N50                            2326                                                                                   
NG50                           1811                                                                                   
N75                            1388                                                                                   
NG75                           830                                                                                    
#misassemblies                 7                                                                                      
#local misassemblies           1                                                                                      
#unaligned contigs             0 + 1 part                                                                             
Unaligned contigs length       18                                                                                     
Genome fraction (%)            77.376                                                                                 
Duplication ratio              1.024                                                                                  
#N's per 100 kbp               2.24                                                                                   
#mismatches per 100 kbp        5.92                                                                                   
#indels per 100 kbp            39.68                                                                                  
#genes                         1320 + 943 part                                                                        
#predicted genes (unique)      2906                                                                                   
#predicted genes (>= 0 bp)     2908                                                                                   
#predicted genes (>= 300 bp)   2122                                                                                   
#predicted genes (>= 1500 bp)  136                                                                                    
#predicted genes (>= 3000 bp)  13                                                                                     
Largest alignment              10677                                                                                  
NA50                           2326                                                                                   
NGA50                          1809                                                                                   
NA75                           1386                                                                                   
NGA75                          824                                                                                    
