All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K101.final_contigs
#Contigs (>= 0 bp)             3549                                                                             
#Contigs (>= 1000 bp)          168                                                                              
Total length (>= 0 bp)         6250455                                                                          
Total length (>= 1000 bp)      5263329                                                                          
#Contigs                       3327                                                                             
Largest contig                 352313                                                                           
Total length                   6219980                                                                          
Reference length               5594470                                                                          
GC (%)                         49.96                                                                            
Reference GC (%)               50.48                                                                            
N50                            124349                                                                           
NG50                           140543                                                                           
N75                            28247                                                                            
NG75                           44324                                                                            
#misassemblies                 37                                                                               
#local misassemblies           14                                                                               
#unaligned contigs             323 + 49 part                                                                    
Unaligned contigs length       96859                                                                            
Genome fraction (%)            94.522                                                                           
Duplication ratio              1.139                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        5.58                                                                             
#indels per 100 kbp            12.69                                                                            
#genes                         4933 + 216 part                                                                  
#predicted genes (unique)      7857                                                                             
#predicted genes (>= 0 bp)     7877                                                                             
#predicted genes (>= 300 bp)   4598                                                                             
#predicted genes (>= 1500 bp)  637                                                                              
#predicted genes (>= 3000 bp)  60                                                                               
Largest alignment              352313                                                                           
NA50                           124349                                                                           
NGA50                          140543                                                                           
NA75                           28247                                                                            
NGA75                          44324                                                                            
