All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_97.final.contig
#Una_fully unaligned contigs      1778                                                                                
Una_Fully unaligned length        600704                                                                              
#Una_partially unaligned contigs  449                                                                                 
#Una_with misassembly             0                                                                                   
#Una_both parts are significant   2                                                                                   
Una_Partially unaligned length    33189                                                                               
#N's                              0                                                                                   
