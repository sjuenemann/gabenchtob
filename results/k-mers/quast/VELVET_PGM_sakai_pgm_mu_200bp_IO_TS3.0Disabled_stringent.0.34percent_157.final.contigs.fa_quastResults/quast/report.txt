All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_157.final.contigs
#Contigs (>= 0 bp)             4444                                                                                  
#Contigs (>= 1000 bp)          1356                                                                                  
Total length (>= 0 bp)         4114635                                                                               
Total length (>= 1000 bp)      2046582                                                                               
#Contigs                       4444                                                                                  
Largest contig                 4721                                                                                  
Total length                   4114635                                                                               
Reference length               5594470                                                                               
GC (%)                         51.29                                                                                 
Reference GC (%)               50.48                                                                                 
N50                            996                                                                                   
NG50                           762                                                                                   
N75                            689                                                                                   
NG75                           None                                                                                  
#misassemblies                 12                                                                                    
#local misassemblies           3                                                                                     
#unaligned contigs             0 + 100 part                                                                          
Unaligned contigs length       2876                                                                                  
Genome fraction (%)            69.664                                                                                
Duplication ratio              1.041                                                                                 
#N's per 100 kbp               1.22                                                                                  
#mismatches per 100 kbp        4.95                                                                                  
#indels per 100 kbp            76.36                                                                                 
#genes                         1311 + 3158 part                                                                      
#predicted genes (unique)      7291                                                                                  
#predicted genes (>= 0 bp)     7297                                                                                  
#predicted genes (>= 300 bp)   4932                                                                                  
#predicted genes (>= 1500 bp)  89                                                                                    
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              4721                                                                                  
NA50                           986                                                                                   
NGA50                          754                                                                                   
NA75                           681                                                                                   
NGA75                          None                                                                                  
