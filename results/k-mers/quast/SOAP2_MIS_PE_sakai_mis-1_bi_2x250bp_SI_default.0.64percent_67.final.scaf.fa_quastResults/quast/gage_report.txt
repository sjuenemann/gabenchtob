All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_67.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_67.final.scaf
GAGE_Contigs #                   82518                                                                            82520                                                                   
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  4341                                                                             4341                                                                    
GAGE_N50                         507 COUNT: 3386                                                                  507 COUNT: 3385                                                         
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               22148698                                                                         22151037                                                                
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     175452(3.14%)                                                                    175004(3.13%)                                                           
GAGE_Missing assembly bases      13209797(59.64%)                                                                 13210553(59.64%)                                                        
GAGE_Missing assembly contigs    53645(65.01%)                                                                    53644(65.01%)                                                           
GAGE_Duplicated reference bases  2951935                                                                          2953467                                                                 
GAGE_Compressed reference bases  285450                                                                           285450                                                                  
GAGE_Bad trim                    352047                                                                           352380                                                                  
GAGE_Avg idy                     99.53                                                                            99.53                                                                   
GAGE_SNPs                        14296                                                                            14287                                                                   
GAGE_Indels < 5bp                93                                                                               93                                                                      
GAGE_Indels >= 5                 1                                                                                4                                                                       
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  4                                                                                4                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          14118                                                                            14115                                                                   
GAGE_Corrected assembly size     5876414                                                                          5875688                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          4341                                                                             4341                                                                    
GAGE_Corrected N50               505 COUNT: 3389                                                                  505 COUNT: 3389                                                         
