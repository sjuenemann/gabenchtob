All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K121.final_contigs
#Contigs (>= 0 bp)             1121                                                                                       
#Contigs (>= 1000 bp)          177                                                                                        
Total length (>= 0 bp)         5595550                                                                                    
Total length (>= 1000 bp)      5315976                                                                                    
#Contigs                       1075                                                                                       
Largest contig                 224187                                                                                     
Total length                   5588912                                                                                    
Reference length               5594470                                                                                    
GC (%)                         50.23                                                                                      
Reference GC (%)               50.48                                                                                      
N50                            66874                                                                                      
NG50                           66874                                                                                      
N75                            34995                                                                                      
NG75                           34995                                                                                      
#misassemblies                 74                                                                                         
#local misassemblies           13                                                                                         
#unaligned contigs             8 + 114 part                                                                               
Unaligned contigs length       6435                                                                                       
Genome fraction (%)            94.779                                                                                     
Duplication ratio              1.047                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        22.05                                                                                      
#indels per 100 kbp            39.19                                                                                      
#genes                         4994 + 150 part                                                                            
#predicted genes (unique)      6697                                                                                       
#predicted genes (>= 0 bp)     6710                                                                                       
#predicted genes (>= 300 bp)   4849                                                                                       
#predicted genes (>= 1500 bp)  507                                                                                        
#predicted genes (>= 3000 bp)  31                                                                                         
Largest alignment              224184                                                                                     
NA50                           66874                                                                                      
NGA50                          66874                                                                                      
NA75                           31448                                                                                      
NGA75                          31448                                                                                      
