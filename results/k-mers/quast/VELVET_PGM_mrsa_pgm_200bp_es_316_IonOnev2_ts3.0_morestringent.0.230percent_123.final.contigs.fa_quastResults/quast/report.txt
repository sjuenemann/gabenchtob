All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_123.final.contigs
#Contigs (>= 0 bp)             2756                                                                                        
#Contigs (>= 1000 bp)          674                                                                                         
Total length (>= 0 bp)         2253059                                                                                     
Total length (>= 1000 bp)      1018727                                                                                     
#Contigs                       2756                                                                                        
Largest contig                 7961                                                                                        
Total length                   2253059                                                                                     
Reference length               2813862                                                                                     
GC (%)                         33.11                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            929                                                                                         
NG50                           759                                                                                         
N75                            599                                                                                         
NG75                           420                                                                                         
#misassemblies                 18                                                                                          
#local misassemblies           0                                                                                           
#unaligned contigs             0 + 94 part                                                                                 
Unaligned contigs length       2639                                                                                        
Genome fraction (%)            76.879                                                                                      
Duplication ratio              1.038                                                                                       
#N's per 100 kbp               0.44                                                                                        
#mismatches per 100 kbp        10.26                                                                                       
#indels per 100 kbp            70.68                                                                                       
#genes                         712 + 1702 part                                                                             
#predicted genes (unique)      3976                                                                                        
#predicted genes (>= 0 bp)     3978                                                                                        
#predicted genes (>= 300 bp)   2701                                                                                        
#predicted genes (>= 1500 bp)  41                                                                                          
#predicted genes (>= 3000 bp)  4                                                                                           
Largest alignment              7961                                                                                        
NA50                           926                                                                                         
NGA50                          754                                                                                         
NA75                           595                                                                                         
NGA75                          416                                                                                         
