All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K87.final_contigs
#Mis_misassemblies               57                                                                                        
#Mis_relocations                 10                                                                                        
#Mis_translocations              0                                                                                         
#Mis_inversions                  47                                                                                        
#Mis_misassembled contigs        55                                                                                        
Mis_Misassembled contigs length  317201                                                                                    
#Mis_local misassemblies         14                                                                                        
#mismatches                      204                                                                                       
#indels                          1735                                                                                      
#Mis_short indels (<= 5 bp)      1732                                                                                      
#Mis_long indels (> 5 bp)        3                                                                                         
Indels length                    1842                                                                                      
