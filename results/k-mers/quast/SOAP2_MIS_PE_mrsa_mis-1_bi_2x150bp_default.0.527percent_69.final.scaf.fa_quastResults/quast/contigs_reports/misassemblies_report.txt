All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_69.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_69.final.scaf
#Mis_misassemblies               2                                                                             17                                                                   
#Mis_relocations                 2                                                                             17                                                                   
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        2                                                                             8                                                                    
Mis_Misassembled contigs length  16620                                                                         388108                                                               
#Mis_local misassemblies         0                                                                             36                                                                   
#mismatches                      32                                                                            99                                                                   
#indels                          32                                                                            1486                                                                 
#Mis_short indels (<= 5 bp)      29                                                                            1428                                                                 
#Mis_long indels (> 5 bp)        3                                                                             58                                                                   
Indels length                    74                                                                            3048                                                                 
