All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_115.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_115.final.scaf
#Contigs                                   2639                                                                              2028                                                                     
#Contigs (>= 0 bp)                         3440                                                                              2779                                                                     
#Contigs (>= 1000 bp)                      665                                                                               280                                                                      
Largest contig                             43554                                                                             180063                                                                   
Total length                               5690650                                                                           5729582                                                                  
Total length (>= 0 bp)                     5809192                                                                           5840244                                                                  
Total length (>= 1000 bp)                  5153823                                                                           5288140                                                                  
Reference length                           5594470                                                                           5594470                                                                  
N50                                        11457                                                                             40139                                                                    
NG50                                       11710                                                                             41400                                                                    
N75                                        5193                                                                              18515                                                                    
NG75                                       5427                                                                              20301                                                                    
L50                                        146                                                                               47                                                                       
LG50                                       142                                                                               45                                                                       
L75                                        329                                                                               97                                                                       
LG75                                       316                                                                               92                                                                       
#local misassemblies                       35                                                                                494                                                                      
#misassemblies                             11                                                                                28                                                                       
#misassembled contigs                      11                                                                                22                                                                       
Misassembled contigs length                35692                                                                             161567                                                                   
Misassemblies inter-contig overlap         127                                                                               115                                                                      
#unaligned contigs                         10 + 1 part                                                                       23 + 13 part                                                             
Unaligned contigs length                   8322                                                                              31978                                                                    
#ambiguously mapped contigs                527                                                                               469                                                                      
Extra bases in ambiguously mapped contigs  -162491                                                                           -136535                                                                  
#N's                                       226                                                                               31278                                                                    
#N's per 100 kbp                           3.97                                                                              545.90                                                                   
Genome fraction (%)                        94.763                                                                            94.764                                                                   
Duplication ratio                          1.041                                                                             1.049                                                                    
#genes                                     4420 + 774 part                                                                   4462 + 710 part                                                          
#predicted genes (unique)                  7314                                                                              7138                                                                     
#predicted genes (>= 0 bp)                 7394                                                                              7215                                                                     
#predicted genes (>= 300 bp)               4681                                                                              4655                                                                     
#predicted genes (>= 1500 bp)              584                                                                               605                                                                      
#predicted genes (>= 3000 bp)              48                                                                                50                                                                       
#mismatches                                398                                                                               504                                                                      
#indels                                    59                                                                                1102                                                                     
Indels length                              284                                                                               2818                                                                     
#mismatches per 100 kbp                    7.51                                                                              9.51                                                                     
#indels per 100 kbp                        1.11                                                                              20.79                                                                    
GC (%)                                     50.46                                                                             50.46                                                                    
Reference GC (%)                           50.48                                                                             50.48                                                                    
Average %IDY                               99.247                                                                            99.245                                                                   
Largest alignment                          43554                                                                             179031                                                                   
NA50                                       11420                                                                             39187                                                                    
NGA50                                      11589                                                                             40098                                                                    
NA75                                       5192                                                                              17692                                                                    
NGA75                                      5412                                                                              19541                                                                    
LA50                                       146                                                                               48                                                                       
LGA50                                      142                                                                               46                                                                       
LA75                                       330                                                                               100                                                                      
LGA75                                      317                                                                               94                                                                       
#Mis_misassemblies                         11                                                                                28                                                                       
#Mis_relocations                           11                                                                                28                                                                       
#Mis_translocations                        0                                                                                 0                                                                        
#Mis_inversions                            0                                                                                 0                                                                        
#Mis_misassembled contigs                  11                                                                                22                                                                       
Mis_Misassembled contigs length            35692                                                                             161567                                                                   
#Mis_local misassemblies                   35                                                                                494                                                                      
#Mis_short indels (<= 5 bp)                49                                                                                1025                                                                     
#Mis_long indels (> 5 bp)                  10                                                                                77                                                                       
#Una_fully unaligned contigs               10                                                                                23                                                                       
Una_Fully unaligned length                 8310                                                                              21556                                                                    
#Una_partially unaligned contigs           1                                                                                 13                                                                       
#Una_with misassembly                      0                                                                                 0                                                                        
#Una_both parts are significant            0                                                                                 11                                                                       
Una_Partially unaligned length             12                                                                                10422                                                                    
GAGE_Contigs #                             2639                                                                              2028                                                                     
GAGE_Min contig                            200                                                                               200                                                                      
GAGE_Max contig                            43554                                                                             180063                                                                   
GAGE_N50                                   11710 COUNT: 142                                                                  41400 COUNT: 45                                                          
GAGE_Genome size                           5594470                                                                           5594470                                                                  
GAGE_Assembly size                         5690650                                                                           5729582                                                                  
GAGE_Chaff bases                           0                                                                                 0                                                                        
GAGE_Missing reference bases               8622(0.15%)                                                                       8163(0.15%)                                                              
GAGE_Missing assembly bases                6972(0.12%)                                                                       40499(0.71%)                                                             
GAGE_Missing assembly contigs              7(0.27%)                                                                          8(0.39%)                                                                 
GAGE_Duplicated reference bases            174040                                                                            172074                                                                   
GAGE_Compressed reference bases            277499                                                                            267278                                                                   
GAGE_Bad trim                              58                                                                                3565                                                                     
GAGE_Avg idy                               100.00                                                                            99.99                                                                    
GAGE_SNPs                                  119                                                                               118                                                                      
GAGE_Indels < 5bp                          40                                                                                83                                                                       
GAGE_Indels >= 5                           12                                                                                126                                                                      
GAGE_Inversions                            2                                                                                 4                                                                        
GAGE_Relocation                            3                                                                                 12                                                                       
GAGE_Translocation                         0                                                                                 0                                                                        
GAGE_Corrected contig #                    1937                                                                              1919                                                                     
GAGE_Corrected assembly size               5507137                                                                           5509748                                                                  
GAGE_Min correct contig                    200                                                                               200                                                                      
GAGE_Max correct contig                    43554                                                                             43554                                                                    
GAGE_Corrected N50                         11120 COUNT: 148                                                                  11146 COUNT: 148                                                         
