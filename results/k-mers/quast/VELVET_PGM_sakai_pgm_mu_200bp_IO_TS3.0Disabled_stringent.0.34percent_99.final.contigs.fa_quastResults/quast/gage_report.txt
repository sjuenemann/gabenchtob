All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_99.final.contigs
GAGE_Contigs #                   2025                                                                                 
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  24699                                                                                
GAGE_N50                         4734 COUNT: 351                                                                      
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5274042                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     173675(3.10%)                                                                        
GAGE_Missing assembly bases      697(0.01%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  14756                                                                                
GAGE_Compressed reference bases  273910                                                                               
GAGE_Bad trim                    307                                                                                  
GAGE_Avg idy                     99.96                                                                                
GAGE_SNPs                        131                                                                                  
GAGE_Indels < 5bp                1796                                                                                 
GAGE_Indels >= 5                 29                                                                                   
GAGE_Inversions                  2                                                                                    
GAGE_Relocation                  16                                                                                   
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          2041                                                                                 
GAGE_Corrected assembly size     5262417                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          23760                                                                                
GAGE_Corrected N50               4546 COUNT: 361                                                                      
