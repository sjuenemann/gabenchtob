All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K115.final_contigs
#Contigs (>= 0 bp)             2039                                                                                             
#Contigs (>= 1000 bp)          73                                                                                               
Total length (>= 0 bp)         3328439                                                                                          
Total length (>= 1000 bp)      2772030                                                                                          
#Contigs                       2030                                                                                             
Largest contig                 174998                                                                                           
Total length                   3327240                                                                                          
Reference length               2813862                                                                                          
GC (%)                         32.58                                                                                            
Reference GC (%)               32.81                                                                                            
N50                            61458                                                                                            
NG50                           69817                                                                                            
N75                            19936                                                                                            
NG75                           39919                                                                                            
#misassemblies                 114                                                                                              
#local misassemblies           15                                                                                               
#unaligned contigs             303 + 442 part                                                                                   
Unaligned contigs length       112968                                                                                           
Genome fraction (%)            98.600                                                                                           
Duplication ratio              1.159                                                                                            
#N's per 100 kbp               0.00                                                                                             
#mismatches per 100 kbp        15.79                                                                                            
#indels per 100 kbp            31.11                                                                                            
#genes                         2637 + 57 part                                                                                   
#predicted genes (unique)      4351                                                                                             
#predicted genes (>= 0 bp)     4354                                                                                             
#predicted genes (>= 300 bp)   2356                                                                                             
#predicted genes (>= 1500 bp)  270                                                                                              
#predicted genes (>= 3000 bp)  25                                                                                               
Largest alignment              174910                                                                                           
NA50                           61458                                                                                            
NGA50                          69768                                                                                            
NA75                           19936                                                                                            
NGA75                          38365                                                                                            
