All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K95.final_contigs
#Contigs                                   6505                                                                                       
#Contigs (>= 0 bp)                         6842                                                                                       
#Contigs (>= 1000 bp)                      65                                                                                         
Largest contig                             236816                                                                                     
Total length                               4740205                                                                                    
Total length (>= 0 bp)                     4784364                                                                                    
Total length (>= 1000 bp)                  2757590                                                                                    
Reference length                           2813862                                                                                    
N50                                        28377                                                                                      
NG50                                       87682                                                                                      
N75                                        318                                                                                        
NG75                                       39827                                                                                      
L50                                        30                                                                                         
LG50                                       10                                                                                         
L75                                        2214                                                                                       
LG75                                       23                                                                                         
#local misassemblies                       9                                                                                          
#misassemblies                             19                                                                                         
#misassembled contigs                      19                                                                                         
Misassembled contigs length                82512                                                                                      
Misassemblies inter-contig overlap         2141                                                                                       
#unaligned contigs                         1675 + 170 part                                                                            
Unaligned contigs length                   522112                                                                                     
#ambiguously mapped contigs                34                                                                                         
Extra bases in ambiguously mapped contigs  -15726                                                                                     
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        98.537                                                                                     
Duplication ratio                          1.516                                                                                      
#genes                                     2656 + 52 part                                                                             
#predicted genes (unique)                  7078                                                                                       
#predicted genes (>= 0 bp)                 7085                                                                                       
#predicted genes (>= 300 bp)               2331                                                                                       
#predicted genes (>= 1500 bp)              289                                                                                        
#predicted genes (>= 3000 bp)              28                                                                                         
#mismatches                                173                                                                                        
#indels                                    494                                                                                        
Indels length                              522                                                                                        
#mismatches per 100 kbp                    6.24                                                                                       
#indels per 100 kbp                        17.82                                                                                      
GC (%)                                     32.12                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               97.387                                                                                     
Largest alignment                          236816                                                                                     
NA50                                       28377                                                                                      
NGA50                                      87682                                                                                      
NA75                                       293                                                                                        
NGA75                                      39827                                                                                      
LA50                                       30                                                                                         
LGA50                                      10                                                                                         
LA75                                       2367                                                                                       
LGA75                                      23                                                                                         
#Mis_misassemblies                         19                                                                                         
#Mis_relocations                           16                                                                                         
#Mis_translocations                        3                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  19                                                                                         
Mis_Misassembled contigs length            82512                                                                                      
#Mis_local misassemblies                   9                                                                                          
#Mis_short indels (<= 5 bp)                492                                                                                        
#Mis_long indels (> 5 bp)                  2                                                                                          
#Una_fully unaligned contigs               1675                                                                                       
Una_Fully unaligned length                 512551                                                                                     
#Una_partially unaligned contigs           170                                                                                        
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            1                                                                                          
Una_Partially unaligned length             9561                                                                                       
GAGE_Contigs #                             6505                                                                                       
GAGE_Min contig                            202                                                                                        
GAGE_Max contig                            236816                                                                                     
GAGE_N50                                   87682 COUNT: 10                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         4740205                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               1457(0.05%)                                                                                
GAGE_Missing assembly bases                240277(5.07%)                                                                              
GAGE_Missing assembly contigs              610(9.38%)                                                                                 
GAGE_Duplicated reference bases            1714883                                                                                    
GAGE_Compressed reference bases            38900                                                                                      
GAGE_Bad trim                              48906                                                                                      
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  45                                                                                         
GAGE_Indels < 5bp                          307                                                                                        
GAGE_Indels >= 5                           9                                                                                          
GAGE_Inversions                            2                                                                                          
GAGE_Relocation                            3                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    147                                                                                        
GAGE_Corrected assembly size               2786489                                                                                    
GAGE_Min correct contig                    206                                                                                        
GAGE_Max correct contig                    181878                                                                                     
GAGE_Corrected N50                         69860 COUNT: 13                                                                            
