All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K95.final_contigs
#Contigs (>= 0 bp)             6842                                                                                       
#Contigs (>= 1000 bp)          65                                                                                         
Total length (>= 0 bp)         4784364                                                                                    
Total length (>= 1000 bp)      2757590                                                                                    
#Contigs                       6505                                                                                       
Largest contig                 236816                                                                                     
Total length                   4740205                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.12                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            28377                                                                                      
NG50                           87682                                                                                      
N75                            318                                                                                        
NG75                           39827                                                                                      
#misassemblies                 19                                                                                         
#local misassemblies           9                                                                                          
#unaligned contigs             1675 + 170 part                                                                            
Unaligned contigs length       522112                                                                                     
Genome fraction (%)            98.537                                                                                     
Duplication ratio              1.516                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        6.24                                                                                       
#indels per 100 kbp            17.82                                                                                      
#genes                         2656 + 52 part                                                                             
#predicted genes (unique)      7078                                                                                       
#predicted genes (>= 0 bp)     7085                                                                                       
#predicted genes (>= 300 bp)   2331                                                                                       
#predicted genes (>= 1500 bp)  289                                                                                        
#predicted genes (>= 3000 bp)  28                                                                                         
Largest alignment              236816                                                                                     
NA50                           28377                                                                                      
NGA50                          87682                                                                                      
NA75                           293                                                                                        
NGA75                          39827                                                                                      
