All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_58-scaffolds_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_58-scaffolds
#Mis_misassemblies               6                                                                                             12                                                                                   
#Mis_relocations                 6                                                                                             12                                                                                   
#Mis_translocations              0                                                                                             0                                                                                    
#Mis_inversions                  0                                                                                             0                                                                                    
#Mis_misassembled contigs        6                                                                                             10                                                                                   
Mis_Misassembled contigs length  262352                                                                                        517055                                                                               
#Mis_local misassemblies         6                                                                                             24                                                                                   
#mismatches                      1171                                                                                          1211                                                                                 
#indels                          59                                                                                            161                                                                                  
#Mis_short indels (<= 5 bp)      49                                                                                            141                                                                                  
#Mis_long indels (> 5 bp)        10                                                                                            20                                                                                   
Indels length                    340                                                                                           638                                                                                  
