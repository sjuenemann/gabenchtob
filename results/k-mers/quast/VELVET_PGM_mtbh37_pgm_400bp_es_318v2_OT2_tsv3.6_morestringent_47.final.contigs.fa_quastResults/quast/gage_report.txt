All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_47.final.contigs
GAGE_Contigs #                   7149                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  3644                                                                          
GAGE_N50                         304 COUNT: 4091                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               2958833                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     1470230(33.33%)                                                               
GAGE_Missing assembly bases      616(0.02%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  0                                                                             
GAGE_Compressed reference bases  6465                                                                          
GAGE_Bad trim                    616                                                                           
GAGE_Avg idy                     99.93                                                                         
GAGE_SNPs                        134                                                                           
GAGE_Indels < 5bp                1816                                                                          
GAGE_Indels >= 5                 4                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  0                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          7148                                                                          
GAGE_Corrected assembly size     2958583                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          3645                                                                          
GAGE_Corrected N50               304 COUNT: 4092                                                               
