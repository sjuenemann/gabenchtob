All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_225.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_225.final.contigs
GAGE_Contigs #                   2956                                                                                     2561                                                                            
GAGE_Min contig                  449                                                                                      449                                                                             
GAGE_Max contig                  5926                                                                                     5926                                                                            
GAGE_N50                         0 COUNT: 0                                                                               0 COUNT: 0                                                                      
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               2653402                                                                                  2721358                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     2771289(49.54%)                                                                          2771289(49.54%)                                                                 
GAGE_Missing assembly bases      5123(0.19%)                                                                              73097(2.69%)                                                                    
GAGE_Missing assembly contigs    4(0.14%)                                                                                 2(0.08%)                                                                        
GAGE_Duplicated reference bases  4088                                                                                     4762                                                                            
GAGE_Compressed reference bases  274651                                                                                   275325                                                                          
GAGE_Bad trim                    155                                                                                      153                                                                             
GAGE_Avg idy                     99.99                                                                                    99.99                                                                           
GAGE_SNPs                        128                                                                                      127                                                                             
GAGE_Indels < 5bp                12                                                                                       11                                                                              
GAGE_Indels >= 5                 0                                                                                        388                                                                             
GAGE_Inversions                  1                                                                                        2                                                                               
GAGE_Relocation                  3                                                                                        4                                                                               
GAGE_Translocation               0                                                                                        0                                                                               
GAGE_Corrected contig #          2950                                                                                     2949                                                                            
GAGE_Corrected assembly size     2644795                                                                                  2644103                                                                         
GAGE_Min correct contig          222                                                                                      222                                                                             
GAGE_Max correct contig          5926                                                                                     5926                                                                            
GAGE_Corrected N50               0 COUNT: 0                                                                               0 COUNT: 0                                                                      
