All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_99.final.contig
GAGE_Contigs #                   14366                                                                    
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  1985                                                                     
GAGE_N50                         304 COUNT: 7185                                                          
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               4566240                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     1837900(32.85%)                                                          
GAGE_Missing assembly bases      20585(0.45%)                                                             
GAGE_Missing assembly contigs    32(0.22%)                                                                
GAGE_Duplicated reference bases  572659                                                                   
GAGE_Compressed reference bases  193411                                                                   
GAGE_Bad trim                    9513                                                                     
GAGE_Avg idy                     99.51                                                                    
GAGE_SNPs                        599                                                                      
GAGE_Indels < 5bp                13373                                                                    
GAGE_Indels >= 5                 9                                                                        
GAGE_Inversions                  24                                                                       
GAGE_Relocation                  16                                                                       
GAGE_Translocation               7                                                                        
GAGE_Corrected contig #          12212                                                                    
GAGE_Corrected assembly size     3931992                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          1985                                                                     
GAGE_Corrected N50               281 COUNT: 7365                                                          
