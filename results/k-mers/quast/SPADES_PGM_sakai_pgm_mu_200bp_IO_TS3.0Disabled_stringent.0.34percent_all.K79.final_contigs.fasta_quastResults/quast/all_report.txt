All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K79.final_contigs
#Contigs                                   1183                                                                                      
#Contigs (>= 0 bp)                         1490                                                                                      
#Contigs (>= 1000 bp)                      184                                                                                       
Largest contig                             307226                                                                                    
Total length                               5525282                                                                                   
Total length (>= 0 bp)                     5561948                                                                                   
Total length (>= 1000 bp)                  5229311                                                                                   
Reference length                           5594470                                                                                   
N50                                        124272                                                                                    
NG50                                       124272                                                                                    
N75                                        40848                                                                                     
NG75                                       39886                                                                                     
L50                                        16                                                                                        
LG50                                       16                                                                                        
L75                                        35                                                                                        
LG75                                       36                                                                                        
#local misassemblies                       13                                                                                        
#misassemblies                             27                                                                                        
#misassembled contigs                      27                                                                                        
Misassembled contigs length                309924                                                                                    
Misassemblies inter-contig overlap         2168                                                                                      
#unaligned contigs                         210 + 135 part                                                                            
Unaligned contigs length                   59386                                                                                     
#ambiguously mapped contigs                148                                                                                       
Extra bases in ambiguously mapped contigs  -102002                                                                                   
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        93.766                                                                                    
Duplication ratio                          1.023                                                                                     
#genes                                     4876 + 215 part                                                                           
#predicted genes (unique)                  6529                                                                                      
#predicted genes (>= 0 bp)                 6534                                                                                      
#predicted genes (>= 300 bp)               4833                                                                                      
#predicted genes (>= 1500 bp)              526                                                                                       
#predicted genes (>= 3000 bp)              42                                                                                        
#mismatches                                171                                                                                       
#indels                                    1602                                                                                      
Indels length                              1665                                                                                      
#mismatches per 100 kbp                    3.26                                                                                      
#indels per 100 kbp                        30.54                                                                                     
GC (%)                                     50.24                                                                                     
Reference GC (%)                           50.48                                                                                     
Average %IDY                               98.696                                                                                    
Largest alignment                          307226                                                                                    
NA50                                       124272                                                                                    
NGA50                                      124272                                                                                    
NA75                                       40848                                                                                     
NGA75                                      36879                                                                                     
LA50                                       16                                                                                        
LGA50                                      16                                                                                        
LA75                                       35                                                                                        
LGA75                                      37                                                                                        
#Mis_misassemblies                         27                                                                                        
#Mis_relocations                           5                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            22                                                                                        
#Mis_misassembled contigs                  27                                                                                        
Mis_Misassembled contigs length            309924                                                                                    
#Mis_local misassemblies                   13                                                                                        
#Mis_short indels (<= 5 bp)                1601                                                                                      
#Mis_long indels (> 5 bp)                  1                                                                                         
#Una_fully unaligned contigs               210                                                                                       
Una_Fully unaligned length                 50644                                                                                     
#Una_partially unaligned contigs           135                                                                                       
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             8742                                                                                      
GAGE_Contigs #                             1183                                                                                      
GAGE_Min contig                            200                                                                                       
GAGE_Max contig                            307226                                                                                    
GAGE_N50                                   124272 COUNT: 16                                                                          
GAGE_Genome size                           5594470                                                                                   
GAGE_Assembly size                         5525282                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               12319(0.22%)                                                                              
GAGE_Missing assembly bases                19466(0.35%)                                                                              
GAGE_Missing assembly contigs              42(3.55%)                                                                                 
GAGE_Duplicated reference bases            156299                                                                                    
GAGE_Compressed reference bases            284576                                                                                    
GAGE_Bad trim                              9323                                                                                      
GAGE_Avg idy                               99.96                                                                                     
GAGE_SNPs                                  270                                                                                       
GAGE_Indels < 5bp                          1674                                                                                      
GAGE_Indels >= 5                           11                                                                                        
GAGE_Inversions                            0                                                                                         
GAGE_Relocation                            8                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    475                                                                                       
GAGE_Corrected assembly size               5353649                                                                                   
GAGE_Min correct contig                    200                                                                                       
GAGE_Max correct contig                    226955                                                                                    
GAGE_Corrected N50                         101884 COUNT: 19                                                                          
