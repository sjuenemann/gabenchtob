All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K79.final_contigs
#Contigs (>= 0 bp)             1490                                                                                      
#Contigs (>= 1000 bp)          184                                                                                       
Total length (>= 0 bp)         5561948                                                                                   
Total length (>= 1000 bp)      5229311                                                                                   
#Contigs                       1183                                                                                      
Largest contig                 307226                                                                                    
Total length                   5525282                                                                                   
Reference length               5594470                                                                                   
GC (%)                         50.24                                                                                     
Reference GC (%)               50.48                                                                                     
N50                            124272                                                                                    
NG50                           124272                                                                                    
N75                            40848                                                                                     
NG75                           39886                                                                                     
#misassemblies                 27                                                                                        
#local misassemblies           13                                                                                        
#unaligned contigs             210 + 135 part                                                                            
Unaligned contigs length       59386                                                                                     
Genome fraction (%)            93.766                                                                                    
Duplication ratio              1.023                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        3.26                                                                                      
#indels per 100 kbp            30.54                                                                                     
#genes                         4876 + 215 part                                                                           
#predicted genes (unique)      6529                                                                                      
#predicted genes (>= 0 bp)     6534                                                                                      
#predicted genes (>= 300 bp)   4833                                                                                      
#predicted genes (>= 1500 bp)  526                                                                                       
#predicted genes (>= 3000 bp)  42                                                                                        
Largest alignment              307226                                                                                    
NA50                           124272                                                                                    
NGA50                          124272                                                                                    
NA75                           40848                                                                                     
NGA75                          36879                                                                                     
