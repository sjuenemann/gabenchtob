All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_113.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_113.final.scaf
GAGE_Contigs #                   2873                                                                              2217                                                                     
GAGE_Min contig                  200                                                                               200                                                                      
GAGE_Max contig                  43556                                                                             144839                                                                   
GAGE_N50                         10710 COUNT: 158                                                                  38016 COUNT: 49                                                          
GAGE_Genome size                 5594470                                                                           5594470                                                                  
GAGE_Assembly size               5712952                                                                           5753514                                                                  
GAGE_Chaff bases                 0                                                                                 0                                                                        
GAGE_Missing reference bases     8475(0.15%)                                                                       7367(0.13%)                                                              
GAGE_Missing assembly bases      6931(0.12%)                                                                       42571(0.74%)                                                             
GAGE_Missing assembly contigs    7(0.24%)                                                                          8(0.36%)                                                                 
GAGE_Duplicated reference bases  186999                                                                            188088                                                                   
GAGE_Compressed reference bases  277777                                                                            272809                                                                   
GAGE_Bad trim                    51                                                                                3635                                                                     
GAGE_Avg idy                     100.00                                                                            99.98                                                                    
GAGE_SNPs                        143                                                                               131                                                                      
GAGE_Indels < 5bp                38                                                                                95                                                                       
GAGE_Indels >= 5                 14                                                                                124                                                                      
GAGE_Inversions                  2                                                                                 3                                                                        
GAGE_Relocation                  4                                                                                 10                                                                       
GAGE_Translocation               0                                                                                 0                                                                        
GAGE_Corrected contig #          2100                                                                              2079                                                                     
GAGE_Corrected assembly size     5517217                                                                           5516646                                                                  
GAGE_Min correct contig          200                                                                               200                                                                      
GAGE_Max correct contig          43556                                                                             43556                                                                    
GAGE_Corrected N50               10194 COUNT: 165                                                                  10382 COUNT: 164                                                         
