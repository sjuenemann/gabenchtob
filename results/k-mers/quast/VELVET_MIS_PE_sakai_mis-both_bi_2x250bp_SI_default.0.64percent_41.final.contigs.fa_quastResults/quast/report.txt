All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_41.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_41.final.contigs
#Contigs (>= 0 bp)             1074                                                                                    848                                                                            
#Contigs (>= 1000 bp)          350                                                                                     162                                                                            
Total length (>= 0 bp)         5356714                                                                                 5369026                                                                        
Total length (>= 1000 bp)      5180615                                                                                 5212827                                                                        
#Contigs                       634                                                                                     411                                                                            
Largest contig                 114581                                                                                  334727                                                                         
Total length                   5302661                                                                                 5315360                                                                        
Reference length               5594470                                                                                 5594470                                                                        
GC (%)                         50.27                                                                                   50.27                                                                          
Reference GC (%)               50.48                                                                                   50.48                                                                          
N50                            27088                                                                                   94881                                                                          
NG50                           26150                                                                                   92178                                                                          
N75                            14041                                                                                   40382                                                                          
NG75                           11755                                                                                   35652                                                                          
#misassemblies                 4                                                                                       8                                                                              
#local misassemblies           9                                                                                       84                                                                             
#unaligned contigs             1 + 1 part                                                                              1 + 0 part                                                                     
Unaligned contigs length       5259                                                                                    5181                                                                           
Genome fraction (%)            92.817                                                                                  93.195                                                                         
Duplication ratio              1.001                                                                                   1.002                                                                          
#N's per 100 kbp               0.00                                                                                    231.63                                                                         
#mismatches per 100 kbp        13.13                                                                                   13.27                                                                          
#indels per 100 kbp            1.08                                                                                    20.73                                                                          
#genes                         4697 + 343 part                                                                         4840 + 222 part                                                                
#predicted genes (unique)      5469                                                                                    5389                                                                           
#predicted genes (>= 0 bp)     5471                                                                                    5391                                                                           
#predicted genes (>= 300 bp)   4487                                                                                    4468                                                                           
#predicted genes (>= 1500 bp)  638                                                                                     645                                                                            
#predicted genes (>= 3000 bp)  60                                                                                      61                                                                             
Largest alignment              114581                                                                                  334180                                                                         
NA50                           27088                                                                                   94703                                                                          
NGA50                          26150                                                                                   92075                                                                          
NA75                           13827                                                                                   40080                                                                          
NGA75                          11680                                                                                   33512                                                                          
