All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_79.final.contig
GAGE_Contigs #                   26257                                                                               
GAGE_Min contig                  200                                                                                 
GAGE_Max contig                  659                                                                                 
GAGE_N50                         313 COUNT: 4052                                                                     
GAGE_Genome size                 2813862                                                                             
GAGE_Assembly size               7258899                                                                             
GAGE_Chaff bases                 0                                                                                   
GAGE_Missing reference bases     262164(9.32%)                                                                       
GAGE_Missing assembly bases      645388(8.89%)                                                                       
GAGE_Missing assembly contigs    1661(6.33%)                                                                         
GAGE_Duplicated reference bases  3312104                                                                             
GAGE_Compressed reference bases  49738                                                                               
GAGE_Bad trim                    167735                                                                              
GAGE_Avg idy                     97.81                                                                               
GAGE_SNPs                        2438                                                                                
GAGE_Indels < 5bp                39321                                                                               
GAGE_Indels >= 5                 4                                                                                   
GAGE_Inversions                  9                                                                                   
GAGE_Relocation                  13                                                                                  
GAGE_Translocation               1                                                                                   
GAGE_Corrected contig #          11877                                                                               
GAGE_Corrected assembly size     3232653                                                                             
GAGE_Min correct contig          200                                                                                 
GAGE_Max correct contig          624                                                                                 
GAGE_Corrected N50               275 COUNT: 4514                                                                     
