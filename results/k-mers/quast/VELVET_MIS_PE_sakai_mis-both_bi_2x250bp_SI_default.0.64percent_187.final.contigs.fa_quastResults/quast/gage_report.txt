All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_187.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_187.final.contigs
GAGE_Contigs #                   891                                                                                      633                                                                             
GAGE_Min contig                  315                                                                                      373                                                                             
GAGE_Max contig                  56966                                                                                    73240                                                                           
GAGE_N50                         12127 COUNT: 135                                                                         21513 COUNT: 80                                                                 
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               5488190                                                                                  5523206                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     20783(0.37%)                                                                             20774(0.37%)                                                                    
GAGE_Missing assembly bases      5823(0.11%)                                                                              40844(0.74%)                                                                    
GAGE_Missing assembly contigs    1(0.11%)                                                                                 1(0.16%)                                                                        
GAGE_Duplicated reference bases  11597                                                                                    11221                                                                           
GAGE_Compressed reference bases  239344                                                                                   238477                                                                          
GAGE_Bad trim                    251                                                                                      194                                                                             
GAGE_Avg idy                     100.00                                                                                   100.00                                                                          
GAGE_SNPs                        137                                                                                      137                                                                             
GAGE_Indels < 5bp                17                                                                                       18                                                                              
GAGE_Indels >= 5                 4                                                                                        249                                                                             
GAGE_Inversions                  1                                                                                        2                                                                               
GAGE_Relocation                  3                                                                                        7                                                                               
GAGE_Translocation               1                                                                                        1                                                                               
GAGE_Corrected contig #          879                                                                                      880                                                                             
GAGE_Corrected assembly size     5472849                                                                                  5473708                                                                         
GAGE_Min correct contig          289                                                                                      289                                                                             
GAGE_Max correct contig          56966                                                                                    56966                                                                           
GAGE_Corrected N50               12049 COUNT: 136                                                                         12049 COUNT: 136                                                                
