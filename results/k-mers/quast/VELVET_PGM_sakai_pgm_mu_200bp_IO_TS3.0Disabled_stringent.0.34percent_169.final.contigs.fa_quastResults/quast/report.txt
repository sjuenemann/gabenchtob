All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_169.final.contigs
#Contigs (>= 0 bp)             3408                                                                                  
#Contigs (>= 1000 bp)          539                                                                                   
Total length (>= 0 bp)         2609263                                                                               
Total length (>= 1000 bp)      687494                                                                                
#Contigs                       3408                                                                                  
Largest contig                 2644                                                                                  
Total length                   2609263                                                                               
Reference length               5594470                                                                               
GC (%)                         51.68                                                                                 
Reference GC (%)               50.48                                                                                 
N50                            759                                                                                   
NG50                           None                                                                                  
N75                            611                                                                                   
NG75                           None                                                                                  
#misassemblies                 5                                                                                     
#local misassemblies           0                                                                                     
#unaligned contigs             0 + 104 part                                                                          
Unaligned contigs length       2692                                                                                  
Genome fraction (%)            44.347                                                                                
Duplication ratio              1.031                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        8.87                                                                                  
#indels per 100 kbp            76.82                                                                                 
#genes                         559 + 2754 part                                                                       
#predicted genes (unique)      4933                                                                                  
#predicted genes (>= 0 bp)     4933                                                                                  
#predicted genes (>= 300 bp)   3386                                                                                  
#predicted genes (>= 1500 bp)  19                                                                                    
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              2644                                                                                  
NA50                           748                                                                                   
NGA50                          None                                                                                  
NA75                           604                                                                                   
NGA75                          None                                                                                  
