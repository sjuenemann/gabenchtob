All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_129.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_129.final.contigs
#Mis_misassemblies               4                                                                                        16                                                                              
#Mis_relocations                 4                                                                                        16                                                                              
#Mis_translocations              0                                                                                        0                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        4                                                                                        14                                                                              
Mis_Misassembled contigs length  130568                                                                                   446860                                                                          
#Mis_local misassemblies         3                                                                                        117                                                                             
#mismatches                      119                                                                                      144                                                                             
#indels                          20                                                                                       240                                                                             
#Mis_short indels (<= 5 bp)      18                                                                                       229                                                                             
#Mis_long indels (> 5 bp)        2                                                                                        11                                                                              
Indels length                    32                                                                                       551                                                                             
