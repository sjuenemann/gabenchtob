All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent.K55.final_contigs
GAGE_Contigs #                   175                                                                                         
GAGE_Min contig                  201                                                                                         
GAGE_Max contig                  148069                                                                                      
GAGE_N50                         46429 COUNT: 16                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               2761010                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     17578(0.62%)                                                                                
GAGE_Missing assembly bases      170(0.01%)                                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  0                                                                                           
GAGE_Compressed reference bases  41199                                                                                       
GAGE_Bad trim                    163                                                                                         
GAGE_Avg idy                     99.97                                                                                       
GAGE_SNPs                        67                                                                                          
GAGE_Indels < 5bp                443                                                                                         
GAGE_Indels >= 5                 12                                                                                          
GAGE_Inversions                  1                                                                                           
GAGE_Relocation                  4                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          191                                                                                         
GAGE_Corrected assembly size     2763521                                                                                     
GAGE_Min correct contig          202                                                                                         
GAGE_Max correct contig          148079                                                                                      
GAGE_Corrected N50               44027 COUNT: 19                                                                             
