All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K41.final_contigs
#Contigs (>= 0 bp)             1464                                                                            
#Contigs (>= 1000 bp)          248                                                                             
Total length (>= 0 bp)         5353780                                                                         
Total length (>= 1000 bp)      5126195                                                                         
#Contigs                       645                                                                             
Largest contig                 200001                                                                          
Total length                   5283707                                                                         
Reference length               5594470                                                                         
GC (%)                         50.28                                                                           
Reference GC (%)               50.48                                                                           
N50                            60184                                                                           
NG50                           53930                                                                           
N75                            22663                                                                           
NG75                           19311                                                                           
#misassemblies                 3                                                                               
#local misassemblies           20                                                                              
#unaligned contigs             28 + 1 part                                                                     
Unaligned contigs length       8312                                                                            
Genome fraction (%)            92.650                                                                          
Duplication ratio              1.005                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        5.61                                                                            
#indels per 100 kbp            7.00                                                                            
#genes                         4741 + 238 part                                                                 
#predicted genes (unique)      5461                                                                            
#predicted genes (>= 0 bp)     5463                                                                            
#predicted genes (>= 300 bp)   4428                                                                            
#predicted genes (>= 1500 bp)  631                                                                             
#predicted genes (>= 3000 bp)  61                                                                              
Largest alignment              200001                                                                          
NA50                           60184                                                                           
NGA50                          53930                                                                           
NA75                           22663                                                                           
NGA75                          19311                                                                           
