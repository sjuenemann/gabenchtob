All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_110-unitigs
#Mis_misassemblies               10                                                                                 
#Mis_relocations                 10                                                                                 
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        10                                                                                 
Mis_Misassembled contigs length  22980                                                                              
#Mis_local misassemblies         2                                                                                  
#mismatches                      31                                                                                 
#indels                          198                                                                                
#Mis_short indels (<= 5 bp)      198                                                                                
#Mis_long indels (> 5 bp)        0                                                                                  
Indels length                    201                                                                                
