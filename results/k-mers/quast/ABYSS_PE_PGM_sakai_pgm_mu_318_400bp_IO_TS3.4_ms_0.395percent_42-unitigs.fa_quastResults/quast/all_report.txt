All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_42-unitigs
#Contigs                                   1257                                                                   
#Contigs (>= 0 bp)                         9083                                                                   
#Contigs (>= 1000 bp)                      751                                                                    
Largest contig                             46900                                                                  
Total length                               5201551                                                                
Total length (>= 0 bp)                     5770618                                                                
Total length (>= 1000 bp)                  4973826                                                                
Reference length                           5594470                                                                
N50                                        10332                                                                  
NG50                                       9659                                                                   
N75                                        4826                                                                   
NG75                                       3844                                                                   
L50                                        156                                                                    
LG50                                       176                                                                    
L75                                        333                                                                    
LG75                                       401                                                                    
#local misassemblies                       6                                                                      
#misassemblies                             1                                                                      
#misassembled contigs                      1                                                                      
Misassembled contigs length                643                                                                    
Misassemblies inter-contig overlap         635                                                                    
#unaligned contigs                         0 + 1 part                                                             
Unaligned contigs length                   43                                                                     
#ambiguously mapped contigs                115                                                                    
Extra bases in ambiguously mapped contigs  -55389                                                                 
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        91.892                                                                 
Duplication ratio                          1.001                                                                  
#genes                                     4224 + 721 part                                                        
#predicted genes (unique)                  5797                                                                   
#predicted genes (>= 0 bp)                 5797                                                                   
#predicted genes (>= 300 bp)               4563                                                                   
#predicted genes (>= 1500 bp)              565                                                                    
#predicted genes (>= 3000 bp)              47                                                                     
#mismatches                                72                                                                     
#indels                                    231                                                                    
Indels length                              233                                                                    
#mismatches per 100 kbp                    1.40                                                                   
#indels per 100 kbp                        4.49                                                                   
GC (%)                                     50.22                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.909                                                                 
Largest alignment                          46900                                                                  
NA50                                       10332                                                                  
NGA50                                      9659                                                                   
NA75                                       4826                                                                   
NGA75                                      3844                                                                   
LA50                                       156                                                                    
LGA50                                      176                                                                    
LA75                                       333                                                                    
LGA75                                      401                                                                    
#Mis_misassemblies                         1                                                                      
#Mis_relocations                           1                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  1                                                                      
Mis_Misassembled contigs length            643                                                                    
#Mis_local misassemblies                   6                                                                      
#Mis_short indels (<= 5 bp)                231                                                                    
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           1                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             43                                                                     
GAGE_Contigs #                             1257                                                                   
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            46900                                                                  
GAGE_N50                                   9659 COUNT: 176                                                        
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5201551                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               307475(5.50%)                                                          
GAGE_Missing assembly bases                52(0.00%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            0                                                                      
GAGE_Compressed reference bases            95577                                                                  
GAGE_Bad trim                              52                                                                     
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  83                                                                     
GAGE_Indels < 5bp                          228                                                                    
GAGE_Indels >= 5                           4                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            3                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    1262                                                                   
GAGE_Corrected assembly size               5201950                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    46902                                                                  
GAGE_Corrected N50                         9548 COUNT: 177                                                        
