All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_42-unitigs
GAGE_Contigs #                   1257                                                                   
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  46900                                                                  
GAGE_N50                         9659 COUNT: 176                                                        
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5201551                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     307475(5.50%)                                                          
GAGE_Missing assembly bases      52(0.00%)                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  0                                                                      
GAGE_Compressed reference bases  95577                                                                  
GAGE_Bad trim                    52                                                                     
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        83                                                                     
GAGE_Indels < 5bp                228                                                                    
GAGE_Indels >= 5                 4                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  3                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          1262                                                                   
GAGE_Corrected assembly size     5201950                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          46902                                                                  
GAGE_Corrected N50               9548 COUNT: 177                                                        
