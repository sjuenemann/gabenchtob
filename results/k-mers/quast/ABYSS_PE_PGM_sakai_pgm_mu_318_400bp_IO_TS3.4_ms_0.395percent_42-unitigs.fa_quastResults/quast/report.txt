All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_42-unitigs
#Contigs (>= 0 bp)             9083                                                                   
#Contigs (>= 1000 bp)          751                                                                    
Total length (>= 0 bp)         5770618                                                                
Total length (>= 1000 bp)      4973826                                                                
#Contigs                       1257                                                                   
Largest contig                 46900                                                                  
Total length                   5201551                                                                
Reference length               5594470                                                                
GC (%)                         50.22                                                                  
Reference GC (%)               50.48                                                                  
N50                            10332                                                                  
NG50                           9659                                                                   
N75                            4826                                                                   
NG75                           3844                                                                   
#misassemblies                 1                                                                      
#local misassemblies           6                                                                      
#unaligned contigs             0 + 1 part                                                             
Unaligned contigs length       43                                                                     
Genome fraction (%)            91.892                                                                 
Duplication ratio              1.001                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        1.40                                                                   
#indels per 100 kbp            4.49                                                                   
#genes                         4224 + 721 part                                                        
#predicted genes (unique)      5797                                                                   
#predicted genes (>= 0 bp)     5797                                                                   
#predicted genes (>= 300 bp)   4563                                                                   
#predicted genes (>= 1500 bp)  565                                                                    
#predicted genes (>= 3000 bp)  47                                                                     
Largest alignment              46900                                                                  
NA50                           10332                                                                  
NGA50                          9659                                                                   
NA75                           4826                                                                   
NGA75                          3844                                                                   
