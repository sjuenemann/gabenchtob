All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_51.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_51.final.contigs
#Una_fully unaligned contigs      1                                                                                       2                                                                              
Una_Fully unaligned length        5436                                                                                    6276                                                                           
#Una_partially unaligned contigs  1                                                                                       1                                                                              
#Una_with misassembly             0                                                                                       0                                                                              
#Una_both parts are significant   0                                                                                       1                                                                              
Una_Partially unaligned length    78                                                                                      287                                                                            
#N's                              0                                                                                       15088                                                                          
