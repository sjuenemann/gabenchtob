All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_51.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_51.final.contigs
#Mis_misassemblies               6                                                                                       14                                                                             
#Mis_relocations                 6                                                                                       14                                                                             
#Mis_translocations              0                                                                                       0                                                                              
#Mis_inversions                  0                                                                                       0                                                                              
#Mis_misassembled contigs        6                                                                                       10                                                                             
Mis_Misassembled contigs length  108846                                                                                  665262                                                                         
#Mis_local misassemblies         18                                                                                      121                                                                            
#mismatches                      747                                                                                     767                                                                            
#indels                          62                                                                                      802                                                                            
#Mis_short indels (<= 5 bp)      58                                                                                      721                                                                            
#Mis_long indels (> 5 bp)        4                                                                                       81                                                                             
Indels length                    123                                                                                     2710                                                                           
