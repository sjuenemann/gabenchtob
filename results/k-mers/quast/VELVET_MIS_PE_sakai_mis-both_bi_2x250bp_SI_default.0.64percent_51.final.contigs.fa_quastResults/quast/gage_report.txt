All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_51.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_51.final.contigs
GAGE_Contigs #                   619                                                                                     398                                                                            
GAGE_Min contig                  200                                                                                     200                                                                            
GAGE_Max contig                  92311                                                                                   346048                                                                         
GAGE_N50                         26170 COUNT: 63                                                                         94785 COUNT: 18                                                                
GAGE_Genome size                 5594470                                                                                 5594470                                                                        
GAGE_Assembly size               5330346                                                                                 5346250                                                                        
GAGE_Chaff bases                 0                                                                                       0                                                                              
GAGE_Missing reference bases     65832(1.18%)                                                                            65112(1.16%)                                                                   
GAGE_Missing assembly bases      5539(0.10%)                                                                             20069(0.38%)                                                                   
GAGE_Missing assembly contigs    1(0.16%)                                                                                1(0.25%)                                                                       
GAGE_Duplicated reference bases  8295                                                                                    8434                                                                           
GAGE_Compressed reference bases  241671                                                                                  240870                                                                         
GAGE_Bad trim                    103                                                                                     506                                                                            
GAGE_Avg idy                     99.98                                                                                   99.97                                                                          
GAGE_SNPs                        595                                                                                     595                                                                            
GAGE_Indels < 5bp                34                                                                                      88                                                                             
GAGE_Indels >= 5                 15                                                                                      186                                                                            
GAGE_Inversions                  2                                                                                       4                                                                              
GAGE_Relocation                  10                                                                                      21                                                                             
GAGE_Translocation               0                                                                                       0                                                                              
GAGE_Corrected contig #          613                                                                                     593                                                                            
GAGE_Corrected assembly size     5321639                                                                                 5322570                                                                        
GAGE_Min correct contig          200                                                                                     200                                                                            
GAGE_Max correct contig          90227                                                                                   90227                                                                          
GAGE_Corrected N50               24951 COUNT: 67                                                                         26170 COUNT: 61                                                                
