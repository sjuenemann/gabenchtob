All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_19.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_19.final.scaf
#Mis_misassemblies               1                                                                             0                                                                    
#Mis_relocations                 1                                                                             0                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        1                                                                             0                                                                    
Mis_Misassembled contigs length  1511                                                                          0                                                                    
#Mis_local misassemblies         25                                                                            651                                                                  
#mismatches                      69                                                                            72                                                                   
#indels                          574                                                                           1805                                                                 
#Mis_short indels (<= 5 bp)      284                                                                           1369                                                                 
#Mis_long indels (> 5 bp)        290                                                                           436                                                                  
Indels length                    7053                                                                          11673                                                                
