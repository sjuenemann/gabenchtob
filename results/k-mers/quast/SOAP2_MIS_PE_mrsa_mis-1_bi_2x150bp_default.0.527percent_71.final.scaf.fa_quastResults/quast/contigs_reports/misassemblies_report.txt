All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_71.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_71.final.scaf
#Mis_misassemblies               1                                                                             14                                                                   
#Mis_relocations                 1                                                                             14                                                                   
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        1                                                                             8                                                                    
Mis_Misassembled contigs length  16376                                                                         387871                                                               
#Mis_local misassemblies         0                                                                             36                                                                   
#mismatches                      32                                                                            67                                                                   
#indels                          22                                                                            889                                                                  
#Mis_short indels (<= 5 bp)      18                                                                            813                                                                  
#Mis_long indels (> 5 bp)        4                                                                             76                                                                   
Indels length                    87                                                                            2892                                                                 
