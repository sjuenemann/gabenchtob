All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_37.final.contig
#Contigs                                   1455                                                                        
#Contigs (>= 0 bp)                         358217                                                                      
#Contigs (>= 1000 bp)                      2                                                                           
Largest contig                             1083                                                                        
Total length                               397470                                                                      
Total length (>= 0 bp)                     21992243                                                                    
Total length (>= 1000 bp)                  2119                                                                        
Reference length                           4411532                                                                     
N50                                        261                                                                         
NG50                                       None                                                                        
N75                                        223                                                                         
NG75                                       None                                                                        
L50                                        570                                                                         
LG50                                       None                                                                        
L75                                        983                                                                         
LG75                                       None                                                                        
#local misassemblies                       0                                                                           
#misassemblies                             0                                                                           
#misassembled contigs                      0                                                                           
Misassembled contigs length                0                                                                           
Misassemblies inter-contig overlap         0                                                                           
#unaligned contigs                         52 + 4 part                                                                 
Unaligned contigs length                   12472                                                                       
#ambiguously mapped contigs                2                                                                           
Extra bases in ambiguously mapped contigs  -533                                                                        
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        8.701                                                                       
Duplication ratio                          1.002                                                                       
#genes                                     5 + 1029 part                                                               
#predicted genes (unique)                  1348                                                                        
#predicted genes (>= 0 bp)                 1348                                                                        
#predicted genes (>= 300 bp)               219                                                                         
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                35                                                                          
#indels                                    222                                                                         
Indels length                              229                                                                         
#mismatches per 100 kbp                    9.12                                                                        
#indels per 100 kbp                        57.83                                                                       
GC (%)                                     69.68                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               99.932                                                                      
Largest alignment                          1083                                                                        
NA50                                       260                                                                         
NGA50                                      None                                                                        
NA75                                       221                                                                         
NGA75                                      None                                                                        
LA50                                       572                                                                         
LGA50                                      None                                                                        
LA75                                       989                                                                         
LGA75                                      None                                                                        
#Mis_misassemblies                         0                                                                           
#Mis_relocations                           0                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  0                                                                           
Mis_Misassembled contigs length            0                                                                           
#Mis_local misassemblies                   0                                                                           
#Mis_short indels (<= 5 bp)                222                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               52                                                                          
Una_Fully unaligned length                 12139                                                                       
#Una_partially unaligned contigs           4                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             333                                                                         
GAGE_Contigs #                             1455                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1083                                                                        
GAGE_N50                                   0 COUNT: 0                                                                  
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         397470                                                                      
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               4024168(91.22%)                                                             
GAGE_Missing assembly bases                10381(2.61%)                                                                
GAGE_Missing assembly contigs              41(2.82%)                                                                   
GAGE_Duplicated reference bases            238                                                                         
GAGE_Compressed reference bases            1163                                                                        
GAGE_Bad trim                              725                                                                         
GAGE_Avg idy                               99.91                                                                       
GAGE_SNPs                                  36                                                                          
GAGE_Indels < 5bp                          298                                                                         
GAGE_Indels >= 5                           1                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            0                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    1405                                                                        
GAGE_Corrected assembly size               385598                                                                      
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1083                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
