All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_103.final.contig
#Contigs (>= 0 bp)             117072                                                                       
#Contigs (>= 1000 bp)          26                                                                           
Total length (>= 0 bp)         20402756                                                                     
Total length (>= 1000 bp)      30316                                                                        
#Contigs                       45656                                                                        
Largest contig                 1647                                                                         
Total length                   10443374                                                                     
Reference length               4411532                                                                      
GC (%)                         64.73                                                                        
Reference GC (%)               65.61                                                                        
N50                            205                                                                          
NG50                           270                                                                          
N75                            204                                                                          
NG75                           206                                                                          
#misassemblies                 47                                                                           
#local misassemblies           5                                                                            
#unaligned contigs             128 + 25 part                                                                
Unaligned contigs length       42760                                                                        
Genome fraction (%)            93.236                                                                       
Duplication ratio              2.513                                                                        
#N's per 100 kbp               0.00                                                                         
#mismatches per 100 kbp        11.62                                                                        
#indels per 100 kbp            372.51                                                                       
#genes                         180 + 3819 part                                                              
#predicted genes (unique)      39892                                                                        
#predicted genes (>= 0 bp)     40405                                                                        
#predicted genes (>= 300 bp)   2171                                                                         
#predicted genes (>= 1500 bp)  0                                                                            
#predicted genes (>= 3000 bp)  0                                                                            
Largest alignment              1647                                                                         
NA50                           205                                                                          
NGA50                          265                                                                          
NA75                           204                                                                          
NGA75                          206                                                                          
