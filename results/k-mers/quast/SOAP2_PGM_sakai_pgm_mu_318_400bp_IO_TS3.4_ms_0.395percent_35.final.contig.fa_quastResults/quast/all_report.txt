All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_35.final.contig
#Contigs                                   960                                                                      
#Contigs (>= 0 bp)                         459353                                                                   
#Contigs (>= 1000 bp)                      0                                                                        
Largest contig                             550                                                                      
Total length                               236224                                                                   
Total length (>= 0 bp)                     26664597                                                                 
Total length (>= 1000 bp)                  0                                                                        
Reference length                           5594470                                                                  
N50                                        238                                                                      
NG50                                       None                                                                     
N75                                        215                                                                      
NG75                                       None                                                                     
L50                                        412                                                                      
LG50                                       None                                                                     
L75                                        674                                                                      
LG75                                       None                                                                     
#local misassemblies                       0                                                                        
#misassemblies                             0                                                                        
#misassembled contigs                      0                                                                        
Misassembled contigs length                0                                                                        
Misassemblies inter-contig overlap         0                                                                        
#unaligned contigs                         45 + 1 part                                                              
Unaligned contigs length                   12070                                                                    
#ambiguously mapped contigs                2                                                                        
Extra bases in ambiguously mapped contigs  -526                                                                     
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        3.996                                                                    
Duplication ratio                          1.000                                                                    
#genes                                     3 + 760 part                                                             
#predicted genes (unique)                  952                                                                      
#predicted genes (>= 0 bp)                 952                                                                      
#predicted genes (>= 300 bp)               75                                                                       
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                5                                                                        
#indels                                    73                                                                       
Indels length                              75                                                                       
#mismatches per 100 kbp                    2.24                                                                     
#indels per 100 kbp                        32.65                                                                    
GC (%)                                     52.47                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.962                                                                   
Largest alignment                          550                                                                      
NA50                                       234                                                                      
NGA50                                      None                                                                     
NA75                                       211                                                                      
NGA75                                      None                                                                     
LA50                                       421                                                                      
LGA50                                      None                                                                     
LA75                                       687                                                                      
LGA75                                      None                                                                     
#Mis_misassemblies                         0                                                                        
#Mis_relocations                           0                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  0                                                                        
Mis_Misassembled contigs length            0                                                                        
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                73                                                                       
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               45                                                                       
Una_Fully unaligned length                 12049                                                                    
#Una_partially unaligned contigs           1                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             21                                                                       
GAGE_Contigs #                             960                                                                      
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            550                                                                      
GAGE_N50                                   0 COUNT: 0                                                               
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         236224                                                                   
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               5369776(95.98%)                                                          
GAGE_Missing assembly bases                11985(5.07%)                                                             
GAGE_Missing assembly contigs              44(4.58%)                                                                
GAGE_Duplicated reference bases            0                                                                        
GAGE_Compressed reference bases            524                                                                      
GAGE_Bad trim                              145                                                                      
GAGE_Avg idy                               99.96                                                                    
GAGE_SNPs                                  6                                                                        
GAGE_Indels < 5bp                          80                                                                       
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            0                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    914                                                                      
GAGE_Corrected assembly size               223936                                                                   
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    550                                                                      
GAGE_Corrected N50                         0 COUNT: 0                                                               
