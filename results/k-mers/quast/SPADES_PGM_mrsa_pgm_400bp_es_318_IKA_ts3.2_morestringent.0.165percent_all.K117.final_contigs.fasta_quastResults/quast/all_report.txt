All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K117.final_contigs
#Contigs                                   16603                                                                                       
#Contigs (>= 0 bp)                         16829                                                                                       
#Contigs (>= 1000 bp)                      56                                                                                          
Largest contig                             236859                                                                                      
Total length                               8285791                                                                                     
Total length (>= 0 bp)                     8319631                                                                                     
Total length (>= 1000 bp)                  2765216                                                                                     
Reference length                           2813862                                                                                     
N50                                        383                                                                                         
NG50                                       116196                                                                                      
N75                                        313                                                                                         
NG75                                       51561                                                                                       
L50                                        3428                                                                                        
LG50                                       9                                                                                           
L75                                        9414                                                                                        
LG75                                       18                                                                                          
#local misassemblies                       7                                                                                           
#misassemblies                             109                                                                                         
#misassembled contigs                      109                                                                                         
Misassembled contigs length                129383                                                                                      
Misassemblies inter-contig overlap         1937                                                                                        
#unaligned contigs                         1832 + 456 part                                                                             
Unaligned contigs length                   624260                                                                                      
#ambiguously mapped contigs                102                                                                                         
Extra bases in ambiguously mapped contigs  -37611                                                                                      
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        98.868                                                                                      
Duplication ratio                          2.741                                                                                       
#genes                                     2667 + 54 part                                                                              
#predicted genes (unique)                  16116                                                                                       
#predicted genes (>= 0 bp)                 16138                                                                                       
#predicted genes (>= 300 bp)               2393                                                                                        
#predicted genes (>= 1500 bp)              289                                                                                         
#predicted genes (>= 3000 bp)              31                                                                                          
#mismatches                                220                                                                                         
#indels                                    754                                                                                         
Indels length                              863                                                                                         
#mismatches per 100 kbp                    7.91                                                                                        
#indels per 100 kbp                        27.10                                                                                       
GC (%)                                     32.00                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               97.582                                                                                      
Largest alignment                          236859                                                                                      
NA50                                       376                                                                                         
NGA50                                      116196                                                                                      
NA75                                       300                                                                                         
NGA75                                      50636                                                                                       
LA50                                       3485                                                                                        
LGA50                                      9                                                                                           
LA75                                       9691                                                                                        
LGA75                                      19                                                                                          
#Mis_misassemblies                         109                                                                                         
#Mis_relocations                           102                                                                                         
#Mis_translocations                        7                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  109                                                                                         
Mis_Misassembled contigs length            129383                                                                                      
#Mis_local misassemblies                   7                                                                                           
#Mis_short indels (<= 5 bp)                751                                                                                         
#Mis_long indels (> 5 bp)                  3                                                                                           
#Una_fully unaligned contigs               1832                                                                                        
Una_Fully unaligned length                 600524                                                                                      
#Una_partially unaligned contigs           456                                                                                         
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            1                                                                                           
Una_Partially unaligned length             23736                                                                                       
GAGE_Contigs #                             16603                                                                                       
GAGE_Min contig                            200                                                                                         
GAGE_Max contig                            236859                                                                                      
GAGE_N50                                   116196 COUNT: 9                                                                             
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         8285791                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               1640(0.06%)                                                                                 
GAGE_Missing assembly bases                317743(3.83%)                                                                               
GAGE_Missing assembly contigs              654(3.94%)                                                                                  
GAGE_Duplicated reference bases            5178961                                                                                     
GAGE_Compressed reference bases            34991                                                                                       
GAGE_Bad trim                              100349                                                                                      
GAGE_Avg idy                               99.98                                                                                       
GAGE_SNPs                                  51                                                                                          
GAGE_Indels < 5bp                          270                                                                                         
GAGE_Indels >= 5                           9                                                                                           
GAGE_Inversions                            0                                                                                           
GAGE_Relocation                            2                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    132                                                                                         
GAGE_Corrected assembly size               2791263                                                                                     
GAGE_Min correct contig                    201                                                                                         
GAGE_Max correct contig                    212212                                                                                      
GAGE_Corrected N50                         77414 COUNT: 11                                                                             
