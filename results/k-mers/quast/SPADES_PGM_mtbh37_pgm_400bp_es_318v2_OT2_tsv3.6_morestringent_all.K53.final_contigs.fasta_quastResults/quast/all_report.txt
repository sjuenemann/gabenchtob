All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K53.final_contigs
#Contigs                                   316                                                                                
#Contigs (>= 0 bp)                         419                                                                                
#Contigs (>= 1000 bp)                      162                                                                                
Largest contig                             158530                                                                             
Total length                               4321971                                                                            
Total length (>= 0 bp)                     4330968                                                                            
Total length (>= 1000 bp)                  4263064                                                                            
Reference length                           4411532                                                                            
N50                                        55563                                                                              
NG50                                       53686                                                                              
N75                                        31709                                                                              
NG75                                       28572                                                                              
L50                                        23                                                                                 
LG50                                       24                                                                                 
L75                                        49                                                                                 
LG75                                       52                                                                                 
#local misassemblies                       40                                                                                 
#misassemblies                             8                                                                                  
#misassembled contigs                      5                                                                                  
Misassembled contigs length                119450                                                                             
Misassemblies inter-contig overlap         4428                                                                               
#unaligned contigs                         57 + 9 part                                                                        
Unaligned contigs length                   15048                                                                              
#ambiguously mapped contigs                23                                                                                 
Extra bases in ambiguously mapped contigs  -17932                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.260                                                                             
Duplication ratio                          1.001                                                                              
#genes                                     3910 + 124 part                                                                    
#predicted genes (unique)                  4451                                                                               
#predicted genes (>= 0 bp)                 4453                                                                               
#predicted genes (>= 300 bp)               3734                                                                               
#predicted genes (>= 1500 bp)              488                                                                                
#predicted genes (>= 3000 bp)              48                                                                                 
#mismatches                                350                                                                                
#indels                                    1174                                                                               
Indels length                              1676                                                                               
#mismatches per 100 kbp                    8.16                                                                               
#indels per 100 kbp                        27.36                                                                              
GC (%)                                     65.43                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.678                                                                             
Largest alignment                          158530                                                                             
NA50                                       53686                                                                              
NGA50                                      52898                                                                              
NA75                                       30543                                                                              
NGA75                                      28572                                                                              
LA50                                       24                                                                                 
LGA50                                      25                                                                                 
LA75                                       50                                                                                 
LGA75                                      53                                                                                 
#Mis_misassemblies                         8                                                                                  
#Mis_relocations                           8                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  5                                                                                  
Mis_Misassembled contigs length            119450                                                                             
#Mis_local misassemblies                   40                                                                                 
#Mis_short indels (<= 5 bp)                1164                                                                               
#Mis_long indels (> 5 bp)                  10                                                                                 
#Una_fully unaligned contigs               57                                                                                 
Una_Fully unaligned length                 14666                                                                              
#Una_partially unaligned contigs           9                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             382                                                                                
GAGE_Contigs #                             316                                                                                
GAGE_Min contig                            204                                                                                
GAGE_Max contig                            158530                                                                             
GAGE_N50                                   53686 COUNT: 24                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4321971                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               53777(1.22%)                                                                       
GAGE_Missing assembly bases                11526(0.27%)                                                                       
GAGE_Missing assembly contigs              41(12.97%)                                                                         
GAGE_Duplicated reference bases            3619                                                                               
GAGE_Compressed reference bases            56547                                                                              
GAGE_Bad trim                              725                                                                                
GAGE_Avg idy                               99.96                                                                              
GAGE_SNPs                                  236                                                                                
GAGE_Indels < 5bp                          1204                                                                               
GAGE_Indels >= 5                           30                                                                                 
GAGE_Inversions                            3                                                                                  
GAGE_Relocation                            23                                                                                 
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    312                                                                                
GAGE_Corrected assembly size               4312224                                                                            
GAGE_Min correct contig                    210                                                                                
GAGE_Max correct contig                    156883                                                                             
GAGE_Corrected N50                         40898 COUNT: 33                                                                    
