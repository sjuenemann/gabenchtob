All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_99.final.contig
#Contigs (>= 0 bp)             104822                                                                              
#Contigs (>= 1000 bp)          1                                                                                   
Total length (>= 0 bp)         19764157                                                                            
Total length (>= 1000 bp)      1016                                                                                
#Contigs                       23900                                                                               
Largest contig                 1016                                                                                
Total length                   7780372                                                                             
Reference length               2813862                                                                             
GC (%)                         31.95                                                                               
Reference GC (%)               32.81                                                                               
N50                            330                                                                                 
NG50                           375                                                                                 
N75                            307                                                                                 
NG75                           359                                                                                 
#misassemblies                 213                                                                                 
#local misassemblies           2                                                                                   
#unaligned contigs             1617 + 430 part                                                                     
Unaligned contigs length       586483                                                                              
Genome fraction (%)            92.491                                                                              
Duplication ratio              2.751                                                                               
#N's per 100 kbp               0.00                                                                                
#mismatches per 100 kbp        118.84                                                                              
#indels per 100 kbp            1728.51                                                                             
#genes                         272 + 2395 part                                                                     
#predicted genes (unique)      21811                                                                               
#predicted genes (>= 0 bp)     21932                                                                               
#predicted genes (>= 300 bp)   949                                                                                 
#predicted genes (>= 1500 bp)  0                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                   
Largest alignment              1016                                                                                
NA50                           323                                                                                 
NGA50                          370                                                                                 
NA75                           300                                                                                 
NGA75                          353                                                                                 
