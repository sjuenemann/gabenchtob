All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_125.final.contig
#Contigs                                   33266                                                                                
#Contigs (>= 0 bp)                         51186                                                                                
#Contigs (>= 1000 bp)                      141                                                                                  
Largest contig                             3085                                                                                 
Total length                               10475503                                                                             
Total length (>= 0 bp)                     13196297                                                                             
Total length (>= 1000 bp)                  172515                                                                               
Reference length                           2813862                                                                              
N50                                        361                                                                                  
NG50                                       433                                                                                  
N75                                        246                                                                                  
NG75                                       412                                                                                  
L50                                        11984                                                                                
LG50                                       2328                                                                                 
L75                                        22090                                                                                
LG75                                       4001                                                                                 
#local misassemblies                       2                                                                                    
#misassemblies                             153                                                                                  
#misassembled contigs                      153                                                                                  
Misassembled contigs length                57129                                                                                
Misassemblies inter-contig overlap         366                                                                                  
#unaligned contigs                         444 + 250 part                                                                       
Unaligned contigs length                   198495                                                                               
#ambiguously mapped contigs                430                                                                                  
Extra bases in ambiguously mapped contigs  -114051                                                                              
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        98.232                                                                               
Duplication ratio                          3.677                                                                                
#genes                                     492 + 2214 part                                                                      
#predicted genes (unique)                  34116                                                                                
#predicted genes (>= 0 bp)                 35006                                                                                
#predicted genes (>= 300 bp)               2585                                                                                 
#predicted genes (>= 1500 bp)              2                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                    
#mismatches                                1513                                                                                 
#indels                                    21911                                                                                
Indels length                              22540                                                                                
#mismatches per 100 kbp                    54.74                                                                                
#indels per 100 kbp                        792.70                                                                               
GC (%)                                     32.71                                                                                
Reference GC (%)                           32.81                                                                                
Average %IDY                               99.176                                                                               
Largest alignment                          3085                                                                                 
NA50                                       306                                                                                  
NGA50                                      430                                                                                  
NA75                                       246                                                                                  
NGA75                                      410                                                                                  
LA50                                       12163                                                                                
LGA50                                      2334                                                                                 
LA75                                       22571                                                                                
LGA75                                      4017                                                                                 
#Mis_misassemblies                         153                                                                                  
#Mis_relocations                           148                                                                                  
#Mis_translocations                        5                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  153                                                                                  
Mis_Misassembled contigs length            57129                                                                                
#Mis_local misassemblies                   2                                                                                    
#Mis_short indels (<= 5 bp)                21911                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                    
#Una_fully unaligned contigs               444                                                                                  
Una_Fully unaligned length                 180059                                                                               
#Una_partially unaligned contigs           250                                                                                  
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            3                                                                                    
Una_Partially unaligned length             18436                                                                                
GAGE_Contigs #                             33266                                                                                
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            3085                                                                                 
GAGE_N50                                   433 COUNT: 2328                                                                      
GAGE_Genome size                           2813862                                                                              
GAGE_Assembly size                         10475503                                                                             
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               18007(0.64%)                                                                         
GAGE_Missing assembly bases                137520(1.31%)                                                                        
GAGE_Missing assembly contigs              192(0.58%)                                                                           
GAGE_Duplicated reference bases            6801207                                                                              
GAGE_Compressed reference bases            33615                                                                                
GAGE_Bad trim                              59072                                                                                
GAGE_Avg idy                               99.92                                                                                
GAGE_SNPs                                  87                                                                                   
GAGE_Indels < 5bp                          1506                                                                                 
GAGE_Indels >= 5                           2                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            0                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    10728                                                                                
GAGE_Corrected assembly size               3534544                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    3085                                                                                 
GAGE_Corrected N50                         386 COUNT: 2389                                                                      
