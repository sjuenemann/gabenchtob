All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K35.final_contigs
#Contigs                                   251                                                                                             
#Contigs (>= 0 bp)                         1405                                                                                            
#Contigs (>= 1000 bp)                      137                                                                                             
Largest contig                             99650                                                                                           
Total length                               2743898                                                                                         
Total length (>= 0 bp)                     2813682                                                                                         
Total length (>= 1000 bp)                  2700232                                                                                         
Reference length                           2813862                                                                                         
N50                                        35394                                                                                           
NG50                                       34943                                                                                           
N75                                        19703                                                                                           
NG75                                       19361                                                                                           
L50                                        25                                                                                              
LG50                                       26                                                                                              
L75                                        50                                                                                              
LG75                                       52                                                                                              
#local misassemblies                       10                                                                                              
#misassemblies                             1                                                                                               
#misassembled contigs                      1                                                                                               
Misassembled contigs length                30229                                                                                           
Misassemblies inter-contig overlap         1558                                                                                            
#unaligned contigs                         13 + 3 part                                                                                     
Unaligned contigs length                   2969                                                                                            
#ambiguously mapped contigs                12                                                                                              
Extra bases in ambiguously mapped contigs  -9708                                                                                           
#N's                                       0                                                                                               
#N's per 100 kbp                           0.00                                                                                            
Genome fraction (%)                        97.018                                                                                          
Duplication ratio                          1.001                                                                                           
#genes                                     2557 + 76 part                                                                                  
#predicted genes (unique)                  2770                                                                                            
#predicted genes (>= 0 bp)                 2770                                                                                            
#predicted genes (>= 300 bp)               2308                                                                                            
#predicted genes (>= 1500 bp)              281                                                                                             
#predicted genes (>= 3000 bp)              24                                                                                              
#mismatches                                148                                                                                             
#indels                                    375                                                                                             
Indels length                              389                                                                                             
#mismatches per 100 kbp                    5.42                                                                                            
#indels per 100 kbp                        13.74                                                                                           
GC (%)                                     32.62                                                                                           
Reference GC (%)                           32.81                                                                                           
Average %IDY                               99.744                                                                                          
Largest alignment                          99650                                                                                           
NA50                                       35394                                                                                           
NGA50                                      34943                                                                                           
NA75                                       19703                                                                                           
NGA75                                      18526                                                                                           
LA50                                       25                                                                                              
LGA50                                      26                                                                                              
LA75                                       50                                                                                              
LGA75                                      53                                                                                              
#Mis_misassemblies                         1                                                                                               
#Mis_relocations                           1                                                                                               
#Mis_translocations                        0                                                                                               
#Mis_inversions                            0                                                                                               
#Mis_misassembled contigs                  1                                                                                               
Mis_Misassembled contigs length            30229                                                                                           
#Mis_local misassemblies                   10                                                                                              
#Mis_short indels (<= 5 bp)                375                                                                                             
#Mis_long indels (> 5 bp)                  0                                                                                               
#Una_fully unaligned contigs               13                                                                                              
Una_Fully unaligned length                 2838                                                                                            
#Una_partially unaligned contigs           3                                                                                               
#Una_with misassembly                      0                                                                                               
#Una_both parts are significant            0                                                                                               
Una_Partially unaligned length             131                                                                                             
GAGE_Contigs #                             251                                                                                             
GAGE_Min contig                            201                                                                                             
GAGE_Max contig                            99650                                                                                           
GAGE_N50                                   34943 COUNT: 26                                                                                 
GAGE_Genome size                           2813862                                                                                         
GAGE_Assembly size                         2743898                                                                                         
GAGE_Chaff bases                           0                                                                                               
GAGE_Missing reference bases               38617(1.37%)                                                                                    
GAGE_Missing assembly bases                3122(0.11%)                                                                                     
GAGE_Missing assembly contigs              13(5.18%)                                                                                       
GAGE_Duplicated reference bases            0                                                                                               
GAGE_Compressed reference bases            37002                                                                                           
GAGE_Bad trim                              284                                                                                             
GAGE_Avg idy                               99.98                                                                                           
GAGE_SNPs                                  99                                                                                              
GAGE_Indels < 5bp                          384                                                                                             
GAGE_Indels >= 5                           9                                                                                               
GAGE_Inversions                            0                                                                                               
GAGE_Relocation                            2                                                                                               
GAGE_Translocation                         0                                                                                               
GAGE_Corrected contig #                    249                                                                                             
GAGE_Corrected assembly size               2742710                                                                                         
GAGE_Min correct contig                    200                                                                                             
GAGE_Max correct contig                    99661                                                                                           
GAGE_Corrected N50                         33961 COUNT: 28                                                                                 
