All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_123.final.contig
#Una_fully unaligned contigs      27                                                                        
Una_Fully unaligned length        10088                                                                     
#Una_partially unaligned contigs  27                                                                        
#Una_with misassembly             0                                                                         
#Una_both parts are significant   1                                                                         
Una_Partially unaligned length    1911                                                                      
#N's                              0                                                                         
