All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_141.final.contigs
GAGE_Contigs #                   4208                                                                           
GAGE_Min contig                  281                                                                            
GAGE_Max contig                  3672                                                                           
GAGE_N50                         0 COUNT: 0                                                                     
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               1989078                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     2599984(58.94%)                                                                
GAGE_Missing assembly bases      36(0.00%)                                                                      
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  98823                                                                          
GAGE_Compressed reference bases  27818                                                                          
GAGE_Bad trim                    16                                                                             
GAGE_Avg idy                     99.91                                                                          
GAGE_SNPs                        76                                                                             
GAGE_Indels < 5bp                1496                                                                           
GAGE_Indels >= 5                 2                                                                              
GAGE_Inversions                  30                                                                             
GAGE_Relocation                  16                                                                             
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          3921                                                                           
GAGE_Corrected assembly size     1880272                                                                        
GAGE_Min correct contig          201                                                                            
GAGE_Max correct contig          3672                                                                           
GAGE_Corrected N50               0 COUNT: 0                                                                     
