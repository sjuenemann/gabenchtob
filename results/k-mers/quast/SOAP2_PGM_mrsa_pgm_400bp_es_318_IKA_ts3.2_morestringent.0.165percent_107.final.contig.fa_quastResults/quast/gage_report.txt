All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_107.final.contig
GAGE_Contigs #                   41973                                                                                
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  1331                                                                                 
GAGE_N50                         395 COUNT: 3173                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               11777227                                                                             
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     12064(0.43%)                                                                         
GAGE_Missing assembly bases      271203(2.30%)                                                                        
GAGE_Missing assembly contigs    459(1.09%)                                                                           
GAGE_Duplicated reference bases  7781296                                                                              
GAGE_Compressed reference bases  39229                                                                                
GAGE_Bad trim                    102177                                                                               
GAGE_Avg idy                     99.72                                                                                
GAGE_SNPs                        222                                                                                  
GAGE_Indels < 5bp                5832                                                                                 
GAGE_Indels >= 5                 1                                                                                    
GAGE_Inversions                  3                                                                                    
GAGE_Relocation                  1                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          14535                                                                                
GAGE_Corrected assembly size     3718693                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          1331                                                                                 
GAGE_Corrected N50               265 COUNT: 3793                                                                      
