All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K87.final_contigs
#Contigs (>= 0 bp)             1210                                                                               
#Contigs (>= 1000 bp)          149                                                                                
Total length (>= 0 bp)         4584063                                                                            
Total length (>= 1000 bp)      4284699                                                                            
#Contigs                       1153                                                                               
Largest contig                 158673                                                                             
Total length                   4577218                                                                            
Reference length               4411532                                                                            
GC (%)                         65.34                                                                              
Reference GC (%)               65.61                                                                              
N50                            63326                                                                              
NG50                           64069                                                                              
N75                            30236                                                                              
NG75                           31743                                                                              
#misassemblies                 12                                                                                 
#local misassemblies           23                                                                                 
#unaligned contigs             285 + 18 part                                                                      
Unaligned contigs length       81221                                                                              
Genome fraction (%)            97.433                                                                             
Duplication ratio              1.043                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        9.26                                                                               
#indels per 100 kbp            33.04                                                                              
#genes                         3937 + 112 part                                                                    
#predicted genes (unique)      5013                                                                               
#predicted genes (>= 0 bp)     5017                                                                               
#predicted genes (>= 300 bp)   3789                                                                               
#predicted genes (>= 1500 bp)  477                                                                                
#predicted genes (>= 3000 bp)  42                                                                                 
Largest alignment              158673                                                                             
NA50                           63326                                                                              
NGA50                          64069                                                                              
NA75                           29664                                                                              
NGA75                          31508                                                                              
