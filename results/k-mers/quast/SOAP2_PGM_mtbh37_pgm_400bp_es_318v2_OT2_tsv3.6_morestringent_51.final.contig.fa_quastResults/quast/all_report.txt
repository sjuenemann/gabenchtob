All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_51.final.contig
#Contigs                                   2801                                                                        
#Contigs (>= 0 bp)                         284330                                                                      
#Contigs (>= 1000 bp)                      2                                                                           
Largest contig                             1318                                                                        
Total length                               751616                                                                      
Total length (>= 0 bp)                     23763257                                                                    
Total length (>= 1000 bp)                  2452                                                                        
Reference length                           4411532                                                                     
N50                                        255                                                                         
NG50                                       None                                                                        
N75                                        220                                                                         
NG75                                       None                                                                        
L50                                        1098                                                                        
LG50                                       None                                                                        
L75                                        1900                                                                        
LG75                                       None                                                                        
#local misassemblies                       0                                                                           
#misassemblies                             0                                                                           
#misassembled contigs                      0                                                                           
Misassembled contigs length                0                                                                           
Misassemblies inter-contig overlap         0                                                                           
#unaligned contigs                         623 + 4 part                                                                
Unaligned contigs length                   139298                                                                      
#ambiguously mapped contigs                5                                                                           
Extra bases in ambiguously mapped contigs  -1180                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        13.813                                                                      
Duplication ratio                          1.003                                                                       
#genes                                     9 + 1495 part                                                               
#predicted genes (unique)                  2360                                                                        
#predicted genes (>= 0 bp)                 2361                                                                        
#predicted genes (>= 300 bp)               395                                                                         
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                46                                                                          
#indels                                    372                                                                         
Indels length                              388                                                                         
#mismatches per 100 kbp                    7.55                                                                        
#indels per 100 kbp                        61.05                                                                       
GC (%)                                     67.96                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               99.926                                                                      
Largest alignment                          1318                                                                        
NA50                                       250                                                                         
NGA50                                      None                                                                        
NA75                                       207                                                                         
NGA75                                      None                                                                        
LA50                                       1107                                                                        
LGA50                                      None                                                                        
LA75                                       1939                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         0                                                                           
#Mis_relocations                           0                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  0                                                                           
Mis_Misassembled contigs length            0                                                                           
#Mis_local misassemblies                   0                                                                           
#Mis_short indels (<= 5 bp)                372                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               623                                                                         
Una_Fully unaligned length                 139029                                                                      
#Una_partially unaligned contigs           4                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             269                                                                         
GAGE_Contigs #                             2801                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1318                                                                        
GAGE_N50                                   0 COUNT: 0                                                                  
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         751616                                                                      
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               3714422(84.20%)                                                             
GAGE_Missing assembly bases                44673(5.94%)                                                                
GAGE_Missing assembly contigs              176(6.28%)                                                                  
GAGE_Duplicated reference bases            5768                                                                        
GAGE_Compressed reference bases            2455                                                                        
GAGE_Bad trim                              4647                                                                        
GAGE_Avg idy                               99.51                                                                       
GAGE_SNPs                                  138                                                                         
GAGE_Indels < 5bp                          3192                                                                        
GAGE_Indels >= 5                           1                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            0                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    2479                                                                        
GAGE_Corrected assembly size               679424                                                                      
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1319                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
