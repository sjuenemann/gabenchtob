All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_119.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_119.final.scaf
#Contigs                                   1371                                                                     923                                                             
#Contigs (>= 0 bp)                         1510                                                                     1051                                                            
#Contigs (>= 1000 bp)                      531                                                                      139                                                             
Largest contig                             42859                                                                    136158                                                          
Total length                               4493322                                                                  4517618                                                         
Total length (>= 0 bp)                     4513602                                                                  4536363                                                         
Total length (>= 1000 bp)                  4259760                                                                  4314041                                                         
Reference length                           4411532                                                                  4411532                                                         
N50                                        11582                                                                    62042                                                           
NG50                                       11800                                                                    63257                                                           
N75                                        6183                                                                     30743                                                           
NG75                                       6508                                                                     31435                                                           
L50                                        126                                                                      26                                                              
LG50                                       122                                                                      25                                                              
L75                                        255                                                                      52                                                              
LG75                                       245                                                                      50                                                              
#local misassemblies                       15                                                                       376                                                             
#misassemblies                             15                                                                       15                                                              
#misassembled contigs                      14                                                                       14                                                              
Misassembled contigs length                37966                                                                    182645                                                          
Misassemblies inter-contig overlap         303                                                                      1576                                                            
#unaligned contigs                         2 + 4 part                                                               2 + 4 part                                                      
Unaligned contigs length                   1568                                                                     1535                                                            
#ambiguously mapped contigs                77                                                                       74                                                              
Extra bases in ambiguously mapped contigs  -28938                                                                   -27884                                                          
#N's                                       87                                                                       22848                                                           
#N's per 100 kbp                           1.94                                                                     505.75                                                          
Genome fraction (%)                        98.282                                                                   98.404                                                          
Duplication ratio                          1.029                                                                    1.034                                                           
#genes                                     3526 + 538 part                                                          3592 + 473 part                                                 
#predicted genes (unique)                  5034                                                                     4910                                                            
#predicted genes (>= 0 bp)                 5085                                                                     4958                                                            
#predicted genes (>= 300 bp)               3702                                                                     3665                                                            
#predicted genes (>= 1500 bp)              512                                                                      525                                                             
#predicted genes (>= 3000 bp)              62                                                                       63                                                              
#mismatches                                409                                                                      413                                                             
#indels                                    81                                                                       874                                                             
Indels length                              371                                                                      2753                                                            
#mismatches per 100 kbp                    9.43                                                                     9.51                                                            
#indels per 100 kbp                        1.87                                                                     20.13                                                           
GC (%)                                     65.62                                                                    65.62                                                           
Reference GC (%)                           65.61                                                                    65.61                                                           
Average %IDY                               99.760                                                                   99.741                                                          
Largest alignment                          42859                                                                    135468                                                          
NA50                                       11582                                                                    61563                                                           
NGA50                                      11783                                                                    62959                                                           
NA75                                       6180                                                                     30231                                                           
NGA75                                      6491                                                                     31311                                                           
LA50                                       126                                                                      26                                                              
LGA50                                      123                                                                      25                                                              
LA75                                       256                                                                      53                                                              
LGA75                                      246                                                                      50                                                              
#Mis_misassemblies                         15                                                                       15                                                              
#Mis_relocations                           5                                                                        5                                                               
#Mis_translocations                        0                                                                        0                                                               
#Mis_inversions                            10                                                                       10                                                              
#Mis_misassembled contigs                  14                                                                       14                                                              
Mis_Misassembled contigs length            37966                                                                    182645                                                          
#Mis_local misassemblies                   15                                                                       376                                                             
#Mis_short indels (<= 5 bp)                71                                                                       798                                                             
#Mis_long indels (> 5 bp)                  10                                                                       76                                                              
#Una_fully unaligned contigs               2                                                                        2                                                               
Una_Fully unaligned length                 499                                                                      499                                                             
#Una_partially unaligned contigs           4                                                                        4                                                               
#Una_with misassembly                      0                                                                        0                                                               
#Una_both parts are significant            1                                                                        0                                                               
Una_Partially unaligned length             1069                                                                     1036                                                            
GAGE_Contigs #                             1371                                                                     923                                                             
GAGE_Min contig                            200                                                                      200                                                             
GAGE_Max contig                            42859                                                                    136158                                                          
GAGE_N50                                   11800 COUNT: 122                                                         63257 COUNT: 25                                                 
GAGE_Genome size                           4411532                                                                  4411532                                                         
GAGE_Assembly size                         4493322                                                                  4517618                                                         
GAGE_Chaff bases                           0                                                                        0                                                               
GAGE_Missing reference bases               18206(0.41%)                                                             17487(0.40%)                                                    
GAGE_Missing assembly bases                1785(0.04%)                                                              23849(0.53%)                                                    
GAGE_Missing assembly contigs              2(0.15%)                                                                 2(0.22%)                                                        
GAGE_Duplicated reference bases            88071                                                                    89463                                                           
GAGE_Compressed reference bases            63942                                                                    62691                                                           
GAGE_Bad trim                              1215                                                                     1210                                                            
GAGE_Avg idy                               99.99                                                                    99.98                                                           
GAGE_SNPs                                  244                                                                      240                                                             
GAGE_Indels < 5bp                          45                                                                       72                                                              
GAGE_Indels >= 5                           12                                                                       151                                                             
GAGE_Inversions                            0                                                                        0                                                               
GAGE_Relocation                            5                                                                        13                                                              
GAGE_Translocation                         0                                                                        0                                                               
GAGE_Corrected contig #                    1028                                                                     1011                                                            
GAGE_Corrected assembly size               4403416                                                                  4403375                                                         
GAGE_Min correct contig                    202                                                                      202                                                             
GAGE_Max correct contig                    42859                                                                    42859                                                           
GAGE_Corrected N50                         11487 COUNT: 129                                                         11667 COUNT: 127                                                
