All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_91.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_91.final.scaf
GAGE_Contigs #                   1609                                                                             624                                                                     
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  40861                                                                            133114                                                                  
GAGE_N50                         6833 COUNT: 242                                                                  38069 COUNT: 46                                                         
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               5270173                                                                          5346045                                                                 
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     177146(3.17%)                                                                    165849(2.96%)                                                           
GAGE_Missing assembly bases      8035(0.15%)                                                                      64391(1.20%)                                                            
GAGE_Missing assembly contigs    8(0.50%)                                                                         9(1.44%)                                                                
GAGE_Duplicated reference bases  15880                                                                            22690                                                                   
GAGE_Compressed reference bases  229435                                                                           227961                                                                  
GAGE_Bad trim                    38                                                                               6290                                                                    
GAGE_Avg idy                     99.99                                                                            99.98                                                                   
GAGE_SNPs                        171                                                                              126                                                                     
GAGE_Indels < 5bp                77                                                                               134                                                                     
GAGE_Indels >= 5                 59                                                                               1018                                                                    
GAGE_Inversions                  2                                                                                6                                                                       
GAGE_Relocation                  4                                                                                17                                                                      
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          1600                                                                             1564                                                                    
GAGE_Corrected assembly size     5244573                                                                          5241445                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          33646                                                                            33646                                                                   
GAGE_Corrected N50               6528 COUNT: 256                                                                  6577 COUNT: 254                                                         
