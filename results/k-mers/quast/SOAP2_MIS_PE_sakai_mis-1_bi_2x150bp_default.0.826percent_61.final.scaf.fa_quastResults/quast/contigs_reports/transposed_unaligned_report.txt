All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                       #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's 
SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_61.final.scaf_broken  2                             5622                        1                                 0                      0                                30                              74   
SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_61.final.scaf         20                            24159                       19                                0                      10                               25542                           50602
