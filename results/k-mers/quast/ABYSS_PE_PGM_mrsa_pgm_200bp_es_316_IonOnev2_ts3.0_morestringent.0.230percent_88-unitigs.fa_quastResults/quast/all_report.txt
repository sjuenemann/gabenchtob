All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_88-unitigs
#Contigs                                   652                                                                                    
#Contigs (>= 0 bp)                         2300                                                                                   
#Contigs (>= 1000 bp)                      472                                                                                    
Largest contig                             57588                                                                                  
Total length                               2766428                                                                                
Total length (>= 0 bp)                     2978139                                                                                
Total length (>= 1000 bp)                  2680015                                                                                
Reference length                           2813862                                                                                
N50                                        8281                                                                                   
NG50                                       7981                                                                                   
N75                                        4358                                                                                   
NG75                                       4235                                                                                   
L50                                        101                                                                                    
LG50                                       104                                                                                    
L75                                        218                                                                                    
LG75                                       226                                                                                    
#local misassemblies                       2                                                                                      
#misassemblies                             20                                                                                     
#misassembled contigs                      20                                                                                     
Misassembled contigs length                46638                                                                                  
Misassemblies inter-contig overlap         396                                                                                    
#unaligned contigs                         0 + 0 part                                                                             
Unaligned contigs length                   0                                                                                      
#ambiguously mapped contigs                11                                                                                     
Extra bases in ambiguously mapped contigs  -4386                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        97.709                                                                                 
Duplication ratio                          1.005                                                                                  
#genes                                     2335 + 343 part                                                                        
#predicted genes (unique)                  3057                                                                                   
#predicted genes (>= 0 bp)                 3060                                                                                   
#predicted genes (>= 300 bp)               2426                                                                                   
#predicted genes (>= 1500 bp)              252                                                                                    
#predicted genes (>= 3000 bp)              19                                                                                     
#mismatches                                44                                                                                     
#indels                                    552                                                                                    
Indels length                              568                                                                                    
#mismatches per 100 kbp                    1.60                                                                                   
#indels per 100 kbp                        20.08                                                                                  
GC (%)                                     32.62                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.406                                                                                 
Largest alignment                          57588                                                                                  
NA50                                       8228                                                                                   
NGA50                                      7952                                                                                   
NA75                                       4358                                                                                   
NGA75                                      4235                                                                                   
LA50                                       101                                                                                    
LGA50                                      104                                                                                    
LA75                                       219                                                                                    
LGA75                                      227                                                                                    
#Mis_misassemblies                         20                                                                                     
#Mis_relocations                           7                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            13                                                                                     
#Mis_misassembled contigs                  20                                                                                     
Mis_Misassembled contigs length            46638                                                                                  
#Mis_local misassemblies                   2                                                                                      
#Mis_short indels (<= 5 bp)                552                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             652                                                                                    
GAGE_Min contig                            200                                                                                    
GAGE_Max contig                            57588                                                                                  
GAGE_N50                                   7981 COUNT: 104                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2766428                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               41693(1.48%)                                                                           
GAGE_Missing assembly bases                6(0.00%)                                                                               
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            2421                                                                                   
GAGE_Compressed reference bases            26397                                                                                  
GAGE_Bad trim                              6                                                                                      
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  40                                                                                     
GAGE_Indels < 5bp                          557                                                                                    
GAGE_Indels >= 5                           3                                                                                      
GAGE_Inversions                            2                                                                                      
GAGE_Relocation                            1                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    651                                                                                    
GAGE_Corrected assembly size               2764399                                                                                
GAGE_Min correct contig                    200                                                                                    
GAGE_Max correct contig                    57592                                                                                  
GAGE_Corrected N50                         7843 COUNT: 105                                                                        
