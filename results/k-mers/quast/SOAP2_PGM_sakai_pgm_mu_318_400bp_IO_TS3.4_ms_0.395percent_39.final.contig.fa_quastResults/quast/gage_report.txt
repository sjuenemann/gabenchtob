All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_39.final.contig
GAGE_Contigs #                   1247                                                                     
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  550                                                                      
GAGE_N50                         0 COUNT: 0                                                               
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               306816                                                                   
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     5303006(94.79%)                                                          
GAGE_Missing assembly bases      15388(5.02%)                                                             
GAGE_Missing assembly contigs    59(4.73%)                                                                
GAGE_Duplicated reference bases  0                                                                        
GAGE_Compressed reference bases  236                                                                      
GAGE_Bad trim                    222                                                                      
GAGE_Avg idy                     99.94                                                                    
GAGE_SNPs                        11                                                                       
GAGE_Indels < 5bp                174                                                                      
GAGE_Indels >= 5                 0                                                                        
GAGE_Inversions                  0                                                                        
GAGE_Relocation                  0                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          1184                                                                     
GAGE_Corrected assembly size     290694                                                                   
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          550                                                                      
GAGE_Corrected N50               0 COUNT: 0                                                               
