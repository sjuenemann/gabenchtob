All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_195.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_195.final.contigs
GAGE_Contigs #                   1250                                                                                     833                                                                             
GAGE_Min contig                  331                                                                                      389                                                                             
GAGE_Max contig                  33755                                                                                    49247                                                                           
GAGE_N50                         7729 COUNT: 221                                                                          13954 COUNT: 125                                                                
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               5499540                                                                                  5558022                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     34819(0.62%)                                                                             34797(0.62%)                                                                    
GAGE_Missing assembly bases      5930(0.11%)                                                                              64208(1.16%)                                                                    
GAGE_Missing assembly contigs    1(0.08%)                                                                                 1(0.12%)                                                                        
GAGE_Duplicated reference bases  16165                                                                                    15672                                                                           
GAGE_Compressed reference bases  237820                                                                                   235227                                                                          
GAGE_Bad trim                    350                                                                                      750                                                                             
GAGE_Avg idy                     99.99                                                                                    99.99                                                                           
GAGE_SNPs                        185                                                                                      188                                                                             
GAGE_Indels < 5bp                19                                                                                       18                                                                              
GAGE_Indels >= 5                 2                                                                                        408                                                                             
GAGE_Inversions                  0                                                                                        2                                                                               
GAGE_Relocation                  3                                                                                        5                                                                               
GAGE_Translocation               1                                                                                        1                                                                               
GAGE_Corrected contig #          1230                                                                                     1230                                                                            
GAGE_Corrected assembly size     5479099                                                                                  5481491                                                                         
GAGE_Min correct contig          387                                                                                      387                                                                             
GAGE_Max correct contig          33755                                                                                    33755                                                                           
GAGE_Corrected N50               7723 COUNT: 222                                                                          7723 COUNT: 222                                                                 
