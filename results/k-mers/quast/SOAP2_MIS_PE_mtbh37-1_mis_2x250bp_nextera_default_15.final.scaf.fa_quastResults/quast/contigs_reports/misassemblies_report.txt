All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_15.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_15.final.scaf
#Mis_misassemblies               0                                                                       5                                                              
#Mis_relocations                 0                                                                       5                                                              
#Mis_translocations              0                                                                       0                                                              
#Mis_inversions                  0                                                                       0                                                              
#Mis_misassembled contigs        0                                                                       5                                                              
Mis_Misassembled contigs length  0                                                                       2737                                                           
#Mis_local misassemblies         0                                                                       196                                                            
#mismatches                      24                                                                      349                                                            
#indels                          9                                                                       50                                                             
#Mis_short indels (<= 5 bp)      8                                                                       48                                                             
#Mis_long indels (> 5 bp)        1                                                                       2                                                              
Indels length                    15                                                                      65                                                             
