All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_15.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_15.final.scaf
#Contigs                                   151                                                                     842                                                            
#Contigs (>= 0 bp)                         6788                                                                    5091                                                           
#Contigs (>= 1000 bp)                      0                                                                       50                                                             
Largest contig                             344                                                                     5155                                                           
Total length                               34314                                                                   417402                                                         
Total length (>= 0 bp)                     737273                                                                  926931                                                         
Total length (>= 1000 bp)                  0                                                                       75419                                                          
Reference length                           4411532                                                                 4411532                                                        
N50                                        222                                                                     575                                                            
NG50                                       None                                                                    None                                                           
N75                                        209                                                                     367                                                            
NG75                                       None                                                                    None                                                           
L50                                        70                                                                      232                                                            
LG50                                       None                                                                    None                                                           
L75                                        109                                                                     459                                                            
LG75                                       None                                                                    None                                                           
#local misassemblies                       0                                                                       196                                                            
#misassemblies                             0                                                                       5                                                              
#misassembled contigs                      0                                                                       5                                                              
Misassembled contigs length                0                                                                       2737                                                           
Misassemblies inter-contig overlap         0                                                                       49                                                             
#unaligned contigs                         72 + 13 part                                                            240 + 409 part                                                 
Unaligned contigs length                   16986                                                                   239848                                                         
#ambiguously mapped contigs                0                                                                       0                                                              
Extra bases in ambiguously mapped contigs  0                                                                       0                                                              
#N's                                       50                                                                      189595                                                         
#N's per 100 kbp                           145.71                                                                  45422.64                                                       
Genome fraction (%)                        0.392                                                                   2.291                                                          
Duplication ratio                          1.001                                                                   1.758                                                          
#genes                                     0 + 55 part                                                             2 + 384 part                                                   
#predicted genes (unique)                  112                                                                     848                                                            
#predicted genes (>= 0 bp)                 112                                                                     848                                                            
#predicted genes (>= 300 bp)               1                                                                       13                                                             
#predicted genes (>= 1500 bp)              0                                                                       0                                                              
#predicted genes (>= 3000 bp)              0                                                                       0                                                              
#mismatches                                24                                                                      349                                                            
#indels                                    9                                                                       50                                                             
Indels length                              15                                                                      65                                                             
#mismatches per 100 kbp                    138.67                                                                  350.96                                                         
#indels per 100 kbp                        52.00                                                                   50.28                                                          
GC (%)                                     54.93                                                                   60.75                                                          
Reference GC (%)                           65.61                                                                   65.61                                                          
Average %IDY                               99.715                                                                  99.617                                                         
Largest alignment                          338                                                                     1101                                                           
NA50                                       100                                                                     None                                                           
NGA50                                      None                                                                    None                                                           
NA75                                       None                                                                    None                                                           
NGA75                                      None                                                                    None                                                           
LA50                                       78                                                                      None                                                           
LGA50                                      None                                                                    None                                                           
LA75                                       None                                                                    None                                                           
LGA75                                      None                                                                    None                                                           
#Mis_misassemblies                         0                                                                       5                                                              
#Mis_relocations                           0                                                                       5                                                              
#Mis_translocations                        0                                                                       0                                                              
#Mis_inversions                            0                                                                       0                                                              
#Mis_misassembled contigs                  0                                                                       5                                                              
Mis_Misassembled contigs length            0                                                                       2737                                                           
#Mis_local misassemblies                   0                                                                       196                                                            
#Mis_short indels (<= 5 bp)                8                                                                       48                                                             
#Mis_long indels (> 5 bp)                  1                                                                       2                                                              
#Una_fully unaligned contigs               72                                                                      240                                                            
Una_Fully unaligned length                 16091                                                                   94005                                                          
#Una_partially unaligned contigs           13                                                                      409                                                            
#Una_with misassembly                      0                                                                       2                                                              
#Una_both parts are significant            0                                                                       5                                                              
Una_Partially unaligned length             895                                                                     145843                                                         
GAGE_Contigs #                             151                                                                     842                                                            
GAGE_Min contig                            200                                                                     200                                                            
GAGE_Max contig                            344                                                                     5155                                                           
GAGE_N50                                   0 COUNT: 0                                                              0 COUNT: 0                                                     
GAGE_Genome size                           4411532                                                                 4411532                                                        
GAGE_Assembly size                         34314                                                                   417402                                                         
GAGE_Chaff bases                           0                                                                       0                                                              
GAGE_Missing reference bases               4393755(99.60%)                                                         4285355(97.14%)                                                
GAGE_Missing assembly bases                16530(48.17%)                                                           291200(69.76%)                                                 
GAGE_Missing assembly contigs              70(46.36%)                                                              174(20.67%)                                                    
GAGE_Duplicated reference bases            0                                                                       0                                                              
GAGE_Compressed reference bases            0                                                                       0                                                              
GAGE_Bad trim                              842                                                                     174688                                                         
GAGE_Avg idy                               99.81                                                                   99.62                                                          
GAGE_SNPs                                  26                                                                      454                                                            
GAGE_Indels < 5bp                          1                                                                       21                                                             
GAGE_Indels >= 5                           4                                                                       384                                                            
GAGE_Inversions                            0                                                                       5                                                              
GAGE_Relocation                            0                                                                       5                                                              
GAGE_Translocation                         0                                                                       0                                                              
GAGE_Corrected contig #                    67                                                                      64                                                             
GAGE_Corrected assembly size               15436                                                                   14684                                                          
GAGE_Min correct contig                    200                                                                     200                                                            
GAGE_Max correct contig                    338                                                                     338                                                            
GAGE_Corrected N50                         0 COUNT: 0                                                              0 COUNT: 0                                                     
