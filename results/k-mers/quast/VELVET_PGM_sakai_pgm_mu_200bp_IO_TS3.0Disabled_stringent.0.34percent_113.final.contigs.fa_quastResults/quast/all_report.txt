All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_113.final.contigs
#Contigs                                   3041                                                                                  
#Contigs (>= 0 bp)                         3041                                                                                  
#Contigs (>= 1000 bp)                      1713                                                                                  
Largest contig                             16799                                                                                 
Total length                               5235591                                                                               
Total length (>= 0 bp)                     5235591                                                                               
Total length (>= 1000 bp)                  4512580                                                                               
Reference length                           5594470                                                                               
N50                                        2709                                                                                  
NG50                                       2501                                                                                  
N75                                        1497                                                                                  
NG75                                       1255                                                                                  
L50                                        586                                                                                   
LG50                                       655                                                                                   
L75                                        1237                                                                                  
LG75                                       1432                                                                                  
#local misassemblies                       174                                                                                   
#misassemblies                             122                                                                                   
#misassembled contigs                      113                                                                                   
Misassembled contigs length                459933                                                                                
Misassemblies inter-contig overlap         200                                                                                   
#unaligned contigs                         0 + 10 part                                                                           
Unaligned contigs length                   670                                                                                   
#ambiguously mapped contigs                294                                                                                   
Extra bases in ambiguously mapped contigs  -97193                                                                                
#N's                                       2830                                                                                  
#N's per 100 kbp                           54.05                                                                                 
Genome fraction (%)                        88.774                                                                                
Duplication ratio                          1.035                                                                                 
#genes                                     3009 + 2017 part                                                                      
#predicted genes (unique)                  7731                                                                                  
#predicted genes (>= 0 bp)                 7744                                                                                  
#predicted genes (>= 300 bp)               5292                                                                                  
#predicted genes (>= 1500 bp)              311                                                                                   
#predicted genes (>= 3000 bp)              11                                                                                    
#mismatches                                129                                                                                   
#indels                                    1852                                                                                  
Indels length                              1908                                                                                  
#mismatches per 100 kbp                    2.60                                                                                  
#indels per 100 kbp                        37.29                                                                                 
GC (%)                                     50.60                                                                                 
Reference GC (%)                           50.48                                                                                 
Average %IDY                               99.346                                                                                
Largest alignment                          12805                                                                                 
NA50                                       2647                                                                                  
NGA50                                      2441                                                                                  
NA75                                       1468                                                                                  
NGA75                                      1221                                                                                  
LA50                                       602                                                                                   
LGA50                                      673                                                                                   
LA75                                       1268                                                                                  
LGA75                                      1468                                                                                  
#Mis_misassemblies                         122                                                                                   
#Mis_relocations                           116                                                                                   
#Mis_translocations                        0                                                                                     
#Mis_inversions                            6                                                                                     
#Mis_misassembled contigs                  113                                                                                   
Mis_Misassembled contigs length            459933                                                                                
#Mis_local misassemblies                   174                                                                                   
#Mis_short indels (<= 5 bp)                1852                                                                                  
#Mis_long indels (> 5 bp)                  0                                                                                     
#Una_fully unaligned contigs               0                                                                                     
Una_Fully unaligned length                 0                                                                                     
#Una_partially unaligned contigs           10                                                                                    
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             670                                                                                   
GAGE_Contigs #                             3041                                                                                  
GAGE_Min contig                            225                                                                                   
GAGE_Max contig                            16799                                                                                 
GAGE_N50                                   2501 COUNT: 655                                                                       
GAGE_Genome size                           5594470                                                                               
GAGE_Assembly size                         5235591                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               368771(6.59%)                                                                         
GAGE_Missing assembly bases                3769(0.07%)                                                                           
GAGE_Missing assembly contigs              1(0.03%)                                                                              
GAGE_Duplicated reference bases            47160                                                                                 
GAGE_Compressed reference bases            251770                                                                                
GAGE_Bad trim                              2028                                                                                  
GAGE_Avg idy                               99.95                                                                                 
GAGE_SNPs                                  98                                                                                    
GAGE_Indels < 5bp                          1705                                                                                  
GAGE_Indels >= 5                           116                                                                                   
GAGE_Inversions                            2                                                                                     
GAGE_Relocation                            27                                                                                    
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    3108                                                                                  
GAGE_Corrected assembly size               5185775                                                                               
GAGE_Min correct contig                    202                                                                                   
GAGE_Max correct contig                    12807                                                                                 
GAGE_Corrected N50                         2294 COUNT: 715                                                                       
