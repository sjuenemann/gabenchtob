All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K65.final_contigs
#Una_fully unaligned contigs      254                                                                                       
Una_Fully unaligned length        58487                                                                                     
#Una_partially unaligned contigs  1                                                                                         
#Una_with misassembly             0                                                                                         
#Una_both parts are significant   0                                                                                         
Una_Partially unaligned length    2                                                                                         
#N's                              0                                                                                         
