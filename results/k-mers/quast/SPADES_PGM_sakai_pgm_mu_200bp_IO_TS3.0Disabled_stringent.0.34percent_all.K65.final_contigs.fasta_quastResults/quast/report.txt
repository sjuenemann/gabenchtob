All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K65.final_contigs
#Contigs (>= 0 bp)             1275                                                                                      
#Contigs (>= 1000 bp)          191                                                                                       
Total length (>= 0 bp)         5442752                                                                                   
Total length (>= 1000 bp)      5184789                                                                                   
#Contigs                       808                                                                                       
Largest contig                 260865                                                                                    
Total length                   5393440                                                                                   
Reference length               5594470                                                                                   
GC (%)                         50.26                                                                                     
Reference GC (%)               50.48                                                                                     
N50                            99657                                                                                     
NG50                           97774                                                                                     
N75                            32833                                                                                     
NG75                           29114                                                                                     
#misassemblies                 3                                                                                         
#local misassemblies           12                                                                                        
#unaligned contigs             254 + 1 part                                                                              
Unaligned contigs length       58489                                                                                     
Genome fraction (%)            93.532                                                                                    
Duplication ratio              1.002                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        4.28                                                                                      
#indels per 100 kbp            27.79                                                                                     
#genes                         4819 + 234 part                                                                           
#predicted genes (unique)      6165                                                                                      
#predicted genes (>= 0 bp)     6170                                                                                      
#predicted genes (>= 300 bp)   4771                                                                                      
#predicted genes (>= 1500 bp)  537                                                                                       
#predicted genes (>= 3000 bp)  43                                                                                        
Largest alignment              247606                                                                                    
NA50                           99657                                                                                     
NGA50                          97774                                                                                     
NA75                           30749                                                                                     
NGA75                          28387                                                                                     
