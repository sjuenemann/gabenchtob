All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K103.final_contigs
GAGE_Contigs #                   2583                                                                                
GAGE_Min contig                  207                                                                                 
GAGE_Max contig                  182815                                                                              
GAGE_N50                         66035 COUNT: 22                                                                     
GAGE_Genome size                 4411532                                                                             
GAGE_Assembly size               5026803                                                                             
GAGE_Chaff bases                 0                                                                                   
GAGE_Missing reference bases     53356(1.21%)                                                                        
GAGE_Missing assembly bases      25623(0.51%)                                                                        
GAGE_Missing assembly contigs    55(2.13%)                                                                           
GAGE_Duplicated reference bases  685291                                                                              
GAGE_Compressed reference bases  56167                                                                               
GAGE_Bad trim                    9702                                                                                
GAGE_Avg idy                     99.95                                                                               
GAGE_SNPs                        250                                                                                 
GAGE_Indels < 5bp                1524                                                                                
GAGE_Indels >= 5                 19                                                                                  
GAGE_Inversions                  2                                                                                   
GAGE_Relocation                  13                                                                                  
GAGE_Translocation               0                                                                                   
GAGE_Corrected contig #          244                                                                                 
GAGE_Corrected assembly size     4323107                                                                             
GAGE_Min correct contig          210                                                                                 
GAGE_Max correct contig          182852                                                                              
GAGE_Corrected N50               53938 COUNT: 28                                                                     
