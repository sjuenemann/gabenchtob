All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_77.final.contig
#Contigs (>= 0 bp)             185043                                                                      
#Contigs (>= 1000 bp)          6                                                                           
Total length (>= 0 bp)         23470190                                                                    
Total length (>= 1000 bp)      6918                                                                        
#Contigs                       7795                                                                        
Largest contig                 1302                                                                        
Total length                   2212337                                                                     
Reference length               4411532                                                                     
GC (%)                         66.14                                                                       
Reference GC (%)               65.61                                                                       
N50                            269                                                                         
NG50                           200                                                                         
N75                            241                                                                         
NG75                           None                                                                        
#misassemblies                 26                                                                          
#local misassemblies           6                                                                           
#unaligned contigs             466 + 31 part                                                               
Unaligned contigs length       128305                                                                      
Genome fraction (%)            41.541                                                                      
Duplication ratio              1.135                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        35.96                                                                       
#indels per 100 kbp            800.66                                                                      
#genes                         58 + 3057 part                                                              
#predicted genes (unique)      6520                                                                        
#predicted genes (>= 0 bp)     6523                                                                        
#predicted genes (>= 300 bp)   1044                                                                        
#predicted genes (>= 1500 bp)  0                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              1225                                                                        
NA50                           265                                                                         
NGA50                          None                                                                        
NA75                           235                                                                         
NGA75                          None                                                                        
