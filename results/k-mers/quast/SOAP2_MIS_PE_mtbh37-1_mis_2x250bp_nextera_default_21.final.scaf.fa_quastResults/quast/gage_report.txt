All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_21.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_21.final.scaf
GAGE_Contigs #                   10007                                                                   10010                                                          
GAGE_Min contig                  200                                                                     200                                                            
GAGE_Max contig                  976                                                                     976                                                            
GAGE_N50                         202 COUNT: 9408                                                         202 COUNT: 9406                                                
GAGE_Genome size                 4411532                                                                 4411532                                                        
GAGE_Assembly size               2326225                                                                 2327255                                                        
GAGE_Chaff bases                 0                                                                       0                                                              
GAGE_Missing reference bases     4008643(90.87%)                                                         4007951(90.85%)                                                
GAGE_Missing assembly bases      1922487(82.64%)                                                         1922825(82.62%)                                                
GAGE_Missing assembly contigs    8465(84.59%)                                                            8465(84.57%)                                                   
GAGE_Duplicated reference bases  0                                                                       0                                                              
GAGE_Compressed reference bases  0                                                                       0                                                              
GAGE_Bad trim                    289                                                                     417                                                            
GAGE_Avg idy                     99.97                                                                   99.97                                                          
GAGE_SNPs                        123                                                                     123                                                            
GAGE_Indels < 5bp                4                                                                       4                                                              
GAGE_Indels >= 5                 1                                                                       3                                                              
GAGE_Inversions                  0                                                                       0                                                              
GAGE_Relocation                  0                                                                       0                                                              
GAGE_Translocation               0                                                                       0                                                              
GAGE_Corrected contig #          1539                                                                    1539                                                           
GAGE_Corrected assembly size     403234                                                                  403234                                                         
GAGE_Min correct contig          200                                                                     200                                                            
GAGE_Max correct contig          976                                                                     976                                                            
GAGE_Corrected N50               0 COUNT: 0                                                              0 COUNT: 0                                                     
