All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_67.final.contigs
#Contigs (>= 0 bp)             51221                                                                                 
#Contigs (>= 1000 bp)          0                                                                                     
Total length (>= 0 bp)         9210568                                                                               
Total length (>= 1000 bp)      0                                                                                     
#Contigs                       10885                                                                                 
Largest contig                 421                                                                                   
Total length                   2493162                                                                               
Reference length               2813862                                                                               
GC (%)                         31.90                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            223                                                                                   
NG50                           219                                                                                   
N75                            210                                                                                   
NG75                           206                                                                                   
#misassemblies                 81                                                                                    
#local misassemblies           0                                                                                     
#unaligned contigs             1870 + 83 part                                                                        
Unaligned contigs length       443166                                                                                
Genome fraction (%)            50.966                                                                                
Duplication ratio              1.425                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        150.27                                                                                
#indels per 100 kbp            2291.60                                                                               
#genes                         67 + 2186 part                                                                        
#predicted genes (unique)      7895                                                                                  
#predicted genes (>= 0 bp)     7903                                                                                  
#predicted genes (>= 300 bp)   4                                                                                     
#predicted genes (>= 1500 bp)  0                                                                                     
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              421                                                                                   
NA50                           216                                                                                   
NGA50                          212                                                                                   
NA75                           202                                                                                   
NGA75                          None                                                                                  
