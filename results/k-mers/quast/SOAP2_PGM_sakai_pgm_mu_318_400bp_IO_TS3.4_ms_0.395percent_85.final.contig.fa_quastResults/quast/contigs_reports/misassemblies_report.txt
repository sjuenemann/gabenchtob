All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_85.final.contig
#Mis_misassemblies               142                                                                      
#Mis_relocations                 134                                                                      
#Mis_translocations              8                                                                        
#Mis_inversions                  0                                                                        
#Mis_misassembled contigs        142                                                                      
Mis_Misassembled contigs length  40853                                                                    
#Mis_local misassemblies         3                                                                        
#mismatches                      764                                                                      
#indels                          19980                                                                    
#Mis_short indels (<= 5 bp)      19980                                                                    
#Mis_long indels (> 5 bp)        0                                                                        
Indels length                    20417                                                                    
