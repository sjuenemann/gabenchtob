All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_50-unitigs
GAGE_Contigs #                   2246                                                                      
GAGE_Min contig                  200                                                                       
GAGE_Max contig                  14564                                                                     
GAGE_N50                         2831 COUNT: 461                                                           
GAGE_Genome size                 4411532                                                                   
GAGE_Assembly size               4157046                                                                   
GAGE_Chaff bases                 0                                                                         
GAGE_Missing reference bases     239520(5.43%)                                                             
GAGE_Missing assembly bases      949(0.02%)                                                                
GAGE_Missing assembly contigs    2(0.09%)                                                                  
GAGE_Duplicated reference bases  0                                                                         
GAGE_Compressed reference bases  21287                                                                     
GAGE_Bad trim                    119                                                                       
GAGE_Avg idy                     99.98                                                                     
GAGE_SNPs                        79                                                                        
GAGE_Indels < 5bp                660                                                                       
GAGE_Indels >= 5                 7                                                                         
GAGE_Inversions                  0                                                                         
GAGE_Relocation                  6                                                                         
GAGE_Translocation               0                                                                         
GAGE_Corrected contig #          2256                                                                      
GAGE_Corrected assembly size     4157946                                                                   
GAGE_Min correct contig          200                                                                       
GAGE_Max correct contig          14565                                                                     
GAGE_Corrected N50               2787 COUNT: 464                                                           
