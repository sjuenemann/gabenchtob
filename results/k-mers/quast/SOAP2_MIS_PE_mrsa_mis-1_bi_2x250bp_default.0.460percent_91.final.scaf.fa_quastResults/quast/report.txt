All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_91.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_91.final.scaf
#Contigs (>= 0 bp)             1915                                                                          1861                                                                 
#Contigs (>= 1000 bp)          200                                                                           178                                                                  
Total length (>= 0 bp)         3048564                                                                       3050146                                                              
Total length (>= 1000 bp)      2742135                                                                       2755483                                                              
#Contigs                       300                                                                           254                                                                  
Largest contig                 71692                                                                         78063                                                                
Total length                   2781896                                                                       2784785                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         32.70                                                                         32.70                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            23194                                                                         24897                                                                
NG50                           23194                                                                         24897                                                                
N75                            11743                                                                         14720                                                                
NG75                           11574                                                                         13257                                                                
#misassemblies                 3                                                                             5                                                                    
#local misassemblies           19                                                                            50                                                                   
#unaligned contigs             9 + 2 part                                                                    10 + 2 part                                                          
Unaligned contigs length       8861                                                                          10228                                                                
Genome fraction (%)            98.195                                                                        98.257                                                               
Duplication ratio              1.002                                                                         1.002                                                                
#N's per 100 kbp               3.45                                                                          60.26                                                                
#mismatches per 100 kbp        2.42                                                                          2.42                                                                 
#indels per 100 kbp            0.98                                                                          4.85                                                                 
#genes                         2509 + 188 part                                                               2519 + 175 part                                                      
#predicted genes (unique)      2818                                                                          2799                                                                 
#predicted genes (>= 0 bp)     2819                                                                          2800                                                                 
#predicted genes (>= 300 bp)   2350                                                                          2342                                                                 
#predicted genes (>= 1500 bp)  282                                                                           283                                                                  
#predicted genes (>= 3000 bp)  29                                                                            29                                                                   
Largest alignment              71692                                                                         78038                                                                
NA50                           23192                                                                         24897                                                                
NGA50                          23192                                                                         24897                                                                
NA75                           11743                                                                         14486                                                                
NGA75                          11574                                                                         13242                                                                
