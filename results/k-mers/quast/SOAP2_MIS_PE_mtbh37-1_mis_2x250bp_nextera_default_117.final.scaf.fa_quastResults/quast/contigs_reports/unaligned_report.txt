All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_117.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_117.final.scaf
#Una_fully unaligned contigs      2                                                                        2                                                               
Una_Fully unaligned length        491                                                                      491                                                             
#Una_partially unaligned contigs  5                                                                        4                                                               
#Una_with misassembly             0                                                                        0                                                               
#Una_both parts are significant   1                                                                        0                                                               
Una_Partially unaligned length    1079                                                                     1034                                                            
#N's                              72                                                                       24876                                                           
