All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K127.final_contigs
#Una_fully unaligned contigs      236                                                                                 
Una_Fully unaligned length        74551                                                                               
#Una_partially unaligned contigs  67                                                                                  
#Una_with misassembly             0                                                                                   
#Una_both parts are significant   0                                                                                   
Una_Partially unaligned length    4615                                                                                
#N's                              0                                                                                   
