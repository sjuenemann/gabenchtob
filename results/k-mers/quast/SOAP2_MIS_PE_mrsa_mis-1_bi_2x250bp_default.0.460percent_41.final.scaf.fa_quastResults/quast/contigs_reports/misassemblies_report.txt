All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_41.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_41.final.scaf
#Mis_misassemblies               1                                                                             1                                                                    
#Mis_relocations                 1                                                                             1                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        1                                                                             1                                                                    
Mis_Misassembled contigs length  1584                                                                          1916                                                                 
#Mis_local misassemblies         29                                                                            290                                                                  
#mismatches                      31                                                                            29                                                                   
#indels                          507                                                                           2282                                                                 
#Mis_short indels (<= 5 bp)      377                                                                           1923                                                                 
#Mis_long indels (> 5 bp)        130                                                                           359                                                                  
Indels length                    4668                                                                          12040                                                                
