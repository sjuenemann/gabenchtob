All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_41.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_41.final.scaf
GAGE_Contigs #                   6963                                                                          6353                                                                 
GAGE_Min contig                  200                                                                           200                                                                  
GAGE_Max contig                  4483                                                                          7466                                                                 
GAGE_N50                         847 COUNT: 977                                                                1209 COUNT: 668                                                      
GAGE_Genome size                 2813862                                                                       2813862                                                              
GAGE_Assembly size               3316220                                                                       3396820                                                              
GAGE_Chaff bases                 0                                                                             0                                                                    
GAGE_Missing reference bases     370514(13.17%)                                                                338859(12.04%)                                                       
GAGE_Missing assembly bases      815868(24.60%)                                                                860759(25.34%)                                                       
GAGE_Missing assembly contigs    3408(48.94%)                                                                  3418(53.80%)                                                         
GAGE_Duplicated reference bases  15934                                                                         16598                                                                
GAGE_Compressed reference bases  5519                                                                          6497                                                                 
GAGE_Bad trim                    2634                                                                          7960                                                                 
GAGE_Avg idy                     99.96                                                                         99.89                                                                
GAGE_SNPs                        386                                                                           384                                                                  
GAGE_Indels < 5bp                331                                                                           451                                                                  
GAGE_Indels >= 5                 243                                                                           1061                                                                 
GAGE_Inversions                  0                                                                             0                                                                    
GAGE_Relocation                  1                                                                             2                                                                    
GAGE_Translocation               0                                                                             0                                                                    
GAGE_Corrected contig #          3618                                                                          3557                                                                 
GAGE_Corrected assembly size     2469269                                                                       2464223                                                              
GAGE_Min correct contig          200                                                                           200                                                                  
GAGE_Max correct contig          4482                                                                          4482                                                                 
GAGE_Corrected N50               776 COUNT: 1048                                                               795 COUNT: 1020                                                      
