All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K75.final_contigs
GAGE_Contigs #                   2045                                                                                       
GAGE_Min contig                  209                                                                                        
GAGE_Max contig                  207827                                                                                     
GAGE_N50                         73240 COUNT: 11                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               3310403                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     7681(0.27%)                                                                                
GAGE_Missing assembly bases      154012(4.65%)                                                                              
GAGE_Missing assembly contigs    461(22.54%)                                                                                
GAGE_Duplicated reference bases  383679                                                                                     
GAGE_Compressed reference bases  42446                                                                                      
GAGE_Bad trim                    16247                                                                                      
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        56                                                                                         
GAGE_Indels < 5bp                232                                                                                        
GAGE_Indels >= 5                 10                                                                                         
GAGE_Inversions                  1                                                                                          
GAGE_Relocation                  3                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          152                                                                                        
GAGE_Corrected assembly size     2775125                                                                                    
GAGE_Min correct contig          209                                                                                        
GAGE_Max correct contig          207845                                                                                     
GAGE_Corrected N50               53147 COUNT: 14                                                                            
