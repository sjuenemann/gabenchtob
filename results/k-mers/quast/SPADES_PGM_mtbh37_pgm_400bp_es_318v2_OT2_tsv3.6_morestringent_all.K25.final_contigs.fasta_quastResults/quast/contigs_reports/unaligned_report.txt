All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K25.final_contigs
#Una_fully unaligned contigs      11                                                                                 
Una_Fully unaligned length        3958                                                                               
#Una_partially unaligned contigs  8                                                                                  
#Una_with misassembly             0                                                                                  
#Una_both parts are significant   1                                                                                  
Una_Partially unaligned length    1075                                                                               
#N's                              0                                                                                  
