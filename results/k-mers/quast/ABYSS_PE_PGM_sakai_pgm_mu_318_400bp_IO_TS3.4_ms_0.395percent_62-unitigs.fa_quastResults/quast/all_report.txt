All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_62-unitigs
#Contigs                                   1200                                                                   
#Contigs (>= 0 bp)                         6877                                                                   
#Contigs (>= 1000 bp)                      680                                                                    
Largest contig                             58384                                                                  
Total length                               5254929                                                                
Total length (>= 0 bp)                     5840796                                                                
Total length (>= 1000 bp)                  5028720                                                                
Reference length                           5594470                                                                
N50                                        11858                                                                  
NG50                                       11144                                                                  
N75                                        5824                                                                   
NG75                                       4795                                                                   
L50                                        138                                                                    
LG50                                       152                                                                    
L75                                        291                                                                    
LG75                                       338                                                                    
#local misassemblies                       6                                                                      
#misassemblies                             2                                                                      
#misassembled contigs                      2                                                                      
Misassembled contigs length                20604                                                                  
Misassemblies inter-contig overlap         650                                                                    
#unaligned contigs                         0 + 1 part                                                             
Unaligned contigs length                   60                                                                     
#ambiguously mapped contigs                122                                                                    
Extra bases in ambiguously mapped contigs  -60256                                                                 
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        92.709                                                                 
Duplication ratio                          1.002                                                                  
#genes                                     4319 + 705 part                                                        
#predicted genes (unique)                  5811                                                                   
#predicted genes (>= 0 bp)                 5812                                                                   
#predicted genes (>= 300 bp)               4570                                                                   
#predicted genes (>= 1500 bp)              576                                                                    
#predicted genes (>= 3000 bp)              52                                                                     
#mismatches                                65                                                                     
#indels                                    227                                                                    
Indels length                              231                                                                    
#mismatches per 100 kbp                    1.25                                                                   
#indels per 100 kbp                        4.38                                                                   
GC (%)                                     50.24                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.905                                                                 
Largest alignment                          58384                                                                  
NA50                                       11858                                                                  
NGA50                                      11094                                                                  
NA75                                       5819                                                                   
NGA75                                      4772                                                                   
LA50                                       138                                                                    
LGA50                                      153                                                                    
LA75                                       292                                                                    
LGA75                                      339                                                                    
#Mis_misassemblies                         2                                                                      
#Mis_relocations                           2                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  2                                                                      
Mis_Misassembled contigs length            20604                                                                  
#Mis_local misassemblies                   6                                                                      
#Mis_short indels (<= 5 bp)                227                                                                    
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           1                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             60                                                                     
GAGE_Contigs #                             1200                                                                   
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            58384                                                                  
GAGE_N50                                   11144 COUNT: 152                                                       
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5254929                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               235256(4.21%)                                                          
GAGE_Missing assembly bases                66(0.00%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            2831                                                                   
GAGE_Compressed reference bases            128242                                                                 
GAGE_Bad trim                              66                                                                     
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  109                                                                    
GAGE_Indels < 5bp                          233                                                                    
GAGE_Indels >= 5                           2                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            6                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    1194                                                                   
GAGE_Corrected assembly size               5252466                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    58384                                                                  
GAGE_Corrected N50                         11015 COUNT: 154                                                       
