All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K105.final_contigs
GAGE_Contigs #                   1361                                                                                       
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  340076                                                                                     
GAGE_N50                         97172 COUNT: 17                                                                            
GAGE_Genome size                 5594470                                                                                    
GAGE_Assembly size               5629284                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     12653(0.23%)                                                                               
GAGE_Missing assembly bases      10062(0.18%)                                                                               
GAGE_Missing assembly contigs    6(0.44%)                                                                                   
GAGE_Duplicated reference bases  271361                                                                                     
GAGE_Compressed reference bases  287947                                                                                     
GAGE_Bad trim                    8501                                                                                       
GAGE_Avg idy                     99.95                                                                                      
GAGE_SNPs                        342                                                                                        
GAGE_Indels < 5bp                1888                                                                                       
GAGE_Indels >= 5                 10                                                                                         
GAGE_Inversions                  1                                                                                          
GAGE_Relocation                  9                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          337                                                                                        
GAGE_Corrected assembly size     5357983                                                                                    
GAGE_Min correct contig          202                                                                                        
GAGE_Max correct contig          224383                                                                                     
GAGE_Corrected N50               94938 COUNT: 20                                                                            
