All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_77.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_77.final.scaf
#Contigs                                   8293                                                                          8023                                                                 
#Contigs (>= 0 bp)                         11197                                                                         10892                                                                
#Contigs (>= 1000 bp)                      825                                                                           728                                                                  
Largest contig                             16589                                                                         17948                                                                
Total length                               4483917                                                                       4499939                                                              
Total length (>= 0 bp)                     4913112                                                                       4923375                                                              
Total length (>= 1000 bp)                  2457780                                                                       2568908                                                              
Reference length                           2813862                                                                       2813862                                                              
N50                                        1408                                                                          1848                                                                 
NG50                                       3213                                                                          4122                                                                 
N75                                        251                                                                           251                                                                  
NG75                                       1657                                                                          2216                                                                 
L50                                        645                                                                           498                                                                  
LG50                                       257                                                                           202                                                                  
L75                                        3702                                                                          3419                                                                 
LG75                                       559                                                                           430                                                                  
#local misassemblies                       9                                                                             58                                                                   
#misassemblies                             5                                                                             7                                                                    
#misassembled contigs                      5                                                                             7                                                                    
Misassembled contigs length                7213                                                                          8837                                                                 
Misassemblies inter-contig overlap         144                                                                           327                                                                  
#unaligned contigs                         4951 + 454 part                                                               4958 + 464 part                                                      
Unaligned contigs length                   1278247                                                                       1298073                                                              
#ambiguously mapped contigs                51                                                                            51                                                                   
Extra bases in ambiguously mapped contigs  -13249                                                                        -13249                                                               
#N's                                       512                                                                           10775                                                                
#N's per 100 kbp                           11.42                                                                         239.45                                                               
Genome fraction (%)                        97.691                                                                        97.442                                                               
Duplication ratio                          1.161                                                                         1.163                                                                
#genes                                     1809 + 871 part                                                               1932 + 739 part                                                      
#predicted genes (unique)                  10060                                                                         9980                                                                 
#predicted genes (>= 0 bp)                 10078                                                                         9995                                                                 
#predicted genes (>= 300 bp)               2757                                                                          2745                                                                 
#predicted genes (>= 1500 bp)              159                                                                           166                                                                  
#predicted genes (>= 3000 bp)              6                                                                             6                                                                    
#mismatches                                2340                                                                          1926                                                                 
#indels                                    269                                                                           2324                                                                 
Indels length                              3010                                                                          10517                                                                
#mismatches per 100 kbp                    85.12                                                                         70.24                                                                
#indels per 100 kbp                        9.79                                                                          84.76                                                                
GC (%)                                     34.27                                                                         34.27                                                                
Reference GC (%)                           32.81                                                                         32.81                                                                
Average %IDY                               98.323                                                                        98.144                                                               
Largest alignment                          16589                                                                         17948                                                                
NA50                                       1403                                                                          1805                                                                 
NGA50                                      3208                                                                          4093                                                                 
NA75                                       None                                                                          None                                                                 
NGA75                                      1652                                                                          2186                                                                 
LA50                                       647                                                                           503                                                                  
LGA50                                      258                                                                           203                                                                  
LA75                                       None                                                                          None                                                                 
LGA75                                      560                                                                           434                                                                  
#Mis_misassemblies                         5                                                                             7                                                                    
#Mis_relocations                           5                                                                             7                                                                    
#Mis_translocations                        0                                                                             0                                                                    
#Mis_inversions                            0                                                                             0                                                                    
#Mis_misassembled contigs                  5                                                                             7                                                                    
Mis_Misassembled contigs length            7213                                                                          8837                                                                 
#Mis_local misassemblies                   9                                                                             58                                                                   
#Mis_short indels (<= 5 bp)                209                                                                           2117                                                                 
#Mis_long indels (> 5 bp)                  60                                                                            207                                                                  
#Una_fully unaligned contigs               4951                                                                          4958                                                                 
Una_Fully unaligned length                 1240276                                                                       1252288                                                              
#Una_partially unaligned contigs           454                                                                           464                                                                  
#Una_with misassembly                      0                                                                             0                                                                    
#Una_both parts are significant            0                                                                             6                                                                    
Una_Partially unaligned length             37971                                                                         45785                                                                
GAGE_Contigs #                             8293                                                                          8023                                                                 
GAGE_Min contig                            200                                                                           200                                                                  
GAGE_Max contig                            16589                                                                         17948                                                                
GAGE_N50                                   3213 COUNT: 257                                                               4122 COUNT: 202                                                      
GAGE_Genome size                           2813862                                                                       2813862                                                              
GAGE_Assembly size                         4483917                                                                       4499939                                                              
GAGE_Chaff bases                           0                                                                             0                                                                    
GAGE_Missing reference bases               23181(0.82%)                                                                  21257(0.76%)                                                         
GAGE_Missing assembly bases                1116262(24.89%)                                                               1126592(25.04%)                                                      
GAGE_Missing assembly contigs              4281(51.62%)                                                                  4279(53.33%)                                                         
GAGE_Duplicated reference bases            522349                                                                        525973                                                               
GAGE_Compressed reference bases            49606                                                                         49422                                                                
GAGE_Bad trim                              46911                                                                         47757                                                                
GAGE_Avg idy                               99.94                                                                         99.93                                                                
GAGE_SNPs                                  745                                                                           713                                                                  
GAGE_Indels < 5bp                          162                                                                           187                                                                  
GAGE_Indels >= 5                           60                                                                            273                                                                  
GAGE_Inversions                            0                                                                             0                                                                    
GAGE_Relocation                            4                                                                             5                                                                    
GAGE_Translocation                         0                                                                             0                                                                    
GAGE_Corrected contig #                    1733                                                                          1720                                                                 
GAGE_Corrected assembly size               2835520                                                                       2834187                                                              
GAGE_Min correct contig                    200                                                                           200                                                                  
GAGE_Max correct contig                    14969                                                                         14969                                                                
GAGE_Corrected N50                         3052 COUNT: 275                                                               3053 COUNT: 274                                                      
