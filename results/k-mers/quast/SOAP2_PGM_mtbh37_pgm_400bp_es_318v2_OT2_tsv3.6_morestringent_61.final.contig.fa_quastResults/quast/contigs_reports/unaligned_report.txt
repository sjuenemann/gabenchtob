All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_61.final.contig
#Una_fully unaligned contigs      2349                                                                        
Una_Fully unaligned length        528990                                                                      
#Una_partially unaligned contigs  3                                                                           
#Una_with misassembly             0                                                                           
#Una_both parts are significant   0                                                                           
Una_Partially unaligned length    189                                                                         
#N's                              0                                                                           
