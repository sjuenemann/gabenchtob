All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_21.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_21.final.scaf
#Contigs (>= 0 bp)             9676                                                                          4547                                                                 
#Contigs (>= 1000 bp)          676                                                                           305                                                                  
Total length (>= 0 bp)         3108384                                                                       3342990                                                              
Total length (>= 1000 bp)      1053095                                                                       2707376                                                              
#Contigs                       3475                                                                          606                                                                  
Largest contig                 5061                                                                          76735                                                                
Total length                   2369210                                                                       2819610                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         32.37                                                                         32.50                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            892                                                                           13826                                                                
NG50                           746                                                                           13855                                                                
N75                            517                                                                           7170                                                                 
NG75                           335                                                                           7181                                                                 
#misassemblies                 0                                                                             4                                                                    
#local misassemblies           6                                                                             347                                                                  
#unaligned contigs             51 + 22 part                                                                  108 + 89 part                                                        
Unaligned contigs length       19679                                                                         561825                                                               
Genome fraction (%)            83.315                                                                        45.976                                                               
Duplication ratio              1.001                                                                         1.744                                                                
#N's per 100 kbp               59.77                                                                         8374.42                                                              
#mismatches per 100 kbp        2.77                                                                          2.48                                                                 
#indels per 100 kbp            31.69                                                                         976.70                                                               
#genes                         712 + 1809 part                                                               895 + 606 part                                                       
#predicted genes (unique)      4902                                                                          4498                                                                 
#predicted genes (>= 0 bp)     4902                                                                          4498                                                                 
#predicted genes (>= 300 bp)   2679                                                                          2737                                                                 
#predicted genes (>= 1500 bp)  22                                                                            43                                                                   
#predicted genes (>= 3000 bp)  0                                                                             0                                                                    
Largest alignment              5061                                                                          32717                                                                
NA50                           887                                                                           None                                                                 
NGA50                          739                                                                           None                                                                 
NA75                           513                                                                           None                                                                 
NGA75                          329                                                                           None                                                                 
