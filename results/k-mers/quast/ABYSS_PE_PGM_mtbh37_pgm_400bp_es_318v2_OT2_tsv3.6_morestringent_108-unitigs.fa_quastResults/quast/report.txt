All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_108-unitigs
#Contigs (>= 0 bp)             9052                                                                       
#Contigs (>= 1000 bp)          1364                                                                       
Total length (>= 0 bp)         5392915                                                                    
Total length (>= 1000 bp)      3598477                                                                    
#Contigs                       6583                                                                       
Largest contig                 19198                                                                      
Total length                   5022715                                                                    
Reference length               4411532                                                                    
GC (%)                         64.81                                                                      
Reference GC (%)               65.61                                                                      
N50                            2198                                                                       
NG50                           2545                                                                       
N75                            782                                                                        
NG75                           1302                                                                       
#misassemblies                 6                                                                          
#local misassemblies           12                                                                         
#unaligned contigs             4 + 1 part                                                                 
Unaligned contigs length       1227                                                                       
Genome fraction (%)            95.594                                                                     
Duplication ratio              1.183                                                                      
#N's per 100 kbp               0.00                                                                       
#mismatches per 100 kbp        2.68                                                                       
#indels per 100 kbp            34.19                                                                      
#genes                         2365 + 1684 part                                                           
#predicted genes (unique)      9552                                                                       
#predicted genes (>= 0 bp)     9625                                                                       
#predicted genes (>= 300 bp)   4196                                                                       
#predicted genes (>= 1500 bp)  269                                                                        
#predicted genes (>= 3000 bp)  13                                                                         
Largest alignment              19198                                                                      
NA50                           2198                                                                       
NGA50                          2545                                                                       
NA75                           777                                                                        
NGA75                          1302                                                                       
