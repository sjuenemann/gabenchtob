All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_163.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_163.final.contigs
#Contigs (>= 0 bp)             519                                                                                      402                                                                             
#Contigs (>= 1000 bp)          305                                                                                      210                                                                             
Total length (>= 0 bp)         5464565                                                                                  5479362                                                                         
Total length (>= 1000 bp)      5360303                                                                                  5388102                                                                         
#Contigs                       518                                                                                      402                                                                             
Largest contig                 150466                                                                                   240649                                                                          
Total length                   5464418                                                                                  5479362                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.36                                                                                    50.36                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            33962                                                                                    67085                                                                           
NG50                           32090                                                                                    65010                                                                           
N75                            17767                                                                                    32804                                                                           
NG75                           16122                                                                                    31198                                                                           
#misassemblies                 6                                                                                        12                                                                              
#local misassemblies           2                                                                                        102                                                                             
#unaligned contigs             1 + 0 part                                                                               1 + 0 part                                                                      
Unaligned contigs length       5548                                                                                     5548                                                                            
Genome fraction (%)            95.457                                                                                   95.586                                                                          
Duplication ratio              1.002                                                                                    1.005                                                                           
#N's per 100 kbp               0.00                                                                                     270.05                                                                          
#mismatches per 100 kbp        2.45                                                                                     2.66                                                                            
#indels per 100 kbp            0.34                                                                                     8.51                                                                            
#genes                         4949 + 327 part                                                                          4960 + 321 part                                                                 
#predicted genes (unique)      5547                                                                                     5532                                                                            
#predicted genes (>= 0 bp)     5572                                                                                     5557                                                                            
#predicted genes (>= 300 bp)   4649                                                                                     4647                                                                            
#predicted genes (>= 1500 bp)  653                                                                                      653                                                                             
#predicted genes (>= 3000 bp)  59                                                                                       59                                                                              
Largest alignment              150466                                                                                   239826                                                                          
NA50                           33962                                                                                    66911                                                                           
NGA50                          32090                                                                                    64844                                                                           
NA75                           17605                                                                                    31198                                                                           
NGA75                          16100                                                                                    30855                                                                           
