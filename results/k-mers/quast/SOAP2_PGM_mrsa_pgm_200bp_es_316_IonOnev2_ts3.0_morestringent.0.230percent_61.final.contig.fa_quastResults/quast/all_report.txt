All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_61.final.contig
#Contigs                                   11106                                                                                    
#Contigs (>= 0 bp)                         52913                                                                                    
#Contigs (>= 1000 bp)                      42                                                                                       
Largest contig                             1759                                                                                     
Total length                               3256597                                                                                  
Total length (>= 0 bp)                     8144243                                                                                  
Total length (>= 1000 bp)                  49771                                                                                    
Reference length                           2813862                                                                                  
N50                                        271                                                                                      
NG50                                       307                                                                                      
N75                                        218                                                                                      
NG75                                       230                                                                                      
L50                                        3744                                                                                     
LG50                                       2975                                                                                     
L75                                        7184                                                                                     
LG75                                       5698                                                                                     
#local misassemblies                       1                                                                                        
#misassemblies                             0                                                                                        
#misassembled contigs                      0                                                                                        
Misassembled contigs length                0                                                                                        
Misassemblies inter-contig overlap         195                                                                                      
#unaligned contigs                         5327 + 10 part                                                                           
Unaligned contigs length                   1157301                                                                                  
#ambiguously mapped contigs                3                                                                                        
Extra bases in ambiguously mapped contigs  -736                                                                                     
#N's                                       0                                                                                        
#N's per 100 kbp                           0.00                                                                                     
Genome fraction (%)                        71.650                                                                                   
Duplication ratio                          1.041                                                                                    
#genes                                     198 + 2261 part                                                                          
#predicted genes (unique)                  8987                                                                                     
#predicted genes (>= 0 bp)                 8998                                                                                     
#predicted genes (>= 300 bp)               2199                                                                                     
#predicted genes (>= 1500 bp)              0                                                                                        
#predicted genes (>= 3000 bp)              0                                                                                        
#mismatches                                91                                                                                       
#indels                                    394                                                                                      
Indels length                              418                                                                                      
#mismatches per 100 kbp                    4.51                                                                                     
#indels per 100 kbp                        19.54                                                                                    
GC (%)                                     32.35                                                                                    
Reference GC (%)                           32.81                                                                                    
Average %IDY                               99.966                                                                                   
Largest alignment                          1759                                                                                     
NA50                                       270                                                                                      
NGA50                                      306                                                                                      
NA75                                       None                                                                                     
NGA75                                      None                                                                                     
LA50                                       3748                                                                                     
LGA50                                      2976                                                                                     
LA75                                       None                                                                                     
LGA75                                      None                                                                                     
#Mis_misassemblies                         0                                                                                        
#Mis_relocations                           0                                                                                        
#Mis_translocations                        0                                                                                        
#Mis_inversions                            0                                                                                        
#Mis_misassembled contigs                  0                                                                                        
Mis_Misassembled contigs length            0                                                                                        
#Mis_local misassemblies                   1                                                                                        
#Mis_short indels (<= 5 bp)                394                                                                                      
#Mis_long indels (> 5 bp)                  0                                                                                        
#Una_fully unaligned contigs               5327                                                                                     
Una_Fully unaligned length                 1156821                                                                                  
#Una_partially unaligned contigs           10                                                                                       
#Una_with misassembly                      0                                                                                        
#Una_both parts are significant            0                                                                                        
Una_Partially unaligned length             480                                                                                      
GAGE_Contigs #                             11106                                                                                    
GAGE_Min contig                            200                                                                                      
GAGE_Max contig                            1759                                                                                     
GAGE_N50                                   307 COUNT: 2975                                                                          
GAGE_Genome size                           2813862                                                                                  
GAGE_Assembly size                         3256597                                                                                  
GAGE_Chaff bases                           0                                                                                        
GAGE_Missing reference bases               556117(19.76%)                                                                           
GAGE_Missing assembly bases                450448(13.83%)                                                                           
GAGE_Missing assembly contigs              1771(15.95%)                                                                             
GAGE_Duplicated reference bases            352251                                                                                   
GAGE_Compressed reference bases            19925                                                                                    
GAGE_Bad trim                              65880                                                                                    
GAGE_Avg idy                               99.48                                                                                    
GAGE_SNPs                                  862                                                                                      
GAGE_Indels < 5bp                          7080                                                                                     
GAGE_Indels >= 5                           1                                                                                        
GAGE_Inversions                            15                                                                                       
GAGE_Relocation                            2                                                                                        
GAGE_Translocation                         0                                                                                        
GAGE_Corrected contig #                    6883                                                                                     
GAGE_Corrected assembly size               2338515                                                                                  
GAGE_Min correct contig                    200                                                                                      
GAGE_Max correct contig                    1759                                                                                     
GAGE_Corrected N50                         307 COUNT: 2977                                                                          
