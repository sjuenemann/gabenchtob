All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_213.final.contigs
#Mis_misassemblies               6                                                                                      
#Mis_relocations                 6                                                                                      
#Mis_translocations              0                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        6                                                                                      
Mis_Misassembled contigs length  30919                                                                                  
#Mis_local misassemblies         1                                                                                      
#mismatches                      134                                                                                    
#indels                          849                                                                                    
#Mis_short indels (<= 5 bp)      849                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    869                                                                                    
