All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_43.final.contigs
#Contigs                                   7080                                                                          
#Contigs (>= 0 bp)                         15986                                                                         
#Contigs (>= 1000 bp)                      202                                                                           
Largest contig                             3740                                                                          
Total length                               2860408                                                                       
Total length (>= 0 bp)                     4023206                                                                       
Total length (>= 1000 bp)                  261513                                                                        
Reference length                           4411532                                                                       
N50                                        432                                                                           
NG50                                       288                                                                           
N75                                        296                                                                           
NG75                                       None                                                                          
L50                                        2121                                                                          
LG50                                       4345                                                                          
L75                                        4137                                                                          
LG75                                       None                                                                          
#local misassemblies                       1                                                                             
#misassemblies                             0                                                                             
#misassembled contigs                      0                                                                             
Misassembled contigs length                0                                                                             
Misassemblies inter-contig overlap         153                                                                           
#unaligned contigs                         1 + 8 part                                                                    
Unaligned contigs length                   719                                                                           
#ambiguously mapped contigs                22                                                                            
Extra bases in ambiguously mapped contigs  -6449                                                                         
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        64.207                                                                        
Duplication ratio                          1.007                                                                         
#genes                                     235 + 3391 part                                                               
#predicted genes (unique)                  7849                                                                          
#predicted genes (>= 0 bp)                 7849                                                                          
#predicted genes (>= 300 bp)               3117                                                                          
#predicted genes (>= 1500 bp)              7                                                                             
#predicted genes (>= 3000 bp)              0                                                                             
#mismatches                                137                                                                           
#indels                                    1528                                                                          
Indels length                              1631                                                                          
#mismatches per 100 kbp                    4.84                                                                          
#indels per 100 kbp                        53.95                                                                         
GC (%)                                     66.57                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.934                                                                        
Largest alignment                          3740                                                                          
NA50                                       431                                                                           
NGA50                                      287                                                                           
NA75                                       295                                                                           
NGA75                                      None                                                                          
LA50                                       2121                                                                          
LGA50                                      4348                                                                          
LA75                                       4140                                                                          
LGA75                                      None                                                                          
#Mis_misassemblies                         0                                                                             
#Mis_relocations                           0                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  0                                                                             
Mis_Misassembled contigs length            0                                                                             
#Mis_local misassemblies                   1                                                                             
#Mis_short indels (<= 5 bp)                1528                                                                          
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               1                                                                             
Una_Fully unaligned length                 435                                                                           
#Una_partially unaligned contigs           8                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             284                                                                           
GAGE_Contigs #                             7080                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            3740                                                                          
GAGE_N50                                   288 COUNT: 4345                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         2860408                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               1565115(35.48%)                                                               
GAGE_Missing assembly bases                450(0.02%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            0                                                                             
GAGE_Compressed reference bases            7180                                                                          
GAGE_Bad trim                              450                                                                           
GAGE_Avg idy                               99.94                                                                         
GAGE_SNPs                                  129                                                                           
GAGE_Indels < 5bp                          1614                                                                          
GAGE_Indels >= 5                           1                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            0                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    7077                                                                          
GAGE_Corrected assembly size               2860268                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    3741                                                                          
GAGE_Corrected N50                         288 COUNT: 4343                                                               
