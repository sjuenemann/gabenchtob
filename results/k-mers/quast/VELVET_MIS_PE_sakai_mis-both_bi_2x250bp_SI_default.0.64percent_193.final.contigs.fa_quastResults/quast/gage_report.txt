All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_193.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_193.final.contigs
GAGE_Contigs #                   1130                                                                                     763                                                                             
GAGE_Min contig                  327                                                                                      385                                                                             
GAGE_Max contig                  40830                                                                                    53113                                                                           
GAGE_N50                         9083 COUNT: 192                                                                          15375 COUNT: 110                                                                
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               5495421                                                                                  5546694                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     30392(0.54%)                                                                             30374(0.54%)                                                                    
GAGE_Missing assembly bases      5899(0.11%)                                                                              56981(1.03%)                                                                    
GAGE_Missing assembly contigs    1(0.09%)                                                                                 1(0.13%)                                                                        
GAGE_Duplicated reference bases  16654                                                                                    18212                                                                           
GAGE_Compressed reference bases  238944                                                                                   238750                                                                          
GAGE_Bad trim                    321                                                                                      520                                                                             
GAGE_Avg idy                     99.99                                                                                    99.99                                                                           
GAGE_SNPs                        172                                                                                      175                                                                             
GAGE_Indels < 5bp                18                                                                                       19                                                                              
GAGE_Indels >= 5                 3                                                                                        359                                                                             
GAGE_Inversions                  0                                                                                        1                                                                               
GAGE_Relocation                  3                                                                                        3                                                                               
GAGE_Translocation               1                                                                                        1                                                                               
GAGE_Corrected contig #          1111                                                                                     1110                                                                            
GAGE_Corrected assembly size     5474890                                                                                  5474887                                                                         
GAGE_Min correct contig          385                                                                                      385                                                                             
GAGE_Max correct contig          40830                                                                                    40830                                                                           
GAGE_Corrected N50               9052 COUNT: 193                                                                          9052 COUNT: 193                                                                 
