All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_119.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_119.final.scaf
#Contigs                                   1522                                                                            686                                                                    
#Contigs (>= 0 bp)                         1982                                                                            1107                                                                   
#Contigs (>= 1000 bp)                      762                                                                             203                                                                    
Largest contig                             57827                                                                           200086                                                                 
Total length                               5439806                                                                         5478746                                                                
Total length (>= 0 bp)                     5510091                                                                         5542556                                                                
Total length (>= 1000 bp)                  5146727                                                                         5319715                                                                
Reference length                           5594470                                                                         5594470                                                                
N50                                        9390                                                                            72226                                                                  
NG50                                       9112                                                                            72226                                                                  
N75                                        4788                                                                            31244                                                                  
NG75                                       4481                                                                            30924                                                                  
L50                                        166                                                                             26                                                                     
LG50                                       174                                                                             26                                                                     
L75                                        360                                                                             56                                                                     
LG75                                       385                                                                             59                                                                     
#local misassemblies                       4                                                                               113                                                                    
#misassemblies                             4                                                                               16                                                                     
#misassembled contigs                      4                                                                               15                                                                     
Misassembled contigs length                75920                                                                           292898                                                                 
Misassemblies inter-contig overlap         29                                                                              29                                                                     
#unaligned contigs                         4 + 0 part                                                                      16 + 13 part                                                           
Unaligned contigs length                   5974                                                                            31484                                                                  
#ambiguously mapped contigs                420                                                                             386                                                                    
Extra bases in ambiguously mapped contigs  -151946                                                                         -142110                                                                
#N's                                       348                                                                             32813                                                                  
#N's per 100 kbp                           6.40                                                                            598.91                                                                 
Genome fraction (%)                        94.323                                                                          94.266                                                                 
Duplication ratio                          1.001                                                                           1.006                                                                  
#genes                                     4389 + 797 part                                                                 4877 + 281 part                                                        
#predicted genes (unique)                  6218                                                                            5941                                                                   
#predicted genes (>= 0 bp)                 6248                                                                            5967                                                                   
#predicted genes (>= 300 bp)               4702                                                                            4646                                                                   
#predicted genes (>= 1500 bp)              595                                                                             612                                                                    
#predicted genes (>= 3000 bp)              48                                                                              55                                                                     
#mismatches                                95                                                                              207                                                                    
#indels                                    115                                                                             6314                                                                   
Indels length                              936                                                                             15308                                                                  
#mismatches per 100 kbp                    1.80                                                                            3.93                                                                   
#indels per 100 kbp                        2.18                                                                            119.73                                                                 
GC (%)                                     50.36                                                                           50.36                                                                  
Reference GC (%)                           50.48                                                                           50.48                                                                  
Average %IDY                               99.131                                                                          98.984                                                                 
Largest alignment                          57827                                                                           199890                                                                 
NA50                                       9390                                                                            72226                                                                  
NGA50                                      9112                                                                            65876                                                                  
NA75                                       4705                                                                            31207                                                                  
NGA75                                      4462                                                                            30811                                                                  
LA50                                       166                                                                             26                                                                     
LGA50                                      174                                                                             27                                                                     
LA75                                       361                                                                             56                                                                     
LGA75                                      386                                                                             59                                                                     
#Mis_misassemblies                         4                                                                               16                                                                     
#Mis_relocations                           4                                                                               16                                                                     
#Mis_translocations                        0                                                                               0                                                                      
#Mis_inversions                            0                                                                               0                                                                      
#Mis_misassembled contigs                  4                                                                               15                                                                     
Mis_Misassembled contigs length            75920                                                                           292898                                                                 
#Mis_local misassemblies                   4                                                                               113                                                                    
#Mis_short indels (<= 5 bp)                68                                                                              5917                                                                   
#Mis_long indels (> 5 bp)                  47                                                                              397                                                                    
#Una_fully unaligned contigs               4                                                                               16                                                                     
Una_Fully unaligned length                 5974                                                                            17599                                                                  
#Una_partially unaligned contigs           0                                                                               13                                                                     
#Una_with misassembly                      0                                                                               0                                                                      
#Una_both parts are significant            0                                                                               11                                                                     
Una_Partially unaligned length             0                                                                               13885                                                                  
GAGE_Contigs #                             1522                                                                            686                                                                    
GAGE_Min contig                            200                                                                             200                                                                    
GAGE_Max contig                            57827                                                                           200086                                                                 
GAGE_N50                                   9112 COUNT: 174                                                                 72226 COUNT: 26                                                        
GAGE_Genome size                           5594470                                                                         5594470                                                                
GAGE_Assembly size                         5439806                                                                         5478746                                                                
GAGE_Chaff bases                           0                                                                               0                                                                      
GAGE_Missing reference bases               27512(0.49%)                                                                    27143(0.49%)                                                           
GAGE_Missing assembly bases                6207(0.11%)                                                                     40871(0.75%)                                                           
GAGE_Missing assembly contigs              4(0.26%)                                                                        4(0.58%)                                                               
GAGE_Duplicated reference bases            18321                                                                           17546                                                                  
GAGE_Compressed reference bases            272322                                                                          264420                                                                 
GAGE_Bad trim                              61                                                                              4503                                                                   
GAGE_Avg idy                               99.99                                                                           99.95                                                                  
GAGE_SNPs                                  95                                                                              93                                                                     
GAGE_Indels < 5bp                          129                                                                             412                                                                    
GAGE_Indels >= 5                           49                                                                              689                                                                    
GAGE_Inversions                            0                                                                               2                                                                      
GAGE_Relocation                            3                                                                               7                                                                      
GAGE_Translocation                         0                                                                               0                                                                      
GAGE_Corrected contig #                    1505                                                                            1399                                                                   
GAGE_Corrected assembly size               5415158                                                                         5415011                                                                
GAGE_Min correct contig                    200                                                                             200                                                                    
GAGE_Max correct contig                    53798                                                                           53798                                                                  
GAGE_Corrected N50                         8518 COUNT: 185                                                                 9826 COUNT: 163                                                        
