All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_31.final.contig
#Contigs (>= 0 bp)             438815                                                                              
#Contigs (>= 1000 bp)          0                                                                                   
Total length (>= 0 bp)         22243725                                                                            
Total length (>= 1000 bp)      0                                                                                   
#Contigs                       310                                                                                 
Largest contig                 659                                                                                 
Total length                   75049                                                                               
Reference length               2813862                                                                             
GC (%)                         30.54                                                                               
Reference GC (%)               32.81                                                                               
N50                            230                                                                                 
NG50                           None                                                                                
N75                            209                                                                                 
NG75                           None                                                                                
#misassemblies                 0                                                                                   
#local misassemblies           0                                                                                   
#unaligned contigs             280 + 0 part                                                                        
Unaligned contigs length       68480                                                                               
Genome fraction (%)            0.233                                                                               
Duplication ratio              1.001                                                                               
#N's per 100 kbp               0.00                                                                                
#mismatches per 100 kbp        121.88                                                                              
#indels per 100 kbp            1112.13                                                                             
#genes                         0 + 27 part                                                                         
#predicted genes (unique)      176                                                                                 
#predicted genes (>= 0 bp)     176                                                                                 
#predicted genes (>= 300 bp)   4                                                                                   
#predicted genes (>= 1500 bp)  0                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                   
Largest alignment              300                                                                                 
NA50                           None                                                                                
NGA50                          None                                                                                
NA75                           None                                                                                
NGA75                          None                                                                                
