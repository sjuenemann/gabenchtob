All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K21.final_contigs
#Contigs (>= 0 bp)             3674                                                                                      
#Contigs (>= 1000 bp)          677                                                                                       
Total length (>= 0 bp)         5331176                                                                                   
Total length (>= 1000 bp)      4978034                                                                                   
#Contigs                       1183                                                                                      
Largest contig                 50312                                                                                     
Total length                   5201404                                                                                   
Reference length               5594470                                                                                   
GC (%)                         50.22                                                                                     
Reference GC (%)               50.48                                                                                     
N50                            10703                                                                                     
NG50                           10075                                                                                     
N75                            5924                                                                                      
NG75                           4724                                                                                      
#misassemblies                 5                                                                                         
#local misassemblies           5                                                                                         
#unaligned contigs             9 + 0 part                                                                                
Unaligned contigs length       2021                                                                                      
Genome fraction (%)            90.933                                                                                    
Duplication ratio              1.005                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        5.11                                                                                      
#indels per 100 kbp            22.04                                                                                     
#genes                         4383 + 465 part                                                                           
#predicted genes (unique)      6041                                                                                      
#predicted genes (>= 0 bp)     6057                                                                                      
#predicted genes (>= 300 bp)   4775                                                                                      
#predicted genes (>= 1500 bp)  497                                                                                       
#predicted genes (>= 3000 bp)  36                                                                                        
Largest alignment              50312                                                                                     
NA50                           10703                                                                                     
NGA50                          10075                                                                                     
NA75                           5893                                                                                      
NGA75                          4714                                                                                      
