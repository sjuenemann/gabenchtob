All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_23.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_23.final.scaf
#Mis_misassemblies               0                                                                       0                                                              
#Mis_relocations                 0                                                                       0                                                              
#Mis_translocations              0                                                                       0                                                              
#Mis_inversions                  0                                                                       0                                                              
#Mis_misassembled contigs        0                                                                       0                                                              
Mis_Misassembled contigs length  0                                                                       0                                                              
#Mis_local misassemblies         1                                                                       1                                                              
#mismatches                      155                                                                     155                                                            
#indels                          6                                                                       6                                                              
#Mis_short indels (<= 5 bp)      6                                                                       6                                                              
#Mis_long indels (> 5 bp)        0                                                                       0                                                              
Indels length                    6                                                                       6                                                              
