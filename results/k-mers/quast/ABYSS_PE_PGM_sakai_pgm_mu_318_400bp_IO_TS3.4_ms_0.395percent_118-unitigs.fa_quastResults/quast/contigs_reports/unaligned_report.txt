All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_118-unitigs
#Una_fully unaligned contigs      1                                                                       
Una_Fully unaligned length        233                                                                     
#Una_partially unaligned contigs  5                                                                       
#Una_with misassembly             0                                                                       
#Una_both parts are significant   0                                                                       
Una_Partially unaligned length    179                                                                     
#N's                              0                                                                       
