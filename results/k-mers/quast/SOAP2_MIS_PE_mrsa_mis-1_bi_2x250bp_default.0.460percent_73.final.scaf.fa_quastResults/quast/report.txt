All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_73.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_73.final.scaf
#Contigs (>= 0 bp)             12659                                                                         12289                                                                
#Contigs (>= 1000 bp)          922                                                                           847                                                                  
Total length (>= 0 bp)         5077514                                                                       5090267                                                              
Total length (>= 1000 bp)      2297174                                                                       2443739                                                              
#Contigs                       9299                                                                          8978                                                                 
Largest contig                 10785                                                                         14948                                                                
Total length                   4599760                                                                       4620694                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         34.37                                                                         34.38                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            998                                                                           1245                                                                 
NG50                           2388                                                                          3197                                                                 
N75                            251                                                                           251                                                                  
NG75                           1255                                                                          1580                                                                 
#misassemblies                 3                                                                             4                                                                    
#local misassemblies           9                                                                             63                                                                   
#unaligned contigs             5383 + 510 part                                                               5409 + 522 part                                                      
Unaligned contigs length       1376149                                                                       1424633                                                              
Genome fraction (%)            97.378                                                                        96.446                                                               
Duplication ratio              1.172                                                                         1.173                                                                
#N's per 100 kbp               15.39                                                                         291.32                                                               
#mismatches per 100 kbp        106.82                                                                        97.35                                                                
#indels per 100 kbp            12.88                                                                         105.75                                                               
#genes                         1620 + 1050 part                                                              1755 + 902 part                                                      
#predicted genes (unique)      8324                                                                          10741                                                                
#predicted genes (>= 0 bp)     8337                                                                          10755                                                                
#predicted genes (>= 300 bp)   2379                                                                          2863                                                                 
#predicted genes (>= 1500 bp)  115                                                                           138                                                                  
#predicted genes (>= 3000 bp)  2                                                                             4                                                                    
Largest alignment              10785                                                                         14948                                                                
NA50                           984                                                                           1150                                                                 
NGA50                          2388                                                                          3168                                                                 
NA75                           None                                                                          None                                                                 
NGA75                          1254                                                                          1540                                                                 
