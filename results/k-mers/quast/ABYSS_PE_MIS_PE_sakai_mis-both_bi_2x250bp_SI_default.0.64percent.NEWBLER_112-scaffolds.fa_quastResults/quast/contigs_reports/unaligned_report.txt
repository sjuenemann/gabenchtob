All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_112-scaffolds_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_112-scaffolds
#Una_fully unaligned contigs      0                                                                                              1                                                                                     
Una_Fully unaligned length        0                                                                                              878                                                                                   
#Una_partially unaligned contigs  0                                                                                              1                                                                                     
#Una_with misassembly             0                                                                                              0                                                                                     
#Una_both parts are significant   0                                                                                              1                                                                                     
Una_Partially unaligned length    0                                                                                              273                                                                                   
#N's                              23                                                                                             2416                                                                                  
