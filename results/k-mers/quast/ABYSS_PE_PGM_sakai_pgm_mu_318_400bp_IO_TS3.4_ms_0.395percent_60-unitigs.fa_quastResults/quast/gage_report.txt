All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_60-unitigs
GAGE_Contigs #                   1206                                                                   
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  48485                                                                  
GAGE_N50                         11092 COUNT: 152                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5248869                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     242623(4.34%)                                                          
GAGE_Missing assembly bases      57(0.00%)                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  2579                                                                   
GAGE_Compressed reference bases  124992                                                                 
GAGE_Bad trim                    57                                                                     
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        84                                                                     
GAGE_Indels < 5bp                229                                                                    
GAGE_Indels >= 5                 3                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  5                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          1201                                                                   
GAGE_Corrected assembly size     5246894                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          48485                                                                  
GAGE_Corrected N50               10994 COUNT: 153                                                       
