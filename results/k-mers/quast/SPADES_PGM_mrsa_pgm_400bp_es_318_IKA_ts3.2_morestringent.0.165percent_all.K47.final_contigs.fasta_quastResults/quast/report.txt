All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K47.final_contigs
#Contigs (>= 0 bp)             1067                                                                                       
#Contigs (>= 1000 bp)          98                                                                                         
Total length (>= 0 bp)         2861776                                                                                    
Total length (>= 1000 bp)      2719243                                                                                    
#Contigs                       385                                                                                        
Largest contig                 148060                                                                                     
Total length                   2808671                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.60                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            46126                                                                                      
NG50                           46126                                                                                      
N75                            30867                                                                                      
NG75                           30253                                                                                      
#misassemblies                 2                                                                                          
#local misassemblies           15                                                                                         
#unaligned contigs             198 + 0 part                                                                               
Unaligned contigs length       55252                                                                                      
Genome fraction (%)            97.482                                                                                     
Duplication ratio              1.001                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        7.55                                                                                       
#indels per 100 kbp            6.60                                                                                       
#genes                         2574 + 74 part                                                                             
#predicted genes (unique)      2833                                                                                       
#predicted genes (>= 0 bp)     2835                                                                                       
#predicted genes (>= 300 bp)   2305                                                                                       
#predicted genes (>= 1500 bp)  288                                                                                        
#predicted genes (>= 3000 bp)  25                                                                                         
Largest alignment              148060                                                                                     
NA50                           46126                                                                                      
NGA50                          46126                                                                                      
NA75                           30867                                                                                      
NGA75                          28235                                                                                      
