All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_161.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_161.final.contigs
#Mis_misassemblies               5                                                                                        12                                                                              
#Mis_relocations                 4                                                                                        11                                                                              
#Mis_translocations              1                                                                                        1                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        5                                                                                        11                                                                              
Mis_Misassembled contigs length  118667                                                                                   301898                                                                          
#Mis_local misassemblies         2                                                                                        98                                                                              
#mismatches                      127                                                                                      138                                                                             
#indels                          19                                                                                       411                                                                             
#Mis_short indels (<= 5 bp)      19                                                                                       406                                                                             
#Mis_long indels (> 5 bp)        0                                                                                        5                                                                               
Indels length                    19                                                                                       595                                                                             
