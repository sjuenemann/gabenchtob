All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_115.final.contig
#Contigs (>= 0 bp)             4374                                                                                      
#Contigs (>= 1000 bp)          948                                                                                       
Total length (>= 0 bp)         2903193                                                                                   
Total length (>= 1000 bp)      1541734                                                                                   
#Contigs                       3434                                                                                      
Largest contig                 5183                                                                                      
Total length                   2777090                                                                                   
Reference length               2813862                                                                                   
GC (%)                         32.90                                                                                     
Reference GC (%)               32.81                                                                                     
N50                            1100                                                                                      
NG50                           1082                                                                                      
N75                            648                                                                                       
NG75                           634                                                                                       
#misassemblies                 17                                                                                        
#local misassemblies           0                                                                                         
#unaligned contigs             0 + 14 part                                                                               
Unaligned contigs length       393                                                                                       
Genome fraction (%)            89.496                                                                                    
Duplication ratio              1.095                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        3.69                                                                                      
#indels per 100 kbp            53.96                                                                                     
#genes                         1058 + 1534 part                                                                          
#predicted genes (unique)      4902                                                                                      
#predicted genes (>= 0 bp)     4928                                                                                      
#predicted genes (>= 300 bp)   2929                                                                                      
#predicted genes (>= 1500 bp)  82                                                                                        
#predicted genes (>= 3000 bp)  6                                                                                         
Largest alignment              5183                                                                                      
NA50                           1099                                                                                      
NGA50                          1081                                                                                      
NA75                           646                                                                                       
NGA75                          633                                                                                       
