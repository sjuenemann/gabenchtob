All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_189.final.contigs
#Mis_misassemblies               1                                                                                     
#Mis_relocations                 1                                                                                     
#Mis_translocations              0                                                                                     
#Mis_inversions                  0                                                                                     
#Mis_misassembled contigs        1                                                                                     
Mis_Misassembled contigs length  1289                                                                                  
#Mis_local misassemblies         0                                                                                     
#mismatches                      16                                                                                    
#indels                          183                                                                                   
#Mis_short indels (<= 5 bp)      183                                                                                   
#Mis_long indels (> 5 bp)        0                                                                                     
Indels length                    188                                                                                   
