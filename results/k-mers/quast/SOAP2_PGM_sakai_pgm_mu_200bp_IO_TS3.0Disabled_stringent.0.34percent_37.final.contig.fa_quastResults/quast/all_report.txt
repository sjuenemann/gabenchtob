All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_37.final.contig
#Contigs                                   5378                                                                               
#Contigs (>= 0 bp)                         213759                                                                             
#Contigs (>= 1000 bp)                      1                                                                                  
Largest contig                             1105                                                                               
Total length                               1511449                                                                            
Total length (>= 0 bp)                     16205411                                                                           
Total length (>= 1000 bp)                  1105                                                                               
Reference length                           5594470                                                                            
N50                                        275                                                                                
NG50                                       None                                                                               
N75                                        231                                                                                
NG75                                       None                                                                               
L50                                        2109                                                                               
LG50                                       None                                                                               
L75                                        3612                                                                               
LG75                                       None                                                                               
#local misassemblies                       0                                                                                  
#misassemblies                             0                                                                                  
#misassembled contigs                      0                                                                                  
Misassembled contigs length                0                                                                                  
Misassemblies inter-contig overlap         0                                                                                  
#unaligned contigs                         19 + 4 part                                                                        
Unaligned contigs length                   4399                                                                               
#ambiguously mapped contigs                6                                                                                  
Extra bases in ambiguously mapped contigs  -1638                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        26.851                                                                             
Duplication ratio                          1.002                                                                              
#genes                                     52 + 3107 part                                                                     
#predicted genes (unique)                  5432                                                                               
#predicted genes (>= 0 bp)                 5432                                                                               
#predicted genes (>= 300 bp)               1084                                                                               
#predicted genes (>= 1500 bp)              0                                                                                  
#predicted genes (>= 3000 bp)              0                                                                                  
#mismatches                                12                                                                                 
#indels                                    183                                                                                
Indels length                              185                                                                                
#mismatches per 100 kbp                    0.80                                                                               
#indels per 100 kbp                        12.18                                                                              
GC (%)                                     50.30                                                                              
Reference GC (%)                           50.48                                                                              
Average %IDY                               99.986                                                                             
Largest alignment                          1105                                                                               
NA50                                       274                                                                                
NGA50                                      None                                                                               
NA75                                       230                                                                                
NGA75                                      None                                                                               
LA50                                       2110                                                                               
LGA50                                      None                                                                               
LA75                                       3614                                                                               
LGA75                                      None                                                                               
#Mis_misassemblies                         0                                                                                  
#Mis_relocations                           0                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  0                                                                                  
Mis_Misassembled contigs length            0                                                                                  
#Mis_local misassemblies                   0                                                                                  
#Mis_short indels (<= 5 bp)                183                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               19                                                                                 
Una_Fully unaligned length                 4160                                                                               
#Una_partially unaligned contigs           4                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             239                                                                                
GAGE_Contigs #                             5378                                                                               
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            1105                                                                               
GAGE_N50                                   0 COUNT: 0                                                                         
GAGE_Genome size                           5594470                                                                            
GAGE_Assembly size                         1511449                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               4087235(73.06%)                                                                    
GAGE_Missing assembly bases                2959(0.20%)                                                                        
GAGE_Missing assembly contigs              12(0.22%)                                                                          
GAGE_Duplicated reference bases            0                                                                                  
GAGE_Compressed reference bases            2075                                                                               
GAGE_Bad trim                              297                                                                                
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  14                                                                                 
GAGE_Indels < 5bp                          247                                                                                
GAGE_Indels >= 5                           0                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            0                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    5362                                                                               
GAGE_Corrected assembly size               1507870                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    1105                                                                               
GAGE_Corrected N50                         0 COUNT: 0                                                                         
