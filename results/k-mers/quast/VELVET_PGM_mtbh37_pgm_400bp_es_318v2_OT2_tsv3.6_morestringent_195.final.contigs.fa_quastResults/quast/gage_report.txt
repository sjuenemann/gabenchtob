All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_195.final.contigs
GAGE_Contigs #                   1919                                                                           
GAGE_Min contig                  389                                                                            
GAGE_Max contig                  9905                                                                           
GAGE_N50                         1179 COUNT: 997                                                                
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               2965047                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     1527775(34.63%)                                                                
GAGE_Missing assembly bases      611(0.02%)                                                                     
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  27973                                                                          
GAGE_Compressed reference bases  29540                                                                          
GAGE_Bad trim                    431                                                                            
GAGE_Avg idy                     99.93                                                                          
GAGE_SNPs                        57                                                                             
GAGE_Indels < 5bp                1647                                                                           
GAGE_Indels >= 5                 17                                                                             
GAGE_Inversions                  1                                                                              
GAGE_Relocation                  2                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          1900                                                                           
GAGE_Corrected assembly size     2938028                                                                        
GAGE_Min correct contig          248                                                                            
GAGE_Max correct contig          9908                                                                           
GAGE_Corrected N50               1167 COUNT: 1019                                                               
