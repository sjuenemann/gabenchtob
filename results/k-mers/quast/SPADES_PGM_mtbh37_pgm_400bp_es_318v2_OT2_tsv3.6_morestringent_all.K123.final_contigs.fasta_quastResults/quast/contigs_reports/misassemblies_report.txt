All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K123.final_contigs
#Mis_misassemblies               50                                                                                  
#Mis_relocations                 49                                                                                  
#Mis_translocations              0                                                                                   
#Mis_inversions                  1                                                                                   
#Mis_misassembled contigs        46                                                                                  
Mis_Misassembled contigs length  88263                                                                               
#Mis_local misassemblies         18                                                                                  
#mismatches                      350                                                                                 
#indels                          2057                                                                                
#Mis_short indels (<= 5 bp)      2052                                                                                
#Mis_long indels (> 5 bp)        5                                                                                   
Indels length                    2267                                                                                
