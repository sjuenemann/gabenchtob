All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K39.final_contigs
#Contigs (>= 0 bp)             1163                                                                                       
#Contigs (>= 1000 bp)          116                                                                                        
Total length (>= 0 bp)         2832124                                                                                    
Total length (>= 1000 bp)      2711761                                                                                    
#Contigs                       308                                                                                        
Largest contig                 137286                                                                                     
Total length                   2774846                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.62                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            37549                                                                                      
NG50                           37549                                                                                      
N75                            24791                                                                                      
NG75                           23949                                                                                      
#misassemblies                 2                                                                                          
#local misassemblies           17                                                                                         
#unaligned contigs             99 + 1 part                                                                                
Unaligned contigs length       28587                                                                                      
Genome fraction (%)            97.274                                                                                     
Duplication ratio              1.001                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        10.67                                                                                      
#indels per 100 kbp            7.16                                                                                       
#genes                         2567 + 69 part                                                                             
#predicted genes (unique)      2785                                                                                       
#predicted genes (>= 0 bp)     2787                                                                                       
#predicted genes (>= 300 bp)   2294                                                                                       
#predicted genes (>= 1500 bp)  281                                                                                        
#predicted genes (>= 3000 bp)  26                                                                                         
Largest alignment              137286                                                                                     
NA50                           37549                                                                                      
NGA50                          37549                                                                                      
NA75                           24213                                                                                      
NGA75                          23273                                                                                      
