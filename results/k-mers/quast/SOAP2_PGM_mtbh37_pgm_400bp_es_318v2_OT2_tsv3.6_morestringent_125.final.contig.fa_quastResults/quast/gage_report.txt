All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_125.final.contig
GAGE_Contigs #                   42423                                                                        
GAGE_Min contig                  200                                                                          
GAGE_Max contig                  1887                                                                         
GAGE_N50                         304 COUNT: 4539                                                              
GAGE_Genome size                 4411532                                                                      
GAGE_Assembly size               11365147                                                                     
GAGE_Chaff bases                 0                                                                            
GAGE_Missing reference bases     340169(7.71%)                                                                
GAGE_Missing assembly bases      1099(0.01%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                     
GAGE_Duplicated reference bases  6097155                                                                      
GAGE_Compressed reference bases  54950                                                                        
GAGE_Bad trim                    1096                                                                         
GAGE_Avg idy                     99.93                                                                        
GAGE_SNPs                        72                                                                           
GAGE_Indels < 5bp                2297                                                                         
GAGE_Indels >= 5                 4                                                                            
GAGE_Inversions                  2                                                                            
GAGE_Relocation                  1                                                                            
GAGE_Translocation               0                                                                            
GAGE_Corrected contig #          17616                                                                        
GAGE_Corrected assembly size     5264337                                                                      
GAGE_Min correct contig          200                                                                          
GAGE_Max correct contig          1888                                                                         
GAGE_Corrected N50               277 COUNT: 4664                                                              
