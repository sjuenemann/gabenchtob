All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_109.final.contig
#Contigs                                   45669                                                                        
#Contigs (>= 0 bp)                         104165                                                                       
#Contigs (>= 1000 bp)                      45                                                                           
Largest contig                             1525                                                                         
Total length                               10954879                                                                     
Total length (>= 0 bp)                     19378414                                                                     
Total length (>= 1000 bp)                  53513                                                                        
Reference length                           4411532                                                                      
N50                                        217                                                                          
NG50                                       282                                                                          
N75                                        215                                                                          
NG75                                       218                                                                          
L50                                        20053                                                                        
LG50                                       5385                                                                         
L75                                        32715                                                                        
LG75                                       10076                                                                        
#local misassemblies                       6                                                                            
#misassemblies                             45                                                                           
#misassembled contigs                      45                                                                           
Misassembled contigs length                14654                                                                        
Misassemblies inter-contig overlap         539                                                                          
#unaligned contigs                         90 + 16 part                                                                 
Unaligned contigs length                   31537                                                                        
#ambiguously mapped contigs                303                                                                          
Extra bases in ambiguously mapped contigs  -66769                                                                       
#N's                                       0                                                                            
#N's per 100 kbp                           0.00                                                                         
Genome fraction (%)                        92.824                                                                       
Duplication ratio                          2.651                                                                        
#genes                                     210 + 3785 part                                                              
#predicted genes (unique)                  41298                                                                        
#predicted genes (>= 0 bp)                 42008                                                                        
#predicted genes (>= 300 bp)               2436                                                                         
#predicted genes (>= 1500 bp)              0                                                                            
#predicted genes (>= 3000 bp)              0                                                                            
#mismatches                                382                                                                          
#indels                                    12500                                                                        
Indels length                              12829                                                                        
#mismatches per 100 kbp                    9.33                                                                         
#indels per 100 kbp                        305.25                                                                       
GC (%)                                     64.65                                                                        
Reference GC (%)                           65.61                                                                        
Average %IDY                               99.575                                                                       
Largest alignment                          1525                                                                         
NA50                                       217                                                                          
NGA50                                      277                                                                          
NA75                                       215                                                                          
NGA75                                      218                                                                          
LA50                                       20133                                                                        
LGA50                                      5420                                                                         
LA75                                       32800                                                                        
LGA75                                      10155                                                                        
#Mis_misassemblies                         45                                                                           
#Mis_relocations                           44                                                                           
#Mis_translocations                        0                                                                            
#Mis_inversions                            1                                                                            
#Mis_misassembled contigs                  45                                                                           
Mis_Misassembled contigs length            14654                                                                        
#Mis_local misassemblies                   6                                                                            
#Mis_short indels (<= 5 bp)                12500                                                                        
#Mis_long indels (> 5 bp)                  0                                                                            
#Una_fully unaligned contigs               90                                                                           
Una_Fully unaligned length                 29856                                                                        
#Una_partially unaligned contigs           16                                                                           
#Una_with misassembly                      0                                                                            
#Una_both parts are significant            0                                                                            
Una_Partially unaligned length             1681                                                                         
GAGE_Contigs #                             45669                                                                        
GAGE_Min contig                            200                                                                          
GAGE_Max contig                            1525                                                                         
GAGE_N50                                   282 COUNT: 5385                                                              
GAGE_Genome size                           4411532                                                                      
GAGE_Assembly size                         10954879                                                                     
GAGE_Chaff bases                           0                                                                            
GAGE_Missing reference bases               263903(5.98%)                                                                
GAGE_Missing assembly bases                8606(0.08%)                                                                  
GAGE_Missing assembly contigs              11(0.02%)                                                                    
GAGE_Duplicated reference bases            5507450                                                                      
GAGE_Compressed reference bases            50670                                                                        
GAGE_Bad trim                              4722                                                                         
GAGE_Avg idy                               99.82                                                                        
GAGE_SNPs                                  125                                                                          
GAGE_Indels < 5bp                          6099                                                                         
GAGE_Indels >= 5                           6                                                                            
GAGE_Inversions                            0                                                                            
GAGE_Relocation                            2                                                                            
GAGE_Translocation                         0                                                                            
GAGE_Corrected contig #                    21090                                                                        
GAGE_Corrected assembly size               5434450                                                                      
GAGE_Min correct contig                    200                                                                          
GAGE_Max correct contig                    1525                                                                         
GAGE_Corrected N50                         219 COUNT: 6040                                                              
