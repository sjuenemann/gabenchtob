All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_109.final.contig
#Mis_misassemblies               45                                                                           
#Mis_relocations                 44                                                                           
#Mis_translocations              0                                                                            
#Mis_inversions                  1                                                                            
#Mis_misassembled contigs        45                                                                           
Mis_Misassembled contigs length  14654                                                                        
#Mis_local misassemblies         6                                                                            
#mismatches                      382                                                                          
#indels                          12500                                                                        
#Mis_short indels (<= 5 bp)      12500                                                                        
#Mis_long indels (> 5 bp)        0                                                                            
Indels length                    12829                                                                        
