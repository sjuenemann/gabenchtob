All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K23.final_contigs
GAGE_Contigs #                   397                                                                                        
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  74590                                                                                      
GAGE_N50                         19458 COUNT: 46                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2729107                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     59968(2.13%)                                                                               
GAGE_Missing assembly bases      7702(0.28%)                                                                                
GAGE_Missing assembly contigs    27(6.80%)                                                                                  
GAGE_Duplicated reference bases  0                                                                                          
GAGE_Compressed reference bases  34570                                                                                      
GAGE_Bad trim                    27                                                                                         
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        116                                                                                        
GAGE_Indels < 5bp                161                                                                                        
GAGE_Indels >= 5                 6                                                                                          
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  2                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          379                                                                                        
GAGE_Corrected assembly size     2723184                                                                                    
GAGE_Min correct contig          200                                                                                        
GAGE_Max correct contig          74595                                                                                      
GAGE_Corrected N50               19459 COUNT: 46                                                                            
