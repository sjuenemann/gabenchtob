All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K63.final_contigs
#Mis_misassemblies               2                                                                                          
#Mis_relocations                 2                                                                                          
#Mis_translocations              0                                                                                          
#Mis_inversions                  0                                                                                          
#Mis_misassembled contigs        2                                                                                          
Mis_Misassembled contigs length  34278                                                                                      
#Mis_local misassemblies         12                                                                                         
#mismatches                      166                                                                                        
#indels                          191                                                                                        
#Mis_short indels (<= 5 bp)      189                                                                                        
#Mis_long indels (> 5 bp)        2                                                                                          
Indels length                    256                                                                                        
