All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K99.final_contigs
#Contigs (>= 0 bp)             2191                                                                               
#Contigs (>= 1000 bp)          142                                                                                
Total length (>= 0 bp)         4895113                                                                            
Total length (>= 1000 bp)      4286468                                                                            
#Contigs                       2156                                                                               
Largest contig                 182935                                                                             
Total length                   4890899                                                                            
Reference length               4411532                                                                            
GC (%)                         65.23                                                                              
Reference GC (%)               65.61                                                                              
N50                            57742                                                                              
NG50                           66035                                                                              
N75                            22453                                                                              
NG75                           31537                                                                              
#misassemblies                 18                                                                                 
#local misassemblies           23                                                                                 
#unaligned contigs             319 + 23 part                                                                      
Unaligned contigs length       93084                                                                              
Genome fraction (%)            97.524                                                                             
Duplication ratio              1.113                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        8.39                                                                               
#indels per 100 kbp            35.75                                                                              
#genes                         3939 + 115 part                                                                    
#predicted genes (unique)      5780                                                                               
#predicted genes (>= 0 bp)     5789                                                                               
#predicted genes (>= 300 bp)   3840                                                                               
#predicted genes (>= 1500 bp)  455                                                                                
#predicted genes (>= 3000 bp)  46                                                                                 
Largest alignment              182935                                                                             
NA50                           55914                                                                              
NGA50                          64090                                                                              
NA75                           22443                                                                              
NGA75                          31498                                                                              
