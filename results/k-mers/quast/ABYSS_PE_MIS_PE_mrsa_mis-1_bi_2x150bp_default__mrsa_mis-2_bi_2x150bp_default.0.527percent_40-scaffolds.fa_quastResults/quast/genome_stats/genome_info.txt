reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  ABYSS_PE_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.527percent_40-scaffolds_broken  | 97.9276524577       | 1.00383154137     | 60          | 2610      | 48        | None      | None      |
  ABYSS_PE_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.527percent_40-scaffolds  | 97.9499350004       | 1.00587408061     | 51          | 2614      | 48        | None      | None      |
