All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_41.final.contigs
#Contigs                                   1477                                                                                 
#Contigs (>= 0 bp)                         2195                                                                                 
#Contigs (>= 1000 bp)                      879                                                                                  
Largest contig                             37204                                                                                
Total length                               5258667                                                                              
Total length (>= 0 bp)                     5351777                                                                              
Total length (>= 1000 bp)                  4989006                                                                              
Reference length                           5594470                                                                              
N50                                        8205                                                                                 
NG50                                       7475                                                                                 
N75                                        4265                                                                                 
NG75                                       3537                                                                                 
L50                                        203                                                                                  
LG50                                       225                                                                                  
L75                                        421                                                                                  
LG75                                       486                                                                                  
#local misassemblies                       11                                                                                   
#misassemblies                             3                                                                                    
#misassembled contigs                      3                                                                                    
Misassembled contigs length                13231                                                                                
Misassemblies inter-contig overlap         1306                                                                                 
#unaligned contigs                         0 + 4 part                                                                           
Unaligned contigs length                   300                                                                                  
#ambiguously mapped contigs                162                                                                                  
Extra bases in ambiguously mapped contigs  -76010                                                                               
#N's                                       30                                                                                   
#N's per 100 kbp                           0.57                                                                                 
Genome fraction (%)                        92.349                                                                               
Duplication ratio                          1.003                                                                                
#genes                                     4205 + 771 part                                                                      
#predicted genes (unique)                  6751                                                                                 
#predicted genes (>= 0 bp)                 6752                                                                                 
#predicted genes (>= 300 bp)               5045                                                                                 
#predicted genes (>= 1500 bp)              411                                                                                  
#predicted genes (>= 3000 bp)              22                                                                                   
#mismatches                                264                                                                                  
#indels                                    2537                                                                                 
Indels length                              2745                                                                                 
#mismatches per 100 kbp                    5.11                                                                                 
#indels per 100 kbp                        49.11                                                                                
GC (%)                                     50.24                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               99.745                                                                               
Largest alignment                          37204                                                                                
NA50                                       8205                                                                                 
NGA50                                      7475                                                                                 
NA75                                       4265                                                                                 
NGA75                                      3526                                                                                 
LA50                                       203                                                                                  
LGA50                                      225                                                                                  
LA75                                       421                                                                                  
LGA75                                      486                                                                                  
#Mis_misassemblies                         3                                                                                    
#Mis_relocations                           3                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  3                                                                                    
Mis_Misassembled contigs length            13231                                                                                
#Mis_local misassemblies                   11                                                                                   
#Mis_short indels (<= 5 bp)                2534                                                                                 
#Mis_long indels (> 5 bp)                  3                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           4                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             300                                                                                  
GAGE_Contigs #                             1477                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            37204                                                                                
GAGE_N50                                   7475 COUNT: 225                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5258667                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               191881(3.43%)                                                                        
GAGE_Missing assembly bases                269(0.01%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            1175                                                                                 
GAGE_Compressed reference bases            170983                                                                               
GAGE_Bad trim                              269                                                                                  
GAGE_Avg idy                               99.94                                                                                
GAGE_SNPs                                  246                                                                                  
GAGE_Indels < 5bp                          2775                                                                                 
GAGE_Indels >= 5                           11                                                                                   
GAGE_Inversions                            1                                                                                    
GAGE_Relocation                            4                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1489                                                                                 
GAGE_Corrected assembly size               5261261                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    37211                                                                                
GAGE_Corrected N50                         7378 COUNT: 226                                                                      
