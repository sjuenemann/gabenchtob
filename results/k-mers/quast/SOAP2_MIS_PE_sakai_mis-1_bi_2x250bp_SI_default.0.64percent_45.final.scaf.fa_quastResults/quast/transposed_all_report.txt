All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                         #Contigs  #Contigs (>= 0 bp)  #Contigs (>= 1000 bp)  Largest contig  Total length  Total length (>= 0 bp)  Total length (>= 1000 bp)  Reference length  N50  NG50  N75  NG75  L50    LG50  L75    LG75   #local misassemblies  #misassemblies  #misassembled contigs  Misassembled contigs length  Misassemblies inter-contig overlap  #unaligned contigs  Unaligned contigs length  #ambiguously mapped contigs  Extra bases in ambiguously mapped contigs  #N's  #N's per 100 kbp  Genome fraction (%)  Duplication ratio  #genes           #predicted genes (unique)  #predicted genes (>= 0 bp)  #predicted genes (>= 300 bp)  #predicted genes (>= 1500 bp)  #predicted genes (>= 3000 bp)  #mismatches  #indels  Indels length  #mismatches per 100 kbp  #indels per 100 kbp  GC (%)  Reference GC (%)  Average %IDY  Largest alignment  NA50  NGA50  NA75  NGA75  LA50  LGA50  LA75  LGA75  #Mis_misassemblies  #Mis_relocations  #Mis_translocations  #Mis_inversions  #Mis_misassembled contigs  Mis_Misassembled contigs length  #Mis_local misassemblies  #Mis_short indels (<= 5 bp)  #Mis_long indels (> 5 bp)  #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  GAGE_Contigs #  GAGE_Min contig  GAGE_Max contig  GAGE_N50         GAGE_Genome size  GAGE_Assembly size  GAGE_Chaff bases  GAGE_Missing reference bases  GAGE_Missing assembly bases  GAGE_Missing assembly contigs  GAGE_Duplicated reference bases  GAGE_Compressed reference bases  GAGE_Bad trim  GAGE_Avg idy  GAGE_SNPs  GAGE_Indels < 5bp  GAGE_Indels >= 5  GAGE_Inversions  GAGE_Relocation  GAGE_Translocation  GAGE_Corrected contig #  GAGE_Corrected assembly size  GAGE_Min correct contig  GAGE_Max correct contig  GAGE_Corrected N50
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_45.final.scaf_broken  49362     122549              129                    2010            12756215      24390807                161188                     5594470           248  261   223  251   20721  6436  34226  11994  0                     1               1                      313                          0                                   40201 + 3 part      9364057                   26                           -6715                                      0     0.00              58.939               1.027              341 + 4151 part  42846                      42849                       3514                          1                              0                              100          5        5              3.03                     0.15                 53.71   50.48             99.993        2010               None  254    None  None   None  6510   None  None   1                   1                 0                    0                1                          313                              0                         5                            0                          40201                         9363697                     3                                 0                      0                                360                             49362           200              2010             261 COUNT: 6436  5594470           12756215            0                 2013682(35.99%)               8930566(70.01%)              38114(77.21%)                  126006                           57024                            33417          99.66         9144       49                 0                 0                1                0                   10377                    3662032                       200                      2010                     254 COUNT: 6507   
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_45.final.scaf         49362     122549              129                    2010            12756215      24390807                161188                     5594470           248  261   223  251   20721  6436  34226  11994  0                     1               1                      313                          0                                   40201 + 3 part      9364057                   26                           -6715                                      0     0.00              58.939               1.027              341 + 4151 part  42846                      42849                       3514                          1                              0                              100          5        5              3.03                     0.15                 53.71   50.48             99.993        2010               None  254    None  None   None  6510   None  None   1                   1                 0                    0                1                          313                              0                         5                            0                          40201                         9363697                     3                                 0                      0                                360                             49362           200              2010             261 COUNT: 6436  5594470           12756215            0                 2013682(35.99%)               8930566(70.01%)              38114(77.21%)                  126006                           57024                            33417          99.66         9144       49                 0                 0                1                0                   10377                    3662032                       200                      2010                     254 COUNT: 6507   
