All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K115.final_contigs
GAGE_Contigs #                   1095                                                                                       
GAGE_Min contig                  201                                                                                        
GAGE_Max contig                  340077                                                                                     
GAGE_N50                         99756 COUNT: 17                                                                            
GAGE_Genome size                 5594470                                                                                    
GAGE_Assembly size               5579863                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     13970(0.25%)                                                                               
GAGE_Missing assembly bases      7193(0.13%)                                                                                
GAGE_Missing assembly contigs    3(0.27%)                                                                                   
GAGE_Duplicated reference bases  228740                                                                                     
GAGE_Compressed reference bases  286230                                                                                     
GAGE_Bad trim                    6319                                                                                       
GAGE_Avg idy                     99.95                                                                                      
GAGE_SNPs                        359                                                                                        
GAGE_Indels < 5bp                1920                                                                                       
GAGE_Indels >= 5                 10                                                                                         
GAGE_Inversions                  5                                                                                          
GAGE_Relocation                  9                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          300                                                                                        
GAGE_Corrected assembly size     5357586                                                                                    
GAGE_Min correct contig          204                                                                                        
GAGE_Max correct contig          224393                                                                                     
GAGE_Corrected N50               92315 COUNT: 20                                                                            
