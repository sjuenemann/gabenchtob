All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_65.final.contigs
#Contigs (>= 0 bp)             50338                                                                                 
#Contigs (>= 1000 bp)          0                                                                                     
Total length (>= 0 bp)         8834261                                                                               
Total length (>= 1000 bp)      0                                                                                     
#Contigs                       9313                                                                                  
Largest contig                 434                                                                                   
Total length                   2124587                                                                               
Reference length               2813862                                                                               
GC (%)                         31.86                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            222                                                                                   
NG50                           213                                                                                   
N75                            209                                                                                   
NG75                           200                                                                                   
#misassemblies                 0                                                                                     
#local misassemblies           0                                                                                     
#unaligned contigs             9277 + 2 part                                                                         
Unaligned contigs length       2116335                                                                               
Genome fraction (%)            0.293                                                                                 
Duplication ratio              1.001                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        0.00                                                                                  
#indels per 100 kbp            412.27                                                                                
#genes                         2 + 29 part                                                                           
#predicted genes (unique)      6478                                                                                  
#predicted genes (>= 0 bp)     6480                                                                                  
#predicted genes (>= 300 bp)   1                                                                                     
#predicted genes (>= 1500 bp)  0                                                                                     
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              306                                                                                   
NA50                           None                                                                                  
NGA50                          None                                                                                  
NA75                           None                                                                                  
NGA75                          None                                                                                  
