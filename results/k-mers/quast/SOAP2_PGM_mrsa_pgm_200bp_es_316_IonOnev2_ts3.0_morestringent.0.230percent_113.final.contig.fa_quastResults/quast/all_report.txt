All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_113.final.contig
#Contigs                                   3562                                                                                      
#Contigs (>= 0 bp)                         4573                                                                                      
#Contigs (>= 1000 bp)                      966                                                                                       
Largest contig                             7866                                                                                      
Total length                               2853638                                                                                   
Total length (>= 0 bp)                     2988379                                                                                   
Total length (>= 1000 bp)                  1617243                                                                                   
Reference length                           2813862                                                                                   
N50                                        1140                                                                                      
NG50                                       1149                                                                                      
N75                                        655                                                                                       
NG75                                       678                                                                                       
L50                                        788                                                                                       
LG50                                       771                                                                                       
L75                                        1611                                                                                      
LG75                                       1566                                                                                      
#local misassemblies                       0                                                                                         
#misassemblies                             13                                                                                        
#misassembled contigs                      13                                                                                        
Misassembled contigs length                7343                                                                                      
Misassemblies inter-contig overlap         42                                                                                        
#unaligned contigs                         0 + 14 part                                                                               
Unaligned contigs length                   568                                                                                       
#ambiguously mapped contigs                86                                                                                        
Extra bases in ambiguously mapped contigs  -21916                                                                                    
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        90.933                                                                                    
Duplication ratio                          1.106                                                                                     
#genes                                     1124 + 1492 part                                                                          
#predicted genes (unique)                  5095                                                                                      
#predicted genes (>= 0 bp)                 5125                                                                                      
#predicted genes (>= 300 bp)               2935                                                                                      
#predicted genes (>= 1500 bp)              89                                                                                        
#predicted genes (>= 3000 bp)              7                                                                                         
#mismatches                                76                                                                                        
#indels                                    1382                                                                                      
Indels length                              1414                                                                                      
#mismatches per 100 kbp                    2.97                                                                                      
#indels per 100 kbp                        54.01                                                                                     
GC (%)                                     32.84                                                                                     
Reference GC (%)                           32.81                                                                                     
Average %IDY                               99.707                                                                                    
Largest alignment                          7866                                                                                      
NA50                                       1139                                                                                      
NGA50                                      1148                                                                                      
NA75                                       654                                                                                       
NGA75                                      676                                                                                       
LA50                                       789                                                                                       
LGA50                                      771                                                                                       
LA75                                       1613                                                                                      
LGA75                                      1568                                                                                      
#Mis_misassemblies                         13                                                                                        
#Mis_relocations                           8                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            5                                                                                         
#Mis_misassembled contigs                  13                                                                                        
Mis_Misassembled contigs length            7343                                                                                      
#Mis_local misassemblies                   0                                                                                         
#Mis_short indels (<= 5 bp)                1382                                                                                      
#Mis_long indels (> 5 bp)                  0                                                                                         
#Una_fully unaligned contigs               0                                                                                         
Una_Fully unaligned length                 0                                                                                         
#Una_partially unaligned contigs           14                                                                                        
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             568                                                                                       
GAGE_Contigs #                             3562                                                                                      
GAGE_Min contig                            200                                                                                       
GAGE_Max contig                            7866                                                                                      
GAGE_N50                                   1149 COUNT: 771                                                                           
GAGE_Genome size                           2813862                                                                                   
GAGE_Assembly size                         2853638                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               211977(7.53%)                                                                             
GAGE_Missing assembly bases                1274(0.04%)                                                                               
GAGE_Missing assembly contigs              0(0.00%)                                                                                  
GAGE_Duplicated reference bases            166422                                                                                    
GAGE_Compressed reference bases            47890                                                                                     
GAGE_Bad trim                              1274                                                                                      
GAGE_Avg idy                               99.93                                                                                     
GAGE_SNPs                                  58                                                                                        
GAGE_Indels < 5bp                          1055                                                                                      
GAGE_Indels >= 5                           1                                                                                         
GAGE_Inversions                            1                                                                                         
GAGE_Relocation                            0                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    2817                                                                                      
GAGE_Corrected assembly size               2686511                                                                                   
GAGE_Min correct contig                    202                                                                                       
GAGE_Max correct contig                    7867                                                                                      
GAGE_Corrected N50                         1148 COUNT: 771                                                                           
