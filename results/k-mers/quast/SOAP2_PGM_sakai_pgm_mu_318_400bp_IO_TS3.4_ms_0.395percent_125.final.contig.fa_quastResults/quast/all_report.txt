All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_125.final.contig
#Contigs                                   51935                                                                     
#Contigs (>= 0 bp)                         83552                                                                     
#Contigs (>= 1000 bp)                      252                                                                       
Largest contig                             3371                                                                      
Total length                               14489898                                                                  
Total length (>= 0 bp)                     19408964                                                                  
Total length (>= 1000 bp)                  317736                                                                    
Reference length                           5594470                                                                   
N50                                        248                                                                       
NG50                                       389                                                                       
N75                                        245                                                                       
NG75                                       272                                                                       
L50                                        21327                                                                     
LG50                                       4916                                                                      
L75                                        36034                                                                     
LG75                                       9142                                                                      
#local misassemblies                       3                                                                         
#misassemblies                             128                                                                       
#misassembled contigs                      127                                                                       
Misassembled contigs length                45248                                                                     
Misassemblies inter-contig overlap         380                                                                       
#unaligned contigs                         23 + 23 part                                                              
Unaligned contigs length                   10311                                                                     
#ambiguously mapped contigs                1662                                                                      
Extra bases in ambiguously mapped contigs  -405085                                                                   
#N's                                       0                                                                         
#N's per 100 kbp                           0.00                                                                      
Genome fraction (%)                        94.100                                                                    
Duplication ratio                          2.674                                                                     
#genes                                     631 + 4542 part                                                           
#predicted genes (unique)                  52147                                                                     
#predicted genes (>= 0 bp)                 53498                                                                     
#predicted genes (>= 300 bp)               4762                                                                      
#predicted genes (>= 1500 bp)              3                                                                         
#predicted genes (>= 3000 bp)              0                                                                         
#mismatches                                235                                                                       
#indels                                    8735                                                                      
Indels length                              8881                                                                      
#mismatches per 100 kbp                    4.46                                                                      
#indels per 100 kbp                        165.93                                                                    
GC (%)                                     50.59                                                                     
Reference GC (%)                           50.48                                                                     
Average %IDY                               99.550                                                                    
Largest alignment                          3371                                                                      
NA50                                       248                                                                       
NGA50                                      387                                                                       
NA75                                       245                                                                       
NGA75                                      266                                                                       
LA50                                       21444                                                                     
LGA50                                      4923                                                                      
LA75                                       36164                                                                     
LGA75                                      9233                                                                      
#Mis_misassemblies                         128                                                                       
#Mis_relocations                           121                                                                       
#Mis_translocations                        7                                                                         
#Mis_inversions                            0                                                                         
#Mis_misassembled contigs                  127                                                                       
Mis_Misassembled contigs length            45248                                                                     
#Mis_local misassemblies                   3                                                                         
#Mis_short indels (<= 5 bp)                8735                                                                      
#Mis_long indels (> 5 bp)                  0                                                                         
#Una_fully unaligned contigs               23                                                                        
Una_Fully unaligned length                 8616                                                                      
#Una_partially unaligned contigs           23                                                                        
#Una_with misassembly                      0                                                                         
#Una_both parts are significant            1                                                                         
Una_Partially unaligned length             1695                                                                      
GAGE_Contigs #                             51935                                                                     
GAGE_Min contig                            200                                                                       
GAGE_Max contig                            3371                                                                      
GAGE_N50                                   389 COUNT: 4916                                                           
GAGE_Genome size                           5594470                                                                   
GAGE_Assembly size                         14489898                                                                  
GAGE_Chaff bases                           0                                                                         
GAGE_Missing reference bases               60295(1.08%)                                                              
GAGE_Missing assembly bases                7490(0.05%)                                                               
GAGE_Missing assembly contigs              11(0.02%)                                                                 
GAGE_Duplicated reference bases            7633678                                                                   
GAGE_Compressed reference bases            243566                                                                    
GAGE_Bad trim                              3574                                                                      
GAGE_Avg idy                               99.96                                                                     
GAGE_SNPs                                  25                                                                        
GAGE_Indels < 5bp                          1531                                                                      
GAGE_Indels >= 5                           1                                                                         
GAGE_Inversions                            1                                                                         
GAGE_Relocation                            2                                                                         
GAGE_Translocation                         1                                                                         
GAGE_Corrected contig #                    21618                                                                     
GAGE_Corrected assembly size               6841689                                                                   
GAGE_Min correct contig                    200                                                                       
GAGE_Max correct contig                    3371                                                                      
GAGE_Corrected N50                         335 COUNT: 5051                                                           
