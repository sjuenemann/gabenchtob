All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_104-unitigs
#Contigs (>= 0 bp)             4483                                                                    
#Contigs (>= 1000 bp)          685                                                                     
Total length (>= 0 bp)         5887518                                                                 
Total length (>= 1000 bp)      5104957                                                                 
#Contigs                       2589                                                                    
Largest contig                 50050                                                                   
Total length                   5615864                                                                 
Reference length               5594470                                                                 
GC (%)                         50.41                                                                   
Reference GC (%)               50.48                                                                   
N50                            11596                                                                   
NG50                           11614                                                                   
N75                            5035                                                                    
NG75                           5118                                                                    
#misassemblies                 5                                                                       
#local misassemblies           6                                                                       
#unaligned contigs             0 + 6 part                                                              
Unaligned contigs length       261                                                                     
Genome fraction (%)            94.372                                                                  
Duplication ratio              1.036                                                                   
#N's per 100 kbp               0.00                                                                    
#mismatches per 100 kbp        1.53                                                                    
#indels per 100 kbp            7.99                                                                    
#genes                         4439 + 721 part                                                         
#predicted genes (unique)      7168                                                                    
#predicted genes (>= 0 bp)     7199                                                                    
#predicted genes (>= 300 bp)   4643                                                                    
#predicted genes (>= 1500 bp)  587                                                                     
#predicted genes (>= 3000 bp)  48                                                                      
Largest alignment              50050                                                                   
NA50                           11596                                                                   
NGA50                          11614                                                                   
NA75                           5023                                                                    
NGA75                          5094                                                                    
