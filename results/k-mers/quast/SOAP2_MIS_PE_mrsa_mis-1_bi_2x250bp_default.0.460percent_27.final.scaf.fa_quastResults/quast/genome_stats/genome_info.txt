reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_27.final.scaf_broken  | 74.7554784137       | 1.00247537932     | 2097        | 578       | 1858      | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_27.final.scaf  | 64.3887297956       | 1.0475209873      | 1755        | 598       | 1571      | None      | None      |
