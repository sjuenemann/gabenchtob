All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_21.final.contigs
GAGE_Contigs #                   1                                                                                          
GAGE_Min contig                  204                                                                                        
GAGE_Max contig                  204                                                                                        
GAGE_N50                         0 COUNT: 0                                                                                 
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               204                                                                                        
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     2813658(99.99%)                                                                            
GAGE_Missing assembly bases      0(0.00%)                                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                                   
GAGE_Duplicated reference bases  0                                                                                          
GAGE_Compressed reference bases  0                                                                                          
GAGE_Bad trim                    0                                                                                          
GAGE_Avg idy                     100.00                                                                                     
GAGE_SNPs                        0                                                                                          
GAGE_Indels < 5bp                0                                                                                          
GAGE_Indels >= 5                 0                                                                                          
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  0                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          1                                                                                          
GAGE_Corrected assembly size     204                                                                                        
GAGE_Min correct contig          204                                                                                        
GAGE_Max correct contig          204                                                                                        
GAGE_Corrected N50               0 COUNT: 0                                                                                 
