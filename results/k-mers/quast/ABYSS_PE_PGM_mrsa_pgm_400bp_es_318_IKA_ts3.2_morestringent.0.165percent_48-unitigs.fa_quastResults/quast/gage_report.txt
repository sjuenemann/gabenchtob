All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_48-unitigs
GAGE_Contigs #                   512                                                                               
GAGE_Min contig                  203                                                                               
GAGE_Max contig                  46489                                                                             
GAGE_N50                         11850 COUNT: 70                                                                   
GAGE_Genome size                 2813862                                                                           
GAGE_Assembly size               2731639                                                                           
GAGE_Chaff bases                 0                                                                                 
GAGE_Missing reference bases     79659(2.83%)                                                                      
GAGE_Missing assembly bases      10(0.00%)                                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                                          
GAGE_Duplicated reference bases  0                                                                                 
GAGE_Compressed reference bases  7080                                                                              
GAGE_Bad trim                    10                                                                                
GAGE_Avg idy                     99.99                                                                             
GAGE_SNPs                        48                                                                                
GAGE_Indels < 5bp                95                                                                                
GAGE_Indels >= 5                 4                                                                                 
GAGE_Inversions                  0                                                                                 
GAGE_Relocation                  1                                                                                 
GAGE_Translocation               0                                                                                 
GAGE_Corrected contig #          517                                                                               
GAGE_Corrected assembly size     2732351                                                                           
GAGE_Min correct contig          203                                                                               
GAGE_Max correct contig          46492                                                                             
GAGE_Corrected N50               11851 COUNT: 70                                                                   
