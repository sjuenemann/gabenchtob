All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K49.final_contigs
#Contigs                                   400                                                                                             
#Contigs (>= 0 bp)                         1187                                                                                            
#Contigs (>= 1000 bp)                      102                                                                                             
Largest contig                             148058                                                                                          
Total length                               2802213                                                                                         
Total length (>= 0 bp)                     2863327                                                                                         
Total length (>= 1000 bp)                  2724291                                                                                         
Reference length                           2813862                                                                                         
N50                                        45362                                                                                           
NG50                                       45362                                                                                           
N75                                        30260                                                                                           
NG75                                       28234                                                                                           
L50                                        18                                                                                              
LG50                                       18                                                                                              
L75                                        37                                                                                              
LG75                                       38                                                                                              
#local misassemblies                       13                                                                                              
#misassemblies                             3                                                                                               
#misassembled contigs                      3                                                                                               
Misassembled contigs length                85945                                                                                           
Misassemblies inter-contig overlap         1804                                                                                            
#unaligned contigs                         213 + 2 part                                                                                    
Unaligned contigs length                   46645                                                                                           
#ambiguously mapped contigs                12                                                                                              
Extra bases in ambiguously mapped contigs  -9747                                                                                           
#N's                                       0                                                                                               
#N's per 100 kbp                           0.00                                                                                            
Genome fraction (%)                        97.546                                                                                          
Duplication ratio                          1.001                                                                                           
#genes                                     2577 + 73 part                                                                                  
#predicted genes (unique)                  2878                                                                                            
#predicted genes (>= 0 bp)                 2878                                                                                            
#predicted genes (>= 300 bp)               2315                                                                                            
#predicted genes (>= 1500 bp)              287                                                                                             
#predicted genes (>= 3000 bp)              24                                                                                              
#mismatches                                120                                                                                             
#indels                                    437                                                                                             
Indels length                              579                                                                                             
#mismatches per 100 kbp                    4.37                                                                                            
#indels per 100 kbp                        15.92                                                                                           
GC (%)                                     32.61                                                                                           
Reference GC (%)                           32.81                                                                                           
Average %IDY                               99.781                                                                                          
Largest alignment                          148058                                                                                          
NA50                                       45362                                                                                           
NGA50                                      45362                                                                                           
NA75                                       28234                                                                                           
NGA75                                      27702                                                                                           
LA50                                       18                                                                                              
LGA50                                      18                                                                                              
LA75                                       37                                                                                              
LGA75                                      38                                                                                              
#Mis_misassemblies                         3                                                                                               
#Mis_relocations                           3                                                                                               
#Mis_translocations                        0                                                                                               
#Mis_inversions                            0                                                                                               
#Mis_misassembled contigs                  3                                                                                               
Mis_Misassembled contigs length            85945                                                                                           
#Mis_local misassemblies                   13                                                                                              
#Mis_short indels (<= 5 bp)                435                                                                                             
#Mis_long indels (> 5 bp)                  2                                                                                               
#Una_fully unaligned contigs               213                                                                                             
Una_Fully unaligned length                 46574                                                                                           
#Una_partially unaligned contigs           2                                                                                               
#Una_with misassembly                      0                                                                                               
#Una_both parts are significant            0                                                                                               
Una_Partially unaligned length             71                                                                                              
GAGE_Contigs #                             400                                                                                             
GAGE_Min contig                            200                                                                                             
GAGE_Max contig                            148058                                                                                          
GAGE_N50                                   45362 COUNT: 18                                                                                 
GAGE_Genome size                           2813862                                                                                         
GAGE_Assembly size                         2802213                                                                                         
GAGE_Chaff bases                           0                                                                                               
GAGE_Missing reference bases               22920(0.81%)                                                                                    
GAGE_Missing assembly bases                33595(1.20%)                                                                                    
GAGE_Missing assembly contigs              143(35.75%)                                                                                     
GAGE_Duplicated reference bases            12643                                                                                           
GAGE_Compressed reference bases            38766                                                                                           
GAGE_Bad trim                              2350                                                                                            
GAGE_Avg idy                               99.97                                                                                           
GAGE_SNPs                                  75                                                                                              
GAGE_Indels < 5bp                          437                                                                                             
GAGE_Indels >= 5                           14                                                                                              
GAGE_Inversions                            0                                                                                               
GAGE_Relocation                            4                                                                                               
GAGE_Translocation                         0                                                                                               
GAGE_Corrected contig #                    206                                                                                             
GAGE_Corrected assembly size               2758538                                                                                         
GAGE_Min correct contig                    203                                                                                             
GAGE_Max correct contig                    148067                                                                                          
GAGE_Corrected N50                         42599 COUNT: 20                                                                                 
