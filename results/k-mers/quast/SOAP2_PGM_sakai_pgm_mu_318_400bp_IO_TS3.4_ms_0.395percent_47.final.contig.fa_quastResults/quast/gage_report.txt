All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_47.final.contig
GAGE_Contigs #                   2302                                                                     
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  642                                                                      
GAGE_N50                         0 COUNT: 0                                                               
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               571564                                                                   
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     5060502(90.46%)                                                          
GAGE_Missing assembly bases      37833(6.62%)                                                             
GAGE_Missing assembly contigs    148(6.43%)                                                               
GAGE_Duplicated reference bases  634                                                                      
GAGE_Compressed reference bases  3316                                                                     
GAGE_Bad trim                    2392                                                                     
GAGE_Avg idy                     99.69                                                                    
GAGE_SNPs                        71                                                                       
GAGE_Indels < 5bp                1544                                                                     
GAGE_Indels >= 5                 1                                                                        
GAGE_Inversions                  1                                                                        
GAGE_Relocation                  0                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          2076                                                                     
GAGE_Corrected assembly size     519195                                                                   
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          642                                                                      
GAGE_Corrected N50               0 COUNT: 0                                                               
