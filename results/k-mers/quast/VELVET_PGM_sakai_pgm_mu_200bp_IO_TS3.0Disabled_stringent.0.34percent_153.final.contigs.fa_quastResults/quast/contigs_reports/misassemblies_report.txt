All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_153.final.contigs
#Mis_misassemblies               13                                                                                    
#Mis_relocations                 8                                                                                     
#Mis_translocations              0                                                                                     
#Mis_inversions                  5                                                                                     
#Mis_misassembled contigs        13                                                                                    
Mis_Misassembled contigs length  25287                                                                                 
#Mis_local misassemblies         7                                                                                     
#mismatches                      263                                                                                   
#indels                          3144                                                                                  
#Mis_short indels (<= 5 bp)      3144                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                     
Indels length                    3205                                                                                  
