All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_31.final.contigs
#Contigs (>= 0 bp)             32442                                                                      
#Contigs (>= 1000 bp)          11                                                                         
Total length (>= 0 bp)         4928975                                                                    
Total length (>= 1000 bp)      12700                                                                      
#Contigs                       6914                                                                       
Largest contig                 1585                                                                       
Total length                   2133028                                                                    
Reference length               5594470                                                                    
GC (%)                         51.88                                                                      
Reference GC (%)               50.48                                                                      
N50                            305                                                                        
NG50                           None                                                                       
N75                            243                                                                        
NG75                           None                                                                       
#misassemblies                 0                                                                          
#local misassemblies           0                                                                          
#unaligned contigs             0 + 1 part                                                                 
Unaligned contigs length       30                                                                         
Genome fraction (%)            37.843                                                                     
Duplication ratio              1.004                                                                      
#N's per 100 kbp               0.00                                                                       
#mismatches per 100 kbp        2.79                                                                       
#indels per 100 kbp            24.85                                                                      
#genes                         76 + 3481 part                                                             
#predicted genes (unique)      7112                                                                       
#predicted genes (>= 0 bp)     7112                                                                       
#predicted genes (>= 300 bp)   2022                                                                       
#predicted genes (>= 1500 bp)  0                                                                          
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              1585                                                                       
NA50                           305                                                                        
NGA50                          None                                                                       
NA75                           243                                                                        
NGA75                          None                                                                       
