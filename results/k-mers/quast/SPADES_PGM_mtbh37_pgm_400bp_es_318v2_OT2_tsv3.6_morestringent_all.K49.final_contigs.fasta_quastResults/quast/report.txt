All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K49.final_contigs
#Contigs (>= 0 bp)             455                                                                                
#Contigs (>= 1000 bp)          174                                                                                
Total length (>= 0 bp)         4328084                                                                            
Total length (>= 1000 bp)      4258477                                                                            
#Contigs                       325                                                                                
Largest contig                 158467                                                                             
Total length                   4317403                                                                            
Reference length               4411532                                                                            
GC (%)                         65.43                                                                              
Reference GC (%)               65.61                                                                              
N50                            52890                                                                              
NG50                           50187                                                                              
N75                            28195                                                                              
NG75                           27314                                                                              
#misassemblies                 8                                                                                  
#local misassemblies           41                                                                                 
#unaligned contigs             41 + 10 part                                                                       
Unaligned contigs length       11043                                                                              
Genome fraction (%)            97.226                                                                             
Duplication ratio              1.001                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        9.77                                                                               
#indels per 100 kbp            27.00                                                                              
#genes                         3900 + 136 part                                                                    
#predicted genes (unique)      4467                                                                               
#predicted genes (>= 0 bp)     4469                                                                               
#predicted genes (>= 300 bp)   3742                                                                               
#predicted genes (>= 1500 bp)  488                                                                                
#predicted genes (>= 3000 bp)  50                                                                                 
Largest alignment              158467                                                                             
NA50                           50187                                                                              
NGA50                          48905                                                                              
NA75                           28195                                                                              
NGA75                          27314                                                                              
