All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_173.final.contigs
GAGE_Contigs #                   2868                                                                        
GAGE_Min contig                  345                                                                         
GAGE_Max contig                  12504                                                                       
GAGE_N50                         1752 COUNT: 894                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               4488059                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1067434(19.08%)                                                             
GAGE_Missing assembly bases      128(0.00%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  43081                                                                       
GAGE_Compressed reference bases  227187                                                                      
GAGE_Bad trim                    106                                                                         
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        41                                                                          
GAGE_Indels < 5bp                611                                                                         
GAGE_Indels >= 5                 4                                                                           
GAGE_Inversions                  4                                                                           
GAGE_Relocation                  2                                                                           
GAGE_Translocation               2                                                                           
GAGE_Corrected contig #          2808                                                                        
GAGE_Corrected assembly size     4443327                                                                     
GAGE_Min correct contig          321                                                                         
GAGE_Max correct contig          12504                                                                       
GAGE_Corrected N50               1744 COUNT: 897                                                             
