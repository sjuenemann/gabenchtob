All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_87.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_87.final.scaf
#Contigs (>= 0 bp)             7213                                                                             5795                                                                    
#Contigs (>= 1000 bp)          1140                                                                             330                                                                     
Total length (>= 0 bp)         6076508                                                                          6144913                                                                 
Total length (>= 1000 bp)      4896722                                                                          5194901                                                                 
#Contigs                       1899                                                                             679                                                                     
Largest contig                 26896                                                                            108400                                                                  
Total length                   5231768                                                                          5329361                                                                 
Reference length               5594470                                                                          5594470                                                                 
GC (%)                         50.22                                                                            50.23                                                                   
Reference GC (%)               50.48                                                                            50.48                                                                   
N50                            5506                                                                             31776                                                                   
NG50                           5219                                                                             30524                                                                   
N75                            2987                                                                             16256                                                                   
NG75                           2408                                                                             13834                                                                   
#misassemblies                 6                                                                                29                                                                      
#local misassemblies           66                                                                               1118                                                                    
#unaligned contigs             11 + 0 part                                                                      16 + 14 part                                                            
Unaligned contigs length       9072                                                                             24764                                                                   
Genome fraction (%)            91.938                                                                           91.981                                                                  
Duplication ratio              1.002                                                                            1.020                                                                   
#N's per 100 kbp               7.38                                                                             1290.79                                                                 
#mismatches per 100 kbp        3.99                                                                             4.24                                                                    
#indels per 100 kbp            1.71                                                                             79.50                                                                   
#genes                         3756 + 1356 part                                                                 3854 + 1265 part                                                        
#predicted genes (unique)      6368                                                                             6066                                                                    
#predicted genes (>= 0 bp)     6372                                                                             6069                                                                    
#predicted genes (>= 300 bp)   4788                                                                             4730                                                                    
#predicted genes (>= 1500 bp)  497                                                                              529                                                                     
#predicted genes (>= 3000 bp)  34                                                                               37                                                                      
Largest alignment              26895                                                                            107545                                                                  
NA50                           5448                                                                             29926                                                                   
NGA50                          5195                                                                             27690                                                                   
NA75                           2953                                                                             14824                                                                   
NGA75                          2390                                                                             13101                                                                   
