All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_87.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_87.final.scaf
#Contigs                                   1899                                                                             679                                                                     
#Contigs (>= 0 bp)                         7213                                                                             5795                                                                    
#Contigs (>= 1000 bp)                      1140                                                                             330                                                                     
Largest contig                             26896                                                                            108400                                                                  
Total length                               5231768                                                                          5329361                                                                 
Total length (>= 0 bp)                     6076508                                                                          6144913                                                                 
Total length (>= 1000 bp)                  4896722                                                                          5194901                                                                 
Reference length                           5594470                                                                          5594470                                                                 
N50                                        5506                                                                             31776                                                                   
NG50                                       5219                                                                             30524                                                                   
N75                                        2987                                                                             16256                                                                   
NG75                                       2408                                                                             13834                                                                   
L50                                        283                                                                              53                                                                      
LG50                                       317                                                                              57                                                                      
L75                                        608                                                                              112                                                                     
LG75                                       709                                                                              125                                                                     
#local misassemblies                       66                                                                               1118                                                                    
#misassemblies                             6                                                                                29                                                                      
#misassembled contigs                      6                                                                                23                                                                      
Misassembled contigs length                11918                                                                            289883                                                                  
Misassemblies inter-contig overlap         5                                                                                11                                                                      
#unaligned contigs                         11 + 0 part                                                                      16 + 14 part                                                            
Unaligned contigs length                   9072                                                                             24764                                                                   
#ambiguously mapped contigs                191                                                                              159                                                                     
Extra bases in ambiguously mapped contigs  -69816                                                                           -57755                                                                  
#N's                                       386                                                                              68791                                                                   
#N's per 100 kbp                           7.38                                                                             1290.79                                                                 
Genome fraction (%)                        91.938                                                                           91.981                                                                  
Duplication ratio                          1.002                                                                            1.020                                                                   
#genes                                     3756 + 1356 part                                                                 3854 + 1265 part                                                        
#predicted genes (unique)                  6368                                                                             6066                                                                    
#predicted genes (>= 0 bp)                 6372                                                                             6069                                                                    
#predicted genes (>= 300 bp)               4788                                                                             4730                                                                    
#predicted genes (>= 1500 bp)              497                                                                              529                                                                     
#predicted genes (>= 3000 bp)              34                                                                               37                                                                      
#mismatches                                205                                                                              218                                                                     
#indels                                    88                                                                               4091                                                                    
Indels length                              713                                                                              8638                                                                    
#mismatches per 100 kbp                    3.99                                                                             4.24                                                                    
#indels per 100 kbp                        1.71                                                                             79.50                                                                   
GC (%)                                     50.22                                                                            50.23                                                                   
Reference GC (%)                           50.48                                                                            50.48                                                                   
Average %IDY                               99.352                                                                           99.265                                                                  
Largest alignment                          26895                                                                            107545                                                                  
NA50                                       5448                                                                             29926                                                                   
NGA50                                      5195                                                                             27690                                                                   
NA75                                       2953                                                                             14824                                                                   
NGA75                                      2390                                                                             13101                                                                   
LA50                                       283                                                                              56                                                                      
LGA50                                      317                                                                              60                                                                      
LA75                                       609                                                                              118                                                                     
LGA75                                      711                                                                              132                                                                     
#Mis_misassemblies                         6                                                                                29                                                                      
#Mis_relocations                           6                                                                                29                                                                      
#Mis_translocations                        0                                                                                0                                                                       
#Mis_inversions                            0                                                                                0                                                                       
#Mis_misassembled contigs                  6                                                                                23                                                                      
Mis_Misassembled contigs length            11918                                                                            289883                                                                  
#Mis_local misassemblies                   66                                                                               1118                                                                    
#Mis_short indels (<= 5 bp)                66                                                                               3943                                                                    
#Mis_long indels (> 5 bp)                  22                                                                               148                                                                     
#Una_fully unaligned contigs               11                                                                               16                                                                      
Una_Fully unaligned length                 9072                                                                             14410                                                                   
#Una_partially unaligned contigs           0                                                                                14                                                                      
#Una_with misassembly                      0                                                                                0                                                                       
#Una_both parts are significant            0                                                                                11                                                                      
Una_Partially unaligned length             0                                                                                10354                                                                   
GAGE_Contigs #                             1899                                                                             679                                                                     
GAGE_Min contig                            200                                                                              200                                                                     
GAGE_Max contig                            26896                                                                            108400                                                                  
GAGE_N50                                   5219 COUNT: 317                                                                  30524 COUNT: 57                                                         
GAGE_Genome size                           5594470                                                                          5594470                                                                 
GAGE_Assembly size                         5231768                                                                          5329361                                                                 
GAGE_Chaff bases                           0                                                                                0                                                                       
GAGE_Missing reference bases               228196(4.08%)                                                                    210090(3.76%)                                                           
GAGE_Missing assembly bases                8357(0.16%)                                                                      81558(1.53%)                                                            
GAGE_Missing assembly contigs              10(0.53%)                                                                        11(1.62%)                                                               
GAGE_Duplicated reference bases            13798                                                                            23446                                                                   
GAGE_Compressed reference bases            211331                                                                           213244                                                                  
GAGE_Bad trim                              28                                                                               6266                                                                    
GAGE_Avg idy                               99.99                                                                            99.98                                                                   
GAGE_SNPs                                  202                                                                              122                                                                     
GAGE_Indels < 5bp                          70                                                                               100                                                                     
GAGE_Indels >= 5                           82                                                                               1323                                                                    
GAGE_Inversions                            2                                                                                6                                                                       
GAGE_Relocation                            6                                                                                24                                                                      
GAGE_Translocation                         0                                                                                0                                                                       
GAGE_Corrected contig #                    1911                                                                             1863                                                                    
GAGE_Corrected assembly size               5206667                                                                          5198220                                                                 
GAGE_Min correct contig                    200                                                                              200                                                                     
GAGE_Max correct contig                    24524                                                                            24524                                                                   
GAGE_Corrected N50                         4899 COUNT: 334                                                                  4927 COUNT: 333                                                         
