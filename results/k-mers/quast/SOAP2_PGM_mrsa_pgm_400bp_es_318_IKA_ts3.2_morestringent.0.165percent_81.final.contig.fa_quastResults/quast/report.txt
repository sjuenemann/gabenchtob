All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_81.final.contig
#Contigs (>= 0 bp)             155508                                                                              
#Contigs (>= 1000 bp)          0                                                                                   
Total length (>= 0 bp)         22882813                                                                            
Total length (>= 1000 bp)      0                                                                                   
#Contigs                       26035                                                                               
Largest contig                 678                                                                                 
Total length                   7344013                                                                             
Reference length               2813862                                                                             
GC (%)                         31.74                                                                               
Reference GC (%)               32.81                                                                               
N50                            278                                                                                 
NG50                           320                                                                                 
N75                            258                                                                                 
NG75                           303                                                                                 
#misassemblies                 174                                                                                 
#local misassemblies           2                                                                                   
#unaligned contigs             3859 + 537 part                                                                     
Unaligned contigs length       1160944                                                                             
Genome fraction (%)            88.268                                                                              
Duplication ratio              2.479                                                                               
#N's per 100 kbp               0.00                                                                                
#mismatches per 100 kbp        150.22                                                                              
#indels per 100 kbp            2117.79                                                                             
#genes                         201 + 2440 part                                                                     
#predicted genes (unique)      20583                                                                               
#predicted genes (>= 0 bp)     20628                                                                               
#predicted genes (>= 300 bp)   253                                                                                 
#predicted genes (>= 1500 bp)  0                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                   
Largest alignment              678                                                                                 
NA50                           267                                                                                 
NGA50                          310                                                                                 
NA75                           245                                                                                 
NGA75                          292                                                                                 
