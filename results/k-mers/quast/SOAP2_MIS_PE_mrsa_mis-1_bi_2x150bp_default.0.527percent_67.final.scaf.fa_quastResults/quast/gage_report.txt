All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_67.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_67.final.scaf
GAGE_Contigs #                   164                                                                           49                                                                   
GAGE_Min contig                  200                                                                           200                                                                  
GAGE_Max contig                  148036                                                                        437773                                                               
GAGE_N50                         43780 COUNT: 18                                                               304416 COUNT: 4                                                      
GAGE_Genome size                 2813862                                                                       2813862                                                              
GAGE_Assembly size               2758048                                                                       2777696                                                              
GAGE_Chaff bases                 0                                                                             0                                                                    
GAGE_Missing reference bases     36031(1.28%)                                                                  24707(0.88%)                                                         
GAGE_Missing assembly bases      5594(0.20%)                                                                   14439(0.52%)                                                         
GAGE_Missing assembly contigs    1(0.61%)                                                                      1(2.04%)                                                             
GAGE_Duplicated reference bases  854                                                                           2014                                                                 
GAGE_Compressed reference bases  32373                                                                         35073                                                                
GAGE_Bad trim                    94                                                                            1255                                                                 
GAGE_Avg idy                     99.99                                                                         99.98                                                                
GAGE_SNPs                        38                                                                            44                                                                   
GAGE_Indels < 5bp                20                                                                            41                                                                   
GAGE_Indels >= 5                 9                                                                             125                                                                  
GAGE_Inversions                  2                                                                             2                                                                    
GAGE_Relocation                  1                                                                             21                                                                   
GAGE_Translocation               0                                                                             0                                                                    
GAGE_Corrected contig #          162                                                                           156                                                                  
GAGE_Corrected assembly size     2750410                                                                       2751024                                                              
GAGE_Min correct contig          200                                                                           200                                                                  
GAGE_Max correct contig          148036                                                                        148036                                                               
GAGE_Corrected N50               43780 COUNT: 18                                                               43780 COUNT: 18                                                      
