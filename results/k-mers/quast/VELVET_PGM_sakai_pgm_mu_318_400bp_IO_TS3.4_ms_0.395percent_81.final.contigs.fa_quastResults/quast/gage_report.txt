All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_81.final.contigs
GAGE_Contigs #                   15714                                                                      
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  706                                                                        
GAGE_N50                         209 COUNT: 11864                                                           
GAGE_Genome size                 5594470                                                                    
GAGE_Assembly size               3582677                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     2766867(49.46%)                                                            
GAGE_Missing assembly bases      17121(0.48%)                                                               
GAGE_Missing assembly contigs    36(0.23%)                                                                  
GAGE_Duplicated reference bases  476778                                                                     
GAGE_Compressed reference bases  228969                                                                     
GAGE_Bad trim                    8301                                                                       
GAGE_Avg idy                     98.62                                                                      
GAGE_SNPs                        1140                                                                       
GAGE_Indels < 5bp                30644                                                                      
GAGE_Indels >= 5                 10                                                                         
GAGE_Inversions                  131                                                                        
GAGE_Relocation                  68                                                                         
GAGE_Translocation               18                                                                         
GAGE_Corrected contig #          12534                                                                      
GAGE_Corrected assembly size     2867345                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          706                                                                        
GAGE_Corrected N50               201 COUNT: 12184                                                           
