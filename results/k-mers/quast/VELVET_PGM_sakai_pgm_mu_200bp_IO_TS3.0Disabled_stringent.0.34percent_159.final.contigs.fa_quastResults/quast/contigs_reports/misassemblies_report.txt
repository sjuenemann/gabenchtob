All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_159.final.contigs
#Mis_misassemblies               10                                                                                    
#Mis_relocations                 3                                                                                     
#Mis_translocations              0                                                                                     
#Mis_inversions                  7                                                                                     
#Mis_misassembled contigs        10                                                                                    
Mis_Misassembled contigs length  9124                                                                                  
#Mis_local misassemblies         1                                                                                     
#mismatches                      226                                                                                   
#indels                          2892                                                                                  
#Mis_short indels (<= 5 bp)      2892                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                     
Indels length                    2940                                                                                  
