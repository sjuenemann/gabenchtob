All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent.K55.final_contigs
GAGE_Contigs #                   591                                                                                   
GAGE_Min contig                  200                                                                                   
GAGE_Max contig                  247589                                                                                
GAGE_N50                         83770 COUNT: 21                                                                       
GAGE_Genome size                 5594470                                                                               
GAGE_Assembly size               5316439                                                                               
GAGE_Chaff bases                 0                                                                                     
GAGE_Missing reference bases     53912(0.96%)                                                                          
GAGE_Missing assembly bases      7(0.00%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                              
GAGE_Duplicated reference bases  239                                                                                   
GAGE_Compressed reference bases  253335                                                                                
GAGE_Bad trim                    7                                                                                     
GAGE_Avg idy                     99.97                                                                                 
GAGE_SNPs                        200                                                                                   
GAGE_Indels < 5bp                1468                                                                                  
GAGE_Indels >= 5                 13                                                                                    
GAGE_Inversions                  0                                                                                     
GAGE_Relocation                  7                                                                                     
GAGE_Translocation               0                                                                                     
GAGE_Corrected contig #          609                                                                                   
GAGE_Corrected assembly size     5319800                                                                               
GAGE_Min correct contig          201                                                                                   
GAGE_Max correct contig          217377                                                                                
GAGE_Corrected N50               65944 COUNT: 25                                                                       
