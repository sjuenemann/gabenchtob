All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_71.final.contigs
#Mis_misassemblies               175                                                                        
#Mis_relocations                 165                                                                        
#Mis_translocations              10                                                                         
#Mis_inversions                  0                                                                          
#Mis_misassembled contigs        175                                                                        
Mis_Misassembled contigs length  38803                                                                      
#Mis_local misassemblies         1                                                                          
#mismatches                      777                                                                        
#indels                          19070                                                                      
#Mis_short indels (<= 5 bp)      19070                                                                      
#Mis_long indels (> 5 bp)        0                                                                          
Indels length                    19415                                                                      
