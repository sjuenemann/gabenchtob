All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_91.final.contigs
#Mis_misassemblies               90                                                                                         
#Mis_relocations                 42                                                                                         
#Mis_translocations              2                                                                                          
#Mis_inversions                  46                                                                                         
#Mis_misassembled contigs        88                                                                                         
Mis_Misassembled contigs length  223697                                                                                     
#Mis_local misassemblies         15                                                                                         
#mismatches                      116                                                                                        
#indels                          783                                                                                        
#Mis_short indels (<= 5 bp)      783                                                                                        
#Mis_long indels (> 5 bp)        0                                                                                          
Indels length                    808                                                                                        
