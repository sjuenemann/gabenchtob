All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_193.final.contigs
#Contigs                                   1966                                                                           
#Contigs (>= 0 bp)                         1966                                                                           
#Contigs (>= 1000 bp)                      1201                                                                           
Largest contig                             9905                                                                           
Total length                               2993770                                                                        
Total length (>= 0 bp)                     2993770                                                                        
Total length (>= 1000 bp)                  2430152                                                                        
Reference length                           4411532                                                                        
N50                                        1811                                                                           
NG50                                       1188                                                                           
N75                                        1165                                                                           
NG75                                       None                                                                           
L50                                        516                                                                            
LG50                                       996                                                                            
L75                                        1030                                                                           
LG75                                       None                                                                           
#local misassemblies                       32                                                                             
#misassemblies                             20                                                                             
#misassembled contigs                      20                                                                             
Misassembled contigs length                61582                                                                          
Misassemblies inter-contig overlap         2101                                                                           
#unaligned contigs                         0 + 1 part                                                                     
Unaligned contigs length                   21                                                                             
#ambiguously mapped contigs                4                                                                              
Extra bases in ambiguously mapped contigs  -2601                                                                          
#N's                                       450                                                                            
#N's per 100 kbp                           15.03                                                                          
Genome fraction (%)                        65.405                                                                         
Duplication ratio                          1.037                                                                          
#genes                                     1449 + 1841 part                                                               
#predicted genes (unique)                  4274                                                                           
#predicted genes (>= 0 bp)                 4296                                                                           
#predicted genes (>= 300 bp)               2858                                                                           
#predicted genes (>= 1500 bp)              107                                                                            
#predicted genes (>= 3000 bp)              1                                                                              
#mismatches                                79                                                                             
#indels                                    1704                                                                           
Indels length                              1779                                                                           
#mismatches per 100 kbp                    2.74                                                                           
#indels per 100 kbp                        59.06                                                                          
GC (%)                                     65.14                                                                          
Reference GC (%)                           65.61                                                                          
Average %IDY                               99.883                                                                         
Largest alignment                          9905                                                                           
NA50                                       1807                                                                           
NGA50                                      1181                                                                           
NA75                                       1157                                                                           
NGA75                                      None                                                                           
LA50                                       519                                                                            
LGA50                                      1002                                                                           
LA75                                       1036                                                                           
LGA75                                      None                                                                           
#Mis_misassemblies                         20                                                                             
#Mis_relocations                           20                                                                             
#Mis_translocations                        0                                                                              
#Mis_inversions                            0                                                                              
#Mis_misassembled contigs                  20                                                                             
Mis_Misassembled contigs length            61582                                                                          
#Mis_local misassemblies                   32                                                                             
#Mis_short indels (<= 5 bp)                1703                                                                           
#Mis_long indels (> 5 bp)                  1                                                                              
#Una_fully unaligned contigs               0                                                                              
Una_Fully unaligned length                 0                                                                              
#Una_partially unaligned contigs           1                                                                              
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             21                                                                             
GAGE_Contigs #                             1966                                                                           
GAGE_Min contig                            385                                                                            
GAGE_Max contig                            9905                                                                           
GAGE_N50                                   1188 COUNT: 996                                                                
GAGE_Genome size                           4411532                                                                        
GAGE_Assembly size                         2993770                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               1506094(34.14%)                                                                
GAGE_Missing assembly bases                582(0.02%)                                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                                       
GAGE_Duplicated reference bases            29411                                                                          
GAGE_Compressed reference bases            26821                                                                          
GAGE_Bad trim                              402                                                                            
GAGE_Avg idy                               99.93                                                                          
GAGE_SNPs                                  58                                                                             
GAGE_Indels < 5bp                          1644                                                                           
GAGE_Indels >= 5                           18                                                                             
GAGE_Inversions                            0                                                                              
GAGE_Relocation                            2                                                                              
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    1944                                                                           
GAGE_Corrected assembly size               2965636                                                                        
GAGE_Min correct contig                    248                                                                            
GAGE_Max correct contig                    9908                                                                           
GAGE_Corrected N50                         1177 COUNT: 1018                                                               
