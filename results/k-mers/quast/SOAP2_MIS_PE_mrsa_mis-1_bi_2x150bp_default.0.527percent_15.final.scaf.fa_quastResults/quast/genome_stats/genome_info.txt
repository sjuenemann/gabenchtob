reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_15.final.scaf_broken  | 22.9380118854       | 1.00249905491     | 2085        | 21        | 1465      | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_15.final.scaf  | 12.9682265868       | 3.49040854133     | 2106        | 4         | 877       | None      | None      |
