All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_15.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_15.final.scaf
GAGE_Contigs #                   2418                                                                          1265                                                                 
GAGE_Min contig                  200                                                                           200                                                                  
GAGE_Max contig                  833                                                                           23165                                                                
GAGE_N50                         0 COUNT: 0                                                                    4725 COUNT: 183                                                      
GAGE_Genome size                 2813862                                                                       2813862                                                              
GAGE_Assembly size               674154                                                                        2962208                                                              
GAGE_Chaff bases                 0                                                                             0                                                                    
GAGE_Missing reference bases     2155850(76.62%)                                                               1247330(44.33%)                                                      
GAGE_Missing assembly bases      15048(2.23%)                                                                  1393450(47.04%)                                                      
GAGE_Missing assembly contigs    33(1.36%)                                                                     35(2.77%)                                                            
GAGE_Duplicated reference bases  220                                                                           298                                                                  
GAGE_Compressed reference bases  0                                                                             1284                                                                 
GAGE_Bad trim                    5287                                                                          146429                                                               
GAGE_Avg idy                     99.83                                                                         99.87                                                                
GAGE_SNPs                        77                                                                            162                                                                  
GAGE_Indels < 5bp                545                                                                           772                                                                  
GAGE_Indels >= 5                 142                                                                           9708                                                                 
GAGE_Inversions                  1                                                                             1                                                                    
GAGE_Relocation                  2                                                                             54                                                                   
GAGE_Translocation               0                                                                             0                                                                    
GAGE_Corrected contig #          2263                                                                          2021                                                                 
GAGE_Corrected assembly size     623556                                                                        563099                                                               
GAGE_Min correct contig          200                                                                           200                                                                  
GAGE_Max correct contig          835                                                                           993                                                                  
GAGE_Corrected N50               0 COUNT: 0                                                                    81 COUNT: 8768                                                       
