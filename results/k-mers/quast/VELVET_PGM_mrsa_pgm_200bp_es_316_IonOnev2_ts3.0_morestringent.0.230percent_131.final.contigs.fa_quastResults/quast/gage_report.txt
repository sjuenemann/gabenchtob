All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_131.final.contigs
GAGE_Contigs #                   2581                                                                                        
GAGE_Min contig                  261                                                                                         
GAGE_Max contig                  3883                                                                                        
GAGE_N50                         520 COUNT: 1656                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               1819842                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     1024447(36.41%)                                                                             
GAGE_Missing assembly bases      3182(0.17%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  1371                                                                                        
GAGE_Compressed reference bases  46144                                                                                       
GAGE_Bad trim                    3182                                                                                        
GAGE_Avg idy                     99.91                                                                                       
GAGE_SNPs                        131                                                                                         
GAGE_Indels < 5bp                1159                                                                                        
GAGE_Indels >= 5                 0                                                                                           
GAGE_Inversions                  4                                                                                           
GAGE_Relocation                  0                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          2579                                                                                        
GAGE_Corrected assembly size     1815796                                                                                     
GAGE_Min correct contig          259                                                                                         
GAGE_Max correct contig          3884                                                                                        
GAGE_Corrected N50               517 COUNT: 1661                                                                             
