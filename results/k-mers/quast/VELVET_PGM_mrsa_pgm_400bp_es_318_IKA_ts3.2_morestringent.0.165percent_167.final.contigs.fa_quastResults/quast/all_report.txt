All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_167.final.contigs
#Contigs                                   2245                                                                                   
#Contigs (>= 0 bp)                         2245                                                                                   
#Contigs (>= 1000 bp)                      680                                                                                    
Largest contig                             8445                                                                                   
Total length                               2051683                                                                                
Total length (>= 0 bp)                     2051683                                                                                
Total length (>= 1000 bp)                  1151274                                                                                
Reference length                           2813862                                                                                
N50                                        1123                                                                                   
NG50                                       782                                                                                    
N75                                        682                                                                                    
NG75                                       None                                                                                   
L50                                        562                                                                                    
LG50                                       969                                                                                    
L75                                        1149                                                                                   
LG75                                       None                                                                                   
#local misassemblies                       0                                                                                      
#misassemblies                             46                                                                                     
#misassembled contigs                      46                                                                                     
Misassembled contigs length                41964                                                                                  
Misassemblies inter-contig overlap         34                                                                                     
#unaligned contigs                         0 + 0 part                                                                             
Unaligned contigs length                   0                                                                                      
#ambiguously mapped contigs                16                                                                                     
Extra bases in ambiguously mapped contigs  -6520                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        69.307                                                                                 
Duplication ratio                          1.049                                                                                  
#genes                                     732 + 1395 part                                                                        
#predicted genes (unique)                  3671                                                                                   
#predicted genes (>= 0 bp)                 3693                                                                                   
#predicted genes (>= 300 bp)               2304                                                                                   
#predicted genes (>= 1500 bp)              50                                                                                     
#predicted genes (>= 3000 bp)              3                                                                                      
#mismatches                                41                                                                                     
#indels                                    608                                                                                    
Indels length                              623                                                                                    
#mismatches per 100 kbp                    2.10                                                                                   
#indels per 100 kbp                        31.18                                                                                  
GC (%)                                     32.57                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.692                                                                                 
Largest alignment                          8445                                                                                   
NA50                                       1117                                                                                   
NGA50                                      776                                                                                    
NA75                                       673                                                                                    
NGA75                                      None                                                                                   
LA50                                       565                                                                                    
LGA50                                      975                                                                                    
LA75                                       1157                                                                                   
LGA75                                      None                                                                                   
#Mis_misassemblies                         46                                                                                     
#Mis_relocations                           44                                                                                     
#Mis_translocations                        2                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  46                                                                                     
Mis_Misassembled contigs length            41964                                                                                  
#Mis_local misassemblies                   0                                                                                      
#Mis_short indels (<= 5 bp)                608                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             2245                                                                                   
GAGE_Min contig                            333                                                                                    
GAGE_Max contig                            8445                                                                                   
GAGE_N50                                   782 COUNT: 969                                                                         
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2051683                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               832379(29.58%)                                                                         
GAGE_Missing assembly bases                23(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            46285                                                                                  
GAGE_Compressed reference bases            33314                                                                                  
GAGE_Bad trim                              19                                                                                     
GAGE_Avg idy                               99.97                                                                                  
GAGE_SNPs                                  32                                                                                     
GAGE_Indels < 5bp                          552                                                                                    
GAGE_Indels >= 5                           0                                                                                      
GAGE_Inversions                            10                                                                                     
GAGE_Relocation                            6                                                                                      
GAGE_Translocation                         1                                                                                      
GAGE_Corrected contig #                    2150                                                                                   
GAGE_Corrected assembly size               2004011                                                                                
GAGE_Min correct contig                    202                                                                                    
GAGE_Max correct contig                    8445                                                                                   
GAGE_Corrected N50                         776 COUNT: 975                                                                         
