All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_21.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_21.final.scaf
GAGE_Contigs #                   13388                                                                            13388                                                                   
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  2271                                                                             2271                                                                    
GAGE_N50                         220 COUNT: 9550                                                                  220 COUNT: 9550                                                         
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               3600357                                                                          3600357                                                                 
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     3875278(69.27%)                                                                  3875278(69.27%)                                                         
GAGE_Missing assembly bases      1876846(52.13%)                                                                  1876846(52.13%)                                                         
GAGE_Missing assembly contigs    8097(60.48%)                                                                     8097(60.48%)                                                            
GAGE_Duplicated reference bases  0                                                                                0                                                                       
GAGE_Compressed reference bases  1554                                                                             1554                                                                    
GAGE_Bad trim                    77                                                                               77                                                                      
GAGE_Avg idy                     99.99                                                                            99.99                                                                   
GAGE_SNPs                        157                                                                              157                                                                     
GAGE_Indels < 5bp                7                                                                                7                                                                       
GAGE_Indels >= 5                 0                                                                                0                                                                       
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  0                                                                                0                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          5290                                                                             5290                                                                    
GAGE_Corrected assembly size     1723332                                                                          1723332                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          2271                                                                             2271                                                                    
GAGE_Corrected N50               0 COUNT: 0                                                                       0 COUNT: 0                                                              
