All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_17.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_17.final.scaf
GAGE_Contigs #                   7289                                                                             7285                                                                    
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  1612                                                                             2602                                                                    
GAGE_N50                         0 COUNT: 0                                                                       0 COUNT: 0                                                              
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               2081191                                                                          2107432                                                                 
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     4117479(73.60%)                                                                  4105325(73.38%)                                                         
GAGE_Missing assembly bases      601123(28.88%)                                                                   615089(29.19%)                                                          
GAGE_Missing assembly contigs    2526(34.65%)                                                                     2530(34.73%)                                                            
GAGE_Duplicated reference bases  0                                                                                0                                                                       
GAGE_Compressed reference bases  1409                                                                             1409                                                                    
GAGE_Bad trim                    414                                                                              4543                                                                    
GAGE_Avg idy                     99.97                                                                            99.97                                                                   
GAGE_SNPs                        388                                                                              424                                                                     
GAGE_Indels < 5bp                17                                                                               23                                                                      
GAGE_Indels >= 5                 7                                                                                108                                                                     
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  2                                                                                2                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          4762                                                                             4758                                                                    
GAGE_Corrected assembly size     1478616                                                                          1478158                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          1612                                                                             1612                                                                    
GAGE_Corrected N50               0 COUNT: 0                                                                       0 COUNT: 0                                                              
