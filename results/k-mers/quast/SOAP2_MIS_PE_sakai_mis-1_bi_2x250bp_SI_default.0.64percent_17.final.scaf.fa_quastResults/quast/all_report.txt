All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_17.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_17.final.scaf
#Contigs                                   7289                                                                             7285                                                                    
#Contigs (>= 0 bp)                         41694                                                                            41561                                                                   
#Contigs (>= 1000 bp)                      8                                                                                22                                                                      
Largest contig                             1612                                                                             2602                                                                    
Total length                               2081191                                                                          2107432                                                                 
Total length (>= 0 bp)                     6603540                                                                          6614625                                                                 
Total length (>= 1000 bp)                  9271                                                                             29587                                                                   
Reference length                           5594470                                                                          5594470                                                                 
N50                                        270                                                                              273                                                                     
NG50                                       None                                                                             None                                                                    
N75                                        229                                                                              230                                                                     
NG75                                       None                                                                             None                                                                    
L50                                        2734                                                                             2686                                                                    
LG50                                       None                                                                             None                                                                    
L75                                        4849                                                                             4818                                                                    
LG75                                       None                                                                             None                                                                    
#local misassemblies                       0                                                                                38                                                                      
#misassemblies                             2                                                                                2                                                                       
#misassembled contigs                      2                                                                                2                                                                       
Misassembled contigs length                647                                                                              647                                                                     
Misassemblies inter-contig overlap         18                                                                               18                                                                      
#unaligned contigs                         2529 + 4 part                                                                    2560 + 32 part                                                          
Unaligned contigs length                   602870                                                                           629141                                                                  
#ambiguously mapped contigs                6                                                                                6                                                                       
Extra bases in ambiguously mapped contigs  -1409                                                                            -1409                                                                   
#N's                                       52                                                                               11138                                                                   
#N's per 100 kbp                           2.50                                                                             528.51                                                                  
Genome fraction (%)                        26.319                                                                           26.210                                                                  
Duplication ratio                          1.003                                                                            1.007                                                                   
#genes                                     92 + 2919 part                                                                   90 + 2922 part                                                          
#predicted genes (unique)                  6753                                                                             6794                                                                    
#predicted genes (>= 0 bp)                 6753                                                                             6794                                                                    
#predicted genes (>= 300 bp)               1198                                                                             1200                                                                    
#predicted genes (>= 1500 bp)              0                                                                                0                                                                       
#predicted genes (>= 3000 bp)              0                                                                                0                                                                       
#mismatches                                392                                                                              414                                                                     
#indels                                    27                                                                               159                                                                     
Indels length                              50                                                                               372                                                                     
#mismatches per 100 kbp                    26.62                                                                            28.23                                                                   
#indels per 100 kbp                        1.83                                                                             10.84                                                                   
GC (%)                                     48.59                                                                            48.55                                                                   
Reference GC (%)                           50.48                                                                            50.48                                                                   
Average %IDY                               99.969                                                                           99.956                                                                  
Largest alignment                          1612                                                                             1699                                                                    
NA50                                       255                                                                              253                                                                     
NGA50                                      None                                                                             None                                                                    
NA75                                       None                                                                             None                                                                    
NGA75                                      None                                                                             None                                                                    
LA50                                       2815                                                                             2848                                                                    
LGA50                                      None                                                                             None                                                                    
LA75                                       None                                                                             None                                                                    
LGA75                                      None                                                                             None                                                                    
#Mis_misassemblies                         2                                                                                2                                                                       
#Mis_relocations                           2                                                                                2                                                                       
#Mis_translocations                        0                                                                                0                                                                       
#Mis_inversions                            0                                                                                0                                                                       
#Mis_misassembled contigs                  2                                                                                2                                                                       
Mis_Misassembled contigs length            647                                                                              647                                                                     
#Mis_local misassemblies                   0                                                                                38                                                                      
#Mis_short indels (<= 5 bp)                24                                                                               148                                                                     
#Mis_long indels (> 5 bp)                  3                                                                                11                                                                      
#Una_fully unaligned contigs               2529                                                                             2560                                                                    
Una_Fully unaligned length                 602491                                                                           620119                                                                  
#Una_partially unaligned contigs           4                                                                                32                                                                      
#Una_with misassembly                      0                                                                                0                                                                       
#Una_both parts are significant            0                                                                                7                                                                       
Una_Partially unaligned length             379                                                                              9022                                                                    
GAGE_Contigs #                             7289                                                                             7285                                                                    
GAGE_Min contig                            200                                                                              200                                                                     
GAGE_Max contig                            1612                                                                             2602                                                                    
GAGE_N50                                   0 COUNT: 0                                                                       0 COUNT: 0                                                              
GAGE_Genome size                           5594470                                                                          5594470                                                                 
GAGE_Assembly size                         2081191                                                                          2107432                                                                 
GAGE_Chaff bases                           0                                                                                0                                                                       
GAGE_Missing reference bases               4117479(73.60%)                                                                  4105325(73.38%)                                                         
GAGE_Missing assembly bases                601123(28.88%)                                                                   615089(29.19%)                                                          
GAGE_Missing assembly contigs              2526(34.65%)                                                                     2530(34.73%)                                                            
GAGE_Duplicated reference bases            0                                                                                0                                                                       
GAGE_Compressed reference bases            1409                                                                             1409                                                                    
GAGE_Bad trim                              414                                                                              4543                                                                    
GAGE_Avg idy                               99.97                                                                            99.97                                                                   
GAGE_SNPs                                  388                                                                              424                                                                     
GAGE_Indels < 5bp                          17                                                                               23                                                                      
GAGE_Indels >= 5                           7                                                                                108                                                                     
GAGE_Inversions                            0                                                                                0                                                                       
GAGE_Relocation                            2                                                                                2                                                                       
GAGE_Translocation                         0                                                                                0                                                                       
GAGE_Corrected contig #                    4762                                                                             4758                                                                    
GAGE_Corrected assembly size               1478616                                                                          1478158                                                                 
GAGE_Min correct contig                    200                                                                              200                                                                     
GAGE_Max correct contig                    1612                                                                             1612                                                                    
GAGE_Corrected N50                         0 COUNT: 0                                                                       0 COUNT: 0                                                              
