All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_127.final.contig
#Mis_misassemblies               5                                                                                   
#Mis_relocations                 3                                                                                   
#Mis_translocations              0                                                                                   
#Mis_inversions                  2                                                                                   
#Mis_misassembled contigs        5                                                                                   
Mis_Misassembled contigs length  3665                                                                                
#Mis_local misassemblies         1                                                                                   
#mismatches                      109                                                                                 
#indels                          2525                                                                                
#Mis_short indels (<= 5 bp)      2525                                                                                
#Mis_long indels (> 5 bp)        0                                                                                   
Indels length                    2608                                                                                
