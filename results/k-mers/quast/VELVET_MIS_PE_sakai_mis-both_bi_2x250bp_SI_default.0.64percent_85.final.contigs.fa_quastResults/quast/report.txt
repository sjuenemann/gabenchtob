All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_85.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_85.final.contigs
#Contigs (>= 0 bp)             628                                                                                     478                                                                            
#Contigs (>= 1000 bp)          292                                                                                     169                                                                            
Total length (>= 0 bp)         5405728                                                                                 5417864                                                                        
Total length (>= 1000 bp)      5279600                                                                                 5306011                                                                        
#Contigs                       566                                                                                     420                                                                            
Largest contig                 138451                                                                                  237842                                                                         
Total length                   5394429                                                                                 5407099                                                                        
Reference length               5594470                                                                                 5594470                                                                        
GC (%)                         50.31                                                                                   50.31                                                                          
Reference GC (%)               50.48                                                                                   50.48                                                                          
N50                            42947                                                                                   81195                                                                          
NG50                           38397                                                                                   81161                                                                          
N75                            17413                                                                                   43308                                                                          
NG75                           16086                                                                                   38461                                                                          
#misassemblies                 4                                                                                       14                                                                             
#local misassemblies           6                                                                                       103                                                                            
#unaligned contigs             1 + 0 part                                                                              1 + 1 part                                                                     
Unaligned contigs length       5470                                                                                    6556                                                                           
Genome fraction (%)            94.436                                                                                  94.518                                                                         
Duplication ratio              1.001                                                                                   1.003                                                                          
#N's per 100 kbp               0.00                                                                                    224.45                                                                         
#mismatches per 100 kbp        4.11                                                                                    5.01                                                                           
#indels per 100 kbp            0.36                                                                                    8.79                                                                           
#genes                         4862 + 301 part                                                                         4904 + 265 part                                                                
#predicted genes (unique)      5510                                                                                    5473                                                                           
#predicted genes (>= 0 bp)     5514                                                                                    5477                                                                           
#predicted genes (>= 300 bp)   4530                                                                                    4525                                                                           
#predicted genes (>= 1500 bp)  646                                                                                     650                                                                            
#predicted genes (>= 3000 bp)  67                                                                                      67                                                                             
Largest alignment              138451                                                                                  237655                                                                         
NA50                           42947                                                                                   77650                                                                          
NGA50                          38397                                                                                   74464                                                                          
NA75                           17413                                                                                   42873                                                                          
NGA75                          16086                                                                                   37853                                                                          
