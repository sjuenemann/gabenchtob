All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K111.final_contigs
#Contigs                                   13230                                                                                       
#Contigs (>= 0 bp)                         13512                                                                                       
#Contigs (>= 1000 bp)                      57                                                                                          
Largest contig                             236846                                                                                      
Total length                               7067295                                                                                     
Total length (>= 0 bp)                     7108352                                                                                     
Total length (>= 1000 bp)                  2764043                                                                                     
Reference length                           2813862                                                                                     
N50                                        391                                                                                         
NG50                                       116185                                                                                      
N75                                        311                                                                                         
NG75                                       49113                                                                                       
L50                                        1915                                                                                        
LG50                                       9                                                                                           
L75                                        6999                                                                                        
LG75                                       19                                                                                          
#local misassemblies                       8                                                                                           
#misassemblies                             74                                                                                          
#misassembled contigs                      74                                                                                          
Misassembled contigs length                100828                                                                                      
Misassemblies inter-contig overlap         2019                                                                                        
#unaligned contigs                         1826 + 364 part                                                                             
Unaligned contigs length                   609353                                                                                      
#ambiguously mapped contigs                79                                                                                          
Extra bases in ambiguously mapped contigs  -29571                                                                                      
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        98.751                                                                                      
Duplication ratio                          2.314                                                                                       
#genes                                     2664 + 57 part                                                                              
#predicted genes (unique)                  12671                                                                                       
#predicted genes (>= 0 bp)                 12682                                                                                       
#predicted genes (>= 300 bp)               2368                                                                                        
#predicted genes (>= 1500 bp)              287                                                                                         
#predicted genes (>= 3000 bp)              30                                                                                          
#mismatches                                209                                                                                         
#indels                                    717                                                                                         
Indels length                              745                                                                                         
#mismatches per 100 kbp                    7.52                                                                                        
#indels per 100 kbp                        25.80                                                                                       
GC (%)                                     32.03                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               97.519                                                                                      
Largest alignment                          236846                                                                                      
NA50                                       383                                                                                         
NGA50                                      116185                                                                                      
NA75                                       295                                                                                         
NGA75                                      49113                                                                                       
LA50                                       1959                                                                                        
LGA50                                      9                                                                                           
LA75                                       7257                                                                                        
LGA75                                      19                                                                                          
#Mis_misassemblies                         74                                                                                          
#Mis_relocations                           70                                                                                          
#Mis_translocations                        4                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  74                                                                                          
Mis_Misassembled contigs length            100828                                                                                      
#Mis_local misassemblies                   8                                                                                           
#Mis_short indels (<= 5 bp)                715                                                                                         
#Mis_long indels (> 5 bp)                  2                                                                                           
#Una_fully unaligned contigs               1826                                                                                        
Una_Fully unaligned length                 589848                                                                                      
#Una_partially unaligned contigs           364                                                                                         
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            1                                                                                           
Una_Partially unaligned length             19505                                                                                       
GAGE_Contigs #                             13230                                                                                       
GAGE_Min contig                            200                                                                                         
GAGE_Max contig                            236846                                                                                      
GAGE_N50                                   116185 COUNT: 9                                                                             
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         7067295                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               1692(0.06%)                                                                                 
GAGE_Missing assembly bases                296450(4.19%)                                                                               
GAGE_Missing assembly contigs              640(4.84%)                                                                                  
GAGE_Duplicated reference bases            3982730                                                                                     
GAGE_Compressed reference bases            35632                                                                                       
GAGE_Bad trim                              86421                                                                                       
GAGE_Avg idy                               99.98                                                                                       
GAGE_SNPs                                  52                                                                                          
GAGE_Indels < 5bp                          298                                                                                         
GAGE_Indels >= 5                           8                                                                                           
GAGE_Inversions                            0                                                                                           
GAGE_Relocation                            2                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    135                                                                                         
GAGE_Corrected assembly size               2790327                                                                                     
GAGE_Min correct contig                    202                                                                                         
GAGE_Max correct contig                    212206                                                                                      
GAGE_Corrected N50                         75657 COUNT: 11                                                                             
