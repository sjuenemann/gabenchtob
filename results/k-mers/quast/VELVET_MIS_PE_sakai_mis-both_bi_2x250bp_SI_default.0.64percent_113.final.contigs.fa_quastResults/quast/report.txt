All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_113.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_113.final.contigs
#Contigs (>= 0 bp)             861                                                                                      607                                                                             
#Contigs (>= 1000 bp)          487                                                                                      287                                                                             
Total length (>= 0 bp)         5420224                                                                                  5446137                                                                         
Total length (>= 1000 bp)      5270271                                                                                  5323252                                                                         
#Contigs                       854                                                                                      607                                                                             
Largest contig                 58738                                                                                    98486                                                                           
Total length                   5419231                                                                                  5446137                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.33                                                                                    50.33                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            17528                                                                                    35124                                                                           
NG50                           16831                                                                                    32804                                                                           
N75                            9821                                                                                     17962                                                                           
NG75                           8989                                                                                     16831                                                                           
#misassemblies                 4                                                                                        24                                                                              
#local misassemblies           4                                                                                        197                                                                             
#unaligned contigs             1 + 0 part                                                                               1 + 0 part                                                                      
Unaligned contigs length       5196                                                                                     5196                                                                            
Genome fraction (%)            94.675                                                                                   94.836                                                                          
Duplication ratio              1.001                                                                                    1.006                                                                           
#N's per 100 kbp               0.00                                                                                     475.81                                                                          
#mismatches per 100 kbp        1.96                                                                                     2.51                                                                            
#indels per 100 kbp            0.30                                                                                     14.93                                                                           
#genes                         4720 + 491 part                                                                          4766 + 455 part                                                                 
#predicted genes (unique)      5753                                                                                     5707                                                                            
#predicted genes (>= 0 bp)     5769                                                                                     5723                                                                            
#predicted genes (>= 300 bp)   4621                                                                                     4621                                                                            
#predicted genes (>= 1500 bp)  628                                                                                      631                                                                             
#predicted genes (>= 3000 bp)  54                                                                                       54                                                                              
Largest alignment              58738                                                                                    98171                                                                           
NA50                           17528                                                                                    32122                                                                           
NGA50                          16831                                                                                    32079                                                                           
NA75                           9717                                                                                     17599                                                                           
NGA75                          8748                                                                                     16170                                                                           
