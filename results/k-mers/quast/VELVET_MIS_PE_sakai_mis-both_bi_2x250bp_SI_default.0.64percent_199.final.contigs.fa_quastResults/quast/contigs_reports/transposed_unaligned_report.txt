All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                                 #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's 
VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_199.final.contigs_broken  1                             5584                        3                                 0                      1                                305                             0    
VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_199.final.contigs         1                             5584                        1                                 0                      0                                15                              76448
