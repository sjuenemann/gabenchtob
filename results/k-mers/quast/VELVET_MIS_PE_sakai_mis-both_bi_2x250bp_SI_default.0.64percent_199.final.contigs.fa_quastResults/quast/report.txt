All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_199.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_199.final.contigs
#Contigs (>= 0 bp)             1575                                                                                     1031                                                                            
#Contigs (>= 1000 bp)          1217                                                                                     814                                                                             
Total length (>= 0 bp)         5502266                                                                                  5578714                                                                         
Total length (>= 1000 bp)      5262672                                                                                  5438959                                                                         
#Contigs                       1575                                                                                     1031                                                                            
Largest contig                 27924                                                                                    43513                                                                           
Total length                   5502266                                                                                  5578714                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.36                                                                                    50.36                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            5736                                                                                     10233                                                                           
NG50                           5584                                                                                     10232                                                                           
N75                            3099                                                                                     5114                                                                            
NG75                           2997                                                                                     5050                                                                            
#misassemblies                 9                                                                                        15                                                                              
#local misassemblies           2                                                                                        503                                                                             
#unaligned contigs             1 + 3 part                                                                               1 + 1 part                                                                      
Unaligned contigs length       5889                                                                                     5599                                                                            
Genome fraction (%)            94.895                                                                                   94.944                                                                          
Duplication ratio              1.019                                                                                    1.033                                                                           
#N's per 100 kbp               0.00                                                                                     1370.35                                                                         
#mismatches per 100 kbp        6.05                                                                                     7.83                                                                            
#indels per 100 kbp            0.51                                                                                     11.62                                                                           
#genes                         4150 + 1119 part                                                                         4183 + 1088 part                                                                
#predicted genes (unique)      6380                                                                                     6309                                                                            
#predicted genes (>= 0 bp)     6415                                                                                     6344                                                                            
#predicted genes (>= 300 bp)   5047                                                                                     5058                                                                            
#predicted genes (>= 1500 bp)  534                                                                                      536                                                                             
#predicted genes (>= 3000 bp)  30                                                                                       30                                                                              
Largest alignment              27924                                                                                    42796                                                                           
NA50                           5660                                                                                     9748                                                                            
NGA50                          5545                                                                                     9748                                                                            
NA75                           3027                                                                                     4789                                                                            
NGA75                          2957                                                                                     4779                                                                            
