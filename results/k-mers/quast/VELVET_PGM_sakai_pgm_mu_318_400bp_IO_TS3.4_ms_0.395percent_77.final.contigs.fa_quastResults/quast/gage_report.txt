All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_77.final.contigs
GAGE_Contigs #                   12289                                                                      
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  706                                                                        
GAGE_N50                         0 COUNT: 0                                                                 
GAGE_Genome size                 5594470                                                                    
GAGE_Assembly size               2768722                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     3260350(58.28%)                                                            
GAGE_Missing assembly bases      18808(0.68%)                                                               
GAGE_Missing assembly contigs    42(0.34%)                                                                  
GAGE_Duplicated reference bases  262338                                                                     
GAGE_Compressed reference bases  200441                                                                     
GAGE_Bad trim                    8827                                                                       
GAGE_Avg idy                     98.53                                                                      
GAGE_SNPs                        1037                                                                       
GAGE_Indels < 5bp                27166                                                                      
GAGE_Indels >= 5                 7                                                                          
GAGE_Inversions                  133                                                                        
GAGE_Relocation                  56                                                                         
GAGE_Translocation               12                                                                         
GAGE_Corrected contig #          10184                                                                      
GAGE_Corrected assembly size     2299697                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          706                                                                        
GAGE_Corrected N50               0 COUNT: 0                                                                 
