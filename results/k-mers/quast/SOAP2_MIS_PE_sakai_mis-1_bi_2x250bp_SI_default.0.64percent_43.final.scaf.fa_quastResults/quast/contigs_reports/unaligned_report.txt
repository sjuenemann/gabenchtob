All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_43.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_43.final.scaf
#Una_fully unaligned contigs      37304                                                                            37304                                                                   
Una_Fully unaligned length        8682944                                                                          8682944                                                                 
#Una_partially unaligned contigs  3                                                                                3                                                                       
#Una_with misassembly             0                                                                                0                                                                       
#Una_both parts are significant   0                                                                                0                                                                       
Una_Partially unaligned length    110                                                                              110                                                                     
#N's                              0                                                                                0                                                                       
