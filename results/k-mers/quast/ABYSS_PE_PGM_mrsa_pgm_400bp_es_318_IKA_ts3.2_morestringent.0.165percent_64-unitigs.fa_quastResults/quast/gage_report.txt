All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_64-unitigs
GAGE_Contigs #                   502                                                                               
GAGE_Min contig                  202                                                                               
GAGE_Max contig                  52483                                                                             
GAGE_N50                         11625 COUNT: 73                                                                   
GAGE_Genome size                 2813862                                                                           
GAGE_Assembly size               2742382                                                                           
GAGE_Chaff bases                 0                                                                                 
GAGE_Missing reference bases     66619(2.37%)                                                                      
GAGE_Missing assembly bases      16(0.00%)                                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                                          
GAGE_Duplicated reference bases  1162                                                                              
GAGE_Compressed reference bases  13996                                                                             
GAGE_Bad trim                    16                                                                                
GAGE_Avg idy                     99.99                                                                             
GAGE_SNPs                        35                                                                                
GAGE_Indels < 5bp                100                                                                               
GAGE_Indels >= 5                 4                                                                                 
GAGE_Inversions                  0                                                                                 
GAGE_Relocation                  1                                                                                 
GAGE_Translocation               0                                                                                 
GAGE_Corrected contig #          501                                                                               
GAGE_Corrected assembly size     2741751                                                                           
GAGE_Min correct contig          202                                                                               
GAGE_Max correct contig          52484                                                                             
GAGE_Corrected N50               11456 COUNT: 74                                                                   
