All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_64-unitigs
#Contigs                                   502                                                                               
#Contigs (>= 0 bp)                         5147                                                                              
#Contigs (>= 1000 bp)                      359                                                                               
Largest contig                             52483                                                                             
Total length                               2742382                                                                           
Total length (>= 0 bp)                     3185707                                                                           
Total length (>= 1000 bp)                  2678064                                                                           
Reference length                           2813862                                                                           
N50                                        11998                                                                             
NG50                                       11625                                                                             
N75                                        6316                                                                              
NG75                                       5661                                                                              
L50                                        70                                                                                
LG50                                       73                                                                                
L75                                        146                                                                               
LG75                                       155                                                                               
#local misassemblies                       3                                                                                 
#misassemblies                             1                                                                                 
#misassembled contigs                      1                                                                                 
Misassembled contigs length                12222                                                                             
Misassemblies inter-contig overlap         476                                                                               
#unaligned contigs                         0 + 0 part                                                                        
Unaligned contigs length                   0                                                                                 
#ambiguously mapped contigs                6                                                                                 
Extra bases in ambiguously mapped contigs  -2022                                                                             
#N's                                       0                                                                                 
#N's per 100 kbp                           0.00                                                                              
Genome fraction (%)                        97.232                                                                            
Duplication ratio                          1.002                                                                             
#genes                                     2366 + 286 part                                                                   
#predicted genes (unique)                  2899                                                                              
#predicted genes (>= 0 bp)                 2900                                                                              
#predicted genes (>= 300 bp)               2386                                                                              
#predicted genes (>= 1500 bp)              258                                                                               
#predicted genes (>= 3000 bp)              25                                                                                
#mismatches                                56                                                                                
#indels                                    107                                                                               
Indels length                              110                                                                               
#mismatches per 100 kbp                    2.05                                                                              
#indels per 100 kbp                        3.91                                                                              
GC (%)                                     32.58                                                                             
Reference GC (%)                           32.81                                                                             
Average %IDY                               99.958                                                                            
Largest alignment                          52483                                                                             
NA50                                       11930                                                                             
NGA50                                      11507                                                                             
NA75                                       6165                                                                              
NGA75                                      5611                                                                              
LA50                                       70                                                                                
LGA50                                      73                                                                                
LA75                                       147                                                                               
LGA75                                      156                                                                               
#Mis_misassemblies                         1                                                                                 
#Mis_relocations                           1                                                                                 
#Mis_translocations                        0                                                                                 
#Mis_inversions                            0                                                                                 
#Mis_misassembled contigs                  1                                                                                 
Mis_Misassembled contigs length            12222                                                                             
#Mis_local misassemblies                   3                                                                                 
#Mis_short indels (<= 5 bp)                107                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                 
#Una_fully unaligned contigs               0                                                                                 
Una_Fully unaligned length                 0                                                                                 
#Una_partially unaligned contigs           0                                                                                 
#Una_with misassembly                      0                                                                                 
#Una_both parts are significant            0                                                                                 
Una_Partially unaligned length             0                                                                                 
GAGE_Contigs #                             502                                                                               
GAGE_Min contig                            202                                                                               
GAGE_Max contig                            52483                                                                             
GAGE_N50                                   11625 COUNT: 73                                                                   
GAGE_Genome size                           2813862                                                                           
GAGE_Assembly size                         2742382                                                                           
GAGE_Chaff bases                           0                                                                                 
GAGE_Missing reference bases               66619(2.37%)                                                                      
GAGE_Missing assembly bases                16(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                          
GAGE_Duplicated reference bases            1162                                                                              
GAGE_Compressed reference bases            13996                                                                             
GAGE_Bad trim                              16                                                                                
GAGE_Avg idy                               99.99                                                                             
GAGE_SNPs                                  35                                                                                
GAGE_Indels < 5bp                          100                                                                               
GAGE_Indels >= 5                           4                                                                                 
GAGE_Inversions                            0                                                                                 
GAGE_Relocation                            1                                                                                 
GAGE_Translocation                         0                                                                                 
GAGE_Corrected contig #                    501                                                                               
GAGE_Corrected assembly size               2741751                                                                           
GAGE_Min correct contig                    202                                                                               
GAGE_Max correct contig                    52484                                                                             
GAGE_Corrected N50                         11456 COUNT: 74                                                                   
