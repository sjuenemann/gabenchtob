All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_99.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_99.final.contigs
GAGE_Contigs #                   537                                                                                     415                                                                            
GAGE_Min contig                  200                                                                                     200                                                                            
GAGE_Max contig                  142720                                                                                  248965                                                                         
GAGE_N50                         41586 COUNT: 39                                                                         93950 COUNT: 18                                                                
GAGE_Genome size                 5594470                                                                                 5594470                                                                        
GAGE_Assembly size               5421191                                                                                 5433248                                                                        
GAGE_Chaff bases                 0                                                                                       0                                                                              
GAGE_Missing reference bases     11951(0.21%)                                                                            11785(0.21%)                                                                   
GAGE_Missing assembly bases      5554(0.10%)                                                                             17324(0.32%)                                                                   
GAGE_Missing assembly contigs    1(0.19%)                                                                                1(0.24%)                                                                       
GAGE_Duplicated reference bases  7297                                                                                    8121                                                                           
GAGE_Compressed reference bases  246671                                                                                  246637                                                                         
GAGE_Bad trim                    70                                                                                      207                                                                            
GAGE_Avg idy                     99.99                                                                                   99.99                                                                          
GAGE_SNPs                        107                                                                                     108                                                                            
GAGE_Indels < 5bp                16                                                                                      27                                                                             
GAGE_Indels >= 5                 3                                                                                       94                                                                             
GAGE_Inversions                  1                                                                                       10                                                                             
GAGE_Relocation                  4                                                                                       8                                                                              
GAGE_Translocation               0                                                                                       0                                                                              
GAGE_Corrected contig #          519                                                                                     513                                                                            
GAGE_Corrected assembly size     5411703                                                                                 5411625                                                                        
GAGE_Min correct contig          200                                                                                     200                                                                            
GAGE_Max correct contig          142720                                                                                  142720                                                                         
GAGE_Corrected N50               41586 COUNT: 39                                                                         41586 COUNT: 38                                                                
