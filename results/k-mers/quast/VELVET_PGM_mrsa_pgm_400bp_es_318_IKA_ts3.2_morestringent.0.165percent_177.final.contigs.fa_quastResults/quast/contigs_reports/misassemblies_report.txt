All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_177.final.contigs
#Mis_misassemblies               32                                                                                     
#Mis_relocations                 30                                                                                     
#Mis_translocations              2                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        31                                                                                     
Mis_Misassembled contigs length  61764                                                                                  
#Mis_local misassemblies         2                                                                                      
#mismatches                      57                                                                                     
#indels                          535                                                                                    
#Mis_short indels (<= 5 bp)      535                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    557                                                                                    
