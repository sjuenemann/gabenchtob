All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_99.final.contig
#Contigs                                   7577                                                                               
#Contigs (>= 0 bp)                         29744                                                                              
#Contigs (>= 1000 bp)                      1599                                                                               
Largest contig                             5905                                                                               
Total length                               5307419                                                                            
Total length (>= 0 bp)                     9192824                                                                            
Total length (>= 1000 bp)                  2547855                                                                            
Reference length                           5594470                                                                            
N50                                        957                                                                                
NG50                                       909                                                                                
N75                                        549                                                                                
NG75                                       488                                                                                
L50                                        1708                                                                               
LG50                                       1862                                                                               
L75                                        3519                                                                               
LG75                                       3936                                                                               
#local misassemblies                       0                                                                                  
#misassemblies                             7                                                                                  
#misassembled contigs                      7                                                                                  
Misassembled contigs length                4464                                                                               
Misassemblies inter-contig overlap         45                                                                                 
#unaligned contigs                         0 + 3 part                                                                         
Unaligned contigs length                   116                                                                                
#ambiguously mapped contigs                216                                                                                
Extra bases in ambiguously mapped contigs  -64436                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        90.195                                                                             
Duplication ratio                          1.039                                                                              
#genes                                     1606 + 3485 part                                                                   
#predicted genes (unique)                  10820                                                                              
#predicted genes (>= 0 bp)                 10855                                                                              
#predicted genes (>= 300 bp)               5825                                                                               
#predicted genes (>= 1500 bp)              99                                                                                 
#predicted genes (>= 3000 bp)              2                                                                                  
#mismatches                                74                                                                                 
#indels                                    1584                                                                               
Indels length                              1647                                                                               
#mismatches per 100 kbp                    1.47                                                                               
#indels per 100 kbp                        31.39                                                                              
GC (%)                                     50.22                                                                              
Reference GC (%)                           50.48                                                                              
Average %IDY                               99.659                                                                             
Largest alignment                          5905                                                                               
NA50                                       956                                                                                
NGA50                                      909                                                                                
NA75                                       544                                                                                
NGA75                                      485                                                                                
LA50                                       1709                                                                               
LGA50                                      1863                                                                               
LA75                                       3526                                                                               
LGA75                                      3945                                                                               
#Mis_misassemblies                         7                                                                                  
#Mis_relocations                           1                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            6                                                                                  
#Mis_misassembled contigs                  7                                                                                  
Mis_Misassembled contigs length            4464                                                                               
#Mis_local misassemblies                   0                                                                                  
#Mis_short indels (<= 5 bp)                1584                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               0                                                                                  
Una_Fully unaligned length                 0                                                                                  
#Una_partially unaligned contigs           3                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             116                                                                                
GAGE_Contigs #                             7577                                                                               
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            5905                                                                               
GAGE_N50                                   909 COUNT: 1862                                                                    
GAGE_Genome size                           5594470                                                                            
GAGE_Assembly size                         5307419                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               357608(6.39%)                                                                      
GAGE_Missing assembly bases                273(0.01%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            107797                                                                             
GAGE_Compressed reference bases            185512                                                                             
GAGE_Bad trim                              273                                                                                
GAGE_Avg idy                               99.97                                                                              
GAGE_SNPs                                  61                                                                                 
GAGE_Indels < 5bp                          1327                                                                               
GAGE_Indels >= 5                           0                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            1                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    7131                                                                               
GAGE_Corrected assembly size               5200662                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    5907                                                                               
GAGE_Corrected N50                         909 COUNT: 1862                                                                    
