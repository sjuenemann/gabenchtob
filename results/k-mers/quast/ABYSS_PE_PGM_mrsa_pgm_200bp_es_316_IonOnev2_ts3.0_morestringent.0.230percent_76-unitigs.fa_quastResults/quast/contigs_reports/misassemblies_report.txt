All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_76-unitigs
#Mis_misassemblies               6                                                                                      
#Mis_relocations                 3                                                                                      
#Mis_translocations              0                                                                                      
#Mis_inversions                  3                                                                                      
#Mis_misassembled contigs        6                                                                                      
Mis_Misassembled contigs length  21841                                                                                  
#Mis_local misassemblies         2                                                                                      
#mismatches                      72                                                                                     
#indels                          414                                                                                    
#Mis_short indels (<= 5 bp)      414                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    419                                                                                    
