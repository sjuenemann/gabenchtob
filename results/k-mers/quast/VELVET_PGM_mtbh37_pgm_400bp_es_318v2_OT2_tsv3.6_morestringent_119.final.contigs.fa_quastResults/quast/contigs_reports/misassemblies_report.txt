All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_119.final.contigs
#Mis_misassemblies               265                                                                            
#Mis_relocations                 264                                                                            
#Mis_translocations              0                                                                              
#Mis_inversions                  1                                                                              
#Mis_misassembled contigs        265                                                                            
Mis_Misassembled contigs length  78986                                                                          
#Mis_local misassemblies         4                                                                              
#mismatches                      2782                                                                           
#indels                          13915                                                                          
#Mis_short indels (<= 5 bp)      13908                                                                          
#Mis_long indels (> 5 bp)        7                                                                              
Indels length                    14250                                                                          
