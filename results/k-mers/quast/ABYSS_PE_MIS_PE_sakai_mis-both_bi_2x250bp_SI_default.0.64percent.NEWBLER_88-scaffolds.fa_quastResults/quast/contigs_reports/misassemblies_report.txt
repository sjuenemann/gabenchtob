All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_88-scaffolds_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_88-scaffolds
#Mis_misassemblies               7                                                                                             18                                                                                   
#Mis_relocations                 7                                                                                             18                                                                                   
#Mis_translocations              0                                                                                             0                                                                                    
#Mis_inversions                  0                                                                                             0                                                                                    
#Mis_misassembled contigs        7                                                                                             16                                                                                   
Mis_Misassembled contigs length  334025                                                                                        621650                                                                               
#Mis_local misassemblies         3                                                                                             29                                                                                   
#mismatches                      799                                                                                           856                                                                                  
#indels                          51                                                                                            252                                                                                  
#Mis_short indels (<= 5 bp)      39                                                                                            232                                                                                  
#Mis_long indels (> 5 bp)        12                                                                                            20                                                                                   
Indels length                    485                                                                                           907                                                                                  
