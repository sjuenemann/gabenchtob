All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_133.final.contigs
#Mis_misassemblies               7                                                                                           
#Mis_relocations                 2                                                                                           
#Mis_translocations              0                                                                                           
#Mis_inversions                  5                                                                                           
#Mis_misassembled contigs        7                                                                                           
Mis_Misassembled contigs length  5083                                                                                        
#Mis_local misassemblies         0                                                                                           
#mismatches                      109                                                                                         
#indels                          1211                                                                                        
#Mis_short indels (<= 5 bp)      1211                                                                                        
#Mis_long indels (> 5 bp)        0                                                                                           
Indels length                    1225                                                                                        
