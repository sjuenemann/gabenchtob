All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_123.final.contigs
#Contigs (>= 0 bp)             2994                                                                                  
#Contigs (>= 1000 bp)          1742                                                                                  
Total length (>= 0 bp)         5118178                                                                               
Total length (>= 1000 bp)      4381850                                                                               
#Contigs                       2994                                                                                  
Largest contig                 14694                                                                                 
Total length                   5118178                                                                               
Reference length               5594470                                                                               
GC (%)                         50.65                                                                                 
Reference GC (%)               50.48                                                                                 
N50                            2548                                                                                  
NG50                           2278                                                                                  
N75                            1448                                                                                  
NG75                           1166                                                                                  
#misassemblies                 83                                                                                    
#local misassemblies           72                                                                                    
#unaligned contigs             0 + 23 part                                                                           
Unaligned contigs length       992                                                                                   
Genome fraction (%)            87.511                                                                                
Duplication ratio              1.027                                                                                 
#N's per 100 kbp               28.33                                                                                 
#mismatches per 100 kbp        4.19                                                                                  
#indels per 100 kbp            41.63                                                                                 
#genes                         2929 + 2064 part                                                                      
#predicted genes (unique)      7571                                                                                  
#predicted genes (>= 0 bp)     7581                                                                                  
#predicted genes (>= 300 bp)   5237                                                                                  
#predicted genes (>= 1500 bp)  300                                                                                   
#predicted genes (>= 3000 bp)  10                                                                                    
Largest alignment              14694                                                                                 
NA50                           2465                                                                                  
NGA50                          2238                                                                                  
NA75                           1419                                                                                  
NGA75                          1133                                                                                  
