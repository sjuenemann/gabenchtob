All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K77.final_contigs_broken  SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K77.final_contigs
GAGE_Contigs #                   738                                                                                      738                                                                             
GAGE_Min contig                  200                                                                                      200                                                                             
GAGE_Max contig                  260955                                                                                   260955                                                                          
GAGE_N50                         95542 COUNT: 19                                                                          95542 COUNT: 19                                                                 
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               5430317                                                                                  5430317                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     18146(0.32%)                                                                             18146(0.32%)                                                                    
GAGE_Missing assembly bases      76350(1.41%)                                                                             76350(1.41%)                                                                    
GAGE_Missing assembly contigs    180(24.39%)                                                                              180(24.39%)                                                                     
GAGE_Duplicated reference bases  1038                                                                                     1038                                                                            
GAGE_Compressed reference bases  287296                                                                                   287296                                                                          
GAGE_Bad trim                    235                                                                                      235                                                                             
GAGE_Avg idy                     99.99                                                                                    99.99                                                                           
GAGE_SNPs                        389                                                                                      389                                                                             
GAGE_Indels < 5bp                15                                                                                       15                                                                              
GAGE_Indels >= 5                 2                                                                                        2                                                                               
GAGE_Inversions                  0                                                                                        0                                                                               
GAGE_Relocation                  3                                                                                        3                                                                               
GAGE_Translocation               0                                                                                        0                                                                               
GAGE_Corrected contig #          561                                                                                      561                                                                             
GAGE_Corrected assembly size     5353721                                                                                  5353721                                                                         
GAGE_Min correct contig          200                                                                                      200                                                                             
GAGE_Max correct contig          226953                                                                                   226953                                                                          
GAGE_Corrected N50               95542 COUNT: 19                                                                          95542 COUNT: 19                                                                 
