All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_88-unitigs
#Mis_misassemblies               1                                                                         
#Mis_relocations                 1                                                                         
#Mis_translocations              0                                                                         
#Mis_inversions                  0                                                                         
#Mis_misassembled contigs        1                                                                         
Mis_Misassembled contigs length  249                                                                       
#Mis_local misassemblies         13                                                                        
#mismatches                      133                                                                       
#indels                          835                                                                       
#Mis_short indels (<= 5 bp)      832                                                                       
#Mis_long indels (> 5 bp)        3                                                                         
Indels length                    977                                                                       
