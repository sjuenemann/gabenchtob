All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_205.final.contigs
#Contigs (>= 0 bp)             1937                                                                           
#Contigs (>= 1000 bp)          1217                                                                           
Total length (>= 0 bp)         2976677                                                                        
Total length (>= 1000 bp)      2411632                                                                        
#Contigs                       1937                                                                           
Largest contig                 8477                                                                           
Total length                   2976677                                                                        
Reference length               4411532                                                                        
GC (%)                         65.04                                                                          
Reference GC (%)               65.61                                                                          
N50                            1826                                                                           
NG50                           1145                                                                           
N75                            1126                                                                           
NG75                           None                                                                           
#misassemblies                 29                                                                             
#local misassemblies           12                                                                             
#unaligned contigs             1 + 0 part                                                                     
Unaligned contigs length       696                                                                            
Genome fraction (%)            65.347                                                                         
Duplication ratio              1.033                                                                          
#N's per 100 kbp               11.76                                                                          
#mismatches per 100 kbp        2.88                                                                           
#indels per 100 kbp            64.35                                                                          
#genes                         1405 + 1934 part                                                               
#predicted genes (unique)      4630                                                                           
#predicted genes (>= 0 bp)     4652                                                                           
#predicted genes (>= 300 bp)   3079                                                                           
#predicted genes (>= 1500 bp)  125                                                                            
#predicted genes (>= 3000 bp)  1                                                                              
Largest alignment              8477                                                                           
NA50                           1807                                                                           
NGA50                          1137                                                                           
NA75                           1116                                                                           
NGA75                          None                                                                           
