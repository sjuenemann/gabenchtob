All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K63.final_contigs
GAGE_Contigs #                   747                                                                             
GAGE_Min contig                  200                                                                             
GAGE_Max contig                  268226                                                                          
GAGE_N50                         97797 COUNT: 19                                                                 
GAGE_Genome size                 5594470                                                                         
GAGE_Assembly size               5378560                                                                         
GAGE_Chaff bases                 0                                                                               
GAGE_Missing reference bases     31384(0.56%)                                                                    
GAGE_Missing assembly bases      18106(0.34%)                                                                    
GAGE_Missing assembly contigs    63(8.43%)                                                                       
GAGE_Duplicated reference bases  30341                                                                           
GAGE_Compressed reference bases  277231                                                                          
GAGE_Bad trim                    1064                                                                            
GAGE_Avg idy                     99.98                                                                           
GAGE_SNPs                        209                                                                             
GAGE_Indels < 5bp                371                                                                             
GAGE_Indels >= 5                 14                                                                              
GAGE_Inversions                  0                                                                               
GAGE_Relocation                  8                                                                               
GAGE_Translocation               0                                                                               
GAGE_Corrected contig #          574                                                                             
GAGE_Corrected assembly size     5333380                                                                         
GAGE_Min correct contig          200                                                                             
GAGE_Max correct contig          217393                                                                          
GAGE_Corrected N50               74559 COUNT: 22                                                                 
