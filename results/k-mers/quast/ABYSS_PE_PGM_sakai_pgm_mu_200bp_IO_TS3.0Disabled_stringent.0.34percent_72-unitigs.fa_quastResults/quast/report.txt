All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_72-unitigs
#Contigs (>= 0 bp)             5253                                                                             
#Contigs (>= 1000 bp)          778                                                                              
Total length (>= 0 bp)         5775915                                                                          
Total length (>= 1000 bp)      5051089                                                                          
#Contigs                       1339                                                                             
Largest contig                 46065                                                                            
Total length                   5290528                                                                          
Reference length               5594470                                                                          
GC (%)                         50.24                                                                            
Reference GC (%)               50.48                                                                            
N50                            9462                                                                             
NG50                           8890                                                                             
N75                            4874                                                                             
NG75                           4221                                                                             
#misassemblies                 3                                                                                
#local misassemblies           4                                                                                
#unaligned contigs             0 + 1 part                                                                       
Unaligned contigs length       47                                                                               
Genome fraction (%)            93.184                                                                           
Duplication ratio              1.002                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        1.44                                                                             
#indels per 100 kbp            26.13                                                                            
#genes                         4277 + 777 part                                                                  
#predicted genes (unique)      6464                                                                             
#predicted genes (>= 0 bp)     6469                                                                             
#predicted genes (>= 300 bp)   4902                                                                             
#predicted genes (>= 1500 bp)  484                                                                              
#predicted genes (>= 3000 bp)  31                                                                               
Largest alignment              46065                                                                            
NA50                           9462                                                                             
NGA50                          8890                                                                             
NA75                           4848                                                                             
NGA75                          4221                                                                             
