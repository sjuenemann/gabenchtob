All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K29.final_contigs
#Contigs                                   545                                                                                
#Contigs (>= 0 bp)                         1037                                                                               
#Contigs (>= 1000 bp)                      354                                                                                
Largest contig                             137652                                                                             
Total length                               4269816                                                                            
Total length (>= 0 bp)                     4303228                                                                            
Total length (>= 1000 bp)                  4185820                                                                            
Reference length                           4411532                                                                            
N50                                        22402                                                                              
NG50                                       21682                                                                              
N75                                        11604                                                                              
NG75                                       9796                                                                               
L50                                        55                                                                                 
LG50                                       58                                                                                 
L75                                        121                                                                                
LG75                                       131                                                                                
#local misassemblies                       38                                                                                 
#misassemblies                             17                                                                                 
#misassembled contigs                      13                                                                                 
Misassembled contigs length                182039                                                                             
Misassemblies inter-contig overlap         13312                                                                              
#unaligned contigs                         11 + 10 part                                                                       
Unaligned contigs length                   5625                                                                               
#ambiguously mapped contigs                24                                                                                 
Extra bases in ambiguously mapped contigs  -17941                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        96.195                                                                             
Duplication ratio                          1.004                                                                              
#genes                                     3776 + 252 part                                                                    
#predicted genes (unique)                  4531                                                                               
#predicted genes (>= 0 bp)                 4532                                                                               
#predicted genes (>= 300 bp)               3768                                                                               
#predicted genes (>= 1500 bp)              477                                                                                
#predicted genes (>= 3000 bp)              40                                                                                 
#mismatches                                453                                                                                
#indels                                    1006                                                                               
Indels length                              1294                                                                               
#mismatches per 100 kbp                    10.67                                                                              
#indels per 100 kbp                        23.71                                                                              
GC (%)                                     65.42                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.733                                                                             
Largest alignment                          137652                                                                             
NA50                                       22387                                                                              
NGA50                                      21423                                                                              
NA75                                       11073                                                                              
NGA75                                      9796                                                                               
LA50                                       56                                                                                 
LGA50                                      59                                                                                 
LA75                                       123                                                                                
LGA75                                      133                                                                                
#Mis_misassemblies                         17                                                                                 
#Mis_relocations                           16                                                                                 
#Mis_translocations                        0                                                                                  
#Mis_inversions                            1                                                                                  
#Mis_misassembled contigs                  13                                                                                 
Mis_Misassembled contigs length            182039                                                                             
#Mis_local misassemblies                   38                                                                                 
#Mis_short indels (<= 5 bp)                997                                                                                
#Mis_long indels (> 5 bp)                  9                                                                                  
#Una_fully unaligned contigs               11                                                                                 
Una_Fully unaligned length                 4727                                                                               
#Una_partially unaligned contigs           10                                                                                 
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            1                                                                                  
Una_Partially unaligned length             898                                                                                
GAGE_Contigs #                             545                                                                                
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            137652                                                                             
GAGE_N50                                   21682 COUNT: 58                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4269816                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               96705(2.19%)                                                                       
GAGE_Missing assembly bases                5661(0.13%)                                                                        
GAGE_Missing assembly contigs              11(2.02%)                                                                          
GAGE_Duplicated reference bases            693                                                                                
GAGE_Compressed reference bases            56250                                                                              
GAGE_Bad trim                              831                                                                                
GAGE_Avg idy                               99.96                                                                              
GAGE_SNPs                                  282                                                                                
GAGE_Indels < 5bp                          1017                                                                               
GAGE_Indels >= 5                           41                                                                                 
GAGE_Inversions                            10                                                                                 
GAGE_Relocation                            14                                                                                 
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    584                                                                                
GAGE_Corrected assembly size               4265969                                                                            
GAGE_Min correct contig                    202                                                                                
GAGE_Max correct contig                    79533                                                                              
GAGE_Corrected N50                         19869 COUNT: 66                                                                    
