All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_74-unitigs
#Contigs                                   2360                                                                      
#Contigs (>= 0 bp)                         9862                                                                      
#Contigs (>= 1000 bp)                      1337                                                                      
Largest contig                             13194                                                                     
Total length                               4146881                                                                   
Total length (>= 0 bp)                     5126415                                                                   
Total length (>= 1000 bp)                  3607321                                                                   
Reference length                           4411532                                                                   
N50                                        2776                                                                      
NG50                                       2613                                                                      
N75                                        1557                                                                      
NG75                                       1345                                                                      
L50                                        453                                                                       
LG50                                       502                                                                       
L75                                        944                                                                       
LG75                                       1080                                                                      
#local misassemblies                       14                                                                        
#misassemblies                             1                                                                         
#misassembled contigs                      1                                                                         
Misassembled contigs length                2195                                                                      
Misassemblies inter-contig overlap         1959                                                                      
#unaligned contigs                         1 + 1 part                                                                
Unaligned contigs length                   701                                                                       
#ambiguously mapped contigs                34                                                                        
Extra bases in ambiguously mapped contigs  -14570                                                                    
#N's                                       0                                                                         
#N's per 100 kbp                           0.00                                                                      
Genome fraction (%)                        93.523                                                                    
Duplication ratio                          1.002                                                                     
#genes                                     2403 + 1618 part                                                          
#predicted genes (unique)                  5764                                                                      
#predicted genes (>= 0 bp)                 5767                                                                      
#predicted genes (>= 300 bp)               4188                                                                      
#predicted genes (>= 1500 bp)              290                                                                       
#predicted genes (>= 3000 bp)              14                                                                        
#mismatches                                148                                                                       
#indels                                    771                                                                       
Indels length                              914                                                                       
#mismatches per 100 kbp                    3.59                                                                      
#indels per 100 kbp                        18.69                                                                     
GC (%)                                     65.34                                                                     
Reference GC (%)                           65.61                                                                     
Average %IDY                               99.930                                                                    
Largest alignment                          13194                                                                     
NA50                                       2776                                                                      
NGA50                                      2613                                                                      
NA75                                       1556                                                                      
NGA75                                      1343                                                                      
LA50                                       453                                                                       
LGA50                                      502                                                                       
LA75                                       944                                                                       
LGA75                                      1081                                                                      
#Mis_misassemblies                         1                                                                         
#Mis_relocations                           1                                                                         
#Mis_translocations                        0                                                                         
#Mis_inversions                            0                                                                         
#Mis_misassembled contigs                  1                                                                         
Mis_Misassembled contigs length            2195                                                                      
#Mis_local misassemblies                   14                                                                        
#Mis_short indels (<= 5 bp)                768                                                                       
#Mis_long indels (> 5 bp)                  3                                                                         
#Una_fully unaligned contigs               1                                                                         
Una_Fully unaligned length                 387                                                                       
#Una_partially unaligned contigs           1                                                                         
#Una_with misassembly                      0                                                                         
#Una_both parts are significant            0                                                                         
Una_Partially unaligned length             314                                                                       
GAGE_Contigs #                             2360                                                                      
GAGE_Min contig                            200                                                                       
GAGE_Max contig                            13194                                                                     
GAGE_N50                                   2613 COUNT: 502                                                           
GAGE_Genome size                           4411532                                                                   
GAGE_Assembly size                         4146881                                                                   
GAGE_Chaff bases                           0                                                                         
GAGE_Missing reference bases               251666(5.70%)                                                             
GAGE_Missing assembly bases                706(0.02%)                                                                
GAGE_Missing assembly contigs              1(0.04%)                                                                  
GAGE_Duplicated reference bases            1172                                                                      
GAGE_Compressed reference bases            26733                                                                     
GAGE_Bad trim                              319                                                                       
GAGE_Avg idy                               99.98                                                                     
GAGE_SNPs                                  95                                                                        
GAGE_Indels < 5bp                          793                                                                       
GAGE_Indels >= 5                           9                                                                         
GAGE_Inversions                            0                                                                         
GAGE_Relocation                            9                                                                         
GAGE_Translocation                         0                                                                         
GAGE_Corrected contig #                    2370                                                                      
GAGE_Corrected assembly size               4147756                                                                   
GAGE_Min correct contig                    200                                                                       
GAGE_Max correct contig                    13199                                                                     
GAGE_Corrected N50                         2588 COUNT: 508                                                           
