All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_25.final.contigs
#Contigs                                   22                                                                                    
#Contigs (>= 0 bp)                         42924                                                                                 
#Contigs (>= 1000 bp)                      0                                                                                     
Largest contig                             328                                                                                   
Total length                               5070                                                                                  
Total length (>= 0 bp)                     2914174                                                                               
Total length (>= 1000 bp)                  0                                                                                     
Reference length                           2813862                                                                               
N50                                        226                                                                                   
NG50                                       None                                                                                  
N75                                        209                                                                                   
NG75                                       None                                                                                  
L50                                        10                                                                                    
LG50                                       None                                                                                  
L75                                        16                                                                                    
LG75                                       None                                                                                  
#local misassemblies                       0                                                                                     
#misassemblies                             0                                                                                     
#misassembled contigs                      0                                                                                     
Misassembled contigs length                0                                                                                     
Misassemblies inter-contig overlap         0                                                                                     
#unaligned contigs                         14 + 0 part                                                                           
Unaligned contigs length                   3320                                                                                  
#ambiguously mapped contigs                0                                                                                     
Extra bases in ambiguously mapped contigs  0                                                                                     
#N's                                       0                                                                                     
#N's per 100 kbp                           0.00                                                                                  
Genome fraction (%)                        0.062                                                                                 
Duplication ratio                          1.000                                                                                 
#genes                                     0 + 8 part                                                                            
#predicted genes (unique)                  19                                                                                    
#predicted genes (>= 0 bp)                 19                                                                                    
#predicted genes (>= 300 bp)               0                                                                                     
#predicted genes (>= 1500 bp)              0                                                                                     
#predicted genes (>= 3000 bp)              0                                                                                     
#mismatches                                1                                                                                     
#indels                                    0                                                                                     
Indels length                              0                                                                                     
#mismatches per 100 kbp                    57.14                                                                                 
#indels per 100 kbp                        0.00                                                                                  
GC (%)                                     34.83                                                                                 
Reference GC (%)                           32.81                                                                                 
Average %IDY                               99.938                                                                                
Largest alignment                          255                                                                                   
NA50                                       None                                                                                  
NGA50                                      None                                                                                  
NA75                                       None                                                                                  
NGA75                                      None                                                                                  
LA50                                       None                                                                                  
LGA50                                      None                                                                                  
LA75                                       None                                                                                  
LGA75                                      None                                                                                  
#Mis_misassemblies                         0                                                                                     
#Mis_relocations                           0                                                                                     
#Mis_translocations                        0                                                                                     
#Mis_inversions                            0                                                                                     
#Mis_misassembled contigs                  0                                                                                     
Mis_Misassembled contigs length            0                                                                                     
#Mis_local misassemblies                   0                                                                                     
#Mis_short indels (<= 5 bp)                0                                                                                     
#Mis_long indels (> 5 bp)                  0                                                                                     
#Una_fully unaligned contigs               14                                                                                    
Una_Fully unaligned length                 3320                                                                                  
#Una_partially unaligned contigs           0                                                                                     
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             0                                                                                     
GAGE_Contigs #                             22                                                                                    
GAGE_Min contig                            202                                                                                   
GAGE_Max contig                            328                                                                                   
GAGE_N50                                   0 COUNT: 0                                                                            
GAGE_Genome size                           2813862                                                                               
GAGE_Assembly size                         5070                                                                                  
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               2812112(99.94%)                                                                       
GAGE_Missing assembly bases                3320(65.48%)                                                                          
GAGE_Missing assembly contigs              14(63.64%)                                                                            
GAGE_Duplicated reference bases            0                                                                                     
GAGE_Compressed reference bases            0                                                                                     
GAGE_Bad trim                              0                                                                                     
GAGE_Avg idy                               99.94                                                                                 
GAGE_SNPs                                  1                                                                                     
GAGE_Indels < 5bp                          0                                                                                     
GAGE_Indels >= 5                           0                                                                                     
GAGE_Inversions                            0                                                                                     
GAGE_Relocation                            0                                                                                     
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    8                                                                                     
GAGE_Corrected assembly size               1750                                                                                  
GAGE_Min correct contig                    202                                                                                   
GAGE_Max correct contig                    255                                                                                   
GAGE_Corrected N50                         0 COUNT: 0                                                                            
