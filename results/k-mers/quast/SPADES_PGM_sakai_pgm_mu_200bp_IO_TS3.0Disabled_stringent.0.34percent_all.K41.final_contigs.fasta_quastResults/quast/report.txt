All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K41.final_contigs
#Contigs (>= 0 bp)             1558                                                                                      
#Contigs (>= 1000 bp)          259                                                                                       
Total length (>= 0 bp)         5357865                                                                                   
Total length (>= 1000 bp)      5131818                                                                                   
#Contigs                       636                                                                                       
Largest contig                 200148                                                                                    
Total length                   5279489                                                                                   
Reference length               5594470                                                                                   
GC (%)                         50.25                                                                                     
Reference GC (%)               50.48                                                                                     
N50                            58457                                                                                     
NG50                           51769                                                                                     
N75                            22378                                                                                     
NG75                           19307                                                                                     
#misassemblies                 3                                                                                         
#local misassemblies           12                                                                                        
#unaligned contigs             12 + 0 part                                                                               
Unaligned contigs length       2656                                                                                      
Genome fraction (%)            92.619                                                                                    
Duplication ratio              1.001                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        3.80                                                                                      
#indels per 100 kbp            24.24                                                                                     
#genes                         4744 + 228 part                                                                           
#predicted genes (unique)      5919                                                                                      
#predicted genes (>= 0 bp)     5919                                                                                      
#predicted genes (>= 300 bp)   4682                                                                                      
#predicted genes (>= 1500 bp)  539                                                                                       
#predicted genes (>= 3000 bp)  39                                                                                        
Largest alignment              200148                                                                                    
NA50                           58457                                                                                     
NGA50                          51769                                                                                     
NA75                           22378                                                                                     
NGA75                          19002                                                                                     
