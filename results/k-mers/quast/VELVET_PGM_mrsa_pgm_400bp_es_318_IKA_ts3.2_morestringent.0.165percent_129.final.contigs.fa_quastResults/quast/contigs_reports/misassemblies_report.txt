All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_129.final.contigs
#Mis_misassemblies               304                                                                                    
#Mis_relocations                 275                                                                                    
#Mis_translocations              25                                                                                     
#Mis_inversions                  4                                                                                      
#Mis_misassembled contigs        304                                                                                    
Mis_Misassembled contigs length  94663                                                                                  
#Mis_local misassemblies         2                                                                                      
#mismatches                      1505                                                                                   
#indels                          18328                                                                                  
#Mis_short indels (<= 5 bp)      18328                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    18756                                                                                  
