All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_41.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_41.final.scaf
#Contigs                                   43047                                                                            43047                                                                   
#Contigs (>= 0 bp)                         128355                                                                           128355                                                                  
#Contigs (>= 1000 bp)                      99                                                                               99                                                                      
Largest contig                             1885                                                                             1885                                                                    
Total length                               11078647                                                                         11078647                                                                
Total length (>= 0 bp)                     24079529                                                                         24079529                                                                
Total length (>= 1000 bp)                  120972                                                                           120972                                                                  
Reference length                           5594470                                                                          5594470                                                                 
N50                                        247                                                                              247                                                                     
NG50                                       251                                                                              251                                                                     
N75                                        222                                                                              222                                                                     
NG75                                       251                                                                              251                                                                     
L50                                        18101                                                                            18101                                                                   
LG50                                       7137                                                                             7137                                                                    
L75                                        29887                                                                            29887                                                                   
LG75                                       12709                                                                            12709                                                                   
#local misassemblies                       0                                                                                0                                                                       
#misassemblies                             1                                                                                1                                                                       
#misassembled contigs                      1                                                                                1                                                                       
Misassembled contigs length                305                                                                              305                                                                     
Misassemblies inter-contig overlap         0                                                                                0                                                                       
#unaligned contigs                         34525 + 1 part                                                                   34525 + 1 part                                                          
Unaligned contigs length                   8025714                                                                          8025714                                                                 
#ambiguously mapped contigs                22                                                                               22                                                                      
Extra bases in ambiguously mapped contigs  -5594                                                                            -5594                                                                   
#N's                                       0                                                                                0                                                                       
#N's per 100 kbp                           0.00                                                                             0.00                                                                    
Genome fraction (%)                        53.345                                                                           53.345                                                                  
Duplication ratio                          1.021                                                                            1.021                                                                   
#genes                                     282 + 4072 part                                                                  282 + 4072 part                                                         
#predicted genes (unique)                  36914                                                                            36914                                                                   
#predicted genes (>= 0 bp)                 36914                                                                            36914                                                                   
#predicted genes (>= 300 bp)               3065                                                                             3065                                                                    
#predicted genes (>= 1500 bp)              0                                                                                0                                                                       
#predicted genes (>= 3000 bp)              0                                                                                0                                                                       
#mismatches                                130                                                                              130                                                                     
#indels                                    6                                                                                6                                                                       
Indels length                              6                                                                                6                                                                       
#mismatches per 100 kbp                    4.36                                                                             4.36                                                                    
#indels per 100 kbp                        0.20                                                                             0.20                                                                    
GC (%)                                     53.64                                                                            53.64                                                                   
Reference GC (%)                           50.48                                                                            50.48                                                                   
Average %IDY                               99.993                                                                           99.993                                                                  
Largest alignment                          1885                                                                             1885                                                                    
NA50                                       None                                                                             None                                                                    
NGA50                                      221                                                                              221                                                                     
NA75                                       None                                                                             None                                                                    
NGA75                                      None                                                                             None                                                                    
LA50                                       None                                                                             None                                                                    
LGA50                                      7309                                                                             7309                                                                    
LA75                                       None                                                                             None                                                                    
LGA75                                      None                                                                             None                                                                    
#Mis_misassemblies                         1                                                                                1                                                                       
#Mis_relocations                           1                                                                                1                                                                       
#Mis_translocations                        0                                                                                0                                                                       
#Mis_inversions                            0                                                                                0                                                                       
#Mis_misassembled contigs                  1                                                                                1                                                                       
Mis_Misassembled contigs length            305                                                                              305                                                                     
#Mis_local misassemblies                   0                                                                                0                                                                       
#Mis_short indels (<= 5 bp)                6                                                                                6                                                                       
#Mis_long indels (> 5 bp)                  0                                                                                0                                                                       
#Una_fully unaligned contigs               34525                                                                            34525                                                                   
Una_Fully unaligned length                 8025609                                                                          8025609                                                                 
#Una_partially unaligned contigs           1                                                                                1                                                                       
#Una_with misassembly                      0                                                                                0                                                                       
#Una_both parts are significant            0                                                                                0                                                                       
Una_Partially unaligned length             105                                                                              105                                                                     
GAGE_Contigs #                             43047                                                                            43047                                                                   
GAGE_Min contig                            200                                                                              200                                                                     
GAGE_Max contig                            1885                                                                             1885                                                                    
GAGE_N50                                   251 COUNT: 7137                                                                  251 COUNT: 7137                                                         
GAGE_Genome size                           5594470                                                                          5594470                                                                 
GAGE_Assembly size                         11078647                                                                         11078647                                                                
GAGE_Chaff bases                           0                                                                                0                                                                       
GAGE_Missing reference bases               2455663(43.89%)                                                                  2455663(43.89%)                                                         
GAGE_Missing assembly bases                7824807(70.63%)                                                                  7824807(70.63%)                                                         
GAGE_Missing assembly contigs              33527(77.88%)                                                                    33527(77.88%)                                                           
GAGE_Duplicated reference bases            45660                                                                            45660                                                                   
GAGE_Compressed reference bases            28540                                                                            28540                                                                   
GAGE_Bad trim                              21798                                                                            21798                                                                   
GAGE_Avg idy                               99.80                                                                            99.80                                                                   
GAGE_SNPs                                  5183                                                                             5183                                                                    
GAGE_Indels < 5bp                          37                                                                               37                                                                      
GAGE_Indels >= 5                           0                                                                                0                                                                       
GAGE_Inversions                            0                                                                                0                                                                       
GAGE_Relocation                            1                                                                                1                                                                       
GAGE_Translocation                         0                                                                                0                                                                       
GAGE_Corrected contig #                    9126                                                                             9126                                                                    
GAGE_Corrected assembly size               3186382                                                                          3186382                                                                 
GAGE_Min correct contig                    200                                                                              200                                                                     
GAGE_Max correct contig                    1885                                                                             1885                                                                    
GAGE_Corrected N50                         226 COUNT: 7289                                                                  226 COUNT: 7289                                                         
