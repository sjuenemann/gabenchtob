All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_145.final.contigs
#Mis_misassemblies               147                                                                         
#Mis_relocations                 138                                                                         
#Mis_translocations              9                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        146                                                                         
Mis_Misassembled contigs length  85244                                                                       
#Mis_local misassemblies         1                                                                           
#mismatches                      62                                                                          
#indels                          1301                                                                        
#Mis_short indels (<= 5 bp)      1301                                                                        
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    1319                                                                        
