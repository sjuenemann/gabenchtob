All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_82-unitigs
#Contigs (>= 0 bp)             3775                                                                              
#Contigs (>= 1000 bp)          299                                                                               
Total length (>= 0 bp)         3157502                                                                           
Total length (>= 1000 bp)      2690218                                                                           
#Contigs                       437                                                                               
Largest contig                 67202                                                                             
Total length                   2755366                                                                           
Reference length               2813862                                                                           
GC (%)                         32.60                                                                             
Reference GC (%)               32.81                                                                             
N50                            14605                                                                             
NG50                           14303                                                                             
N75                            7634                                                                              
NG75                           7229                                                                              
#misassemblies                 1                                                                                 
#local misassemblies           1                                                                                 
#unaligned contigs             0 + 0 part                                                                        
Unaligned contigs length       0                                                                                 
Genome fraction (%)            97.630                                                                            
Duplication ratio              1.002                                                                             
#N's per 100 kbp               0.00                                                                              
#mismatches per 100 kbp        1.09                                                                              
#indels per 100 kbp            4.00                                                                              
#genes                         2437 + 242 part                                                                   
#predicted genes (unique)      2878                                                                              
#predicted genes (>= 0 bp)     2884                                                                              
#predicted genes (>= 300 bp)   2374                                                                              
#predicted genes (>= 1500 bp)  267                                                                               
#predicted genes (>= 3000 bp)  23                                                                                
Largest alignment              67202                                                                             
NA50                           14605                                                                             
NGA50                          14303                                                                             
NA75                           7634                                                                              
NGA75                          7229                                                                              
