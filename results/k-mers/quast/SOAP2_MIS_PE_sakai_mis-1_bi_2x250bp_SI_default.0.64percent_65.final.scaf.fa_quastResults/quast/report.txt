All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_65.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_65.final.scaf
#Contigs (>= 0 bp)             104323                                                                           104311                                                                  
#Contigs (>= 1000 bp)          600                                                                              600                                                                     
Total length (>= 0 bp)         24933679                                                                         24934592                                                                
Total length (>= 1000 bp)      798359                                                                           798639                                                                  
#Contigs                       82194                                                                            82195                                                                   
Largest contig                 4341                                                                             4341                                                                    
Total length                   21778894                                                                         21781802                                                                
Reference length               5594470                                                                          5594470                                                                 
GC (%)                         53.88                                                                            53.88                                                                   
Reference GC (%)               50.48                                                                            50.48                                                                   
N50                            250                                                                              250                                                                     
NG50                           464                                                                              465                                                                     
N75                            228                                                                              228                                                                     
NG75                           280                                                                              280                                                                     
#misassemblies                 2                                                                                2                                                                       
#local misassemblies           0                                                                                5                                                                       
#unaligned contigs             60082 + 2304 part                                                                60083 + 2307 part                                                       
Unaligned contigs length       14542312                                                                         14543960                                                                
Genome fraction (%)            89.911                                                                           89.915                                                                  
Duplication ratio              1.417                                                                            1.417                                                                   
#N's per 100 kbp               0.00                                                                             4.20                                                                    
#mismatches per 100 kbp        572.94                                                                           565.02                                                                  
#indels per 100 kbp            4.81                                                                             4.75                                                                    
#genes                         809 + 4298 part                                                                  809 + 4299 part                                                         
#predicted genes (unique)      74336                                                                            74345                                                                   
#predicted genes (>= 0 bp)     74455                                                                            74464                                                                   
#predicted genes (>= 300 bp)   5705                                                                             5707                                                                    
#predicted genes (>= 1500 bp)  13                                                                               13                                                                      
#predicted genes (>= 3000 bp)  0                                                                                0                                                                       
Largest alignment              4341                                                                             4341                                                                    
NA50                           None                                                                             None                                                                    
NGA50                          463                                                                              464                                                                     
NA75                           None                                                                             None                                                                    
NGA75                          274                                                                              274                                                                     
