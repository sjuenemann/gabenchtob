All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_51.final.contigs
#Contigs (>= 0 bp)             16385                                                                         
#Contigs (>= 1000 bp)          44                                                                            
Total length (>= 0 bp)         3483210                                                                       
Total length (>= 1000 bp)      53273                                                                         
#Contigs                       6054                                                                          
Largest contig                 2123                                                                          
Total length                   2054537                                                                       
Reference length               4411532                                                                       
GC (%)                         67.13                                                                         
Reference GC (%)               65.61                                                                         
N50                            342                                                                           
NG50                           None                                                                          
N75                            257                                                                           
NG75                           None                                                                          
#misassemblies                 0                                                                             
#local misassemblies           1                                                                             
#unaligned contigs             2 + 6 part                                                                    
Unaligned contigs length       750                                                                           
Genome fraction (%)            46.223                                                                        
Duplication ratio              1.006                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        5.88                                                                          
#indels per 100 kbp            53.01                                                                         
#genes                         90 + 3102 part                                                                
#predicted genes (unique)      6368                                                                          
#predicted genes (>= 0 bp)     6368                                                                          
#predicted genes (>= 300 bp)   1964                                                                          
#predicted genes (>= 1500 bp)  1                                                                             
#predicted genes (>= 3000 bp)  0                                                                             
Largest alignment              2123                                                                          
NA50                           342                                                                           
NGA50                          None                                                                          
NA75                           257                                                                           
NGA75                          None                                                                          
