All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_107.final.contigs
#Contigs                                   1931                                                                                        
#Contigs (>= 0 bp)                         1931                                                                                        
#Contigs (>= 1000 bp)                      930                                                                                         
Largest contig                             10419                                                                                       
Total length                               2443749                                                                                     
Total length (>= 0 bp)                     2443749                                                                                     
Total length (>= 1000 bp)                  1842152                                                                                     
Reference length                           2813862                                                                                     
N50                                        1674                                                                                        
NG50                                       1493                                                                                        
N75                                        1009                                                                                        
NG75                                       720                                                                                         
L50                                        458                                                                                         
LG50                                       575                                                                                         
L75                                        921                                                                                         
LG75                                       1246                                                                                        
#local misassemblies                       3                                                                                           
#misassemblies                             48                                                                                          
#misassembled contigs                      48                                                                                          
Misassembled contigs length                87234                                                                                       
Misassemblies inter-contig overlap         1029                                                                                        
#unaligned contigs                         0 + 41 part                                                                                 
Unaligned contigs length                   1228                                                                                        
#ambiguously mapped contigs                28                                                                                          
Extra bases in ambiguously mapped contigs  -8578                                                                                       
#N's                                       80                                                                                          
#N's per 100 kbp                           3.27                                                                                        
Genome fraction (%)                        84.848                                                                                      
Duplication ratio                          1.020                                                                                       
#genes                                     1208 + 1305 part                                                                            
#predicted genes (unique)                  3602                                                                                        
#predicted genes (>= 0 bp)                 3604                                                                                        
#predicted genes (>= 300 bp)               2526                                                                                        
#predicted genes (>= 1500 bp)              123                                                                                         
#predicted genes (>= 3000 bp)              7                                                                                           
#mismatches                                203                                                                                         
#indels                                    1090                                                                                        
Indels length                              1109                                                                                        
#mismatches per 100 kbp                    8.50                                                                                        
#indels per 100 kbp                        45.65                                                                                       
GC (%)                                     32.90                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               99.701                                                                                      
Largest alignment                          10419                                                                                       
NA50                                       1663                                                                                        
NGA50                                      1486                                                                                        
NA75                                       1004                                                                                        
NGA75                                      700                                                                                         
LA50                                       461                                                                                         
LGA50                                      579                                                                                         
LA75                                       927                                                                                         
LGA75                                      1256                                                                                        
#Mis_misassemblies                         48                                                                                          
#Mis_relocations                           22                                                                                          
#Mis_translocations                        0                                                                                           
#Mis_inversions                            26                                                                                          
#Mis_misassembled contigs                  48                                                                                          
Mis_Misassembled contigs length            87234                                                                                       
#Mis_local misassemblies                   3                                                                                           
#Mis_short indels (<= 5 bp)                1090                                                                                        
#Mis_long indels (> 5 bp)                  0                                                                                           
#Una_fully unaligned contigs               0                                                                                           
Una_Fully unaligned length                 0                                                                                           
#Una_partially unaligned contigs           41                                                                                          
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             1228                                                                                        
GAGE_Contigs #                             1931                                                                                        
GAGE_Min contig                            213                                                                                         
GAGE_Max contig                            10419                                                                                       
GAGE_N50                                   1493 COUNT: 575                                                                             
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         2443749                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               390444(13.88%)                                                                              
GAGE_Missing assembly bases                2079(0.09%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            6822                                                                                        
GAGE_Compressed reference bases            39193                                                                                       
GAGE_Bad trim                              1850                                                                                        
GAGE_Avg idy                               99.94                                                                                       
GAGE_SNPs                                  135                                                                                         
GAGE_Indels < 5bp                          995                                                                                         
GAGE_Indels >= 5                           2                                                                                           
GAGE_Inversions                            8                                                                                           
GAGE_Relocation                            2                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    1927                                                                                        
GAGE_Corrected assembly size               2435822                                                                                     
GAGE_Min correct contig                    210                                                                                         
GAGE_Max correct contig                    10419                                                                                       
GAGE_Corrected N50                         1486 COUNT: 579                                                                             
