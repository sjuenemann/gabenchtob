All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_173.final.contigs
#Contigs (>= 0 bp)             60                                                                                          
#Contigs (>= 1000 bp)          3                                                                                           
Total length (>= 0 bp)         35955                                                                                       
Total length (>= 1000 bp)      3229                                                                                        
#Contigs                       60                                                                                          
Largest contig                 1147                                                                                        
Total length                   35955                                                                                       
Reference length               2813862                                                                                     
GC (%)                         35.34                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            586                                                                                         
NG50                           None                                                                                        
N75                            539                                                                                         
NG75                           None                                                                                        
#misassemblies                 1                                                                                           
#local misassemblies           0                                                                                           
#unaligned contigs             0 + 0 part                                                                                  
Unaligned contigs length       0                                                                                           
Genome fraction (%)            1.234                                                                                       
Duplication ratio              1.005                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        2.88                                                                                        
#indels per 100 kbp            66.23                                                                                       
#genes                         6 + 60 part                                                                                 
#predicted genes (unique)      70                                                                                          
#predicted genes (>= 0 bp)     70                                                                                          
#predicted genes (>= 300 bp)   54                                                                                          
#predicted genes (>= 1500 bp)  0                                                                                           
#predicted genes (>= 3000 bp)  0                                                                                           
Largest alignment              1147                                                                                        
NA50                           577                                                                                         
NGA50                          None                                                                                        
NA75                           529                                                                                         
NGA75                          None                                                                                        
