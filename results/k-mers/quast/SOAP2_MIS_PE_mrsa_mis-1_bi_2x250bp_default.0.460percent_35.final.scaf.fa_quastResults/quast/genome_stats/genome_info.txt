reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_35.final.scaf_broken  | 81.9041232299       | 1.00955842249     | 1826        | 675       | 1846      | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_35.final.scaf  | 76.3520385861       | 1.0217785827      | 1634        | 775       | 1630      | None      | None      |
