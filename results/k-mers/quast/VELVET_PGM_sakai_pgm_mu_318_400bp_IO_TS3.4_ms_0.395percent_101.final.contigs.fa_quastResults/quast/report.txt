All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_101.final.contigs
#Contigs (>= 0 bp)             40478                                                                       
#Contigs (>= 1000 bp)          0                                                                           
Total length (>= 0 bp)         9315909                                                                     
Total length (>= 1000 bp)      0                                                                           
#Contigs                       40478                                                                       
Largest contig                 655                                                                         
Total length                   9315909                                                                     
Reference length               5594470                                                                     
GC (%)                         50.23                                                                       
Reference GC (%)               50.48                                                                       
N50                            223                                                                         
NG50                           247                                                                         
N75                            203                                                                         
NG75                           228                                                                         
#misassemblies                 1036                                                                        
#local misassemblies           7                                                                           
#unaligned contigs             218 + 110 part                                                              
Unaligned contigs length       64303                                                                       
Genome fraction (%)            77.797                                                                      
Duplication ratio              2.050                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        89.86                                                                       
#indels per 100 kbp            911.00                                                                      
#genes                         148 + 4844 part                                                             
#predicted genes (unique)      36843                                                                       
#predicted genes (>= 0 bp)     36949                                                                       
#predicted genes (>= 300 bp)   220                                                                         
#predicted genes (>= 1500 bp)  0                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              655                                                                         
NA50                           219                                                                         
NGA50                          244                                                                         
NA75                           201                                                                         
NGA75                          224                                                                         
