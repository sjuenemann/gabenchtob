All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_61.final.contig
GAGE_Contigs #                   11012                                                                              
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  1780                                                                               
GAGE_N50                         255 COUNT: 6759                                                                    
GAGE_Genome size                 5594470                                                                            
GAGE_Assembly size               3739384                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     2015253(36.02%)                                                                    
GAGE_Missing assembly bases      43795(1.17%)                                                                       
GAGE_Missing assembly contigs    143(1.30%)                                                                         
GAGE_Duplicated reference bases  89549                                                                              
GAGE_Compressed reference bases  55082                                                                              
GAGE_Bad trim                    13003                                                                              
GAGE_Avg idy                     99.86                                                                              
GAGE_SNPs                        293                                                                                
GAGE_Indels < 5bp                3421                                                                               
GAGE_Indels >= 5                 5                                                                                  
GAGE_Inversions                  6                                                                                  
GAGE_Relocation                  2                                                                                  
GAGE_Translocation               1                                                                                  
GAGE_Corrected contig #          10224                                                                              
GAGE_Corrected assembly size     3569772                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          1780                                                                               
GAGE_Corrected N50               255 COUNT: 6760                                                                    
