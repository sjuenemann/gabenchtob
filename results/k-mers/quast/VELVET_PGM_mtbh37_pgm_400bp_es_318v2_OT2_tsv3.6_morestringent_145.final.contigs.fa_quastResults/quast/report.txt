All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_145.final.contigs
#Contigs (>= 0 bp)             4013                                                                           
#Contigs (>= 1000 bp)          206                                                                            
Total length (>= 0 bp)         2008297                                                                        
Total length (>= 1000 bp)      259471                                                                         
#Contigs                       4013                                                                           
Largest contig                 2896                                                                           
Total length                   2008297                                                                        
Reference length               4411532                                                                        
GC (%)                         66.15                                                                          
Reference GC (%)               65.61                                                                          
N50                            521                                                                            
NG50                           None                                                                           
N75                            372                                                                            
NG75                           None                                                                           
#misassemblies                 93                                                                             
#local misassemblies           3                                                                              
#unaligned contigs             0 + 0 part                                                                     
Unaligned contigs length       0                                                                              
Genome fraction (%)            41.141                                                                         
Duplication ratio              1.104                                                                          
#N's per 100 kbp               0.50                                                                           
#mismatches per 100 kbp        4.41                                                                           
#indels per 100 kbp            84.63                                                                          
#genes                         229 + 2222 part                                                                
#predicted genes (unique)      4858                                                                           
#predicted genes (>= 0 bp)     4891                                                                           
#predicted genes (>= 300 bp)   2412                                                                           
#predicted genes (>= 1500 bp)  5                                                                              
#predicted genes (>= 3000 bp)  0                                                                              
Largest alignment              2896                                                                           
NA50                           516                                                                            
NGA50                          None                                                                           
NA75                           367                                                                            
NGA75                          None                                                                           
