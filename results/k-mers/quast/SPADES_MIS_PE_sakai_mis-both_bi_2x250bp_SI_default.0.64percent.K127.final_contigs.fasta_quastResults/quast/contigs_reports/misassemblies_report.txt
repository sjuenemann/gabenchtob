All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K127.final_contigs_broken  SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K127.final_contigs
#Mis_misassemblies               6                                                                                         6                                                                                
#Mis_relocations                 6                                                                                         6                                                                                
#Mis_translocations              0                                                                                         0                                                                                
#Mis_inversions                  0                                                                                         0                                                                                
#Mis_misassembled contigs        6                                                                                         6                                                                                
Mis_Misassembled contigs length  576441                                                                                    576441                                                                           
#Mis_local misassemblies         1                                                                                         1                                                                                
#mismatches                      503                                                                                       525                                                                              
#indels                          19                                                                                        19                                                                               
#Mis_short indels (<= 5 bp)      19                                                                                        19                                                                               
#Mis_long indels (> 5 bp)        0                                                                                         0                                                                                
Indels length                    22                                                                                        22                                                                               
