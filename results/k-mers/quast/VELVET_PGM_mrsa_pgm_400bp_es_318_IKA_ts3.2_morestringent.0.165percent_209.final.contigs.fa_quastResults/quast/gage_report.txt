All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_209.final.contigs
GAGE_Contigs #                   1090                                                                                   
GAGE_Min contig                  417                                                                                    
GAGE_Max contig                  12663                                                                                  
GAGE_N50                         2106 COUNT: 375                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2282506                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     566082(20.12%)                                                                         
GAGE_Missing assembly bases      242(0.01%)                                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  1670                                                                                   
GAGE_Compressed reference bases  15908                                                                                  
GAGE_Bad trim                    212                                                                                    
GAGE_Avg idy                     99.95                                                                                  
GAGE_SNPs                        58                                                                                     
GAGE_Indels < 5bp                731                                                                                    
GAGE_Indels >= 5                 0                                                                                      
GAGE_Inversions                  1                                                                                      
GAGE_Relocation                  4                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          1095                                                                                   
GAGE_Corrected assembly size     2284475                                                                                
GAGE_Min correct contig          391                                                                                    
GAGE_Max correct contig          12662                                                                                  
GAGE_Corrected N50               2101 COUNT: 378                                                                        
