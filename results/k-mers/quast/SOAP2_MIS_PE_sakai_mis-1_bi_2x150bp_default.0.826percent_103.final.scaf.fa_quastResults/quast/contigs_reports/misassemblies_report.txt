All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_103.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_103.final.scaf
#Mis_misassemblies               4                                                                               27                                                                     
#Mis_relocations                 4                                                                               27                                                                     
#Mis_translocations              0                                                                               0                                                                      
#Mis_inversions                  0                                                                               0                                                                      
#Mis_misassembled contigs        4                                                                               21                                                                     
Mis_Misassembled contigs length  90394                                                                           498370                                                                 
#Mis_local misassemblies         3                                                                               98                                                                     
#mismatches                      66                                                                              178                                                                    
#indels                          43                                                                              1534                                                                   
#Mis_short indels (<= 5 bp)      24                                                                              1412                                                                   
#Mis_long indels (> 5 bp)        19                                                                              122                                                                    
Indels length                    413                                                                             4161                                                                   
