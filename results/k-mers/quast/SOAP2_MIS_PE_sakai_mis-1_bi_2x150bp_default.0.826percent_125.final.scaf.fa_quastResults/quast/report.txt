All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_125.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_125.final.scaf
#Contigs (>= 0 bp)             3214                                                                            1151                                                                   
#Contigs (>= 1000 bp)          1520                                                                            350                                                                    
Total length (>= 0 bp)         5426172                                                                         5501369                                                                
Total length (>= 1000 bp)      4721363                                                                         5287878                                                                
#Contigs                       2854                                                                            826                                                                    
Largest contig                 19186                                                                           132319                                                                 
Total length                   5370218                                                                         5451048                                                                
Reference length               5594470                                                                         5594470                                                                
GC (%)                         50.43                                                                           50.43                                                                  
Reference GC (%)               50.48                                                                           50.48                                                                  
N50                            3368                                                                            33298                                                                  
NG50                           3229                                                                            32443                                                                  
N75                            1809                                                                            13817                                                                  
NG75                           1599                                                                            12953                                                                  
#misassemblies                 2                                                                               17                                                                     
#local misassemblies           8                                                                               287                                                                    
#unaligned contigs             5 + 1 part                                                                      14 + 10 part                                                           
Unaligned contigs length       6976                                                                            28522                                                                  
Genome fraction (%)            93.110                                                                          93.379                                                                 
Duplication ratio              1.002                                                                           1.013                                                                  
#N's per 100 kbp               14.15                                                                           1393.44                                                                
#mismatches per 100 kbp        2.27                                                                            4.23                                                                   
#indels per 100 kbp            4.09                                                                            329.92                                                                 
#genes                         3423 + 1752 part                                                                4568 + 588 part                                                        
#predicted genes (unique)      7161                                                                            6403                                                                   
#predicted genes (>= 0 bp)     7189                                                                            6425                                                                   
#predicted genes (>= 300 bp)   5119                                                                            4946                                                                   
#predicted genes (>= 1500 bp)  439                                                                             493                                                                    
#predicted genes (>= 3000 bp)  32                                                                              41                                                                     
Largest alignment              19186                                                                           131906                                                                 
NA50                           3342                                                                            33248                                                                  
NGA50                          3210                                                                            32068                                                                  
NA75                           1791                                                                            13684                                                                  
NGA75                          1555                                                                            12554                                                                  
