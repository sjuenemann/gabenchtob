All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_151.final.contigs
#Contigs                                   1053                                                                                        
#Contigs (>= 0 bp)                         1053                                                                                        
#Contigs (>= 1000 bp)                      28                                                                                          
Largest contig                             1521                                                                                        
Total length                               609752                                                                                      
Total length (>= 0 bp)                     609752                                                                                      
Total length (>= 1000 bp)                  32176                                                                                       
Reference length                           2813862                                                                                     
N50                                        557                                                                                         
NG50                                       None                                                                                        
N75                                        495                                                                                         
NG75                                       None                                                                                        
L50                                        438                                                                                         
LG50                                       None                                                                                        
L75                                        729                                                                                         
LG75                                       None                                                                                        
#local misassemblies                       0                                                                                           
#misassemblies                             4                                                                                           
#misassembled contigs                      4                                                                                           
Misassembled contigs length                1938                                                                                        
Misassemblies inter-contig overlap         254                                                                                         
#unaligned contigs                         0 + 38 part                                                                                 
Unaligned contigs length                   1096                                                                                        
#ambiguously mapped contigs                7                                                                                           
Extra bases in ambiguously mapped contigs  -5071                                                                                       
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        21.040                                                                                      
Duplication ratio                          1.020                                                                                       
#genes                                     83 + 930 part                                                                               
#predicted genes (unique)                  1320                                                                                        
#predicted genes (>= 0 bp)                 1320                                                                                        
#predicted genes (>= 300 bp)               927                                                                                         
#predicted genes (>= 1500 bp)              0                                                                                           
#predicted genes (>= 3000 bp)              0                                                                                           
#mismatches                                69                                                                                          
#indels                                    447                                                                                         
Indels length                              466                                                                                         
#mismatches per 100 kbp                    11.65                                                                                       
#indels per 100 kbp                        75.50                                                                                       
GC (%)                                     34.40                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               99.822                                                                                      
Largest alignment                          1521                                                                                        
NA50                                       550                                                                                         
NGA50                                      None                                                                                        
NA75                                       491                                                                                         
NGA75                                      None                                                                                        
LA50                                       441                                                                                         
LGA50                                      None                                                                                        
LA75                                       734                                                                                         
LGA75                                      None                                                                                        
#Mis_misassemblies                         4                                                                                           
#Mis_relocations                           3                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            1                                                                                           
#Mis_misassembled contigs                  4                                                                                           
Mis_Misassembled contigs length            1938                                                                                        
#Mis_local misassemblies                   0                                                                                           
#Mis_short indels (<= 5 bp)                446                                                                                         
#Mis_long indels (> 5 bp)                  1                                                                                           
#Una_fully unaligned contigs               0                                                                                           
Una_Fully unaligned length                 0                                                                                           
#Una_partially unaligned contigs           38                                                                                          
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             1096                                                                                        
GAGE_Contigs #                             1053                                                                                        
GAGE_Min contig                            301                                                                                         
GAGE_Max contig                            1521                                                                                        
GAGE_N50                                   0 COUNT: 0                                                                                  
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         609752                                                                                      
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               2190005(77.83%)                                                                             
GAGE_Missing assembly bases                1382(0.23%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            0                                                                                           
GAGE_Compressed reference bases            30553                                                                                       
GAGE_Bad trim                              1382                                                                                        
GAGE_Avg idy                               99.91                                                                                       
GAGE_SNPs                                  60                                                                                          
GAGE_Indels < 5bp                          429                                                                                         
GAGE_Indels >= 5                           0                                                                                           
GAGE_Inversions                            2                                                                                           
GAGE_Relocation                            2                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    1054                                                                                        
GAGE_Corrected assembly size               608478                                                                                      
GAGE_Min correct contig                    300                                                                                         
GAGE_Max correct contig                    1521                                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                                  
