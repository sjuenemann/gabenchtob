All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_93.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_93.final.scaf
#Mis_misassemblies               6                                                                                24                                                                      
#Mis_relocations                 6                                                                                24                                                                      
#Mis_translocations              0                                                                                0                                                                       
#Mis_inversions                  0                                                                                0                                                                       
#Mis_misassembled contigs        6                                                                                21                                                                      
Mis_Misassembled contigs length  17418                                                                            193706                                                                  
#Mis_local misassemblies         40                                                                               779                                                                     
#mismatches                      135                                                                              183                                                                     
#indels                          78                                                                               1983                                                                    
#Mis_short indels (<= 5 bp)      67                                                                               1870                                                                    
#Mis_long indels (> 5 bp)        11                                                                               113                                                                     
Indels length                    406                                                                              5338                                                                    
