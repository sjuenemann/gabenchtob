All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_80-unitigs
#Contigs                                   1163                                                                   
#Contigs (>= 0 bp)                         5249                                                                   
#Contigs (>= 1000 bp)                      652                                                                    
Largest contig                             59205                                                                  
Total length                               5302034                                                                
Total length (>= 0 bp)                     5836706                                                                
Total length (>= 1000 bp)                  5089274                                                                
Reference length                           5594470                                                                
N50                                        13046                                                                  
NG50                                       12116                                                                  
N75                                        6284                                                                   
NG75                                       5265                                                                   
L50                                        124                                                                    
LG50                                       136                                                                    
L75                                        268                                                                    
LG75                                       306                                                                    
#local misassemblies                       9                                                                      
#misassemblies                             2                                                                      
#misassembled contigs                      2                                                                      
Misassembled contigs length                12048                                                                  
Misassemblies inter-contig overlap         1101                                                                   
#unaligned contigs                         0 + 0 part                                                             
Unaligned contigs length                   0                                                                      
#ambiguously mapped contigs                152                                                                    
Extra bases in ambiguously mapped contigs  -69043                                                                 
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        93.364                                                                 
Duplication ratio                          1.002                                                                  
#genes                                     4424 + 661 part                                                        
#predicted genes (unique)                  5874                                                                   
#predicted genes (>= 0 bp)                 5881                                                                   
#predicted genes (>= 300 bp)               4591                                                                   
#predicted genes (>= 1500 bp)              585                                                                    
#predicted genes (>= 3000 bp)              54                                                                     
#mismatches                                75                                                                     
#indels                                    256                                                                    
Indels length                              260                                                                    
#mismatches per 100 kbp                    1.44                                                                   
#indels per 100 kbp                        4.90                                                                   
GC (%)                                     50.26                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.272                                                                 
Largest alignment                          59205                                                                  
NA50                                       13046                                                                  
NGA50                                      12116                                                                  
NA75                                       6283                                                                   
NGA75                                      5260                                                                   
LA50                                       124                                                                    
LGA50                                      136                                                                    
LA75                                       269                                                                    
LGA75                                      307                                                                    
#Mis_misassemblies                         2                                                                      
#Mis_relocations                           2                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  2                                                                      
Mis_Misassembled contigs length            12048                                                                  
#Mis_local misassemblies                   9                                                                      
#Mis_short indels (<= 5 bp)                256                                                                    
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           0                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             0                                                                      
GAGE_Contigs #                             1163                                                                   
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            59205                                                                  
GAGE_N50                                   12116 COUNT: 136                                                       
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5302034                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               151214(2.70%)                                                          
GAGE_Missing assembly bases                0(0.00%)                                                               
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            5713                                                                   
GAGE_Compressed reference bases            189727                                                                 
GAGE_Bad trim                              0                                                                      
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  71                                                                     
GAGE_Indels < 5bp                          258                                                                    
GAGE_Indels >= 5                           7                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            5                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    1150                                                                   
GAGE_Corrected assembly size               5297400                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    59210                                                                  
GAGE_Corrected N50                         12024 COUNT: 137                                                       
