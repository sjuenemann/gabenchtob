All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_107.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_107.final.scaf
#Contigs                                   980                                                                             651                                                                    
#Contigs (>= 0 bp)                         1640                                                                            1262                                                                   
#Contigs (>= 1000 bp)                      280                                                                             139                                                                    
Largest contig                             142817                                                                          374600                                                                 
Total length                               5445278                                                                         5469820                                                                
Total length (>= 0 bp)                     5539878                                                                         5556825                                                                
Total length (>= 1000 bp)                  5230438                                                                         5321537                                                                
Reference length                           5594470                                                                         5594470                                                                
N50                                        38386                                                                           124399                                                                 
NG50                                       36844                                                                           124399                                                                 
N75                                        18772                                                                           67370                                                                  
NG75                                       17480                                                                           50676                                                                  
L50                                        42                                                                              14                                                                     
LG50                                       44                                                                              14                                                                     
L75                                        90                                                                              28                                                                     
LG75                                       96                                                                              30                                                                     
#local misassemblies                       3                                                                               98                                                                     
#misassemblies                             6                                                                               26                                                                     
#misassembled contigs                      6                                                                               22                                                                     
Misassembled contigs length                107696                                                                          540250                                                                 
Misassemblies inter-contig overlap         8                                                                               300                                                                    
#unaligned contigs                         3 + 0 part                                                                      9 + 7 part                                                             
Unaligned contigs length                   5815                                                                            16900                                                                  
#ambiguously mapped contigs                444                                                                             407                                                                    
Extra bases in ambiguously mapped contigs  -146912                                                                         -128856                                                                
#N's                                       148                                                                             17095                                                                  
#N's per 100 kbp                           2.72                                                                            312.53                                                                 
Genome fraction (%)                        94.512                                                                          94.599                                                                 
Duplication ratio                          1.001                                                                           1.006                                                                  
#genes                                     4829 + 351 part                                                                 4957 + 213 part                                                        
#predicted genes (unique)                  5843                                                                            5734                                                                   
#predicted genes (>= 0 bp)                 5867                                                                            5756                                                                   
#predicted genes (>= 300 bp)               4491                                                                            4497                                                                   
#predicted genes (>= 1500 bp)              646                                                                             658                                                                    
#predicted genes (>= 3000 bp)              63                                                                              63                                                                     
#mismatches                                65                                                                              200                                                                    
#indels                                    48                                                                              2303                                                                   
Indels length                              567                                                                             5055                                                                   
#mismatches per 100 kbp                    1.23                                                                            3.78                                                                   
#indels per 100 kbp                        0.91                                                                            43.52                                                                  
GC (%)                                     50.33                                                                           50.34                                                                  
Reference GC (%)                           50.48                                                                           50.48                                                                  
Average %IDY                               99.040                                                                          98.999                                                                 
Largest alignment                          142817                                                                          355989                                                                 
NA50                                       38386                                                                           124399                                                                 
NGA50                                      36844                                                                           124399                                                                 
NA75                                       18772                                                                           58798                                                                  
NGA75                                      17477                                                                           50543                                                                  
LA50                                       42                                                                              15                                                                     
LGA50                                      44                                                                              15                                                                     
LA75                                       90                                                                              30                                                                     
LGA75                                      96                                                                              31                                                                     
#Mis_misassemblies                         6                                                                               26                                                                     
#Mis_relocations                           6                                                                               26                                                                     
#Mis_translocations                        0                                                                               0                                                                      
#Mis_inversions                            0                                                                               0                                                                      
#Mis_misassembled contigs                  6                                                                               22                                                                     
Mis_Misassembled contigs length            107696                                                                          540250                                                                 
#Mis_local misassemblies                   3                                                                               98                                                                     
#Mis_short indels (<= 5 bp)                22                                                                              2172                                                                   
#Mis_long indels (> 5 bp)                  26                                                                              131                                                                    
#Una_fully unaligned contigs               3                                                                               9                                                                      
Una_Fully unaligned length                 5815                                                                            11368                                                                  
#Una_partially unaligned contigs           0                                                                               7                                                                      
#Una_with misassembly                      0                                                                               0                                                                      
#Una_both parts are significant            0                                                                               5                                                                      
Una_Partially unaligned length             0                                                                               5532                                                                   
GAGE_Contigs #                             980                                                                             651                                                                    
GAGE_Min contig                            200                                                                             200                                                                    
GAGE_Max contig                            142817                                                                          374600                                                                 
GAGE_N50                                   36844 COUNT: 44                                                                 124399 COUNT: 14                                                       
GAGE_Genome size                           5594470                                                                         5594470                                                                
GAGE_Assembly size                         5445278                                                                         5469820                                                                
GAGE_Chaff bases                           0                                                                               0                                                                      
GAGE_Missing reference bases               10014(0.18%)                                                                    7279(0.13%)                                                            
GAGE_Missing assembly bases                5976(0.11%)                                                                     24001(0.44%)                                                           
GAGE_Missing assembly contigs              3(0.31%)                                                                        3(0.46%)                                                               
GAGE_Duplicated reference bases            18736                                                                           17351                                                                  
GAGE_Compressed reference bases            280601                                                                          273417                                                                 
GAGE_Bad trim                              50                                                                              2440                                                                   
GAGE_Avg idy                               100.00                                                                          99.98                                                                  
GAGE_SNPs                                  63                                                                              65                                                                     
GAGE_Indels < 5bp                          41                                                                              125                                                                    
GAGE_Indels >= 5                           26                                                                              204                                                                    
GAGE_Inversions                            1                                                                               2                                                                      
GAGE_Relocation                            3                                                                               15                                                                     
GAGE_Translocation                         0                                                                               0                                                                      
GAGE_Corrected contig #                    931                                                                             909                                                                    
GAGE_Corrected assembly size               5419902                                                                         5422028                                                                
GAGE_Min correct contig                    200                                                                             200                                                                    
GAGE_Max correct contig                    142816                                                                          142816                                                                 
GAGE_Corrected N50                         33539 COUNT: 49                                                                 36130 COUNT: 47                                                        
