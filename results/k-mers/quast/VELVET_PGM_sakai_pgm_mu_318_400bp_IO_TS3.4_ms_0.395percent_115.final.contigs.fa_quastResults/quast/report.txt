All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_115.final.contigs
#Contigs (>= 0 bp)             27994                                                                       
#Contigs (>= 1000 bp)          0                                                                           
Total length (>= 0 bp)         7301316                                                                     
Total length (>= 1000 bp)      0                                                                           
#Contigs                       27994                                                                       
Largest contig                 810                                                                         
Total length                   7301316                                                                     
Reference length               5594470                                                                     
GC (%)                         50.24                                                                       
Reference GC (%)               50.48                                                                       
N50                            252                                                                         
NG50                           266                                                                         
N75                            232                                                                         
NG75                           245                                                                         
#misassemblies                 854                                                                         
#local misassemblies           3                                                                           
#unaligned contigs             35 + 44 part                                                                
Unaligned contigs length       14038                                                                       
Genome fraction (%)            68.550                                                                      
Duplication ratio              1.831                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        78.02                                                                       
#indels per 100 kbp            838.98                                                                      
#genes                         161 + 4693 part                                                             
#predicted genes (unique)      27708                                                                       
#predicted genes (>= 0 bp)     27783                                                                       
#predicted genes (>= 300 bp)   394                                                                         
#predicted genes (>= 1500 bp)  0                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              810                                                                         
NA50                           248                                                                         
NGA50                          262                                                                         
NA75                           229                                                                         
NGA75                          241                                                                         
