All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_115.final.contigs
#Mis_misassemblies               854                                                                         
#Mis_relocations                 792                                                                         
#Mis_translocations              61                                                                          
#Mis_inversions                  1                                                                           
#Mis_misassembled contigs        854                                                                         
Mis_Misassembled contigs length  236187                                                                      
#Mis_local misassemblies         3                                                                           
#mismatches                      2992                                                                        
#indels                          32175                                                                       
#Mis_short indels (<= 5 bp)      32173                                                                       
#Mis_long indels (> 5 bp)        2                                                                           
Indels length                    32732                                                                       
