All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_59.final.contig
#Contigs (>= 0 bp)             240565                                                                              
#Contigs (>= 1000 bp)          0                                                                                   
Total length (>= 0 bp)         24510288                                                                            
Total length (>= 1000 bp)      0                                                                                   
#Contigs                       16127                                                                               
Largest contig                 659                                                                                 
Total length                   3750036                                                                             
Reference length               2813862                                                                             
GC (%)                         31.29                                                                               
Reference GC (%)               32.81                                                                               
N50                            226                                                                                 
NG50                           237                                                                                 
N75                            211                                                                                 
NG75                           222                                                                                 
#misassemblies                 0                                                                                   
#local misassemblies           1                                                                                   
#unaligned contigs             15597 + 4 part                                                                      
Unaligned contigs length       3622095                                                                             
Genome fraction (%)            4.401                                                                               
Duplication ratio              1.030                                                                               
#N's per 100 kbp               0.00                                                                                
#mismatches per 100 kbp        42.80                                                                               
#indels per 100 kbp            524.05                                                                              
#genes                         3 + 404 part                                                                        
#predicted genes (unique)      9650                                                                                
#predicted genes (>= 0 bp)     9659                                                                                
#predicted genes (>= 300 bp)   31                                                                                  
#predicted genes (>= 1500 bp)  0                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                   
Largest alignment              476                                                                                 
NA50                           None                                                                                
NGA50                          None                                                                                
NA75                           None                                                                                
NGA75                          None                                                                                
