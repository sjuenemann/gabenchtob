All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_102-unitigs
#Mis_misassemblies               3                                                                          
#Mis_relocations                 3                                                                          
#Mis_translocations              0                                                                          
#Mis_inversions                  0                                                                          
#Mis_misassembled contigs        3                                                                          
Mis_Misassembled contigs length  874                                                                        
#Mis_local misassemblies         13                                                                         
#mismatches                      112                                                                        
#indels                          1655                                                                       
#Mis_short indels (<= 5 bp)      1653                                                                       
#Mis_long indels (> 5 bp)        2                                                                          
Indels length                    1775                                                                       
