All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_215.final.contigs
GAGE_Contigs #                   1164                                                                                   
GAGE_Min contig                  429                                                                                    
GAGE_Max contig                  12778                                                                                  
GAGE_N50                         1906 COUNT: 411                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2266178                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     580919(20.64%)                                                                         
GAGE_Missing assembly bases      270(0.01%)                                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  3540                                                                                   
GAGE_Compressed reference bases  29219                                                                                  
GAGE_Bad trim                    270                                                                                    
GAGE_Avg idy                     99.95                                                                                  
GAGE_SNPs                        70                                                                                     
GAGE_Indels < 5bp                734                                                                                    
GAGE_Indels >= 5                 0                                                                                      
GAGE_Inversions                  0                                                                                      
GAGE_Relocation                  1                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          1163                                                                                   
GAGE_Corrected assembly size     2265462                                                                                
GAGE_Min correct contig          429                                                                                    
GAGE_Max correct contig          12778                                                                                  
GAGE_Corrected N50               1907 COUNT: 412                                                                        
