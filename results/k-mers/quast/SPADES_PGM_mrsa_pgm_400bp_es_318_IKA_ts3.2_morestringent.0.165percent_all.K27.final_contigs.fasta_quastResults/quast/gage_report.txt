All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K27.final_contigs
GAGE_Contigs #                   322                                                                                        
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  74598                                                                                      
GAGE_N50                         28872 COUNT: 33                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2740193                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     50962(1.81%)                                                                               
GAGE_Missing assembly bases      10597(0.39%)                                                                               
GAGE_Missing assembly contigs    37(11.49%)                                                                                 
GAGE_Duplicated reference bases  0                                                                                          
GAGE_Compressed reference bases  34873                                                                                      
GAGE_Bad trim                    56                                                                                         
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        112                                                                                        
GAGE_Indels < 5bp                173                                                                                        
GAGE_Indels >= 5                 6                                                                                          
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  2                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          294                                                                                        
GAGE_Corrected assembly size     2731109                                                                                    
GAGE_Min correct contig          200                                                                                        
GAGE_Max correct contig          74603                                                                                      
GAGE_Corrected N50               27923 COUNT: 34                                                                            
