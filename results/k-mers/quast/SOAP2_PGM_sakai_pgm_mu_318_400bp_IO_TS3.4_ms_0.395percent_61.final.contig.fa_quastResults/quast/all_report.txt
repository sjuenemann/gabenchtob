All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_61.final.contig
#Contigs                                   7146                                                                     
#Contigs (>= 0 bp)                         278348                                                                   
#Contigs (>= 1000 bp)                      0                                                                        
Largest contig                             757                                                                      
Total length                               1753198                                                                  
Total length (>= 0 bp)                     28252171                                                                 
Total length (>= 1000 bp)                  0                                                                        
Reference length                           5594470                                                                  
N50                                        233                                                                      
NG50                                       None                                                                     
N75                                        213                                                                      
NG75                                       None                                                                     
L50                                        3040                                                                     
LG50                                       None                                                                     
L75                                        5017                                                                     
LG75                                       None                                                                     
#local misassemblies                       2                                                                        
#misassemblies                             0                                                                        
#misassembled contigs                      0                                                                        
Misassembled contigs length                0                                                                        
Misassemblies inter-contig overlap         319                                                                      
#unaligned contigs                         3541 + 3 part                                                            
Unaligned contigs length                   793719                                                                   
#ambiguously mapped contigs                10                                                                       
Extra bases in ambiguously mapped contigs  -2194                                                                    
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        17.063                                                                   
Duplication ratio                          1.003                                                                    
#genes                                     34 + 2348 part                                                           
#predicted genes (unique)                  5891                                                                     
#predicted genes (>= 0 bp)                 5893                                                                     
#predicted genes (>= 300 bp)               539                                                                      
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                23                                                                       
#indels                                    231                                                                      
Indels length                              243                                                                      
#mismatches per 100 kbp                    2.41                                                                     
#indels per 100 kbp                        24.20                                                                    
GC (%)                                     49.50                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.970                                                                   
Largest alignment                          757                                                                      
NA50                                       207                                                                      
NGA50                                      None                                                                     
NA75                                       None                                                                     
NGA75                                      None                                                                     
LA50                                       3198                                                                     
LGA50                                      None                                                                     
LA75                                       None                                                                     
LGA75                                      None                                                                     
#Mis_misassemblies                         0                                                                        
#Mis_relocations                           0                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  0                                                                        
Mis_Misassembled contigs length            0                                                                        
#Mis_local misassemblies                   2                                                                        
#Mis_short indels (<= 5 bp)                231                                                                      
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               3541                                                                     
Una_Fully unaligned length                 793501                                                                   
#Una_partially unaligned contigs           3                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             218                                                                      
GAGE_Contigs #                             7146                                                                     
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            757                                                                      
GAGE_N50                                   0 COUNT: 0                                                               
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         1753198                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               4017314(71.81%)                                                          
GAGE_Missing assembly bases                104383(5.95%)                                                            
GAGE_Missing assembly contigs              380(5.32%)                                                               
GAGE_Duplicated reference bases            48384                                                                    
GAGE_Compressed reference bases            57699                                                                    
GAGE_Bad trim                              15777                                                                    
GAGE_Avg idy                               98.92                                                                    
GAGE_SNPs                                  542                                                                      
GAGE_Indels < 5bp                          14485                                                                    
GAGE_Indels >= 5                           4                                                                        
GAGE_Inversions                            13                                                                       
GAGE_Relocation                            7                                                                        
GAGE_Translocation                         3                                                                        
GAGE_Corrected contig #                    6079                                                                     
GAGE_Corrected assembly size               1509846                                                                  
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    757                                                                      
GAGE_Corrected N50                         0 COUNT: 0                                                               
