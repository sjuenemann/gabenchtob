All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K123.final_contigs
#Mis_misassemblies               139                                                                                         
#Mis_relocations                 131                                                                                         
#Mis_translocations              8                                                                                           
#Mis_inversions                  0                                                                                           
#Mis_misassembled contigs        139                                                                                         
Mis_Misassembled contigs length  295847                                                                                      
#Mis_local misassemblies         8                                                                                           
#mismatches                      215                                                                                         
#indels                          744                                                                                         
#Mis_short indels (<= 5 bp)      742                                                                                         
#Mis_long indels (> 5 bp)        2                                                                                           
Indels length                    785                                                                                         
