All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_104-unitigs
GAGE_Contigs #                   1506                                                                                    
GAGE_Min contig                  200                                                                                     
GAGE_Max contig                  24484                                                                                   
GAGE_N50                         4300 COUNT: 197                                                                         
GAGE_Genome size                 2813862                                                                                 
GAGE_Assembly size               2851560                                                                                 
GAGE_Chaff bases                 0                                                                                       
GAGE_Missing reference bases     34138(1.21%)                                                                            
GAGE_Missing assembly bases      221(0.01%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                                
GAGE_Duplicated reference bases  72505                                                                                   
GAGE_Compressed reference bases  42865                                                                                   
GAGE_Bad trim                    221                                                                                     
GAGE_Avg idy                     99.97                                                                                   
GAGE_SNPs                        52                                                                                      
GAGE_Indels < 5bp                678                                                                                     
GAGE_Indels >= 5                 0                                                                                       
GAGE_Inversions                  1                                                                                       
GAGE_Relocation                  1                                                                                       
GAGE_Translocation               0                                                                                       
GAGE_Corrected contig #          1171                                                                                    
GAGE_Corrected assembly size     2779095                                                                                 
GAGE_Min correct contig          200                                                                                     
GAGE_Max correct contig          24484                                                                                   
GAGE_Corrected N50               4301 COUNT: 197                                                                         
