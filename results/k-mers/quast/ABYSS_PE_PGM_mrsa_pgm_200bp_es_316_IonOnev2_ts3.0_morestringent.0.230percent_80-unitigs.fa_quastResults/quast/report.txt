All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_80-unitigs
#Contigs (>= 0 bp)             2485                                                                                   
#Contigs (>= 1000 bp)          412                                                                                    
Total length (>= 0 bp)         2985999                                                                                
Total length (>= 1000 bp)      2688212                                                                                
#Contigs                       565                                                                                    
Largest contig                 60712                                                                                  
Total length                   2762110                                                                                
Reference length               2813862                                                                                
GC (%)                         32.61                                                                                  
Reference GC (%)               32.81                                                                                  
N50                            9635                                                                                   
NG50                           9582                                                                                   
N75                            5010                                                                                   
NG75                           4825                                                                                   
#misassemblies                 13                                                                                     
#local misassemblies           3                                                                                      
#unaligned contigs             0 + 0 part                                                                             
Unaligned contigs length       0                                                                                      
Genome fraction (%)            97.701                                                                                 
Duplication ratio              1.004                                                                                  
#N's per 100 kbp               0.00                                                                                   
#mismatches per 100 kbp        2.22                                                                                   
#indels per 100 kbp            16.30                                                                                  
#genes                         2393 + 284 part                                                                        
#predicted genes (unique)      2978                                                                                   
#predicted genes (>= 0 bp)     2982                                                                                   
#predicted genes (>= 300 bp)   2406                                                                                   
#predicted genes (>= 1500 bp)  261                                                                                    
#predicted genes (>= 3000 bp)  21                                                                                     
Largest alignment              60712                                                                                  
NA50                           9635                                                                                   
NGA50                          9582                                                                                   
NA75                           5010                                                                                   
NGA75                          4825                                                                                   
