All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_96-unitigs
#Contigs (>= 0 bp)             4641                                                                   
#Contigs (>= 1000 bp)          656                                                                    
Total length (>= 0 bp)         5862173                                                                
Total length (>= 1000 bp)      5098311                                                                
#Contigs                       1210                                                                   
Largest contig                 51165                                                                  
Total length                   5328962                                                                
Reference length               5594470                                                                
GC (%)                         50.29                                                                  
Reference GC (%)               50.48                                                                  
N50                            13303                                                                  
NG50                           12059                                                                  
N75                            6280                                                                   
NG75                           5131                                                                   
#misassemblies                 2                                                                      
#local misassemblies           6                                                                      
#unaligned contigs             0 + 0 part                                                             
Unaligned contigs length       0                                                                      
Genome fraction (%)            93.641                                                                 
Duplication ratio              1.003                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        1.39                                                                   
#indels per 100 kbp            5.02                                                                   
#genes                         4445 + 673 part                                                        
#predicted genes (unique)      5956                                                                   
#predicted genes (>= 0 bp)     5967                                                                   
#predicted genes (>= 300 bp)   4631                                                                   
#predicted genes (>= 1500 bp)  589                                                                    
#predicted genes (>= 3000 bp)  51                                                                     
Largest alignment              51165                                                                  
NA50                           13303                                                                  
NGA50                          12059                                                                  
NA75                           6172                                                                   
NGA75                          5131                                                                   
