All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_96-unitigs
GAGE_Contigs #                   1210                                                                   
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  51165                                                                  
GAGE_N50                         12059 COUNT: 130                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5328962                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     119376(2.13%)                                                          
GAGE_Missing assembly bases      4(0.00%)                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  10759                                                                  
GAGE_Compressed reference bases  216412                                                                 
GAGE_Bad trim                    4                                                                      
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        64                                                                     
GAGE_Indels < 5bp                273                                                                    
GAGE_Indels >= 5                 4                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  5                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          1177                                                                   
GAGE_Corrected assembly size     5319068                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          51167                                                                  
GAGE_Corrected N50               12060 COUNT: 130                                                       
