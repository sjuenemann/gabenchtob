All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K87.final_contigs
#Contigs                                   3040                                                                                            
#Contigs (>= 0 bp)                         3214                                                                                            
#Contigs (>= 1000 bp)                      63                                                                                              
Largest contig                             239946                                                                                          
Total length                               3517038                                                                                         
Total length (>= 0 bp)                     3538736                                                                                         
Total length (>= 1000 bp)                  2757440                                                                                         
Reference length                           2813862                                                                                         
N50                                        69793                                                                                           
NG50                                       89440                                                                                           
N75                                        17372                                                                                           
NG75                                       52104                                                                                           
L50                                        15                                                                                              
LG50                                       10                                                                                              
L75                                        36                                                                                              
LG75                                       20                                                                                              
#local misassemblies                       13                                                                                              
#misassemblies                             107                                                                                             
#misassembled contigs                      107                                                                                             
Misassembled contigs length                465012                                                                                          
Misassemblies inter-contig overlap         3979                                                                                            
#unaligned contigs                         951 + 589 part                                                                                  
Unaligned contigs length                   282902                                                                                          
#ambiguously mapped contigs                25                                                                                              
Extra bases in ambiguously mapped contigs  -13649                                                                                          
#N's                                       0                                                                                               
#N's per 100 kbp                           0.00                                                                                            
Genome fraction (%)                        98.222                                                                                          
Duplication ratio                          1.167                                                                                           
#genes                                     2637 + 53 part                                                                                  
#predicted genes (unique)                  4687                                                                                            
#predicted genes (>= 0 bp)                 4689                                                                                            
#predicted genes (>= 300 bp)               2343                                                                                            
#predicted genes (>= 1500 bp)              277                                                                                             
#predicted genes (>= 3000 bp)              22                                                                                              
#mismatches                                228                                                                                             
#indels                                    752                                                                                             
Indels length                              914                                                                                             
#mismatches per 100 kbp                    8.25                                                                                            
#indels per 100 kbp                        27.21                                                                                           
GC (%)                                     32.51                                                                                           
Reference GC (%)                           32.81                                                                                           
Average %IDY                               97.574                                                                                          
Largest alignment                          196729                                                                                          
NA50                                       68092                                                                                           
NGA50                                      88622                                                                                           
NA75                                       16617                                                                                           
NGA75                                      45882                                                                                           
LA50                                       16                                                                                              
LGA50                                      11                                                                                              
LA75                                       39                                                                                              
LGA75                                      22                                                                                              
#Mis_misassemblies                         107                                                                                             
#Mis_relocations                           32                                                                                              
#Mis_translocations                        0                                                                                               
#Mis_inversions                            75                                                                                              
#Mis_misassembled contigs                  107                                                                                             
Mis_Misassembled contigs length            465012                                                                                          
#Mis_local misassemblies                   13                                                                                              
#Mis_short indels (<= 5 bp)                750                                                                                             
#Mis_long indels (> 5 bp)                  2                                                                                               
#Una_fully unaligned contigs               951                                                                                             
Una_Fully unaligned length                 240641                                                                                          
#Una_partially unaligned contigs           589                                                                                             
#Una_with misassembly                      0                                                                                               
#Una_both parts are significant            0                                                                                               
Una_Partially unaligned length             42261                                                                                           
GAGE_Contigs #                             3040                                                                                            
GAGE_Min contig                            203                                                                                             
GAGE_Max contig                            239946                                                                                          
GAGE_N50                                   89440 COUNT: 10                                                                                 
GAGE_Genome size                           2813862                                                                                         
GAGE_Assembly size                         3517038                                                                                         
GAGE_Chaff bases                           0                                                                                               
GAGE_Missing reference bases               4070(0.14%)                                                                                     
GAGE_Missing assembly bases                150119(4.27%)                                                                                   
GAGE_Missing assembly contigs              374(12.30%)                                                                                     
GAGE_Duplicated reference bases            594013                                                                                          
GAGE_Compressed reference bases            43418                                                                                           
GAGE_Bad trim                              55624                                                                                           
GAGE_Avg idy                               99.97                                                                                           
GAGE_SNPs                                  83                                                                                              
GAGE_Indels < 5bp                          575                                                                                             
GAGE_Indels >= 5                           11                                                                                              
GAGE_Inversions                            4                                                                                               
GAGE_Relocation                            6                                                                                               
GAGE_Translocation                         0                                                                                               
GAGE_Corrected contig #                    126                                                                                             
GAGE_Corrected assembly size               2777669                                                                                         
GAGE_Min correct contig                    203                                                                                             
GAGE_Max correct contig                    156035                                                                                          
GAGE_Corrected N50                         60484 COUNT: 15                                                                                 
