All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_34-unitigs
#Contigs                                   352                                                                                    
#Contigs (>= 0 bp)                         4917                                                                                   
#Contigs (>= 1000 bp)                      237                                                                                    
Largest contig                             58300                                                                                  
Total length                               2726639                                                                                
Total length (>= 0 bp)                     2962410                                                                                
Total length (>= 1000 bp)                  2678918                                                                                
Reference length                           2813862                                                                                
N50                                        17759                                                                                  
NG50                                       17561                                                                                  
N75                                        9609                                                                                   
NG75                                       8844                                                                                   
L50                                        48                                                                                     
LG50                                       50                                                                                     
L75                                        100                                                                                    
LG75                                       107                                                                                    
#local misassemblies                       2                                                                                      
#misassemblies                             1                                                                                      
#misassembled contigs                      1                                                                                      
Misassembled contigs length                20200                                                                                  
Misassemblies inter-contig overlap         399                                                                                    
#unaligned contigs                         0 + 0 part                                                                             
Unaligned contigs length                   0                                                                                      
#ambiguously mapped contigs                14                                                                                     
Extra bases in ambiguously mapped contigs  -5953                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        96.605                                                                                 
Duplication ratio                          1.001                                                                                  
#genes                                     2479 + 142 part                                                                        
#predicted genes (unique)                  2788                                                                                   
#predicted genes (>= 0 bp)                 2788                                                                                   
#predicted genes (>= 300 bp)               2311                                                                                   
#predicted genes (>= 1500 bp)              279                                                                                    
#predicted genes (>= 3000 bp)              22                                                                                     
#mismatches                                58                                                                                     
#indels                                    256                                                                                    
Indels length                              268                                                                                    
#mismatches per 100 kbp                    2.13                                                                                   
#indels per 100 kbp                        9.42                                                                                   
GC (%)                                     32.59                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.908                                                                                 
Largest alignment                          58300                                                                                  
NA50                                       17620                                                                                  
NGA50                                      17560                                                                                  
NA75                                       9510                                                                                   
NGA75                                      8828                                                                                   
LA50                                       48                                                                                     
LGA50                                      50                                                                                     
LA75                                       101                                                                                    
LGA75                                      108                                                                                    
#Mis_misassemblies                         1                                                                                      
#Mis_relocations                           1                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  1                                                                                      
Mis_Misassembled contigs length            20200                                                                                  
#Mis_local misassemblies                   2                                                                                      
#Mis_short indels (<= 5 bp)                256                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             352                                                                                    
GAGE_Min contig                            203                                                                                    
GAGE_Max contig                            58300                                                                                  
GAGE_N50                                   17561 COUNT: 50                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2726639                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               76267(2.71%)                                                                           
GAGE_Missing assembly bases                11(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            0                                                                                      
GAGE_Compressed reference bases            13889                                                                                  
GAGE_Bad trim                              11                                                                                     
GAGE_Avg idy                               99.99                                                                                  
GAGE_SNPs                                  55                                                                                     
GAGE_Indels < 5bp                          265                                                                                    
GAGE_Indels >= 5                           2                                                                                      
GAGE_Inversions                            0                                                                                      
GAGE_Relocation                            1                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    355                                                                                    
GAGE_Corrected assembly size               2727294                                                                                
GAGE_Min correct contig                    203                                                                                    
GAGE_Max correct contig                    58307                                                                                  
GAGE_Corrected N50                         17561 COUNT: 50                                                                        
