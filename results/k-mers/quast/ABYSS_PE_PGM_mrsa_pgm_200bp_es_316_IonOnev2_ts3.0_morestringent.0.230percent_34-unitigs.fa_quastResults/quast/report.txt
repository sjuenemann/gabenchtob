All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_34-unitigs
#Contigs (>= 0 bp)             4917                                                                                   
#Contigs (>= 1000 bp)          237                                                                                    
Total length (>= 0 bp)         2962410                                                                                
Total length (>= 1000 bp)      2678918                                                                                
#Contigs                       352                                                                                    
Largest contig                 58300                                                                                  
Total length                   2726639                                                                                
Reference length               2813862                                                                                
GC (%)                         32.59                                                                                  
Reference GC (%)               32.81                                                                                  
N50                            17759                                                                                  
NG50                           17561                                                                                  
N75                            9609                                                                                   
NG75                           8844                                                                                   
#misassemblies                 1                                                                                      
#local misassemblies           2                                                                                      
#unaligned contigs             0 + 0 part                                                                             
Unaligned contigs length       0                                                                                      
Genome fraction (%)            96.605                                                                                 
Duplication ratio              1.001                                                                                  
#N's per 100 kbp               0.00                                                                                   
#mismatches per 100 kbp        2.13                                                                                   
#indels per 100 kbp            9.42                                                                                   
#genes                         2479 + 142 part                                                                        
#predicted genes (unique)      2788                                                                                   
#predicted genes (>= 0 bp)     2788                                                                                   
#predicted genes (>= 300 bp)   2311                                                                                   
#predicted genes (>= 1500 bp)  279                                                                                    
#predicted genes (>= 3000 bp)  22                                                                                     
Largest alignment              58300                                                                                  
NA50                           17620                                                                                  
NGA50                          17560                                                                                  
NA75                           9510                                                                                   
NGA75                          8828                                                                                   
