All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_117.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_117.final.scaf
#Mis_misassemblies               6                                                                               22                                                                     
#Mis_relocations                 6                                                                               22                                                                     
#Mis_translocations              0                                                                               0                                                                      
#Mis_inversions                  0                                                                               0                                                                      
#Mis_misassembled contigs        6                                                                               20                                                                     
Mis_Misassembled contigs length  80240                                                                           317802                                                                 
#Mis_local misassemblies         4                                                                               102                                                                    
#mismatches                      80                                                                              153                                                                    
#indels                          122                                                                             5271                                                                   
#Mis_short indels (<= 5 bp)      82                                                                              4986                                                                   
#Mis_long indels (> 5 bp)        40                                                                              285                                                                    
Indels length                    831                                                                             11542                                                                  
