reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_63.final.scaf_broken  | 93.8215520164       | 1.03290443328     | 748         | 1195      | 1438      | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_63.final.scaf  | 91.3045842333       | 1.0336157186      | 534         | 1327      | 1247      | None      | None      |
