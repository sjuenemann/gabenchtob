All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_121.final.contigs
#Mis_misassemblies               154                                                                            
#Mis_relocations                 153                                                                            
#Mis_translocations              0                                                                              
#Mis_inversions                  1                                                                              
#Mis_misassembled contigs        154                                                                            
Mis_Misassembled contigs length  64529                                                                          
#Mis_local misassemblies         2                                                                              
#mismatches                      177                                                                            
#indels                          1936                                                                           
#Mis_short indels (<= 5 bp)      1936                                                                           
#Mis_long indels (> 5 bp)        0                                                                              
Indels length                    2011                                                                           
