All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_51.final.contig
#Contigs                                   6968                                                                                
#Contigs (>= 0 bp)                         282161                                                                              
#Contigs (>= 1000 bp)                      0                                                                                   
Largest contig                             659                                                                                 
Total length                               1599031                                                                             
Total length (>= 0 bp)                     24431607                                                                            
Total length (>= 1000 bp)                  0                                                                                   
Reference length                           2813862                                                                             
N50                                        222                                                                                 
NG50                                       204                                                                                 
N75                                        209                                                                                 
NG75                                       None                                                                                
L50                                        3153                                                                                
LG50                                       6016                                                                                
L75                                        5009                                                                                
LG75                                       None                                                                                
#local misassemblies                       0                                                                                   
#misassemblies                             0                                                                                   
#misassembled contigs                      0                                                                                   
Misassembled contigs length                0                                                                                   
Misassemblies inter-contig overlap         0                                                                                   
#unaligned contigs                         6689 + 2 part                                                                       
Unaligned contigs length                   1532465                                                                             
#ambiguously mapped contigs                0                                                                                   
Extra bases in ambiguously mapped contigs  0                                                                                   
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        2.314                                                                               
Duplication ratio                          1.022                                                                               
#genes                                     3 + 225 part                                                                        
#predicted genes (unique)                  3779                                                                                
#predicted genes (>= 0 bp)                 3779                                                                                
#predicted genes (>= 300 bp)               14                                                                                  
#predicted genes (>= 1500 bp)              0                                                                                   
#predicted genes (>= 3000 bp)              0                                                                                   
#mismatches                                46                                                                                  
#indels                                    486                                                                                 
Indels length                              508                                                                                 
#mismatches per 100 kbp                    70.65                                                                               
#indels per 100 kbp                        746.43                                                                              
GC (%)                                     30.94                                                                               
Reference GC (%)                           32.81                                                                               
Average %IDY                               99.132                                                                              
Largest alignment                          398                                                                                 
NA50                                       None                                                                                
NGA50                                      None                                                                                
NA75                                       None                                                                                
NGA75                                      None                                                                                
LA50                                       None                                                                                
LGA50                                      None                                                                                
LA75                                       None                                                                                
LGA75                                      None                                                                                
#Mis_misassemblies                         0                                                                                   
#Mis_relocations                           0                                                                                   
#Mis_translocations                        0                                                                                   
#Mis_inversions                            0                                                                                   
#Mis_misassembled contigs                  0                                                                                   
Mis_Misassembled contigs length            0                                                                                   
#Mis_local misassemblies                   0                                                                                   
#Mis_short indels (<= 5 bp)                486                                                                                 
#Mis_long indels (> 5 bp)                  0                                                                                   
#Una_fully unaligned contigs               6689                                                                                
Una_Fully unaligned length                 1532422                                                                             
#Una_partially unaligned contigs           2                                                                                   
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             43                                                                                  
GAGE_Contigs #                             6968                                                                                
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            659                                                                                 
GAGE_N50                                   204 COUNT: 6016                                                                     
GAGE_Genome size                           2813862                                                                             
GAGE_Assembly size                         1599031                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               2037183(72.40%)                                                                     
GAGE_Missing assembly bases                688645(43.07%)                                                                      
GAGE_Missing assembly contigs              2775(39.82%)                                                                        
GAGE_Duplicated reference bases            49251                                                                               
GAGE_Compressed reference bases            7502                                                                                
GAGE_Bad trim                              40531                                                                               
GAGE_Avg idy                               96.61                                                                               
GAGE_SNPs                                  1480                                                                                
GAGE_Indels < 5bp                          22212                                                                               
GAGE_Indels >= 5                           4                                                                                   
GAGE_Inversions                            4                                                                                   
GAGE_Relocation                            1                                                                                   
GAGE_Translocation                         0                                                                                   
GAGE_Corrected contig #                    3192                                                                                
GAGE_Corrected assembly size               720887                                                                              
GAGE_Min correct contig                    200                                                                                 
GAGE_Max correct contig                    397                                                                                 
GAGE_Corrected N50                         0 COUNT: 0                                                                          
