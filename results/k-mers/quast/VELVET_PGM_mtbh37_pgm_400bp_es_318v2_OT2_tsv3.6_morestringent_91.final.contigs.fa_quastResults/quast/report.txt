All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_91.final.contigs
#Contigs (>= 0 bp)             9140                                                                          
#Contigs (>= 1000 bp)          234                                                                           
Total length (>= 0 bp)         3355708                                                                       
Total length (>= 1000 bp)      302140                                                                        
#Contigs                       7741                                                                          
Largest contig                 2901                                                                          
Total length                   3091208                                                                       
Reference length               4411532                                                                       
GC (%)                         66.21                                                                         
Reference GC (%)               65.61                                                                         
N50                            437                                                                           
NG50                           302                                                                           
N75                            284                                                                           
NG75                           None                                                                          
#misassemblies                 65                                                                            
#local misassemblies           6                                                                             
#unaligned contigs             3 + 5 part                                                                    
Unaligned contigs length       1486                                                                          
Genome fraction (%)            66.807                                                                        
Duplication ratio              1.046                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        5.02                                                                          
#indels per 100 kbp            86.62                                                                         
#genes                         278 + 3338 part                                                               
#predicted genes (unique)      8502                                                                          
#predicted genes (>= 0 bp)     8515                                                                          
#predicted genes (>= 300 bp)   3145                                                                          
#predicted genes (>= 1500 bp)  4                                                                             
#predicted genes (>= 3000 bp)  0                                                                             
Largest alignment              2901                                                                          
NA50                           436                                                                           
NGA50                          301                                                                           
NA75                           282                                                                           
NGA75                          None                                                                          
