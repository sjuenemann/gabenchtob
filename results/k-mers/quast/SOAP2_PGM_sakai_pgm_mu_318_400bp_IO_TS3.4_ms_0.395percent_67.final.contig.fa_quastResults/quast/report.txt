All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_67.final.contig
#Contigs (>= 0 bp)             249253                                                                   
#Contigs (>= 1000 bp)          0                                                                        
Total length (>= 0 bp)         27936179                                                                 
Total length (>= 1000 bp)      0                                                                        
#Contigs                       10737                                                                    
Largest contig                 877                                                                      
Total length                   2657482                                                                  
Reference length               5594470                                                                  
GC (%)                         49.29                                                                    
Reference GC (%)               50.48                                                                    
N50                            235                                                                      
NG50                           None                                                                     
N75                            215                                                                      
NG75                           None                                                                     
#misassemblies                 101                                                                      
#local misassemblies           2                                                                        
#unaligned contigs             1195 + 123 part                                                          
Unaligned contigs length       291562                                                                   
Genome fraction (%)            36.859                                                                   
Duplication ratio              1.137                                                                    
#N's per 100 kbp               0.00                                                                     
#mismatches per 100 kbp        36.42                                                                    
#indels per 100 kbp            999.49                                                                   
#genes                         65 + 3745 part                                                           
#predicted genes (unique)      8750                                                                     
#predicted genes (>= 0 bp)     8752                                                                     
#predicted genes (>= 300 bp)   763                                                                      
#predicted genes (>= 1500 bp)  0                                                                        
#predicted genes (>= 3000 bp)  0                                                                        
Largest alignment              877                                                                      
NA50                           230                                                                      
NGA50                          None                                                                     
NA75                           208                                                                      
NGA75                          None                                                                     
