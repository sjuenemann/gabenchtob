All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K47.final_contigs
#Contigs (>= 0 bp)             479                                                                                
#Contigs (>= 1000 bp)          181                                                                                
Total length (>= 0 bp)         4327690                                                                            
Total length (>= 1000 bp)      4256663                                                                            
#Contigs                       331                                                                                
Largest contig                 158464                                                                             
Total length                   4315616                                                                            
Reference length               4411532                                                                            
GC (%)                         65.43                                                                              
Reference GC (%)               65.61                                                                              
N50                            52828                                                                              
NG50                           50185                                                                              
N75                            26974                                                                              
NG75                           25274                                                                              
#misassemblies                 10                                                                                 
#local misassemblies           43                                                                                 
#unaligned contigs             36 + 10 part                                                                       
Unaligned contigs length       9918                                                                               
Genome fraction (%)            97.216                                                                             
Duplication ratio              1.001                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        9.72                                                                               
#indels per 100 kbp            27.28                                                                              
#genes                         3897 + 139 part                                                                    
#predicted genes (unique)      4482                                                                               
#predicted genes (>= 0 bp)     4483                                                                               
#predicted genes (>= 300 bp)   3743                                                                               
#predicted genes (>= 1500 bp)  491                                                                                
#predicted genes (>= 3000 bp)  51                                                                                 
Largest alignment              158464                                                                             
NA50                           50185                                                                              
NGA50                          48919                                                                              
NA75                           26974                                                                              
NGA75                          25274                                                                              
