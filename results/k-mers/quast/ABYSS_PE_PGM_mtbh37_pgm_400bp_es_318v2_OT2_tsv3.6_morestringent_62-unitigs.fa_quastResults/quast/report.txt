All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_62-unitigs
#Contigs (>= 0 bp)             11310                                                                     
#Contigs (>= 1000 bp)          1341                                                                      
Total length (>= 0 bp)         5110208                                                                   
Total length (>= 1000 bp)      3549609                                                                   
#Contigs                       2435                                                                      
Largest contig                 13854                                                                     
Total length                   4137497                                                                   
Reference length               4411532                                                                   
GC (%)                         65.36                                                                     
Reference GC (%)               65.61                                                                     
N50                            2723                                                                      
NG50                           2513                                                                      
N75                            1474                                                                      
NG75                           1258                                                                      
#misassemblies                 0                                                                         
#local misassemblies           13                                                                        
#unaligned contigs             2 + 1 part                                                                
Unaligned contigs length       848                                                                       
Genome fraction (%)            93.432                                                                    
Duplication ratio              1.001                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        2.86                                                                      
#indels per 100 kbp            16.13                                                                     
#genes                         2357 + 1651 part                                                          
#predicted genes (unique)      5747                                                                      
#predicted genes (>= 0 bp)     5750                                                                      
#predicted genes (>= 300 bp)   4194                                                                      
#predicted genes (>= 1500 bp)  267                                                                       
#predicted genes (>= 3000 bp)  19                                                                        
Largest alignment              13854                                                                     
NA50                           2723                                                                      
NGA50                          2513                                                                      
NA75                           1474                                                                      
NGA75                          1258                                                                      
