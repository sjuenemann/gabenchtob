All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_93.final.contig
#Contigs                                   24611                                                                               
#Contigs (>= 0 bp)                         120232                                                                              
#Contigs (>= 1000 bp)                      0                                                                                   
Largest contig                             846                                                                                 
Total length                               7683800                                                                             
Total length (>= 0 bp)                     20937371                                                                            
Total length (>= 1000 bp)                  0                                                                                   
Reference length                           2813862                                                                             
N50                                        313                                                                                 
NG50                                       358                                                                                 
N75                                        291                                                                                 
NG75                                       341                                                                                 
L50                                        10913                                                                               
LG50                                       3578                                                                                
L75                                        17289                                                                               
LG75                                       5594                                                                                
#local misassemblies                       1                                                                                   
#misassemblies                             199                                                                                 
#misassembled contigs                      199                                                                                 
Misassembled contigs length                64061                                                                               
Misassemblies inter-contig overlap         861                                                                                 
#unaligned contigs                         2164 + 462 part                                                                     
Unaligned contigs length                   740038                                                                              
#ambiguously mapped contigs                113                                                                                 
Extra bases in ambiguously mapped contigs  -35394                                                                              
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        91.339                                                                              
Duplication ratio                          2.688                                                                               
#genes                                     250 + 2405 part                                                                     
#predicted genes (unique)                  21652                                                                               
#predicted genes (>= 0 bp)                 21748                                                                               
#predicted genes (>= 300 bp)               630                                                                                 
#predicted genes (>= 1500 bp)              0                                                                                   
#predicted genes (>= 3000 bp)              0                                                                                   
#mismatches                                3318                                                                                
#indels                                    48114                                                                               
Indels length                              49466                                                                               
#mismatches per 100 kbp                    129.10                                                                              
#indels per 100 kbp                        1872.02                                                                             
GC (%)                                     31.90                                                                               
Reference GC (%)                           32.81                                                                               
Average %IDY                               97.934                                                                              
Largest alignment                          822                                                                                 
NA50                                       305                                                                                 
NGA50                                      351                                                                                 
NA75                                       282                                                                                 
NGA75                                      334                                                                                 
LA50                                       11136                                                                               
LGA50                                      3638                                                                                
LA75                                       17688                                                                               
LGA75                                      5695                                                                                
#Mis_misassemblies                         199                                                                                 
#Mis_relocations                           184                                                                                 
#Mis_translocations                        13                                                                                  
#Mis_inversions                            2                                                                                   
#Mis_misassembled contigs                  199                                                                                 
Mis_Misassembled contigs length            64061                                                                               
#Mis_local misassemblies                   1                                                                                   
#Mis_short indels (<= 5 bp)                48113                                                                               
#Mis_long indels (> 5 bp)                  1                                                                                   
#Una_fully unaligned contigs               2164                                                                                
Una_Fully unaligned length                 704890                                                                              
#Una_partially unaligned contigs           462                                                                                 
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            1                                                                                   
Una_Partially unaligned length             35148                                                                               
GAGE_Contigs #                             24611                                                                               
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            846                                                                                 
GAGE_N50                                   358 COUNT: 3578                                                                     
GAGE_Genome size                           2813862                                                                             
GAGE_Assembly size                         7683800                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               193288(6.87%)                                                                       
GAGE_Missing assembly bases                425204(5.53%)                                                                       
GAGE_Missing assembly contigs              880(3.58%)                                                                          
GAGE_Duplicated reference bases            3868461                                                                             
GAGE_Compressed reference bases            49935                                                                               
GAGE_Bad trim                              136246                                                                              
GAGE_Avg idy                               98.31                                                                               
GAGE_SNPs                                  1851                                                                                
GAGE_Indels < 5bp                          30589                                                                               
GAGE_Indels >= 5                           4                                                                                   
GAGE_Inversions                            8                                                                                   
GAGE_Relocation                            4                                                                                   
GAGE_Translocation                         3                                                                                   
GAGE_Corrected contig #                    10992                                                                               
GAGE_Corrected assembly size               3356504                                                                             
GAGE_Min correct contig                    200                                                                                 
GAGE_Max correct contig                    822                                                                                 
GAGE_Corrected N50                         317 COUNT: 3908                                                                     
