All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_77.final.contigs
GAGE_Contigs #                   7126                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  2252                                                                          
GAGE_N50                         251 COUNT: 4943                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               2689679                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     1755507(39.79%)                                                               
GAGE_Missing assembly bases      558(0.02%)                                                                    
GAGE_Missing assembly contigs    1(0.01%)                                                                      
GAGE_Duplicated reference bases  18277                                                                         
GAGE_Compressed reference bases  13508                                                                         
GAGE_Bad trim                    326                                                                           
GAGE_Avg idy                     99.92                                                                         
GAGE_SNPs                        113                                                                           
GAGE_Indels < 5bp                1986                                                                          
GAGE_Indels >= 5                 6                                                                             
GAGE_Inversions                  10                                                                            
GAGE_Relocation                  9                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          7020                                                                          
GAGE_Corrected assembly size     2665248                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          2252                                                                          
GAGE_Corrected N50               250 COUNT: 4949                                                               
