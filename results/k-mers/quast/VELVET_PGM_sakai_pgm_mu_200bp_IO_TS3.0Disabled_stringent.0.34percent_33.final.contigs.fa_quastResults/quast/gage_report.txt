All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_33.final.contigs
GAGE_Contigs #                   1525                                                                                 
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  31139                                                                                
GAGE_N50                         6716 COUNT: 251                                                                      
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5227556                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     227003(4.06%)                                                                        
GAGE_Missing assembly bases      139(0.00%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  299                                                                                  
GAGE_Compressed reference bases  159760                                                                               
GAGE_Bad trim                    109                                                                                  
GAGE_Avg idy                     99.93                                                                                
GAGE_SNPs                        389                                                                                  
GAGE_Indels < 5bp                3069                                                                                 
GAGE_Indels >= 5                 7                                                                                    
GAGE_Inversions                  3                                                                                    
GAGE_Relocation                  9                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          1545                                                                                 
GAGE_Corrected assembly size     5231729                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          31157                                                                                
GAGE_Corrected N50               6694 COUNT: 251                                                                      
