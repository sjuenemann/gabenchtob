All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K127.final_contigs
#Mis_misassemblies               81                                                                                         
#Mis_relocations                 21                                                                                         
#Mis_translocations              0                                                                                          
#Mis_inversions                  60                                                                                         
#Mis_misassembled contigs        77                                                                                         
Mis_Misassembled contigs length  383684                                                                                     
#Mis_local misassemblies         11                                                                                         
#mismatches                      1077                                                                                       
#indels                          2190                                                                                       
#Mis_short indels (<= 5 bp)      2184                                                                                       
#Mis_long indels (> 5 bp)        6                                                                                          
Indels length                    2330                                                                                       
