All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K85.final_contigs
#Contigs                                   3783                                                                                       
#Contigs (>= 0 bp)                         4152                                                                                       
#Contigs (>= 1000 bp)                      70                                                                                         
Largest contig                             207847                                                                                     
Total length                               3854520                                                                                    
Total length (>= 0 bp)                     3898137                                                                                    
Total length (>= 1000 bp)                  2753278                                                                                    
Reference length                           2813862                                                                                    
N50                                        43237                                                                                      
NG50                                       87665                                                                                      
N75                                        384                                                                                        
NG75                                       37277                                                                                      
L50                                        19                                                                                         
LG50                                       10                                                                                         
L75                                        393                                                                                        
LG75                                       24                                                                                         
#local misassemblies                       9                                                                                          
#misassemblies                             6                                                                                          
#misassembled contigs                      6                                                                                          
Misassembled contigs length                70895                                                                                      
Misassemblies inter-contig overlap         2366                                                                                       
#unaligned contigs                         1465 + 76 part                                                                             
Unaligned contigs length                   438047                                                                                     
#ambiguously mapped contigs                21                                                                                         
Extra bases in ambiguously mapped contigs  -11935                                                                                     
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        98.292                                                                                     
Duplication ratio                          1.232                                                                                      
#genes                                     2647 + 54 part                                                                             
#predicted genes (unique)                  5034                                                                                       
#predicted genes (>= 0 bp)                 5041                                                                                       
#predicted genes (>= 300 bp)               2321                                                                                       
#predicted genes (>= 1500 bp)              288                                                                                        
#predicted genes (>= 3000 bp)              28                                                                                         
#mismatches                                172                                                                                        
#indels                                    330                                                                                        
Indels length                              404                                                                                        
#mismatches per 100 kbp                    6.22                                                                                       
#indels per 100 kbp                        11.93                                                                                      
GC (%)                                     32.25                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               97.418                                                                                     
Largest alignment                          207847                                                                                     
NA50                                       43237                                                                                      
NGA50                                      87665                                                                                      
NA75                                       350                                                                                        
NGA75                                      37277                                                                                      
LA50                                       19                                                                                         
LGA50                                      10                                                                                         
LA75                                       430                                                                                        
LGA75                                      24                                                                                         
#Mis_misassemblies                         6                                                                                          
#Mis_relocations                           5                                                                                          
#Mis_translocations                        1                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  6                                                                                          
Mis_Misassembled contigs length            70895                                                                                      
#Mis_local misassemblies                   9                                                                                          
#Mis_short indels (<= 5 bp)                326                                                                                        
#Mis_long indels (> 5 bp)                  4                                                                                          
#Una_fully unaligned contigs               1465                                                                                       
Una_Fully unaligned length                 433584                                                                                     
#Una_partially unaligned contigs           76                                                                                         
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             4463                                                                                       
GAGE_Contigs #                             3783                                                                                       
GAGE_Min contig                            201                                                                                        
GAGE_Max contig                            207847                                                                                     
GAGE_N50                                   87665 COUNT: 10                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         3854520                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               3095(0.11%)                                                                                
GAGE_Missing assembly bases                200024(5.19%)                                                                              
GAGE_Missing assembly contigs              559(14.78%)                                                                                
GAGE_Duplicated reference bases            874375                                                                                     
GAGE_Compressed reference bases            42871                                                                                      
GAGE_Bad trim                              29130                                                                                      
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  50                                                                                         
GAGE_Indels < 5bp                          293                                                                                        
GAGE_Indels >= 5                           9                                                                                          
GAGE_Inversions                            1                                                                                          
GAGE_Relocation                            2                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    148                                                                                        
GAGE_Corrected assembly size               2782050                                                                                    
GAGE_Min correct contig                    201                                                                                        
GAGE_Max correct contig                    207865                                                                                     
GAGE_Corrected N50                         64171 COUNT: 13                                                                            
