All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_117.final.contig
#Contigs                                   44534                                                                        
#Contigs (>= 0 bp)                         88882                                                                        
#Contigs (>= 1000 bp)                      81                                                                           
Largest contig                             1847                                                                         
Total length                               11302758                                                                     
Total length (>= 0 bp)                     17959065                                                                     
Total length (>= 1000 bp)                  98006                                                                        
Reference length                           4411532                                                                      
N50                                        233                                                                          
NG50                                       292                                                                          
N75                                        231                                                                          
NG75                                       234                                                                          
L50                                        19470                                                                        
LG50                                       4965                                                                         
L75                                        31655                                                                        
LG75                                       9428                                                                         
#local misassemblies                       5                                                                            
#misassemblies                             24                                                                           
#misassembled contigs                      24                                                                           
Misassembled contigs length                7462                                                                         
Misassemblies inter-contig overlap         408                                                                          
#unaligned contigs                         44 + 5 part                                                                  
Unaligned contigs length                   15114                                                                        
#ambiguously mapped contigs                308                                                                          
Extra bases in ambiguously mapped contigs  -71133                                                                       
#N's                                       0                                                                            
#N's per 100 kbp                           0.00                                                                         
Genome fraction (%)                        92.160                                                                       
Duplication ratio                          2.759                                                                        
#genes                                     238 + 3736 part                                                              
#predicted genes (unique)                  41894                                                                        
#predicted genes (>= 0 bp)                 42827                                                                        
#predicted genes (>= 300 bp)               2764                                                                         
#predicted genes (>= 1500 bp)              0                                                                            
#predicted genes (>= 3000 bp)              0                                                                            
#mismatches                                255                                                                          
#indels                                    8983                                                                         
Indels length                              9207                                                                         
#mismatches per 100 kbp                    6.27                                                                         
#indels per 100 kbp                        220.95                                                                       
GC (%)                                     64.60                                                                        
Reference GC (%)                           65.61                                                                        
Average %IDY                               99.645                                                                       
Largest alignment                          1846                                                                         
NA50                                       233                                                                          
NGA50                                      290                                                                          
NA75                                       231                                                                          
NGA75                                      234                                                                          
LA50                                       19503                                                                        
LGA50                                      4980                                                                         
LA75                                       31691                                                                        
LGA75                                      9460                                                                         
#Mis_misassemblies                         24                                                                           
#Mis_relocations                           23                                                                           
#Mis_translocations                        0                                                                            
#Mis_inversions                            1                                                                            
#Mis_misassembled contigs                  24                                                                           
Mis_Misassembled contigs length            7462                                                                         
#Mis_local misassemblies                   5                                                                            
#Mis_short indels (<= 5 bp)                8983                                                                         
#Mis_long indels (> 5 bp)                  0                                                                            
#Una_fully unaligned contigs               44                                                                           
Una_Fully unaligned length                 14604                                                                        
#Una_partially unaligned contigs           5                                                                            
#Una_with misassembly                      0                                                                            
#Una_both parts are significant            0                                                                            
Una_Partially unaligned length             510                                                                          
GAGE_Contigs #                             44534                                                                        
GAGE_Min contig                            200                                                                          
GAGE_Max contig                            1847                                                                         
GAGE_N50                                   292 COUNT: 4965                                                              
GAGE_Genome size                           4411532                                                                      
GAGE_Assembly size                         11302758                                                                     
GAGE_Chaff bases                           0                                                                            
GAGE_Missing reference bases               295742(6.70%)                                                                
GAGE_Missing assembly bases                6521(0.06%)                                                                  
GAGE_Missing assembly contigs              13(0.03%)                                                                    
GAGE_Duplicated reference bases            5927040                                                                      
GAGE_Compressed reference bases            52637                                                                        
GAGE_Bad trim                              2694                                                                         
GAGE_Avg idy                               99.89                                                                        
GAGE_SNPs                                  91                                                                           
GAGE_Indels < 5bp                          3879                                                                         
GAGE_Indels >= 5                           4                                                                            
GAGE_Inversions                            0                                                                            
GAGE_Relocation                            1                                                                            
GAGE_Translocation                         0                                                                            
GAGE_Corrected contig #                    19299                                                                        
GAGE_Corrected assembly size               5366543                                                                      
GAGE_Min correct contig                    200                                                                          
GAGE_Max correct contig                    1849                                                                         
GAGE_Corrected N50                         244 COUNT: 5313                                                              
