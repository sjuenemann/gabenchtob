All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_71.final.contigs
GAGE_Contigs #                   1540                                                                                 
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  35670                                                                                
GAGE_N50                         7948 COUNT: 209                                                                      
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5345797                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     87168(1.56%)                                                                         
GAGE_Missing assembly bases      1699(0.03%)                                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  10451                                                                                
GAGE_Compressed reference bases  243876                                                                               
GAGE_Bad trim                    805                                                                                  
GAGE_Avg idy                     99.95                                                                                
GAGE_SNPs                        109                                                                                  
GAGE_Indels < 5bp                2184                                                                                 
GAGE_Indels >= 5                 48                                                                                   
GAGE_Inversions                  2                                                                                    
GAGE_Relocation                  52                                                                                   
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          1589                                                                                 
GAGE_Corrected assembly size     5335970                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          32749                                                                                
GAGE_Corrected N50               7222 COUNT: 232                                                                      
