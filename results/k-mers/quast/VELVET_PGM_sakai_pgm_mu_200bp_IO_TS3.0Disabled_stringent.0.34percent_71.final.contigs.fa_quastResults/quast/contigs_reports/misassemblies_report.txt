All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_71.final.contigs
#Mis_misassemblies               33                                                                                   
#Mis_relocations                 29                                                                                   
#Mis_translocations              0                                                                                    
#Mis_inversions                  4                                                                                    
#Mis_misassembled contigs        32                                                                                   
Mis_Misassembled contigs length  240881                                                                               
#Mis_local misassemblies         38                                                                                   
#mismatches                      169                                                                                  
#indels                          2593                                                                                 
#Mis_short indels (<= 5 bp)      2566                                                                                 
#Mis_long indels (> 5 bp)        27                                                                                   
Indels length                    4243                                                                                 
