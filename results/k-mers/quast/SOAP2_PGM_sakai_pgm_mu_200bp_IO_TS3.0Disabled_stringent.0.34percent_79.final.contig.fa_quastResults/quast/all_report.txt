All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_79.final.contig
#Contigs                                   10359                                                                              
#Contigs (>= 0 bp)                         61547                                                                              
#Contigs (>= 1000 bp)                      572                                                                                
Largest contig                             3943                                                                               
Total length                               4745193                                                                            
Total length (>= 0 bp)                     11841350                                                                           
Total length (>= 1000 bp)                  738230                                                                             
Reference length                           5594470                                                                            
N50                                        526                                                                                
NG50                                       453                                                                                
N75                                        334                                                                                
NG75                                       250                                                                                
L50                                        2886                                                                               
LG50                                       3755                                                                               
L75                                        5714                                                                               
LG75                                       7934                                                                               
#local misassemblies                       2                                                                                  
#misassemblies                             34                                                                                 
#misassembled contigs                      33                                                                                 
Misassembled contigs length                8157                                                                               
Misassemblies inter-contig overlap         259                                                                                
#unaligned contigs                         118 + 116 part                                                                     
Unaligned contigs length                   37363                                                                              
#ambiguously mapped contigs                123                                                                                
Extra bases in ambiguously mapped contigs  -33329                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        80.408                                                                             
Duplication ratio                          1.039                                                                              
#genes                                     743 + 4189 part                                                                    
#predicted genes (unique)                  11988                                                                              
#predicted genes (>= 0 bp)                 11995                                                                              
#predicted genes (>= 300 bp)               5322                                                                               
#predicted genes (>= 1500 bp)              14                                                                                 
#predicted genes (>= 3000 bp)              0                                                                                  
#mismatches                                217                                                                                
#indels                                    2211                                                                               
Indels length                              2281                                                                               
#mismatches per 100 kbp                    4.82                                                                               
#indels per 100 kbp                        49.15                                                                              
GC (%)                                     50.05                                                                              
Reference GC (%)                           50.48                                                                              
Average %IDY                               99.713                                                                             
Largest alignment                          3943                                                                               
NA50                                       526                                                                                
NGA50                                      452                                                                                
NA75                                       333                                                                                
NGA75                                      246                                                                                
LA50                                       2887                                                                               
LGA50                                      3757                                                                               
LA75                                       5720                                                                               
LGA75                                      7956                                                                               
#Mis_misassemblies                         34                                                                                 
#Mis_relocations                           7                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            27                                                                                 
#Mis_misassembled contigs                  33                                                                                 
Mis_Misassembled contigs length            8157                                                                               
#Mis_local misassemblies                   2                                                                                  
#Mis_short indels (<= 5 bp)                2211                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               118                                                                                
Una_Fully unaligned length                 29069                                                                              
#Una_partially unaligned contigs           116                                                                                
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             8294                                                                               
GAGE_Contigs #                             10359                                                                              
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            3943                                                                               
GAGE_N50                                   453 COUNT: 3755                                                                    
GAGE_Genome size                           5594470                                                                            
GAGE_Assembly size                         4745193                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               964055(17.23%)                                                                     
GAGE_Missing assembly bases                12558(0.26%)                                                                       
GAGE_Missing assembly contigs              22(0.21%)                                                                          
GAGE_Duplicated reference bases            129039                                                                             
GAGE_Compressed reference bases            119895                                                                             
GAGE_Bad trim                              7010                                                                               
GAGE_Avg idy                               99.94                                                                              
GAGE_SNPs                                  155                                                                                
GAGE_Indels < 5bp                          1751                                                                               
GAGE_Indels >= 5                           3                                                                                  
GAGE_Inversions                            10                                                                                 
GAGE_Relocation                            0                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    9742                                                                               
GAGE_Corrected assembly size               4595646                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    3943                                                                               
GAGE_Corrected N50                         453 COUNT: 3754                                                                    
