All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K39.final_contigs
#Mis_misassemblies               2                                                                                               
#Mis_relocations                 2                                                                                               
#Mis_translocations              0                                                                                               
#Mis_inversions                  0                                                                                               
#Mis_misassembled contigs        2                                                                                               
Mis_Misassembled contigs length  54185                                                                                           
#Mis_local misassemblies         10                                                                                              
#mismatches                      179                                                                                             
#indels                          391                                                                                             
#Mis_short indels (<= 5 bp)      391                                                                                             
#Mis_long indels (> 5 bp)        0                                                                                               
Indels length                    414                                                                                             
