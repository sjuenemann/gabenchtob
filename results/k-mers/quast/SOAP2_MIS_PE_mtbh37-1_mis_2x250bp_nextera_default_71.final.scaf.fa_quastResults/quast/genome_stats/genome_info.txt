reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_71.final.scaf_broken  | 94.3574477075       | 1.57259905987     | 1361        | 479       | 3554      | None      | None      |
  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_71.final.scaf  | 94.3465444657       | 1.57335838782     | 1364        | 479       | 3554      | None      | None      |
