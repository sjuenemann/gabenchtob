All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_71.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_71.final.scaf
GAGE_Contigs #                   99482                                                                   99476                                                          
GAGE_Min contig                  200                                                                     200                                                            
GAGE_Max contig                  3077                                                                    3077                                                           
GAGE_N50                         455 COUNT: 3186                                                         459 COUNT: 3173                                                
GAGE_Genome size                 4411532                                                                 4411532                                                        
GAGE_Assembly size               26012156                                                                26021625                                                       
GAGE_Chaff bases                 0                                                                       0                                                              
GAGE_Missing reference bases     152621(3.46%)                                                           151032(3.42%)                                                  
GAGE_Missing assembly bases      18552014(71.32%)                                                        18556538(71.31%)                                               
GAGE_Missing assembly contigs    73373(73.76%)                                                           73374(73.76%)                                                  
GAGE_Duplicated reference bases  2540844                                                                 2543230                                                        
GAGE_Compressed reference bases  66524                                                                   66453                                                          
GAGE_Bad trim                    632882                                                                  633117                                                         
GAGE_Avg idy                     99.50                                                                   99.51                                                          
GAGE_SNPs                        10848                                                                   10777                                                          
GAGE_Indels < 5bp                165                                                                     163                                                            
GAGE_Indels >= 5                 6                                                                       22                                                             
GAGE_Inversions                  1                                                                       1                                                              
GAGE_Relocation                  1                                                                       1                                                              
GAGE_Translocation               0                                                                       0                                                              
GAGE_Corrected contig #          12265                                                                   12261                                                          
GAGE_Corrected assembly size     4771836                                                                 4770741                                                        
GAGE_Min correct contig          200                                                                     200                                                            
GAGE_Max correct contig          3077                                                                    3077                                                           
GAGE_Corrected N50               454 COUNT: 3188                                                         454 COUNT: 3188                                                
