All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_81.final.contigs
GAGE_Contigs #                   7314                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  2678                                                                          
GAGE_N50                         269 COUNT: 4621                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               2820388                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     1648061(37.36%)                                                               
GAGE_Missing assembly bases      536(0.02%)                                                                    
GAGE_Missing assembly contigs    1(0.01%)                                                                      
GAGE_Duplicated reference bases  27890                                                                         
GAGE_Compressed reference bases  12209                                                                         
GAGE_Bad trim                    304                                                                           
GAGE_Avg idy                     99.91                                                                         
GAGE_SNPs                        116                                                                           
GAGE_Indels < 5bp                2161                                                                          
GAGE_Indels >= 5                 6                                                                             
GAGE_Inversions                  13                                                                            
GAGE_Relocation                  11                                                                            
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          7155                                                                          
GAGE_Corrected assembly size     2783935                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          2682                                                                          
GAGE_Corrected N50               268 COUNT: 4626                                                               
