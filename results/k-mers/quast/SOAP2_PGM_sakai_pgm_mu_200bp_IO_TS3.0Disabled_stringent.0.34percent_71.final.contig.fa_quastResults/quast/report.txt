All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_71.final.contig
#Contigs (>= 0 bp)             79583                                                                              
#Contigs (>= 1000 bp)          247                                                                                
Total length (>= 0 bp)         12992189                                                                           
Total length (>= 1000 bp)      301220                                                                             
#Contigs                       11780                                                                              
Largest contig                 2249                                                                               
Total length                   4532568                                                                            
Reference length               5594470                                                                            
GC (%)                         49.94                                                                              
Reference GC (%)               50.48                                                                              
N50                            417                                                                                
NG50                           349                                                                                
N75                            277                                                                                
NG75                           219                                                                                
#misassemblies                 82                                                                                 
#local misassemblies           2                                                                                  
#unaligned contigs             443 + 203 part                                                                     
Unaligned contigs length       118492                                                                             
Genome fraction (%)            74.159                                                                             
Duplication ratio              1.057                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        8.63                                                                               
#indels per 100 kbp            92.39                                                                              
#genes                         489 + 4319 part                                                                    
#predicted genes (unique)      12440                                                                              
#predicted genes (>= 0 bp)     12444                                                                              
#predicted genes (>= 300 bp)   4715                                                                               
#predicted genes (>= 1500 bp)  2                                                                                  
#predicted genes (>= 3000 bp)  0                                                                                  
Largest alignment              2249                                                                               
NA50                           417                                                                                
NGA50                          348                                                                                
NA75                           276                                                                                
NGA75                          213                                                                                
