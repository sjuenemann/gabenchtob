All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_75.final.scaf_broken  70981                         17606761                    4546                              0                      0                                496655                          15  
SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_75.final.scaf         71000                         17617205                    4549                              0                      2                                498976                          5031
