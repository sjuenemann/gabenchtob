All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_53.final.contigs
#Contigs (>= 0 bp)             15727                                                                         
#Contigs (>= 1000 bp)          48                                                                            
Total length (>= 0 bp)         3460118                                                                       
Total length (>= 1000 bp)      57202                                                                         
#Contigs                       6159                                                                          
Largest contig                 2123                                                                          
Total length                   2109221                                                                       
Reference length               4411532                                                                       
GC (%)                         67.12                                                                         
Reference GC (%)               65.61                                                                         
N50                            346                                                                           
NG50                           None                                                                          
N75                            259                                                                           
NG75                           None                                                                          
#misassemblies                 0                                                                             
#local misassemblies           1                                                                             
#unaligned contigs             3 + 9 part                                                                    
Unaligned contigs length       1131                                                                          
Genome fraction (%)            47.416                                                                        
Duplication ratio              1.006                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        6.55                                                                          
#indels per 100 kbp            56.65                                                                         
#genes                         97 + 3130 part                                                                
#predicted genes (unique)      6480                                                                          
#predicted genes (>= 0 bp)     6480                                                                          
#predicted genes (>= 300 bp)   2027                                                                          
#predicted genes (>= 1500 bp)  0                                                                             
#predicted genes (>= 3000 bp)  0                                                                             
Largest alignment              2123                                                                          
NA50                           346                                                                           
NGA50                          None                                                                          
NA75                           259                                                                           
NGA75                          None                                                                          
