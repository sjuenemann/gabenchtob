All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_53.final.contigs
#Contigs                                   6159                                                                          
#Contigs (>= 0 bp)                         15727                                                                         
#Contigs (>= 1000 bp)                      48                                                                            
Largest contig                             2123                                                                          
Total length                               2109221                                                                       
Total length (>= 0 bp)                     3460118                                                                       
Total length (>= 1000 bp)                  57202                                                                         
Reference length                           4411532                                                                       
N50                                        346                                                                           
NG50                                       None                                                                          
N75                                        259                                                                           
NG75                                       None                                                                          
L50                                        2053                                                                          
LG50                                       None                                                                          
L75                                        3832                                                                          
LG75                                       None                                                                          
#local misassemblies                       1                                                                             
#misassemblies                             0                                                                             
#misassembled contigs                      0                                                                             
Misassembled contigs length                0                                                                             
Misassemblies inter-contig overlap         153                                                                           
#unaligned contigs                         3 + 9 part                                                                    
Unaligned contigs length                   1131                                                                          
#ambiguously mapped contigs                13                                                                            
Extra bases in ambiguously mapped contigs  -3349                                                                         
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        47.416                                                                        
Duplication ratio                          1.006                                                                         
#genes                                     97 + 3130 part                                                                
#predicted genes (unique)                  6480                                                                          
#predicted genes (>= 0 bp)                 6480                                                                          
#predicted genes (>= 300 bp)               2027                                                                          
#predicted genes (>= 1500 bp)              0                                                                             
#predicted genes (>= 3000 bp)              0                                                                             
#mismatches                                137                                                                           
#indels                                    1185                                                                          
Indels length                              1245                                                                          
#mismatches per 100 kbp                    6.55                                                                          
#indels per 100 kbp                        56.65                                                                         
GC (%)                                     67.12                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.932                                                                        
Largest alignment                          2123                                                                          
NA50                                       346                                                                           
NGA50                                      None                                                                          
NA75                                       259                                                                           
NGA75                                      None                                                                          
LA50                                       2054                                                                          
LGA50                                      None                                                                          
LA75                                       3834                                                                          
LGA75                                      None                                                                          
#Mis_misassemblies                         0                                                                             
#Mis_relocations                           0                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  0                                                                             
Mis_Misassembled contigs length            0                                                                             
#Mis_local misassemblies                   1                                                                             
#Mis_short indels (<= 5 bp)                1185                                                                          
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               3                                                                             
Una_Fully unaligned length                 715                                                                           
#Una_partially unaligned contigs           9                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             416                                                                           
GAGE_Contigs #                             6159                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            2123                                                                          
GAGE_N50                                   0 COUNT: 0                                                                    
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         2109221                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               2311904(52.41%)                                                               
GAGE_Missing assembly bases                553(0.03%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            244                                                                           
GAGE_Compressed reference bases            4323                                                                          
GAGE_Bad trim                              553                                                                           
GAGE_Avg idy                               99.93                                                                         
GAGE_SNPs                                  127                                                                           
GAGE_Indels < 5bp                          1237                                                                          
GAGE_Indels >= 5                           1                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            0                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    6153                                                                          
GAGE_Corrected assembly size               2108045                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    2124                                                                          
GAGE_Corrected N50                         0 COUNT: 0                                                                    
