All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_100-unitigs
#Contigs (>= 0 bp)             4688                                                                    
#Contigs (>= 1000 bp)          688                                                                     
Total length (>= 0 bp)         5893481                                                                 
Total length (>= 1000 bp)      5102456                                                                 
#Contigs                       1242                                                                    
Largest contig                 50077                                                                   
Total length                   5335330                                                                 
Reference length               5594470                                                                 
GC (%)                         50.29                                                                   
Reference GC (%)               50.48                                                                   
N50                            11827                                                                   
NG50                           11197                                                                   
N75                            5868                                                                    
NG75                           5019                                                                    
#misassemblies                 5                                                                       
#local misassemblies           6                                                                       
#unaligned contigs             0 + 0 part                                                              
Unaligned contigs length       0                                                                       
Genome fraction (%)            93.711                                                                  
Duplication ratio              1.003                                                                   
#N's per 100 kbp               0.00                                                                    
#mismatches per 100 kbp        1.56                                                                    
#indels per 100 kbp            5.34                                                                    
#genes                         4426 + 705 part                                                         
#predicted genes (unique)      5993                                                                    
#predicted genes (>= 0 bp)     6005                                                                    
#predicted genes (>= 300 bp)   4638                                                                    
#predicted genes (>= 1500 bp)  588                                                                     
#predicted genes (>= 3000 bp)  48                                                                      
Largest alignment              50077                                                                   
NA50                           11827                                                                   
NGA50                          11162                                                                   
NA75                           5868                                                                    
NGA75                          5011                                                                    
