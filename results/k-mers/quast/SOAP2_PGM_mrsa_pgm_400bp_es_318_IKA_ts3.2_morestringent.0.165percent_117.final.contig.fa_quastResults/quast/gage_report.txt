All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_117.final.contig
GAGE_Contigs #                   38059                                                                                
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  1931                                                                                 
GAGE_N50                         414 COUNT: 2775                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               11365436                                                                             
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     18235(0.65%)                                                                         
GAGE_Missing assembly bases      192563(1.69%)                                                                        
GAGE_Missing assembly contigs    290(0.76%)                                                                           
GAGE_Duplicated reference bases  7511414                                                                              
GAGE_Compressed reference bases  37659                                                                                
GAGE_Bad trim                    79043                                                                                
GAGE_Avg idy                     99.87                                                                                
GAGE_SNPs                        127                                                                                  
GAGE_Indels < 5bp                2583                                                                                 
GAGE_Indels >= 5                 4                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  0                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          12671                                                                                
GAGE_Corrected assembly size     3658401                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          1931                                                                                 
GAGE_Corrected N50               308 COUNT: 3057                                                                      
