All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_117.final.contig
#Mis_misassemblies               180                                                                                  
#Mis_relocations                 173                                                                                  
#Mis_translocations              7                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        180                                                                                  
Mis_Misassembled contigs length  65609                                                                                
#Mis_local misassemblies         2                                                                                    
#mismatches                      1954                                                                                 
#indels                          28879                                                                                
#Mis_short indels (<= 5 bp)      28879                                                                                
#Mis_long indels (> 5 bp)        0                                                                                    
Indels length                    29720                                                                                
