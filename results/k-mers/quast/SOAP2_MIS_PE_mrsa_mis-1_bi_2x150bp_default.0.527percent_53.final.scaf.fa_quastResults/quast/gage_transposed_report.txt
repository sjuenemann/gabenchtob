All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                    
SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_53.final.scaf_broken
SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_53.final.scaf       
