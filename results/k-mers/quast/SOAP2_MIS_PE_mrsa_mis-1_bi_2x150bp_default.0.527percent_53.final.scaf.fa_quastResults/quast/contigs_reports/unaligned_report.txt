All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_53.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_53.final.scaf
#Una_fully unaligned contigs      5                                                                             8                                                                    
Una_Fully unaligned length        6697                                                                          8799                                                                 
#Una_partially unaligned contigs  0                                                                             2                                                                    
#Una_with misassembly             0                                                                             0                                                                    
#Una_both parts are significant   0                                                                             1                                                                    
Una_Partially unaligned length    0                                                                             742                                                                  
#N's                              138                                                                           17112                                                                
