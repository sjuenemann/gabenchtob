All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K49.final_contigs
GAGE_Contigs #                   628                                                                             
GAGE_Min contig                  200                                                                             
GAGE_Max contig                  268198                                                                          
GAGE_N50                         86358 COUNT: 20                                                                 
GAGE_Genome size                 5594470                                                                         
GAGE_Assembly size               5311007                                                                         
GAGE_Chaff bases                 0                                                                               
GAGE_Missing reference bases     79957(1.43%)                                                                    
GAGE_Missing assembly bases      12094(0.23%)                                                                    
GAGE_Missing assembly contigs    44(7.01%)                                                                       
GAGE_Duplicated reference bases  2360                                                                            
GAGE_Compressed reference bases  241250                                                                          
GAGE_Bad trim                    94                                                                              
GAGE_Avg idy                     99.98                                                                           
GAGE_SNPs                        233                                                                             
GAGE_Indels < 5bp                333                                                                             
GAGE_Indels >= 5                 18                                                                              
GAGE_Inversions                  0                                                                               
GAGE_Relocation                  9                                                                               
GAGE_Translocation               0                                                                               
GAGE_Corrected contig #          599                                                                             
GAGE_Corrected assembly size     5300812                                                                         
GAGE_Min correct contig          201                                                                             
GAGE_Max correct contig          217365                                                                          
GAGE_Corrected N50               61244 COUNT: 25                                                                 
