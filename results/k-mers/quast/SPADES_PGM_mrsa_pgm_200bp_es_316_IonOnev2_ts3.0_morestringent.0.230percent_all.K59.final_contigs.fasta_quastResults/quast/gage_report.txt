All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K59.final_contigs
GAGE_Contigs #                   762                                                                                             
GAGE_Min contig                  202                                                                                             
GAGE_Max contig                  148388                                                                                          
GAGE_N50                         47194 COUNT: 15                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2901474                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     13546(0.48%)                                                                                    
GAGE_Missing assembly bases      64004(2.21%)                                                                                    
GAGE_Missing assembly contigs    249(32.68%)                                                                                     
GAGE_Duplicated reference bases  71275                                                                                           
GAGE_Compressed reference bases  40404                                                                                           
GAGE_Bad trim                    6951                                                                                            
GAGE_Avg idy                     99.97                                                                                           
GAGE_SNPs                        67                                                                                              
GAGE_Indels < 5bp                464                                                                                             
GAGE_Indels >= 5                 13                                                                                              
GAGE_Inversions                  1                                                                                               
GAGE_Relocation                  4                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          194                                                                                             
GAGE_Corrected assembly size     2769966                                                                                         
GAGE_Min correct contig          202                                                                                             
GAGE_Max correct contig          148087                                                                                          
GAGE_Corrected N50               44027 COUNT: 19                                                                                 
