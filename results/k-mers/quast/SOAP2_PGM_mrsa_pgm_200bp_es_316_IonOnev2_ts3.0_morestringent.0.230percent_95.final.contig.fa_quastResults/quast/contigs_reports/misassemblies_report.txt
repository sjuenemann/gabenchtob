All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_95.final.contig
#Mis_misassemblies               14                                                                                       
#Mis_relocations                 8                                                                                        
#Mis_translocations              1                                                                                        
#Mis_inversions                  5                                                                                        
#Mis_misassembled contigs        14                                                                                       
Mis_Misassembled contigs length  16796                                                                                    
#Mis_local misassemblies         1                                                                                        
#mismatches                      68                                                                                       
#indels                          932                                                                                      
#Mis_short indels (<= 5 bp)      932                                                                                      
#Mis_long indels (> 5 bp)        0                                                                                        
Indels length                    959                                                                                      
