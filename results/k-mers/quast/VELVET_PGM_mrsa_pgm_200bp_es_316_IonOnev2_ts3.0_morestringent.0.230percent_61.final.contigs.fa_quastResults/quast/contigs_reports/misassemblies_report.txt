All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_61.final.contigs
#Mis_misassemblies               3                                                                                          
#Mis_relocations                 3                                                                                          
#Mis_translocations              0                                                                                          
#Mis_inversions                  0                                                                                          
#Mis_misassembled contigs        3                                                                                          
Mis_Misassembled contigs length  13813                                                                                      
#Mis_local misassemblies         9                                                                                          
#mismatches                      190                                                                                        
#indels                          709                                                                                        
#Mis_short indels (<= 5 bp)      709                                                                                        
#Mis_long indels (> 5 bp)        0                                                                                          
Indels length                    763                                                                                        
