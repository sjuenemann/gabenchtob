All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K21.final_contigs
#Contigs (>= 0 bp)             3537                                                                            
#Contigs (>= 1000 bp)          670                                                                             
Total length (>= 0 bp)         5329112                                                                         
Total length (>= 1000 bp)      4981339                                                                         
#Contigs                       1179                                                                            
Largest contig                 50325                                                                           
Total length                   5205808                                                                         
Reference length               5594470                                                                         
GC (%)                         50.25                                                                           
Reference GC (%)               50.48                                                                           
N50                            10817                                                                           
NG50                           10169                                                                           
N75                            6009                                                                            
NG75                           4755                                                                            
#misassemblies                 6                                                                               
#local misassemblies           7                                                                               
#unaligned contigs             21 + 0 part                                                                     
Unaligned contigs length       6444                                                                            
Genome fraction (%)            90.928                                                                          
Duplication ratio              1.006                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        5.41                                                                            
#indels per 100 kbp            7.37                                                                            
#genes                         4382 + 463 part                                                                 
#predicted genes (unique)      5657                                                                            
#predicted genes (>= 0 bp)     5673                                                                            
#predicted genes (>= 300 bp)   4565                                                                            
#predicted genes (>= 1500 bp)  575                                                                             
#predicted genes (>= 3000 bp)  50                                                                              
Largest alignment              50325                                                                           
NA50                           10796                                                                           
NGA50                          10144                                                                           
NA75                           5926                                                                            
NGA75                          4754                                                                            
