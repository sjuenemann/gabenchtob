All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_25.final.contigs
#Contigs                                   13                                                                                         
#Contigs (>= 0 bp)                         50381                                                                                      
#Contigs (>= 1000 bp)                      0                                                                                          
Largest contig                             246                                                                                        
Total length                               2832                                                                                       
Total length (>= 0 bp)                     3331694                                                                                    
Total length (>= 1000 bp)                  0                                                                                          
Reference length                           2813862                                                                                    
N50                                        215                                                                                        
NG50                                       None                                                                                       
N75                                        206                                                                                        
NG75                                       None                                                                                       
L50                                        7                                                                                          
LG50                                       None                                                                                       
L75                                        10                                                                                         
LG75                                       None                                                                                       
#local misassemblies                       0                                                                                          
#misassemblies                             0                                                                                          
#misassembled contigs                      0                                                                                          
Misassembled contigs length                0                                                                                          
Misassemblies inter-contig overlap         0                                                                                          
#unaligned contigs                         1 + 0 part                                                                                 
Unaligned contigs length                   200                                                                                        
#ambiguously mapped contigs                0                                                                                          
Extra bases in ambiguously mapped contigs  0                                                                                          
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        0.094                                                                                      
Duplication ratio                          1.000                                                                                      
#genes                                     0 + 11 part                                                                                
#predicted genes (unique)                  13                                                                                         
#predicted genes (>= 0 bp)                 13                                                                                         
#predicted genes (>= 300 bp)               0                                                                                          
#predicted genes (>= 1500 bp)              0                                                                                          
#predicted genes (>= 3000 bp)              0                                                                                          
#mismatches                                0                                                                                          
#indels                                    1                                                                                          
Indels length                              1                                                                                          
#mismatches per 100 kbp                    0.00                                                                                       
#indels per 100 kbp                        38.01                                                                                      
GC (%)                                     34.32                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.964                                                                                     
Largest alignment                          246                                                                                        
NA50                                       215                                                                                        
NGA50                                      None                                                                                       
NA75                                       206                                                                                        
NGA75                                      None                                                                                       
LA50                                       7                                                                                          
LGA50                                      None                                                                                       
LA75                                       10                                                                                         
LGA75                                      None                                                                                       
#Mis_misassemblies                         0                                                                                          
#Mis_relocations                           0                                                                                          
#Mis_translocations                        0                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  0                                                                                          
Mis_Misassembled contigs length            0                                                                                          
#Mis_local misassemblies                   0                                                                                          
#Mis_short indels (<= 5 bp)                1                                                                                          
#Mis_long indels (> 5 bp)                  0                                                                                          
#Una_fully unaligned contigs               1                                                                                          
Una_Fully unaligned length                 200                                                                                        
#Una_partially unaligned contigs           0                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             0                                                                                          
GAGE_Contigs #                             13                                                                                         
GAGE_Min contig                            200                                                                                        
GAGE_Max contig                            246                                                                                        
GAGE_N50                                   0 COUNT: 0                                                                                 
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         2832                                                                                       
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               2811231(99.91%)                                                                            
GAGE_Missing assembly bases                200(7.06%)                                                                                 
GAGE_Missing assembly contigs              1(7.69%)                                                                                   
GAGE_Duplicated reference bases            0                                                                                          
GAGE_Compressed reference bases            0                                                                                          
GAGE_Bad trim                              0                                                                                          
GAGE_Avg idy                               99.96                                                                                      
GAGE_SNPs                                  0                                                                                          
GAGE_Indels < 5bp                          1                                                                                          
GAGE_Indels >= 5                           0                                                                                          
GAGE_Inversions                            0                                                                                          
GAGE_Relocation                            0                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    12                                                                                         
GAGE_Corrected assembly size               2631                                                                                       
GAGE_Min correct contig                    201                                                                                        
GAGE_Max correct contig                    246                                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                                 
