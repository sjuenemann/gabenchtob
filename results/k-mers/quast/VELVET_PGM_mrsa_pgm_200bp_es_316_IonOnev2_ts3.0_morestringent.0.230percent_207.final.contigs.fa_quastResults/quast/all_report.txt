All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_207.final.contigs
#Contigs                                   1                                                                                           
#Contigs (>= 0 bp)                         1                                                                                           
#Contigs (>= 1000 bp)                      0                                                                                           
Largest contig                             803                                                                                         
Total length                               803                                                                                         
Total length (>= 0 bp)                     803                                                                                         
Total length (>= 1000 bp)                  0                                                                                           
Reference length                           2813862                                                                                     
N50                                        803                                                                                         
NG50                                       None                                                                                        
N75                                        803                                                                                         
NG75                                       None                                                                                        
L50                                        1                                                                                           
LG50                                       None                                                                                        
L75                                        1                                                                                           
LG75                                       None                                                                                        
#local misassemblies                       0                                                                                           
#misassemblies                             0                                                                                           
#misassembled contigs                      0                                                                                           
Misassembled contigs length                0                                                                                           
Misassemblies inter-contig overlap         0                                                                                           
#unaligned contigs                         0 + 1 part                                                                                  
Unaligned contigs length                   18                                                                                          
#ambiguously mapped contigs                0                                                                                           
Extra bases in ambiguously mapped contigs  0                                                                                           
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        0.028                                                                                       
Duplication ratio                          0.997                                                                                       
#genes                                     0 + 1 part                                                                                  
#predicted genes (unique)                  1                                                                                           
#predicted genes (>= 0 bp)                 1                                                                                           
#predicted genes (>= 300 bp)               1                                                                                           
#predicted genes (>= 1500 bp)              0                                                                                           
#predicted genes (>= 3000 bp)              0                                                                                           
#mismatches                                0                                                                                           
#indels                                    2                                                                                           
Indels length                              2                                                                                           
#mismatches per 100 kbp                    0.00                                                                                        
#indels per 100 kbp                        254.13                                                                                      
GC (%)                                     32.88                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               99.750                                                                                      
Largest alignment                          785                                                                                         
NA50                                       785                                                                                         
NGA50                                      None                                                                                        
NA75                                       785                                                                                         
NGA75                                      None                                                                                        
LA50                                       1                                                                                           
LGA50                                      None                                                                                        
LA75                                       1                                                                                           
LGA75                                      None                                                                                        
#Mis_misassemblies                         0                                                                                           
#Mis_relocations                           0                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  0                                                                                           
Mis_Misassembled contigs length            0                                                                                           
#Mis_local misassemblies                   0                                                                                           
#Mis_short indels (<= 5 bp)                2                                                                                           
#Mis_long indels (> 5 bp)                  0                                                                                           
#Una_fully unaligned contigs               0                                                                                           
Una_Fully unaligned length                 0                                                                                           
#Una_partially unaligned contigs           1                                                                                           
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             18                                                                                          
GAGE_Contigs #                             1                                                                                           
GAGE_Min contig                            803                                                                                         
GAGE_Max contig                            803                                                                                         
GAGE_N50                                   0 COUNT: 0                                                                                  
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         803                                                                                         
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               2813075(99.97%)                                                                             
GAGE_Missing assembly bases                18(2.24%)                                                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            0                                                                                           
GAGE_Compressed reference bases            0                                                                                           
GAGE_Bad trim                              18                                                                                          
GAGE_Avg idy                               99.75                                                                                       
GAGE_SNPs                                  0                                                                                           
GAGE_Indels < 5bp                          2                                                                                           
GAGE_Indels >= 5                           0                                                                                           
GAGE_Inversions                            0                                                                                           
GAGE_Relocation                            0                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    1                                                                                           
GAGE_Corrected assembly size               787                                                                                         
GAGE_Min correct contig                    787                                                                                         
GAGE_Max correct contig                    787                                                                                         
GAGE_Corrected N50                         0 COUNT: 0                                                                                  
