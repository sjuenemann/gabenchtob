All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_112-unitigs
#Contigs                                   2677                                                                    
#Contigs (>= 0 bp)                         4204                                                                    
#Contigs (>= 1000 bp)                      697                                                                     
Largest contig                             54664                                                                   
Total length                               5662352                                                                 
Total length (>= 0 bp)                     5886127                                                                 
Total length (>= 1000 bp)                  5114938                                                                 
Reference length                           5594470                                                                 
N50                                        11279                                                                   
NG50                                       11407                                                                   
N75                                        4870                                                                    
NG75                                       5170                                                                    
L50                                        145                                                                     
LG50                                       142                                                                     
L75                                        333                                                                     
LG75                                       323                                                                     
#local misassemblies                       6                                                                       
#misassemblies                             5                                                                       
#misassembled contigs                      5                                                                       
Misassembled contigs length                25977                                                                   
Misassemblies inter-contig overlap         1036                                                                    
#unaligned contigs                         1 + 6 part                                                              
Unaligned contigs length                   522                                                                     
#ambiguously mapped contigs                542                                                                     
Extra bases in ambiguously mapped contigs  -161843                                                                 
#N's                                       0                                                                       
#N's per 100 kbp                           0.00                                                                    
Genome fraction (%)                        94.427                                                                  
Duplication ratio                          1.041                                                                   
#genes                                     4447 + 722 part                                                         
#predicted genes (unique)                  7288                                                                    
#predicted genes (>= 0 bp)                 7333                                                                    
#predicted genes (>= 300 bp)               4675                                                                    
#predicted genes (>= 1500 bp)              586                                                                     
#predicted genes (>= 3000 bp)              47                                                                      
#mismatches                                76                                                                      
#indels                                    414                                                                     
Indels length                              424                                                                     
#mismatches per 100 kbp                    1.44                                                                    
#indels per 100 kbp                        7.84                                                                    
GC (%)                                     50.45                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               99.129                                                                  
Largest alignment                          54664                                                                   
NA50                                       11268                                                                   
NGA50                                      11341                                                                   
NA75                                       4870                                                                    
NGA75                                      5164                                                                    
LA50                                       146                                                                     
LGA50                                      143                                                                     
LA75                                       334                                                                     
LGA75                                      324                                                                     
#Mis_misassemblies                         5                                                                       
#Mis_relocations                           5                                                                       
#Mis_translocations                        0                                                                       
#Mis_inversions                            0                                                                       
#Mis_misassembled contigs                  5                                                                       
Mis_Misassembled contigs length            25977                                                                   
#Mis_local misassemblies                   6                                                                       
#Mis_short indels (<= 5 bp)                414                                                                     
#Mis_long indels (> 5 bp)                  0                                                                       
#Una_fully unaligned contigs               1                                                                       
Una_Fully unaligned length                 221                                                                     
#Una_partially unaligned contigs           6                                                                       
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             301                                                                     
GAGE_Contigs #                             2677                                                                    
GAGE_Min contig                            200                                                                     
GAGE_Max contig                            54664                                                                   
GAGE_N50                                   11407 COUNT: 142                                                        
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5662352                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               22360(0.40%)                                                            
GAGE_Missing assembly bases                330(0.01%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                
GAGE_Duplicated reference bases            199114                                                                  
GAGE_Compressed reference bases            275747                                                                  
GAGE_Bad trim                              330                                                                     
GAGE_Avg idy                               99.99                                                                   
GAGE_SNPs                                  46                                                                      
GAGE_Indels < 5bp                          426                                                                     
GAGE_Indels >= 5                           3                                                                       
GAGE_Inversions                            0                                                                       
GAGE_Relocation                            5                                                                       
GAGE_Translocation                         0                                                                       
GAGE_Corrected contig #                    1775                                                                    
GAGE_Corrected assembly size               5463363                                                                 
GAGE_Min correct contig                    200                                                                     
GAGE_Max correct contig                    54666                                                                   
GAGE_Corrected N50                         11279 COUNT: 143                                                        
