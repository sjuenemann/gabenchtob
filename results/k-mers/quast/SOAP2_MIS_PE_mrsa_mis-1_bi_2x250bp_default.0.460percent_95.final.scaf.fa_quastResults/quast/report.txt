All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_95.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_95.final.scaf
#Contigs (>= 0 bp)             1914                                                                          1866                                                                 
#Contigs (>= 1000 bp)          162                                                                           143                                                                  
Total length (>= 0 bp)         3048468                                                                       3049840                                                              
Total length (>= 1000 bp)      2748787                                                                       2759330                                                              
#Contigs                       256                                                                           217                                                                  
Largest contig                 73854                                                                         87215                                                                
Total length                   2783728                                                                       2786501                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         32.69                                                                         32.70                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            27634                                                                         32442                                                                
NG50                           27634                                                                         32259                                                                
N75                            15331                                                                         17947                                                                
NG75                           15089                                                                         17914                                                                
#misassemblies                 1                                                                             4                                                                    
#local misassemblies           13                                                                            39                                                                   
#unaligned contigs             8 + 2 part                                                                    9 + 1 part                                                           
Unaligned contigs length       8603                                                                          9853                                                                 
Genome fraction (%)            98.252                                                                        98.298                                                               
Duplication ratio              1.002                                                                         1.002                                                                
#N's per 100 kbp               3.66                                                                          52.90                                                                
#mismatches per 100 kbp        1.63                                                                          2.10                                                                 
#indels per 100 kbp            0.98                                                                          6.29                                                                 
#genes                         2551 + 143 part                                                               2560 + 132 part                                                      
#predicted genes (unique)      2790                                                                          2773                                                                 
#predicted genes (>= 0 bp)     2792                                                                          2775                                                                 
#predicted genes (>= 300 bp)   2334                                                                          2328                                                                 
#predicted genes (>= 1500 bp)  286                                                                           287                                                                  
#predicted genes (>= 3000 bp)  29                                                                            29                                                                   
Largest alignment              73854                                                                         87191                                                                
NA50                           27634                                                                         32442                                                                
NGA50                          27634                                                                         32201                                                                
NA75                           15275                                                                         17914                                                                
NGA75                          14955                                                                         17022                                                                
