All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_95.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_95.final.scaf
#Mis_misassemblies               1                                                                             4                                                                    
#Mis_relocations                 1                                                                             4                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        1                                                                             3                                                                    
Mis_Misassembled contigs length  24089                                                                         25266                                                                
#Mis_local misassemblies         13                                                                            39                                                                   
#mismatches                      45                                                                            58                                                                   
#indels                          27                                                                            174                                                                  
#Mis_short indels (<= 5 bp)      20                                                                            154                                                                  
#Mis_long indels (> 5 bp)        7                                                                             20                                                                   
Indels length                    184                                                                           637                                                                  
