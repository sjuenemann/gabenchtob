All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                       #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's  
SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_19.final.scaf_broken  87                            25512                       22                                0                      0                                1068                            579   
SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_19.final.scaf         439                           407165                      562                               35                     213                              784201                          811387
