All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_19.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_19.final.scaf
#Mis_misassemblies               1                                                                              2                                                                     
#Mis_relocations                 1                                                                              2                                                                     
#Mis_translocations              0                                                                              0                                                                     
#Mis_inversions                  0                                                                              0                                                                     
#Mis_misassembled contigs        1                                                                              2                                                                     
Mis_Misassembled contigs length  223                                                                            5964                                                                  
#Mis_local misassemblies         4                                                                              2411                                                                  
#mismatches                      195                                                                            127                                                                   
#indels                          301                                                                            2996                                                                  
#Mis_short indels (<= 5 bp)      281                                                                            2872                                                                  
#Mis_long indels (> 5 bp)        20                                                                             124                                                                   
Indels length                    491                                                                            5605                                                                  
