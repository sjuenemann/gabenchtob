All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_63.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_63.final.scaf
#Mis_misassemblies               1                                                                             2                                                                    
#Mis_relocations                 1                                                                             2                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        1                                                                             2                                                                    
Mis_Misassembled contigs length  19487                                                                         162553                                                               
#Mis_local misassemblies         1                                                                             54                                                                   
#mismatches                      35                                                                            36                                                                   
#indels                          17                                                                            1109                                                                 
#Mis_short indels (<= 5 bp)      11                                                                            1005                                                                 
#Mis_long indels (> 5 bp)        6                                                                             104                                                                  
Indels length                    189                                                                           3850                                                                 
