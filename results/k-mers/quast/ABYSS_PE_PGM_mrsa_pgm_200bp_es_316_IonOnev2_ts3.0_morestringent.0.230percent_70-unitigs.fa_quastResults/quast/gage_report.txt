All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_70-unitigs
GAGE_Contigs #                   513                                                                                    
GAGE_Min contig                  201                                                                                    
GAGE_Max contig                  41853                                                                                  
GAGE_N50                         11251 COUNT: 76                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2757275                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     40734(1.45%)                                                                           
GAGE_Missing assembly bases      9(0.00%)                                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  995                                                                                    
GAGE_Compressed reference bases  29761                                                                                  
GAGE_Bad trim                    9                                                                                      
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        67                                                                                     
GAGE_Indels < 5bp                345                                                                                    
GAGE_Indels >= 5                 2                                                                                      
GAGE_Inversions                  2                                                                                      
GAGE_Relocation                  2                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          516                                                                                    
GAGE_Corrected assembly size     2757262                                                                                
GAGE_Min correct contig          201                                                                                    
GAGE_Max correct contig          41860                                                                                  
GAGE_Corrected N50               10808 COUNT: 77                                                                        
