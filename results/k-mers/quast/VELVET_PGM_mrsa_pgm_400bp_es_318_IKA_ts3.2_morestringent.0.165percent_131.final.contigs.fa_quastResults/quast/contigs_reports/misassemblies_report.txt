All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_131.final.contigs
#Mis_misassemblies               292                                                                                    
#Mis_relocations                 263                                                                                    
#Mis_translocations              25                                                                                     
#Mis_inversions                  4                                                                                      
#Mis_misassembled contigs        292                                                                                    
Mis_Misassembled contigs length  92442                                                                                  
#Mis_local misassemblies         2                                                                                      
#mismatches                      1441                                                                                   
#indels                          17397                                                                                  
#Mis_short indels (<= 5 bp)      17397                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    17805                                                                                  
