All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K81.final_contigs
#Contigs                                   828                                                                                
#Contigs (>= 0 bp)                         888                                                                                
#Contigs (>= 1000 bp)                      148                                                                                
Largest contig                             182941                                                                             
Total length                               4481927                                                                            
Total length (>= 0 bp)                     4488704                                                                            
Total length (>= 1000 bp)                  4281993                                                                            
Reference length                           4411532                                                                            
N50                                        64024                                                                              
NG50                                       64024                                                                              
N75                                        31736                                                                              
NG75                                       32278                                                                              
L50                                        22                                                                                 
LG50                                       22                                                                                 
L75                                        47                                                                                 
LG75                                       46                                                                                 
#local misassemblies                       28                                                                                 
#misassemblies                             11                                                                                 
#misassembled contigs                      8                                                                                  
Misassembled contigs length                121556                                                                             
Misassemblies inter-contig overlap         23687                                                                              
#unaligned contigs                         254 + 14 part                                                                      
Unaligned contigs length                   70244                                                                              
#ambiguously mapped contigs                23                                                                                 
Extra bases in ambiguously mapped contigs  -16487                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.437                                                                             
Duplication ratio                          1.028                                                                              
#genes                                     3937 + 112 part                                                                    
#predicted genes (unique)                  4822                                                                               
#predicted genes (>= 0 bp)                 4826                                                                               
#predicted genes (>= 300 bp)               3774                                                                               
#predicted genes (>= 1500 bp)              468                                                                                
#predicted genes (>= 3000 bp)              50                                                                                 
#mismatches                                373                                                                                
#indels                                    1396                                                                               
Indels length                              1730                                                                               
#mismatches per 100 kbp                    8.68                                                                               
#indels per 100 kbp                        32.48                                                                              
GC (%)                                     65.37                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.062                                                                             
Largest alignment                          182941                                                                             
NA50                                       63317                                                                              
NGA50                                      64024                                                                              
NA75                                       31505                                                                              
NGA75                                      31736                                                                              
LA50                                       23                                                                                 
LGA50                                      22                                                                                 
LA75                                       48                                                                                 
LGA75                                      47                                                                                 
#Mis_misassemblies                         11                                                                                 
#Mis_relocations                           11                                                                                 
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  8                                                                                  
Mis_Misassembled contigs length            121556                                                                             
#Mis_local misassemblies                   28                                                                                 
#Mis_short indels (<= 5 bp)                1390                                                                               
#Mis_long indels (> 5 bp)                  6                                                                                  
#Una_fully unaligned contigs               254                                                                                
Una_Fully unaligned length                 69457                                                                              
#Una_partially unaligned contigs           14                                                                                 
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             787                                                                                
GAGE_Contigs #                             828                                                                                
GAGE_Min contig                            208                                                                                
GAGE_Max contig                            182941                                                                             
GAGE_N50                                   64024 COUNT: 22                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4481927                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               50967(1.16%)                                                                       
GAGE_Missing assembly bases                23909(0.53%)                                                                       
GAGE_Missing assembly contigs              67(8.09%)                                                                          
GAGE_Duplicated reference bases            144387                                                                             
GAGE_Compressed reference bases            58085                                                                              
GAGE_Bad trim                              5783                                                                               
GAGE_Avg idy                               99.96                                                                              
GAGE_SNPs                                  286                                                                                
GAGE_Indels < 5bp                          1414                                                                               
GAGE_Indels >= 5                           22                                                                                 
GAGE_Inversions                            3                                                                                  
GAGE_Relocation                            15                                                                                 
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    257                                                                                
GAGE_Corrected assembly size               4318526                                                                            
GAGE_Min correct contig                    208                                                                                
GAGE_Max correct contig                    182973                                                                             
GAGE_Corrected N50                         52843 COUNT: 27                                                                    
