All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_44-unitigs
#Contigs                                   537                                                                               
#Contigs (>= 0 bp)                         6723                                                                              
#Contigs (>= 1000 bp)                      359                                                                               
Largest contig                             46254                                                                             
Total length                               2726787                                                                           
Total length (>= 0 bp)                     3138549                                                                           
Total length (>= 1000 bp)                  2645215                                                                           
Reference length                           2813862                                                                           
N50                                        11919                                                                             
NG50                                       11661                                                                             
N75                                        6069                                                                              
NG75                                       5372                                                                              
L50                                        74                                                                                
LG50                                       78                                                                                
L75                                        152                                                                               
LG75                                       164                                                                               
#local misassemblies                       4                                                                                 
#misassemblies                             1                                                                                 
#misassembled contigs                      1                                                                                 
Misassembled contigs length                3462                                                                              
Misassemblies inter-contig overlap         675                                                                               
#unaligned contigs                         0 + 0 part                                                                        
Unaligned contigs length                   0                                                                                 
#ambiguously mapped contigs                4                                                                                 
Extra bases in ambiguously mapped contigs  -1651                                                                             
#N's                                       0                                                                                 
#N's per 100 kbp                           0.00                                                                              
Genome fraction (%)                        96.757                                                                            
Duplication ratio                          1.001                                                                             
#genes                                     2334 + 292 part                                                                   
#predicted genes (unique)                  2872                                                                              
#predicted genes (>= 0 bp)                 2872                                                                              
#predicted genes (>= 300 bp)               2369                                                                              
#predicted genes (>= 1500 bp)              262                                                                               
#predicted genes (>= 3000 bp)              20                                                                                
#mismatches                                77                                                                                
#indels                                    90                                                                                
Indels length                              95                                                                                
#mismatches per 100 kbp                    2.83                                                                              
#indels per 100 kbp                        3.31                                                                              
GC (%)                                     32.58                                                                             
Reference GC (%)                           32.81                                                                             
Average %IDY                               99.948                                                                            
Largest alignment                          46254                                                                             
NA50                                       11919                                                                             
NGA50                                      11661                                                                             
NA75                                       6069                                                                              
NGA75                                      5372                                                                              
LA50                                       74                                                                                
LGA50                                      78                                                                                
LA75                                       152                                                                               
LGA75                                      164                                                                               
#Mis_misassemblies                         1                                                                                 
#Mis_relocations                           1                                                                                 
#Mis_translocations                        0                                                                                 
#Mis_inversions                            0                                                                                 
#Mis_misassembled contigs                  1                                                                                 
Mis_Misassembled contigs length            3462                                                                              
#Mis_local misassemblies                   4                                                                                 
#Mis_short indels (<= 5 bp)                90                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                 
#Una_fully unaligned contigs               0                                                                                 
Una_Fully unaligned length                 0                                                                                 
#Una_partially unaligned contigs           0                                                                                 
#Una_with misassembly                      0                                                                                 
#Una_both parts are significant            0                                                                                 
Una_Partially unaligned length             0                                                                                 
GAGE_Contigs #                             537                                                                               
GAGE_Min contig                            200                                                                               
GAGE_Max contig                            46254                                                                             
GAGE_N50                                   11661 COUNT: 78                                                                   
GAGE_Genome size                           2813862                                                                           
GAGE_Assembly size                         2726787                                                                           
GAGE_Chaff bases                           0                                                                                 
GAGE_Missing reference bases               84030(2.99%)                                                                      
GAGE_Missing assembly bases                10(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                          
GAGE_Duplicated reference bases            0                                                                                 
GAGE_Compressed reference bases            6437                                                                              
GAGE_Bad trim                              10                                                                                
GAGE_Avg idy                               99.99                                                                             
GAGE_SNPs                                  46                                                                                
GAGE_Indels < 5bp                          88                                                                                
GAGE_Indels >= 5                           5                                                                                 
GAGE_Inversions                            0                                                                                 
GAGE_Relocation                            1                                                                                 
GAGE_Translocation                         0                                                                                 
GAGE_Corrected contig #                    542                                                                               
GAGE_Corrected assembly size               2727521                                                                           
GAGE_Min correct contig                    200                                                                               
GAGE_Max correct contig                    46257                                                                             
GAGE_Corrected N50                         11664 COUNT: 78                                                                   
