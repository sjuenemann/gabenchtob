All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_159.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_159.final.contigs
#Mis_misassemblies               6                                                                                        13                                                                              
#Mis_relocations                 6                                                                                        13                                                                              
#Mis_translocations              0                                                                                        0                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        6                                                                                        12                                                                              
Mis_Misassembled contigs length  112502                                                                                   358260                                                                          
#Mis_local misassemblies         1                                                                                        93                                                                              
#mismatches                      135                                                                                      143                                                                             
#indels                          19                                                                                       276                                                                             
#Mis_short indels (<= 5 bp)      19                                                                                       273                                                                             
#Mis_long indels (> 5 bp)        0                                                                                        3                                                                               
Indels length                    19                                                                                       465                                                                             
