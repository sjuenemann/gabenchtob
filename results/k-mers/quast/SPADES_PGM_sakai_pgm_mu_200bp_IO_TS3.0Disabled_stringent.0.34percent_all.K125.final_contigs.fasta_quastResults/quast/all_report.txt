All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K125.final_contigs
#Contigs                                   1109                                                                                       
#Contigs (>= 0 bp)                         1155                                                                                       
#Contigs (>= 1000 bp)                      173                                                                                        
Largest contig                             184081                                                                                     
Total length                               5602809                                                                                    
Total length (>= 0 bp)                     5609554                                                                                    
Total length (>= 1000 bp)                  5314544                                                                                    
Reference length                           5594470                                                                                    
N50                                        75313                                                                                      
NG50                                       75313                                                                                      
N75                                        35000                                                                                      
NG75                                       35000                                                                                      
L50                                        24                                                                                         
LG50                                       24                                                                                         
L75                                        51                                                                                         
LG75                                       51                                                                                         
#local misassemblies                       13                                                                                         
#misassemblies                             79                                                                                         
#misassembled contigs                      75                                                                                         
Misassembled contigs length                312492                                                                                     
Misassemblies inter-contig overlap         12892                                                                                      
#unaligned contigs                         6 + 130 part                                                                               
Unaligned contigs length                   6235                                                                                       
#ambiguously mapped contigs                57                                                                                         
Extra bases in ambiguously mapped contigs  -41028                                                                                     
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        94.769                                                                                     
Duplication ratio                          1.050                                                                                      
#genes                                     4993 + 146 part                                                                            
#predicted genes (unique)                  6709                                                                                       
#predicted genes (>= 0 bp)                 6725                                                                                       
#predicted genes (>= 300 bp)               4865                                                                                       
#predicted genes (>= 1500 bp)              509                                                                                        
#predicted genes (>= 3000 bp)              27                                                                                         
#mismatches                                1154                                                                                       
#indels                                    2094                                                                                       
Indels length                              2207                                                                                       
#mismatches per 100 kbp                    21.77                                                                                      
#indels per 100 kbp                        39.50                                                                                      
GC (%)                                     50.24                                                                                      
Reference GC (%)                           50.48                                                                                      
Average %IDY                               98.576                                                                                     
Largest alignment                          184081                                                                                     
NA50                                       74865                                                                                      
NGA50                                      74865                                                                                      
NA75                                       32620                                                                                      
NGA75                                      32620                                                                                      
LA50                                       24                                                                                         
LGA50                                      24                                                                                         
LA75                                       52                                                                                         
LGA75                                      52                                                                                         
#Mis_misassemblies                         79                                                                                         
#Mis_relocations                           23                                                                                         
#Mis_translocations                        0                                                                                          
#Mis_inversions                            56                                                                                         
#Mis_misassembled contigs                  75                                                                                         
Mis_Misassembled contigs length            312492                                                                                     
#Mis_local misassemblies                   13                                                                                         
#Mis_short indels (<= 5 bp)                2091                                                                                       
#Mis_long indels (> 5 bp)                  3                                                                                          
#Una_fully unaligned contigs               6                                                                                          
Una_Fully unaligned length                 1743                                                                                       
#Una_partially unaligned contigs           130                                                                                        
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             4492                                                                                       
GAGE_Contigs #                             1109                                                                                       
GAGE_Min contig                            227                                                                                        
GAGE_Max contig                            184081                                                                                     
GAGE_N50                                   75313 COUNT: 24                                                                            
GAGE_Genome size                           5594470                                                                                    
GAGE_Assembly size                         5602809                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               19484(0.35%)                                                                               
GAGE_Missing assembly bases                6227(0.11%)                                                                                
GAGE_Missing assembly contigs              1(0.09%)                                                                                   
GAGE_Duplicated reference bases            266063                                                                                     
GAGE_Compressed reference bases            278025                                                                                     
GAGE_Bad trim                              5892                                                                                       
GAGE_Avg idy                               99.93                                                                                      
GAGE_SNPs                                  521                                                                                        
GAGE_Indels < 5bp                          1984                                                                                       
GAGE_Indels >= 5                           9                                                                                          
GAGE_Inversions                            8                                                                                          
GAGE_Relocation                            13                                                                                         
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    244                                                                                        
GAGE_Corrected assembly size               5352928                                                                                    
GAGE_Min correct contig                    203                                                                                        
GAGE_Max correct contig                    162342                                                                                     
GAGE_Corrected N50                         71270 COUNT: 25                                                                            
