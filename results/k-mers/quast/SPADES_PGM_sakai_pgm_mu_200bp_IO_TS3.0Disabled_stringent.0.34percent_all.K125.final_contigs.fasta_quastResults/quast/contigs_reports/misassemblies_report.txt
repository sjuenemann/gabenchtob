All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K125.final_contigs
#Mis_misassemblies               79                                                                                         
#Mis_relocations                 23                                                                                         
#Mis_translocations              0                                                                                          
#Mis_inversions                  56                                                                                         
#Mis_misassembled contigs        75                                                                                         
Mis_Misassembled contigs length  312492                                                                                     
#Mis_local misassemblies         13                                                                                         
#mismatches                      1154                                                                                       
#indels                          2094                                                                                       
#Mis_short indels (<= 5 bp)      2091                                                                                       
#Mis_long indels (> 5 bp)        3                                                                                          
Indels length                    2207                                                                                       
