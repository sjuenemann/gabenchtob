All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_79.final.contig
GAGE_Contigs #                   6147                                                                                     
GAGE_Min contig                  200                                                                                      
GAGE_Max contig                  6088                                                                                     
GAGE_N50                         956 COUNT: 914                                                                           
GAGE_Genome size                 2813862                                                                                  
GAGE_Assembly size               3352958                                                                                  
GAGE_Chaff bases                 0                                                                                        
GAGE_Missing reference bases     104252(3.70%)                                                                            
GAGE_Missing assembly bases      144133(4.30%)                                                                            
GAGE_Missing assembly contigs    361(5.87%)                                                                               
GAGE_Duplicated reference bases  417953                                                                                   
GAGE_Compressed reference bases  33755                                                                                    
GAGE_Bad trim                    53174                                                                                    
GAGE_Avg idy                     99.89                                                                                    
GAGE_SNPs                        146                                                                                      
GAGE_Indels < 5bp                1472                                                                                     
GAGE_Indels >= 5                 3                                                                                        
GAGE_Inversions                  9                                                                                        
GAGE_Relocation                  1                                                                                        
GAGE_Translocation               0                                                                                        
GAGE_Corrected contig #          3868                                                                                     
GAGE_Corrected assembly size     2778571                                                                                  
GAGE_Min correct contig          200                                                                                      
GAGE_Max correct contig          6088                                                                                     
GAGE_Corrected N50               952 COUNT: 916                                                                           
