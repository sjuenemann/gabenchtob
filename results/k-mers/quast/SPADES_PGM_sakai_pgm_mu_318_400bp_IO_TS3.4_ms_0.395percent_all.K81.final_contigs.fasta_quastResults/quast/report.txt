All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K81.final_contigs
#Contigs (>= 0 bp)             1684                                                                            
#Contigs (>= 1000 bp)          185                                                                             
Total length (>= 0 bp)         5625816                                                                         
Total length (>= 1000 bp)      5232588                                                                         
#Contigs                       1334                                                                            
Largest contig                 307450                                                                          
Total length                   5583707                                                                         
Reference length               5594470                                                                         
GC (%)                         50.19                                                                           
Reference GC (%)               50.48                                                                           
N50                            106945                                                                          
NG50                           106945                                                                          
N75                            36886                                                                           
NG75                           36886                                                                           
#misassemblies                 6                                                                               
#local misassemblies           15                                                                              
#unaligned contigs             297 + 11 part                                                                   
Unaligned contigs length       79529                                                                           
Genome fraction (%)            94.069                                                                          
Duplication ratio              1.029                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        4.12                                                                            
#indels per 100 kbp            8.61                                                                            
#genes                         4895 + 219 part                                                                 
#predicted genes (unique)      6108                                                                            
#predicted genes (>= 0 bp)     6114                                                                            
#predicted genes (>= 300 bp)   4541                                                                            
#predicted genes (>= 1500 bp)  638                                                                             
#predicted genes (>= 3000 bp)  62                                                                              
Largest alignment              307450                                                                          
NA50                           106945                                                                          
NGA50                          106945                                                                          
NA75                           35776                                                                           
NGA75                          35776                                                                           
