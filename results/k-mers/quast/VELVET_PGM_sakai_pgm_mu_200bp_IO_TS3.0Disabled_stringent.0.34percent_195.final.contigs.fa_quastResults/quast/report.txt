All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_195.final.contigs
#Contigs (>= 0 bp)             122                                                                                   
#Contigs (>= 1000 bp)          3                                                                                     
Total length (>= 0 bp)         82992                                                                                 
Total length (>= 1000 bp)      3984                                                                                  
#Contigs                       122                                                                                   
Largest contig                 1966                                                                                  
Total length                   82992                                                                                 
Reference length               5594470                                                                               
GC (%)                         52.64                                                                                 
Reference GC (%)               50.48                                                                                 
N50                            650                                                                                   
NG50                           None                                                                                  
N75                            610                                                                                   
NG75                           None                                                                                  
#misassemblies                 1                                                                                     
#local misassemblies           0                                                                                     
#unaligned contigs             0 + 4 part                                                                            
Unaligned contigs length       92                                                                                    
Genome fraction (%)            1.221                                                                                 
Duplication ratio              1.000                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        1.46                                                                                  
#indels per 100 kbp            39.53                                                                                 
#genes                         6 + 120 part                                                                          
#predicted genes (unique)      154                                                                                   
#predicted genes (>= 0 bp)     154                                                                                   
#predicted genes (>= 300 bp)   114                                                                                   
#predicted genes (>= 1500 bp)  0                                                                                     
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              915                                                                                   
NA50                           619                                                                                   
NGA50                          None                                                                                  
NA75                           591                                                                                   
NGA75                          None                                                                                  
