All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K99.final_contigs
#Contigs                                   3044                                                                            
#Contigs (>= 0 bp)                         3289                                                                            
#Contigs (>= 1000 bp)                      167                                                                             
Largest contig                             352309                                                                          
Total length                               6129353                                                                         
Total length (>= 0 bp)                     6163411                                                                         
Total length (>= 1000 bp)                  5261961                                                                         
Reference length                           5594470                                                                         
N50                                        124345                                                                          
NG50                                       140539                                                                          
N75                                        29908                                                                           
NG75                                       44319                                                                           
L50                                        16                                                                              
LG50                                       14                                                                              
L75                                        42                                                                              
LG75                                       31                                                                              
#local misassemblies                       14                                                                              
#misassemblies                             31                                                                              
#misassembled contigs                      31                                                                              
Misassembled contigs length                360659                                                                          
Misassemblies inter-contig overlap         3561                                                                            
#unaligned contigs                         325 + 44 part                                                                   
Unaligned contigs length                   96056                                                                           
#ambiguously mapped contigs                182                                                                             
Extra bases in ambiguously mapped contigs  -99920                                                                          
#N's                                       0                                                                               
#N's per 100 kbp                           0.00                                                                            
Genome fraction (%)                        94.477                                                                          
Duplication ratio                          1.123                                                                           
#genes                                     4930 + 210 part                                                                 
#predicted genes (unique)                  7603                                                                            
#predicted genes (>= 0 bp)                 7623                                                                            
#predicted genes (>= 300 bp)               4591                                                                            
#predicted genes (>= 1500 bp)              636                                                                             
#predicted genes (>= 3000 bp)              60                                                                              
#mismatches                                302                                                                             
#indels                                    601                                                                             
Indels length                              736                                                                             
#mismatches per 100 kbp                    5.71                                                                            
#indels per 100 kbp                        11.37                                                                           
GC (%)                                     49.98                                                                           
Reference GC (%)                           50.48                                                                           
Average %IDY                               98.328                                                                          
Largest alignment                          352309                                                                          
NA50                                       124345                                                                          
NGA50                                      140539                                                                          
NA75                                       29908                                                                           
NGA75                                      44319                                                                           
LA50                                       17                                                                              
LGA50                                      15                                                                              
LA75                                       43                                                                              
LGA75                                      32                                                                              
#Mis_misassemblies                         31                                                                              
#Mis_relocations                           29                                                                              
#Mis_translocations                        2                                                                               
#Mis_inversions                            0                                                                               
#Mis_misassembled contigs                  31                                                                              
Mis_Misassembled contigs length            360659                                                                          
#Mis_local misassemblies                   14                                                                              
#Mis_short indels (<= 5 bp)                593                                                                             
#Mis_long indels (> 5 bp)                  8                                                                               
#Una_fully unaligned contigs               325                                                                             
Una_Fully unaligned length                 92955                                                                           
#Una_partially unaligned contigs           44                                                                              
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            0                                                                               
Una_Partially unaligned length             3101                                                                            
GAGE_Contigs #                             3044                                                                            
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            352309                                                                          
GAGE_N50                                   140539 COUNT: 14                                                                
GAGE_Genome size                           5594470                                                                         
GAGE_Assembly size                         6129353                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               7200(0.13%)                                                                     
GAGE_Missing assembly bases                32423(0.53%)                                                                    
GAGE_Missing assembly contigs              75(2.46%)                                                                       
GAGE_Duplicated reference bases            721858                                                                          
GAGE_Compressed reference bases            282601                                                                          
GAGE_Bad trim                              10248                                                                           
GAGE_Avg idy                               99.98                                                                           
GAGE_SNPs                                  181                                                                             
GAGE_Indels < 5bp                          482                                                                             
GAGE_Indels >= 5                           11                                                                              
GAGE_Inversions                            0                                                                               
GAGE_Relocation                            11                                                                              
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    440                                                                             
GAGE_Corrected assembly size               5378199                                                                         
GAGE_Min correct contig                    201                                                                             
GAGE_Max correct contig                    238374                                                                          
GAGE_Corrected N50                         111195 COUNT: 17                                                                
