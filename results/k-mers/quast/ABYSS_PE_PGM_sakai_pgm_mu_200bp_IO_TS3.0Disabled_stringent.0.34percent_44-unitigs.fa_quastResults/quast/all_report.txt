All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_44-unitigs
#Contigs                                   1214                                                                             
#Contigs (>= 0 bp)                         7429                                                                             
#Contigs (>= 1000 bp)                      761                                                                              
Largest contig                             63914                                                                            
Total length                               5210517                                                                          
Total length (>= 0 bp)                     5700395                                                                          
Total length (>= 1000 bp)                  5013497                                                                          
Reference length                           5594470                                                                          
N50                                        9822                                                                             
NG50                                       9231                                                                             
N75                                        4982                                                                             
NG75                                       4181                                                                             
L50                                        156                                                                              
LG50                                       176                                                                              
L75                                        336                                                                              
LG75                                       399                                                                              
#local misassemblies                       4                                                                                
#misassemblies                             2                                                                                
#misassembled contigs                      2                                                                                
Misassembled contigs length                12775                                                                            
Misassemblies inter-contig overlap         347                                                                              
#unaligned contigs                         0 + 0 part                                                                       
Unaligned contigs length                   0                                                                                
#ambiguously mapped contigs                121                                                                              
Extra bases in ambiguously mapped contigs  -58623                                                                           
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        92.021                                                                           
Duplication ratio                          1.001                                                                            
#genes                                     4239 + 704 part                                                                  
#predicted genes (unique)                  6168                                                                             
#predicted genes (>= 0 bp)                 6168                                                                             
#predicted genes (>= 300 bp)               4754                                                                             
#predicted genes (>= 1500 bp)              510                                                                              
#predicted genes (>= 3000 bp)              38                                                                               
#mismatches                                78                                                                               
#indels                                    1128                                                                             
Indels length                              1166                                                                             
#mismatches per 100 kbp                    1.52                                                                             
#indels per 100 kbp                        21.91                                                                            
GC (%)                                     50.20                                                                            
Reference GC (%)                           50.48                                                                            
Average %IDY                               99.896                                                                           
Largest alignment                          63914                                                                            
NA50                                       9822                                                                             
NGA50                                      9231                                                                             
NA75                                       4982                                                                             
NGA75                                      4179                                                                             
LA50                                       156                                                                              
LGA50                                      176                                                                              
LA75                                       336                                                                              
LGA75                                      400                                                                              
#Mis_misassemblies                         2                                                                                
#Mis_relocations                           2                                                                                
#Mis_translocations                        0                                                                                
#Mis_inversions                            0                                                                                
#Mis_misassembled contigs                  2                                                                                
Mis_Misassembled contigs length            12775                                                                            
#Mis_local misassemblies                   4                                                                                
#Mis_short indels (<= 5 bp)                1128                                                                             
#Mis_long indels (> 5 bp)                  0                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           0                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             0                                                                                
GAGE_Contigs #                             1214                                                                             
GAGE_Min contig                            200                                                                              
GAGE_Max contig                            63914                                                                            
GAGE_N50                                   9231 COUNT: 176                                                                  
GAGE_Genome size                           5594470                                                                          
GAGE_Assembly size                         5210517                                                                          
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               285060(5.10%)                                                                    
GAGE_Missing assembly bases                2(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            201                                                                              
GAGE_Compressed reference bases            107548                                                                           
GAGE_Bad trim                              2                                                                                
GAGE_Avg idy                               99.98                                                                            
GAGE_SNPs                                  107                                                                              
GAGE_Indels < 5bp                          1168                                                                             
GAGE_Indels >= 5                           4                                                                                
GAGE_Inversions                            0                                                                                
GAGE_Relocation                            2                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    1219                                                                             
GAGE_Corrected assembly size               5211808                                                                          
GAGE_Min correct contig                    200                                                                              
GAGE_Max correct contig                    63930                                                                            
GAGE_Corrected N50                         9103 COUNT: 177                                                                  
