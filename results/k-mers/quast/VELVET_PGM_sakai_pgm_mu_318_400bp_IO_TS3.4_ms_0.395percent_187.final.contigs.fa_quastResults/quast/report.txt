All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_187.final.contigs
#Contigs (>= 0 bp)             1932                                                                        
#Contigs (>= 1000 bp)          1334                                                                        
Total length (>= 0 bp)         4594895                                                                     
Total length (>= 1000 bp)      4200807                                                                     
#Contigs                       1932                                                                        
Largest contig                 16454                                                                       
Total length                   4594895                                                                     
Reference length               5594470                                                                     
GC (%)                         51.00                                                                       
Reference GC (%)               50.48                                                                       
N50                            3680                                                                        
NG50                           2891                                                                        
N75                            2011                                                                        
NG75                           1004                                                                        
#misassemblies                 60                                                                          
#local misassemblies           36                                                                          
#unaligned contigs             0 + 0 part                                                                  
Unaligned contigs length       0                                                                           
Genome fraction (%)            78.730                                                                      
Duplication ratio              1.026                                                                       
#N's per 100 kbp               15.02                                                                       
#mismatches per 100 kbp        1.43                                                                        
#indels per 100 kbp            19.96                                                                       
#genes                         2977 + 1549 part                                                            
#predicted genes (unique)      5916                                                                        
#predicted genes (>= 0 bp)     5943                                                                        
#predicted genes (>= 300 bp)   4425                                                                        
#predicted genes (>= 1500 bp)  375                                                                         
#predicted genes (>= 3000 bp)  26                                                                          
Largest alignment              16454                                                                       
NA50                           3665                                                                        
NGA50                          2842                                                                        
NA75                           1964                                                                        
NGA75                          959                                                                         
