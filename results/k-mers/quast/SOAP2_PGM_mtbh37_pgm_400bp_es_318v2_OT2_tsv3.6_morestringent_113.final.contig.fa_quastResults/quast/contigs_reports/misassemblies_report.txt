All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_113.final.contig
#Mis_misassemblies               39                                                                           
#Mis_relocations                 38                                                                           
#Mis_translocations              0                                                                            
#Mis_inversions                  1                                                                            
#Mis_misassembled contigs        39                                                                           
Mis_Misassembled contigs length  12163                                                                        
#Mis_local misassemblies         5                                                                            
#mismatches                      340                                                                          
#indels                          10727                                                                        
#Mis_short indels (<= 5 bp)      10727                                                                        
#Mis_long indels (> 5 bp)        0                                                                            
Indels length                    10972                                                                        
