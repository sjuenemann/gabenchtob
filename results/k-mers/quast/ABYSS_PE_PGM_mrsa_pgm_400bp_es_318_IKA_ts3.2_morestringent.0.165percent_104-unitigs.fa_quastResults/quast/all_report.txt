All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_104-unitigs
#Contigs                                   1215                                                                               
#Contigs (>= 0 bp)                         3100                                                                               
#Contigs (>= 1000 bp)                      321                                                                                
Largest contig                             52482                                                                              
Total length                               2915951                                                                            
Total length (>= 0 bp)                     3162228                                                                            
Total length (>= 1000 bp)                  2698829                                                                            
Reference length                           2813862                                                                            
N50                                        11945                                                                              
NG50                                       12140                                                                              
N75                                        6335                                                                               
NG75                                       6906                                                                               
L50                                        72                                                                                 
LG50                                       67                                                                                 
L75                                        155                                                                                
LG75                                       143                                                                                
#local misassemblies                       2                                                                                  
#misassemblies                             5                                                                                  
#misassembled contigs                      5                                                                                  
Misassembled contigs length                20329                                                                              
Misassemblies inter-contig overlap         291                                                                                
#unaligned contigs                         0 + 9 part                                                                         
Unaligned contigs length                   345                                                                                
#ambiguously mapped contigs                132                                                                                
Extra bases in ambiguously mapped contigs  -28488                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        98.269                                                                             
Duplication ratio                          1.044                                                                              
#genes                                     2451 + 246 part                                                                    
#predicted genes (unique)                  3520                                                                               
#predicted genes (>= 0 bp)                 3539                                                                               
#predicted genes (>= 300 bp)               2374                                                                               
#predicted genes (>= 1500 bp)              257                                                                                
#predicted genes (>= 3000 bp)              27                                                                                 
#mismatches                                30                                                                                 
#indels                                    179                                                                                
Indels length                              181                                                                                
#mismatches per 100 kbp                    1.08                                                                               
#indels per 100 kbp                        6.47                                                                               
GC (%)                                     32.75                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               99.271                                                                             
Largest alignment                          52482                                                                              
NA50                                       11945                                                                              
NGA50                                      12105                                                                              
NA75                                       6183                                                                               
NGA75                                      6883                                                                               
LA50                                       72                                                                                 
LGA50                                      68                                                                                 
LA75                                       156                                                                                
LGA75                                      144                                                                                
#Mis_misassemblies                         5                                                                                  
#Mis_relocations                           5                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  5                                                                                  
Mis_Misassembled contigs length            20329                                                                              
#Mis_local misassemblies                   2                                                                                  
#Mis_short indels (<= 5 bp)                179                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               0                                                                                  
Una_Fully unaligned length                 0                                                                                  
#Una_partially unaligned contigs           9                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             345                                                                                
GAGE_Contigs #                             1215                                                                               
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            52482                                                                              
GAGE_N50                                   12140 COUNT: 67                                                                    
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2915951                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               9705(0.34%)                                                                        
GAGE_Missing assembly bases                375(0.01%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            114757                                                                             
GAGE_Compressed reference bases            39373                                                                              
GAGE_Bad trim                              375                                                                                
GAGE_Avg idy                               99.99                                                                              
GAGE_SNPs                                  31                                                                                 
GAGE_Indels < 5bp                          216                                                                                
GAGE_Indels >= 5                           2                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            1                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    648                                                                                
GAGE_Corrected assembly size               2800159                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    52484                                                                              
GAGE_Corrected N50                         12094 COUNT: 69                                                                    
