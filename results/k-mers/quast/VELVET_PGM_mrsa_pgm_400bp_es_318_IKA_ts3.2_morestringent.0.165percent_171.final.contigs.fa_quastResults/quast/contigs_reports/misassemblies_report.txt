All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_171.final.contigs
#Mis_misassemblies               35                                                                                     
#Mis_relocations                 33                                                                                     
#Mis_translocations              2                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        35                                                                                     
Mis_Misassembled contigs length  35714                                                                                  
#Mis_local misassemblies         1                                                                                      
#mismatches                      52                                                                                     
#indels                          567                                                                                    
#Mis_short indels (<= 5 bp)      567                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    585                                                                                    
