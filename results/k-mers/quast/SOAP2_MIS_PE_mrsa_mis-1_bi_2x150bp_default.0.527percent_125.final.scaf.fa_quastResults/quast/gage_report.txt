All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_125.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_125.final.scaf
GAGE_Contigs #                   1893                                                                           751                                                                   
GAGE_Min contig                  200                                                                            200                                                                   
GAGE_Max contig                  13581                                                                          63710                                                                 
GAGE_N50                         1987 COUNT: 396                                                                6732 COUNT: 116                                                       
GAGE_Genome size                 2813862                                                                        2813862                                                               
GAGE_Assembly size               2679154                                                                        2712085                                                               
GAGE_Chaff bases                 0                                                                              0                                                                     
GAGE_Missing reference bases     135429(4.81%)                                                                  132783(4.72%)                                                         
GAGE_Missing assembly bases      6169(0.23%)                                                                    36242(1.34%)                                                          
GAGE_Missing assembly contigs    1(0.05%)                                                                       1(0.13%)                                                              
GAGE_Duplicated reference bases  2926                                                                           2926                                                                  
GAGE_Compressed reference bases  44005                                                                          43754                                                                 
GAGE_Bad trim                    13                                                                             38                                                                    
GAGE_Avg idy                     99.98                                                                          99.84                                                                 
GAGE_SNPs                        31                                                                             31                                                                    
GAGE_Indels < 5bp                216                                                                            742                                                                   
GAGE_Indels >= 5                 180                                                                            1118                                                                  
GAGE_Inversions                  0                                                                              0                                                                     
GAGE_Relocation                  2                                                                              5                                                                     
GAGE_Translocation               0                                                                              0                                                                     
GAGE_Corrected contig #          2065                                                                           1875                                                                  
GAGE_Corrected assembly size     2670580                                                                        2673226                                                               
GAGE_Min correct contig          206                                                                            206                                                                   
GAGE_Max correct contig          13581                                                                          26794                                                                 
GAGE_Corrected N50               1740 COUNT: 449                                                                1981 COUNT: 396                                                       
