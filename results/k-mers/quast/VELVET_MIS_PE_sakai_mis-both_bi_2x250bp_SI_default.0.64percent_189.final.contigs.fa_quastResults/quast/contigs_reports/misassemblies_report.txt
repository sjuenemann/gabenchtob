All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_189.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_189.final.contigs
#Mis_misassemblies               5                                                                                        7                                                                               
#Mis_relocations                 4                                                                                        6                                                                               
#Mis_translocations              1                                                                                        1                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        5                                                                                        7                                                                               
Mis_Misassembled contigs length  45707                                                                                    72787                                                                           
#Mis_local misassemblies         3                                                                                        266                                                                             
#mismatches                      162                                                                                      168                                                                             
#indels                          18                                                                                       883                                                                             
#Mis_short indels (<= 5 bp)      18                                                                                       860                                                                             
#Mis_long indels (> 5 bp)        0                                                                                        23                                                                              
Indels length                    18                                                                                       2069                                                                            
