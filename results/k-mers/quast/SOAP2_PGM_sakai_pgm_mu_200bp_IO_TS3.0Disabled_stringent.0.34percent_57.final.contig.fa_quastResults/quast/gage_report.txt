All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_57.final.contig
GAGE_Contigs #                   10049                                                                              
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  1406                                                                               
GAGE_N50                         228 COUNT: 7618                                                                    
GAGE_Genome size                 5594470                                                                            
GAGE_Assembly size               3312303                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     2367275(42.31%)                                                                    
GAGE_Missing assembly bases      30028(0.91%)                                                                       
GAGE_Missing assembly contigs    102(1.02%)                                                                         
GAGE_Duplicated reference bases  45888                                                                              
GAGE_Compressed reference bases  36863                                                                              
GAGE_Bad trim                    8048                                                                               
GAGE_Avg idy                     99.90                                                                              
GAGE_SNPs                        199                                                                                
GAGE_Indels < 5bp                2273                                                                               
GAGE_Indels >= 5                 3                                                                                  
GAGE_Inversions                  5                                                                                  
GAGE_Relocation                  1                                                                                  
GAGE_Translocation               1                                                                                  
GAGE_Corrected contig #          9578                                                                               
GAGE_Corrected assembly size     3211824                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          1406                                                                               
GAGE_Corrected N50               226 COUNT: 7622                                                                    
