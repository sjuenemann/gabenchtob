All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_112-unitigs
#Contigs                                   7820                                                                       
#Contigs (>= 0 bp)                         10700                                                                      
#Contigs (>= 1000 bp)                      1387                                                                       
Largest contig                             14879                                                                      
Total length                               5252914                                                                    
Total length (>= 0 bp)                     5690635                                                                    
Total length (>= 1000 bp)                  3467339                                                                    
Reference length                           4411532                                                                    
N50                                        1853                                                                       
NG50                                       2305                                                                       
N75                                        452                                                                        
NG75                                       1142                                                                       
L50                                        776                                                                        
LG50                                       572                                                                        
L75                                        2061                                                                       
LG75                                       1240                                                                       
#local misassemblies                       10                                                                         
#misassemblies                             7                                                                          
#misassembled contigs                      7                                                                          
Misassembled contigs length                4531                                                                       
Misassemblies inter-contig overlap         1222                                                                       
#unaligned contigs                         4 + 1 part                                                                 
Unaligned contigs length                   1285                                                                       
#ambiguously mapped contigs                144                                                                        
Extra bases in ambiguously mapped contigs  -36559                                                                     
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        95.697                                                                     
Duplication ratio                          1.236                                                                      
#genes                                     2236 + 1817 part                                                           
#predicted genes (unique)                  10645                                                                      
#predicted genes (>= 0 bp)                 10740                                                                      
#predicted genes (>= 300 bp)               4212                                                                       
#predicted genes (>= 1500 bp)              241                                                                        
#predicted genes (>= 3000 bp)              11                                                                         
#mismatches                                119                                                                        
#indels                                    1577                                                                       
Indels length                              1698                                                                       
#mismatches per 100 kbp                    2.82                                                                       
#indels per 100 kbp                        37.35                                                                      
GC (%)                                     64.75                                                                      
Reference GC (%)                           65.61                                                                      
Average %IDY                               99.668                                                                     
Largest alignment                          14879                                                                      
NA50                                       1853                                                                       
NGA50                                      2305                                                                       
NA75                                       444                                                                        
NGA75                                      1139                                                                       
LA50                                       776                                                                        
LGA50                                      572                                                                        
LA75                                       2066                                                                       
LGA75                                      1240                                                                       
#Mis_misassemblies                         7                                                                          
#Mis_relocations                           7                                                                          
#Mis_translocations                        0                                                                          
#Mis_inversions                            0                                                                          
#Mis_misassembled contigs                  7                                                                          
Mis_Misassembled contigs length            4531                                                                       
#Mis_local misassemblies                   10                                                                         
#Mis_short indels (<= 5 bp)                1575                                                                       
#Mis_long indels (> 5 bp)                  2                                                                          
#Una_fully unaligned contigs               4                                                                          
Una_Fully unaligned length                 971                                                                        
#Una_partially unaligned contigs           1                                                                          
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             314                                                                        
GAGE_Contigs #                             7820                                                                       
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            14879                                                                      
GAGE_N50                                   2305 COUNT: 572                                                            
GAGE_Genome size                           4411532                                                                    
GAGE_Assembly size                         5252914                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               136307(3.09%)                                                              
GAGE_Missing assembly bases                1321(0.03%)                                                                
GAGE_Missing assembly contigs              4(0.05%)                                                                   
GAGE_Duplicated reference bases            719896                                                                     
GAGE_Compressed reference bases            57238                                                                      
GAGE_Bad trim                              350                                                                        
GAGE_Avg idy                               99.95                                                                      
GAGE_SNPs                                  78                                                                         
GAGE_Indels < 5bp                          1346                                                                       
GAGE_Indels >= 5                           7                                                                          
GAGE_Inversions                            0                                                                          
GAGE_Relocation                            5                                                                          
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    4574                                                                       
GAGE_Corrected assembly size               4534093                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    14883                                                                      
GAGE_Corrected N50                         2295 COUNT: 576                                                            
