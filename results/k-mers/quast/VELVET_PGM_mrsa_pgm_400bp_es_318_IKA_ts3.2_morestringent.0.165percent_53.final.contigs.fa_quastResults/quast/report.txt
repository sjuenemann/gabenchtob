All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_53.final.contigs
#Contigs (>= 0 bp)             41196                                                                                 
#Contigs (>= 1000 bp)          0                                                                                     
Total length (>= 0 bp)         6054602                                                                               
Total length (>= 1000 bp)      0                                                                                     
#Contigs                       2477                                                                                  
Largest contig                 416                                                                                   
Total length                   557358                                                                                
Reference length               2813862                                                                               
GC (%)                         31.24                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            220                                                                                   
NG50                           None                                                                                  
N75                            208                                                                                   
NG75                           None                                                                                  
#misassemblies                 0                                                                                     
#local misassemblies           0                                                                                     
#unaligned contigs             2447 + 0 part                                                                         
Unaligned contigs length       550721                                                                                
Genome fraction (%)            0.233                                                                                 
Duplication ratio              1.013                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        0.00                                                                                  
#indels per 100 kbp            213.74                                                                                
#genes                         2 + 22 part                                                                           
#predicted genes (unique)      1471                                                                                  
#predicted genes (>= 0 bp)     1471                                                                                  
#predicted genes (>= 300 bp)   0                                                                                     
#predicted genes (>= 1500 bp)  0                                                                                     
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              271                                                                                   
NA50                           None                                                                                  
NGA50                          None                                                                                  
NA75                           None                                                                                  
NGA75                          None                                                                                  
