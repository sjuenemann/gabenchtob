All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_110-unitigs
GAGE_Contigs #                   2689                                                                    
GAGE_Min contig                  200                                                                     
GAGE_Max contig                  48938                                                                   
GAGE_N50                         11331 COUNT: 141                                                        
GAGE_Genome size                 5594470                                                                 
GAGE_Assembly size               5657603                                                                 
GAGE_Chaff bases                 0                                                                       
GAGE_Missing reference bases     20363(0.36%)                                                            
GAGE_Missing assembly bases      316(0.01%)                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                
GAGE_Duplicated reference bases  193963                                                                  
GAGE_Compressed reference bases  277825                                                                  
GAGE_Bad trim                    316                                                                     
GAGE_Avg idy                     99.99                                                                   
GAGE_SNPs                        50                                                                      
GAGE_Indels < 5bp                432                                                                     
GAGE_Indels >= 5                 3                                                                       
GAGE_Inversions                  0                                                                       
GAGE_Relocation                  6                                                                       
GAGE_Translocation               0                                                                       
GAGE_Corrected contig #          1792                                                                    
GAGE_Corrected assembly size     5462231                                                                 
GAGE_Min correct contig          200                                                                     
GAGE_Max correct contig          48940                                                                   
GAGE_Corrected N50               11268 COUNT: 143                                                        
