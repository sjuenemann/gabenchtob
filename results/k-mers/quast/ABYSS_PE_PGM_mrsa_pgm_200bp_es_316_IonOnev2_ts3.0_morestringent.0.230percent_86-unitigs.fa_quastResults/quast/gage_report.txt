All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_86-unitigs
GAGE_Contigs #                   615                                                                                    
GAGE_Min contig                  203                                                                                    
GAGE_Max contig                  60717                                                                                  
GAGE_N50                         8597 COUNT: 98                                                                         
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2765362                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     41378(1.47%)                                                                           
GAGE_Missing assembly bases      12(0.00%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  2333                                                                                   
GAGE_Compressed reference bases  26174                                                                                  
GAGE_Bad trim                    12                                                                                     
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        42                                                                                     
GAGE_Indels < 5bp                539                                                                                    
GAGE_Indels >= 5                 3                                                                                      
GAGE_Inversions                  2                                                                                      
GAGE_Relocation                  1                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          612                                                                                    
GAGE_Corrected assembly size     2763225                                                                                
GAGE_Min correct contig          206                                                                                    
GAGE_Max correct contig          60722                                                                                  
GAGE_Corrected N50               8443 COUNT: 99                                                                         
