All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_46-unitigs
#Contigs (>= 0 bp)             8050                                                                   
#Contigs (>= 1000 bp)          693                                                                    
Total length (>= 0 bp)         5758660                                                                
Total length (>= 1000 bp)      5009899                                                                
#Contigs                       1163                                                                   
Largest contig                 43756                                                                  
Total length                   5215037                                                                
Reference length               5594470                                                                
GC (%)                         50.22                                                                  
Reference GC (%)               50.48                                                                  
N50                            11739                                                                  
NG50                           11003                                                                  
N75                            5800                                                                   
NG75                           4548                                                                   
#misassemblies                 2                                                                      
#local misassemblies           6                                                                      
#unaligned contigs             0 + 0 part                                                             
Unaligned contigs length       0                                                                      
Genome fraction (%)            92.096                                                                 
Duplication ratio              1.001                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        1.38                                                                   
#indels per 100 kbp            4.62                                                                   
#genes                         4282 + 677 part                                                        
#predicted genes (unique)      5738                                                                   
#predicted genes (>= 0 bp)     5738                                                                   
#predicted genes (>= 300 bp)   4560                                                                   
#predicted genes (>= 1500 bp)  573                                                                    
#predicted genes (>= 3000 bp)  51                                                                     
Largest alignment              43756                                                                  
NA50                           11739                                                                  
NGA50                          11003                                                                  
NA75                           5800                                                                   
NGA75                          4548                                                                   
