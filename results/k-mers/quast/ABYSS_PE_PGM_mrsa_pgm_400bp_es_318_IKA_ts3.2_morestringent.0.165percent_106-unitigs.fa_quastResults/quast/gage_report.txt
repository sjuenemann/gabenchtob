All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_106-unitigs
GAGE_Contigs #                   1195                                                                               
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  52482                                                                              
GAGE_N50                         13194 COUNT: 65                                                                    
GAGE_Genome size                 2813862                                                                            
GAGE_Assembly size               2916075                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     10358(0.37%)                                                                       
GAGE_Missing assembly bases      396(0.01%)                                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  117473                                                                             
GAGE_Compressed reference bases  39116                                                                              
GAGE_Bad trim                    396                                                                                
GAGE_Avg idy                     99.99                                                                              
GAGE_SNPs                        31                                                                                 
GAGE_Indels < 5bp                210                                                                                
GAGE_Indels >= 5                 2                                                                                  
GAGE_Inversions                  0                                                                                  
GAGE_Relocation                  1                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          629                                                                                
GAGE_Corrected assembly size     2798337                                                                            
GAGE_Min correct contig          201                                                                                
GAGE_Max correct contig          52484                                                                              
GAGE_Corrected N50               12748 COUNT: 66                                                                    
