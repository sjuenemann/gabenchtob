All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_49.final.contig
GAGE_Contigs #                   2485                                                                        
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  1318                                                                        
GAGE_N50                         0 COUNT: 0                                                                  
GAGE_Genome size                 4411532                                                                     
GAGE_Assembly size               670528                                                                      
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     3784889(85.80%)                                                             
GAGE_Missing assembly bases      37943(5.66%)                                                                
GAGE_Missing assembly contigs    154(6.20%)                                                                  
GAGE_Duplicated reference bases  3946                                                                        
GAGE_Compressed reference bases  1736                                                                        
GAGE_Bad trim                    3131                                                                        
GAGE_Avg idy                     99.63                                                                       
GAGE_SNPs                        102                                                                         
GAGE_Indels < 5bp                2158                                                                        
GAGE_Indels >= 5                 0                                                                           
GAGE_Inversions                  0                                                                           
GAGE_Relocation                  1                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          2231                                                                        
GAGE_Corrected assembly size     613942                                                                      
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          1319                                                                        
GAGE_Corrected N50               0 COUNT: 0                                                                  
