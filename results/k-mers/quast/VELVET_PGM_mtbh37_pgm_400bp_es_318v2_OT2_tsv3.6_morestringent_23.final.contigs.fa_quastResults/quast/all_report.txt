All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_23.final.contigs
#Contigs                                   2461                                                                          
#Contigs (>= 0 bp)                         3442                                                                          
#Contigs (>= 1000 bp)                      1310                                                                          
Largest contig                             12873                                                                         
Total length                               4124931                                                                       
Total length (>= 0 bp)                     4221075                                                                       
Total length (>= 1000 bp)                  3531001                                                                       
Reference length                           4411532                                                                       
N50                                        2752                                                                          
NG50                                       2558                                                                          
N75                                        1500                                                                          
NG75                                       1243                                                                          
L50                                        450                                                                           
LG50                                       504                                                                           
L75                                        955                                                                           
LG75                                       1112                                                                          
#local misassemblies                       7                                                                             
#misassemblies                             7                                                                             
#misassembled contigs                      7                                                                             
Misassembled contigs length                9350                                                                          
Misassemblies inter-contig overlap         925                                                                           
#unaligned contigs                         2 + 5 part                                                                    
Unaligned contigs length                   1107                                                                          
#ambiguously mapped contigs                34                                                                            
Extra bases in ambiguously mapped contigs  -15510                                                                        
#N's                                       70                                                                            
#N's per 100 kbp                           1.70                                                                          
Genome fraction (%)                        92.600                                                                        
Duplication ratio                          1.006                                                                         
#genes                                     2486 + 1505 part                                                              
#predicted genes (unique)                  6038                                                                          
#predicted genes (>= 0 bp)                 6040                                                                          
#predicted genes (>= 300 bp)               4229                                                                          
#predicted genes (>= 1500 bp)              240                                                                           
#predicted genes (>= 3000 bp)              4                                                                             
#mismatches                                499                                                                           
#indels                                    2352                                                                          
Indels length                              2623                                                                          
#mismatches per 100 kbp                    12.22                                                                         
#indels per 100 kbp                        57.58                                                                         
GC (%)                                     65.19                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.888                                                                        
Largest alignment                          12873                                                                         
NA50                                       2747                                                                          
NGA50                                      2552                                                                          
NA75                                       1500                                                                          
NGA75                                      1240                                                                          
LA50                                       451                                                                           
LGA50                                      505                                                                           
LA75                                       956                                                                           
LGA75                                      1114                                                                          
#Mis_misassemblies                         7                                                                             
#Mis_relocations                           7                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  7                                                                             
Mis_Misassembled contigs length            9350                                                                          
#Mis_local misassemblies                   7                                                                             
#Mis_short indels (<= 5 bp)                2347                                                                          
#Mis_long indels (> 5 bp)                  5                                                                             
#Una_fully unaligned contigs               2                                                                             
Una_Fully unaligned length                 915                                                                           
#Una_partially unaligned contigs           5                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             192                                                                           
GAGE_Contigs #                             2461                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            12873                                                                         
GAGE_N50                                   2558 COUNT: 504                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         4124931                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               284096(6.44%)                                                                 
GAGE_Missing assembly bases                1445(0.04%)                                                                   
GAGE_Missing assembly contigs              2(0.08%)                                                                      
GAGE_Duplicated reference bases            3157                                                                          
GAGE_Compressed reference bases            28388                                                                         
GAGE_Bad trim                              440                                                                           
GAGE_Avg idy                               99.93                                                                         
GAGE_SNPs                                  284                                                                           
GAGE_Indels < 5bp                          2468                                                                          
GAGE_Indels >= 5                           12                                                                            
GAGE_Inversions                            2                                                                             
GAGE_Relocation                            3                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    2474                                                                          
GAGE_Corrected assembly size               4122060                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    12875                                                                         
GAGE_Corrected N50                         2549 COUNT: 507                                                               
