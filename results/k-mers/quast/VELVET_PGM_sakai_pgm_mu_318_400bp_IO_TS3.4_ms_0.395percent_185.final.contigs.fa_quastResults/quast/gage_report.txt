All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_185.final.contigs
GAGE_Contigs #                   1987                                                                        
GAGE_Min contig                  369                                                                         
GAGE_Max contig                  16454                                                                       
GAGE_N50                         2753 COUNT: 559                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               4568224                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     977297(17.47%)                                                              
GAGE_Missing assembly bases      1094(0.02%)                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  33390                                                                       
GAGE_Compressed reference bases  234592                                                                      
GAGE_Bad trim                    922                                                                         
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        70                                                                          
GAGE_Indels < 5bp                741                                                                         
GAGE_Indels >= 5                 20                                                                          
GAGE_Inversions                  3                                                                           
GAGE_Relocation                  3                                                                           
GAGE_Translocation               1                                                                           
GAGE_Corrected contig #          1975                                                                        
GAGE_Corrected assembly size     4536198                                                                     
GAGE_Min correct contig          257                                                                         
GAGE_Max correct contig          16455                                                                       
GAGE_Corrected N50               2695 COUNT: 572                                                             
