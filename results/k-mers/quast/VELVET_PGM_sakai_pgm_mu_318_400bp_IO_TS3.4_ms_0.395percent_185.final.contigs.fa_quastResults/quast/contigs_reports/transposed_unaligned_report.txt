All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                      #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_185.final.contigs  0                             0                           1                                 0                      1                                565                             490 
