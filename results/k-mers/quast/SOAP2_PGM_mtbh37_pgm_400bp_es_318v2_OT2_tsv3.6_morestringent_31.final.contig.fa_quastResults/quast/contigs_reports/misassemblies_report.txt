All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_31.final.contig
#Mis_misassemblies               0                                                                           
#Mis_relocations                 0                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        0                                                                           
Mis_Misassembled contigs length  0                                                                           
#Mis_local misassemblies         1                                                                           
#mismatches                      20                                                                          
#indels                          165                                                                         
#Mis_short indels (<= 5 bp)      165                                                                         
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    168                                                                         
