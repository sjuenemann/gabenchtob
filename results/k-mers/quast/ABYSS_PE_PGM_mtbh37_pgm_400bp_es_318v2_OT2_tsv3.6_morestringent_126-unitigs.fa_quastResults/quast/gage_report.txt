All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_126-unitigs
GAGE_Contigs #                   8149                                                                       
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  12715                                                                      
GAGE_N50                         2325 COUNT: 566                                                            
GAGE_Genome size                 4411532                                                                    
GAGE_Assembly size               5454204                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     143984(3.26%)                                                              
GAGE_Missing assembly bases      1426(0.03%)                                                                
GAGE_Missing assembly contigs    4(0.05%)                                                                   
GAGE_Duplicated reference bases  915109                                                                     
GAGE_Compressed reference bases  51907                                                                      
GAGE_Bad trim                    371                                                                        
GAGE_Avg idy                     99.96                                                                      
GAGE_SNPs                        95                                                                         
GAGE_Indels < 5bp                1347                                                                       
GAGE_Indels >= 5                 3                                                                          
GAGE_Inversions                  0                                                                          
GAGE_Relocation                  6                                                                          
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          4449                                                                       
GAGE_Corrected assembly size     4538849                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          12720                                                                      
GAGE_Corrected N50               2314 COUNT: 571                                                            
