All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_89.final.contig
#Contigs                                   13453                                                                    
#Contigs (>= 0 bp)                         167384                                                                   
#Contigs (>= 1000 bp)                      7                                                                        
Largest contig                             1382                                                                     
Total length                               4006450                                                                  
Total length (>= 0 bp)                     25629209                                                                 
Total length (>= 1000 bp)                  7930                                                                     
Reference length                           5594470                                                                  
N50                                        292                                                                      
NG50                                       271                                                                      
N75                                        262                                                                      
NG75                                       None                                                                     
L50                                        5424                                                                     
LG50                                       8253                                                                     
L75                                        9029                                                                     
LG75                                       None                                                                     
#local misassemblies                       5                                                                        
#misassemblies                             152                                                                      
#misassembled contigs                      152                                                                      
Misassembled contigs length                45716                                                                    
Misassemblies inter-contig overlap         710                                                                      
#unaligned contigs                         268 + 64 part                                                            
Unaligned contigs length                   88561                                                                    
#ambiguously mapped contigs                128                                                                      
Extra bases in ambiguously mapped contigs  -34760                                                                   
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        56.413                                                                   
Duplication ratio                          1.231                                                                    
#genes                                     197 + 4292 part                                                          
#predicted genes (unique)                  12895                                                                    
#predicted genes (>= 0 bp)                 12914                                                                    
#predicted genes (>= 300 bp)               2083                                                                     
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                716                                                                      
#indels                                    19420                                                                    
Indels length                              19792                                                                    
#mismatches per 100 kbp                    22.69                                                                    
#indels per 100 kbp                        615.34                                                                   
GC (%)                                     49.36                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.195                                                                   
Largest alignment                          1382                                                                     
NA50                                       289                                                                      
NGA50                                      266                                                                      
NA75                                       249                                                                      
NGA75                                      None                                                                     
LA50                                       5460                                                                     
LGA50                                      8326                                                                     
LA75                                       9133                                                                     
LGA75                                      None                                                                     
#Mis_misassemblies                         152                                                                      
#Mis_relocations                           144                                                                      
#Mis_translocations                        8                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  152                                                                      
Mis_Misassembled contigs length            45716                                                                    
#Mis_local misassemblies                   5                                                                        
#Mis_short indels (<= 5 bp)                19420                                                                    
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               268                                                                      
Una_Fully unaligned length                 80729                                                                    
#Una_partially unaligned contigs           64                                                                       
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            1                                                                        
Una_Partially unaligned length             7832                                                                     
GAGE_Contigs #                             13453                                                                    
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            1382                                                                     
GAGE_N50                                   271 COUNT: 8253                                                          
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         4006450                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               2264812(40.48%)                                                          
GAGE_Missing assembly bases                31312(0.78%)                                                             
GAGE_Missing assembly contigs              55(0.41%)                                                                
GAGE_Duplicated reference bases            445032                                                                   
GAGE_Compressed reference bases            152934                                                                   
GAGE_Bad trim                              13923                                                                    
GAGE_Avg idy                               99.32                                                                    
GAGE_SNPs                                  652                                                                      
GAGE_Indels < 5bp                          17045                                                                    
GAGE_Indels >= 5                           10                                                                       
GAGE_Inversions                            32                                                                       
GAGE_Relocation                            22                                                                       
GAGE_Translocation                         6                                                                        
GAGE_Corrected contig #                    11572                                                                    
GAGE_Corrected assembly size               3475484                                                                  
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    1382                                                                     
GAGE_Corrected N50                         245 COUNT: 8492                                                          
