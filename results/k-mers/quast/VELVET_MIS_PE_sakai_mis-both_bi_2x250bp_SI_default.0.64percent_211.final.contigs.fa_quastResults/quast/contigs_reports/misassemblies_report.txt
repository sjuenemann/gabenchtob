All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_211.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_211.final.contigs
#Mis_misassemblies               11                                                                                       18                                                                              
#Mis_relocations                 10                                                                                       17                                                                              
#Mis_translocations              1                                                                                        1                                                                               
#Mis_inversions                  0                                                                                        0                                                                               
#Mis_misassembled contigs        11                                                                                       17                                                                              
Mis_Misassembled contigs length  39036                                                                                    90864                                                                           
#Mis_local misassemblies         4                                                                                        1095                                                                            
#mismatches                      432                                                                                      480                                                                             
#indels                          20                                                                                       1249                                                                            
#Mis_short indels (<= 5 bp)      20                                                                                       1234                                                                            
#Mis_long indels (> 5 bp)        0                                                                                        15                                                                              
Indels length                    20                                                                                       2198                                                                            
