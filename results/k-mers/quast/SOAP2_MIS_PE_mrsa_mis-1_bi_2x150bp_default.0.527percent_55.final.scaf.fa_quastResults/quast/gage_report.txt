All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_55.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_55.final.scaf
GAGE_Contigs #                   278                                                                           65                                                                   
GAGE_Min contig                  202                                                                           202                                                                  
GAGE_Max contig                  77813                                                                         318169                                                               
GAGE_N50                         20421 COUNT: 41                                                               129860 COUNT: 8                                                      
GAGE_Genome size                 2813862                                                                       2813862                                                              
GAGE_Assembly size               2743239                                                                       2770883                                                              
GAGE_Chaff bases                 0                                                                             0                                                                    
GAGE_Missing reference bases     62062(2.21%)                                                                  47702(1.70%)                                                         
GAGE_Missing assembly bases      5867(0.21%)                                                                   21828(0.79%)                                                         
GAGE_Missing assembly contigs    2(0.72%)                                                                      2(3.08%)                                                             
GAGE_Duplicated reference bases  204                                                                           1517                                                                 
GAGE_Compressed reference bases  17174                                                                         23120                                                                
GAGE_Bad trim                    132                                                                           1026                                                                 
GAGE_Avg idy                     100.00                                                                        99.96                                                                
GAGE_SNPs                        32                                                                            37                                                                   
GAGE_Indels < 5bp                26                                                                            97                                                                   
GAGE_Indels >= 5                 21                                                                            251                                                                  
GAGE_Inversions                  0                                                                             0                                                                    
GAGE_Relocation                  1                                                                             15                                                                   
GAGE_Translocation               0                                                                             0                                                                    
GAGE_Corrected contig #          289                                                                           269                                                                  
GAGE_Corrected assembly size     2736238                                                                       2737355                                                              
GAGE_Min correct contig          205                                                                           205                                                                  
GAGE_Max correct contig          77813                                                                         77813                                                                
GAGE_Corrected N50               19432 COUNT: 42                                                               20596 COUNT: 40                                                      
