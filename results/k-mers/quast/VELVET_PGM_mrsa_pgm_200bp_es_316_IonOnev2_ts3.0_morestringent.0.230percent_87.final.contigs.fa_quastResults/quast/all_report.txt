All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_87.final.contigs
#Contigs                                   1203                                                                                       
#Contigs (>= 0 bp)                         1220                                                                                       
#Contigs (>= 1000 bp)                      773                                                                                        
Largest contig                             16002                                                                                      
Total length                               2652371                                                                                    
Total length (>= 0 bp)                     2655458                                                                                    
Total length (>= 1000 bp)                  2408177                                                                                    
Reference length                           2813862                                                                                    
N50                                        3635                                                                                       
NG50                                       3366                                                                                       
N75                                        1894                                                                                       
NG75                                       1639                                                                                       
L50                                        222                                                                                        
LG50                                       245                                                                                        
L75                                        477                                                                                        
LG75                                       545                                                                                        
#local misassemblies                       10                                                                                         
#misassemblies                             54                                                                                         
#misassembled contigs                      52                                                                                         
Misassembled contigs length                126980                                                                                     
Misassemblies inter-contig overlap         1023                                                                                       
#unaligned contigs                         0 + 4 part                                                                                 
Unaligned contigs length                   123                                                                                        
#ambiguously mapped contigs                21                                                                                         
Extra bases in ambiguously mapped contigs  -9284                                                                                      
#N's                                       170                                                                                        
#N's per 100 kbp                           6.41                                                                                       
Genome fraction (%)                        93.164                                                                                     
Duplication ratio                          1.009                                                                                      
#genes                                     1841 + 769 part                                                                            
#predicted genes (unique)                  3274                                                                                       
#predicted genes (>= 0 bp)                 3278                                                                                       
#predicted genes (>= 300 bp)               2475                                                                                       
#predicted genes (>= 1500 bp)              202                                                                                        
#predicted genes (>= 3000 bp)              15                                                                                         
#mismatches                                69                                                                                         
#indels                                    639                                                                                        
Indels length                              735                                                                                        
#mismatches per 100 kbp                    2.63                                                                                       
#indels per 100 kbp                        24.38                                                                                      
GC (%)                                     32.76                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.613                                                                                     
Largest alignment                          16002                                                                                      
NA50                                       3615                                                                                       
NGA50                                      3348                                                                                       
NA75                                       1882                                                                                       
NGA75                                      1629                                                                                       
LA50                                       223                                                                                        
LGA50                                      247                                                                                        
LA75                                       481                                                                                        
LGA75                                      550                                                                                        
#Mis_misassemblies                         54                                                                                         
#Mis_relocations                           24                                                                                         
#Mis_translocations                        0                                                                                          
#Mis_inversions                            30                                                                                         
#Mis_misassembled contigs                  52                                                                                         
Mis_Misassembled contigs length            126980                                                                                     
#Mis_local misassemblies                   10                                                                                         
#Mis_short indels (<= 5 bp)                638                                                                                        
#Mis_long indels (> 5 bp)                  1                                                                                          
#Una_fully unaligned contigs               0                                                                                          
Una_Fully unaligned length                 0                                                                                          
#Una_partially unaligned contigs           4                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             123                                                                                        
GAGE_Contigs #                             1203                                                                                       
GAGE_Min contig                            208                                                                                        
GAGE_Max contig                            16002                                                                                      
GAGE_N50                                   3366 COUNT: 245                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         2652371                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               147739(5.25%)                                                                              
GAGE_Missing assembly bases                406(0.02%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                   
GAGE_Duplicated reference bases            4905                                                                                       
GAGE_Compressed reference bases            43147                                                                                      
GAGE_Bad trim                              315                                                                                        
GAGE_Avg idy                               99.97                                                                                      
GAGE_SNPs                                  55                                                                                         
GAGE_Indels < 5bp                          639                                                                                        
GAGE_Indels >= 5                           8                                                                                          
GAGE_Inversions                            9                                                                                          
GAGE_Relocation                            6                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    1216                                                                                       
GAGE_Corrected assembly size               2647471                                                                                    
GAGE_Min correct contig                    203                                                                                        
GAGE_Max correct contig                    16002                                                                                      
GAGE_Corrected N50                         3258 COUNT: 250                                                                            
