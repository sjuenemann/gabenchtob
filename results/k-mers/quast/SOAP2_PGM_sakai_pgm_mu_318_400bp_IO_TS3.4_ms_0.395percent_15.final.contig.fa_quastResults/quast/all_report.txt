All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_15.final.contig
#Contigs                                   16                                                                       
#Contigs (>= 0 bp)                         786345                                                                   
#Contigs (>= 1000 bp)                      0                                                                        
Largest contig                             296                                                                      
Total length                               3691                                                                     
Total length (>= 0 bp)                     19515128                                                                 
Total length (>= 1000 bp)                  0                                                                        
Reference length                           5594470                                                                  
N50                                        231                                                                      
NG50                                       None                                                                     
N75                                        205                                                                      
NG75                                       None                                                                     
L50                                        8                                                                        
LG50                                       None                                                                     
L75                                        12                                                                       
LG75                                       None                                                                     
#local misassemblies                       0                                                                        
#misassemblies                             0                                                                        
#misassembled contigs                      0                                                                        
Misassembled contigs length                0                                                                        
Misassemblies inter-contig overlap         0                                                                        
#unaligned contigs                         5 + 0 part                                                               
Unaligned contigs length                   1238                                                                     
#ambiguously mapped contigs                0                                                                        
Extra bases in ambiguously mapped contigs  0                                                                        
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        0.044                                                                    
Duplication ratio                          1.000                                                                    
#genes                                     2 + 8 part                                                               
#predicted genes (unique)                  17                                                                       
#predicted genes (>= 0 bp)                 17                                                                       
#predicted genes (>= 300 bp)               0                                                                        
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                0                                                                        
#indels                                    5                                                                        
Indels length                              5                                                                        
#mismatches per 100 kbp                    0.00                                                                     
#indels per 100 kbp                        203.75                                                                   
GC (%)                                     55.78                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.817                                                                   
Largest alignment                          286                                                                      
NA50                                       205                                                                      
NGA50                                      None                                                                     
NA75                                       None                                                                     
NGA75                                      None                                                                     
LA50                                       9                                                                        
LGA50                                      None                                                                     
LA75                                       None                                                                     
LGA75                                      None                                                                     
#Mis_misassemblies                         0                                                                        
#Mis_relocations                           0                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  0                                                                        
Mis_Misassembled contigs length            0                                                                        
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                5                                                                        
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               5                                                                        
Una_Fully unaligned length                 1238                                                                     
#Una_partially unaligned contigs           0                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             0                                                                        
GAGE_Contigs #                             16                                                                       
GAGE_Min contig                            201                                                                      
GAGE_Max contig                            296                                                                      
GAGE_N50                                   0 COUNT: 0                                                               
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         3691                                                                     
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               5592016(99.96%)                                                          
GAGE_Missing assembly bases                1238(33.54%)                                                             
GAGE_Missing assembly contigs              5(31.25%)                                                                
GAGE_Duplicated reference bases            0                                                                        
GAGE_Compressed reference bases            0                                                                        
GAGE_Bad trim                              0                                                                        
GAGE_Avg idy                               99.80                                                                    
GAGE_SNPs                                  0                                                                        
GAGE_Indels < 5bp                          5                                                                        
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            0                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    11                                                                       
GAGE_Corrected assembly size               2454                                                                     
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    287                                                                      
GAGE_Corrected N50                         0 COUNT: 0                                                               
