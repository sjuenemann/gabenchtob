All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_193.final.contigs
GAGE_Contigs #                   3                                                                                           
GAGE_Min contig                  797                                                                                         
GAGE_Max contig                  1944                                                                                        
GAGE_N50                         0 COUNT: 0                                                                                  
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               3887                                                                                        
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     2810018(99.86%)                                                                             
GAGE_Missing assembly bases      0(0.00%)                                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  0                                                                                           
GAGE_Compressed reference bases  0                                                                                           
GAGE_Bad trim                    0                                                                                           
GAGE_Avg idy                     99.95                                                                                       
GAGE_SNPs                        0                                                                                           
GAGE_Indels < 5bp                1                                                                                           
GAGE_Indels >= 5                 0                                                                                           
GAGE_Inversions                  0                                                                                           
GAGE_Relocation                  1                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          4                                                                                           
GAGE_Corrected assembly size     3889                                                                                        
GAGE_Min correct contig          397                                                                                         
GAGE_Max correct contig          1548                                                                                        
GAGE_Corrected N50               0 COUNT: 0                                                                                  
