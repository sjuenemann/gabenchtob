All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_151.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_151.final.contigs
#Contigs (>= 0 bp)             504                                                                                      402                                                                             
#Contigs (>= 1000 bp)          283                                                                                      204                                                                             
Total length (>= 0 bp)         5456149                                                                                  5468123                                                                         
Total length (>= 1000 bp)      5351845                                                                                  5376963                                                                         
#Contigs                       503                                                                                      402                                                                             
Largest contig                 150463                                                                                   206683                                                                          
Total length                   5456056                                                                                  5468123                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.35                                                                                    50.35                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            36677                                                                                    65008                                                                           
NG50                           35836                                                                                    64038                                                                           
N75                            20117                                                                                    32928                                                                           
NG75                           18825                                                                                    31593                                                                           
#misassemblies                 7                                                                                        15                                                                              
#local misassemblies           2                                                                                        87                                                                              
#unaligned contigs             1 + 0 part                                                                               1 + 0 part                                                                      
Unaligned contigs length       5536                                                                                     5536                                                                            
Genome fraction (%)            95.427                                                                                   95.499                                                                          
Duplication ratio              1.001                                                                                    1.003                                                                           
#N's per 100 kbp               0.00                                                                                     218.98                                                                          
#mismatches per 100 kbp        3.07                                                                                     3.14                                                                            
#indels per 100 kbp            0.66                                                                                     4.42                                                                            
#genes                         4965 + 299 part                                                                          4971 + 297 part                                                                 
#predicted genes (unique)      5552                                                                                     5533                                                                            
#predicted genes (>= 0 bp)     5570                                                                                     5552                                                                            
#predicted genes (>= 300 bp)   4618                                                                                     4611                                                                            
#predicted genes (>= 1500 bp)  651                                                                                      653                                                                             
#predicted genes (>= 3000 bp)  61                                                                                       61                                                                              
Largest alignment              150463                                                                                   165434                                                                          
NA50                           35836                                                                                    64038                                                                           
NGA50                          34659                                                                                    63676                                                                           
NA75                           20051                                                                                    31593                                                                           
NGA75                          17960                                                                                    31083                                                                           
