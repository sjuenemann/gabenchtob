All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_79.final.contigs
GAGE_Contigs #                   7180                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  2678                                                                          
GAGE_N50                         257 COUNT: 4807                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               2737752                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     1719596(38.98%)                                                               
GAGE_Missing assembly bases      544(0.02%)                                                                    
GAGE_Missing assembly contigs    1(0.01%)                                                                      
GAGE_Duplicated reference bases  22225                                                                         
GAGE_Compressed reference bases  10983                                                                         
GAGE_Bad trim                    313                                                                           
GAGE_Avg idy                     99.92                                                                         
GAGE_SNPs                        131                                                                           
GAGE_Indels < 5bp                2024                                                                          
GAGE_Indels >= 5                 7                                                                             
GAGE_Inversions                  12                                                                            
GAGE_Relocation                  8                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          7050                                                                          
GAGE_Corrected assembly size     2707976                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          2682                                                                          
GAGE_Corrected N50               257 COUNT: 4814                                                               
