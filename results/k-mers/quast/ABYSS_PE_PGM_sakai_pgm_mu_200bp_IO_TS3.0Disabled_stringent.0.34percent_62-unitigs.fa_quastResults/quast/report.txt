All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_62-unitigs
#Contigs (>= 0 bp)             5757                                                                             
#Contigs (>= 1000 bp)          736                                                                              
Total length (>= 0 bp)         5747053                                                                          
Total length (>= 1000 bp)      5043316                                                                          
#Contigs                       1259                                                                             
Largest contig                 53983                                                                            
Total length                   5261479                                                                          
Reference length               5594470                                                                          
GC (%)                         50.23                                                                            
Reference GC (%)               50.48                                                                            
N50                            10448                                                                            
NG50                           9584                                                                             
N75                            5264                                                                             
NG75                           4355                                                                             
#misassemblies                 2                                                                                
#local misassemblies           5                                                                                
#unaligned contigs             0 + 1 part                                                                       
Unaligned contigs length       47                                                                               
Genome fraction (%)            92.775                                                                           
Duplication ratio              1.001                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        1.64                                                                             
#indels per 100 kbp            24.14                                                                            
#genes                         4301 + 729 part                                                                  
#predicted genes (unique)      6317                                                                             
#predicted genes (>= 0 bp)     6319                                                                             
#predicted genes (>= 300 bp)   4821                                                                             
#predicted genes (>= 1500 bp)  496                                                                              
#predicted genes (>= 3000 bp)  34                                                                               
Largest alignment              53983                                                                            
NA50                           10315                                                                            
NGA50                          9578                                                                             
NA75                           5264                                                                             
NGA75                          4355                                                                             
