All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_111.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_111.final.scaf
#Contigs                                   3131                                                                              2423                                                                     
#Contigs (>= 0 bp)                         4103                                                                              3347                                                                     
#Contigs (>= 1000 bp)                      755                                                                               311                                                                      
Largest contig                             41509                                                                             144808                                                                   
Total length                               5737689                                                                           5780351                                                                  
Total length (>= 0 bp)                     5878982                                                                           5914206                                                                  
Total length (>= 1000 bp)                  5096239                                                                           5261983                                                                  
Reference length                           5594470                                                                           5594470                                                                  
N50                                        9369                                                                              32179                                                                    
NG50                                       9620                                                                              34608                                                                    
N75                                        3962                                                                              13641                                                                    
NG75                                       4361                                                                              15286                                                                    
L50                                        181                                                                               55                                                                       
LG50                                       173                                                                               53                                                                       
L75                                        411                                                                               122                                                                      
LG75                                       385                                                                               113                                                                      
#local misassemblies                       37                                                                                594                                                                      
#misassemblies                             12                                                                                27                                                                       
#misassembled contigs                      12                                                                                22                                                                       
Misassembled contigs length                37690                                                                             98529                                                                    
Misassemblies inter-contig overlap         109                                                                               109                                                                      
#unaligned contigs                         10 + 0 part                                                                       22 + 13 part                                                             
Unaligned contigs length                   8366                                                                              28092                                                                    
#ambiguously mapped contigs                553                                                                               500                                                                      
Extra bases in ambiguously mapped contigs  -166171                                                                           -143564                                                                  
#N's                                       225                                                                               35449                                                                    
#N's per 100 kbp                           3.92                                                                              613.27                                                                   
Genome fraction (%)                        94.657                                                                            94.652                                                                   
Duplication ratio                          1.051                                                                             1.059                                                                    
#genes                                     4289 + 902 part                                                                   4336 + 838 part                                                          
#predicted genes (unique)                  7739                                                                              7528                                                                     
#predicted genes (>= 0 bp)                 7822                                                                              7608                                                                     
#predicted genes (>= 300 bp)               4702                                                                              4671                                                                     
#predicted genes (>= 1500 bp)              556                                                                               580                                                                      
#predicted genes (>= 3000 bp)              45                                                                                51                                                                       
#mismatches                                454                                                                               592                                                                      
#indels                                    49                                                                                1124                                                                     
Indels length                              157                                                                               2499                                                                     
#mismatches per 100 kbp                    8.57                                                                              11.18                                                                    
#indels per 100 kbp                        0.93                                                                              21.23                                                                    
GC (%)                                     50.46                                                                             50.47                                                                    
Reference GC (%)                           50.48                                                                             50.48                                                                    
Average %IDY                               99.271                                                                            99.262                                                                   
Largest alignment                          41505                                                                             144122                                                                   
NA50                                       9356                                                                              31710                                                                    
NGA50                                      9565                                                                              32984                                                                    
NA75                                       3962                                                                              13066                                                                    
NGA75                                      4361                                                                              14481                                                                    
LA50                                       181                                                                               56                                                                       
LGA50                                      174                                                                               53                                                                       
LA75                                       412                                                                               125                                                                      
LGA75                                      386                                                                               115                                                                      
#Mis_misassemblies                         12                                                                                27                                                                       
#Mis_relocations                           12                                                                                27                                                                       
#Mis_translocations                        0                                                                                 0                                                                        
#Mis_inversions                            0                                                                                 0                                                                        
#Mis_misassembled contigs                  12                                                                                22                                                                       
Mis_Misassembled contigs length            37690                                                                             98529                                                                    
#Mis_local misassemblies                   37                                                                                594                                                                      
#Mis_short indels (<= 5 bp)                40                                                                                1056                                                                     
#Mis_long indels (> 5 bp)                  9                                                                                 68                                                                       
#Una_fully unaligned contigs               10                                                                                22                                                                       
Una_Fully unaligned length                 8366                                                                              19574                                                                    
#Una_partially unaligned contigs           0                                                                                 13                                                                       
#Una_with misassembly                      0                                                                                 0                                                                        
#Una_both parts are significant            0                                                                                 10                                                                       
Una_Partially unaligned length             0                                                                                 8518                                                                     
GAGE_Contigs #                             3131                                                                              2423                                                                     
GAGE_Min contig                            200                                                                               200                                                                      
GAGE_Max contig                            41509                                                                             144808                                                                   
GAGE_N50                                   9620 COUNT: 173                                                                   34608 COUNT: 53                                                          
GAGE_Genome size                           5594470                                                                           5594470                                                                  
GAGE_Assembly size                         5737689                                                                           5780351                                                                  
GAGE_Chaff bases                           0                                                                                 0                                                                        
GAGE_Missing reference bases               9345(0.17%)                                                                       8244(0.15%)                                                              
GAGE_Missing assembly bases                6992(0.12%)                                                                       44262(0.77%)                                                             
GAGE_Missing assembly contigs              7(0.22%)                                                                          7(0.29%)                                                                 
GAGE_Duplicated reference bases            204086                                                                            205150                                                                   
GAGE_Compressed reference bases            279573                                                                            274604                                                                   
GAGE_Bad trim                              95                                                                                3619                                                                     
GAGE_Avg idy                               100.00                                                                            99.98                                                                    
GAGE_SNPs                                  155                                                                               150                                                                      
GAGE_Indels < 5bp                          38                                                                                103                                                                      
GAGE_Indels >= 5                           13                                                                                128                                                                      
GAGE_Inversions                            3                                                                                 4                                                                        
GAGE_Relocation                            3                                                                                 9                                                                        
GAGE_Translocation                         0                                                                                 0                                                                        
GAGE_Corrected contig #                    2269                                                                              2248                                                                     
GAGE_Corrected assembly size               5521397                                                                           5524304                                                                  
GAGE_Min correct contig                    200                                                                               200                                                                      
GAGE_Max correct contig                    41505                                                                             41505                                                                    
GAGE_Corrected N50                         9356 COUNT: 182                                                                   9374 COUNT: 181                                                          
