All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K59.final_contigs
GAGE_Contigs #                   695                                                                             
GAGE_Min contig                  200                                                                             
GAGE_Max contig                  268216                                                                          
GAGE_N50                         97790 COUNT: 19                                                                 
GAGE_Genome size                 5594470                                                                         
GAGE_Assembly size               5353546                                                                         
GAGE_Chaff bases                 0                                                                               
GAGE_Missing reference bases     42082(0.75%)                                                                    
GAGE_Missing assembly bases      17308(0.32%)                                                                    
GAGE_Missing assembly contigs    63(9.06%)                                                                       
GAGE_Duplicated reference bases  16590                                                                           
GAGE_Compressed reference bases  267836                                                                          
GAGE_Bad trim                    800                                                                             
GAGE_Avg idy                     99.98                                                                           
GAGE_SNPs                        202                                                                             
GAGE_Indels < 5bp                364                                                                             
GAGE_Indels >= 5                 16                                                                              
GAGE_Inversions                  0                                                                               
GAGE_Relocation                  9                                                                               
GAGE_Translocation               0                                                                               
GAGE_Corrected contig #          581                                                                             
GAGE_Corrected assembly size     5323139                                                                         
GAGE_Min correct contig          200                                                                             
GAGE_Max correct contig          217385                                                                          
GAGE_Corrected N50               65952 COUNT: 23                                                                 
