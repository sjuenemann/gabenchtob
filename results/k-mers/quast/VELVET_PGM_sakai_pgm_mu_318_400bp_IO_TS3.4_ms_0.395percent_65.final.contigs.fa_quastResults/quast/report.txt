All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_65.final.contigs
#Contigs (>= 0 bp)             32529                                                                      
#Contigs (>= 1000 bp)          0                                                                          
Total length (>= 0 bp)         5435204                                                                    
Total length (>= 1000 bp)      0                                                                          
#Contigs                       3441                                                                       
Largest contig                 533                                                                        
Total length                   778946                                                                     
Reference length               5594470                                                                    
GC (%)                         49.37                                                                      
Reference GC (%)               50.48                                                                      
N50                            220                                                                        
NG50                           None                                                                       
N75                            208                                                                        
NG75                           None                                                                       
#misassemblies                 0                                                                          
#local misassemblies           3                                                                          
#unaligned contigs             2707 + 3 part                                                              
Unaligned contigs length       599180                                                                     
Genome fraction (%)            3.200                                                                      
Duplication ratio              1.003                                                                      
#N's per 100 kbp               0.00                                                                       
#mismatches per 100 kbp        12.85                                                                      
#indels per 100 kbp            15.08                                                                      
#genes                         3 + 605 part                                                               
#predicted genes (unique)      2590                                                                       
#predicted genes (>= 0 bp)     2590                                                                       
#predicted genes (>= 300 bp)   57                                                                         
#predicted genes (>= 1500 bp)  0                                                                          
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              533                                                                        
NA50                           None                                                                       
NGA50                          None                                                                       
NA75                           None                                                                       
NGA75                          None                                                                       
