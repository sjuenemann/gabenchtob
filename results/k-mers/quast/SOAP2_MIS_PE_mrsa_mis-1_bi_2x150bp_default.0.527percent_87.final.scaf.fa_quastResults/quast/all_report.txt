All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_87.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_87.final.scaf
#Contigs                                   141                                                                           53                                                                   
#Contigs (>= 0 bp)                         592                                                                           485                                                                  
#Contigs (>= 1000 bp)                      86                                                                            31                                                                   
Largest contig                             207782                                                                        428083                                                               
Total length                               2776559                                                                       2782708                                                              
Total length (>= 0 bp)                     2840873                                                                       2844561                                                              
Total length (>= 1000 bp)                  2752086                                                                       2772934                                                              
Reference length                           2813862                                                                       2813862                                                              
N50                                        64041                                                                         178963                                                               
NG50                                       64041                                                                         178963                                                               
N75                                        33696                                                                         106472                                                               
NG75                                       32693                                                                         106472                                                               
L50                                        15                                                                            5                                                                    
LG50                                       15                                                                            5                                                                    
L75                                        30                                                                            10                                                                   
LG75                                       31                                                                            10                                                                   
#local misassemblies                       2                                                                             26                                                                   
#misassemblies                             2                                                                             4                                                                    
#misassembled contigs                      2                                                                             3                                                                    
Misassembled contigs length                66990                                                                         308714                                                               
Misassemblies inter-contig overlap         0                                                                             686                                                                  
#unaligned contigs                         1 + 0 part                                                                    1 + 0 part                                                           
Unaligned contigs length                   5385                                                                          5385                                                                 
#ambiguously mapped contigs                13                                                                            13                                                                   
Extra bases in ambiguously mapped contigs  -8604                                                                         -8604                                                                
#N's                                       29                                                                            3717                                                                 
#N's per 100 kbp                           1.04                                                                          133.57                                                               
Genome fraction (%)                        98.150                                                                        98.324                                                               
Duplication ratio                          1.000                                                                         1.001                                                                
#genes                                     2635 + 56 part                                                                2674 + 19 part                                                       
#predicted genes (unique)                  2670                                                                          2645                                                                 
#predicted genes (>= 0 bp)                 2675                                                                          2646                                                                 
#predicted genes (>= 300 bp)               2285                                                                          2287                                                                 
#predicted genes (>= 1500 bp)              299                                                                           299                                                                  
#predicted genes (>= 3000 bp)              27                                                                            29                                                                   
#mismatches                                33                                                                            37                                                                   
#indels                                    19                                                                            638                                                                  
Indels length                              70                                                                            2434                                                                 
#mismatches per 100 kbp                    1.19                                                                          1.34                                                                 
#indels per 100 kbp                        0.69                                                                          23.06                                                                
GC (%)                                     32.69                                                                         32.69                                                                
Reference GC (%)                           32.81                                                                         32.81                                                                
Average %IDY                               98.835                                                                        98.716                                                               
Largest alignment                          207782                                                                        428001                                                               
NA50                                       57711                                                                         178922                                                               
NGA50                                      57711                                                                         178922                                                               
NA75                                       33696                                                                         106391                                                               
NGA75                                      32693                                                                         86129                                                                
LA50                                       15                                                                            5                                                                    
LGA50                                      15                                                                            5                                                                    
LA75                                       30                                                                            10                                                                   
LGA75                                      31                                                                            11                                                                   
#Mis_misassemblies                         2                                                                             4                                                                    
#Mis_relocations                           2                                                                             4                                                                    
#Mis_translocations                        0                                                                             0                                                                    
#Mis_inversions                            0                                                                             0                                                                    
#Mis_misassembled contigs                  2                                                                             3                                                                    
Mis_Misassembled contigs length            66990                                                                         308714                                                               
#Mis_local misassemblies                   2                                                                             26                                                                   
#Mis_short indels (<= 5 bp)                15                                                                            574                                                                  
#Mis_long indels (> 5 bp)                  4                                                                             64                                                                   
#Una_fully unaligned contigs               1                                                                             1                                                                    
Una_Fully unaligned length                 5385                                                                          5385                                                                 
#Una_partially unaligned contigs           0                                                                             0                                                                    
#Una_with misassembly                      0                                                                             0                                                                    
#Una_both parts are significant            0                                                                             0                                                                    
Una_Partially unaligned length             0                                                                             0                                                                    
GAGE_Contigs #                             141                                                                           53                                                                   
GAGE_Min contig                            204                                                                           214                                                                  
GAGE_Max contig                            207782                                                                        428083                                                               
GAGE_N50                                   64041 COUNT: 15                                                               178963 COUNT: 5                                                      
GAGE_Genome size                           2813862                                                                       2813862                                                              
GAGE_Assembly size                         2776559                                                                       2782708                                                              
GAGE_Chaff bases                           0                                                                             0                                                                    
GAGE_Missing reference bases               10108(0.36%)                                                                  8663(0.31%)                                                          
GAGE_Missing assembly bases                5411(0.19%)                                                                   8800(0.32%)                                                          
GAGE_Missing assembly contigs              1(0.71%)                                                                      1(1.89%)                                                             
GAGE_Duplicated reference bases            261                                                                           840                                                                  
GAGE_Compressed reference bases            44020                                                                         42208                                                                
GAGE_Bad trim                              0                                                                             304                                                                  
GAGE_Avg idy                               99.99                                                                         99.98                                                                
GAGE_SNPs                                  30                                                                            30                                                                   
GAGE_Indels < 5bp                          13                                                                            50                                                                   
GAGE_Indels >= 5                           4                                                                             52                                                                   
GAGE_Inversions                            0                                                                             0                                                                    
GAGE_Relocation                            3                                                                             14                                                                   
GAGE_Translocation                         0                                                                             0                                                                    
GAGE_Corrected contig #                    143                                                                           136                                                                  
GAGE_Corrected assembly size               2770134                                                                       2770914                                                              
GAGE_Min correct contig                    204                                                                           204                                                                  
GAGE_Max correct contig                    207782                                                                        207782                                                               
GAGE_Corrected N50                         57711 COUNT: 15                                                               57711 COUNT: 15                                                      
