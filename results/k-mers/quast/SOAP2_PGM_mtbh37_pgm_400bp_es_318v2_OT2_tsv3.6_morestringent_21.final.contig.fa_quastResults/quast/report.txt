All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_21.final.contig
#Contigs (>= 0 bp)             484512                                                                      
#Contigs (>= 1000 bp)          0                                                                           
Total length (>= 0 bp)         17721623                                                                    
Total length (>= 1000 bp)      0                                                                           
#Contigs                       741                                                                         
Largest contig                 853                                                                         
Total length                   194489                                                                      
Reference length               4411532                                                                     
GC (%)                         70.19                                                                       
Reference GC (%)               65.61                                                                       
N50                            248                                                                         
NG50                           None                                                                        
N75                            218                                                                         
NG75                           None                                                                        
#misassemblies                 0                                                                           
#local misassemblies           1                                                                           
#unaligned contigs             8 + 2 part                                                                  
Unaligned contigs length       2304                                                                        
Genome fraction (%)            4.355                                                                       
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        5.73                                                                        
#indels per 100 kbp            51.01                                                                       
#genes                         1 + 599 part                                                                
#predicted genes (unique)      700                                                                         
#predicted genes (>= 0 bp)     700                                                                         
#predicted genes (>= 300 bp)   114                                                                         
#predicted genes (>= 1500 bp)  0                                                                           
#predicted genes (>= 3000 bp)  0                                                                           
Largest alignment              853                                                                         
NA50                           247                                                                         
NGA50                          None                                                                        
NA75                           217                                                                         
NGA75                          None                                                                        
