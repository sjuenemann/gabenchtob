All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_124-unitigs
#Contigs (>= 0 bp)             3908                                                                    
#Contigs (>= 1000 bp)          726                                                                     
Total length (>= 0 bp)         5889640                                                                 
Total length (>= 1000 bp)      5128220                                                                 
#Contigs                       2697                                                                    
Largest contig                 47635                                                                   
Total length                   5702242                                                                 
Reference length               5594470                                                                 
GC (%)                         50.50                                                                   
Reference GC (%)               50.48                                                                   
N50                            10306                                                                   
NG50                           10658                                                                   
N75                            4803                                                                    
NG75                           5113                                                                    
#misassemblies                 9                                                                       
#local misassemblies           1                                                                       
#unaligned contigs             1 + 6 part                                                              
Unaligned contigs length       417                                                                     
Genome fraction (%)            94.558                                                                  
Duplication ratio              1.045                                                                   
#N's per 100 kbp               0.00                                                                    
#mismatches per 100 kbp        0.96                                                                    
#indels per 100 kbp            8.70                                                                    
#genes                         4448 + 731 part                                                         
#predicted genes (unique)      7412                                                                    
#predicted genes (>= 0 bp)     7469                                                                    
#predicted genes (>= 300 bp)   4709                                                                    
#predicted genes (>= 1500 bp)  582                                                                     
#predicted genes (>= 3000 bp)  49                                                                      
Largest alignment              47635                                                                   
NA50                           10292                                                                   
NGA50                          10583                                                                   
NA75                           4775                                                                    
NGA75                          5113                                                                    
