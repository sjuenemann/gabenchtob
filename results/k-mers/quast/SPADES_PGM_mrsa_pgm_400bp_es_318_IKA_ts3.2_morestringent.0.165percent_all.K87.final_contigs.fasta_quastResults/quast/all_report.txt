All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K87.final_contigs
#Contigs                                   4247                                                                                       
#Contigs (>= 0 bp)                         4609                                                                                       
#Contigs (>= 1000 bp)                      70                                                                                         
Largest contig                             207849                                                                                     
Total length                               4004037                                                                                    
Total length (>= 0 bp)                     4047757                                                                                    
Total length (>= 1000 bp)                  2754661                                                                                    
Reference length                           2813862                                                                                    
N50                                        41444                                                                                      
NG50                                       87667                                                                                      
N75                                        362                                                                                        
NG75                                       37281                                                                                      
L50                                        20                                                                                         
LG50                                       10                                                                                         
L75                                        685                                                                                        
LG75                                       23                                                                                         
#local misassemblies                       9                                                                                          
#misassemblies                             7                                                                                          
#misassembled contigs                      7                                                                                          
Misassembled contigs length                71183                                                                                      
Misassemblies inter-contig overlap         2371                                                                                       
#unaligned contigs                         1511 + 90 part                                                                             
Unaligned contigs length                   455388                                                                                     
#ambiguously mapped contigs                23                                                                                         
Extra bases in ambiguously mapped contigs  -12645                                                                                     
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        98.360                                                                                     
Duplication ratio                          1.278                                                                                      
#genes                                     2648 + 55 part                                                                             
#predicted genes (unique)                  5340                                                                                       
#predicted genes (>= 0 bp)                 5347                                                                                       
#predicted genes (>= 300 bp)               2324                                                                                       
#predicted genes (>= 1500 bp)              289                                                                                        
#predicted genes (>= 3000 bp)              27                                                                                         
#mismatches                                178                                                                                        
#indels                                    430                                                                                        
Indels length                              450                                                                                        
#mismatches per 100 kbp                    6.43                                                                                       
#indels per 100 kbp                        15.54                                                                                      
GC (%)                                     32.23                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               97.408                                                                                     
Largest alignment                          207849                                                                                     
NA50                                       41444                                                                                      
NGA50                                      87667                                                                                      
NA75                                       324                                                                                        
NGA75                                      37281                                                                                      
LA50                                       20                                                                                         
LGA50                                      10                                                                                         
LA75                                       745                                                                                        
LGA75                                      23                                                                                         
#Mis_misassemblies                         7                                                                                          
#Mis_relocations                           6                                                                                          
#Mis_translocations                        1                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  7                                                                                          
Mis_Misassembled contigs length            71183                                                                                      
#Mis_local misassemblies                   9                                                                                          
#Mis_short indels (<= 5 bp)                428                                                                                        
#Mis_long indels (> 5 bp)                  2                                                                                          
#Una_fully unaligned contigs               1511                                                                                       
Una_Fully unaligned length                 450091                                                                                     
#Una_partially unaligned contigs           90                                                                                         
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             5297                                                                                       
GAGE_Contigs #                             4247                                                                                       
GAGE_Min contig                            200                                                                                        
GAGE_Max contig                            207849                                                                                     
GAGE_N50                                   87667 COUNT: 10                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         4004037                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               2376(0.08%)                                                                                
GAGE_Missing assembly bases                205914(5.14%)                                                                              
GAGE_Missing assembly contigs              565(13.30%)                                                                                
GAGE_Duplicated reference bases            1016443                                                                                    
GAGE_Compressed reference bases            41562                                                                                      
GAGE_Bad trim                              32210                                                                                      
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  50                                                                                         
GAGE_Indels < 5bp                          280                                                                                        
GAGE_Indels >= 5                           9                                                                                          
GAGE_Inversions                            1                                                                                          
GAGE_Relocation                            3                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    150                                                                                        
GAGE_Corrected assembly size               2783324                                                                                    
GAGE_Min correct contig                    200                                                                                        
GAGE_Max correct contig                    207869                                                                                     
GAGE_Corrected N50                         70445 COUNT: 12                                                                            
