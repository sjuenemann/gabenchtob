All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_155.final.contigs
#Contigs                                   6691                                                                        
#Contigs (>= 0 bp)                         6691                                                                        
#Contigs (>= 1000 bp)                      658                                                                         
Largest contig                             5274                                                                        
Total length                               3975045                                                                     
Total length (>= 0 bp)                     3975045                                                                     
Total length (>= 1000 bp)                  918086                                                                      
Reference length                           5594470                                                                     
N50                                        646                                                                         
NG50                                       462                                                                         
N75                                        430                                                                         
NG75                                       None                                                                        
L50                                        2008                                                                        
LG50                                       3489                                                                        
L75                                        3902                                                                        
LG75                                       None                                                                        
#local misassemblies                       1                                                                           
#misassemblies                             135                                                                         
#misassembled contigs                      135                                                                         
Misassembled contigs length                93462                                                                       
Misassemblies inter-contig overlap         410                                                                         
#unaligned contigs                         0 + 2 part                                                                  
Unaligned contigs length                   40                                                                          
#ambiguously mapped contigs                123                                                                         
Extra bases in ambiguously mapped contigs  -44629                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        65.113                                                                      
Duplication ratio                          1.079                                                                       
#genes                                     819 + 3495 part                                                             
#predicted genes (unique)                  8918                                                                        
#predicted genes (>= 0 bp)                 8969                                                                        
#predicted genes (>= 300 bp)               5297                                                                        
#predicted genes (>= 1500 bp)              17                                                                          
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                64                                                                          
#indels                                    1150                                                                        
Indels length                              1162                                                                        
#mismatches per 100 kbp                    1.76                                                                        
#indels per 100 kbp                        31.57                                                                       
GC (%)                                     50.08                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.659                                                                      
Largest alignment                          5274                                                                        
NA50                                       640                                                                         
NGA50                                      455                                                                         
NA75                                       426                                                                         
NGA75                                      None                                                                        
LA50                                       2020                                                                        
LGA50                                      3518                                                                        
LA75                                       3937                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         135                                                                         
#Mis_relocations                           127                                                                         
#Mis_translocations                        8                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  135                                                                         
Mis_Misassembled contigs length            93462                                                                       
#Mis_local misassemblies                   1                                                                           
#Mis_short indels (<= 5 bp)                1150                                                                        
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           2                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             40                                                                          
GAGE_Contigs #                             6691                                                                        
GAGE_Min contig                            309                                                                         
GAGE_Max contig                            5274                                                                        
GAGE_N50                                   462 COUNT: 3489                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         3975045                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               1778901(31.80%)                                                             
GAGE_Missing assembly bases                67(0.00%)                                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            144806                                                                      
GAGE_Compressed reference bases            192254                                                                      
GAGE_Bad trim                              66                                                                          
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  47                                                                          
GAGE_Indels < 5bp                          953                                                                         
GAGE_Indels >= 5                           1                                                                           
GAGE_Inversions                            34                                                                          
GAGE_Relocation                            13                                                                          
GAGE_Translocation                         8                                                                           
GAGE_Corrected contig #                    6321                                                                        
GAGE_Corrected assembly size               3818512                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    5272                                                                        
GAGE_Corrected N50                         455 COUNT: 3518                                                             
