All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_155.final.contigs
GAGE_Contigs #                   6691                                                                        
GAGE_Min contig                  309                                                                         
GAGE_Max contig                  5274                                                                        
GAGE_N50                         462 COUNT: 3489                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               3975045                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1778901(31.80%)                                                             
GAGE_Missing assembly bases      67(0.00%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  144806                                                                      
GAGE_Compressed reference bases  192254                                                                      
GAGE_Bad trim                    66                                                                          
GAGE_Avg idy                     99.97                                                                       
GAGE_SNPs                        47                                                                          
GAGE_Indels < 5bp                953                                                                         
GAGE_Indels >= 5                 1                                                                           
GAGE_Inversions                  34                                                                          
GAGE_Relocation                  13                                                                          
GAGE_Translocation               8                                                                           
GAGE_Corrected contig #          6321                                                                        
GAGE_Corrected assembly size     3818512                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          5272                                                                        
GAGE_Corrected N50               455 COUNT: 3518                                                             
