All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_58-unitigs
#Mis_misassemblies               2                                                                      
#Mis_relocations                 2                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        2                                                                      
Mis_Misassembled contigs length  20501                                                                  
#Mis_local misassemblies         5                                                                      
#mismatches                      50                                                                     
#indels                          226                                                                    
#Mis_short indels (<= 5 bp)      226                                                                    
#Mis_long indels (> 5 bp)        0                                                                      
Indels length                    230                                                                    
