All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_58-unitigs
#Contigs (>= 0 bp)             7492                                                                   
#Contigs (>= 1000 bp)          709                                                                    
Total length (>= 0 bp)         5850137                                                                
Total length (>= 1000 bp)      5022949                                                                
#Contigs                       1215                                                                   
Largest contig                 48419                                                                  
Total length                   5239844                                                                
Reference length               5594470                                                                
GC (%)                         50.24                                                                  
Reference GC (%)               50.48                                                                  
N50                            11275                                                                  
NG50                           10282                                                                  
N75                            5602                                                                   
NG75                           4537                                                                   
#misassemblies                 2                                                                      
#local misassemblies           5                                                                      
#unaligned contigs             0 + 0 part                                                             
Unaligned contigs length       0                                                                      
Genome fraction (%)            92.506                                                                 
Duplication ratio              1.001                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        0.97                                                                   
#indels per 100 kbp            4.37                                                                   
#genes                         4289 + 715 part                                                        
#predicted genes (unique)      5803                                                                   
#predicted genes (>= 0 bp)     5805                                                                   
#predicted genes (>= 300 bp)   4572                                                                   
#predicted genes (>= 1500 bp)  573                                                                    
#predicted genes (>= 3000 bp)  51                                                                     
Largest alignment              48419                                                                  
NA50                           11275                                                                  
NGA50                          10282                                                                  
NA75                           5602                                                                   
NGA75                          4529                                                                   
