All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_151.final.contigs
#Mis_misassemblies               61                                                                             
#Mis_relocations                 61                                                                             
#Mis_translocations              0                                                                              
#Mis_inversions                  0                                                                              
#Mis_misassembled contigs        60                                                                             
Mis_Misassembled contigs length  47375                                                                          
#Mis_local misassemblies         3                                                                              
#mismatches                      48                                                                             
#indels                          1124                                                                           
#Mis_short indels (<= 5 bp)      1123                                                                           
#Mis_long indels (> 5 bp)        1                                                                              
Indels length                    1181                                                                           
