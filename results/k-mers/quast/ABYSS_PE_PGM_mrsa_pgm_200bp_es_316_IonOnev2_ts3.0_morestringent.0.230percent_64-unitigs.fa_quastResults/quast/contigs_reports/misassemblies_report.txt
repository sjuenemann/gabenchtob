All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_64-unitigs
#Mis_misassemblies               1                                                                                      
#Mis_relocations                 1                                                                                      
#Mis_translocations              0                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        1                                                                                      
Mis_Misassembled contigs length  20147                                                                                  
#Mis_local misassemblies         4                                                                                      
#mismatches                      116                                                                                    
#indels                          316                                                                                    
#Mis_short indels (<= 5 bp)      316                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    324                                                                                    
