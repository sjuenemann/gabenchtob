All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_123.final.contigs
#Mis_misassemblies               361                                                                                    
#Mis_relocations                 333                                                                                    
#Mis_translocations              24                                                                                     
#Mis_inversions                  4                                                                                      
#Mis_misassembled contigs        361                                                                                    
Mis_Misassembled contigs length  108158                                                                                 
#Mis_local misassemblies         1                                                                                      
#mismatches                      1684                                                                                   
#indels                          20798                                                                                  
#Mis_short indels (<= 5 bp)      20798                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                      
Indels length                    21282                                                                                  
