All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_59.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_59.final.scaf
#Contigs                                   1028                                                                           323                                                                   
#Contigs (>= 0 bp)                         3389                                                                           2336                                                                  
#Contigs (>= 1000 bp)                      595                                                                            129                                                                   
Largest contig                             62308                                                                          374351                                                                
Total length                               5242821                                                                        5341432                                                               
Total length (>= 0 bp)                     5528146                                                                        5584815                                                               
Total length (>= 1000 bp)                  5073189                                                                        5263627                                                               
Reference length                           5594470                                                                        5594470                                                               
N50                                        13219                                                                          148285                                                                
NG50                                       12057                                                                          145174                                                                
N75                                        7216                                                                           76036                                                                 
NG75                                       5726                                                                           54877                                                                 
L50                                        115                                                                            12                                                                    
LG50                                       129                                                                            13                                                                    
L75                                        250                                                                            24                                                                    
LG75                                       291                                                                            27                                                                    
#local misassemblies                       3                                                                              191                                                                   
#misassemblies                             5                                                                              11                                                                    
#misassembled contigs                      5                                                                              9                                                                     
Misassembled contigs length                23502                                                                          513673                                                                
Misassemblies inter-contig overlap         5                                                                              5                                                                     
#unaligned contigs                         4 + 3 part                                                                     21 + 19 part                                                          
Unaligned contigs length                   7217                                                                           48047                                                                 
#ambiguously mapped contigs                144                                                                            116                                                                   
Extra bases in ambiguously mapped contigs  -67821                                                                         -55131                                                                
#N's                                       78                                                                             56749                                                                 
#N's per 100 kbp                           1.49                                                                           1062.43                                                               
Genome fraction (%)                        92.371                                                                         92.304                                                                
Duplication ratio                          1.000                                                                          1.014                                                                 
#genes                                     4371 + 630 part                                                                4767 + 217 part                                                       
#predicted genes (unique)                  5656                                                                           5526                                                                  
#predicted genes (>= 0 bp)                 5657                                                                           5527                                                                  
#predicted genes (>= 300 bp)               4500                                                                           4494                                                                  
#predicted genes (>= 1500 bp)              604                                                                            618                                                                   
#predicted genes (>= 3000 bp)              51                                                                             52                                                                    
#mismatches                                62                                                                             62                                                                    
#indels                                    36                                                                             3695                                                                  
Indels length                              263                                                                            13139                                                                 
#mismatches per 100 kbp                    1.20                                                                           1.20                                                                  
#indels per 100 kbp                        0.70                                                                           71.55                                                                 
GC (%)                                     50.22                                                                          50.24                                                                 
Reference GC (%)                           50.48                                                                          50.48                                                                 
Average %IDY                               99.956                                                                         99.648                                                                
Largest alignment                          62308                                                                          351719                                                                
NA50                                       13219                                                                          147798                                                                
NGA50                                      12057                                                                          145174                                                                
NA75                                       7189                                                                           71661                                                                 
NGA75                                      5706                                                                           46500                                                                 
LA50                                       115                                                                            13                                                                    
LGA50                                      129                                                                            14                                                                    
LA75                                       250                                                                            26                                                                    
LGA75                                      291                                                                            29                                                                    
#Mis_misassemblies                         5                                                                              11                                                                    
#Mis_relocations                           5                                                                              11                                                                    
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  5                                                                              9                                                                     
Mis_Misassembled contigs length            23502                                                                          513673                                                                
#Mis_local misassemblies                   3                                                                              191                                                                   
#Mis_short indels (<= 5 bp)                30                                                                             3271                                                                  
#Mis_long indels (> 5 bp)                  6                                                                              424                                                                   
#Una_fully unaligned contigs               4                                                                              21                                                                    
Una_Fully unaligned length                 6685                                                                           26717                                                                 
#Una_partially unaligned contigs           3                                                                              19                                                                    
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            0                                                                              8                                                                     
Una_Partially unaligned length             532                                                                            21330                                                                 
GAGE_Contigs #                             1028                                                                           323                                                                   
GAGE_Min contig                            200                                                                            200                                                                   
GAGE_Max contig                            62308                                                                          374351                                                                
GAGE_N50                                   12057 COUNT: 129                                                               145174 COUNT: 13                                                      
GAGE_Genome size                           5594470                                                                        5594470                                                               
GAGE_Assembly size                         5242821                                                                        5341432                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               232132(4.15%)                                                                  179175(3.20%)                                                         
GAGE_Missing assembly bases                5693(0.11%)                                                                    65422(1.22%)                                                          
GAGE_Missing assembly contigs              1(0.10%)                                                                       2(0.62%)                                                              
GAGE_Duplicated reference bases            1506                                                                           3776                                                                  
GAGE_Compressed reference bases            142213                                                                         165766                                                                
GAGE_Bad trim                              247                                                                            6423                                                                  
GAGE_Avg idy                               100.00                                                                         99.92                                                                 
GAGE_SNPs                                  76                                                                             76                                                                    
GAGE_Indels < 5bp                          27                                                                             227                                                                   
GAGE_Indels >= 5                           11                                                                             828                                                                   
GAGE_Inversions                            9                                                                              14                                                                    
GAGE_Relocation                            2                                                                              14                                                                    
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    1028                                                                           955                                                                   
GAGE_Corrected assembly size               5233524                                                                        5234301                                                               
GAGE_Min correct contig                    200                                                                            200                                                                   
GAGE_Max correct contig                    62308                                                                          75447                                                                 
GAGE_Corrected N50                         11999 COUNT: 130                                                               13219 COUNT: 118                                                      
