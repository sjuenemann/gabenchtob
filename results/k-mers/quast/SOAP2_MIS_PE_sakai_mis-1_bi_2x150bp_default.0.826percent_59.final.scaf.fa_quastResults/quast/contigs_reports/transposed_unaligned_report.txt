All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                       #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's 
SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_59.final.scaf_broken  4                             6685                        3                                 0                      0                                532                             78   
SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_59.final.scaf         21                            26717                       19                                0                      8                                21330                           56749
