reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  VELVET_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.460percent_191.final.contigs_broken  | 96.7632030284       | 1.0283838264      | 172         | 2178      | 516       | None      | None      |
  VELVET_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.460percent_191.final.contigs  | 96.7657617893       | 1.03215081229     | 172         | 2242      | 452       | None      | None      |
