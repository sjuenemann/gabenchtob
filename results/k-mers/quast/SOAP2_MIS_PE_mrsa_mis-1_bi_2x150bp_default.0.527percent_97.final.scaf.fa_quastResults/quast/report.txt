All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_97.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_97.final.scaf
#Contigs (>= 0 bp)             475                                                                           385                                                                  
#Contigs (>= 1000 bp)          88                                                                            43                                                                   
Total length (>= 0 bp)         2829745                                                                       2832642                                                              
Total length (>= 1000 bp)      2757067                                                                       2776521                                                              
#Contigs                       150                                                                           74                                                                   
Largest contig                 147524                                                                        311434                                                               
Total length                   2783483                                                                       2788270                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         32.70                                                                         32.70                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            57289                                                                         162509                                                               
NG50                           57289                                                                         162509                                                               
N75                            32167                                                                         79790                                                                
NG75                           32167                                                                         79790                                                                
#misassemblies                 2                                                                             4                                                                    
#local misassemblies           1                                                                             23                                                                   
#unaligned contigs             1 + 0 part                                                                    1 + 0 part                                                           
Unaligned contigs length       5385                                                                          5385                                                                 
Genome fraction (%)            98.324                                                                        98.438                                                               
Duplication ratio              1.001                                                                         1.001                                                                
#N's per 100 kbp               2.91                                                                          106.80                                                               
#mismatches per 100 kbp        1.08                                                                          1.19                                                                 
#indels per 100 kbp            0.87                                                                          21.59                                                                
#genes                         2635 + 63 part                                                                2666 + 31 part                                                       
#predicted genes (unique)      2676                                                                          2653                                                                 
#predicted genes (>= 0 bp)     2677                                                                          2654                                                                 
#predicted genes (>= 300 bp)   2293                                                                          2291                                                                 
#predicted genes (>= 1500 bp)  299                                                                           299                                                                  
#predicted genes (>= 3000 bp)  29                                                                            29                                                                   
Largest alignment              147524                                                                        311423                                                               
NA50                           57289                                                                         162464                                                               
NGA50                          57289                                                                         162464                                                               
NA75                           32167                                                                         61403                                                                
NGA75                          30399                                                                         61403                                                                
