All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_97.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_97.final.scaf
#Mis_misassemblies               2                                                                             4                                                                    
#Mis_relocations                 2                                                                             4                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        2                                                                             3                                                                    
Mis_Misassembled contigs length  68523                                                                         275930                                                               
#Mis_local misassemblies         1                                                                             23                                                                   
#mismatches                      30                                                                            33                                                                   
#indels                          24                                                                            598                                                                  
#Mis_short indels (<= 5 bp)      11                                                                            545                                                                  
#Mis_long indels (> 5 bp)        13                                                                            53                                                                   
Indels length                    252                                                                           1775                                                                 
