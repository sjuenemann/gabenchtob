All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K39.final_contigs
GAGE_Contigs #                   357                                                                                
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  158460                                                                             
GAGE_N50                         39393 COUNT: 34                                                                    
GAGE_Genome size                 4411532                                                                            
GAGE_Assembly size               4301249                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     65303(1.48%)                                                                       
GAGE_Missing assembly bases      5720(0.13%)                                                                        
GAGE_Missing assembly contigs    18(5.04%)                                                                          
GAGE_Duplicated reference bases  0                                                                                  
GAGE_Compressed reference bases  55670                                                                              
GAGE_Bad trim                    444                                                                                
GAGE_Avg idy                     99.96                                                                              
GAGE_SNPs                        281                                                                                
GAGE_Indels < 5bp                1081                                                                               
GAGE_Indels >= 5                 39                                                                                 
GAGE_Inversions                  6                                                                                  
GAGE_Relocation                  17                                                                                 
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          394                                                                                
GAGE_Corrected assembly size     4299141                                                                            
GAGE_Min correct contig          203                                                                                
GAGE_Max correct contig          96004                                                                              
GAGE_Corrected N50               30314 COUNT: 44                                                                    
