All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_110-unitigs
#Mis_misassemblies               35                                                                                      
#Mis_relocations                 19                                                                                      
#Mis_translocations              1                                                                                       
#Mis_inversions                  15                                                                                      
#Mis_misassembled contigs        35                                                                                      
Mis_Misassembled contigs length  54534                                                                                   
#Mis_local misassemblies         2                                                                                       
#mismatches                      89                                                                                      
#indels                          1220                                                                                    
#Mis_short indels (<= 5 bp)      1220                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                       
Indels length                    1255                                                                                    
