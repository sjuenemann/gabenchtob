All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_37.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_37.final.scaf
#Contigs (>= 0 bp)             78452                                                                          77890                                                                 
#Contigs (>= 1000 bp)          434                                                                            491                                                                   
Total length (>= 0 bp)         13884935                                                                       13917862                                                              
Total length (>= 1000 bp)      605104                                                                         744400                                                                
#Contigs                       9887                                                                           9722                                                                  
Largest contig                 4921                                                                           9308                                                                  
Total length                   4426175                                                                        4514559                                                               
Reference length               5594470                                                                        5594470                                                               
GC (%)                         49.70                                                                          49.72                                                                 
Reference GC (%)               50.48                                                                          50.48                                                                 
N50                            493                                                                            514                                                                   
NG50                           402                                                                            422                                                                   
N75                            330                                                                            341                                                                   
NG75                           225                                                                            236                                                                   
#misassemblies                 2                                                                              2                                                                     
#local misassemblies           1                                                                              117                                                                   
#unaligned contigs             44 + 3 part                                                                    182 + 47 part                                                         
Unaligned contigs length       12765                                                                          135520                                                                
Genome fraction (%)            76.123                                                                         75.140                                                                
Duplication ratio              1.032                                                                          1.037                                                                 
#N's per 100 kbp               1.81                                                                           731.17                                                                
#mismatches per 100 kbp        2.16                                                                           2.00                                                                  
#indels per 100 kbp            1.01                                                                           51.29                                                                 
#genes                         668 + 4103 part                                                                696 + 4035 part                                                       
#predicted genes (unique)      11761                                                                          11899                                                                 
#predicted genes (>= 0 bp)     11761                                                                          11899                                                                 
#predicted genes (>= 300 bp)   5129                                                                           5148                                                                  
#predicted genes (>= 1500 bp)  17                                                                             18                                                                    
#predicted genes (>= 3000 bp)  1                                                                              1                                                                     
Largest alignment              4921                                                                           9240                                                                  
NA50                           493                                                                            488                                                                   
NGA50                          402                                                                            403                                                                   
NA75                           329                                                                            319                                                                   
NGA75                          223                                                                            216                                                                   
