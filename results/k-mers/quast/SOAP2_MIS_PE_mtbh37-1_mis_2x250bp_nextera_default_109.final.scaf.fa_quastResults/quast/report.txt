All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_109.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_109.final.scaf
#Contigs (>= 0 bp)             2170                                                                     1529                                                            
#Contigs (>= 1000 bp)          644                                                                      132                                                             
Total length (>= 0 bp)         4579251                                                                  4611064                                                         
Total length (>= 1000 bp)      4209091                                                                  4302188                                                         
#Contigs                       1940                                                                     1325                                                            
Largest contig                 41698                                                                    222161                                                          
Total length                   4547731                                                                  4582913                                                         
Reference length               4411532                                                                  4411532                                                         
GC (%)                         65.64                                                                    65.64                                                           
Reference GC (%)               65.61                                                                    65.61                                                           
N50                            8885                                                                     63012                                                           
NG50                           9068                                                                     63453                                                           
N75                            4495                                                                     31829                                                           
NG75                           4828                                                                     36362                                                           
#misassemblies                 19                                                                       19                                                              
#local misassemblies           23                                                                       538                                                             
#unaligned contigs             2 + 4 part                                                               2 + 4 part                                                      
Unaligned contigs length       1579                                                                     2421                                                            
Genome fraction (%)            98.280                                                                   98.437                                                          
Duplication ratio              1.042                                                                    1.048                                                           
#N's per 100 kbp               3.30                                                                     697.44                                                          
#mismatches per 100 kbp        11.30                                                                    11.12                                                           
#indels per 100 kbp            1.50                                                                     25.88                                                           
#genes                         3382 + 684 part                                                          3457 + 611 part                                                 
#predicted genes (unique)      5517                                                                     5327                                                            
#predicted genes (>= 0 bp)     5565                                                                     5374                                                            
#predicted genes (>= 300 bp)   3723                                                                     3660                                                            
#predicted genes (>= 1500 bp)  495                                                                      515                                                             
#predicted genes (>= 3000 bp)  57                                                                       61                                                              
Largest alignment              41689                                                                    220471                                                          
NA50                           8860                                                                     61040                                                           
NGA50                          9059                                                                     62789                                                           
NA75                           4493                                                                     30643                                                           
NGA75                          4828                                                                     35871                                                           
