All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_42-unitigs
#Contigs                                   1231                                                                             
#Contigs (>= 0 bp)                         7738                                                                             
#Contigs (>= 1000 bp)                      765                                                                              
Largest contig                             63911                                                                            
Total length                               5206209                                                                          
Total length (>= 0 bp)                     5697338                                                                          
Total length (>= 1000 bp)                  5006606                                                                          
Reference length                           5594470                                                                          
N50                                        9799                                                                             
NG50                                       9055                                                                             
N75                                        5176                                                                             
NG75                                       4197                                                                             
L50                                        159                                                                              
LG50                                       179                                                                              
L75                                        337                                                                              
LG75                                       401                                                                              
#local misassemblies                       4                                                                                
#misassemblies                             2                                                                                
#misassembled contigs                      2                                                                                
Misassembled contigs length                8546                                                                             
Misassemblies inter-contig overlap         317                                                                              
#unaligned contigs                         0 + 0 part                                                                       
Unaligned contigs length                   0                                                                                
#ambiguously mapped contigs                129                                                                              
Extra bases in ambiguously mapped contigs  -60045                                                                           
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        91.920                                                                           
Duplication ratio                          1.001                                                                            
#genes                                     4243 + 692 part                                                                  
#predicted genes (unique)                  6170                                                                             
#predicted genes (>= 0 bp)                 6170                                                                             
#predicted genes (>= 300 bp)               4752                                                                             
#predicted genes (>= 1500 bp)              512                                                                              
#predicted genes (>= 3000 bp)              37                                                                               
#mismatches                                70                                                                               
#indels                                    1108                                                                             
Indels length                              1148                                                                             
#mismatches per 100 kbp                    1.36                                                                             
#indels per 100 kbp                        21.55                                                                            
GC (%)                                     50.20                                                                            
Reference GC (%)                           50.48                                                                            
Average %IDY                               99.890                                                                           
Largest alignment                          63911                                                                            
NA50                                       9799                                                                             
NGA50                                      9055                                                                             
NA75                                       5149                                                                             
NGA75                                      4197                                                                             
LA50                                       159                                                                              
LGA50                                      179                                                                              
LA75                                       338                                                                              
LGA75                                      401                                                                              
#Mis_misassemblies                         2                                                                                
#Mis_relocations                           2                                                                                
#Mis_translocations                        0                                                                                
#Mis_inversions                            0                                                                                
#Mis_misassembled contigs                  2                                                                                
Mis_Misassembled contigs length            8546                                                                             
#Mis_local misassemblies                   4                                                                                
#Mis_short indels (<= 5 bp)                1108                                                                             
#Mis_long indels (> 5 bp)                  0                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           0                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             0                                                                                
GAGE_Contigs #                             1231                                                                             
GAGE_Min contig                            200                                                                              
GAGE_Max contig                            63911                                                                            
GAGE_N50                                   9055 COUNT: 179                                                                  
GAGE_Genome size                           5594470                                                                          
GAGE_Assembly size                         5206209                                                                          
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               291159(5.20%)                                                                    
GAGE_Missing assembly bases                2(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            0                                                                                
GAGE_Compressed reference bases            105087                                                                           
GAGE_Bad trim                              2                                                                                
GAGE_Avg idy                               99.98                                                                            
GAGE_SNPs                                  106                                                                              
GAGE_Indels < 5bp                          1150                                                                             
GAGE_Indels >= 5                           4                                                                                
GAGE_Inversions                            0                                                                                
GAGE_Relocation                            2                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    1237                                                                             
GAGE_Corrected assembly size               5207658                                                                          
GAGE_Min correct contig                    200                                                                              
GAGE_Max correct contig                    63928                                                                            
GAGE_Corrected N50                         8949 COUNT: 180                                                                  
