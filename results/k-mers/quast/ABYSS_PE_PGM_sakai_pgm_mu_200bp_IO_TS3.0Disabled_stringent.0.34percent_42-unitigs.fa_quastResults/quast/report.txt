All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_42-unitigs
#Contigs (>= 0 bp)             7738                                                                             
#Contigs (>= 1000 bp)          765                                                                              
Total length (>= 0 bp)         5697338                                                                          
Total length (>= 1000 bp)      5006606                                                                          
#Contigs                       1231                                                                             
Largest contig                 63911                                                                            
Total length                   5206209                                                                          
Reference length               5594470                                                                          
GC (%)                         50.20                                                                            
Reference GC (%)               50.48                                                                            
N50                            9799                                                                             
NG50                           9055                                                                             
N75                            5176                                                                             
NG75                           4197                                                                             
#misassemblies                 2                                                                                
#local misassemblies           4                                                                                
#unaligned contigs             0 + 0 part                                                                       
Unaligned contigs length       0                                                                                
Genome fraction (%)            91.920                                                                           
Duplication ratio              1.001                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        1.36                                                                             
#indels per 100 kbp            21.55                                                                            
#genes                         4243 + 692 part                                                                  
#predicted genes (unique)      6170                                                                             
#predicted genes (>= 0 bp)     6170                                                                             
#predicted genes (>= 300 bp)   4752                                                                             
#predicted genes (>= 1500 bp)  512                                                                              
#predicted genes (>= 3000 bp)  37                                                                               
Largest alignment              63911                                                                            
NA50                           9799                                                                             
NGA50                          9055                                                                             
NA75                           5149                                                                             
NGA75                          4197                                                                             
