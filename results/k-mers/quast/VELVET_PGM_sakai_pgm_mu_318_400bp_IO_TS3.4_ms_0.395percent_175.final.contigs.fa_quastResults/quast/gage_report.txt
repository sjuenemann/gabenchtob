All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_175.final.contigs
GAGE_Contigs #                   2784                                                                        
GAGE_Min contig                  349                                                                         
GAGE_Max contig                  11556                                                                       
GAGE_N50                         1832 COUNT: 853                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               4525261                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1037096(18.54%)                                                             
GAGE_Missing assembly bases      213(0.00%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  44034                                                                       
GAGE_Compressed reference bases  226962                                                                      
GAGE_Bad trim                    111                                                                         
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        77                                                                          
GAGE_Indels < 5bp                625                                                                         
GAGE_Indels >= 5                 11                                                                          
GAGE_Inversions                  2                                                                           
GAGE_Relocation                  4                                                                           
GAGE_Translocation               1                                                                           
GAGE_Corrected contig #          2729                                                                        
GAGE_Corrected assembly size     4479914                                                                     
GAGE_Min correct contig          254                                                                         
GAGE_Max correct contig          11555                                                                       
GAGE_Corrected N50               1812 COUNT: 857                                                             
