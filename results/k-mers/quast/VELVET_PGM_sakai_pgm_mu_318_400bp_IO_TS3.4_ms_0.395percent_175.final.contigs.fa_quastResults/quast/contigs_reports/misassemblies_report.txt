All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_175.final.contigs
#Mis_misassemblies               57                                                                          
#Mis_relocations                 53                                                                          
#Mis_translocations              4                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        57                                                                          
Mis_Misassembled contigs length  104242                                                                      
#Mis_local misassemblies         14                                                                          
#mismatches                      77                                                                          
#indels                          683                                                                         
#Mis_short indels (<= 5 bp)      683                                                                         
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    692                                                                         
