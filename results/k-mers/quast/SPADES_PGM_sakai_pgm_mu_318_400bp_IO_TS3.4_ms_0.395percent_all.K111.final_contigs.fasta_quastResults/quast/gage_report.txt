All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K111.final_contigs
GAGE_Contigs #                   5106                                                                             
GAGE_Min contig                  201                                                                              
GAGE_Max contig                  352334                                                                           
GAGE_N50                         140560 COUNT: 14                                                                 
GAGE_Genome size                 5594470                                                                          
GAGE_Assembly size               6805579                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     5260(0.09%)                                                                      
GAGE_Missing assembly bases      33962(0.50%)                                                                     
GAGE_Missing assembly contigs    61(1.19%)                                                                        
GAGE_Duplicated reference bases  1389886                                                                          
GAGE_Compressed reference bases  279352                                                                           
GAGE_Bad trim                    15111                                                                            
GAGE_Avg idy                     99.98                                                                            
GAGE_SNPs                        207                                                                              
GAGE_Indels < 5bp                517                                                                              
GAGE_Indels >= 5                 8                                                                                
GAGE_Inversions                  2                                                                                
GAGE_Relocation                  11                                                                               
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          420                                                                              
GAGE_Corrected assembly size     5386109                                                                          
GAGE_Min correct contig          201                                                                              
GAGE_Max correct contig          238398                                                                           
GAGE_Corrected N50               111207 COUNT: 17                                                                 
