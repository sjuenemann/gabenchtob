All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K119.final_contigs
#Una_fully unaligned contigs      230                                                                                              
Una_Fully unaligned length        65568                                                                                            
#Una_partially unaligned contigs  366                                                                                              
#Una_with misassembly             0                                                                                                
#Una_both parts are significant   1                                                                                                
Una_Partially unaligned length    23556                                                                                            
#N's                              0                                                                                                
