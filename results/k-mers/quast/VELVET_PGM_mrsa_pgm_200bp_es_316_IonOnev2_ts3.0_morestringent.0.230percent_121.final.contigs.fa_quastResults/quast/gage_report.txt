All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_121.final.contigs
GAGE_Contigs #                   2731                                                                                        
GAGE_Min contig                  241                                                                                         
GAGE_Max contig                  5440                                                                                        
GAGE_N50                         834 COUNT: 1028                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               2330045                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     545625(19.39%)                                                                              
GAGE_Missing assembly bases      2871(0.12%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  2238                                                                                        
GAGE_Compressed reference bases  36749                                                                                       
GAGE_Bad trim                    2860                                                                                        
GAGE_Avg idy                     99.92                                                                                       
GAGE_SNPs                        106                                                                                         
GAGE_Indels < 5bp                1282                                                                                        
GAGE_Indels >= 5                 1                                                                                           
GAGE_Inversions                  4                                                                                           
GAGE_Relocation                  2                                                                                           
GAGE_Translocation               1                                                                                           
GAGE_Corrected contig #          2730                                                                                        
GAGE_Corrected assembly size     2325038                                                                                     
GAGE_Min correct contig          237                                                                                         
GAGE_Max correct contig          5442                                                                                        
GAGE_Corrected N50               833 COUNT: 1030                                                                             
