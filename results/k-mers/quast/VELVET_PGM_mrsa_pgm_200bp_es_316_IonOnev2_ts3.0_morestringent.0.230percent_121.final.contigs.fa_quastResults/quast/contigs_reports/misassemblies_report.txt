All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_121.final.contigs
#Mis_misassemblies               21                                                                                          
#Mis_relocations                 6                                                                                           
#Mis_translocations              1                                                                                           
#Mis_inversions                  14                                                                                          
#Mis_misassembled contigs        21                                                                                          
Mis_Misassembled contigs length  15347                                                                                       
#Mis_local misassemblies         0                                                                                           
#mismatches                      145                                                                                         
#indels                          1528                                                                                        
#Mis_short indels (<= 5 bp)      1528                                                                                        
#Mis_long indels (> 5 bp)        0                                                                                           
Indels length                    1556                                                                                        
