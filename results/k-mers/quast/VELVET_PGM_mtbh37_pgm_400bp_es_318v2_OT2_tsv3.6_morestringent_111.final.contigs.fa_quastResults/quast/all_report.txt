All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_111.final.contigs
#Contigs                                   17911                                                                          
#Contigs (>= 0 bp)                         17911                                                                          
#Contigs (>= 1000 bp)                      0                                                                              
Largest contig                             853                                                                            
Total length                               4422949                                                                        
Total length (>= 0 bp)                     4422949                                                                        
Total length (>= 1000 bp)                  0                                                                              
Reference length                           4411532                                                                        
N50                                        234                                                                            
NG50                                       234                                                                            
N75                                        221                                                                            
NG75                                       221                                                                            
L50                                        7997                                                                           
LG50                                       7973                                                                           
L75                                        12908                                                                          
LG75                                       12869                                                                          
#local misassemblies                       5                                                                              
#misassemblies                             302                                                                            
#misassembled contigs                      302                                                                            
Misassembled contigs length                83067                                                                          
Misassemblies inter-contig overlap         903                                                                            
#unaligned contigs                         43 + 17 part                                                                   
Unaligned contigs length                   12643                                                                          
#ambiguously mapped contigs                190                                                                            
Extra bases in ambiguously mapped contigs  -44406                                                                         
#N's                                       0                                                                              
#N's per 100 kbp                           0.00                                                                           
Genome fraction (%)                        56.618                                                                         
Duplication ratio                          1.748                                                                          
#genes                                     71 + 3188 part                                                                 
#predicted genes (unique)                  16085                                                                          
#predicted genes (>= 0 bp)                 16134                                                                          
#predicted genes (>= 300 bp)               327                                                                            
#predicted genes (>= 1500 bp)              0                                                                              
#predicted genes (>= 3000 bp)              0                                                                              
#mismatches                                3520                                                                           
#indels                                    17841                                                                          
Indels length                              18239                                                                          
#mismatches per 100 kbp                    140.93                                                                         
#indels per 100 kbp                        714.30                                                                         
GC (%)                                     64.32                                                                          
Reference GC (%)                           65.61                                                                          
Average %IDY                               99.088                                                                         
Largest alignment                          853                                                                            
NA50                                       232                                                                            
NGA50                                      232                                                                            
NA75                                       221                                                                            
NGA75                                      221                                                                            
LA50                                       8059                                                                           
LGA50                                      8035                                                                           
LA75                                       12993                                                                          
LGA75                                      12954                                                                          
#Mis_misassemblies                         302                                                                            
#Mis_relocations                           301                                                                            
#Mis_translocations                        0                                                                              
#Mis_inversions                            1                                                                              
#Mis_misassembled contigs                  302                                                                            
Mis_Misassembled contigs length            83067                                                                          
#Mis_local misassemblies                   5                                                                              
#Mis_short indels (<= 5 bp)                17838                                                                          
#Mis_long indels (> 5 bp)                  3                                                                              
#Una_fully unaligned contigs               43                                                                             
Una_Fully unaligned length                 10708                                                                          
#Una_partially unaligned contigs           17                                                                             
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             1935                                                                           
GAGE_Contigs #                             17911                                                                          
GAGE_Min contig                            221                                                                            
GAGE_Max contig                            853                                                                            
GAGE_N50                                   234 COUNT: 7973                                                                
GAGE_Genome size                           4411532                                                                        
GAGE_Assembly size                         4422949                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               1864366(42.26%)                                                                
GAGE_Missing assembly bases                1654(0.04%)                                                                    
GAGE_Missing assembly contigs              2(0.01%)                                                                       
GAGE_Duplicated reference bases            1317666                                                                        
GAGE_Compressed reference bases            53987                                                                          
GAGE_Bad trim                              924                                                                            
GAGE_Avg idy                               99.15                                                                          
GAGE_SNPs                                  3140                                                                           
GAGE_Indels < 5bp                          15114                                                                          
GAGE_Indels >= 5                           10                                                                             
GAGE_Inversions                            61                                                                             
GAGE_Relocation                            33                                                                             
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    12292                                                                          
GAGE_Corrected assembly size               3050631                                                                        
GAGE_Min correct contig                    200                                                                            
GAGE_Max correct contig                    857                                                                            
GAGE_Corrected N50                         221 COUNT: 8462                                                                
