All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_75.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_75.final.scaf
#Mis_misassemblies               6                                                                                6                                                                       
#Mis_relocations                 4                                                                                4                                                                       
#Mis_translocations              0                                                                                0                                                                       
#Mis_inversions                  2                                                                                2                                                                       
#Mis_misassembled contigs        6                                                                                6                                                                       
Mis_Misassembled contigs length  3002                                                                             3002                                                                    
#Mis_local misassemblies         3                                                                                25                                                                      
#mismatches                      20490                                                                            20481                                                                   
#indels                          192                                                                              262                                                                     
#Mis_short indels (<= 5 bp)      192                                                                              260                                                                     
#Mis_long indels (> 5 bp)        0                                                                                2                                                                       
Indels length                    196                                                                              311                                                                     
