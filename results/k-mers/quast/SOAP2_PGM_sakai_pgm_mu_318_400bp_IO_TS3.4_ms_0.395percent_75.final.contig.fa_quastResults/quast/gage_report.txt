All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_75.final.contig
GAGE_Contigs #                   11730                                                                    
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  1216                                                                     
GAGE_N50                         222 COUNT: 10098                                                         
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               3140063                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     2904379(51.92%)                                                          
GAGE_Missing assembly bases      62398(1.99%)                                                             
GAGE_Missing assembly contigs    148(1.26%)                                                               
GAGE_Duplicated reference bases  256953                                                                   
GAGE_Compressed reference bases  128236                                                                   
GAGE_Bad trim                    22348                                                                    
GAGE_Avg idy                     99.02                                                                    
GAGE_SNPs                        780                                                                      
GAGE_Indels < 5bp                20781                                                                    
GAGE_Indels >= 5                 12                                                                       
GAGE_Inversions                  52                                                                       
GAGE_Relocation                  25                                                                       
GAGE_Translocation               11                                                                       
GAGE_Corrected contig #          10200                                                                    
GAGE_Corrected assembly size     2744825                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          1216                                                                     
GAGE_Corrected N50               117 COUNT: 10527                                                         
