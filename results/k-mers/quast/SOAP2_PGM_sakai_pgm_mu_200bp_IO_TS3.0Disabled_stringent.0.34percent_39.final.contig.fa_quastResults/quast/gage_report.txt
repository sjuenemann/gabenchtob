All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_39.final.contig
GAGE_Contigs #                   5825                                                                               
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  956                                                                                
GAGE_N50                         0 COUNT: 0                                                                         
GAGE_Genome size                 5594470                                                                            
GAGE_Assembly size               1667593                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     3932233(70.29%)                                                                    
GAGE_Missing assembly bases      3935(0.24%)                                                                        
GAGE_Missing assembly contigs    16(0.27%)                                                                          
GAGE_Duplicated reference bases  0                                                                                  
GAGE_Compressed reference bases  2351                                                                               
GAGE_Bad trim                    396                                                                                
GAGE_Avg idy                     99.98                                                                              
GAGE_SNPs                        18                                                                                 
GAGE_Indels < 5bp                305                                                                                
GAGE_Indels >= 5                 0                                                                                  
GAGE_Inversions                  0                                                                                  
GAGE_Relocation                  0                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          5804                                                                               
GAGE_Corrected assembly size     1662972                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          956                                                                                
GAGE_Corrected N50               0 COUNT: 0                                                                         
