All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_15.final.contig
#Contigs (>= 0 bp)             495825                                                                             
#Contigs (>= 1000 bp)          0                                                                                  
Total length (>= 0 bp)         14333202                                                                           
Total length (>= 1000 bp)      0                                                                                  
#Contigs                       127                                                                                
Largest contig                 313                                                                                
Total length                   28834                                                                              
Reference length               5594470                                                                            
GC (%)                         47.59                                                                              
Reference GC (%)               50.48                                                                              
N50                            223                                                                                
NG50                           None                                                                               
N75                            210                                                                                
NG75                           None                                                                               
#misassemblies                 0                                                                                  
#local misassemblies           0                                                                                  
#unaligned contigs             3 + 4 part                                                                         
Unaligned contigs length       817                                                                                
Genome fraction (%)            0.501                                                                              
Duplication ratio              1.000                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        28.56                                                                              
#indels per 100 kbp            160.65                                                                             
#genes                         1 + 109 part                                                                       
#predicted genes (unique)      121                                                                                
#predicted genes (>= 0 bp)     121                                                                                
#predicted genes (>= 300 bp)   0                                                                                  
#predicted genes (>= 1500 bp)  0                                                                                  
#predicted genes (>= 3000 bp)  0                                                                                  
Largest alignment              313                                                                                
NA50                           221                                                                                
NGA50                          None                                                                               
NA75                           210                                                                                
NGA75                          None                                                                               
