All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_69.final.contig
#Contigs (>= 0 bp)             34386                                                                                    
#Contigs (>= 1000 bp)          301                                                                                      
Total length (>= 0 bp)         6649259                                                                                  
Total length (>= 1000 bp)      384068                                                                                   
#Contigs                       11886                                                                                    
Largest contig                 2621                                                                                     
Total length                   4002620                                                                                  
Reference length               2813862                                                                                  
GC (%)                         32.33                                                                                    
Reference GC (%)               32.81                                                                                    
N50                            328                                                                                      
NG50                           491                                                                                      
N75                            227                                                                                      
NG75                           301                                                                                      
#misassemblies                 93                                                                                       
#local misassemblies           2                                                                                        
#unaligned contigs             2812 + 1025 part                                                                         
Unaligned contigs length       730186                                                                                   
Genome fraction (%)            90.684                                                                                   
Duplication ratio              1.280                                                                                    
#N's per 100 kbp               0.00                                                                                     
#mismatches per 100 kbp        45.93                                                                                    
#indels per 100 kbp            326.41                                                                                   
#genes                         425 + 2189 part                                                                          
#predicted genes (unique)      9752                                                                                     
#predicted genes (>= 0 bp)     9776                                                                                     
#predicted genes (>= 300 bp)   2910                                                                                     
#predicted genes (>= 1500 bp)  6                                                                                        
#predicted genes (>= 3000 bp)  0                                                                                        
Largest alignment              2621                                                                                     
NA50                           327                                                                                      
NGA50                          490                                                                                      
NA75                           210                                                                                      
NGA75                          300                                                                                      
