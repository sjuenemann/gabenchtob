All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_143.final.contigs
#Contigs                                   7739                                                                        
#Contigs (>= 0 bp)                         7739                                                                        
#Contigs (>= 1000 bp)                      470                                                                         
Largest contig                             3678                                                                        
Total length                               3965812                                                                     
Total length (>= 0 bp)                     3965812                                                                     
Total length (>= 1000 bp)                  610286                                                                      
Reference length                           5594470                                                                     
N50                                        542                                                                         
NG50                                       397                                                                         
N75                                        374                                                                         
NG75                                       None                                                                        
L50                                        2408                                                                        
LG50                                       4177                                                                        
L75                                        4638                                                                        
LG75                                       None                                                                        
#local misassemblies                       1                                                                           
#misassemblies                             148                                                                         
#misassembled contigs                      147                                                                         
Misassembled contigs length                82460                                                                       
Misassemblies inter-contig overlap         394                                                                         
#unaligned contigs                         0 + 1 part                                                                  
Unaligned contigs length                   25                                                                          
#ambiguously mapped contigs                130                                                                         
Extra bases in ambiguously mapped contigs  -43636                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        64.747                                                                      
Duplication ratio                          1.083                                                                       
#genes                                     667 + 3788 part                                                             
#predicted genes (unique)                  9660                                                                        
#predicted genes (>= 0 bp)                 9715                                                                        
#predicted genes (>= 300 bp)               4958                                                                        
#predicted genes (>= 1500 bp)              8                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                72                                                                          
#indels                                    1357                                                                        
Indels length                              1374                                                                        
#mismatches per 100 kbp                    1.99                                                                        
#indels per 100 kbp                        37.46                                                                       
GC (%)                                     49.94                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.692                                                                      
Largest alignment                          3536                                                                        
NA50                                       536                                                                         
NGA50                                      391                                                                         
NA75                                       368                                                                         
NGA75                                      None                                                                        
LA50                                       2420                                                                        
LGA50                                      4209                                                                        
LA75                                       4676                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         148                                                                         
#Mis_relocations                           139                                                                         
#Mis_translocations                        9                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  147                                                                         
Mis_Misassembled contigs length            82460                                                                       
#Mis_local misassemblies                   1                                                                           
#Mis_short indels (<= 5 bp)                1357                                                                        
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           1                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             25                                                                          
GAGE_Contigs #                             7739                                                                        
GAGE_Min contig                            285                                                                         
GAGE_Max contig                            3678                                                                        
GAGE_N50                                   397 COUNT: 4177                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         3965812                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               1792279(32.04%)                                                             
GAGE_Missing assembly bases                42(0.00%)                                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            152782                                                                      
GAGE_Compressed reference bases            197736                                                                      
GAGE_Bad trim                              41                                                                          
GAGE_Avg idy                               99.96                                                                       
GAGE_SNPs                                  63                                                                          
GAGE_Indels < 5bp                          1114                                                                        
GAGE_Indels >= 5                           1                                                                           
GAGE_Inversions                            30                                                                          
GAGE_Relocation                            12                                                                          
GAGE_Translocation                         7                                                                           
GAGE_Corrected contig #                    7293                                                                        
GAGE_Corrected assembly size               3797813                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    3535                                                                        
GAGE_Corrected N50                         389 COUNT: 4210                                                             
