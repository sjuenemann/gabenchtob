All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_143.final.contigs
GAGE_Contigs #                   7739                                                                        
GAGE_Min contig                  285                                                                         
GAGE_Max contig                  3678                                                                        
GAGE_N50                         397 COUNT: 4177                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               3965812                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1792279(32.04%)                                                             
GAGE_Missing assembly bases      42(0.00%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  152782                                                                      
GAGE_Compressed reference bases  197736                                                                      
GAGE_Bad trim                    41                                                                          
GAGE_Avg idy                     99.96                                                                       
GAGE_SNPs                        63                                                                          
GAGE_Indels < 5bp                1114                                                                        
GAGE_Indels >= 5                 1                                                                           
GAGE_Inversions                  30                                                                          
GAGE_Relocation                  12                                                                          
GAGE_Translocation               7                                                                           
GAGE_Corrected contig #          7293                                                                        
GAGE_Corrected assembly size     3797813                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          3535                                                                        
GAGE_Corrected N50               389 COUNT: 4210                                                             
