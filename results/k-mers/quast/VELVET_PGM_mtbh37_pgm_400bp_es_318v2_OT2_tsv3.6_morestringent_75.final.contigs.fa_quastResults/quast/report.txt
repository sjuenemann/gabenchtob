All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_75.final.contigs
#Contigs (>= 0 bp)             10902                                                                         
#Contigs (>= 1000 bp)          123                                                                           
Total length (>= 0 bp)         3297575                                                                       
Total length (>= 1000 bp)      151963                                                                        
#Contigs                       7033                                                                          
Largest contig                 2252                                                                          
Total length                   2631763                                                                       
Reference length               4411532                                                                       
GC (%)                         66.61                                                                         
Reference GC (%)               65.61                                                                         
N50                            395                                                                           
NG50                           244                                                                           
N75                            275                                                                           
NG75                           None                                                                          
#misassemblies                 31                                                                            
#local misassemblies           7                                                                             
#unaligned contigs             4 + 6 part                                                                    
Unaligned contigs length       1941                                                                          
Genome fraction (%)            58.666                                                                        
Duplication ratio              1.014                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        4.83                                                                          
#indels per 100 kbp            69.40                                                                         
#genes                         173 + 3330 part                                                               
#predicted genes (unique)      7537                                                                          
#predicted genes (>= 0 bp)     7539                                                                          
#predicted genes (>= 300 bp)   2721                                                                          
#predicted genes (>= 1500 bp)  2                                                                             
#predicted genes (>= 3000 bp)  0                                                                             
Largest alignment              2252                                                                          
NA50                           394                                                                           
NGA50                          243                                                                           
NA75                           275                                                                           
NGA75                          None                                                                          
