All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_107.final.contig
#Mis_misassemblies               159                                                                       
#Mis_relocations                 149                                                                       
#Mis_translocations              10                                                                        
#Mis_inversions                  0                                                                         
#Mis_misassembled contigs        159                                                                       
Mis_Misassembled contigs length  50896                                                                     
#Mis_local misassemblies         2                                                                         
#mismatches                      451                                                                       
#indels                          16334                                                                     
#Mis_short indels (<= 5 bp)      16333                                                                     
#Mis_long indels (> 5 bp)        1                                                                         
Indels length                    16607                                                                     
