All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_57.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_57.final.scaf
#Mis_misassemblies               1                                                                             4                                                                    
#Mis_relocations                 1                                                                             4                                                                    
#Mis_translocations              0                                                                             0                                                                    
#Mis_inversions                  0                                                                             0                                                                    
#Mis_misassembled contigs        1                                                                             3                                                                    
Mis_Misassembled contigs length  14449                                                                         395975                                                               
#Mis_local misassemblies         1                                                                             51                                                                   
#mismatches                      35                                                                            46                                                                   
#indels                          102                                                                           1293                                                                 
#Mis_short indels (<= 5 bp)      90                                                                            1140                                                                 
#Mis_long indels (> 5 bp)        12                                                                            153                                                                  
Indels length                    510                                                                           5417                                                                 
