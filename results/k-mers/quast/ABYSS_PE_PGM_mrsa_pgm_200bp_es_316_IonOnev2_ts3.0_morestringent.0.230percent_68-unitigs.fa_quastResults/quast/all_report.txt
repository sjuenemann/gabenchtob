All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_68-unitigs
#Contigs                                   495                                                                                    
#Contigs (>= 0 bp)                         2744                                                                                   
#Contigs (>= 1000 bp)                      349                                                                                    
Largest contig                             48952                                                                                  
Total length                               2755466                                                                                
Total length (>= 0 bp)                     2980345                                                                                
Total length (>= 1000 bp)                  2687785                                                                                
Reference length                           2813862                                                                                
N50                                        11723                                                                                  
NG50                                       11406                                                                                  
N75                                        6546                                                                                   
NG75                                       6170                                                                                   
L50                                        69                                                                                     
LG50                                       72                                                                                     
L75                                        146                                                                                    
LG75                                       153                                                                                    
#local misassemblies                       4                                                                                      
#misassemblies                             2                                                                                      
#misassembled contigs                      2                                                                                      
Misassembled contigs length                20395                                                                                  
Misassemblies inter-contig overlap         903                                                                                    
#unaligned contigs                         0 + 0 part                                                                             
Unaligned contigs length                   0                                                                                      
#ambiguously mapped contigs                11                                                                                     
Extra bases in ambiguously mapped contigs  -4690                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        97.524                                                                                 
Duplication ratio                          1.003                                                                                  
#genes                                     2422 + 228 part                                                                        
#predicted genes (unique)                  2903                                                                                   
#predicted genes (>= 0 bp)                 2904                                                                                   
#predicted genes (>= 300 bp)               2373                                                                                   
#predicted genes (>= 1500 bp)              269                                                                                    
#predicted genes (>= 3000 bp)              23                                                                                     
#mismatches                                91                                                                                     
#indels                                    337                                                                                    
Indels length                              346                                                                                    
#mismatches per 100 kbp                    3.32                                                                                   
#indels per 100 kbp                        12.28                                                                                  
GC (%)                                     32.59                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.489                                                                                 
Largest alignment                          48952                                                                                  
NA50                                       11617                                                                                  
NGA50                                      11389                                                                                  
NA75                                       6543                                                                                   
NGA75                                      6071                                                                                   
LA50                                       70                                                                                     
LGA50                                      73                                                                                     
LA75                                       147                                                                                    
LGA75                                      154                                                                                    
#Mis_misassemblies                         2                                                                                      
#Mis_relocations                           1                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            1                                                                                      
#Mis_misassembled contigs                  2                                                                                      
Mis_Misassembled contigs length            20395                                                                                  
#Mis_local misassemblies                   4                                                                                      
#Mis_short indels (<= 5 bp)                337                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             495                                                                                    
GAGE_Min contig                            201                                                                                    
GAGE_Max contig                            48952                                                                                  
GAGE_N50                                   11406 COUNT: 72                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2755466                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               43674(1.55%)                                                                           
GAGE_Missing assembly bases                13(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            1206                                                                                   
GAGE_Compressed reference bases            27736                                                                                  
GAGE_Bad trim                              13                                                                                     
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  69                                                                                     
GAGE_Indels < 5bp                          341                                                                                    
GAGE_Indels >= 5                           3                                                                                      
GAGE_Inversions                            1                                                                                      
GAGE_Relocation                            2                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    495                                                                                    
GAGE_Corrected assembly size               2755405                                                                                
GAGE_Min correct contig                    201                                                                                    
GAGE_Max correct contig                    48960                                                                                  
GAGE_Corrected N50                         11340 COUNT: 73                                                                        
