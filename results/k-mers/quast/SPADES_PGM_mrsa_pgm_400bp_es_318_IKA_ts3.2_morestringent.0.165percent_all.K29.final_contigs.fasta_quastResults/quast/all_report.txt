All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K29.final_contigs
#Contigs                                   312                                                                                        
#Contigs (>= 0 bp)                         1431                                                                                       
#Contigs (>= 1000 bp)                      152                                                                                        
Largest contig                             99524                                                                                      
Total length                               2744578                                                                                    
Total length (>= 0 bp)                     2805183                                                                                    
Total length (>= 1000 bp)                  2686324                                                                                    
Reference length                           2813862                                                                                    
N50                                        31249                                                                                      
NG50                                       30753                                                                                      
N75                                        17613                                                                                      
NG75                                       15860                                                                                      
L50                                        28                                                                                         
LG50                                       29                                                                                         
L75                                        56                                                                                         
LG75                                       59                                                                                         
#local misassemblies                       10                                                                                         
#misassemblies                             1                                                                                          
#misassembled contigs                      1                                                                                          
Misassembled contigs length                30219                                                                                      
Misassemblies inter-contig overlap         1429                                                                                       
#unaligned contigs                         43 + 1 part                                                                                
Unaligned contigs length                   12506                                                                                      
#ambiguously mapped contigs                14                                                                                         
Extra bases in ambiguously mapped contigs  -9835                                                                                      
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        96.713                                                                                     
Duplication ratio                          1.001                                                                                      
#genes                                     2542 + 81 part                                                                             
#predicted genes (unique)                  2748                                                                                       
#predicted genes (>= 0 bp)                 2750                                                                                       
#predicted genes (>= 300 bp)               2291                                                                                       
#predicted genes (>= 1500 bp)              284                                                                                        
#predicted genes (>= 3000 bp)              28                                                                                         
#mismatches                                183                                                                                        
#indels                                    168                                                                                        
Indels length                              171                                                                                        
#mismatches per 100 kbp                    6.72                                                                                       
#indels per 100 kbp                        6.17                                                                                       
GC (%)                                     32.62                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.796                                                                                     
Largest alignment                          99524                                                                                      
NA50                                       31249                                                                                      
NGA50                                      30753                                                                                      
NA75                                       17613                                                                                      
NGA75                                      15860                                                                                      
LA50                                       28                                                                                         
LGA50                                      29                                                                                         
LA75                                       56                                                                                         
LGA75                                      59                                                                                         
#Mis_misassemblies                         1                                                                                          
#Mis_relocations                           1                                                                                          
#Mis_translocations                        0                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  1                                                                                          
Mis_Misassembled contigs length            30219                                                                                      
#Mis_local misassemblies                   10                                                                                         
#Mis_short indels (<= 5 bp)                168                                                                                        
#Mis_long indels (> 5 bp)                  0                                                                                          
#Una_fully unaligned contigs               43                                                                                         
Una_Fully unaligned length                 12472                                                                                      
#Una_partially unaligned contigs           1                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             34                                                                                         
GAGE_Contigs #                             312                                                                                        
GAGE_Min contig                            200                                                                                        
GAGE_Max contig                            99524                                                                                      
GAGE_N50                                   30753 COUNT: 29                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         2744578                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               48040(1.71%)                                                                               
GAGE_Missing assembly bases                12610(0.46%)                                                                               
GAGE_Missing assembly contigs              43(13.78%)                                                                                 
GAGE_Duplicated reference bases            0                                                                                          
GAGE_Compressed reference bases            35434                                                                                      
GAGE_Bad trim                              138                                                                                        
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  109                                                                                        
GAGE_Indels < 5bp                          170                                                                                        
GAGE_Indels >= 5                           7                                                                                          
GAGE_Inversions                            0                                                                                          
GAGE_Relocation                            3                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    280                                                                                        
GAGE_Corrected assembly size               2733859                                                                                    
GAGE_Min correct contig                    200                                                                                        
GAGE_Max correct contig                    99536                                                                                      
GAGE_Corrected N50                         30698 COUNT: 31                                                                            
