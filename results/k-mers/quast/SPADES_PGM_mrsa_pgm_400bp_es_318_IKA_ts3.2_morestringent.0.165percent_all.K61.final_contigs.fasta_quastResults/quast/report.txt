All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K61.final_contigs
#Contigs (>= 0 bp)             1395                                                                                       
#Contigs (>= 1000 bp)          92                                                                                         
Total length (>= 0 bp)         2994004                                                                                    
Total length (>= 1000 bp)      2734785                                                                                    
#Contigs                       831                                                                                        
Largest contig                 148087                                                                                     
Total length                   2943684                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.52                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            48326                                                                                      
NG50                           51323                                                                                      
N75                            26869                                                                                      
NG75                           30859                                                                                      
#misassemblies                 2                                                                                          
#local misassemblies           12                                                                                         
#unaligned contigs             659 + 0 part                                                                               
Unaligned contigs length       177766                                                                                     
Genome fraction (%)            97.863                                                                                     
Duplication ratio              1.001                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        6.03                                                                                       
#indels per 100 kbp            7.23                                                                                       
#genes                         2590 + 71 part                                                                             
#predicted genes (unique)      3082                                                                                       
#predicted genes (>= 0 bp)     3084                                                                                       
#predicted genes (>= 300 bp)   2305                                                                                       
#predicted genes (>= 1500 bp)  287                                                                                        
#predicted genes (>= 3000 bp)  25                                                                                         
Largest alignment              148087                                                                                     
NA50                           48326                                                                                      
NGA50                          51323                                                                                      
NA75                           25867                                                                                      
NGA75                          30859                                                                                      
