All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_53.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_53.final.scaf
GAGE_Contigs #                   62518                                                                            62521                                                                   
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  3208                                                                             3208                                                                    
GAGE_N50                         326 COUNT: 5236                                                                  327 COUNT: 5236                                                         
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               16282059                                                                         16283320                                                                
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     1137530(20.33%)                                                                  1137343(20.33%)                                                         
GAGE_Missing assembly bases      11037846(67.79%)                                                                 11038199(67.79%)                                                        
GAGE_Missing assembly contigs    46710(74.71%)                                                                    46710(74.71%)                                                           
GAGE_Duplicated reference bases  523015                                                                           523639                                                                  
GAGE_Compressed reference bases  162189                                                                           162189                                                                  
GAGE_Bad trim                    63970                                                                            64149                                                                   
GAGE_Avg idy                     99.47                                                                            99.47                                                                   
GAGE_SNPs                        16096                                                                            16081                                                                   
GAGE_Indels < 5bp                100                                                                              100                                                                     
GAGE_Indels >= 5                 0                                                                                3                                                                       
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  1                                                                                1                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          12928                                                                            12926                                                                   
GAGE_Corrected assembly size     4651453                                                                          4651006                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          3208                                                                             3208                                                                    
GAGE_Corrected N50               322 COUNT: 5258                                                                  322 COUNT: 5258                                                         
