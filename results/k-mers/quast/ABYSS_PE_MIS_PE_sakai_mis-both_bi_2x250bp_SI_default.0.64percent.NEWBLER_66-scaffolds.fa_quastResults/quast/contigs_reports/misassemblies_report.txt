All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_66-scaffolds_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_66-scaffolds
#Mis_misassemblies               4                                                                                             14                                                                                   
#Mis_relocations                 4                                                                                             14                                                                                   
#Mis_translocations              0                                                                                             0                                                                                    
#Mis_inversions                  0                                                                                             0                                                                                    
#Mis_misassembled contigs        4                                                                                             10                                                                                   
Mis_Misassembled contigs length  232088                                                                                        514353                                                                               
#Mis_local misassemblies         4                                                                                             20                                                                                   
#mismatches                      945                                                                                           1008                                                                                 
#indels                          51                                                                                            86                                                                                   
#Mis_short indels (<= 5 bp)      45                                                                                            70                                                                                   
#Mis_long indels (> 5 bp)        6                                                                                             16                                                                                   
Indels length                    192                                                                                           489                                                                                  
