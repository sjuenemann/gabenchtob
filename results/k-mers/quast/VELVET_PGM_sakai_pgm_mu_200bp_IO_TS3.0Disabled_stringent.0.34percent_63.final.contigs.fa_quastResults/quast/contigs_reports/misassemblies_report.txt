All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_63.final.contigs
#Mis_misassemblies               6                                                                                    
#Mis_relocations                 6                                                                                    
#Mis_translocations              0                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        6                                                                                    
Mis_Misassembled contigs length  39187                                                                                
#Mis_local misassemblies         22                                                                                   
#mismatches                      203                                                                                  
#indels                          2917                                                                                 
#Mis_short indels (<= 5 bp)      2911                                                                                 
#Mis_long indels (> 5 bp)        6                                                                                    
Indels length                    3392                                                                                 
