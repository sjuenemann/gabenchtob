All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_47.final.contig
#Contigs                                   7713                                                                               
#Contigs (>= 0 bp)                         159749                                                                             
#Contigs (>= 1000 bp)                      1                                                                                  
Largest contig                             1039                                                                               
Total length                               2348699                                                                            
Total length (>= 0 bp)                     15642572                                                                           
Total length (>= 1000 bp)                  1039                                                                               
Reference length                           5594470                                                                            
N50                                        304                                                                                
NG50                                       None                                                                               
N75                                        243                                                                                
NG75                                       None                                                                               
L50                                        2868                                                                               
LG50                                       None                                                                               
L75                                        5037                                                                               
LG75                                       None                                                                               
#local misassemblies                       0                                                                                  
#misassemblies                             1                                                                                  
#misassembled contigs                      1                                                                                  
Misassembled contigs length                406                                                                                
Misassemblies inter-contig overlap         0                                                                                  
#unaligned contigs                         125 + 4 part                                                                       
Unaligned contigs length                   26634                                                                              
#ambiguously mapped contigs                9                                                                                  
Extra bases in ambiguously mapped contigs  -2624                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        41.292                                                                             
Duplication ratio                          1.004                                                                              
#genes                                     115 + 3801 part                                                                    
#predicted genes (unique)                  7895                                                                               
#predicted genes (>= 0 bp)                 7895                                                                               
#predicted genes (>= 300 bp)               2063                                                                               
#predicted genes (>= 1500 bp)              0                                                                                  
#predicted genes (>= 3000 bp)              0                                                                                  
#mismatches                                24                                                                                 
#indels                                    296                                                                                
Indels length                              307                                                                                
#mismatches per 100 kbp                    1.04                                                                               
#indels per 100 kbp                        12.81                                                                              
GC (%)                                     50.14                                                                              
Reference GC (%)                           50.48                                                                              
Average %IDY                               99.985                                                                             
Largest alignment                          1039                                                                               
NA50                                       304                                                                                
NGA50                                      None                                                                               
NA75                                       243                                                                                
NGA75                                      None                                                                               
LA50                                       2869                                                                               
LGA50                                      None                                                                               
LA75                                       5040                                                                               
LGA75                                      None                                                                               
#Mis_misassemblies                         1                                                                                  
#Mis_relocations                           1                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  1                                                                                  
Mis_Misassembled contigs length            406                                                                                
#Mis_local misassemblies                   0                                                                                  
#Mis_short indels (<= 5 bp)                296                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               125                                                                                
Una_Fully unaligned length                 26461                                                                              
#Una_partially unaligned contigs           4                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             173                                                                                
GAGE_Contigs #                             7713                                                                               
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            1039                                                                               
GAGE_N50                                   0 COUNT: 0                                                                         
GAGE_Genome size                           5594470                                                                            
GAGE_Assembly size                         2348699                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               3260566(58.28%)                                                                    
GAGE_Missing assembly bases                8823(0.38%)                                                                        
GAGE_Missing assembly contigs              35(0.45%)                                                                          
GAGE_Duplicated reference bases            2208                                                                               
GAGE_Compressed reference bases            9440                                                                               
GAGE_Bad trim                              1321                                                                               
GAGE_Avg idy                               99.96                                                                              
GAGE_SNPs                                  65                                                                                 
GAGE_Indels < 5bp                          715                                                                                
GAGE_Indels >= 5                           0                                                                                  
GAGE_Inversions                            1                                                                                  
GAGE_Relocation                            1                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    7643                                                                               
GAGE_Corrected assembly size               2333742                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    1039                                                                               
GAGE_Corrected N50                         0 COUNT: 0                                                                         
