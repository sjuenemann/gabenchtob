All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K97.final_contigs
#Contigs                                   7222                                                                                       
#Contigs (>= 0 bp)                         7561                                                                                       
#Contigs (>= 1000 bp)                      65                                                                                         
Largest contig                             236818                                                                                     
Total length                               4981254                                                                                    
Total length (>= 0 bp)                     5026227                                                                                    
Total length (>= 1000 bp)                  2758121                                                                                    
Reference length                           2813862                                                                                    
N50                                        24236                                                                                      
NG50                                       87685                                                                                      
N75                                        315                                                                                        
NG75                                       39831                                                                                      
L50                                        35                                                                                         
LG50                                       10                                                                                         
L75                                        2719                                                                                       
LG75                                       23                                                                                         
#local misassemblies                       8                                                                                          
#misassemblies                             24                                                                                         
#misassembled contigs                      24                                                                                         
Misassembled contigs length                84164                                                                                      
Misassemblies inter-contig overlap         1953                                                                                       
#unaligned contigs                         1722 + 194 part                                                                            
Unaligned contigs length                   540469                                                                                     
#ambiguously mapped contigs                39                                                                                         
Extra bases in ambiguously mapped contigs  -17103                                                                                     
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        98.589                                                                                     
Duplication ratio                          1.595                                                                                      
#genes                                     2657 + 52 part                                                                             
#predicted genes (unique)                  7645                                                                                       
#predicted genes (>= 0 bp)                 7651                                                                                       
#predicted genes (>= 300 bp)               2334                                                                                       
#predicted genes (>= 1500 bp)              290                                                                                        
#predicted genes (>= 3000 bp)              28                                                                                         
#mismatches                                173                                                                                        
#indels                                    531                                                                                        
Indels length                              562                                                                                        
#mismatches per 100 kbp                    6.24                                                                                       
#indels per 100 kbp                        19.14                                                                                      
GC (%)                                     32.10                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               97.398                                                                                     
Largest alignment                          236818                                                                                     
NA50                                       24236                                                                                      
NGA50                                      87685                                                                                      
NA75                                       292                                                                                        
NGA75                                      39831                                                                                      
LA50                                       35                                                                                         
LGA50                                      10                                                                                         
LA75                                       2890                                                                                       
LGA75                                      23                                                                                         
#Mis_misassemblies                         24                                                                                         
#Mis_relocations                           20                                                                                         
#Mis_translocations                        4                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  24                                                                                         
Mis_Misassembled contigs length            84164                                                                                      
#Mis_local misassemblies                   8                                                                                          
#Mis_short indels (<= 5 bp)                529                                                                                        
#Mis_long indels (> 5 bp)                  2                                                                                          
#Una_fully unaligned contigs               1722                                                                                       
Una_Fully unaligned length                 530192                                                                                     
#Una_partially unaligned contigs           194                                                                                        
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            1                                                                                          
Una_Partially unaligned length             10277                                                                                      
GAGE_Contigs #                             7222                                                                                       
GAGE_Min contig                            202                                                                                        
GAGE_Max contig                            236818                                                                                     
GAGE_N50                                   87685 COUNT: 10                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         4981254                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               1309(0.05%)                                                                                
GAGE_Missing assembly bases                247672(4.97%)                                                                              
GAGE_Missing assembly contigs              618(8.56%)                                                                                 
GAGE_Duplicated reference bases            1946766                                                                                    
GAGE_Compressed reference bases            37723                                                                                      
GAGE_Bad trim                              52604                                                                                      
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  50                                                                                         
GAGE_Indels < 5bp                          312                                                                                        
GAGE_Indels >= 5                           8                                                                                          
GAGE_Inversions                            1                                                                                          
GAGE_Relocation                            3                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    150                                                                                        
GAGE_Corrected assembly size               2788309                                                                                    
GAGE_Min correct contig                    202                                                                                        
GAGE_Max correct contig                    181880                                                                                     
GAGE_Corrected N50                         74285 COUNT: 12                                                                            
