All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_69.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_69.final.scaf
#Contigs (>= 0 bp)             2039                                                                           1466                                                                  
#Contigs (>= 1000 bp)          300                                                                            124                                                                   
Total length (>= 0 bp)         5481023                                                                        5512170                                                               
Total length (>= 1000 bp)      5155658                                                                        5278161                                                               
#Contigs                       684                                                                            343                                                                   
Largest contig                 145274                                                                         374695                                                                
Total length                   5298803                                                                        5362189                                                               
Reference length               5594470                                                                        5594470                                                               
GC (%)                         50.25                                                                          50.27                                                                 
Reference GC (%)               50.48                                                                          50.48                                                                 
N50                            37153                                                                          149289                                                                
NG50                           36119                                                                          148304                                                                
N75                            17711                                                                          77459                                                                 
NG75                           15132                                                                          74098                                                                 
#misassemblies                 3                                                                              45                                                                    
#local misassemblies           1                                                                              130                                                                   
#unaligned contigs             1 + 2 part                                                                     7 + 7 part                                                            
Unaligned contigs length       5602                                                                           18453                                                                 
Genome fraction (%)            93.200                                                                         93.402                                                                
Duplication ratio              1.000                                                                          1.011                                                                 
#N's per 100 kbp               0.87                                                                           581.74                                                                
#mismatches per 100 kbp        1.09                                                                           3.66                                                                  
#indels per 100 kbp            0.56                                                                           53.24                                                                 
#genes                         4701 + 325 part                                                                4866 + 190 part                                                       
#predicted genes (unique)      5472                                                                           5404                                                                  
#predicted genes (>= 0 bp)     5475                                                                           5407                                                                  
#predicted genes (>= 300 bp)   4418                                                                           4425                                                                  
#predicted genes (>= 1500 bp)  647                                                                            651                                                                   
#predicted genes (>= 3000 bp)  61                                                                             60                                                                    
Largest alignment              145274                                                                         361726                                                                
NA50                           37101                                                                          148249                                                                
NGA50                          34665                                                                          147879                                                                
NA75                           17187                                                                          77179                                                                 
NGA75                          15132                                                                          73993                                                                 
