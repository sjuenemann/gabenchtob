All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K97.final_contigs
#Contigs (>= 0 bp)             1778                                                                                      
#Contigs (>= 1000 bp)          177                                                                                       
Total length (>= 0 bp)         5700122                                                                                   
Total length (>= 1000 bp)      5270651                                                                                   
#Contigs                       1591                                                                                      
Largest contig                 374842                                                                                    
Total length                   5674411                                                                                   
Reference length               5594470                                                                                   
GC (%)                         50.25                                                                                     
Reference GC (%)               50.48                                                                                     
N50                            124308                                                                                    
NG50                           124308                                                                                    
N75                            41317                                                                                     
NG75                           43228                                                                                     
#misassemblies                 83                                                                                        
#local misassemblies           15                                                                                        
#unaligned contigs             83 + 223 part                                                                             
Unaligned contigs length       33264                                                                                     
Genome fraction (%)            94.277                                                                                    
Duplication ratio              1.052                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        4.36                                                                                      
#indels per 100 kbp            34.28                                                                                     
#genes                         4936 + 205 part                                                                           
#predicted genes (unique)      6971                                                                                      
#predicted genes (>= 0 bp)     6986                                                                                      
#predicted genes (>= 300 bp)   4853                                                                                      
#predicted genes (>= 1500 bp)  521                                                                                       
#predicted genes (>= 3000 bp)  39                                                                                        
Largest alignment              311192                                                                                    
NA50                           124308                                                                                    
NGA50                          124308                                                                                    
NA75                           41248                                                                                     
NGA75                          43228                                                                                     
