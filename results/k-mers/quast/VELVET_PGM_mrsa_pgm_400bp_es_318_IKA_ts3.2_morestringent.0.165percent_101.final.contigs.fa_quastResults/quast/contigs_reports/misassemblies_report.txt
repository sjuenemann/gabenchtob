All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_101.final.contigs
#Mis_misassemblies               527                                                                                    
#Mis_relocations                 489                                                                                    
#Mis_translocations              34                                                                                     
#Mis_inversions                  4                                                                                      
#Mis_misassembled contigs        527                                                                                    
Mis_Misassembled contigs length  135228                                                                                 
#Mis_local misassemblies         3                                                                                      
#mismatches                      1985                                                                                   
#indels                          27635                                                                                  
#Mis_short indels (<= 5 bp)      27634                                                                                  
#Mis_long indels (> 5 bp)        1                                                                                      
Indels length                    28394                                                                                  
