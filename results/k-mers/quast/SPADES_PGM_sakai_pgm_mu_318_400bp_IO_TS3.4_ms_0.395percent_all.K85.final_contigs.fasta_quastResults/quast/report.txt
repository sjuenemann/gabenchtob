All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K85.final_contigs
#Contigs (>= 0 bp)             1939                                                                            
#Contigs (>= 1000 bp)          181                                                                             
Total length (>= 0 bp)         5711980                                                                         
Total length (>= 1000 bp)      5239835                                                                         
#Contigs                       1623                                                                            
Largest contig                 307455                                                                          
Total length                   5672041                                                                         
Reference length               5594470                                                                         
GC (%)                         50.15                                                                           
Reference GC (%)               50.48                                                                           
N50                            107169                                                                          
NG50                           114030                                                                          
N75                            35784                                                                           
NG75                           39910                                                                           
#misassemblies                 10                                                                              
#local misassemblies           14                                                                              
#unaligned contigs             313 + 15 part                                                                   
Unaligned contigs length       85352                                                                           
Genome fraction (%)            94.136                                                                          
Duplication ratio              1.044                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        4.69                                                                            
#indels per 100 kbp            9.34                                                                            
#genes                         4901 + 218 part                                                                 
#predicted genes (unique)      6365                                                                            
#predicted genes (>= 0 bp)     6372                                                                            
#predicted genes (>= 300 bp)   4549                                                                            
#predicted genes (>= 1500 bp)  638                                                                             
#predicted genes (>= 3000 bp)  62                                                                              
Largest alignment              307455                                                                          
NA50                           107169                                                                          
NGA50                          107169                                                                          
NA75                           35639                                                                           
NGA75                          36894                                                                           
