All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K85.final_contigs
#Contigs                                   1623                                                                            
#Contigs (>= 0 bp)                         1939                                                                            
#Contigs (>= 1000 bp)                      181                                                                             
Largest contig                             307455                                                                          
Total length                               5672041                                                                         
Total length (>= 0 bp)                     5711980                                                                         
Total length (>= 1000 bp)                  5239835                                                                         
Reference length                           5594470                                                                         
N50                                        107169                                                                          
NG50                                       114030                                                                          
N75                                        35784                                                                           
NG75                                       39910                                                                           
L50                                        17                                                                              
LG50                                       16                                                                              
L75                                        38                                                                              
LG75                                       36                                                                              
#local misassemblies                       14                                                                              
#misassemblies                             10                                                                              
#misassembled contigs                      10                                                                              
Misassembled contigs length                236536                                                                          
Misassemblies inter-contig overlap         2062                                                                            
#unaligned contigs                         313 + 15 part                                                                   
Unaligned contigs length                   85352                                                                           
#ambiguously mapped contigs                150                                                                             
Extra bases in ambiguously mapped contigs  -88661                                                                          
#N's                                       0                                                                               
#N's per 100 kbp                           0.00                                                                            
Genome fraction (%)                        94.136                                                                          
Duplication ratio                          1.044                                                                           
#genes                                     4901 + 218 part                                                                 
#predicted genes (unique)                  6365                                                                            
#predicted genes (>= 0 bp)                 6372                                                                            
#predicted genes (>= 300 bp)               4549                                                                            
#predicted genes (>= 1500 bp)              638                                                                             
#predicted genes (>= 3000 bp)              62                                                                              
#mismatches                                247                                                                             
#indels                                    492                                                                             
Indels length                              623                                                                             
#mismatches per 100 kbp                    4.69                                                                            
#indels per 100 kbp                        9.34                                                                            
GC (%)                                     50.15                                                                           
Reference GC (%)                           50.48                                                                           
Average %IDY                               98.592                                                                          
Largest alignment                          307455                                                                          
NA50                                       107169                                                                          
NGA50                                      107169                                                                          
NA75                                       35639                                                                           
NGA75                                      36894                                                                           
LA50                                       17                                                                              
LGA50                                      17                                                                              
LA75                                       39                                                                              
LGA75                                      37                                                                              
#Mis_misassemblies                         10                                                                              
#Mis_relocations                           10                                                                              
#Mis_translocations                        0                                                                               
#Mis_inversions                            0                                                                               
#Mis_misassembled contigs                  10                                                                              
Mis_Misassembled contigs length            236536                                                                          
#Mis_local misassemblies                   14                                                                              
#Mis_short indels (<= 5 bp)                484                                                                             
#Mis_long indels (> 5 bp)                  8                                                                               
#Una_fully unaligned contigs               313                                                                             
Una_Fully unaligned length                 83938                                                                           
#Una_partially unaligned contigs           15                                                                              
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            0                                                                               
Una_Partially unaligned length             1414                                                                            
GAGE_Contigs #                             1623                                                                            
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            307455                                                                          
GAGE_N50                                   114030 COUNT: 16                                                                
GAGE_Genome size                           5594470                                                                         
GAGE_Assembly size                         5672041                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               14277(0.26%)                                                                    
GAGE_Missing assembly bases                28078(0.50%)                                                                    
GAGE_Missing assembly contigs              81(4.99%)                                                                       
GAGE_Duplicated reference bases            286929                                                                          
GAGE_Compressed reference bases            283103                                                                          
GAGE_Bad trim                              4962                                                                            
GAGE_Avg idy                               99.98                                                                           
GAGE_SNPs                                  232                                                                             
GAGE_Indels < 5bp                          425                                                                             
GAGE_Indels >= 5                           11                                                                              
GAGE_Inversions                            0                                                                               
GAGE_Relocation                            10                                                                              
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    471                                                                             
GAGE_Corrected assembly size               5359602                                                                         
GAGE_Min correct contig                    200                                                                             
GAGE_Max correct contig                    224251                                                                          
GAGE_Corrected N50                         99725 COUNT: 19                                                                 
