All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_19.final.contig
GAGE_Contigs #                   33                                                                                  
GAGE_Min contig                  200                                                                                 
GAGE_Max contig                  436                                                                                 
GAGE_N50                         0 COUNT: 0                                                                          
GAGE_Genome size                 2813862                                                                             
GAGE_Assembly size               8797                                                                                
GAGE_Chaff bases                 0                                                                                   
GAGE_Missing reference bases     2813632(99.99%)                                                                     
GAGE_Missing assembly bases      8567(97.39%)                                                                        
GAGE_Missing assembly contigs    32(96.97%)                                                                          
GAGE_Duplicated reference bases  0                                                                                   
GAGE_Compressed reference bases  0                                                                                   
GAGE_Bad trim                    0                                                                                   
GAGE_Avg idy                     100.00                                                                              
GAGE_SNPs                        0                                                                                   
GAGE_Indels < 5bp                0                                                                                   
GAGE_Indels >= 5                 0                                                                                   
GAGE_Inversions                  0                                                                                   
GAGE_Relocation                  0                                                                                   
GAGE_Translocation               0                                                                                   
GAGE_Corrected contig #          1                                                                                   
GAGE_Corrected assembly size     230                                                                                 
GAGE_Min correct contig          230                                                                                 
GAGE_Max correct contig          230                                                                                 
GAGE_Corrected N50               0 COUNT: 0                                                                          
