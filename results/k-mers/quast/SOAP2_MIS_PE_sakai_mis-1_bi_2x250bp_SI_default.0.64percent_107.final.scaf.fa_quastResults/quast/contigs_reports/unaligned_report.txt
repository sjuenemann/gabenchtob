All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_107.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_107.final.scaf
#Una_fully unaligned contigs      10                                                                                20                                                                       
Una_Fully unaligned length        8119                                                                              16811                                                                    
#Una_partially unaligned contigs  0                                                                                 8                                                                        
#Una_with misassembly             0                                                                                 0                                                                        
#Una_both parts are significant   0                                                                                 6                                                                        
Una_Partially unaligned length    0                                                                                 4152                                                                     
#N's                              303                                                                               43085                                                                    
