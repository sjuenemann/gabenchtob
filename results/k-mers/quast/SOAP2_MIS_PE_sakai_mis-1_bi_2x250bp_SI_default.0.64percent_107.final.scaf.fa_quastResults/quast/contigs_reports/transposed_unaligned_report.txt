All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                          #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's 
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_107.final.scaf_broken  10                            8119                        0                                 0                      0                                0                               303  
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_107.final.scaf         20                            16811                       8                                 0                      6                                4152                            43085
