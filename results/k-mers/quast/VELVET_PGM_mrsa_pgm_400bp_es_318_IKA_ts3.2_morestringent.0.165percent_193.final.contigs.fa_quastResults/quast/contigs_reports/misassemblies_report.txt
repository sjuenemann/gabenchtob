All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_193.final.contigs
#Mis_misassemblies               19                                                                                     
#Mis_relocations                 18                                                                                     
#Mis_translocations              1                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        19                                                                                     
Mis_Misassembled contigs length  64220                                                                                  
#Mis_local misassemblies         8                                                                                      
#mismatches                      139                                                                                    
#indels                          684                                                                                    
#Mis_short indels (<= 5 bp)      683                                                                                    
#Mis_long indels (> 5 bp)        1                                                                                      
Indels length                    722                                                                                    
