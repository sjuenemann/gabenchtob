All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_21.final.contig
#Contigs                                   54                                                                                  
#Contigs (>= 0 bp)                         576879                                                                              
#Contigs (>= 1000 bp)                      0                                                                                   
Largest contig                             436                                                                                 
Total length                               14118                                                                               
Total length (>= 0 bp)                     19589372                                                                            
Total length (>= 1000 bp)                  0                                                                                   
Reference length                           2813862                                                                             
N50                                        255                                                                                 
NG50                                       None                                                                                
N75                                        220                                                                                 
NG75                                       None                                                                                
L50                                        23                                                                                  
LG50                                       None                                                                                
L75                                        38                                                                                  
LG75                                       None                                                                                
#local misassemblies                       0                                                                                   
#misassemblies                             0                                                                                   
#misassembled contigs                      0                                                                                   
Misassembled contigs length                0                                                                                   
Misassemblies inter-contig overlap         0                                                                                   
#unaligned contigs                         50 + 0 part                                                                         
Unaligned contigs length                   13201                                                                               
#ambiguously mapped contigs                0                                                                                   
Extra bases in ambiguously mapped contigs  0                                                                                   
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        0.033                                                                               
Duplication ratio                          1.000                                                                               
#genes                                     0 + 4 part                                                                          
#predicted genes (unique)                  41                                                                                  
#predicted genes (>= 0 bp)                 41                                                                                  
#predicted genes (>= 300 bp)               2                                                                                   
#predicted genes (>= 1500 bp)              0                                                                                   
#predicted genes (>= 3000 bp)              0                                                                                   
#mismatches                                1                                                                                   
#indels                                    0                                                                                   
Indels length                              0                                                                                   
#mismatches per 100 kbp                    109.05                                                                              
#indels per 100 kbp                        0.00                                                                                
GC (%)                                     34.25                                                                               
Reference GC (%)                           32.81                                                                               
Average %IDY                               99.903                                                                              
Largest alignment                          255                                                                                 
NA50                                       None                                                                                
NGA50                                      None                                                                                
NA75                                       None                                                                                
NGA75                                      None                                                                                
LA50                                       None                                                                                
LGA50                                      None                                                                                
LA75                                       None                                                                                
LGA75                                      None                                                                                
#Mis_misassemblies                         0                                                                                   
#Mis_relocations                           0                                                                                   
#Mis_translocations                        0                                                                                   
#Mis_inversions                            0                                                                                   
#Mis_misassembled contigs                  0                                                                                   
Mis_Misassembled contigs length            0                                                                                   
#Mis_local misassemblies                   0                                                                                   
#Mis_short indels (<= 5 bp)                0                                                                                   
#Mis_long indels (> 5 bp)                  0                                                                                   
#Una_fully unaligned contigs               50                                                                                  
Una_Fully unaligned length                 13201                                                                               
#Una_partially unaligned contigs           0                                                                                   
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             0                                                                                   
GAGE_Contigs #                             54                                                                                  
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            436                                                                                 
GAGE_N50                                   0 COUNT: 0                                                                          
GAGE_Genome size                           2813862                                                                             
GAGE_Assembly size                         14118                                                                               
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               2812945(99.97%)                                                                     
GAGE_Missing assembly bases                13201(93.50%)                                                                       
GAGE_Missing assembly contigs              50(92.59%)                                                                          
GAGE_Duplicated reference bases            0                                                                                   
GAGE_Compressed reference bases            0                                                                                   
GAGE_Bad trim                              0                                                                                   
GAGE_Avg idy                               99.89                                                                               
GAGE_SNPs                                  1                                                                                   
GAGE_Indels < 5bp                          0                                                                                   
GAGE_Indels >= 5                           0                                                                                   
GAGE_Inversions                            0                                                                                   
GAGE_Relocation                            0                                                                                   
GAGE_Translocation                         0                                                                                   
GAGE_Corrected contig #                    4                                                                                   
GAGE_Corrected assembly size               917                                                                                 
GAGE_Min correct contig                    204                                                                                 
GAGE_Max correct contig                    255                                                                                 
GAGE_Corrected N50                         0 COUNT: 0                                                                          
