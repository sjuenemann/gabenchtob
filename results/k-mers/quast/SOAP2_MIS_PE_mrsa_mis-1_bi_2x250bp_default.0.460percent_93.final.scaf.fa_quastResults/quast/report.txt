All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_93.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_93.final.scaf
#Contigs (>= 0 bp)             1896                                                                          1847                                                                 
#Contigs (>= 1000 bp)          182                                                                           158                                                                  
Total length (>= 0 bp)         3047982                                                                       3049345                                                              
Total length (>= 1000 bp)      2746980                                                                       2756618                                                              
#Contigs                       273                                                                           233                                                                  
Largest contig                 71692                                                                         78142                                                                
Total length                   2782410                                                                       2785106                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         32.69                                                                         32.69                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            24621                                                                         28268                                                                
NG50                           24435                                                                         28268                                                                
N75                            13337                                                                         16666                                                                
NG75                           12753                                                                         16507                                                                
#misassemblies                 1                                                                             4                                                                    
#local misassemblies           15                                                                            41                                                                   
#unaligned contigs             8 + 2 part                                                                    9 + 1 part                                                           
Unaligned contigs length       8596                                                                          9842                                                                 
Genome fraction (%)            98.222                                                                        98.256                                                               
Duplication ratio              1.002                                                                         1.002                                                                
#N's per 100 kbp               3.31                                                                          52.24                                                                
#mismatches per 100 kbp        1.85                                                                          2.21                                                                 
#indels per 100 kbp            0.98                                                                          5.39                                                                 
#genes                         2530 + 167 part                                                               2541 + 152 part                                                      
#predicted genes (unique)      2788                                                                          2770                                                                 
#predicted genes (>= 0 bp)     2790                                                                          2772                                                                 
#predicted genes (>= 300 bp)   2333                                                                          2329                                                                 
#predicted genes (>= 1500 bp)  285                                                                           286                                                                  
#predicted genes (>= 3000 bp)  28                                                                            28                                                                   
Largest alignment              71692                                                                         78128                                                                
NA50                           24621                                                                         28268                                                                
NGA50                          24435                                                                         28268                                                                
NA75                           13337                                                                         16457                                                                
NGA75                          12753                                                                         15777                                                                
