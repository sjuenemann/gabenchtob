All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_71.final.contig
GAGE_Contigs #                   11229                                                                    
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  1216                                                                     
GAGE_N50                         207 COUNT: 10727                                                         
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               2899059                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     3107110(55.54%)                                                          
GAGE_Missing assembly bases      72741(2.51%)                                                             
GAGE_Missing assembly contigs    189(1.68%)                                                               
GAGE_Duplicated reference bases  204886                                                                   
GAGE_Compressed reference bases  108332                                                                   
GAGE_Bad trim                    24258                                                                    
GAGE_Avg idy                     98.91                                                                    
GAGE_SNPs                        776                                                                      
GAGE_Indels < 5bp                21480                                                                    
GAGE_Indels >= 5                 13                                                                       
GAGE_Inversions                  55                                                                       
GAGE_Relocation                  28                                                                       
GAGE_Translocation               10                                                                       
GAGE_Corrected contig #          9795                                                                     
GAGE_Corrected assembly size     2540894                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          1216                                                                     
GAGE_Corrected N50               0 COUNT: 0                                                               
