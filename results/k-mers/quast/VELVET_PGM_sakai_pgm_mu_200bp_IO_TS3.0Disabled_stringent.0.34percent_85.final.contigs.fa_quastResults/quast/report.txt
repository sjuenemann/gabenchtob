All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_85.final.contigs
#Contigs (>= 0 bp)             1987                                                                                 
#Contigs (>= 1000 bp)          1128                                                                                 
Total length (>= 0 bp)         5327217                                                                              
Total length (>= 1000 bp)      4934344                                                                              
#Contigs                       1908                                                                                 
Largest contig                 34192                                                                                
Total length                   5312732                                                                              
Reference length               5594470                                                                              
GC (%)                         50.41                                                                                
Reference GC (%)               50.48                                                                                
N50                            5821                                                                                 
NG50                           5562                                                                                 
N75                            2871                                                                                 
NG75                           2464                                                                                 
#misassemblies                 52                                                                                   
#local misassemblies           27                                                                                   
#unaligned contigs             0 + 3 part                                                                           
Unaligned contigs length       380                                                                                  
Genome fraction (%)            92.855                                                                               
Duplication ratio              1.007                                                                                
#N's per 100 kbp               15.81                                                                                
#mismatches per 100 kbp        3.39                                                                                 
#indels per 100 kbp            36.11                                                                                
#genes                         4019 + 1073 part                                                                     
#predicted genes (unique)      6872                                                                                 
#predicted genes (>= 0 bp)     6879                                                                                 
#predicted genes (>= 300 bp)   5026                                                                                 
#predicted genes (>= 1500 bp)  434                                                                                  
#predicted genes (>= 3000 bp)  22                                                                                   
Largest alignment              34192                                                                                
NA50                           5706                                                                                 
NGA50                          5406                                                                                 
NA75                           2817                                                                                 
NGA75                          2421                                                                                 
