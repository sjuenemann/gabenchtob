All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_101.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_101.final.scaf
#Contigs (>= 0 bp)             6473                                                                              5291                                                                     
#Contigs (>= 1000 bp)          1086                                                                              426                                                                      
Total length (>= 0 bp)         6104112                                                                           6159389                                                                  
Total length (>= 1000 bp)      4927439                                                                           5206164                                                                  
#Contigs                       4857                                                                              3766                                                                     
Largest contig                 28367                                                                             154856                                                                   
Total length                   5876314                                                                           5945141                                                                  
Reference length               5594470                                                                           5594470                                                                  
GC (%)                         50.47                                                                             50.47                                                                    
Reference GC (%)               50.48                                                                             50.48                                                                    
N50                            5092                                                                              20602                                                                    
NG50                           5506                                                                              21286                                                                    
N75                            2194                                                                              7417                                                                     
NG75                           2634                                                                              9371                                                                     
#misassemblies                 7                                                                                 24                                                                       
#local misassemblies           66                                                                                1012                                                                     
#unaligned contigs             10 + 4 part                                                                       22 + 11 part                                                             
Unaligned contigs length       8130                                                                              23547                                                                    
Genome fraction (%)            94.415                                                                            94.398                                                                   
Duplication ratio              1.081                                                                             1.095                                                                    
#N's per 100 kbp               6.47                                                                              936.18                                                                   
#mismatches per 100 kbp        16.05                                                                             17.16                                                                    
#indels per 100 kbp            1.04                                                                              17.17                                                                    
#genes                         3841 + 1325 part                                                                  3889 + 1267 part                                                         
#predicted genes (unique)      9186                                                                              8873                                                                     
#predicted genes (>= 0 bp)     9257                                                                              8943                                                                     
#predicted genes (>= 300 bp)   4836                                                                              4791                                                                     
#predicted genes (>= 1500 bp)  485                                                                               506                                                                      
#predicted genes (>= 3000 bp)  31                                                                                34                                                                       
Largest alignment              28367                                                                             153361                                                                   
NA50                           5089                                                                              20432                                                                    
NGA50                          5465                                                                              20913                                                                    
NA75                           2169                                                                              6760                                                                     
NGA75                          2616                                                                              8977                                                                     
