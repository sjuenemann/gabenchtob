All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_53.final.contigs
GAGE_Contigs #                   4751                                                                                       
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  3373                                                                                       
GAGE_N50                         504 COUNT: 1712                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2393059                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     439771(15.63%)                                                                             
GAGE_Missing assembly bases      392(0.02%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                   
GAGE_Duplicated reference bases  287                                                                                        
GAGE_Compressed reference bases  6399                                                                                       
GAGE_Bad trim                    392                                                                                        
GAGE_Avg idy                     99.96                                                                                      
GAGE_SNPs                        90                                                                                         
GAGE_Indels < 5bp                659                                                                                        
GAGE_Indels >= 5                 2                                                                                          
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  1                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          4747                                                                                       
GAGE_Corrected assembly size     2392262                                                                                    
GAGE_Min correct contig          200                                                                                        
GAGE_Max correct contig          3373                                                                                       
GAGE_Corrected N50               504 COUNT: 1712                                                                            
