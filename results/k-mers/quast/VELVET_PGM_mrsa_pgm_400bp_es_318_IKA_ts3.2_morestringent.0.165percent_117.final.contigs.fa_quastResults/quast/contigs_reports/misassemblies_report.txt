All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_117.final.contigs
#Mis_misassemblies               424                                                                                    
#Mis_relocations                 395                                                                                    
#Mis_translocations              25                                                                                     
#Mis_inversions                  4                                                                                      
#Mis_misassembled contigs        424                                                                                    
Mis_Misassembled contigs length  122377                                                                                 
#Mis_local misassemblies         2                                                                                      
#mismatches                      1814                                                                                   
#indels                          23016                                                                                  
#Mis_short indels (<= 5 bp)      23015                                                                                  
#Mis_long indels (> 5 bp)        1                                                                                      
Indels length                    23548                                                                                  
