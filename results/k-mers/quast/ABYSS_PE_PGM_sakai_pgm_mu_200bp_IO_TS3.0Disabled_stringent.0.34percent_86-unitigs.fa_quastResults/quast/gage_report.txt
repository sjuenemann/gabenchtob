All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_86-unitigs
GAGE_Contigs #                   1383                                                                             
GAGE_Min contig                  200                                                                              
GAGE_Max contig                  49767                                                                            
GAGE_N50                         9159 COUNT: 178                                                                  
GAGE_Genome size                 5594470                                                                          
GAGE_Assembly size               5328073                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     134278(2.40%)                                                                    
GAGE_Missing assembly bases      30(0.00%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                         
GAGE_Duplicated reference bases  11393                                                                            
GAGE_Compressed reference bases  198202                                                                           
GAGE_Bad trim                    30                                                                               
GAGE_Avg idy                     99.97                                                                            
GAGE_SNPs                        58                                                                               
GAGE_Indels < 5bp                1546                                                                             
GAGE_Indels >= 5                 3                                                                                
GAGE_Inversions                  0                                                                                
GAGE_Relocation                  4                                                                                
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          1343                                                                             
GAGE_Corrected assembly size     5318772                                                                          
GAGE_Min correct contig          200                                                                              
GAGE_Max correct contig          49787                                                                            
GAGE_Corrected N50               9159 COUNT: 179                                                                  
