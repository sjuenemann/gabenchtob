All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_101.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_101.final.scaf
#Mis_misassemblies               2                                                                        2                                                               
#Mis_relocations                 2                                                                        2                                                               
#Mis_translocations              0                                                                        0                                                               
#Mis_inversions                  0                                                                        0                                                               
#Mis_misassembled contigs        2                                                                        2                                                               
Mis_Misassembled contigs length  1874                                                                     1874                                                            
#Mis_local misassemblies         32                                                                       758                                                             
#mismatches                      601                                                                      633                                                             
#indels                          87                                                                       1375                                                            
#Mis_short indels (<= 5 bp)      77                                                                       1286                                                            
#Mis_long indels (> 5 bp)        10                                                                       89                                                              
Indels length                    289                                                                      3490                                                            
