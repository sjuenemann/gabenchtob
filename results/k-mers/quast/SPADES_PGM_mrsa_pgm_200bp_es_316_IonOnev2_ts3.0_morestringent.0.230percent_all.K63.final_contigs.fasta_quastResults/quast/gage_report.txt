All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K63.final_contigs
GAGE_Contigs #                   1019                                                                                            
GAGE_Min contig                  210                                                                                             
GAGE_Max contig                  180417                                                                                          
GAGE_N50                         68852 COUNT: 13                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2968958                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     9991(0.36%)                                                                                     
GAGE_Missing assembly bases      82347(2.77%)                                                                                    
GAGE_Missing assembly contigs    305(29.93%)                                                                                     
GAGE_Duplicated reference bases  117901                                                                                          
GAGE_Compressed reference bases  42757                                                                                           
GAGE_Bad trim                    11416                                                                                           
GAGE_Avg idy                     99.97                                                                                           
GAGE_SNPs                        76                                                                                              
GAGE_Indels < 5bp                497                                                                                             
GAGE_Indels >= 5                 13                                                                                              
GAGE_Inversions                  2                                                                                               
GAGE_Relocation                  5                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          180                                                                                             
GAGE_Corrected assembly size     2771990                                                                                         
GAGE_Min correct contig          215                                                                                             
GAGE_Max correct contig          148095                                                                                          
GAGE_Corrected N50               46449 COUNT: 17                                                                                 
