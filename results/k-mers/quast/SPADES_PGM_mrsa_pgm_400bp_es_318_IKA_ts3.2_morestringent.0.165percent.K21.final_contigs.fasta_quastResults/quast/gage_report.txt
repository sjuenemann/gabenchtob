All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent.K21.final_contigs
GAGE_Contigs #                   529                                                                                    
GAGE_Min contig                  201                                                                                    
GAGE_Max contig                  34805                                                                                  
GAGE_N50                         11392 COUNT: 79                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2718140                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     66383(2.36%)                                                                           
GAGE_Missing assembly bases      248(0.01%)                                                                             
GAGE_Missing assembly contigs    1(0.19%)                                                                               
GAGE_Duplicated reference bases  0                                                                                      
GAGE_Compressed reference bases  34781                                                                                  
GAGE_Bad trim                    3                                                                                      
GAGE_Avg idy                     99.99                                                                                  
GAGE_SNPs                        130                                                                                    
GAGE_Indels < 5bp                154                                                                                    
GAGE_Indels >= 5                 5                                                                                      
GAGE_Inversions                  0                                                                                      
GAGE_Relocation                  1                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          534                                                                                    
GAGE_Corrected assembly size     2718681                                                                                
GAGE_Min correct contig          201                                                                                    
GAGE_Max correct contig          34808                                                                                  
GAGE_Corrected N50               11385 COUNT: 79                                                                        
