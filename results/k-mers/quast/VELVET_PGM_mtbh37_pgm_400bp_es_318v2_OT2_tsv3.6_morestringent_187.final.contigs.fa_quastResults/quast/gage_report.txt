All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_187.final.contigs
GAGE_Contigs #                   1993                                                                           
GAGE_Min contig                  373                                                                            
GAGE_Max contig                  7823                                                                           
GAGE_N50                         1119 COUNT: 1055                                                               
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               2918759                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     1582779(35.88%)                                                                
GAGE_Missing assembly bases      470(0.02%)                                                                     
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  28128                                                                          
GAGE_Compressed reference bases  27026                                                                          
GAGE_Bad trim                    330                                                                            
GAGE_Avg idy                     99.94                                                                          
GAGE_SNPs                        42                                                                             
GAGE_Indels < 5bp                1572                                                                           
GAGE_Indels >= 5                 13                                                                             
GAGE_Inversions                  2                                                                              
GAGE_Relocation                  2                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          1965                                                                           
GAGE_Corrected assembly size     2892595                                                                        
GAGE_Min correct contig          248                                                                            
GAGE_Max correct contig          7306                                                                           
GAGE_Corrected N50               1106 COUNT: 1074                                                               
