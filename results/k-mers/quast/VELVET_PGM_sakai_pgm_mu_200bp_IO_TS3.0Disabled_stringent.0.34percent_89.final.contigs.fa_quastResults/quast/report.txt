All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_89.final.contigs
#Contigs (>= 0 bp)             1706                                                                                 
#Contigs (>= 1000 bp)          982                                                                                  
Total length (>= 0 bp)         5349290                                                                              
Total length (>= 1000 bp)      5025218                                                                              
#Contigs                       1645                                                                                 
Largest contig                 35594                                                                                
Total length                   5337869                                                                              
Reference length               5594470                                                                              
GC (%)                         50.40                                                                                
Reference GC (%)               50.48                                                                                
N50                            7311                                                                                 
NG50                           6807                                                                                 
N75                            3677                                                                                 
NG75                           3193                                                                                 
#misassemblies                 82                                                                                   
#local misassemblies           48                                                                                   
#unaligned contigs             0 + 2 part                                                                           
Unaligned contigs length       147                                                                                  
Genome fraction (%)            93.120                                                                               
Duplication ratio              1.008                                                                                
#N's per 100 kbp               24.35                                                                                
#mismatches per 100 kbp        3.67                                                                                 
#indels per 100 kbp            38.97                                                                                
#genes                         4162 + 944 part                                                                      
#predicted genes (unique)      6829                                                                                 
#predicted genes (>= 0 bp)     6839                                                                                 
#predicted genes (>= 300 bp)   5017                                                                                 
#predicted genes (>= 1500 bp)  431                                                                                  
#predicted genes (>= 3000 bp)  24                                                                                   
Largest alignment              31190                                                                                
NA50                           6912                                                                                 
NGA50                          6400                                                                                 
NA75                           3587                                                                                 
NGA75                          2989                                                                                 
