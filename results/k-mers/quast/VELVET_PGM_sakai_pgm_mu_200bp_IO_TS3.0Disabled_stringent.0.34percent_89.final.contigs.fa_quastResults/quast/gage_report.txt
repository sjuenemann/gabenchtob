All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_89.final.contigs
GAGE_Contigs #                   1645                                                                                 
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  35594                                                                                
GAGE_N50                         6807 COUNT: 242                                                                      
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5337869                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     113831(2.03%)                                                                        
GAGE_Missing assembly bases      1377(0.03%)                                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  13975                                                                                
GAGE_Compressed reference bases  260282                                                                               
GAGE_Bad trim                    366                                                                                  
GAGE_Avg idy                     99.95                                                                                
GAGE_SNPs                        116                                                                                  
GAGE_Indels < 5bp                1961                                                                                 
GAGE_Indels >= 5                 44                                                                                   
GAGE_Inversions                  3                                                                                    
GAGE_Relocation                  65                                                                                   
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          1710                                                                                 
GAGE_Corrected assembly size     5324352                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          31206                                                                                
GAGE_Corrected N50               6184 COUNT: 269                                                                      
