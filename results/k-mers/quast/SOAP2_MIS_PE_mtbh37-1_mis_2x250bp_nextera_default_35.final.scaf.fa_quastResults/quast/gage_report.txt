All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_35.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_35.final.scaf
GAGE_Contigs #                   41294                                                                   41294                                                          
GAGE_Min contig                  200                                                                     200                                                            
GAGE_Max contig                  855                                                                     855                                                            
GAGE_N50                         250 COUNT: 8121                                                         250 COUNT: 8121                                                
GAGE_Genome size                 4411532                                                                 4411532                                                        
GAGE_Assembly size               9705174                                                                 9705174                                                        
GAGE_Chaff bases                 0                                                                       0                                                              
GAGE_Missing reference bases     3317411(75.20%)                                                         3317411(75.20%)                                                
GAGE_Missing assembly bases      8599128(88.60%)                                                         8599128(88.60%)                                                
GAGE_Missing assembly contigs    37252(90.21%)                                                           37252(90.21%)                                                  
GAGE_Duplicated reference bases  1458                                                                    1458                                                           
GAGE_Compressed reference bases  1194                                                                    1194                                                           
GAGE_Bad trim                    10547                                                                   10547                                                          
GAGE_Avg idy                     99.89                                                                   99.89                                                          
GAGE_SNPs                        1070                                                                    1070                                                           
GAGE_Indels < 5bp                18                                                                      18                                                             
GAGE_Indels >= 5                 1                                                                       1                                                              
GAGE_Inversions                  0                                                                       0                                                              
GAGE_Relocation                  0                                                                       0                                                              
GAGE_Translocation               0                                                                       0                                                              
GAGE_Corrected contig #          3938                                                                    3938                                                           
GAGE_Corrected assembly size     1092995                                                                 1092995                                                        
GAGE_Min correct contig          200                                                                     200                                                            
GAGE_Max correct contig          855                                                                     855                                                            
GAGE_Corrected N50               0 COUNT: 0                                                              0 COUNT: 0                                                     
