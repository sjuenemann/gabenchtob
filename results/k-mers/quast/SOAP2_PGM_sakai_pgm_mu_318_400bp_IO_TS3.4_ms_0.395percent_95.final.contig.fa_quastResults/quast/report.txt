All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_95.final.contig
#Contigs (>= 0 bp)             149305                                                                   
#Contigs (>= 1000 bp)          19                                                                       
Total length (>= 0 bp)         24662017                                                                 
Total length (>= 1000 bp)      21232                                                                    
#Contigs                       13986                                                                    
Largest contig                 1382                                                                     
Total length                   4333399                                                                  
Reference length               5594470                                                                  
GC (%)                         49.37                                                                    
Reference GC (%)               50.48                                                                    
N50                            308                                                                      
NG50                           290                                                                      
N75                            264                                                                      
NG75                           206                                                                      
#misassemblies                 159                                                                      
#local misassemblies           2                                                                        
#unaligned contigs             171 + 54 part                                                            
Unaligned contigs length       61511                                                                    
Genome fraction (%)            60.728                                                                   
Duplication ratio              1.246                                                                    
#N's per 100 kbp               0.00                                                                     
#mismatches per 100 kbp        18.51                                                                    
#indels per 100 kbp            518.04                                                                   
#genes                         234 + 4358 part                                                          
#predicted genes (unique)      13806                                                                    
#predicted genes (>= 0 bp)     13834                                                                    
#predicted genes (>= 300 bp)   2602                                                                     
#predicted genes (>= 1500 bp)  0                                                                        
#predicted genes (>= 3000 bp)  0                                                                        
Largest alignment              1382                                                                     
NA50                           305                                                                      
NGA50                          287                                                                      
NA75                           253                                                                      
NGA75                          194                                                                      
