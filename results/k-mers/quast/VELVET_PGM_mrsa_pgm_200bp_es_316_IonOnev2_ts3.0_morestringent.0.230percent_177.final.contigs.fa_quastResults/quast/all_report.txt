All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_177.final.contigs
#Contigs                                   14                                                                                          
#Contigs (>= 0 bp)                         14                                                                                          
#Contigs (>= 1000 bp)                      1                                                                                           
Largest contig                             1059                                                                                        
Total length                               8956                                                                                        
Total length (>= 0 bp)                     8956                                                                                        
Total length (>= 1000 bp)                  1059                                                                                        
Reference length                           2813862                                                                                     
N50                                        662                                                                                         
NG50                                       None                                                                                        
N75                                        548                                                                                         
NG75                                       None                                                                                        
L50                                        6                                                                                           
LG50                                       None                                                                                        
L75                                        10                                                                                          
LG75                                       None                                                                                        
#local misassemblies                       0                                                                                           
#misassemblies                             1                                                                                           
#misassembled contigs                      1                                                                                           
Misassembled contigs length                732                                                                                         
Misassemblies inter-contig overlap         0                                                                                           
#unaligned contigs                         0 + 0 part                                                                                  
Unaligned contigs length                   0                                                                                           
#ambiguously mapped contigs                2                                                                                           
Extra bases in ambiguously mapped contigs  -1765                                                                                       
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        0.249                                                                                       
Duplication ratio                          1.025                                                                                       
#genes                                     0 + 10 part                                                                                 
#predicted genes (unique)                  13                                                                                          
#predicted genes (>= 0 bp)                 13                                                                                          
#predicted genes (>= 300 bp)               9                                                                                           
#predicted genes (>= 1500 bp)              0                                                                                           
#predicted genes (>= 3000 bp)              0                                                                                           
#mismatches                                0                                                                                           
#indels                                    3                                                                                           
Indels length                              3                                                                                           
#mismatches per 100 kbp                    0.00                                                                                        
#indels per 100 kbp                        42.78                                                                                       
GC (%)                                     37.24                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               99.831                                                                                      
Largest alignment                          900                                                                                         
NA50                                       548                                                                                         
NGA50                                      None                                                                                        
NA75                                       356                                                                                         
NGA75                                      None                                                                                        
LA50                                       7                                                                                           
LGA50                                      None                                                                                        
LA75                                       12                                                                                          
LGA75                                      None                                                                                        
#Mis_misassemblies                         1                                                                                           
#Mis_relocations                           1                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  1                                                                                           
Mis_Misassembled contigs length            732                                                                                         
#Mis_local misassemblies                   0                                                                                           
#Mis_short indels (<= 5 bp)                3                                                                                           
#Mis_long indels (> 5 bp)                  0                                                                                           
#Una_fully unaligned contigs               0                                                                                           
Una_Fully unaligned length                 0                                                                                           
#Una_partially unaligned contigs           0                                                                                           
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             0                                                                                           
GAGE_Contigs #                             14                                                                                          
GAGE_Min contig                            356                                                                                         
GAGE_Max contig                            1059                                                                                        
GAGE_N50                                   0 COUNT: 0                                                                                  
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         8956                                                                                        
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               2792251(99.23%)                                                                             
GAGE_Missing assembly bases                0(0.00%)                                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            0                                                                                           
GAGE_Compressed reference bases            13534                                                                                       
GAGE_Bad trim                              0                                                                                           
GAGE_Avg idy                               99.94                                                                                       
GAGE_SNPs                                  0                                                                                           
GAGE_Indels < 5bp                          4                                                                                           
GAGE_Indels >= 5                           0                                                                                           
GAGE_Inversions                            0                                                                                           
GAGE_Relocation                            1                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    15                                                                                          
GAGE_Corrected assembly size               8957                                                                                        
GAGE_Min correct contig                    210                                                                                         
GAGE_Max correct contig                    1059                                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                                  
