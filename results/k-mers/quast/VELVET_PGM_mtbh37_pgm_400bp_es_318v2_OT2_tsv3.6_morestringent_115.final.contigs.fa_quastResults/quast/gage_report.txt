All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_115.final.contigs
GAGE_Contigs #                   15833                                                                          
GAGE_Min contig                  229                                                                            
GAGE_Max contig                  853                                                                            
GAGE_N50                         239 COUNT: 7776                                                                
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               4062288                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     1972049(44.70%)                                                                
GAGE_Missing assembly bases      997(0.02%)                                                                     
GAGE_Missing assembly contigs    1(0.01%)                                                                       
GAGE_Duplicated reference bases  1114890                                                                        
GAGE_Compressed reference bases  53815                                                                          
GAGE_Bad trim                    569                                                                            
GAGE_Avg idy                     99.20                                                                          
GAGE_SNPs                        2820                                                                           
GAGE_Indels < 5bp                13491                                                                          
GAGE_Indels >= 5                 12                                                                             
GAGE_Inversions                  67                                                                             
GAGE_Relocation                  34                                                                             
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          11213                                                                          
GAGE_Corrected assembly size     2894925                                                                        
GAGE_Min correct contig          200                                                                            
GAGE_Max correct contig          857                                                                            
GAGE_Corrected N50               229 COUNT: 8197                                                                
