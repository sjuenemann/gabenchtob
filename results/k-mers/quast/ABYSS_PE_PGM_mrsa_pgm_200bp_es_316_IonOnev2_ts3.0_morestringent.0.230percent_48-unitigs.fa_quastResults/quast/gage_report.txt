All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_48-unitigs
GAGE_Contigs #                   425                                                                                    
GAGE_Min contig                  203                                                                                    
GAGE_Max contig                  69773                                                                                  
GAGE_N50                         13518 COUNT: 62                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2743027                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     59241(2.11%)                                                                           
GAGE_Missing assembly bases      74(0.00%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  211                                                                                    
GAGE_Compressed reference bases  19336                                                                                  
GAGE_Bad trim                    74                                                                                     
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        64                                                                                     
GAGE_Indels < 5bp                270                                                                                    
GAGE_Indels >= 5                 6                                                                                      
GAGE_Inversions                  0                                                                                      
GAGE_Relocation                  2                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          431                                                                                    
GAGE_Corrected assembly size     2744088                                                                                
GAGE_Min correct contig          203                                                                                    
GAGE_Max correct contig          69783                                                                                  
GAGE_Corrected N50               13348 COUNT: 63                                                                        
