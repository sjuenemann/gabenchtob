All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_55.final.contigs
#Contigs                                   1344                                                                                 
#Contigs (>= 0 bp)                         1778                                                                                 
#Contigs (>= 1000 bp)                      767                                                                                  
Largest contig                             40538                                                                                
Total length                               5306998                                                                              
Total length (>= 0 bp)                     5372329                                                                              
Total length (>= 1000 bp)                  5054534                                                                              
Reference length                           5594470                                                                              
N50                                        9905                                                                                 
NG50                                       9254                                                                                 
N75                                        5179                                                                                 
NG75                                       4363                                                                                 
L50                                        169                                                                                  
LG50                                       184                                                                                  
L75                                        349                                                                                  
LG75                                       394                                                                                  
#local misassemblies                       19                                                                                   
#misassemblies                             11                                                                                   
#misassembled contigs                      10                                                                                   
Misassembled contigs length                104711                                                                               
Misassemblies inter-contig overlap         1409                                                                                 
#unaligned contigs                         1 + 8 part                                                                           
Unaligned contigs length                   1697                                                                                 
#ambiguously mapped contigs                174                                                                                  
Extra bases in ambiguously mapped contigs  -78060                                                                               
#N's                                       380                                                                                  
#N's per 100 kbp                           7.16                                                                                 
Genome fraction (%)                        93.124                                                                               
Duplication ratio                          1.004                                                                                
#genes                                     4322 + 722 part                                                                      
#predicted genes (unique)                  6704                                                                                 
#predicted genes (>= 0 bp)                 6707                                                                                 
#predicted genes (>= 300 bp)               4993                                                                                 
#predicted genes (>= 1500 bp)              430                                                                                  
#predicted genes (>= 3000 bp)              25                                                                                   
#mismatches                                188                                                                                  
#indels                                    2781                                                                                 
Indels length                              3185                                                                                 
#mismatches per 100 kbp                    3.61                                                                                 
#indels per 100 kbp                        53.38                                                                                
GC (%)                                     50.26                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               99.683                                                                               
Largest alignment                          40538                                                                                
NA50                                       9846                                                                                 
NGA50                                      9206                                                                                 
NA75                                       5159                                                                                 
NGA75                                      4356                                                                                 
LA50                                       171                                                                                  
LGA50                                      186                                                                                  
LA75                                       352                                                                                  
LGA75                                      397                                                                                  
#Mis_misassemblies                         11                                                                                   
#Mis_relocations                           11                                                                                   
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  10                                                                                   
Mis_Misassembled contigs length            104711                                                                               
#Mis_local misassemblies                   19                                                                                   
#Mis_short indels (<= 5 bp)                2774                                                                                 
#Mis_long indels (> 5 bp)                  7                                                                                    
#Una_fully unaligned contigs               1                                                                                    
Una_Fully unaligned length                 835                                                                                  
#Una_partially unaligned contigs           8                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             862                                                                                  
GAGE_Contigs #                             1344                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            40538                                                                                
GAGE_N50                                   9254 COUNT: 184                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5306998                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               135074(2.41%)                                                                        
GAGE_Missing assembly bases                602(0.01%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            4770                                                                                 
GAGE_Compressed reference bases            195490                                                                               
GAGE_Bad trim                              332                                                                                  
GAGE_Avg idy                               99.95                                                                                
GAGE_SNPs                                  171                                                                                  
GAGE_Indels < 5bp                          2548                                                                                 
GAGE_Indels >= 5                           30                                                                                   
GAGE_Inversions                            2                                                                                    
GAGE_Relocation                            10                                                                                   
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1369                                                                                 
GAGE_Corrected assembly size               5304614                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    40556                                                                                
GAGE_Corrected N50                         8705 COUNT: 195                                                                      
