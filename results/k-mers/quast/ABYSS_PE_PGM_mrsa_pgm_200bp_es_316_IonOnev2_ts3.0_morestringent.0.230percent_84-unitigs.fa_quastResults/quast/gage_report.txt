All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_84-unitigs
GAGE_Contigs #                   613                                                                                    
GAGE_Min contig                  202                                                                                    
GAGE_Max contig                  60715                                                                                  
GAGE_N50                         9143 COUNT: 93                                                                         
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2764133                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     40059(1.42%)                                                                           
GAGE_Missing assembly bases      5(0.00%)                                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  2126                                                                                   
GAGE_Compressed reference bases  27273                                                                                  
GAGE_Bad trim                    5                                                                                      
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        40                                                                                     
GAGE_Indels < 5bp                485                                                                                    
GAGE_Indels >= 5                 3                                                                                      
GAGE_Inversions                  0                                                                                      
GAGE_Relocation                  1                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          612                                                                                    
GAGE_Corrected assembly size     2762438                                                                                
GAGE_Min correct contig          202                                                                                    
GAGE_Max correct contig          60720                                                                                  
GAGE_Corrected N50               9118 COUNT: 94                                                                         
