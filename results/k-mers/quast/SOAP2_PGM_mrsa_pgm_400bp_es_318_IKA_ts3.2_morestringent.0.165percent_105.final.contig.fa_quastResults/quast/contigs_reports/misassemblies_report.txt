All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_105.final.contig
#Mis_misassemblies               224                                                                                  
#Mis_relocations                 208                                                                                  
#Mis_translocations              15                                                                                   
#Mis_inversions                  1                                                                                    
#Mis_misassembled contigs        224                                                                                  
Mis_Misassembled contigs length  76790                                                                                
#Mis_local misassemblies         2                                                                                    
#mismatches                      2524                                                                                 
#indels                          37376                                                                                
#Mis_short indels (<= 5 bp)      37376                                                                                
#Mis_long indels (> 5 bp)        0                                                                                    
Indels length                    38450                                                                                
