All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_80-unitigs
#Mis_misassemblies               3                                                                                
#Mis_relocations                 2                                                                                
#Mis_translocations              0                                                                                
#Mis_inversions                  1                                                                                
#Mis_misassembled contigs        3                                                                                
Mis_Misassembled contigs length  3232                                                                             
#Mis_local misassemblies         4                                                                                
#mismatches                      61                                                                               
#indels                          1422                                                                             
#Mis_short indels (<= 5 bp)      1422                                                                             
#Mis_long indels (> 5 bp)        0                                                                                
Indels length                    1473                                                                             
