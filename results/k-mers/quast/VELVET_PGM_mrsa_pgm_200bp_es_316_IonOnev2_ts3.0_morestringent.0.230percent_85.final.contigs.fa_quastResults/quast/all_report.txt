All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_85.final.contigs
#Contigs                                   1118                                                                                       
#Contigs (>= 0 bp)                         1137                                                                                       
#Contigs (>= 1000 bp)                      719                                                                                        
Largest contig                             19187                                                                                      
Total length                               2668362                                                                                    
Total length (>= 0 bp)                     2671793                                                                                    
Total length (>= 1000 bp)                  2444525                                                                                    
Reference length                           2813862                                                                                    
N50                                        4068                                                                                       
NG50                                       3809                                                                                       
N75                                        2186                                                                                       
NG75                                       1837                                                                                       
L50                                        199                                                                                        
LG50                                       218                                                                                        
L75                                        422                                                                                        
LG75                                       477                                                                                        
#local misassemblies                       11                                                                                         
#misassemblies                             52                                                                                         
#misassembled contigs                      50                                                                                         
Misassembled contigs length                121613                                                                                     
Misassemblies inter-contig overlap         1592                                                                                       
#unaligned contigs                         1 + 4 part                                                                                 
Unaligned contigs length                   975                                                                                        
#ambiguously mapped contigs                21                                                                                         
Extra bases in ambiguously mapped contigs  -8764                                                                                      
#N's                                       160                                                                                        
#N's per 100 kbp                           6.00                                                                                       
Genome fraction (%)                        93.754                                                                                     
Duplication ratio                          1.008                                                                                      
#genes                                     1932 + 693 part                                                                            
#predicted genes (unique)                  3233                                                                                       
#predicted genes (>= 0 bp)                 3237                                                                                       
#predicted genes (>= 300 bp)               2454                                                                                       
#predicted genes (>= 1500 bp)              212                                                                                        
#predicted genes (>= 3000 bp)              16                                                                                         
#mismatches                                105                                                                                        
#indels                                    788                                                                                        
Indels length                              810                                                                                        
#mismatches per 100 kbp                    3.98                                                                                       
#indels per 100 kbp                        29.87                                                                                      
GC (%)                                     32.75                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.590                                                                                     
Largest alignment                          19187                                                                                      
NA50                                       4059                                                                                       
NGA50                                      3783                                                                                       
NA75                                       2164                                                                                       
NGA75                                      1813                                                                                       
LA50                                       201                                                                                        
LGA50                                      219                                                                                        
LA75                                       426                                                                                        
LGA75                                      481                                                                                        
#Mis_misassemblies                         52                                                                                         
#Mis_relocations                           21                                                                                         
#Mis_translocations                        0                                                                                          
#Mis_inversions                            31                                                                                         
#Mis_misassembled contigs                  50                                                                                         
Mis_Misassembled contigs length            121613                                                                                     
#Mis_local misassemblies                   11                                                                                         
#Mis_short indels (<= 5 bp)                788                                                                                        
#Mis_long indels (> 5 bp)                  0                                                                                          
#Una_fully unaligned contigs               1                                                                                          
Una_Fully unaligned length                 796                                                                                        
#Una_partially unaligned contigs           4                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             179                                                                                        
GAGE_Contigs #                             1118                                                                                       
GAGE_Min contig                            204                                                                                        
GAGE_Max contig                            19187                                                                                      
GAGE_N50                                   3809 COUNT: 218                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         2668362                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               130741(4.65%)                                                                              
GAGE_Missing assembly bases                393(0.01%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                   
GAGE_Duplicated reference bases            4221                                                                                       
GAGE_Compressed reference bases            43012                                                                                      
GAGE_Bad trim                              263                                                                                        
GAGE_Avg idy                               99.97                                                                                      
GAGE_SNPs                                  55                                                                                         
GAGE_Indels < 5bp                          616                                                                                        
GAGE_Indels >= 5                           14                                                                                         
GAGE_Inversions                            7                                                                                          
GAGE_Relocation                            3                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    1137                                                                                       
GAGE_Corrected assembly size               2665170                                                                                    
GAGE_Min correct contig                    202                                                                                        
GAGE_Max correct contig                    19192                                                                                      
GAGE_Corrected N50                         3751 COUNT: 222                                                                            
