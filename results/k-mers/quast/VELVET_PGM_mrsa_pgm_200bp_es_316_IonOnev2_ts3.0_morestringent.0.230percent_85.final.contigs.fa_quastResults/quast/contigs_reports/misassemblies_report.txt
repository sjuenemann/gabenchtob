All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_85.final.contigs
#Mis_misassemblies               52                                                                                         
#Mis_relocations                 21                                                                                         
#Mis_translocations              0                                                                                          
#Mis_inversions                  31                                                                                         
#Mis_misassembled contigs        50                                                                                         
Mis_Misassembled contigs length  121613                                                                                     
#Mis_local misassemblies         11                                                                                         
#mismatches                      105                                                                                        
#indels                          788                                                                                        
#Mis_short indels (<= 5 bp)      788                                                                                        
#Mis_long indels (> 5 bp)        0                                                                                          
Indels length                    810                                                                                        
