All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_83.final.contig
GAGE_Contigs #                   12746                                                                    
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  1382                                                                     
GAGE_N50                         251 COUNT: 8976                                                          
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               3639040                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     2536410(45.34%)                                                          
GAGE_Missing assembly bases      44069(1.21%)                                                             
GAGE_Missing assembly contigs    96(0.75%)                                                                
GAGE_Duplicated reference bases  357335                                                                   
GAGE_Compressed reference bases  137785                                                                   
GAGE_Bad trim                    15773                                                                    
GAGE_Avg idy                     99.21                                                                    
GAGE_SNPs                        695                                                                      
GAGE_Indels < 5bp                18620                                                                    
GAGE_Indels >= 5                 13                                                                       
GAGE_Inversions                  43                                                                       
GAGE_Relocation                  20                                                                       
GAGE_Translocation               8                                                                        
GAGE_Corrected contig #          11038                                                                    
GAGE_Corrected assembly size     3173494                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          1382                                                                     
GAGE_Corrected N50               224 COUNT: 9257                                                          
