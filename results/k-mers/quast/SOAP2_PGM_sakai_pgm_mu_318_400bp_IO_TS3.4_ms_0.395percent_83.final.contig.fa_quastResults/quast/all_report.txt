All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_83.final.contig
#Contigs                                   12746                                                                    
#Contigs (>= 0 bp)                         186548                                                                   
#Contigs (>= 1000 bp)                      4                                                                        
Largest contig                             1382                                                                     
Total length                               3639040                                                                  
Total length (>= 0 bp)                     26393896                                                                 
Total length (>= 1000 bp)                  4520                                                                     
Reference length                           5594470                                                                  
N50                                        277                                                                      
NG50                                       251                                                                      
N75                                        252                                                                      
NG75                                       None                                                                     
L50                                        5254                                                                     
LG50                                       8976                                                                     
L75                                        8706                                                                     
LG75                                       None                                                                     
#local misassemblies                       2                                                                        
#misassemblies                             146                                                                      
#misassembled contigs                      146                                                                      
Misassembled contigs length                40633                                                                    
Misassemblies inter-contig overlap         375                                                                      
#unaligned contigs                         400 + 81 part                                                            
Unaligned contigs length                   122891                                                                   
#ambiguously mapped contigs                116                                                                      
Extra bases in ambiguously mapped contigs  -29681                                                                   
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        51.577                                                                   
Duplication ratio                          1.208                                                                    
#genes                                     157 + 4179 part                                                          
#predicted genes (unique)                  11855                                                                    
#predicted genes (>= 0 bp)                 11870                                                                    
#predicted genes (>= 300 bp)               1633                                                                     
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                762                                                                      
#indels                                    20331                                                                    
Indels length                              20757                                                                    
#mismatches per 100 kbp                    26.41                                                                    
#indels per 100 kbp                        704.60                                                                   
GC (%)                                     49.36                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.100                                                                   
Largest alignment                          1382                                                                     
NA50                                       273                                                                      
NGA50                                      239                                                                      
NA75                                       244                                                                      
NGA75                                      None                                                                     
LA50                                       5292                                                                     
LGA50                                      9080                                                                     
LA75                                       8798                                                                     
LGA75                                      None                                                                     
#Mis_misassemblies                         146                                                                      
#Mis_relocations                           137                                                                      
#Mis_translocations                        9                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  146                                                                      
Mis_Misassembled contigs length            40633                                                                    
#Mis_local misassemblies                   2                                                                        
#Mis_short indels (<= 5 bp)                20331                                                                    
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               400                                                                      
Una_Fully unaligned length                 113007                                                                   
#Una_partially unaligned contigs           81                                                                       
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             9884                                                                     
GAGE_Contigs #                             12746                                                                    
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            1382                                                                     
GAGE_N50                                   251 COUNT: 8976                                                          
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         3639040                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               2536410(45.34%)                                                          
GAGE_Missing assembly bases                44069(1.21%)                                                             
GAGE_Missing assembly contigs              96(0.75%)                                                                
GAGE_Duplicated reference bases            357335                                                                   
GAGE_Compressed reference bases            137785                                                                   
GAGE_Bad trim                              15773                                                                    
GAGE_Avg idy                               99.21                                                                    
GAGE_SNPs                                  695                                                                      
GAGE_Indels < 5bp                          18620                                                                    
GAGE_Indels >= 5                           13                                                                       
GAGE_Inversions                            43                                                                       
GAGE_Relocation                            20                                                                       
GAGE_Translocation                         8                                                                        
GAGE_Corrected contig #                    11038                                                                    
GAGE_Corrected assembly size               3173494                                                                  
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    1382                                                                     
GAGE_Corrected N50                         224 COUNT: 9257                                                          
