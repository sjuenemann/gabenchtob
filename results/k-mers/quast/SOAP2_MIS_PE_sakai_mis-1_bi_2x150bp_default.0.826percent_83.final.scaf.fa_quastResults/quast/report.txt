All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_83.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_83.final.scaf
#Contigs (>= 0 bp)             1507                                                                           1111                                                                  
#Contigs (>= 1000 bp)          213                                                                            132                                                                   
Total length (>= 0 bp)         5476191                                                                        5496584                                                               
Total length (>= 1000 bp)      5201943                                                                        5302067                                                               
#Contigs                       612                                                                            360                                                                   
Largest contig                 221578                                                                         374898                                                                
Total length                   5348906                                                                        5390482                                                               
Reference length               5594470                                                                        5594470                                                               
GC (%)                         50.27                                                                          50.29                                                                 
Reference GC (%)               50.48                                                                          50.48                                                                 
N50                            63065                                                                          144832                                                                
NG50                           61001                                                                          142690                                                                
N75                            30764                                                                          76232                                                                 
NG75                           29496                                                                          73065                                                                 
#misassemblies                 7                                                                              44                                                                    
#local misassemblies           2                                                                              101                                                                   
#unaligned contigs             1 + 0 part                                                                     10 + 4 part                                                           
Unaligned contigs length       5385                                                                           19696                                                                 
Genome fraction (%)            93.814                                                                         94.025                                                                
Duplication ratio              1.000                                                                          1.008                                                                 
#N's per 100 kbp               1.14                                                                           379.45                                                                
#mismatches per 100 kbp        1.07                                                                           4.05                                                                  
#indels per 100 kbp            0.40                                                                           34.43                                                                 
#genes                         4839 + 260 part                                                                4937 + 185 part                                                       
#predicted genes (unique)      5487                                                                           5435                                                                  
#predicted genes (>= 0 bp)     5493                                                                           5439                                                                  
#predicted genes (>= 300 bp)   4435                                                                           4434                                                                  
#predicted genes (>= 1500 bp)  663                                                                            663                                                                   
#predicted genes (>= 3000 bp)  66                                                                             65                                                                    
Largest alignment              217350                                                                         362676                                                                
NA50                           63065                                                                          144801                                                                
NGA50                          61001                                                                          142690                                                                
NA75                           30742                                                                          76232                                                                 
NGA75                          28809                                                                          72946                                                                 
