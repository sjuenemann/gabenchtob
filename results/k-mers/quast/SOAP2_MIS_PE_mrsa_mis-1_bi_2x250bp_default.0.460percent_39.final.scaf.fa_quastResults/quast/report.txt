All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_39.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_39.final.scaf
#Contigs (>= 0 bp)             17377                                                                         16408                                                                
#Contigs (>= 1000 bp)          716                                                                           843                                                                  
Total length (>= 0 bp)         4775884                                                                       4812437                                                              
Total length (>= 1000 bp)      1148394                                                                       1577212                                                              
#Contigs                       6695                                                                          6098                                                                 
Largest contig                 6427                                                                          6629                                                                 
Total length                   3220837                                                                       3308285                                                              
Reference length               2813862                                                                       2813862                                                              
GC (%)                         33.69                                                                         33.68                                                                
Reference GC (%)               32.81                                                                         32.81                                                                
N50                            674                                                                           927                                                                  
NG50                           814                                                                           1169                                                                 
N75                            251                                                                           274                                                                  
NG75                           396                                                                           521                                                                  
#misassemblies                 1                                                                             1                                                                    
#local misassemblies           34                                                                            281                                                                  
#unaligned contigs             3319 + 3 part                                                                 3448 + 57 part                                                       
Unaligned contigs length       824558                                                                        1008952                                                              
Genome fraction (%)            83.990                                                                        79.572                                                               
Duplication ratio              1.013                                                                         1.026                                                                
#N's per 100 kbp               37.23                                                                         1140.38                                                              
#mismatches per 100 kbp        1.40                                                                          1.34                                                                 
#indels per 100 kbp            20.86                                                                         90.57                                                                
#genes                         751 + 1782 part                                                               852 + 1585 part                                                      
#predicted genes (unique)      7690                                                                          7408                                                                 
#predicted genes (>= 0 bp)     7690                                                                          7408                                                                 
#predicted genes (>= 300 bp)   2792                                                                          2541                                                                 
#predicted genes (>= 1500 bp)  26                                                                            18                                                                   
#predicted genes (>= 3000 bp)  0                                                                             0                                                                    
Largest alignment              6427                                                                          6546                                                                 
NA50                           654                                                                           708                                                                  
NGA50                          792                                                                           954                                                                  
NA75                           None                                                                          None                                                                 
NGA75                          361                                                                           319                                                                  
