All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_25.final.contig
#Contigs                                   280                                                                                      
#Contigs (>= 0 bp)                         250208                                                                                   
#Contigs (>= 1000 bp)                      0                                                                                        
Largest contig                             401                                                                                      
Total length                               64491                                                                                    
Total length (>= 0 bp)                     12947879                                                                                 
Total length (>= 1000 bp)                  0                                                                                        
Reference length                           2813862                                                                                  
N50                                        222                                                                                      
NG50                                       None                                                                                     
N75                                        210                                                                                      
NG75                                       None                                                                                     
L50                                        127                                                                                      
LG50                                       None                                                                                     
L75                                        202                                                                                      
LG75                                       None                                                                                     
#local misassemblies                       0                                                                                        
#misassemblies                             0                                                                                        
#misassembled contigs                      0                                                                                        
Misassembled contigs length                0                                                                                        
Misassemblies inter-contig overlap         0                                                                                        
#unaligned contigs                         15 + 0 part                                                                              
Unaligned contigs length                   3264                                                                                     
#ambiguously mapped contigs                0                                                                                        
Extra bases in ambiguously mapped contigs  0                                                                                        
#N's                                       0                                                                                        
#N's per 100 kbp                           0.00                                                                                     
Genome fraction (%)                        2.171                                                                                    
Duplication ratio                          1.002                                                                                    
#genes                                     2 + 226 part                                                                             
#predicted genes (unique)                  261                                                                                      
#predicted genes (>= 0 bp)                 261                                                                                      
#predicted genes (>= 300 bp)               8                                                                                        
#predicted genes (>= 1500 bp)              0                                                                                        
#predicted genes (>= 3000 bp)              0                                                                                        
#mismatches                                28                                                                                       
#indels                                    104                                                                                      
Indels length                              107                                                                                      
#mismatches per 100 kbp                    45.83                                                                                    
#indels per 100 kbp                        170.22                                                                                   
GC (%)                                     34.40                                                                                    
Reference GC (%)                           32.81                                                                                    
Average %IDY                               99.764                                                                                   
Largest alignment                          401                                                                                      
NA50                                       221                                                                                      
NGA50                                      None                                                                                     
NA75                                       209                                                                                      
NGA75                                      None                                                                                     
LA50                                       128                                                                                      
LGA50                                      None                                                                                     
LA75                                       203                                                                                      
LGA75                                      None                                                                                     
#Mis_misassemblies                         0                                                                                        
#Mis_relocations                           0                                                                                        
#Mis_translocations                        0                                                                                        
#Mis_inversions                            0                                                                                        
#Mis_misassembled contigs                  0                                                                                        
Mis_Misassembled contigs length            0                                                                                        
#Mis_local misassemblies                   0                                                                                        
#Mis_short indels (<= 5 bp)                104                                                                                      
#Mis_long indels (> 5 bp)                  0                                                                                        
#Una_fully unaligned contigs               15                                                                                       
Una_Fully unaligned length                 3264                                                                                     
#Una_partially unaligned contigs           0                                                                                        
#Una_with misassembly                      0                                                                                        
#Una_both parts are significant            0                                                                                        
Una_Partially unaligned length             0                                                                                        
GAGE_Contigs #                             280                                                                                      
GAGE_Min contig                            200                                                                                      
GAGE_Max contig                            401                                                                                      
GAGE_N50                                   0 COUNT: 0                                                                               
GAGE_Genome size                           2813862                                                                                  
GAGE_Assembly size                         64491                                                                                    
GAGE_Chaff bases                           0                                                                                        
GAGE_Missing reference bases               2752790(97.83%)                                                                          
GAGE_Missing assembly bases                3323(5.15%)                                                                              
GAGE_Missing assembly contigs              15(5.36%)                                                                                
GAGE_Duplicated reference bases            0                                                                                        
GAGE_Compressed reference bases            0                                                                                        
GAGE_Bad trim                              59                                                                                       
GAGE_Avg idy                               99.78                                                                                    
GAGE_SNPs                                  26                                                                                       
GAGE_Indels < 5bp                          95                                                                                       
GAGE_Indels >= 5                           0                                                                                        
GAGE_Inversions                            0                                                                                        
GAGE_Relocation                            0                                                                                        
GAGE_Translocation                         0                                                                                        
GAGE_Corrected contig #                    263                                                                                      
GAGE_Corrected assembly size               60750                                                                                    
GAGE_Min correct contig                    200                                                                                      
GAGE_Max correct contig                    401                                                                                      
GAGE_Corrected N50                         0 COUNT: 0                                                                               
