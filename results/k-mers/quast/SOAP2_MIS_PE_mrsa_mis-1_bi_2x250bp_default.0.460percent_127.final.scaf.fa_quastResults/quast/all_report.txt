All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_127.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_127.final.scaf
#Contigs                                   206                                                                            190                                                                   
#Contigs (>= 0 bp)                         312                                                                            295                                                                   
#Contigs (>= 1000 bp)                      74                                                                             66                                                                    
Largest contig                             237912                                                                         237912                                                                
Total length                               2814413                                                                        2815110                                                               
Total length (>= 0 bp)                     2830847                                                                        2831364                                                               
Total length (>= 1000 bp)                  2771475                                                                        2776321                                                               
Reference length                           2813862                                                                        2813862                                                               
N50                                        85171                                                                          87224                                                                 
NG50                                       85171                                                                          87224                                                                 
N75                                        35856                                                                          45162                                                                 
NG75                                       35856                                                                          45162                                                                 
L50                                        11                                                                             10                                                                    
LG50                                       11                                                                             10                                                                    
L75                                        25                                                                             21                                                                    
LG75                                       25                                                                             21                                                                    
#local misassemblies                       0                                                                              7                                                                     
#misassemblies                             3                                                                              4                                                                     
#misassembled contigs                      3                                                                              4                                                                     
Misassembled contigs length                68988                                                                          71328                                                                 
Misassemblies inter-contig overlap         1                                                                              1                                                                     
#unaligned contigs                         3 + 1 part                                                                     3 + 1 part                                                            
Unaligned contigs length                   6449                                                                           6449                                                                  
#ambiguously mapped contigs                60                                                                             58                                                                    
Extra bases in ambiguously mapped contigs  -18621                                                                         -18093                                                                
#N's                                       36                                                                             553                                                                   
#N's per 100 kbp                           1.28                                                                           19.64                                                                 
Genome fraction (%)                        98.673                                                                         98.693                                                                
Duplication ratio                          1.005                                                                          1.005                                                                 
#genes                                     2655 + 56 part                                                                 2658 + 54 part                                                        
#predicted genes (unique)                  2744                                                                           2743                                                                  
#predicted genes (>= 0 bp)                 2751                                                                           2748                                                                  
#predicted genes (>= 300 bp)               2304                                                                           2303                                                                  
#predicted genes (>= 1500 bp)              297                                                                            296                                                                   
#predicted genes (>= 3000 bp)              31                                                                             30                                                                    
#mismatches                                58                                                                             52                                                                    
#indels                                    21                                                                             30                                                                    
Indels length                              180                                                                            402                                                                   
#mismatches per 100 kbp                    2.09                                                                           1.87                                                                  
#indels per 100 kbp                        0.76                                                                           1.08                                                                  
GC (%)                                     32.75                                                                          32.75                                                                 
Reference GC (%)                           32.81                                                                          32.81                                                                 
Average %IDY                               99.039                                                                         99.028                                                                
Largest alignment                          237912                                                                         237912                                                                
NA50                                       85171                                                                          87224                                                                 
NGA50                                      85171                                                                          87224                                                                 
NA75                                       35856                                                                          45162                                                                 
NGA75                                      35856                                                                          45162                                                                 
LA50                                       11                                                                             10                                                                    
LGA50                                      11                                                                             10                                                                    
LA75                                       25                                                                             21                                                                    
LGA75                                      25                                                                             21                                                                    
#Mis_misassemblies                         3                                                                              4                                                                     
#Mis_relocations                           3                                                                              4                                                                     
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  3                                                                              4                                                                     
Mis_Misassembled contigs length            68988                                                                          71328                                                                 
#Mis_local misassemblies                   0                                                                              7                                                                     
#Mis_short indels (<= 5 bp)                17                                                                             20                                                                    
#Mis_long indels (> 5 bp)                  4                                                                              10                                                                    
#Una_fully unaligned contigs               3                                                                              3                                                                     
Una_Fully unaligned length                 5895                                                                           5895                                                                  
#Una_partially unaligned contigs           1                                                                              1                                                                     
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            1                                                                              1                                                                     
Una_Partially unaligned length             554                                                                            554                                                                   
GAGE_Contigs #                             206                                                                            190                                                                   
GAGE_Min contig                            201                                                                            201                                                                   
GAGE_Max contig                            237912                                                                         237912                                                                
GAGE_N50                                   85171 COUNT: 11                                                                87224 COUNT: 10                                                       
GAGE_Genome size                           2813862                                                                        2813862                                                               
GAGE_Assembly size                         2814413                                                                        2815110                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               1434(0.05%)                                                                    1408(0.05%)                                                           
GAGE_Missing assembly bases                5908(0.21%)                                                                    6351(0.23%)                                                           
GAGE_Missing assembly contigs              3(1.46%)                                                                       3(1.58%)                                                              
GAGE_Duplicated reference bases            10084                                                                          10609                                                                 
GAGE_Compressed reference bases            38896                                                                          38910                                                                 
GAGE_Bad trim                              2                                                                              2                                                                     
GAGE_Avg idy                               99.99                                                                          99.99                                                                 
GAGE_SNPs                                  49                                                                             49                                                                    
GAGE_Indels < 5bp                          17                                                                             22                                                                    
GAGE_Indels >= 5                           1                                                                              7                                                                     
GAGE_Inversions                            0                                                                              0                                                                     
GAGE_Relocation                            2                                                                              3                                                                     
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    173                                                                            169                                                                   
GAGE_Corrected assembly size               2798127                                                                        2797679                                                               
GAGE_Min correct contig                    201                                                                            201                                                                   
GAGE_Max correct contig                    205606                                                                         205606                                                                
GAGE_Corrected N50                         78544 COUNT: 12                                                                78544 COUNT: 12                                                       
