All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_127.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_127.final.scaf
#Una_fully unaligned contigs      3                                                                              3                                                                     
Una_Fully unaligned length        5895                                                                           5895                                                                  
#Una_partially unaligned contigs  1                                                                              1                                                                     
#Una_with misassembly             0                                                                              0                                                                     
#Una_both parts are significant   1                                                                              1                                                                     
Una_Partially unaligned length    554                                                                            554                                                                   
#N's                              36                                                                             553                                                                   
