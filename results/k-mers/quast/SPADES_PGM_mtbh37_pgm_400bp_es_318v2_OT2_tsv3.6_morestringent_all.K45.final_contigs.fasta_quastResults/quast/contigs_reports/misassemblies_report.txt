All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K45.final_contigs
#Mis_misassemblies               9                                                                                  
#Mis_relocations                 9                                                                                  
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        6                                                                                  
Mis_Misassembled contigs length  187280                                                                             
#Mis_local misassemblies         43                                                                                 
#mismatches                      343                                                                                
#indels                          1148                                                                               
#Mis_short indels (<= 5 bp)      1138                                                                               
#Mis_long indels (> 5 bp)        10                                                                                 
Indels length                    1627                                                                               
