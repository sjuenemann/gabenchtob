All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K45.final_contigs
#Contigs                                   334                                                                                
#Contigs (>= 0 bp)                         492                                                                                
#Contigs (>= 1000 bp)                      189                                                                                
Largest contig                             158463                                                                             
Total length                               4312009                                                                            
Total length (>= 0 bp)                     4325205                                                                            
Total length (>= 1000 bp)                  4254498                                                                            
Reference length                           4411532                                                                            
N50                                        47655                                                                              
NG50                                       47592                                                                              
N75                                        25270                                                                              
NG75                                       23113                                                                              
L50                                        26                                                                                 
LG50                                       27                                                                                 
L75                                        56                                                                                 
LG75                                       59                                                                                 
#local misassemblies                       43                                                                                 
#misassemblies                             9                                                                                  
#misassembled contigs                      6                                                                                  
Misassembled contigs length                187280                                                                             
Misassemblies inter-contig overlap         3786                                                                               
#unaligned contigs                         30 + 11 part                                                                       
Unaligned contigs length                   8662                                                                               
#ambiguously mapped contigs                25                                                                                 
Extra bases in ambiguously mapped contigs  -18369                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.161                                                                             
Duplication ratio                          1.001                                                                              
#genes                                     3894 + 142 part                                                                    
#predicted genes (unique)                  4464                                                                               
#predicted genes (>= 0 bp)                 4467                                                                               
#predicted genes (>= 300 bp)               3737                                                                               
#predicted genes (>= 1500 bp)              495                                                                                
#predicted genes (>= 3000 bp)              50                                                                                 
#mismatches                                343                                                                                
#indels                                    1148                                                                               
Indels length                              1627                                                                               
#mismatches per 100 kbp                    8.00                                                                               
#indels per 100 kbp                        26.78                                                                              
GC (%)                                     65.43                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.679                                                                             
Largest alignment                          158463                                                                             
NA50                                       47592                                                                              
NGA50                                      47301                                                                              
NA75                                       25270                                                                              
NGA75                                      23113                                                                              
LA50                                       27                                                                                 
LGA50                                      28                                                                                 
LA75                                       57                                                                                 
LGA75                                      60                                                                                 
#Mis_misassemblies                         9                                                                                  
#Mis_relocations                           9                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  6                                                                                  
Mis_Misassembled contigs length            187280                                                                             
#Mis_local misassemblies                   43                                                                                 
#Mis_short indels (<= 5 bp)                1138                                                                               
#Mis_long indels (> 5 bp)                  10                                                                                 
#Una_fully unaligned contigs               30                                                                                 
Una_Fully unaligned length                 8100                                                                               
#Una_partially unaligned contigs           11                                                                                 
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             562                                                                                
GAGE_Contigs #                             334                                                                                
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            158463                                                                             
GAGE_N50                                   47592 COUNT: 27                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4312009                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               57866(1.31%)                                                                       
GAGE_Missing assembly bases                8209(0.19%)                                                                        
GAGE_Missing assembly contigs              28(8.38%)                                                                          
GAGE_Duplicated reference bases            450                                                                                
GAGE_Compressed reference bases            55027                                                                              
GAGE_Bad trim                              558                                                                                
GAGE_Avg idy                               99.96                                                                              
GAGE_SNPs                                  284                                                                                
GAGE_Indels < 5bp                          1186                                                                               
GAGE_Indels >= 5                           37                                                                                 
GAGE_Inversions                            4                                                                                  
GAGE_Relocation                            18                                                                                 
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    356                                                                                
GAGE_Corrected assembly size               4307097                                                                            
GAGE_Min correct contig                    203                                                                                
GAGE_Max correct contig                    125426                                                                             
GAGE_Corrected N50                         35391 COUNT: 37                                                                    
