All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K43.final_contigs
GAGE_Contigs #                   342                                                                                        
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  148052                                                                                     
GAGE_N50                         39045 COUNT: 21                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2792469                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     27452(0.98%)                                                                               
GAGE_Missing assembly bases      38739(1.39%)                                                                               
GAGE_Missing assembly contigs    135(39.47%)                                                                                
GAGE_Duplicated reference bases  2086                                                                                       
GAGE_Compressed reference bases  37056                                                                                      
GAGE_Bad trim                    37                                                                                         
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        64                                                                                         
GAGE_Indels < 5bp                167                                                                                        
GAGE_Indels >= 5                 12                                                                                         
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  7                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          216                                                                                        
GAGE_Corrected assembly size     2754538                                                                                    
GAGE_Min correct contig          202                                                                                        
GAGE_Max correct contig          148055                                                                                     
GAGE_Corrected N50               35410 COUNT: 23                                                                            
