All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_137.final.contigs
GAGE_Contigs #                   14260                                                                                  
GAGE_Min contig                  273                                                                                    
GAGE_Max contig                  1062                                                                                   
GAGE_N50                         330 COUNT: 3740                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               4474921                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     744341(26.45%)                                                                         
GAGE_Missing assembly bases      3017(0.07%)                                                                            
GAGE_Missing assembly contigs    4(0.03%)                                                                               
GAGE_Duplicated reference bases  1891866                                                                                
GAGE_Compressed reference bases  51358                                                                                  
GAGE_Bad trim                    1544                                                                                   
GAGE_Avg idy                     99.22                                                                                  
GAGE_SNPs                        1097                                                                                   
GAGE_Indels < 5bp                12813                                                                                  
GAGE_Indels >= 5                 1                                                                                      
GAGE_Inversions                  21                                                                                     
GAGE_Relocation                  13                                                                                     
GAGE_Translocation               3                                                                                      
GAGE_Corrected contig #          8061                                                                                   
GAGE_Corrected assembly size     2553595                                                                                
GAGE_Min correct contig          200                                                                                    
GAGE_Max correct contig          1060                                                                                   
GAGE_Corrected N50               301 COUNT: 3990                                                                        
