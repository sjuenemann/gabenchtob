All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_111.final.contigs
#Contigs                                   3073                                                                                  
#Contigs (>= 0 bp)                         3073                                                                                  
#Contigs (>= 1000 bp)                      1716                                                                                  
Largest contig                             13578                                                                                 
Total length                               5235578                                                                               
Total length (>= 0 bp)                     5235578                                                                               
Total length (>= 1000 bp)                  4507515                                                                               
Reference length                           5594470                                                                               
N50                                        2685                                                                                  
NG50                                       2479                                                                                  
N75                                        1511                                                                                  
NG75                                       1255                                                                                  
L50                                        599                                                                                   
LG50                                       669                                                                                   
L75                                        1244                                                                                  
LG75                                       1439                                                                                  
#local misassemblies                       174                                                                                   
#misassemblies                             137                                                                                   
#misassembled contigs                      132                                                                                   
Misassembled contigs length                475434                                                                                
Misassemblies inter-contig overlap         354                                                                                   
#unaligned contigs                         0 + 12 part                                                                           
Unaligned contigs length                   586                                                                                   
#ambiguously mapped contigs                293                                                                                   
Extra bases in ambiguously mapped contigs  -95046                                                                                
#N's                                       2940                                                                                  
#N's per 100 kbp                           56.15                                                                                 
Genome fraction (%)                        88.645                                                                                
Duplication ratio                          1.037                                                                                 
#genes                                     2994 + 2017 part                                                                      
#predicted genes (unique)                  7768                                                                                  
#predicted genes (>= 0 bp)                 7781                                                                                  
#predicted genes (>= 300 bp)               5314                                                                                  
#predicted genes (>= 1500 bp)              302                                                                                   
#predicted genes (>= 3000 bp)              15                                                                                    
#mismatches                                125                                                                                   
#indels                                    1840                                                                                  
Indels length                              1940                                                                                  
#mismatches per 100 kbp                    2.52                                                                                  
#indels per 100 kbp                        37.10                                                                                 
GC (%)                                     50.60                                                                                 
Reference GC (%)                           50.48                                                                                 
Average %IDY                               99.370                                                                                
Largest alignment                          13568                                                                                 
NA50                                       2620                                                                                  
NGA50                                      2414                                                                                  
NA75                                       1464                                                                                  
NGA75                                      1210                                                                                  
LA50                                       616                                                                                   
LGA50                                      688                                                                                   
LA75                                       1280                                                                                  
LGA75                                      1483                                                                                  
#Mis_misassemblies                         137                                                                                   
#Mis_relocations                           127                                                                                   
#Mis_translocations                        0                                                                                     
#Mis_inversions                            10                                                                                    
#Mis_misassembled contigs                  132                                                                                   
Mis_Misassembled contigs length            475434                                                                                
#Mis_local misassemblies                   174                                                                                   
#Mis_short indels (<= 5 bp)                1839                                                                                  
#Mis_long indels (> 5 bp)                  1                                                                                     
#Una_fully unaligned contigs               0                                                                                     
Una_Fully unaligned length                 0                                                                                     
#Una_partially unaligned contigs           12                                                                                    
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             586                                                                                   
GAGE_Contigs #                             3073                                                                                  
GAGE_Min contig                            221                                                                                   
GAGE_Max contig                            13578                                                                                 
GAGE_N50                                   2479 COUNT: 669                                                                       
GAGE_Genome size                           5594470                                                                               
GAGE_Assembly size                         5235578                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               376834(6.74%)                                                                         
GAGE_Missing assembly bases                3574(0.07%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                              
GAGE_Duplicated reference bases            45208                                                                                 
GAGE_Compressed reference bases            251809                                                                                
GAGE_Bad trim                              1974                                                                                  
GAGE_Avg idy                               99.96                                                                                 
GAGE_SNPs                                  60                                                                                    
GAGE_Indels < 5bp                          1607                                                                                  
GAGE_Indels >= 5                           122                                                                                   
GAGE_Inversions                            1                                                                                     
GAGE_Relocation                            44                                                                                    
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    3158                                                                                  
GAGE_Corrected assembly size               5188444                                                                               
GAGE_Min correct contig                    202                                                                                   
GAGE_Max correct contig                    11709                                                                                 
GAGE_Corrected N50                         2281 COUNT: 733                                                                       
