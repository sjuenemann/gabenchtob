All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K53.final_contigs
#Contigs                                   508                                                                                        
#Contigs (>= 0 bp)                         1113                                                                                       
#Contigs (>= 1000 bp)                      93                                                                                         
Largest contig                             148072                                                                                     
Total length                               2847702                                                                                    
Total length (>= 0 bp)                     2898028                                                                                    
Total length (>= 1000 bp)                  2724994                                                                                    
Reference length                           2813862                                                                                    
N50                                        48310                                                                                      
NG50                                       48821                                                                                      
N75                                        30265                                                                                      
NG75                                       30843                                                                                      
L50                                        16                                                                                         
LG50                                       15                                                                                         
L75                                        35                                                                                         
LG75                                       34                                                                                         
#local misassemblies                       14                                                                                         
#misassemblies                             3                                                                                          
#misassembled contigs                      3                                                                                          
Misassembled contigs length                64201                                                                                      
Misassemblies inter-contig overlap         2241                                                                                       
#unaligned contigs                         328 + 0 part                                                                               
Unaligned contigs length                   89282                                                                                      
#ambiguously mapped contigs                13                                                                                         
Extra bases in ambiguously mapped contigs  -9591                                                                                      
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        97.652                                                                                     
Duplication ratio                          1.001                                                                                      
#genes                                     2578 + 76 part                                                                             
#predicted genes (unique)                  2917                                                                                       
#predicted genes (>= 0 bp)                 2919                                                                                       
#predicted genes (>= 300 bp)               2307                                                                                       
#predicted genes (>= 1500 bp)              289                                                                                        
#predicted genes (>= 3000 bp)              26                                                                                         
#mismatches                                159                                                                                        
#indels                                    191                                                                                        
Indels length                              257                                                                                        
#mismatches per 100 kbp                    5.79                                                                                       
#indels per 100 kbp                        6.95                                                                                       
GC (%)                                     32.58                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.762                                                                                     
Largest alignment                          148072                                                                                     
NA50                                       48310                                                                                      
NGA50                                      48793                                                                                      
NA75                                       28247                                                                                      
NGA75                                      30843                                                                                      
LA50                                       16                                                                                         
LGA50                                      15                                                                                         
LA75                                       35                                                                                         
LGA75                                      34                                                                                         
#Mis_misassemblies                         3                                                                                          
#Mis_relocations                           3                                                                                          
#Mis_translocations                        0                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  3                                                                                          
Mis_Misassembled contigs length            64201                                                                                      
#Mis_local misassemblies                   14                                                                                         
#Mis_short indels (<= 5 bp)                189                                                                                        
#Mis_long indels (> 5 bp)                  2                                                                                          
#Una_fully unaligned contigs               328                                                                                        
Una_Fully unaligned length                 89282                                                                                      
#Una_partially unaligned contigs           0                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             0                                                                                          
GAGE_Contigs #                             508                                                                                        
GAGE_Min contig                            202                                                                                        
GAGE_Max contig                            148072                                                                                     
GAGE_N50                                   48821 COUNT: 15                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         2847702                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               19446(0.69%)                                                                               
GAGE_Missing assembly bases                69957(2.46%)                                                                               
GAGE_Missing assembly contigs              243(47.83%)                                                                                
GAGE_Duplicated reference bases            18599                                                                                      
GAGE_Compressed reference bases            40557                                                                                      
GAGE_Bad trim                              1453                                                                                       
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  40                                                                                         
GAGE_Indels < 5bp                          194                                                                                        
GAGE_Indels >= 5                           10                                                                                         
GAGE_Inversions                            0                                                                                          
GAGE_Relocation                            7                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    200                                                                                        
GAGE_Corrected assembly size               2761674                                                                                    
GAGE_Min correct contig                    202                                                                                        
GAGE_Max correct contig                    148075                                                                                     
GAGE_Corrected N50                         44027 COUNT: 19                                                                            
