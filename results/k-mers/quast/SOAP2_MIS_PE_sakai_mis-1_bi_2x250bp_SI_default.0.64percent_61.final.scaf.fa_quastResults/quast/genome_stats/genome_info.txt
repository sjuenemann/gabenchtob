reference chromosomes:
	gi_10955262_ref_NC_002127.1__Escherichia_coli_O157_H7_str._Sakai_plasmid_pOSAK1__complete_sequence (3306 bp)
	gi_10955266_ref_NC_002128.1__Escherichia_coli_O157_H7_str._Sakai_plasmid_pO157__complete_sequence (92721 bp)
	gi_15829254_ref_NC_002695.1__Escherichia_coli_O157_H7_str._Sakai_chromosome__complete_genome__edited_22_position__s.leopold__and_23_positions (5498443 bp)

total genome size: 5594470

gap min size: 50
partial gene/operon min size: 100

genes loaded: 5460


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_61.final.scaf_broken  | 78.215505669        | 1.05860604702     | 4231        | 624       | 4256      | None      | None      |
  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_61.final.scaf  | 78.2003835931       | 1.05877006933     | 4229        | 624       | 4256      | None      | None      |
