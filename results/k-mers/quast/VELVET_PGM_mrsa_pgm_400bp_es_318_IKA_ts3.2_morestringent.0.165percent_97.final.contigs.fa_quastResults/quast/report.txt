All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_97.final.contigs
#Contigs (>= 0 bp)             45920                                                                                 
#Contigs (>= 1000 bp)          0                                                                                     
Total length (>= 0 bp)         10943426                                                                              
Total length (>= 1000 bp)      0                                                                                     
#Contigs                       40061                                                                                 
Largest contig                 618                                                                                   
Total length                   9796567                                                                               
Reference length               2813862                                                                               
GC (%)                         32.62                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            240                                                                                   
NG50                           299                                                                                   
N75                            216                                                                                   
NG75                           281                                                                                   
#misassemblies                 503                                                                                   
#local misassemblies           3                                                                                     
#unaligned contigs             559 + 120 part                                                                        
Unaligned contigs length       150160                                                                                
Genome fraction (%)            93.595                                                                                
Duplication ratio              3.618                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        74.23                                                                                 
#indels per 100 kbp            1192.00                                                                               
#genes                         204 + 2450 part                                                                       
#predicted genes (unique)      38176                                                                                 
#predicted genes (>= 0 bp)     38406                                                                                 
#predicted genes (>= 300 bp)   41                                                                                    
#predicted genes (>= 1500 bp)  0                                                                                     
#predicted genes (>= 3000 bp)  0                                                                                     
Largest alignment              618                                                                                   
NA50                           238                                                                                   
NGA50                          297                                                                                   
NA75                           214                                                                                   
NGA75                          279                                                                                   
