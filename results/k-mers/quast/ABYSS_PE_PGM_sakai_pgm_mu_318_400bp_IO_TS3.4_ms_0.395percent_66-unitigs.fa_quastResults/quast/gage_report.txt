All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_66-unitigs
GAGE_Contigs #                   1192                                                                   
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  49745                                                                  
GAGE_N50                         10938 COUNT: 149                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5264413                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     215053(3.84%)                                                          
GAGE_Missing assembly bases      7(0.00%)                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  3804                                                                   
GAGE_Compressed reference bases  146893                                                                 
GAGE_Bad trim                    7                                                                      
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        91                                                                     
GAGE_Indels < 5bp                235                                                                    
GAGE_Indels >= 5                 4                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  6                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          1185                                                                   
GAGE_Corrected assembly size     5261180                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          49745                                                                  
GAGE_Corrected N50               10823 COUNT: 150                                                       
