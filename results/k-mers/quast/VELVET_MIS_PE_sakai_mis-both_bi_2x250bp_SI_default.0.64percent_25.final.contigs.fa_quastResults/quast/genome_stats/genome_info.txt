reference chromosomes:
	gi_10955262_ref_NC_002127.1__Escherichia_coli_O157_H7_str._Sakai_plasmid_pOSAK1__complete_sequence (3306 bp)
	gi_10955266_ref_NC_002128.1__Escherichia_coli_O157_H7_str._Sakai_plasmid_pO157__complete_sequence (92721 bp)
	gi_15829254_ref_NC_002695.1__Escherichia_coli_O157_H7_str._Sakai_chromosome__complete_genome__edited_22_position__s.leopold__and_23_positions (5498443 bp)

total genome size: 5594470

gap min size: 50
partial gene/operon min size: 100

genes loaded: 5460


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_25.final.contigs_broken  | 91.2441214271       | 1.00129803854     | 357         | 4534      | 335       | None      | None      |
  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_25.final.contigs  | 91.2355951502       | 1.00136183355     | 359         | 4534      | 335       | None      | None      |
