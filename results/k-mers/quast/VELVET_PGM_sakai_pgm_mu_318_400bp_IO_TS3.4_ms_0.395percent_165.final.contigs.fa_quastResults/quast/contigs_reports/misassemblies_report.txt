All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_165.final.contigs
#Mis_misassemblies               101                                                                         
#Mis_relocations                 92                                                                          
#Mis_translocations              9                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        101                                                                         
Mis_Misassembled contigs length  114522                                                                      
#Mis_local misassemblies         2                                                                           
#mismatches                      47                                                                          
#indels                          670                                                                         
#Mis_short indels (<= 5 bp)      670                                                                         
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    679                                                                         
