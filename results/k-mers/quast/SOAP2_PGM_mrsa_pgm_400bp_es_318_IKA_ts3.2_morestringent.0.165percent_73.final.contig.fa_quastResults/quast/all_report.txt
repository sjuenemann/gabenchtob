All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_73.final.contig
#Contigs                                   26874                                                                               
#Contigs (>= 0 bp)                         183071                                                                              
#Contigs (>= 1000 bp)                      0                                                                                   
Largest contig                             659                                                                                 
Total length                               6952399                                                                             
Total length (>= 0 bp)                     23817479                                                                            
Total length (>= 1000 bp)                  0                                                                                   
Reference length                           2813862                                                                             
N50                                        253                                                                                 
NG50                                       291                                                                                 
N75                                        234                                                                                 
NG75                                       275                                                                                 
L50                                        12014                                                                               
LG50                                       4330                                                                                
L75                                        19159                                                                               
LG75                                       6824                                                                                
#local misassemblies                       1                                                                                   
#misassemblies                             138                                                                                 
#misassembled contigs                      138                                                                                 
Misassembled contigs length                35774                                                                               
Misassemblies inter-contig overlap         350                                                                                 
#unaligned contigs                         5741 + 556 part                                                                     
Unaligned contigs length                   1562764                                                                             
#ambiguously mapped contigs                88                                                                                  
Extra bases in ambiguously mapped contigs  -21831                                                                              
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        84.874                                                                              
Duplication ratio                          2.248                                                                               
#genes                                     175 + 2445 part                                                                     
#predicted genes (unique)                  19397                                                                               
#predicted genes (>= 0 bp)                 19445                                                                               
#predicted genes (>= 300 bp)               126                                                                                 
#predicted genes (>= 1500 bp)              0                                                                                   
#predicted genes (>= 3000 bp)              0                                                                                   
#mismatches                                3941                                                                                
#indels                                    54792                                                                               
Indels length                              56327                                                                               
#mismatches per 100 kbp                    165.02                                                                              
#indels per 100 kbp                        2294.24                                                                             
GC (%)                                     31.69                                                                               
Reference GC (%)                           32.81                                                                               
Average %IDY                               97.421                                                                              
Largest alignment                          653                                                                                 
NA50                                       239                                                                                 
NGA50                                      277                                                                                 
NA75                                       211                                                                                 
NGA75                                      261                                                                                 
LA50                                       12645                                                                               
LGA50                                      4546                                                                                
LA75                                       20263                                                                               
LGA75                                      7167                                                                                
#Mis_misassemblies                         138                                                                                 
#Mis_relocations                           129                                                                                 
#Mis_translocations                        6                                                                                   
#Mis_inversions                            3                                                                                   
#Mis_misassembled contigs                  138                                                                                 
Mis_Misassembled contigs length            35774                                                                               
#Mis_local misassemblies                   1                                                                                   
#Mis_short indels (<= 5 bp)                54792                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                   
#Una_fully unaligned contigs               5741                                                                                
Una_Fully unaligned length                 1519867                                                                             
#Una_partially unaligned contigs           556                                                                                 
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             42897                                                                               
GAGE_Contigs #                             26874                                                                               
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            659                                                                                 
GAGE_N50                                   291 COUNT: 4330                                                                     
GAGE_Genome size                           2813862                                                                             
GAGE_Assembly size                         6952399                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               303421(10.78%)                                                                      
GAGE_Missing assembly bases                796039(11.45%)                                                                      
GAGE_Missing assembly contigs              2295(8.54%)                                                                         
GAGE_Duplicated reference bases            2921693                                                                             
GAGE_Compressed reference bases            46654                                                                               
GAGE_Bad trim                              179811                                                                              
GAGE_Avg idy                               97.59                                                                               
GAGE_SNPs                                  2867                                                                                
GAGE_Indels < 5bp                          43106                                                                               
GAGE_Indels >= 5                           9                                                                                   
GAGE_Inversions                            18                                                                                  
GAGE_Relocation                            9                                                                                   
GAGE_Translocation                         0                                                                                   
GAGE_Corrected contig #                    12277                                                                               
GAGE_Corrected assembly size               3139344                                                                             
GAGE_Min correct contig                    200                                                                                 
GAGE_Max correct contig                    653                                                                                 
GAGE_Corrected N50                         256 COUNT: 4844                                                                     
