All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_31.final.contig
GAGE_Contigs #                   702                                                                      
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  606                                                                      
GAGE_N50                         0 COUNT: 0                                                               
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               170866                                                                   
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     5433397(97.12%)                                                          
GAGE_Missing assembly bases      9974(5.84%)                                                              
GAGE_Missing assembly contigs    35(4.99%)                                                                
GAGE_Duplicated reference bases  0                                                                        
GAGE_Compressed reference bases  204                                                                      
GAGE_Bad trim                    2                                                                        
GAGE_Avg idy                     99.97                                                                    
GAGE_SNPs                        0                                                                        
GAGE_Indels < 5bp                43                                                                       
GAGE_Indels >= 5                 0                                                                        
GAGE_Inversions                  0                                                                        
GAGE_Relocation                  0                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          667                                                                      
GAGE_Corrected assembly size     160877                                                                   
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          605                                                                      
GAGE_Corrected N50               0 COUNT: 0                                                               
