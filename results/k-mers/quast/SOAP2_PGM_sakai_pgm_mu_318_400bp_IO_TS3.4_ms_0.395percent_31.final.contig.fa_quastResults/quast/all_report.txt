All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_31.final.contig
#Contigs                                   702                                                                      
#Contigs (>= 0 bp)                         500883                                                                   
#Contigs (>= 1000 bp)                      0                                                                        
Largest contig                             606                                                                      
Total length                               170866                                                                   
Total length (>= 0 bp)                     25862929                                                                 
Total length (>= 1000 bp)                  0                                                                        
Reference length                           5594470                                                                  
N50                                        234                                                                      
NG50                                       None                                                                     
N75                                        214                                                                      
NG75                                       None                                                                     
L50                                        304                                                                      
LG50                                       None                                                                     
L75                                        495                                                                      
LG75                                       None                                                                     
#local misassemblies                       0                                                                        
#misassemblies                             0                                                                        
#misassembled contigs                      0                                                                        
Misassembled contigs length                0                                                                        
Misassemblies inter-contig overlap         0                                                                        
#unaligned contigs                         35 + 0 part                                                              
Unaligned contigs length                   9972                                                                     
#ambiguously mapped contigs                1                                                                        
Extra bases in ambiguously mapped contigs  -204                                                                     
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        2.872                                                                    
Duplication ratio                          1.000                                                                    
#genes                                     4 + 570 part                                                             
#predicted genes (unique)                  701                                                                      
#predicted genes (>= 0 bp)                 701                                                                      
#predicted genes (>= 300 bp)               44                                                                       
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                0                                                                        
#indels                                    42                                                                       
Indels length                              43                                                                       
#mismatches per 100 kbp                    0.00                                                                     
#indels per 100 kbp                        26.14                                                                    
GC (%)                                     52.90                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.973                                                                   
Largest alignment                          606                                                                      
NA50                                       230                                                                      
NGA50                                      None                                                                     
NA75                                       210                                                                      
NGA75                                      None                                                                     
LA50                                       313                                                                      
LGA50                                      None                                                                     
LA75                                       507                                                                      
LGA75                                      None                                                                     
#Mis_misassemblies                         0                                                                        
#Mis_relocations                           0                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  0                                                                        
Mis_Misassembled contigs length            0                                                                        
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                42                                                                       
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               35                                                                       
Una_Fully unaligned length                 9972                                                                     
#Una_partially unaligned contigs           0                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             0                                                                        
GAGE_Contigs #                             702                                                                      
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            606                                                                      
GAGE_N50                                   0 COUNT: 0                                                               
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         170866                                                                   
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               5433397(97.12%)                                                          
GAGE_Missing assembly bases                9974(5.84%)                                                              
GAGE_Missing assembly contigs              35(4.99%)                                                                
GAGE_Duplicated reference bases            0                                                                        
GAGE_Compressed reference bases            204                                                                      
GAGE_Bad trim                              2                                                                        
GAGE_Avg idy                               99.97                                                                    
GAGE_SNPs                                  0                                                                        
GAGE_Indels < 5bp                          43                                                                       
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            0                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    667                                                                      
GAGE_Corrected assembly size               160877                                                                   
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    605                                                                      
GAGE_Corrected N50                         0 COUNT: 0                                                               
