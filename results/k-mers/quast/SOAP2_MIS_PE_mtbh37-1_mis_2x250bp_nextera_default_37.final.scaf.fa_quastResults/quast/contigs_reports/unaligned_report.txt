All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_37.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_37.final.scaf
#Una_fully unaligned contigs      41229                                                                   41228                                                          
Una_Fully unaligned length        9528467                                                                 9528180                                                        
#Una_partially unaligned contigs  8                                                                       9                                                              
#Una_with misassembly             0                                                                       0                                                              
#Una_both parts are significant   0                                                                       0                                                              
Una_Partially unaligned length    818                                                                     1201                                                           
#N's                              0                                                                       96                                                             
