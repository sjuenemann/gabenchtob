All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_51.final.contigs
#Contigs (>= 0 bp)             1894                                                                                 
#Contigs (>= 1000 bp)          823                                                                                  
Total length (>= 0 bp)         5360220                                                                              
Total length (>= 1000 bp)      5019115                                                                              
#Contigs                       1422                                                                                 
Largest contig                 40918                                                                                
Total length                   5291507                                                                              
Reference length               5594470                                                                              
GC (%)                         50.26                                                                                
Reference GC (%)               50.48                                                                                
N50                            8857                                                                                 
NG50                           8321                                                                                 
N75                            4792                                                                                 
NG75                           3887                                                                                 
#misassemblies                 4                                                                                    
#local misassemblies           10                                                                                   
#unaligned contigs             1 + 0 part                                                                           
Unaligned contigs length       827                                                                                  
Genome fraction (%)            92.880                                                                               
Duplication ratio              1.003                                                                                
#N's per 100 kbp               0.19                                                                                 
#mismatches per 100 kbp        4.12                                                                                 
#indels per 100 kbp            47.84                                                                                
#genes                         4271 + 749 part                                                                      
#predicted genes (unique)      6737                                                                                 
#predicted genes (>= 0 bp)     6740                                                                                 
#predicted genes (>= 300 bp)   4998                                                                                 
#predicted genes (>= 1500 bp)  423                                                                                  
#predicted genes (>= 3000 bp)  23                                                                                   
Largest alignment              40918                                                                                
NA50                           8754                                                                                 
NGA50                          8321                                                                                 
NA75                           4782                                                                                 
NGA75                          3875                                                                                 
