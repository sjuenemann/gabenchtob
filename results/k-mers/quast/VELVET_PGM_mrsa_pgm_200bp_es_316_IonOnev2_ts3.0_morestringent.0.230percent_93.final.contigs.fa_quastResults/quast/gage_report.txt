All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_93.final.contigs
GAGE_Contigs #                   1144                                                                                       
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  21939                                                                                      
GAGE_N50                         3554 COUNT: 238                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2650788                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     161838(5.75%)                                                                              
GAGE_Missing assembly bases      969(0.04%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                   
GAGE_Duplicated reference bases  8061                                                                                       
GAGE_Compressed reference bases  41091                                                                                      
GAGE_Bad trim                    838                                                                                        
GAGE_Avg idy                     99.96                                                                                      
GAGE_SNPs                        64                                                                                         
GAGE_Indels < 5bp                808                                                                                        
GAGE_Indels >= 5                 12                                                                                         
GAGE_Inversions                  11                                                                                         
GAGE_Relocation                  5                                                                                          
GAGE_Translocation               2                                                                                          
GAGE_Corrected contig #          1161                                                                                       
GAGE_Corrected assembly size     2643865                                                                                    
GAGE_Min correct contig          200                                                                                        
GAGE_Max correct contig          21940                                                                                      
GAGE_Corrected N50               3468 COUNT: 244                                                                            
