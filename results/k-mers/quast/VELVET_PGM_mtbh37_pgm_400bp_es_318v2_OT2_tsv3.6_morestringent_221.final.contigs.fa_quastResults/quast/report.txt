All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_221.final.contigs
#Contigs (>= 0 bp)             1926                                                                           
#Contigs (>= 1000 bp)          1256                                                                           
Total length (>= 0 bp)         2815757                                                                        
Total length (>= 1000 bp)      2266744                                                                        
#Contigs                       1926                                                                           
Largest contig                 7119                                                                           
Total length                   2815757                                                                        
Reference length               4411532                                                                        
GC (%)                         64.77                                                                          
Reference GC (%)               65.61                                                                          
N50                            1610                                                                           
NG50                           1037                                                                           
N75                            1095                                                                           
NG75                           None                                                                           
#misassemblies                 12                                                                             
#local misassemblies           5                                                                              
#unaligned contigs             1 + 2 part                                                                     
Unaligned contigs length       1601                                                                           
Genome fraction (%)            62.097                                                                         
Duplication ratio              1.028                                                                          
#N's per 100 kbp               4.97                                                                           
#mismatches per 100 kbp        2.66                                                                           
#indels per 100 kbp            69.50                                                                          
#genes                         1240 + 1988 part                                                               
#predicted genes (unique)      4372                                                                           
#predicted genes (>= 0 bp)     4387                                                                           
#predicted genes (>= 300 bp)   2901                                                                           
#predicted genes (>= 1500 bp)  110                                                                            
#predicted genes (>= 3000 bp)  0                                                                              
Largest alignment              7119                                                                           
NA50                           1598                                                                           
NGA50                          1032                                                                           
NA75                           1091                                                                           
NGA75                          None                                                                           
