All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_221.final.contigs
#Contigs                                   1926                                                                           
#Contigs (>= 0 bp)                         1926                                                                           
#Contigs (>= 1000 bp)                      1256                                                                           
Largest contig                             7119                                                                           
Total length                               2815757                                                                        
Total length (>= 0 bp)                     2815757                                                                        
Total length (>= 1000 bp)                  2266744                                                                        
Reference length                           4411532                                                                        
N50                                        1610                                                                           
NG50                                       1037                                                                           
N75                                        1095                                                                           
NG75                                       None                                                                           
L50                                        575                                                                            
LG50                                       1197                                                                           
L75                                        1109                                                                           
LG75                                       None                                                                           
#local misassemblies                       5                                                                              
#misassemblies                             12                                                                             
#misassembled contigs                      12                                                                             
Misassembled contigs length                36531                                                                          
Misassemblies inter-contig overlap         1936                                                                           
#unaligned contigs                         1 + 2 part                                                                     
Unaligned contigs length                   1601                                                                           
#ambiguously mapped contigs                1                                                                              
Extra bases in ambiguously mapped contigs  -731                                                                           
#N's                                       140                                                                            
#N's per 100 kbp                           4.97                                                                           
Genome fraction (%)                        62.097                                                                         
Duplication ratio                          1.028                                                                          
#genes                                     1240 + 1988 part                                                               
#predicted genes (unique)                  4372                                                                           
#predicted genes (>= 0 bp)                 4387                                                                           
#predicted genes (>= 300 bp)               2901                                                                           
#predicted genes (>= 1500 bp)              110                                                                            
#predicted genes (>= 3000 bp)              0                                                                              
#mismatches                                73                                                                             
#indels                                    1904                                                                           
Indels length                              1991                                                                           
#mismatches per 100 kbp                    2.66                                                                           
#indels per 100 kbp                        69.50                                                                          
GC (%)                                     64.77                                                                          
Reference GC (%)                           65.61                                                                          
Average %IDY                               99.880                                                                         
Largest alignment                          7119                                                                           
NA50                                       1598                                                                           
NGA50                                      1032                                                                           
NA75                                       1091                                                                           
NGA75                                      None                                                                           
LA50                                       579                                                                            
LGA50                                      1203                                                                           
LA75                                       1115                                                                           
LGA75                                      None                                                                           
#Mis_misassemblies                         12                                                                             
#Mis_relocations                           12                                                                             
#Mis_translocations                        0                                                                              
#Mis_inversions                            0                                                                              
#Mis_misassembled contigs                  12                                                                             
Mis_Misassembled contigs length            36531                                                                          
#Mis_local misassemblies                   5                                                                              
#Mis_short indels (<= 5 bp)                1902                                                                           
#Mis_long indels (> 5 bp)                  2                                                                              
#Una_fully unaligned contigs               1                                                                              
Una_Fully unaligned length                 696                                                                            
#Una_partially unaligned contigs           2                                                                              
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             905                                                                            
GAGE_Contigs #                             1926                                                                           
GAGE_Min contig                            441                                                                            
GAGE_Max contig                            7119                                                                           
GAGE_N50                                   1037 COUNT: 1197                                                               
GAGE_Genome size                           4411532                                                                        
GAGE_Assembly size                         2815757                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               1655287(37.52%)                                                                
GAGE_Missing assembly bases                1293(0.05%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                       
GAGE_Duplicated reference bases            8139                                                                           
GAGE_Compressed reference bases            23764                                                                          
GAGE_Bad trim                              1253                                                                           
GAGE_Avg idy                               99.92                                                                          
GAGE_SNPs                                  66                                                                             
GAGE_Indels < 5bp                          1817                                                                           
GAGE_Indels >= 5                           2                                                                              
GAGE_Inversions                            0                                                                              
GAGE_Relocation                            4                                                                              
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    1925                                                                           
GAGE_Corrected assembly size               2808369                                                                        
GAGE_Min correct contig                    323                                                                            
GAGE_Max correct contig                    7120                                                                           
GAGE_Corrected N50                         1030 COUNT: 1203                                                               
