All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_221.final.contigs
GAGE_Contigs #                   1926                                                                           
GAGE_Min contig                  441                                                                            
GAGE_Max contig                  7119                                                                           
GAGE_N50                         1037 COUNT: 1197                                                               
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               2815757                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     1655287(37.52%)                                                                
GAGE_Missing assembly bases      1293(0.05%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  8139                                                                           
GAGE_Compressed reference bases  23764                                                                          
GAGE_Bad trim                    1253                                                                           
GAGE_Avg idy                     99.92                                                                          
GAGE_SNPs                        66                                                                             
GAGE_Indels < 5bp                1817                                                                           
GAGE_Indels >= 5                 2                                                                              
GAGE_Inversions                  0                                                                              
GAGE_Relocation                  4                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          1925                                                                           
GAGE_Corrected assembly size     2808369                                                                        
GAGE_Min correct contig          323                                                                            
GAGE_Max correct contig          7120                                                                           
GAGE_Corrected N50               1030 COUNT: 1203                                                               
