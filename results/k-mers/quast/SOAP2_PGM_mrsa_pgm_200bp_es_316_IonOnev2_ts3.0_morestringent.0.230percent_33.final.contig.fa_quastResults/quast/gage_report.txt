All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_33.final.contig
GAGE_Contigs #                   1077                                                                                     
GAGE_Min contig                  200                                                                                      
GAGE_Max contig                  523                                                                                      
GAGE_N50                         0 COUNT: 0                                                                               
GAGE_Genome size                 2813862                                                                                  
GAGE_Assembly size               262972                                                                                   
GAGE_Chaff bases                 0                                                                                        
GAGE_Missing reference bases     2570830(91.36%)                                                                          
GAGE_Missing assembly bases      19619(7.46%)                                                                             
GAGE_Missing assembly contigs    91(8.45%)                                                                                
GAGE_Duplicated reference bases  0                                                                                        
GAGE_Compressed reference bases  0                                                                                        
GAGE_Bad trim                    273                                                                                      
GAGE_Avg idy                     99.91                                                                                    
GAGE_SNPs                        27                                                                                       
GAGE_Indels < 5bp                197                                                                                      
GAGE_Indels >= 5                 0                                                                                        
GAGE_Inversions                  0                                                                                        
GAGE_Relocation                  0                                                                                        
GAGE_Translocation               0                                                                                        
GAGE_Corrected contig #          981                                                                                      
GAGE_Corrected assembly size     242423                                                                                   
GAGE_Min correct contig          200                                                                                      
GAGE_Max correct contig          524                                                                                      
GAGE_Corrected N50               0 COUNT: 0                                                                               
