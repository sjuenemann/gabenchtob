All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_109.final.contigs
#Contigs                                   3134                                                                                  
#Contigs (>= 0 bp)                         3134                                                                                  
#Contigs (>= 1000 bp)                      1732                                                                                  
Largest contig                             15561                                                                                 
Total length                               5255540                                                                               
Total length (>= 0 bp)                     5255540                                                                               
Total length (>= 1000 bp)                  4513016                                                                               
Reference length                           5594470                                                                               
N50                                        2644                                                                                  
NG50                                       2446                                                                                  
N75                                        1516                                                                                  
NG75                                       1281                                                                                  
L50                                        608                                                                                   
LG50                                       675                                                                                   
L75                                        1269                                                                                  
LG75                                       1451                                                                                  
#local misassemblies                       192                                                                                   
#misassemblies                             149                                                                                   
#misassembled contigs                      138                                                                                   
Misassembled contigs length                501844                                                                                
Misassemblies inter-contig overlap         1143                                                                                  
#unaligned contigs                         1 + 11 part                                                                           
Unaligned contigs length                   1521                                                                                  
#ambiguously mapped contigs                297                                                                                   
Extra bases in ambiguously mapped contigs  -96794                                                                                
#N's                                       3170                                                                                  
#N's per 100 kbp                           60.32                                                                                 
Genome fraction (%)                        88.799                                                                                
Duplication ratio                          1.038                                                                                 
#genes                                     2979 + 2028 part                                                                      
#predicted genes (unique)                  7810                                                                                  
#predicted genes (>= 0 bp)                 7825                                                                                  
#predicted genes (>= 300 bp)               5315                                                                                  
#predicted genes (>= 1500 bp)              308                                                                                   
#predicted genes (>= 3000 bp)              12                                                                                    
#mismatches                                183                                                                                   
#indels                                    1787                                                                                  
Indels length                              1855                                                                                  
#mismatches per 100 kbp                    3.68                                                                                  
#indels per 100 kbp                        35.97                                                                                 
GC (%)                                     50.55                                                                                 
Reference GC (%)                           50.48                                                                                 
Average %IDY                               99.378                                                                                
Largest alignment                          15551                                                                                 
NA50                                       2548                                                                                  
NGA50                                      2372                                                                                  
NA75                                       1464                                                                                  
NGA75                                      1221                                                                                  
LA50                                       627                                                                                   
LGA50                                      696                                                                                   
LA75                                       1308                                                                                  
LGA75                                      1497                                                                                  
#Mis_misassemblies                         149                                                                                   
#Mis_relocations                           137                                                                                   
#Mis_translocations                        0                                                                                     
#Mis_inversions                            12                                                                                    
#Mis_misassembled contigs                  138                                                                                   
Mis_Misassembled contigs length            501844                                                                                
#Mis_local misassemblies                   192                                                                                   
#Mis_short indels (<= 5 bp)                1786                                                                                  
#Mis_long indels (> 5 bp)                  1                                                                                     
#Una_fully unaligned contigs               1                                                                                     
Una_Fully unaligned length                 1108                                                                                  
#Una_partially unaligned contigs           11                                                                                    
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             413                                                                                   
GAGE_Contigs #                             3134                                                                                  
GAGE_Min contig                            217                                                                                   
GAGE_Max contig                            15561                                                                                 
GAGE_N50                                   2446 COUNT: 675                                                                       
GAGE_Genome size                           5594470                                                                               
GAGE_Assembly size                         5255540                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               364471(6.51%)                                                                         
GAGE_Missing assembly bases                3630(0.07%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                              
GAGE_Duplicated reference bases            48564                                                                                 
GAGE_Compressed reference bases            248047                                                                                
GAGE_Bad trim                              1800                                                                                  
GAGE_Avg idy                               99.95                                                                                 
GAGE_SNPs                                  90                                                                                    
GAGE_Indels < 5bp                          1591                                                                                  
GAGE_Indels >= 5                           150                                                                                   
GAGE_Inversions                            4                                                                                     
GAGE_Relocation                            38                                                                                    
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    3238                                                                                  
GAGE_Corrected assembly size               5205887                                                                               
GAGE_Min correct contig                    202                                                                                   
GAGE_Max correct contig                    13350                                                                                 
GAGE_Corrected N50                         2228 COUNT: 752                                                                       
