All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent.K55.final_contigs
#Contigs                                   179                                                                                    
#Contigs (>= 0 bp)                         740                                                                                    
#Contigs (>= 1000 bp)                      93                                                                                     
Largest contig                             148076                                                                                 
Total length                               2760886                                                                                
Total length (>= 0 bp)                     2808880                                                                                
Total length (>= 1000 bp)                  2727053                                                                                
Reference length                           2813862                                                                                
N50                                        51312                                                                                  
NG50                                       51312                                                                                  
N75                                        30883                                                                                  
NG75                                       30847                                                                                  
L50                                        15                                                                                     
LG50                                       15                                                                                     
L75                                        33                                                                                     
LG75                                       34                                                                                     
#local misassemblies                       12                                                                                     
#misassemblies                             2                                                                                      
#misassembled contigs                      2                                                                                      
Misassembled contigs length                34247                                                                                  
Misassemblies inter-contig overlap         2263                                                                                   
#unaligned contigs                         2 + 0 part                                                                             
Unaligned contigs length                   756                                                                                    
#ambiguously mapped contigs                13                                                                                     
Extra bases in ambiguously mapped contigs  -9539                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        97.705                                                                                 
Duplication ratio                          1.001                                                                                  
#genes                                     2584 + 71 part                                                                         
#predicted genes (unique)                  2714                                                                                   
#predicted genes (>= 0 bp)                 2715                                                                                   
#predicted genes (>= 300 bp)               2292                                                                                   
#predicted genes (>= 1500 bp)              290                                                                                    
#predicted genes (>= 3000 bp)              26                                                                                     
#mismatches                                168                                                                                    
#indels                                    185                                                                                    
Indels length                              196                                                                                    
#mismatches per 100 kbp                    6.11                                                                                   
#indels per 100 kbp                        6.73                                                                                   
GC (%)                                     32.64                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.767                                                                                 
Largest alignment                          148076                                                                                 
NA50                                       51312                                                                                  
NGA50                                      51312                                                                                  
NA75                                       30883                                                                                  
NGA75                                      30847                                                                                  
LA50                                       15                                                                                     
LGA50                                      15                                                                                     
LA75                                       33                                                                                     
LGA75                                      34                                                                                     
#Mis_misassemblies                         2                                                                                      
#Mis_relocations                           2                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  2                                                                                      
Mis_Misassembled contigs length            34247                                                                                  
#Mis_local misassemblies                   12                                                                                     
#Mis_short indels (<= 5 bp)                184                                                                                    
#Mis_long indels (> 5 bp)                  1                                                                                      
#Una_fully unaligned contigs               2                                                                                      
Una_Fully unaligned length                 756                                                                                    
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             179                                                                                    
GAGE_Min contig                            200                                                                                    
GAGE_Max contig                            148076                                                                                 
GAGE_N50                                   51312 COUNT: 15                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2760886                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               18611(0.66%)                                                                           
GAGE_Missing assembly bases                482(0.02%)                                                                             
GAGE_Missing assembly contigs              1(0.56%)                                                                               
GAGE_Duplicated reference bases            0                                                                                      
GAGE_Compressed reference bases            40720                                                                                  
GAGE_Bad trim                              210                                                                                    
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  40                                                                                     
GAGE_Indels < 5bp                          179                                                                                    
GAGE_Indels >= 5                           9                                                                                      
GAGE_Inversions                            1                                                                                      
GAGE_Relocation                            6                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    193                                                                                    
GAGE_Corrected assembly size               2762849                                                                                
GAGE_Min correct contig                    200                                                                                    
GAGE_Max correct contig                    148079                                                                                 
GAGE_Corrected N50                         44027 COUNT: 19                                                                        
