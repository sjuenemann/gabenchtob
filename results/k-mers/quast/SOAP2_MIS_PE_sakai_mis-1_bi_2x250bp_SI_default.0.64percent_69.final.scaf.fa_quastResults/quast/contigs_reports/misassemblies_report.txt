All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_69.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_69.final.scaf
#Mis_misassemblies               3                                                                                3                                                                       
#Mis_relocations                 2                                                                                2                                                                       
#Mis_translocations              0                                                                                0                                                                       
#Mis_inversions                  1                                                                                1                                                                       
#Mis_misassembled contigs        3                                                                                3                                                                       
Mis_Misassembled contigs length  1887                                                                             1887                                                                    
#Mis_local misassemblies         1                                                                                8                                                                       
#mismatches                      27169                                                                            27406                                                                   
#indels                          255                                                                              341                                                                     
#Mis_short indels (<= 5 bp)      255                                                                              341                                                                     
#Mis_long indels (> 5 bp)        0                                                                                0                                                                       
Indels length                    258                                                                              344                                                                     
