All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_115.final.contig
#Contigs (>= 0 bp)             101133                                                                    
#Contigs (>= 1000 bp)          130                                                                       
Total length (>= 0 bp)         21106158                                                                  
Total length (>= 1000 bp)      157427                                                                    
#Contigs                       55080                                                                     
Largest contig                 2390                                                                      
Total length                   14247545                                                                  
Reference length               5594470                                                                   
GC (%)                         50.44                                                                     
Reference GC (%)               50.48                                                                     
N50                            228                                                                       
NG50                           356                                                                       
N75                            226                                                                       
NG75                           252                                                                       
#misassemblies                 151                                                                       
#local misassemblies           3                                                                         
#unaligned contigs             46 + 30 part                                                              
Unaligned contigs length       19612                                                                     
Genome fraction (%)            93.909                                                                    
Duplication ratio              2.637                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        6.99                                                                      
#indels per 100 kbp            236.29                                                                    
#genes                         483 + 4681 part                                                           
#predicted genes (unique)      53319                                                                     
#predicted genes (>= 0 bp)     54343                                                                     
#predicted genes (>= 300 bp)   4139                                                                      
#predicted genes (>= 1500 bp)  1                                                                         
#predicted genes (>= 3000 bp)  0                                                                         
Largest alignment              2390                                                                      
NA50                           228                                                                       
NGA50                          353                                                                       
NA75                           225                                                                       
NGA75                          246                                                                       
