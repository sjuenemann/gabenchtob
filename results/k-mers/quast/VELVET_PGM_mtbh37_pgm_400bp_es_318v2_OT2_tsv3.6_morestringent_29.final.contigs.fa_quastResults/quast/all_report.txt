All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_29.final.contigs
#Contigs                                   2591                                                                          
#Contigs (>= 0 bp)                         3423                                                                          
#Contigs (>= 1000 bp)                      1338                                                                          
Largest contig                             19517                                                                         
Total length                               4180522                                                                       
Total length (>= 0 bp)                     4273648                                                                       
Total length (>= 1000 bp)                  3517682                                                                       
Reference length                           4411532                                                                       
N50                                        2643                                                                          
NG50                                       2498                                                                          
N75                                        1407                                                                          
NG75                                       1226                                                                          
L50                                        481                                                                           
LG50                                       526                                                                           
L75                                        1020                                                                          
LG75                                       1152                                                                          
#local misassemblies                       6                                                                             
#misassemblies                             0                                                                             
#misassembled contigs                      0                                                                             
Misassembled contigs length                0                                                                             
Misassemblies inter-contig overlap         691                                                                           
#unaligned contigs                         2 + 4 part                                                                    
Unaligned contigs length                   1447                                                                          
#ambiguously mapped contigs                28                                                                            
Extra bases in ambiguously mapped contigs  -13586                                                                        
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        93.855                                                                        
Duplication ratio                          1.006                                                                         
#genes                                     2452 + 1551 part                                                              
#predicted genes (unique)                  6152                                                                          
#predicted genes (>= 0 bp)                 6152                                                                          
#predicted genes (>= 300 bp)               4296                                                                          
#predicted genes (>= 1500 bp)              216                                                                           
#predicted genes (>= 3000 bp)              9                                                                             
#mismatches                                271                                                                           
#indels                                    2412                                                                          
Indels length                              2670                                                                          
#mismatches per 100 kbp                    6.55                                                                          
#indels per 100 kbp                        58.25                                                                         
GC (%)                                     65.29                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.908                                                                        
Largest alignment                          19517                                                                         
NA50                                       2643                                                                          
NGA50                                      2498                                                                          
NA75                                       1407                                                                          
NGA75                                      1226                                                                          
LA50                                       481                                                                           
LGA50                                      526                                                                           
LA75                                       1020                                                                          
LGA75                                      1152                                                                          
#Mis_misassemblies                         0                                                                             
#Mis_relocations                           0                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  0                                                                             
Mis_Misassembled contigs length            0                                                                             
#Mis_local misassemblies                   6                                                                             
#Mis_short indels (<= 5 bp)                2410                                                                          
#Mis_long indels (> 5 bp)                  2                                                                             
#Una_fully unaligned contigs               2                                                                             
Una_Fully unaligned length                 857                                                                           
#Una_partially unaligned contigs           4                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            1                                                                             
Una_Partially unaligned length             590                                                                           
GAGE_Contigs #                             2591                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            19517                                                                         
GAGE_N50                                   2498 COUNT: 526                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         4180522                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               239233(5.42%)                                                                 
GAGE_Missing assembly bases                1546(0.04%)                                                                   
GAGE_Missing assembly contigs              2(0.08%)                                                                      
GAGE_Duplicated reference bases            0                                                                             
GAGE_Compressed reference bases            19362                                                                         
GAGE_Bad trim                              689                                                                           
GAGE_Avg idy                               99.93                                                                         
GAGE_SNPs                                  241                                                                           
GAGE_Indels < 5bp                          2577                                                                          
GAGE_Indels >= 5                           8                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            1                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    2597                                                                          
GAGE_Corrected assembly size               4180692                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    19527                                                                         
GAGE_Corrected N50                         2497 COUNT: 528                                                               
