All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_23.final.contigs
#Contigs (>= 0 bp)             5698                                                                       
#Contigs (>= 1000 bp)          1650                                                                       
Total length (>= 0 bp)         5349503                                                                    
Total length (>= 1000 bp)      4286411                                                                    
#Contigs                       3325                                                                       
Largest contig                 12308                                                                      
Total length                   5134489                                                                    
Reference length               5594470                                                                    
GC (%)                         50.18                                                                      
Reference GC (%)               50.48                                                                      
N50                            2556                                                                       
NG50                           2336                                                                       
N75                            1402                                                                       
NG75                           1074                                                                       
#misassemblies                 2                                                                          
#local misassemblies           9                                                                          
#unaligned contigs             2 + 2 part                                                                 
Unaligned contigs length       890                                                                        
Genome fraction (%)            90.160                                                                     
Duplication ratio              1.007                                                                      
#N's per 100 kbp               0.58                                                                       
#mismatches per 100 kbp        5.25                                                                       
#indels per 100 kbp            35.19                                                                      
#genes                         3071 + 1780 part                                                           
#predicted genes (unique)      7477                                                                       
#predicted genes (>= 0 bp)     7477                                                                       
#predicted genes (>= 300 bp)   5131                                                                       
#predicted genes (>= 1500 bp)  321                                                                        
#predicted genes (>= 3000 bp)  13                                                                         
Largest alignment              12308                                                                      
NA50                           2556                                                                       
NGA50                          2336                                                                       
NA75                           1394                                                                       
NGA75                          1069                                                                       
