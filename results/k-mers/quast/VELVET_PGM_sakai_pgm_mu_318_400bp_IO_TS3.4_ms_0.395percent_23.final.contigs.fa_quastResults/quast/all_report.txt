All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_23.final.contigs
#Contigs                                   3325                                                                       
#Contigs (>= 0 bp)                         5698                                                                       
#Contigs (>= 1000 bp)                      1650                                                                       
Largest contig                             12308                                                                      
Total length                               5134489                                                                    
Total length (>= 0 bp)                     5349503                                                                    
Total length (>= 1000 bp)                  4286411                                                                    
Reference length                           5594470                                                                    
N50                                        2556                                                                       
NG50                                       2336                                                                       
N75                                        1402                                                                       
NG75                                       1074                                                                       
L50                                        609                                                                        
LG50                                       703                                                                        
L75                                        1284                                                                       
LG75                                       1563                                                                       
#local misassemblies                       9                                                                          
#misassemblies                             2                                                                          
#misassembled contigs                      2                                                                          
Misassembled contigs length                1765                                                                       
Misassemblies inter-contig overlap         338                                                                        
#unaligned contigs                         2 + 2 part                                                                 
Unaligned contigs length                   890                                                                        
#ambiguously mapped contigs                132                                                                        
Extra bases in ambiguously mapped contigs  -54771                                                                     
#N's                                       30                                                                         
#N's per 100 kbp                           0.58                                                                       
Genome fraction (%)                        90.160                                                                     
Duplication ratio                          1.007                                                                      
#genes                                     3071 + 1780 part                                                           
#predicted genes (unique)                  7477                                                                       
#predicted genes (>= 0 bp)                 7477                                                                       
#predicted genes (>= 300 bp)               5131                                                                       
#predicted genes (>= 1500 bp)              321                                                                        
#predicted genes (>= 3000 bp)              13                                                                         
#mismatches                                265                                                                        
#indels                                    1775                                                                       
Indels length                              2056                                                                       
#mismatches per 100 kbp                    5.25                                                                       
#indels per 100 kbp                        35.19                                                                      
GC (%)                                     50.18                                                                      
Reference GC (%)                           50.48                                                                      
Average %IDY                               99.864                                                                     
Largest alignment                          12308                                                                      
NA50                                       2556                                                                       
NGA50                                      2336                                                                       
NA75                                       1394                                                                       
NGA75                                      1069                                                                       
LA50                                       609                                                                        
LGA50                                      703                                                                        
LA75                                       1285                                                                       
LGA75                                      1565                                                                       
#Mis_misassemblies                         2                                                                          
#Mis_relocations                           2                                                                          
#Mis_translocations                        0                                                                          
#Mis_inversions                            0                                                                          
#Mis_misassembled contigs                  2                                                                          
Mis_Misassembled contigs length            1765                                                                       
#Mis_local misassemblies                   9                                                                          
#Mis_short indels (<= 5 bp)                1770                                                                       
#Mis_long indels (> 5 bp)                  5                                                                          
#Una_fully unaligned contigs               2                                                                          
Una_Fully unaligned length                 842                                                                        
#Una_partially unaligned contigs           2                                                                          
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             48                                                                         
GAGE_Contigs #                             3325                                                                       
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            12308                                                                      
GAGE_N50                                   2336 COUNT: 703                                                            
GAGE_Genome size                           5594470                                                                    
GAGE_Assembly size                         5134489                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               387561(6.93%)                                                              
GAGE_Missing assembly bases                390(0.01%)                                                                 
GAGE_Missing assembly contigs              1(0.03%)                                                                   
GAGE_Duplicated reference bases            0                                                                          
GAGE_Compressed reference bases            109902                                                                     
GAGE_Bad trim                              129                                                                        
GAGE_Avg idy                               99.95                                                                      
GAGE_SNPs                                  334                                                                        
GAGE_Indels < 5bp                          1966                                                                       
GAGE_Indels >= 5                           8                                                                          
GAGE_Inversions                            3                                                                          
GAGE_Relocation                            7                                                                          
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    3335                                                                       
GAGE_Corrected assembly size               5134142                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    12307                                                                      
GAGE_Corrected N50                         2335 COUNT: 704                                                            
