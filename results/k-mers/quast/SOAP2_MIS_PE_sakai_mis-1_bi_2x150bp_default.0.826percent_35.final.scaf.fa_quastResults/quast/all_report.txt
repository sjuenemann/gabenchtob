All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_35.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_35.final.scaf
#Contigs                                   9987                                                                           9862                                                                  
#Contigs (>= 0 bp)                         82663                                                                          82166                                                                 
#Contigs (>= 1000 bp)                      312                                                                            367                                                                   
Largest contig                             4090                                                                           7723                                                                  
Total length                               4188163                                                                        4267774                                                               
Total length (>= 0 bp)                     13952171                                                                       13981460                                                              
Total length (>= 1000 bp)                  425164                                                                         544042                                                                
Reference length                           5594470                                                                        5594470                                                               
N50                                        452                                                                            467                                                                   
NG50                                       351                                                                            369                                                                   
N75                                        309                                                                            316                                                                   
NG75                                       None                                                                           207                                                                   
L50                                        2966                                                                           2844                                                                  
LG50                                       4732                                                                           4445                                                                  
L75                                        5777                                                                           5629                                                                  
LG75                                       None                                                                           9509                                                                  
#local misassemblies                       2                                                                              103                                                                   
#misassemblies                             2                                                                              2                                                                     
#misassembled contigs                      2                                                                              2                                                                     
Misassembled contigs length                1570                                                                           1570                                                                  
Misassemblies inter-contig overlap         0                                                                              0                                                                     
#unaligned contigs                         45 + 2 part                                                                    168 + 54 part                                                         
Unaligned contigs length                   12487                                                                          138864                                                                
#ambiguously mapped contigs                53                                                                             53                                                                    
Extra bases in ambiguously mapped contigs  -14680                                                                         -14680                                                                
#N's                                       32                                                                             29324                                                                 
#N's per 100 kbp                           0.76                                                                           687.10                                                                
Genome fraction (%)                        72.235                                                                         71.150                                                                
Duplication ratio                          1.030                                                                          1.034                                                                 
#genes                                     546 + 4171 part                                                                559 + 4119 part                                                       
#predicted genes (unique)                  11560                                                                          11679                                                                 
#predicted genes (>= 0 bp)                 11560                                                                          11679                                                                 
#predicted genes (>= 300 bp)               4660                                                                           4684                                                                  
#predicted genes (>= 1500 bp)              14                                                                             15                                                                    
#predicted genes (>= 3000 bp)              1                                                                              1                                                                     
#mismatches                                106                                                                            104                                                                   
#indels                                    25                                                                             1226                                                                  
Indels length                              29                                                                             2692                                                                  
#mismatches per 100 kbp                    2.62                                                                           2.61                                                                  
#indels per 100 kbp                        0.62                                                                           30.80                                                                 
GC (%)                                     49.54                                                                          49.54                                                                 
Reference GC (%)                           50.48                                                                          50.48                                                                 
Average %IDY                               99.996                                                                         99.973                                                                
Largest alignment                          4090                                                                           7723                                                                  
NA50                                       451                                                                            446                                                                   
NGA50                                      350                                                                            347                                                                   
NA75                                       308                                                                            299                                                                   
NGA75                                      None                                                                           None                                                                  
LA50                                       2969                                                                           3002                                                                  
LGA50                                      4739                                                                           4691                                                                  
LA75                                       5788                                                                           5946                                                                  
LGA75                                      None                                                                           None                                                                  
#Mis_misassemblies                         2                                                                              2                                                                     
#Mis_relocations                           2                                                                              2                                                                     
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  2                                                                              2                                                                     
Mis_Misassembled contigs length            1570                                                                           1570                                                                  
#Mis_local misassemblies                   2                                                                              103                                                                   
#Mis_short indels (<= 5 bp)                25                                                                             1176                                                                  
#Mis_long indels (> 5 bp)                  0                                                                              50                                                                    
#Una_fully unaligned contigs               45                                                                             168                                                                   
Una_Fully unaligned length                 12385                                                                          113968                                                                
#Una_partially unaligned contigs           2                                                                              54                                                                    
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            0                                                                              14                                                                    
Una_Partially unaligned length             102                                                                            24896                                                                 
GAGE_Contigs #                             9987                                                                           9862                                                                  
GAGE_Min contig                            200                                                                            200                                                                   
GAGE_Max contig                            4090                                                                           7723                                                                  
GAGE_N50                                   351 COUNT: 4732                                                                369 COUNT: 4445                                                       
GAGE_Genome size                           5594470                                                                        5594470                                                               
GAGE_Assembly size                         4188163                                                                        4267774                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               1521792(27.20%)                                                                1482227(26.49%)                                                       
GAGE_Missing assembly bases                11482(0.27%)                                                                   48112(1.13%)                                                          
GAGE_Missing assembly contigs              41(0.41%)                                                                      46(0.47%)                                                             
GAGE_Duplicated reference bases            225                                                                            225                                                                   
GAGE_Compressed reference bases            17125                                                                          17620                                                                 
GAGE_Bad trim                              121                                                                            8128                                                                  
GAGE_Avg idy                               100.00                                                                         99.99                                                                 
GAGE_SNPs                                  89                                                                             99                                                                    
GAGE_Indels < 5bp                          29                                                                             49                                                                    
GAGE_Indels >= 5                           3                                                                              421                                                                   
GAGE_Inversions                            0                                                                              0                                                                     
GAGE_Relocation                            2                                                                              4                                                                     
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    9945                                                                           9928                                                                  
GAGE_Corrected assembly size               4175998                                                                        4171840                                                               
GAGE_Min correct contig                    200                                                                            200                                                                   
GAGE_Max correct contig                    4090                                                                           4090                                                                  
GAGE_Corrected N50                         351 COUNT: 4737                                                                351 COUNT: 4735                                                       
