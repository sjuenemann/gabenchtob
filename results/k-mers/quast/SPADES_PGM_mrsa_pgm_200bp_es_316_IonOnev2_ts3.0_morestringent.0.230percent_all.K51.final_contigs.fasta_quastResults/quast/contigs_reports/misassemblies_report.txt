All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K51.final_contigs
#Mis_misassemblies               2                                                                                               
#Mis_relocations                 2                                                                                               
#Mis_translocations              0                                                                                               
#Mis_inversions                  0                                                                                               
#Mis_misassembled contigs        2                                                                                               
Mis_Misassembled contigs length  63674                                                                                           
#Mis_local misassemblies         13                                                                                              
#mismatches                      129                                                                                             
#indels                          446                                                                                             
#Mis_short indels (<= 5 bp)      443                                                                                             
#Mis_long indels (> 5 bp)        3                                                                                               
Indels length                    627                                                                                             
