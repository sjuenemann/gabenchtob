All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K125.final_contigs
GAGE_Contigs #                   8358                                                                             
GAGE_Min contig                  200                                                                              
GAGE_Max contig                  268345                                                                           
GAGE_N50                         135088 COUNT: 15                                                                 
GAGE_Genome size                 5594470                                                                          
GAGE_Assembly size               7947884                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     4386(0.08%)                                                                      
GAGE_Missing assembly bases      32693(0.41%)                                                                     
GAGE_Missing assembly contigs    45(0.54%)                                                                        
GAGE_Duplicated reference bases  2529574                                                                          
GAGE_Compressed reference bases  280179                                                                           
GAGE_Bad trim                    17950                                                                            
GAGE_Avg idy                     99.98                                                                            
GAGE_SNPs                        189                                                                              
GAGE_Indels < 5bp                542                                                                              
GAGE_Indels >= 5                 9                                                                                
GAGE_Inversions                  1                                                                                
GAGE_Relocation                  11                                                                               
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          381                                                                              
GAGE_Corrected assembly size     5389099                                                                          
GAGE_Min correct contig          203                                                                              
GAGE_Max correct contig          238480                                                                           
GAGE_Corrected N50               96595 COUNT: 18                                                                  
