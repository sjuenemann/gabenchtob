All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_119.final.contig
GAGE_Contigs #                   37083                                                                                
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  1701                                                                                 
GAGE_N50                         418 COUNT: 2679                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               11216824                                                                             
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     15911(0.57%)                                                                         
GAGE_Missing assembly bases      180288(1.61%)                                                                        
GAGE_Missing assembly contigs    269(0.73%)                                                                           
GAGE_Duplicated reference bases  7391894                                                                              
GAGE_Compressed reference bases  34468                                                                                
GAGE_Bad trim                    73923                                                                                
GAGE_Avg idy                     99.89                                                                                
GAGE_SNPs                        106                                                                                  
GAGE_Indels < 5bp                2170                                                                                 
GAGE_Indels >= 5                 2                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  0                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          12259                                                                                
GAGE_Corrected assembly size     3642157                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          1701                                                                                 
GAGE_Corrected N50               325 COUNT: 2894                                                                      
