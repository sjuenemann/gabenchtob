All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_113.final.contig
GAGE_Contigs #                   55362                                                                     
GAGE_Min contig                  200                                                                       
GAGE_Max contig                  2388                                                                      
GAGE_N50                         349 COUNT: 5921                                                           
GAGE_Genome size                 5594470                                                                   
GAGE_Assembly size               14101767                                                                  
GAGE_Chaff bases                 0                                                                         
GAGE_Missing reference bases     66089(1.18%)                                                              
GAGE_Missing assembly bases      12266(0.09%)                                                              
GAGE_Missing assembly contigs    19(0.03%)                                                                 
GAGE_Duplicated reference bases  7081924                                                                   
GAGE_Compressed reference bases  267808                                                                    
GAGE_Bad trim                    5599                                                                      
GAGE_Avg idy                     99.91                                                                     
GAGE_SNPs                        70                                                                        
GAGE_Indels < 5bp                3839                                                                      
GAGE_Indels >= 5                 1                                                                         
GAGE_Inversions                  2                                                                         
GAGE_Relocation                  1                                                                         
GAGE_Translocation               0                                                                         
GAGE_Corrected contig #          25175                                                                     
GAGE_Corrected assembly size     6994446                                                                   
GAGE_Min correct contig          200                                                                       
GAGE_Max correct contig          2388                                                                      
GAGE_Corrected N50               273 COUNT: 6362                                                           
