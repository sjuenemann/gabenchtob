All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_99.final.contig
#Contigs                                   9334                                                                        
#Contigs (>= 0 bp)                         124491                                                                      
#Contigs (>= 1000 bp)                      35                                                                          
Largest contig                             1976                                                                        
Total length                               3021776                                                                     
Total length (>= 0 bp)                     20812348                                                                    
Total length (>= 1000 bp)                  40806                                                                       
Reference length                           4411532                                                                     
N50                                        322                                                                         
NG50                                       272                                                                         
N75                                        264                                                                         
NG75                                       None                                                                        
L50                                        3474                                                                        
LG50                                       5777                                                                        
L75                                        6003                                                                        
LG75                                       None                                                                        
#local misassemblies                       5                                                                           
#misassemblies                             31                                                                          
#misassembled contigs                      31                                                                          
Misassembled contigs length                10194                                                                       
Misassemblies inter-contig overlap         619                                                                         
#unaligned contigs                         131 + 18 part                                                               
Unaligned contigs length                   45339                                                                       
#ambiguously mapped contigs                25                                                                          
Extra bases in ambiguously mapped contigs  -6645                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        56.880                                                                      
Duplication ratio                          1.184                                                                       
#genes                                     144 + 3411 part                                                             
#predicted genes (unique)                  8889                                                                        
#predicted genes (>= 0 bp)                 8904                                                                        
#predicted genes (>= 300 bp)               2128                                                                        
#predicted genes (>= 1500 bp)              1                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                544                                                                         
#indels                                    12005                                                                       
Indels length                              12393                                                                       
#mismatches per 100 kbp                    21.68                                                                       
#indels per 100 kbp                        478.42                                                                      
GC (%)                                     65.96                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               99.389                                                                      
Largest alignment                          1976                                                                        
NA50                                       320                                                                         
NGA50                                      265                                                                         
NA75                                       257                                                                         
NGA75                                      None                                                                        
LA50                                       3485                                                                        
LGA50                                      5822                                                                        
LA75                                       6054                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         31                                                                          
#Mis_relocations                           31                                                                          
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  31                                                                          
Mis_Misassembled contigs length            10194                                                                       
#Mis_local misassemblies                   5                                                                           
#Mis_short indels (<= 5 bp)                12004                                                                       
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               131                                                                         
Una_Fully unaligned length                 43178                                                                       
#Una_partially unaligned contigs           18                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             2161                                                                        
GAGE_Contigs #                             9334                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1976                                                                        
GAGE_N50                                   272 COUNT: 5777                                                             
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         3021776                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               1856482(42.08%)                                                             
GAGE_Missing assembly bases                13067(0.43%)                                                                
GAGE_Missing assembly contigs              20(0.21%)                                                                   
GAGE_Duplicated reference bases            261967                                                                      
GAGE_Compressed reference bases            29297                                                                       
GAGE_Bad trim                              6627                                                                        
GAGE_Avg idy                               99.45                                                                       
GAGE_SNPs                                  488                                                                         
GAGE_Indels < 5bp                          10815                                                                       
GAGE_Indels >= 5                           14                                                                          
GAGE_Inversions                            8                                                                           
GAGE_Relocation                            4                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    8302                                                                        
GAGE_Corrected assembly size               2731041                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1976                                                                        
GAGE_Corrected N50                         249 COUNT: 5930                                                             
