All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_99.final.contig
#Mis_misassemblies               31                                                                          
#Mis_relocations                 31                                                                          
#Mis_translocations              0                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        31                                                                          
Mis_Misassembled contigs length  10194                                                                       
#Mis_local misassemblies         5                                                                           
#mismatches                      544                                                                         
#indels                          12005                                                                       
#Mis_short indels (<= 5 bp)      12004                                                                       
#Mis_long indels (> 5 bp)        1                                                                           
Indels length                    12393                                                                       
