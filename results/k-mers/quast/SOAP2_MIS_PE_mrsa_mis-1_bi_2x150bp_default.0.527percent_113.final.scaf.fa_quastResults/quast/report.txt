All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_113.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_113.final.scaf
#Contigs (>= 0 bp)             591                                                                            343                                                                   
#Contigs (>= 1000 bp)          333                                                                            150                                                                   
Total length (>= 0 bp)         2814515                                                                        2821583                                                               
Total length (>= 1000 bp)      2736813                                                                        2777827                                                               
#Contigs                       479                                                                            234                                                                   
Largest contig                 39757                                                                          115750                                                                
Total length                   2798768                                                                        2806255                                                               
Reference length               2813862                                                                        2813862                                                               
GC (%)                         32.74                                                                          32.74                                                                 
Reference GC (%)               32.81                                                                          32.81                                                                 
N50                            13919                                                                          36313                                                                 
NG50                           13919                                                                          36313                                                                 
N75                            6490                                                                           16505                                                                 
NG75                           6478                                                                           16505                                                                 
#misassemblies                 5                                                                              9                                                                     
#local misassemblies           2                                                                              17                                                                    
#unaligned contigs             3 + 0 part                                                                     3 + 1 part                                                            
Unaligned contigs length       5839                                                                           10217                                                                 
Genome fraction (%)            98.266                                                                         98.267                                                                
Duplication ratio              1.005                                                                          1.006                                                                 
#N's per 100 kbp               11.90                                                                          263.73                                                                
#mismatches per 100 kbp        1.34                                                                           1.52                                                                  
#indels per 100 kbp            3.69                                                                           71.54                                                                 
#genes                         2448 + 255 part                                                                2591 + 104 part                                                       
#predicted genes (unique)      2887                                                                           2807                                                                  
#predicted genes (>= 0 bp)     2896                                                                           2817                                                                  
#predicted genes (>= 300 bp)   2368                                                                           2349                                                                  
#predicted genes (>= 1500 bp)  270                                                                            277                                                                   
#predicted genes (>= 3000 bp)  22                                                                             22                                                                    
Largest alignment              39757                                                                          115750                                                                
NA50                           13803                                                                          35189                                                                 
NGA50                          13803                                                                          35189                                                                 
NA75                           6490                                                                           16505                                                                 
NGA75                          6478                                                                           16505                                                                 
