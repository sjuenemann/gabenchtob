All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K79.final_contigs
GAGE_Contigs #                   2480                                                                                            
GAGE_Min contig                  204                                                                                             
GAGE_Max contig                  207828                                                                                          
GAGE_N50                         88796 COUNT: 10                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               3355327                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     4735(0.17%)                                                                                     
GAGE_Missing assembly bases      140047(4.17%)                                                                                   
GAGE_Missing assembly contigs    385(15.52%)                                                                                     
GAGE_Duplicated reference bases  442955                                                                                          
GAGE_Compressed reference bases  45534                                                                                           
GAGE_Bad trim                    45181                                                                                           
GAGE_Avg idy                     99.97                                                                                           
GAGE_SNPs                        117                                                                                             
GAGE_Indels < 5bp                564                                                                                             
GAGE_Indels >= 5                 10                                                                                              
GAGE_Inversions                  1                                                                                               
GAGE_Relocation                  6                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          134                                                                                             
GAGE_Corrected assembly size     2775727                                                                                         
GAGE_Min correct contig          203                                                                                             
GAGE_Max correct contig          207853                                                                                          
GAGE_Corrected N50               55460 COUNT: 15                                                                                 
