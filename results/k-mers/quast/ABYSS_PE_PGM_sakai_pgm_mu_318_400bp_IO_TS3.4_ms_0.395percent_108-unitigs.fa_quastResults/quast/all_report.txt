All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_108-unitigs
#Contigs                                   2594                                                                    
#Contigs (>= 0 bp)                         4217                                                                    
#Contigs (>= 1000 bp)                      679                                                                     
Largest contig                             50019                                                                   
Total length                               5635343                                                                 
Total length (>= 0 bp)                     5869127                                                                 
Total length (>= 1000 bp)                  5113101                                                                 
Reference length                           5594470                                                                 
N50                                        11596                                                                   
NG50                                       11615                                                                   
N75                                        4990                                                                    
NG75                                       5180                                                                    
L50                                        139                                                                     
LG50                                       138                                                                     
L75                                        317                                                                     
LG75                                       311                                                                     
#local misassemblies                       7                                                                       
#misassemblies                             5                                                                       
#misassembled contigs                      5                                                                       
Misassembled contigs length                29369                                                                   
Misassemblies inter-contig overlap         1119                                                                    
#unaligned contigs                         0 + 6 part                                                              
Unaligned contigs length                   281                                                                     
#ambiguously mapped contigs                528                                                                     
Extra bases in ambiguously mapped contigs  -156136                                                                 
#N's                                       0                                                                       
#N's per 100 kbp                           0.00                                                                    
Genome fraction (%)                        94.390                                                                  
Duplication ratio                          1.038                                                                   
#genes                                     4457 + 707 part                                                         
#predicted genes (unique)                  7231                                                                    
#predicted genes (>= 0 bp)                 7268                                                                    
#predicted genes (>= 300 bp)               4657                                                                    
#predicted genes (>= 1500 bp)              585                                                                     
#predicted genes (>= 3000 bp)              47                                                                      
#mismatches                                80                                                                      
#indels                                    404                                                                     
Indels length                              414                                                                     
#mismatches per 100 kbp                    1.51                                                                    
#indels per 100 kbp                        7.65                                                                    
GC (%)                                     50.42                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               99.135                                                                  
Largest alignment                          50019                                                                   
NA50                                       11549                                                                   
NGA50                                      11615                                                                   
NA75                                       4982                                                                    
NGA75                                      5122                                                                    
LA50                                       140                                                                     
LGA50                                      138                                                                     
LA75                                       318                                                                     
LGA75                                      312                                                                     
#Mis_misassemblies                         5                                                                       
#Mis_relocations                           5                                                                       
#Mis_translocations                        0                                                                       
#Mis_inversions                            0                                                                       
#Mis_misassembled contigs                  5                                                                       
Mis_Misassembled contigs length            29369                                                                   
#Mis_local misassemblies                   7                                                                       
#Mis_short indels (<= 5 bp)                404                                                                     
#Mis_long indels (> 5 bp)                  0                                                                       
#Una_fully unaligned contigs               0                                                                       
Una_Fully unaligned length                 0                                                                       
#Una_partially unaligned contigs           6                                                                       
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             281                                                                     
GAGE_Contigs #                             2594                                                                    
GAGE_Min contig                            200                                                                     
GAGE_Max contig                            50019                                                                   
GAGE_N50                                   11615 COUNT: 138                                                        
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5635343                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               20065(0.36%)                                                            
GAGE_Missing assembly bases                289(0.01%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                
GAGE_Duplicated reference bases            178618                                                                  
GAGE_Compressed reference bases            280096                                                                  
GAGE_Bad trim                              289                                                                     
GAGE_Avg idy                               99.99                                                                   
GAGE_SNPs                                  55                                                                      
GAGE_Indels < 5bp                          428                                                                     
GAGE_Indels >= 5                           4                                                                       
GAGE_Inversions                            0                                                                       
GAGE_Relocation                            6                                                                       
GAGE_Translocation                         0                                                                       
GAGE_Corrected contig #                    1753                                                                    
GAGE_Corrected assembly size               5455282                                                                 
GAGE_Min correct contig                    200                                                                     
GAGE_Max correct contig                    50023                                                                   
GAGE_Corrected N50                         11597 COUNT: 139                                                        
