All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_60-unitigs
GAGE_Contigs #                   1246                                                                             
GAGE_Min contig                  200                                                                              
GAGE_Max contig                  51897                                                                            
GAGE_N50                         9799 COUNT: 165                                                                  
GAGE_Genome size                 5594470                                                                          
GAGE_Assembly size               5254576                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     230891(4.13%)                                                                    
GAGE_Missing assembly bases      99(0.00%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                         
GAGE_Duplicated reference bases  1860                                                                             
GAGE_Compressed reference bases  130897                                                                           
GAGE_Bad trim                    99                                                                               
GAGE_Avg idy                     99.97                                                                            
GAGE_SNPs                        113                                                                              
GAGE_Indels < 5bp                1275                                                                             
GAGE_Indels >= 5                 4                                                                                
GAGE_Inversions                  0                                                                                
GAGE_Relocation                  3                                                                                
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          1244                                                                             
GAGE_Corrected assembly size     5254184                                                                          
GAGE_Min correct contig          200                                                                              
GAGE_Max correct contig          51910                                                                            
GAGE_Corrected N50               9683 COUNT: 167                                                                  
