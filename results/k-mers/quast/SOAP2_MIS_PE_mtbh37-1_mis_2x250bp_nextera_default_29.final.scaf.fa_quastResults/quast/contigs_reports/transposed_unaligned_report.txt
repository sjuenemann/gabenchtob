All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_29.final.scaf_broken  25426                         5822102                     6                                 0                      0                                319                             0   
SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_29.final.scaf         25427                         5822381                     7                                 0                      0                                472                             132 
