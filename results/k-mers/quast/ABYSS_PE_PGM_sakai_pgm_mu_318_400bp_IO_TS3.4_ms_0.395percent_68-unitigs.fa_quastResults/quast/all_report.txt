All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_68-unitigs
#Contigs                                   1194                                                                   
#Contigs (>= 0 bp)                         6476                                                                   
#Contigs (>= 1000 bp)                      680                                                                    
Largest contig                             42961                                                                  
Total length                               5267256                                                                
Total length (>= 0 bp)                     5861338                                                                
Total length (>= 1000 bp)                  5045316                                                                
Reference length                           5594470                                                                
N50                                        11374                                                                  
NG50                                       10838                                                                  
N75                                        5915                                                                   
NG75                                       4865                                                                   
L50                                        137                                                                    
LG50                                       152                                                                    
L75                                        294                                                                    
LG75                                       339                                                                    
#local misassemblies                       7                                                                      
#misassemblies                             3                                                                      
#misassembled contigs                      3                                                                      
Misassembled contigs length                17512                                                                  
Misassemblies inter-contig overlap         650                                                                    
#unaligned contigs                         0 + 0 part                                                             
Unaligned contigs length                   0                                                                      
#ambiguously mapped contigs                130                                                                    
Extra bases in ambiguously mapped contigs  -61653                                                                 
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        92.893                                                                 
Duplication ratio                          1.002                                                                  
#genes                                     4328 + 710 part                                                        
#predicted genes (unique)                  5851                                                                   
#predicted genes (>= 0 bp)                 5853                                                                   
#predicted genes (>= 300 bp)               4589                                                                   
#predicted genes (>= 1500 bp)              582                                                                    
#predicted genes (>= 3000 bp)              53                                                                     
#mismatches                                57                                                                     
#indels                                    231                                                                    
Indels length                              235                                                                    
#mismatches per 100 kbp                    1.10                                                                   
#indels per 100 kbp                        4.44                                                                   
GC (%)                                     50.24                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.375                                                                 
Largest alignment                          42961                                                                  
NA50                                       11374                                                                  
NGA50                                      10823                                                                  
NA75                                       5915                                                                   
NGA75                                      4865                                                                   
LA50                                       137                                                                    
LGA50                                      152                                                                    
LA75                                       294                                                                    
LGA75                                      339                                                                    
#Mis_misassemblies                         3                                                                      
#Mis_relocations                           3                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  3                                                                      
Mis_Misassembled contigs length            17512                                                                  
#Mis_local misassemblies                   7                                                                      
#Mis_short indels (<= 5 bp)                231                                                                    
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           0                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             0                                                                      
GAGE_Contigs #                             1194                                                                   
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            42961                                                                  
GAGE_N50                                   10838 COUNT: 152                                                       
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5267256                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               205270(3.67%)                                                          
GAGE_Missing assembly bases                10(0.00%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            3842                                                                   
GAGE_Compressed reference bases            157720                                                                 
GAGE_Bad trim                              10                                                                     
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  87                                                                     
GAGE_Indels < 5bp                          238                                                                    
GAGE_Indels >= 5                           4                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            5                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    1186                                                                   
GAGE_Corrected assembly size               5263834                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    42961                                                                  
GAGE_Corrected N50                         10699 COUNT: 153                                                       
