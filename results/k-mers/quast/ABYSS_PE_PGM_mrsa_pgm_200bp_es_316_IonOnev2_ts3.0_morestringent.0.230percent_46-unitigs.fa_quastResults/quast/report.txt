All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_46-unitigs
#Contigs (>= 0 bp)             3562                                                                                   
#Contigs (>= 1000 bp)          295                                                                                    
Total length (>= 0 bp)         2961170                                                                                
Total length (>= 1000 bp)      2687597                                                                                
#Contigs                       419                                                                                    
Largest contig                 69773                                                                                  
Total length                   2742084                                                                                
Reference length               2813862                                                                                
GC (%)                         32.59                                                                                  
Reference GC (%)               32.81                                                                                  
N50                            14223                                                                                  
NG50                           13902                                                                                  
N75                            8095                                                                                   
NG75                           7879                                                                                   
#misassemblies                 1                                                                                      
#local misassemblies           5                                                                                      
#unaligned contigs             0 + 0 part                                                                             
Unaligned contigs length       0                                                                                      
Genome fraction (%)            97.075                                                                                 
Duplication ratio              1.002                                                                                  
#N's per 100 kbp               0.00                                                                                   
#mismatches per 100 kbp        4.21                                                                                   
#indels per 100 kbp            9.99                                                                                   
#genes                         2455 + 177 part                                                                        
#predicted genes (unique)      2838                                                                                   
#predicted genes (>= 0 bp)     2838                                                                                   
#predicted genes (>= 300 bp)   2340                                                                                   
#predicted genes (>= 1500 bp)  277                                                                                    
#predicted genes (>= 3000 bp)  20                                                                                     
Largest alignment              69773                                                                                  
NA50                           14223                                                                                  
NGA50                          13902                                                                                  
NA75                           8065                                                                                   
NGA75                          7644                                                                                   
