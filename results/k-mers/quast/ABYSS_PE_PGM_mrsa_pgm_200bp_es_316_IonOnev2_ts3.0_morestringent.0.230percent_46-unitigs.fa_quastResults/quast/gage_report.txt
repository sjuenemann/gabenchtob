All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_46-unitigs
GAGE_Contigs #                   419                                                                                    
GAGE_Min contig                  207                                                                                    
GAGE_Max contig                  69773                                                                                  
GAGE_N50                         13902 COUNT: 58                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2742084                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     60265(2.14%)                                                                           
GAGE_Missing assembly bases      10(0.00%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  209                                                                                    
GAGE_Compressed reference bases  18364                                                                                  
GAGE_Bad trim                    10                                                                                     
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        88                                                                                     
GAGE_Indels < 5bp                266                                                                                    
GAGE_Indels >= 5                 5                                                                                      
GAGE_Inversions                  0                                                                                      
GAGE_Relocation                  2                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          424                                                                                    
GAGE_Corrected assembly size     2743013                                                                                
GAGE_Min correct contig          207                                                                                    
GAGE_Max correct contig          69783                                                                                  
GAGE_Corrected N50               13902 COUNT: 58                                                                        
