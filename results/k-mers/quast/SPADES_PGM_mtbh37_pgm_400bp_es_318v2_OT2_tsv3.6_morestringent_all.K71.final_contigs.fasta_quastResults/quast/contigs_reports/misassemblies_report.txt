All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K71.final_contigs
#Mis_misassemblies               11                                                                                 
#Mis_relocations                 11                                                                                 
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        7                                                                                  
Mis_Misassembled contigs length  121200                                                                             
#Mis_local misassemblies         29                                                                                 
#mismatches                      374                                                                                
#indels                          1273                                                                               
#Mis_short indels (<= 5 bp)      1267                                                                               
#Mis_long indels (> 5 bp)        6                                                                                  
Indels length                    1602                                                                               
