All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_175.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_175.final.contigs
#Contigs (>= 0 bp)             628                                                                                      463                                                                             
#Contigs (>= 1000 bp)          425                                                                                      282                                                                             
Total length (>= 0 bp)         5475916                                                                                  5497321                                                                         
Total length (>= 1000 bp)      5370035                                                                                  5405248                                                                         
#Contigs                       627                                                                                      463                                                                             
Largest contig                 77611                                                                                    121177                                                                          
Total length                   5475741                                                                                  5497321                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.36                                                                                    50.36                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            22110                                                                                    44571                                                                           
NG50                           21913                                                                                    43676                                                                           
N75                            11848                                                                                    20913                                                                           
NG75                           11493                                                                                    19221                                                                           
#misassemblies                 7                                                                                        14                                                                              
#local misassemblies           2                                                                                        142                                                                             
#unaligned contigs             1 + 0 part                                                                               1 + 0 part                                                                      
Unaligned contigs length       5560                                                                                     5560                                                                            
Genome fraction (%)            95.505                                                                                   95.586                                                                          
Duplication ratio              1.004                                                                                    1.008                                                                           
#N's per 100 kbp               0.00                                                                                     389.37                                                                          
#mismatches per 100 kbp        3.57                                                                                     4.79                                                                            
#indels per 100 kbp            0.30                                                                                     7.48                                                                            
#genes                         4858 + 423 part                                                                          4878 + 405 part                                                                 
#predicted genes (unique)      5658                                                                                     5636                                                                            
#predicted genes (>= 0 bp)     5690                                                                                     5667                                                                            
#predicted genes (>= 300 bp)   4702                                                                                     4699                                                                            
#predicted genes (>= 1500 bp)  640                                                                                      639                                                                             
#predicted genes (>= 3000 bp)  59                                                                                       60                                                                              
Largest alignment              77611                                                                                    120573                                                                          
NA50                           22021                                                                                    42796                                                                           
NGA50                          21913                                                                                    42593                                                                           
NA75                           11705                                                                                    18962                                                                           
NGA75                          11349                                                                                    17656                                                                           
