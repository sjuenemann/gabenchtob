All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_39.final.contigs
#Contigs                                   1486                                                                                 
#Contigs (>= 0 bp)                         2279                                                                                 
#Contigs (>= 1000 bp)                      896                                                                                  
Largest contig                             26132                                                                                
Total length                               5254552                                                                              
Total length (>= 0 bp)                     5353446                                                                              
Total length (>= 1000 bp)                  4986744                                                                              
Reference length                           5594470                                                                              
N50                                        7786                                                                                 
NG50                                       7383                                                                                 
N75                                        4259                                                                                 
NG75                                       3486                                                                                 
L50                                        211                                                                                  
LG50                                       234                                                                                  
L75                                        434                                                                                  
LG75                                       500                                                                                  
#local misassemblies                       7                                                                                    
#misassemblies                             2                                                                                    
#misassembled contigs                      2                                                                                    
Misassembled contigs length                11748                                                                                
Misassemblies inter-contig overlap         1148                                                                                 
#unaligned contigs                         0 + 2 part                                                                           
Unaligned contigs length                   189                                                                                  
#ambiguously mapped contigs                157                                                                                  
Extra bases in ambiguously mapped contigs  -74806                                                                               
#N's                                       10                                                                                   
#N's per 100 kbp                           0.19                                                                                 
Genome fraction (%)                        92.309                                                                               
Duplication ratio                          1.003                                                                                
#genes                                     4199 + 773 part                                                                      
#predicted genes (unique)                  6745                                                                                 
#predicted genes (>= 0 bp)                 6746                                                                                 
#predicted genes (>= 300 bp)               4995                                                                                 
#predicted genes (>= 1500 bp)              393                                                                                  
#predicted genes (>= 3000 bp)              17                                                                                   
#mismatches                                272                                                                                  
#indels                                    2595                                                                                 
Indels length                              2867                                                                                 
#mismatches per 100 kbp                    5.27                                                                                 
#indels per 100 kbp                        50.25                                                                                
GC (%)                                     50.24                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               99.749                                                                               
Largest alignment                          26132                                                                                
NA50                                       7786                                                                                 
NGA50                                      7383                                                                                 
NA75                                       4259                                                                                 
NGA75                                      3463                                                                                 
LA50                                       211                                                                                  
LGA50                                      234                                                                                  
LA75                                       434                                                                                  
LGA75                                      501                                                                                  
#Mis_misassemblies                         2                                                                                    
#Mis_relocations                           2                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  2                                                                                    
Mis_Misassembled contigs length            11748                                                                                
#Mis_local misassemblies                   7                                                                                    
#Mis_short indels (<= 5 bp)                2588                                                                                 
#Mis_long indels (> 5 bp)                  7                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           2                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             189                                                                                  
GAGE_Contigs #                             1486                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            26132                                                                                
GAGE_N50                                   7383 COUNT: 234                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5254552                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               203203(3.63%)                                                                        
GAGE_Missing assembly bases                250(0.00%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            1208                                                                                 
GAGE_Compressed reference bases            162815                                                                               
GAGE_Bad trim                              240                                                                                  
GAGE_Avg idy                               99.94                                                                                
GAGE_SNPs                                  269                                                                                  
GAGE_Indels < 5bp                          2833                                                                                 
GAGE_Indels >= 5                           9                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            4                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1495                                                                                 
GAGE_Corrected assembly size               5256544                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    26146                                                                                
GAGE_Corrected N50                         7345 COUNT: 234                                                                      
