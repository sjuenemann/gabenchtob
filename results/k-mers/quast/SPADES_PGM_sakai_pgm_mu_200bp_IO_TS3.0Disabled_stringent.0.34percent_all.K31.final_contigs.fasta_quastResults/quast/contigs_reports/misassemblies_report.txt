All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K31.final_contigs
#Mis_misassemblies               3                                                                                         
#Mis_relocations                 3                                                                                         
#Mis_translocations              0                                                                                         
#Mis_inversions                  0                                                                                         
#Mis_misassembled contigs        3                                                                                         
Mis_Misassembled contigs length  34633                                                                                     
#Mis_local misassemblies         9                                                                                         
#mismatches                      230                                                                                       
#indels                          1207                                                                                      
#Mis_short indels (<= 5 bp)      1206                                                                                      
#Mis_long indels (> 5 bp)        1                                                                                         
Indels length                    1262                                                                                      
