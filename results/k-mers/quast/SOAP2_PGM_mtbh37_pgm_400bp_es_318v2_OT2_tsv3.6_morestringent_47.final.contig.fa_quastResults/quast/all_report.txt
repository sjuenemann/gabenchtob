All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_47.final.contig
#Contigs                                   2236                                                                        
#Contigs (>= 0 bp)                         302811                                                                      
#Contigs (>= 1000 bp)                      1                                                                           
Largest contig                             1318                                                                        
Total length                               606735                                                                      
Total length (>= 0 bp)                     23370036                                                                    
Total length (>= 1000 bp)                  1318                                                                        
Reference length                           4411532                                                                     
N50                                        258                                                                         
NG50                                       None                                                                        
N75                                        220                                                                         
NG75                                       None                                                                        
L50                                        872                                                                         
LG50                                       None                                                                        
L75                                        1511                                                                        
LG75                                       None                                                                        
#local misassemblies                       1                                                                           
#misassemblies                             0                                                                           
#misassembled contigs                      0                                                                           
Misassembled contigs length                0                                                                           
Misassemblies inter-contig overlap         103                                                                         
#unaligned contigs                         326 + 3 part                                                                
Unaligned contigs length                   72645                                                                       
#ambiguously mapped contigs                5                                                                           
Extra bases in ambiguously mapped contigs  -1177                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        12.051                                                                      
Duplication ratio                          1.003                                                                       
#genes                                     10 + 1322 part                                                              
#predicted genes (unique)                  1873                                                                        
#predicted genes (>= 0 bp)                 1873                                                                        
#predicted genes (>= 300 bp)               327                                                                         
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                37                                                                          
#indels                                    294                                                                         
Indels length                              303                                                                         
#mismatches per 100 kbp                    6.96                                                                        
#indels per 100 kbp                        55.30                                                                       
GC (%)                                     68.53                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               99.935                                                                      
Largest alignment                          1318                                                                        
NA50                                       255                                                                         
NGA50                                      None                                                                        
NA75                                       214                                                                         
NGA75                                      None                                                                        
LA50                                       875                                                                         
LGA50                                      None                                                                        
LA75                                       1527                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         0                                                                           
#Mis_relocations                           0                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  0                                                                           
Mis_Misassembled contigs length            0                                                                           
#Mis_local misassemblies                   1                                                                           
#Mis_short indels (<= 5 bp)                294                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               326                                                                         
Una_Fully unaligned length                 72411                                                                       
#Una_partially unaligned contigs           3                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             234                                                                         
GAGE_Contigs #                             2236                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1318                                                                        
GAGE_N50                                   0 COUNT: 0                                                                  
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         606735                                                                      
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               3839763(87.04%)                                                             
GAGE_Missing assembly bases                32654(5.38%)                                                                
GAGE_Missing assembly contigs              132(5.90%)                                                                  
GAGE_Duplicated reference bases            1362                                                                        
GAGE_Compressed reference bases            1728                                                                        
GAGE_Bad trim                              2899                                                                        
GAGE_Avg idy                               99.69                                                                       
GAGE_SNPs                                  82                                                                          
GAGE_Indels < 5bp                          1649                                                                        
GAGE_Indels >= 5                           0                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            1                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    2025                                                                        
GAGE_Corrected assembly size               559868                                                                      
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1319                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
