All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_95.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_95.final.scaf
#Contigs                                   139                                                                           67                                                                   
#Contigs (>= 0 bp)                         487                                                                           397                                                                  
#Contigs (>= 1000 bp)                      82                                                                            37                                                                   
Largest contig                             161449                                                                        311371                                                               
Total length                               2780753                                                                       2786228                                                              
Total length (>= 0 bp)                     2831006                                                                       2833938                                                              
Total length (>= 1000 bp)                  2756111                                                                       2774419                                                              
Reference length                           2813862                                                                       2813862                                                              
N50                                        64890                                                                         162511                                                               
NG50                                       64595                                                                         162511                                                               
N75                                        34881                                                                         80388                                                                
NG75                                       34881                                                                         79764                                                                
L50                                        14                                                                            7                                                                    
LG50                                       15                                                                            7                                                                    
L75                                        30                                                                            12                                                                   
LG75                                       30                                                                            13                                                                   
#local misassemblies                       1                                                                             23                                                                   
#misassemblies                             2                                                                             4                                                                    
#misassembled contigs                      2                                                                             3                                                                    
Misassembled contigs length                66686                                                                         310939                                                               
Misassemblies inter-contig overlap         0                                                                             584                                                                  
#unaligned contigs                         1 + 0 part                                                                    1 + 0 part                                                           
Unaligned contigs length                   5385                                                                          5385                                                                 
#ambiguously mapped contigs                12                                                                            12                                                                   
Extra bases in ambiguously mapped contigs  -8655                                                                         -8655                                                                
#N's                                       48                                                                            2980                                                                 
#N's per 100 kbp                           1.73                                                                          106.95                                                               
Genome fraction (%)                        98.276                                                                        98.423                                                               
Duplication ratio                          1.000                                                                         1.001                                                                
#genes                                     2643 + 53 part                                                                2673 + 23 part                                                       
#predicted genes (unique)                  2672                                                                          2647                                                                 
#predicted genes (>= 0 bp)                 2675                                                                          2648                                                                 
#predicted genes (>= 300 bp)               2287                                                                          2288                                                                 
#predicted genes (>= 1500 bp)              299                                                                           299                                                                  
#predicted genes (>= 3000 bp)              29                                                                            29                                                                   
#mismatches                                30                                                                            36                                                                   
#indels                                    16                                                                            371                                                                  
Indels length                              124                                                                           1800                                                                 
#mismatches per 100 kbp                    1.08                                                                          1.30                                                                 
#indels per 100 kbp                        0.58                                                                          13.40                                                                
GC (%)                                     32.69                                                                         32.70                                                                
Reference GC (%)                           32.81                                                                         32.81                                                                
Average %IDY                               98.788                                                                        98.730                                                               
Largest alignment                          161449                                                                        311358                                                               
NA50                                       64595                                                                         162464                                                               
NGA50                                      57719                                                                         162464                                                               
NA75                                       34881                                                                         79764                                                                
NGA75                                      34881                                                                         68464                                                                
LA50                                       14                                                                            7                                                                    
LGA50                                      15                                                                            7                                                                    
LA75                                       30                                                                            12                                                                   
LGA75                                      30                                                                            13                                                                   
#Mis_misassemblies                         2                                                                             4                                                                    
#Mis_relocations                           2                                                                             4                                                                    
#Mis_translocations                        0                                                                             0                                                                    
#Mis_inversions                            0                                                                             0                                                                    
#Mis_misassembled contigs                  2                                                                             3                                                                    
Mis_Misassembled contigs length            66686                                                                         310939                                                               
#Mis_local misassemblies                   1                                                                             23                                                                   
#Mis_short indels (<= 5 bp)                8                                                                             307                                                                  
#Mis_long indels (> 5 bp)                  8                                                                             64                                                                   
#Una_fully unaligned contigs               1                                                                             1                                                                    
Una_Fully unaligned length                 5385                                                                          5385                                                                 
#Una_partially unaligned contigs           0                                                                             0                                                                    
#Una_with misassembly                      0                                                                             0                                                                    
#Una_both parts are significant            0                                                                             0                                                                    
Una_Partially unaligned length             0                                                                             0                                                                    
GAGE_Contigs #                             139                                                                           67                                                                   
GAGE_Min contig                            200                                                                           200                                                                  
GAGE_Max contig                            161449                                                                        311371                                                               
GAGE_N50                                   64595 COUNT: 15                                                               162511 COUNT: 7                                                      
GAGE_Genome size                           2813862                                                                       2813862                                                              
GAGE_Assembly size                         2780753                                                                       2786228                                                              
GAGE_Chaff bases                           0                                                                             0                                                                    
GAGE_Missing reference bases               7881(0.28%)                                                                   6910(0.25%)                                                          
GAGE_Missing assembly bases                5425(0.20%)                                                                   8525(0.31%)                                                          
GAGE_Missing assembly contigs              1(0.72%)                                                                      1(1.49%)                                                             
GAGE_Duplicated reference bases            715                                                                           916                                                                  
GAGE_Compressed reference bases            42474                                                                         40619                                                                
GAGE_Bad trim                              1                                                                             187                                                                  
GAGE_Avg idy                               99.99                                                                         99.99                                                                
GAGE_SNPs                                  30                                                                            31                                                                   
GAGE_Indels < 5bp                          10                                                                            28                                                                   
GAGE_Indels >= 5                           8                                                                             50                                                                   
GAGE_Inversions                            0                                                                             0                                                                    
GAGE_Relocation                            2                                                                             18                                                                   
GAGE_Translocation                         0                                                                             0                                                                    
GAGE_Corrected contig #                    144                                                                           140                                                                  
GAGE_Corrected assembly size               2774190                                                                       2774653                                                              
GAGE_Min correct contig                    200                                                                           200                                                                  
GAGE_Max correct contig                    161449                                                                        161449                                                               
GAGE_Corrected N50                         51920 COUNT: 16                                                               57384 COUNT: 16                                                      
