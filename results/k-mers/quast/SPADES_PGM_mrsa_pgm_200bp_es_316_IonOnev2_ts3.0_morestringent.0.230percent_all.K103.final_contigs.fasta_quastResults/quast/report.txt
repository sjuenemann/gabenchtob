All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K103.final_contigs
#Contigs (>= 0 bp)             3017                                                                                             
#Contigs (>= 1000 bp)          66                                                                                               
Total length (>= 0 bp)         3558845                                                                                          
Total length (>= 1000 bp)      2770399                                                                                          
#Contigs                       3000                                                                                             
Largest contig                 271944                                                                                           
Total length                   3556694                                                                                          
Reference length               2813862                                                                                          
GC (%)                         32.56                                                                                            
Reference GC (%)               32.81                                                                                            
N50                            68133                                                                                            
NG50                           82084                                                                                            
N75                            14196                                                                                            
NG75                           46981                                                                                            
#misassemblies                 163                                                                                              
#local misassemblies           14                                                                                               
#unaligned contigs             575 + 648 part                                                                                   
Unaligned contigs length       199128                                                                                           
Genome fraction (%)            98.387                                                                                           
Duplication ratio              1.211                                                                                            
#N's per 100 kbp               0.00                                                                                             
#mismatches per 100 kbp        14.12                                                                                            
#indels per 100 kbp            29.40                                                                                            
#genes                         2646 + 49 part                                                                                   
#predicted genes (unique)      4949                                                                                             
#predicted genes (>= 0 bp)     4952                                                                                             
#predicted genes (>= 300 bp)   2339                                                                                             
#predicted genes (>= 1500 bp)  275                                                                                              
#predicted genes (>= 3000 bp)  26                                                                                               
Largest alignment              207868                                                                                           
NA50                           54830                                                                                            
NGA50                          81896                                                                                            
NA75                           10477                                                                                            
NGA75                          38517                                                                                            
