All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K103.final_contigs
#Mis_misassemblies               163                                                                                              
#Mis_relocations                 55                                                                                               
#Mis_translocations              2                                                                                                
#Mis_inversions                  106                                                                                              
#Mis_misassembled contigs        162                                                                                              
Mis_Misassembled contigs length  684308                                                                                           
#Mis_local misassemblies         14                                                                                               
#mismatches                      391                                                                                              
#indels                          814                                                                                              
#Mis_short indels (<= 5 bp)      811                                                                                              
#Mis_long indels (> 5 bp)        3                                                                                                
Indels length                    1032                                                                                             
