All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K107.final_contigs
#Contigs                                   3163                                                                                
#Contigs (>= 0 bp)                         3186                                                                                
#Contigs (>= 1000 bp)                      206                                                                                 
Largest contig                             156354                                                                              
Total length                               5197563                                                                             
Total length (>= 0 bp)                     5200304                                                                             
Total length (>= 1000 bp)                  4286696                                                                             
Reference length                           4411532                                                                             
N50                                        30721                                                                               
NG50                                       38034                                                                               
N75                                        9647                                                                                
NG75                                       20742                                                                               
L50                                        45                                                                                  
LG50                                       34                                                                                  
L75                                        115                                                                                 
LG75                                       74                                                                                  
#local misassemblies                       20                                                                                  
#misassemblies                             22                                                                                  
#misassembled contigs                      18                                                                                  
Misassembled contigs length                215744                                                                              
Misassemblies inter-contig overlap         6461                                                                                
#unaligned contigs                         293 + 34 part                                                                       
Unaligned contigs length                   89617                                                                               
#ambiguously mapped contigs                24                                                                                  
Extra bases in ambiguously mapped contigs  -14814                                                                              
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        97.430                                                                              
Duplication ratio                          1.186                                                                               
#genes                                     3888 + 175 part                                                                     
#predicted genes (unique)                  5469                                                                                
#predicted genes (>= 0 bp)                 5474                                                                                
#predicted genes (>= 300 bp)               2993                                                                                
#predicted genes (>= 1500 bp)              340                                                                                 
#predicted genes (>= 3000 bp)              31                                                                                  
#mismatches                                524                                                                                 
#indels                                    1972                                                                                
Indels length                              2295                                                                                
#mismatches per 100 kbp                    12.19                                                                               
#indels per 100 kbp                        45.88                                                                               
GC (%)                                     65.16                                                                               
Reference GC (%)                           65.61                                                                               
Average %IDY                               98.335                                                                              
Largest alignment                          156354                                                                              
NA50                                       30445                                                                               
NGA50                                      38034                                                                               
NA75                                       9313                                                                                
NGA75                                      20742                                                                               
LA50                                       46                                                                                  
LGA50                                      34                                                                                  
LA75                                       117                                                                                 
LGA75                                      74                                                                                  
#Mis_misassemblies                         22                                                                                  
#Mis_relocations                           22                                                                                  
#Mis_translocations                        0                                                                                   
#Mis_inversions                            0                                                                                   
#Mis_misassembled contigs                  18                                                                                  
Mis_Misassembled contigs length            215744                                                                              
#Mis_local misassemblies                   20                                                                                  
#Mis_short indels (<= 5 bp)                1966                                                                                
#Mis_long indels (> 5 bp)                  6                                                                                   
#Una_fully unaligned contigs               293                                                                                 
Una_Fully unaligned length                 87094                                                                               
#Una_partially unaligned contigs           34                                                                                  
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             2523                                                                                
GAGE_Contigs #                             3163                                                                                
GAGE_Min contig                            211                                                                                 
GAGE_Max contig                            156354                                                                              
GAGE_N50                                   38034 COUNT: 34                                                                     
GAGE_Genome size                           4411532                                                                             
GAGE_Assembly size                         5197563                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               59147(1.34%)                                                                        
GAGE_Missing assembly bases                26858(0.52%)                                                                        
GAGE_Missing assembly contigs              53(1.68%)                                                                           
GAGE_Duplicated reference bases            849703                                                                              
GAGE_Compressed reference bases            55886                                                                               
GAGE_Bad trim                              11201                                                                               
GAGE_Avg idy                               99.93                                                                               
GAGE_SNPs                                  358                                                                                 
GAGE_Indels < 5bp                          1772                                                                                
GAGE_Indels >= 5                           16                                                                                  
GAGE_Inversions                            1                                                                                   
GAGE_Relocation                            12                                                                                  
GAGE_Translocation                         0                                                                                   
GAGE_Corrected contig #                    316                                                                                 
GAGE_Corrected assembly size               4329096                                                                             
GAGE_Min correct contig                    206                                                                                 
GAGE_Max correct contig                    156385                                                                              
GAGE_Corrected N50                         31555 COUNT: 40                                                                     
