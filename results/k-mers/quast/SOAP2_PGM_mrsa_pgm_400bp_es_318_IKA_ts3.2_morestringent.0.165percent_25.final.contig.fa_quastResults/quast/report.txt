All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_25.final.contig
#Contigs (>= 0 bp)             514495                                                                              
#Contigs (>= 1000 bp)          0                                                                                   
Total length (>= 0 bp)         20867360                                                                            
Total length (>= 1000 bp)      0                                                                                   
#Contigs                       105                                                                                 
Largest contig                 436                                                                                 
Total length                   26976                                                                               
Reference length               2813862                                                                             
GC (%)                         32.19                                                                               
Reference GC (%)               32.81                                                                               
N50                            251                                                                                 
NG50                           None                                                                                
N75                            217                                                                                 
NG75                           None                                                                                
#misassemblies                 0                                                                                   
#local misassemblies           0                                                                                   
#unaligned contigs             99 + 0 part                                                                         
Unaligned contigs length       25530                                                                               
Genome fraction (%)            0.051                                                                               
Duplication ratio              1.004                                                                               
#N's per 100 kbp               0.00                                                                                
#mismatches per 100 kbp        69.44                                                                               
#indels per 100 kbp            694.44                                                                              
#genes                         0 + 6 part                                                                          
#predicted genes (unique)      72                                                                                  
#predicted genes (>= 0 bp)     72                                                                                  
#predicted genes (>= 300 bp)   2                                                                                   
#predicted genes (>= 1500 bp)  0                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                   
Largest alignment              314                                                                                 
NA50                           None                                                                                
NGA50                          None                                                                                
NA75                           None                                                                                
NGA75                          None                                                                                
