All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_71.final.contigs
#Contigs                                   15406                                                                                 
#Contigs (>= 0 bp)                         52362                                                                                 
#Contigs (>= 1000 bp)                      0                                                                                     
Largest contig                             451                                                                                   
Total length                               3528487                                                                               
Total length (>= 0 bp)                     9855795                                                                               
Total length (>= 1000 bp)                  0                                                                                     
Reference length                           2813862                                                                               
N50                                        223                                                                                   
NG50                                       231                                                                                   
N75                                        208                                                                                   
NG75                                       217                                                                                   
L50                                        6982                                                                                  
LG50                                       5410                                                                                  
L75                                        11076                                                                                 
LG75                                       8555                                                                                  
#local misassemblies                       0                                                                                     
#misassemblies                             157                                                                                   
#misassembled contigs                      157                                                                                   
Misassembled contigs length                35517                                                                                 
Misassemblies inter-contig overlap         272                                                                                   
#unaligned contigs                         1709 + 92 part                                                                        
Unaligned contigs length                   413371                                                                                
#ambiguously mapped contigs                66                                                                                    
Extra bases in ambiguously mapped contigs  -14595                                                                                
#N's                                       0                                                                                     
#N's per 100 kbp                           0.00                                                                                  
Genome fraction (%)                        65.809                                                                                
Duplication ratio                          1.675                                                                                 
#genes                                     101 + 2343 part                                                                       
#predicted genes (unique)                  11706                                                                                 
#predicted genes (>= 0 bp)                 11720                                                                                 
#predicted genes (>= 300 bp)               4                                                                                     
#predicted genes (>= 1500 bp)              0                                                                                     
#predicted genes (>= 3000 bp)              0                                                                                     
#mismatches                                2451                                                                                  
#indels                                    37607                                                                                 
Indels length                              38739                                                                                 
#mismatches per 100 kbp                    132.36                                                                                
#indels per 100 kbp                        2030.80                                                                               
GC (%)                                     32.12                                                                                 
Reference GC (%)                           32.81                                                                                 
Average %IDY                               97.792                                                                                
Largest alignment                          450                                                                                   
NA50                                       217                                                                                   
NGA50                                      226                                                                                   
NA75                                       204                                                                                   
NGA75                                      211                                                                                   
LA50                                       7154                                                                                  
LGA50                                      5540                                                                                  
LA75                                       11362                                                                                 
LGA75                                      8772                                                                                  
#Mis_misassemblies                         157                                                                                   
#Mis_relocations                           143                                                                                   
#Mis_translocations                        10                                                                                    
#Mis_inversions                            4                                                                                     
#Mis_misassembled contigs                  157                                                                                   
Mis_Misassembled contigs length            35517                                                                                 
#Mis_local misassemblies                   0                                                                                     
#Mis_short indels (<= 5 bp)                37607                                                                                 
#Mis_long indels (> 5 bp)                  0                                                                                     
#Una_fully unaligned contigs               1709                                                                                  
Una_Fully unaligned length                 402043                                                                                
#Una_partially unaligned contigs           92                                                                                    
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             11328                                                                                 
GAGE_Contigs #                             15406                                                                                 
GAGE_Min contig                            200                                                                                   
GAGE_Max contig                            451                                                                                   
GAGE_N50                                   231 COUNT: 5410                                                                       
GAGE_Genome size                           2813862                                                                               
GAGE_Assembly size                         3528487                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               843411(29.97%)                                                                        
GAGE_Missing assembly bases                120284(3.41%)                                                                         
GAGE_Missing assembly contigs              388(2.52%)                                                                            
GAGE_Duplicated reference bases            982476                                                                                
GAGE_Compressed reference bases            48732                                                                                 
GAGE_Bad trim                              27350                                                                                 
GAGE_Avg idy                               97.75                                                                                 
GAGE_SNPs                                  2108                                                                                  
GAGE_Indels < 5bp                          33530                                                                                 
GAGE_Indels >= 5                           12                                                                                    
GAGE_Inversions                            19                                                                                    
GAGE_Relocation                            17                                                                                    
GAGE_Translocation                         4                                                                                     
GAGE_Corrected contig #                    9963                                                                                  
GAGE_Corrected assembly size               2288372                                                                               
GAGE_Min correct contig                    200                                                                                   
GAGE_Max correct contig                    412                                                                                   
GAGE_Corrected N50                         217 COUNT: 5713                                                                       
