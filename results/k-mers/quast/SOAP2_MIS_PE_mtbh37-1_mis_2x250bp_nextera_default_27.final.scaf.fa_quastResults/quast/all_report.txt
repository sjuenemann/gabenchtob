All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_27.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_27.final.scaf
#Contigs                                   23835                                                                   23838                                                          
#Contigs (>= 0 bp)                         156238                                                                  156235                                                         
#Contigs (>= 1000 bp)                      0                                                                       0                                                              
Largest contig                             892                                                                     892                                                            
Total length                               5536763                                                                 5537949                                                        
Total length (>= 0 bp)                     23386631                                                                23386957                                                       
Total length (>= 1000 bp)                  0                                                                       0                                                              
Reference length                           4411532                                                                 4411532                                                        
N50                                        232                                                                     232                                                            
NG50                                       240                                                                     240                                                            
N75                                        215                                                                     215                                                            
NG75                                       225                                                                     225                                                            
L50                                        10932                                                                   10932                                                          
LG50                                       8546                                                                    8544                                                           
L75                                        17140                                                                   17142                                                          
LG75                                       13298                                                                   13295                                                          
#local misassemblies                       1                                                                       3                                                              
#misassemblies                             0                                                                       0                                                              
#misassembled contigs                      0                                                                       0                                                              
Misassembled contigs length                0                                                                       0                                                              
Misassemblies inter-contig overlap         8                                                                       8                                                              
#unaligned contigs                         21436 + 8 part                                                          21437 + 8 part                                                 
Unaligned contigs length                   4897206                                                                 4897509                                                        
#ambiguously mapped contigs                2                                                                       2                                                              
Extra bases in ambiguously mapped contigs  -440                                                                    -440                                                           
#N's                                       0                                                                       326                                                            
#N's per 100 kbp                           0.00                                                                    5.89                                                           
Genome fraction (%)                        14.427                                                                  14.441                                                         
Duplication ratio                          1.004                                                                   1.005                                                          
#genes                                     20 + 1706 part                                                          20 + 1708 part                                                 
#predicted genes (unique)                  11606                                                                   11609                                                          
#predicted genes (>= 0 bp)                 11606                                                                   11609                                                          
#predicted genes (>= 300 bp)               369                                                                     369                                                            
#predicted genes (>= 1500 bp)              0                                                                       0                                                              
#predicted genes (>= 3000 bp)              0                                                                       0                                                              
#mismatches                                156                                                                     156                                                            
#indels                                    7                                                                       7                                                              
Indels length                              7                                                                       7                                                              
#mismatches per 100 kbp                    24.51                                                                   24.49                                                          
#indels per 100 kbp                        1.10                                                                    1.10                                                           
GC (%)                                     71.34                                                                   71.34                                                          
Reference GC (%)                           65.61                                                                   65.61                                                          
Average %IDY                               99.972                                                                  99.972                                                         
Largest alignment                          892                                                                     892                                                            
NA50                                       None                                                                    None                                                           
NGA50                                      None                                                                    None                                                           
NA75                                       None                                                                    None                                                           
NGA75                                      None                                                                    None                                                           
LA50                                       None                                                                    None                                                           
LGA50                                      None                                                                    None                                                           
LA75                                       None                                                                    None                                                           
LGA75                                      None                                                                    None                                                           
#Mis_misassemblies                         0                                                                       0                                                              
#Mis_relocations                           0                                                                       0                                                              
#Mis_translocations                        0                                                                       0                                                              
#Mis_inversions                            0                                                                       0                                                              
#Mis_misassembled contigs                  0                                                                       0                                                              
Mis_Misassembled contigs length            0                                                                       0                                                              
#Mis_local misassemblies                   1                                                                       3                                                              
#Mis_short indels (<= 5 bp)                7                                                                       7                                                              
#Mis_long indels (> 5 bp)                  0                                                                       0                                                              
#Una_fully unaligned contigs               21436                                                                   21437                                                          
Una_Fully unaligned length                 4896556                                                                 4896859                                                        
#Una_partially unaligned contigs           8                                                                       8                                                              
#Una_with misassembly                      0                                                                       0                                                              
#Una_both parts are significant            0                                                                       0                                                              
Una_Partially unaligned length             650                                                                     650                                                            
GAGE_Contigs #                             23835                                                                   23838                                                          
GAGE_Min contig                            200                                                                     200                                                            
GAGE_Max contig                            892                                                                     892                                                            
GAGE_N50                                   240 COUNT: 8546                                                         240 COUNT: 8544                                                
GAGE_Genome size                           4411532                                                                 4411532                                                        
GAGE_Assembly size                         5536763                                                                 5537949                                                        
GAGE_Chaff bases                           0                                                                       0                                                              
GAGE_Missing reference bases               3774087(85.55%)                                                         3773227(85.53%)                                                
GAGE_Missing assembly bases                4897209(88.45%)                                                         4897535(88.44%)                                                
GAGE_Missing assembly contigs              21436(89.93%)                                                           21436(89.92%)                                                  
GAGE_Duplicated reference bases            0                                                                       0                                                              
GAGE_Compressed reference bases            542                                                                     542                                                            
GAGE_Bad trim                              653                                                                     653                                                            
GAGE_Avg idy                               99.97                                                                   99.97                                                          
GAGE_SNPs                                  151                                                                     151                                                            
GAGE_Indels < 5bp                          7                                                                       7                                                              
GAGE_Indels >= 5                           1                                                                       4                                                              
GAGE_Inversions                            0                                                                       0                                                              
GAGE_Relocation                            0                                                                       0                                                              
GAGE_Translocation                         0                                                                       0                                                              
GAGE_Corrected contig #                    2392                                                                    2392                                                           
GAGE_Corrected assembly size               638388                                                                  638388                                                         
GAGE_Min correct contig                    200                                                                     200                                                            
GAGE_Max correct contig                    892                                                                     892                                                            
GAGE_Corrected N50                         0 COUNT: 0                                                              0 COUNT: 0                                                     
