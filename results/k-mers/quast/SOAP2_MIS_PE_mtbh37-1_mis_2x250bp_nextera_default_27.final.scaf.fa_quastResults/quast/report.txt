All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_27.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_27.final.scaf
#Contigs (>= 0 bp)             156238                                                                  156235                                                         
#Contigs (>= 1000 bp)          0                                                                       0                                                              
Total length (>= 0 bp)         23386631                                                                23386957                                                       
Total length (>= 1000 bp)      0                                                                       0                                                              
#Contigs                       23835                                                                   23838                                                          
Largest contig                 892                                                                     892                                                            
Total length                   5536763                                                                 5537949                                                        
Reference length               4411532                                                                 4411532                                                        
GC (%)                         71.34                                                                   71.34                                                          
Reference GC (%)               65.61                                                                   65.61                                                          
N50                            232                                                                     232                                                            
NG50                           240                                                                     240                                                            
N75                            215                                                                     215                                                            
NG75                           225                                                                     225                                                            
#misassemblies                 0                                                                       0                                                              
#local misassemblies           1                                                                       3                                                              
#unaligned contigs             21436 + 8 part                                                          21437 + 8 part                                                 
Unaligned contigs length       4897206                                                                 4897509                                                        
Genome fraction (%)            14.427                                                                  14.441                                                         
Duplication ratio              1.004                                                                   1.005                                                          
#N's per 100 kbp               0.00                                                                    5.89                                                           
#mismatches per 100 kbp        24.51                                                                   24.49                                                          
#indels per 100 kbp            1.10                                                                    1.10                                                           
#genes                         20 + 1706 part                                                          20 + 1708 part                                                 
#predicted genes (unique)      11606                                                                   11609                                                          
#predicted genes (>= 0 bp)     11606                                                                   11609                                                          
#predicted genes (>= 300 bp)   369                                                                     369                                                            
#predicted genes (>= 1500 bp)  0                                                                       0                                                              
#predicted genes (>= 3000 bp)  0                                                                       0                                                              
Largest alignment              892                                                                     892                                                            
NA50                           None                                                                    None                                                           
NGA50                          None                                                                    None                                                           
NA75                           None                                                                    None                                                           
NGA75                          None                                                                    None                                                           
