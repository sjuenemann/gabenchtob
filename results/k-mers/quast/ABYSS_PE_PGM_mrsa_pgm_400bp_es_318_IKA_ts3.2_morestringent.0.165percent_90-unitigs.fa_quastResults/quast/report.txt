All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_90-unitigs
#Contigs (>= 0 bp)             3753                                                                              
#Contigs (>= 1000 bp)          316                                                                               
Total length (>= 0 bp)         3190755                                                                           
Total length (>= 1000 bp)      2689541                                                                           
#Contigs                       457                                                                               
Largest contig                 51551                                                                             
Total length                   2756074                                                                           
Reference length               2813862                                                                           
GC (%)                         32.60                                                                             
Reference GC (%)               32.81                                                                             
N50                            13344                                                                             
NG50                           13160                                                                             
N75                            7268                                                                              
NG75                           7013                                                                              
#misassemblies                 1                                                                                 
#local misassemblies           1                                                                                 
#unaligned contigs             0 + 0 part                                                                        
Unaligned contigs length       0                                                                                 
Genome fraction (%)            97.628                                                                            
Duplication ratio              1.002                                                                             
#N's per 100 kbp               0.00                                                                              
#mismatches per 100 kbp        1.09                                                                              
#indels per 100 kbp            4.33                                                                              
#genes                         2429 + 255 part                                                                   
#predicted genes (unique)      2895                                                                              
#predicted genes (>= 0 bp)     2898                                                                              
#predicted genes (>= 300 bp)   2386                                                                              
#predicted genes (>= 1500 bp)  262                                                                               
#predicted genes (>= 3000 bp)  23                                                                                
Largest alignment              51551                                                                             
NA50                           13344                                                                             
NGA50                          13122                                                                             
NA75                           7173                                                                              
NGA75                          6971                                                                              
