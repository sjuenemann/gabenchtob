All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_81.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_81.final.scaf
GAGE_Contigs #                   57713                                                                            57689                                                                   
GAGE_Min contig                  200                                                                              200                                                                     
GAGE_Max contig                  9084                                                                             9084                                                                    
GAGE_N50                         1607 COUNT: 1052                                                                 1609 COUNT: 1050                                                        
GAGE_Genome size                 5594470                                                                          5594470                                                                 
GAGE_Assembly size               18658570                                                                         18665623                                                                
GAGE_Chaff bases                 0                                                                                0                                                                       
GAGE_Missing reference bases     40610(0.73%)                                                                     40065(0.72%)                                                            
GAGE_Missing assembly bases      9949343(53.32%)                                                                  9952286(53.32%)                                                         
GAGE_Missing assembly contigs    38662(66.99%)                                                                    38662(67.02%)                                                           
GAGE_Duplicated reference bases  3052933                                                                          3057666                                                                 
GAGE_Compressed reference bases  299544                                                                           300059                                                                  
GAGE_Bad trim                    265148                                                                           265545                                                                  
GAGE_Avg idy                     99.89                                                                            99.89                                                                   
GAGE_SNPs                        3076                                                                             3019                                                                    
GAGE_Indels < 5bp                50                                                                               50                                                                      
GAGE_Indels >= 5                 3                                                                                37                                                                      
GAGE_Inversions                  0                                                                                0                                                                       
GAGE_Relocation                  2                                                                                2                                                                       
GAGE_Translocation               0                                                                                0                                                                       
GAGE_Corrected contig #          5732                                                                             5718                                                                    
GAGE_Corrected assembly size     5639866                                                                          5636324                                                                 
GAGE_Min correct contig          200                                                                              200                                                                     
GAGE_Max correct contig          9084                                                                             9084                                                                    
GAGE_Corrected N50               1602 COUNT: 1053                                                                 1602 COUNT: 1053                                                        
