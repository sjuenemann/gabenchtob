All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_51.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_51.final.scaf
GAGE_Contigs #                   346                                                                           69                                                                   
GAGE_Min contig                  201                                                                           201                                                                  
GAGE_Max contig                  74358                                                                         318080                                                               
GAGE_N50                         16338 COUNT: 54                                                               133202 COUNT: 7                                                      
GAGE_Genome size                 2813862                                                                       2813862                                                              
GAGE_Assembly size               2733271                                                                       2767471                                                              
GAGE_Chaff bases                 0                                                                             0                                                                    
GAGE_Missing reference bases     75475(2.68%)                                                                  56800(2.02%)                                                         
GAGE_Missing assembly bases      5657(0.21%)                                                                   24754(0.89%)                                                         
GAGE_Missing assembly contigs    1(0.29%)                                                                      1(1.45%)                                                             
GAGE_Duplicated reference bases  0                                                                             780                                                                  
GAGE_Compressed reference bases  12538                                                                         19128                                                                
GAGE_Bad trim                    135                                                                           730                                                                  
GAGE_Avg idy                     100.00                                                                        99.94                                                                
GAGE_SNPs                        45                                                                            47                                                                   
GAGE_Indels < 5bp                27                                                                            135                                                                  
GAGE_Indels >= 5                 20                                                                            352                                                                  
GAGE_Inversions                  1                                                                             2                                                                    
GAGE_Relocation                  1                                                                             5                                                                    
GAGE_Translocation               0                                                                             0                                                                    
GAGE_Corrected contig #          364                                                                           334                                                                  
GAGE_Corrected assembly size     2727306                                                                       2729423                                                              
GAGE_Min correct contig          201                                                                           201                                                                  
GAGE_Max correct contig          74354                                                                         74354                                                                
GAGE_Corrected N50               15656 COUNT: 57                                                               16329 COUNT: 52                                                      
