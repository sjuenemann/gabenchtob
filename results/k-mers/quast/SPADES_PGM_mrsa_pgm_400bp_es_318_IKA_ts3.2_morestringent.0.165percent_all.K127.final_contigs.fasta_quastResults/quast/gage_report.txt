All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K127.final_contigs
GAGE_Contigs #                   22978                                                                                       
GAGE_Min contig                  200                                                                                         
GAGE_Max contig                  326491                                                                                      
GAGE_N50                         142576 COUNT: 8                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               10658890                                                                                    
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     1254(0.04%)                                                                                 
GAGE_Missing assembly bases      349468(3.28%)                                                                               
GAGE_Missing assembly contigs    657(2.86%)                                                                                  
GAGE_Duplicated reference bases  7520567                                                                                     
GAGE_Compressed reference bases  35517                                                                                       
GAGE_Bad trim                    124059                                                                                      
GAGE_Avg idy                     99.98                                                                                       
GAGE_SNPs                        55                                                                                          
GAGE_Indels < 5bp                300                                                                                         
GAGE_Indels >= 5                 8                                                                                           
GAGE_Inversions                  0                                                                                           
GAGE_Relocation                  2                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          127                                                                                         
GAGE_Corrected assembly size     2792232                                                                                     
GAGE_Min correct contig          201                                                                                         
GAGE_Max correct contig          326518                                                                                      
GAGE_Corrected N50               80509 COUNT: 9                                                                              
