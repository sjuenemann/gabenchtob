All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_116-unitigs
#Contigs (>= 0 bp)             3617                                                                              
#Contigs (>= 1000 bp)          870                                                                               
Total length (>= 0 bp)         5766078                                                                           
Total length (>= 1000 bp)      5076024                                                                           
#Contigs                       2658                                                                              
Largest contig                 40571                                                                             
Total length                   5622443                                                                           
Reference length               5594470                                                                           
GC (%)                         50.44                                                                             
Reference GC (%)               50.48                                                                             
N50                            7738                                                                              
NG50                           7762                                                                              
N75                            3817                                                                              
NG75                           3885                                                                              
#misassemblies                 7                                                                                 
#local misassemblies           2                                                                                 
#unaligned contigs             0 + 3 part                                                                        
Unaligned contigs length       153                                                                               
Genome fraction (%)            94.356                                                                            
Duplication ratio              1.034                                                                             
#N's per 100 kbp               0.00                                                                              
#mismatches per 100 kbp        0.93                                                                              
#indels per 100 kbp            36.39                                                                             
#genes                         4309 + 873 part                                                                   
#predicted genes (unique)      7862                                                                              
#predicted genes (>= 0 bp)     7896                                                                              
#predicted genes (>= 300 bp)   5028                                                                              
#predicted genes (>= 1500 bp)  443                                                                               
#predicted genes (>= 3000 bp)  25                                                                                
Largest alignment              40571                                                                             
NA50                           7719                                                                              
NGA50                          7762                                                                              
NA75                           3816                                                                              
NGA75                          3882                                                                              
