All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_61.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_61.final.scaf
#Una_fully unaligned contigs      89722                                                                   89724                                                          
Una_Fully unaligned length        21097912                                                                21098778                                                       
#Una_partially unaligned contigs  18                                                                      18                                                             
#Una_with misassembly             0                                                                       0                                                              
#Una_both parts are significant   0                                                                       0                                                              
Una_Partially unaligned length    1859                                                                    1859                                                           
#N's                              1                                                                       544                                                            
