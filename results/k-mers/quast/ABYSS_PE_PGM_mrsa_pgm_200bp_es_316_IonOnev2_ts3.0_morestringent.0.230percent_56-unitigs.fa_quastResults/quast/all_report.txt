All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_56-unitigs
#Contigs                                   465                                                                                    
#Contigs (>= 0 bp)                         3149                                                                                   
#Contigs (>= 1000 bp)                      337                                                                                    
Largest contig                             85095                                                                                  
Total length                               2748133                                                                                
Total length (>= 0 bp)                     2973030                                                                                
Total length (>= 1000 bp)                  2692435                                                                                
Reference length                           2813862                                                                                
N50                                        12258                                                                                  
NG50                                       11711                                                                                  
N75                                        6774                                                                                   
NG75                                       6417                                                                                   
L50                                        68                                                                                     
LG50                                       71                                                                                     
L75                                        142                                                                                    
LG75                                       150                                                                                    
#local misassemblies                       3                                                                                      
#misassemblies                             1                                                                                      
#misassembled contigs                      1                                                                                      
Misassembled contigs length                20147                                                                                  
Misassemblies inter-contig overlap         468                                                                                    
#unaligned contigs                         0 + 0 part                                                                             
Unaligned contigs length                   0                                                                                      
#ambiguously mapped contigs                10                                                                                     
Extra bases in ambiguously mapped contigs  -4922                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        97.282                                                                                 
Duplication ratio                          1.002                                                                                  
#genes                                     2433 + 206 part                                                                        
#predicted genes (unique)                  2866                                                                                   
#predicted genes (>= 0 bp)                 2868                                                                                   
#predicted genes (>= 300 bp)               2350                                                                                   
#predicted genes (>= 1500 bp)              270                                                                                    
#predicted genes (>= 3000 bp)              23                                                                                     
#mismatches                                90                                                                                     
#indels                                    291                                                                                    
Indels length                              297                                                                                    
#mismatches per 100 kbp                    3.29                                                                                   
#indels per 100 kbp                        10.63                                                                                  
GC (%)                                     32.59                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               99.897                                                                                 
Largest alignment                          85095                                                                                  
NA50                                       12258                                                                                  
NGA50                                      11711                                                                                  
NA75                                       6770                                                                                   
NGA75                                      6340                                                                                   
LA50                                       68                                                                                     
LGA50                                      71                                                                                     
LA75                                       143                                                                                    
LGA75                                      151                                                                                    
#Mis_misassemblies                         1                                                                                      
#Mis_relocations                           1                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  1                                                                                      
Mis_Misassembled contigs length            20147                                                                                  
#Mis_local misassemblies                   3                                                                                      
#Mis_short indels (<= 5 bp)                291                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             465                                                                                    
GAGE_Min contig                            200                                                                                    
GAGE_Max contig                            85095                                                                                  
GAGE_N50                                   11711 COUNT: 71                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2748133                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               55362(1.97%)                                                                           
GAGE_Missing assembly bases                21(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            447                                                                                    
GAGE_Compressed reference bases            20227                                                                                  
GAGE_Bad trim                              21                                                                                     
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  67                                                                                     
GAGE_Indels < 5bp                          287                                                                                    
GAGE_Indels >= 5                           3                                                                                      
GAGE_Inversions                            0                                                                                      
GAGE_Relocation                            2                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    467                                                                                    
GAGE_Corrected assembly size               2748420                                                                                
GAGE_Min correct contig                    200                                                                                    
GAGE_Max correct contig                    85097                                                                                  
GAGE_Corrected N50                         11711 COUNT: 71                                                                        
