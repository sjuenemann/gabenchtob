All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K91.final_contigs
#Mis_misassemblies               12                                                                                 
#Mis_relocations                 12                                                                                 
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        8                                                                                  
Mis_Misassembled contigs length  121628                                                                             
#Mis_local misassemblies         23                                                                                 
#mismatches                      386                                                                                
#indels                          1487                                                                               
#Mis_short indels (<= 5 bp)      1482                                                                               
#Mis_long indels (> 5 bp)        5                                                                                  
Indels length                    1786                                                                               
