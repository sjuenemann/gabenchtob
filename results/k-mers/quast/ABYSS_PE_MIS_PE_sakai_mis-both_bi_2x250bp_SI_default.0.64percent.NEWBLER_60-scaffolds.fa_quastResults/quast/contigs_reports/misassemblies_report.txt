All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_60-scaffolds_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_60-scaffolds
#Mis_misassemblies               8                                                                                             15                                                                                   
#Mis_relocations                 8                                                                                             15                                                                                   
#Mis_translocations              0                                                                                             0                                                                                    
#Mis_inversions                  0                                                                                             0                                                                                    
#Mis_misassembled contigs        7                                                                                             13                                                                                   
Mis_Misassembled contigs length  272443                                                                                        520014                                                                               
#Mis_local misassemblies         6                                                                                             26                                                                                   
#mismatches                      1062                                                                                          1077                                                                                 
#indels                          58                                                                                            76                                                                                   
#Mis_short indels (<= 5 bp)      50                                                                                            55                                                                                   
#Mis_long indels (> 5 bp)        8                                                                                             21                                                                                   
Indels length                    264                                                                                           553                                                                                  
