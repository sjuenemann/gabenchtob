All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_all.K83.final_contigs_broken  SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_all.K83.final_contigs
#Mis_misassemblies               5                                                                                            5                                                                                   
#Mis_relocations                 3                                                                                            3                                                                                   
#Mis_translocations              0                                                                                            0                                                                                   
#Mis_inversions                  2                                                                                            2                                                                                   
#Mis_misassembled contigs        5                                                                                            5                                                                                   
Mis_Misassembled contigs length  96448                                                                                        96448                                                                               
#Mis_local misassemblies         2                                                                                            2                                                                                   
#mismatches                      1323                                                                                         1343                                                                                
#indels                          39                                                                                           39                                                                                  
#Mis_short indels (<= 5 bp)      39                                                                                           39                                                                                  
#Mis_long indels (> 5 bp)        0                                                                                            0                                                                                   
Indels length                    39                                                                                           39                                                                                  
