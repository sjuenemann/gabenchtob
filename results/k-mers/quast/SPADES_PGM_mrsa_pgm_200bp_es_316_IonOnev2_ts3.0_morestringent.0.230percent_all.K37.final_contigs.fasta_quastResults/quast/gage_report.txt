All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K37.final_contigs
GAGE_Contigs #                   238                                                                                             
GAGE_Min contig                  201                                                                                             
GAGE_Max contig                  137274                                                                                          
GAGE_N50                         35398 COUNT: 23                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2748338                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     34074(1.21%)                                                                                    
GAGE_Missing assembly bases      3673(0.13%)                                                                                     
GAGE_Missing assembly contigs    16(6.72%)                                                                                       
GAGE_Duplicated reference bases  387                                                                                             
GAGE_Compressed reference bases  37635                                                                                           
GAGE_Bad trim                    199                                                                                             
GAGE_Avg idy                     99.98                                                                                           
GAGE_SNPs                        103                                                                                             
GAGE_Indels < 5bp                396                                                                                             
GAGE_Indels >= 5                 10                                                                                              
GAGE_Inversions                  0                                                                                               
GAGE_Relocation                  4                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          235                                                                                             
GAGE_Corrected assembly size     2746527                                                                                         
GAGE_Min correct contig          201                                                                                             
GAGE_Max correct contig          137291                                                                                          
GAGE_Corrected N50               34947 COUNT: 24                                                                                 
