All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_100-unitigs
#Contigs (>= 0 bp)             2250                                                                                    
#Contigs (>= 1000 bp)          688                                                                                     
Total length (>= 0 bp)         2937453                                                                                 
Total length (>= 1000 bp)      2576655                                                                                 
#Contigs                       1059                                                                                    
Largest contig                 21584                                                                                   
Total length                   2762751                                                                                 
Reference length               2813862                                                                                 
GC (%)                         32.64                                                                                   
Reference GC (%)               32.81                                                                                   
N50                            4578                                                                                    
NG50                           4451                                                                                    
N75                            2560                                                                                    
NG75                           2441                                                                                    
#misassemblies                 39                                                                                      
#local misassemblies           1                                                                                       
#unaligned contigs             0 + 1 part                                                                              
Unaligned contigs length       51                                                                                      
Genome fraction (%)            97.167                                                                                  
Duplication ratio              1.008                                                                                   
#N's per 100 kbp               0.00                                                                                    
#mismatches per 100 kbp        1.87                                                                                    
#indels per 100 kbp            22.60                                                                                   
#genes                         2133 + 547 part                                                                         
#predicted genes (unique)      3291                                                                                    
#predicted genes (>= 0 bp)     3293                                                                                    
#predicted genes (>= 300 bp)   2521                                                                                    
#predicted genes (>= 1500 bp)  226                                                                                     
#predicted genes (>= 3000 bp)  18                                                                                      
Largest alignment              21584                                                                                   
NA50                           4535                                                                                    
NGA50                          4433                                                                                    
NA75                           2538                                                                                    
NGA75                          2438                                                                                    
