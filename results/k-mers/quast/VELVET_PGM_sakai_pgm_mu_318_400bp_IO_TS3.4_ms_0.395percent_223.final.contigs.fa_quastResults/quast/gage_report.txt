All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_223.final.contigs
GAGE_Contigs #                   1862                                                                        
GAGE_Min contig                  445                                                                         
GAGE_Max contig                  17128                                                                       
GAGE_N50                         2546 COUNT: 628                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               4529499                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1002905(17.93%)                                                             
GAGE_Missing assembly bases      465(0.01%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  19466                                                                       
GAGE_Compressed reference bases  231721                                                                      
GAGE_Bad trim                    395                                                                         
GAGE_Avg idy                     99.97                                                                       
GAGE_SNPs                        113                                                                         
GAGE_Indels < 5bp                1092                                                                        
GAGE_Indels >= 5                 2                                                                           
GAGE_Inversions                  1                                                                           
GAGE_Relocation                  6                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          1854                                                                        
GAGE_Corrected assembly size     4511406                                                                     
GAGE_Min correct contig          440                                                                         
GAGE_Max correct contig          17127                                                                       
GAGE_Corrected N50               2521 COUNT: 633                                                             
