All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K85.final_contigs
#Contigs                                   1378                                                                                      
#Contigs (>= 0 bp)                         1626                                                                                      
#Contigs (>= 1000 bp)                      182                                                                                       
Largest contig                             307372                                                                                    
Total length                               5588068                                                                                   
Total length (>= 0 bp)                     5619682                                                                                   
Total length (>= 1000 bp)                  5243367                                                                                   
Reference length                           5594470                                                                                   
N50                                        124282                                                                                    
NG50                                       124282                                                                                    
N75                                        39897                                                                                     
NG75                                       39897                                                                                     
L50                                        16                                                                                        
LG50                                       16                                                                                        
L75                                        36                                                                                        
LG75                                       36                                                                                        
#local misassemblies                       13                                                                                        
#misassemblies                             52                                                                                        
#misassembled contigs                      50                                                                                        
Misassembled contigs length                315838                                                                                    
Misassemblies inter-contig overlap         2366                                                                                      
#unaligned contigs                         165 + 201 part                                                                            
Unaligned contigs length                   51989                                                                                     
#ambiguously mapped contigs                149                                                                                       
Extra bases in ambiguously mapped contigs  -98092                                                                                    
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        94.016                                                                                    
Duplication ratio                          1.034                                                                                     
#genes                                     4908 + 208 part                                                                           
#predicted genes (unique)                  6733                                                                                      
#predicted genes (>= 0 bp)                 6740                                                                                      
#predicted genes (>= 300 bp)               4847                                                                                      
#predicted genes (>= 1500 bp)              527                                                                                       
#predicted genes (>= 3000 bp)              42                                                                                        
#mismatches                                177                                                                                       
#indels                                    1714                                                                                      
Indels length                              1822                                                                                      
#mismatches per 100 kbp                    3.37                                                                                      
#indels per 100 kbp                        32.59                                                                                     
GC (%)                                     50.26                                                                                     
Reference GC (%)                           50.48                                                                                     
Average %IDY                               98.617                                                                                    
Largest alignment                          307372                                                                                    
NA50                                       124282                                                                                    
NGA50                                      124282                                                                                    
NA75                                       39897                                                                                     
NGA75                                      36890                                                                                     
LA50                                       16                                                                                        
LGA50                                      16                                                                                        
LA75                                       36                                                                                        
LGA75                                      37                                                                                        
#Mis_misassemblies                         52                                                                                        
#Mis_relocations                           7                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            45                                                                                        
#Mis_misassembled contigs                  50                                                                                        
Mis_Misassembled contigs length            315838                                                                                    
#Mis_local misassemblies                   13                                                                                        
#Mis_short indels (<= 5 bp)                1711                                                                                      
#Mis_long indels (> 5 bp)                  3                                                                                         
#Una_fully unaligned contigs               165                                                                                       
Una_Fully unaligned length                 40499                                                                                     
#Una_partially unaligned contigs           201                                                                                       
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             11490                                                                                     
GAGE_Contigs #                             1378                                                                                      
GAGE_Min contig                            200                                                                                       
GAGE_Max contig                            307372                                                                                    
GAGE_N50                                   124282 COUNT: 16                                                                          
GAGE_Genome size                           5594470                                                                                   
GAGE_Assembly size                         5588068                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               8354(0.15%)                                                                               
GAGE_Missing assembly bases                17537(0.31%)                                                                              
GAGE_Missing assembly contigs              25(1.81%)                                                                                 
GAGE_Duplicated reference bases            212038                                                                                    
GAGE_Compressed reference bases            287966                                                                                    
GAGE_Bad trim                              11343                                                                                     
GAGE_Avg idy                               99.96                                                                                     
GAGE_SNPs                                  239                                                                                       
GAGE_Indels < 5bp                          1752                                                                                      
GAGE_Indels >= 5                           11                                                                                        
GAGE_Inversions                            0                                                                                         
GAGE_Relocation                            7                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    466                                                                                       
GAGE_Corrected assembly size               5362920                                                                                   
GAGE_Min correct contig                    200                                                                                       
GAGE_Max correct contig                    226961                                                                                    
GAGE_Corrected N50                         102106 COUNT: 19                                                                          
