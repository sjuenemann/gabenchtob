All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_137.final.contigs
GAGE_Contigs #                   4190                                                                           
GAGE_Min contig                  273                                                                            
GAGE_Max contig                  2497                                                                           
GAGE_N50                         0 COUNT: 0                                                                     
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               1892277                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     2685649(60.88%)                                                                
GAGE_Missing assembly bases      71(0.00%)                                                                      
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  90094                                                                          
GAGE_Compressed reference bases  23706                                                                          
GAGE_Bad trim                    58                                                                             
GAGE_Avg idy                     99.90                                                                          
GAGE_SNPs                        99                                                                             
GAGE_Indels < 5bp                1464                                                                           
GAGE_Indels >= 5                 2                                                                              
GAGE_Inversions                  35                                                                             
GAGE_Relocation                  21                                                                             
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          3912                                                                           
GAGE_Corrected assembly size     1788428                                                                        
GAGE_Min correct contig          202                                                                            
GAGE_Max correct contig          2497                                                                           
GAGE_Corrected N50               0 COUNT: 0                                                                     
