All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K113.final_contigs
#Una_fully unaligned contigs      275                                                                                 
Una_Fully unaligned length        83423                                                                               
#Una_partially unaligned contigs  40                                                                                  
#Una_with misassembly             0                                                                                   
#Una_both parts are significant   0                                                                                   
Una_Partially unaligned length    2719                                                                                
#N's                              0                                                                                   
