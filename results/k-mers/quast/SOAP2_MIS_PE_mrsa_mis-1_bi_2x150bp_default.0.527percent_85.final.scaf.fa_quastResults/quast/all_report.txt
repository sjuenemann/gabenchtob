All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_85.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_85.final.scaf
#Contigs                                   141                                                                           57                                                                   
#Contigs (>= 0 bp)                         615                                                                           510                                                                  
#Contigs (>= 1000 bp)                      80                                                                            30                                                                   
Largest contig                             207780                                                                        524564                                                               
Total length                               2775363                                                                       2781860                                                              
Total length (>= 0 bp)                     2843296                                                                       2846939                                                              
Total length (>= 1000 bp)                  2749530                                                                       2770616                                                              
Reference length                           2813862                                                                       2813862                                                              
N50                                        66423                                                                         237701                                                               
NG50                                       66423                                                                         172415                                                               
N75                                        33696                                                                         109050                                                               
NG75                                       32691                                                                         109050                                                               
L50                                        13                                                                            4                                                                    
LG50                                       13                                                                            5                                                                    
L75                                        27                                                                            9                                                                    
LG75                                       28                                                                            9                                                                    
#local misassemblies                       2                                                                             23                                                                   
#misassemblies                             1                                                                             3                                                                    
#misassembled contigs                      1                                                                             2                                                                    
Misassembled contigs length                66423                                                                         308161                                                               
Misassemblies inter-contig overlap         0                                                                             480                                                                  
#unaligned contigs                         1 + 0 part                                                                    1 + 1 part                                                           
Unaligned contigs length                   5385                                                                          6441                                                                 
#ambiguously mapped contigs                16                                                                            16                                                                   
Extra bases in ambiguously mapped contigs  -8569                                                                         -8569                                                                
#N's                                       34                                                                            3677                                                                 
#N's per 100 kbp                           1.23                                                                          132.18                                                               
Genome fraction (%)                        98.117                                                                        98.241                                                               
Duplication ratio                          1.000                                                                         1.001                                                                
#genes                                     2637 + 54 part                                                                2672 + 19 part                                                       
#predicted genes (unique)                  2667                                                                          2644                                                                 
#predicted genes (>= 0 bp)                 2669                                                                          2644                                                                 
#predicted genes (>= 300 bp)               2285                                                                          2286                                                                 
#predicted genes (>= 1500 bp)              299                                                                           299                                                                  
#predicted genes (>= 3000 bp)              27                                                                            28                                                                   
#mismatches                                33                                                                            37                                                                   
#indels                                    13                                                                            608                                                                  
Indels length                              60                                                                            2272                                                                 
#mismatches per 100 kbp                    1.20                                                                          1.34                                                                 
#indels per 100 kbp                        0.47                                                                          21.99                                                                
GC (%)                                     32.69                                                                         32.69                                                                
Reference GC (%)                           32.81                                                                         32.81                                                                
Average %IDY                               98.909                                                                        98.785                                                               
Largest alignment                          207780                                                                        524424                                                               
NA50                                       64987                                                                         172415                                                               
NGA50                                      64987                                                                         172415                                                               
NA75                                       33696                                                                         81905                                                                
NGA75                                      32691                                                                         81905                                                                
LA50                                       13                                                                            5                                                                    
LGA50                                      13                                                                            5                                                                    
LA75                                       27                                                                            10                                                                   
LGA75                                      28                                                                            10                                                                   
#Mis_misassemblies                         1                                                                             3                                                                    
#Mis_relocations                           1                                                                             3                                                                    
#Mis_translocations                        0                                                                             0                                                                    
#Mis_inversions                            0                                                                             0                                                                    
#Mis_misassembled contigs                  1                                                                             2                                                                    
Mis_Misassembled contigs length            66423                                                                         308161                                                               
#Mis_local misassemblies                   2                                                                             23                                                                   
#Mis_short indels (<= 5 bp)                9                                                                             547                                                                  
#Mis_long indels (> 5 bp)                  4                                                                             61                                                                   
#Una_fully unaligned contigs               1                                                                             1                                                                    
Una_Fully unaligned length                 5385                                                                          5385                                                                 
#Una_partially unaligned contigs           0                                                                             1                                                                    
#Una_with misassembly                      0                                                                             0                                                                    
#Una_both parts are significant            0                                                                             1                                                                    
Una_Partially unaligned length             0                                                                             1056                                                                 
GAGE_Contigs #                             141                                                                           57                                                                   
GAGE_Min contig                            200                                                                           210                                                                  
GAGE_Max contig                            207780                                                                        524564                                                               
GAGE_N50                                   66423 COUNT: 13                                                               172415 COUNT: 5                                                      
GAGE_Genome size                           2813862                                                                       2813862                                                              
GAGE_Assembly size                         2775363                                                                       2781860                                                              
GAGE_Chaff bases                           0                                                                             0                                                                    
GAGE_Missing reference bases               10077(0.36%)                                                                  9562(0.34%)                                                          
GAGE_Missing assembly bases                5414(0.20%)                                                                   9585(0.34%)                                                          
GAGE_Missing assembly contigs              1(0.71%)                                                                      1(1.75%)                                                             
GAGE_Duplicated reference bases            406                                                                           908                                                                  
GAGE_Compressed reference bases            43697                                                                         41934                                                                
GAGE_Bad trim                              4                                                                             267                                                                  
GAGE_Avg idy                               99.99                                                                         99.98                                                                
GAGE_SNPs                                  30                                                                            30                                                                   
GAGE_Indels < 5bp                          13                                                                            32                                                                   
GAGE_Indels >= 5                           3                                                                             51                                                                   
GAGE_Inversions                            0                                                                             0                                                                    
GAGE_Relocation                            1                                                                             10                                                                   
GAGE_Translocation                         0                                                                             0                                                                    
GAGE_Corrected contig #                    143                                                                           138                                                                  
GAGE_Corrected assembly size               2769260                                                                       2769209                                                              
GAGE_Min correct contig                    200                                                                           200                                                                  
GAGE_Max correct contig                    207780                                                                        207780                                                               
GAGE_Corrected N50                         64987 COUNT: 13                                                               65099 COUNT: 13                                                      
