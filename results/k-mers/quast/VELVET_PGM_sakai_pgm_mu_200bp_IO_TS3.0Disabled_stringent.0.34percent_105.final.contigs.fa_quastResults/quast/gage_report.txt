All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_105.final.contigs
GAGE_Contigs #                   3392                                                                                  
GAGE_Min contig                  209                                                                                   
GAGE_Max contig                  13439                                                                                 
GAGE_N50                         2349 COUNT: 702                                                                       
GAGE_Genome size                 5594470                                                                               
GAGE_Assembly size               5284886                                                                               
GAGE_Chaff bases                 0                                                                                     
GAGE_Missing reference bases     343379(6.14%)                                                                         
GAGE_Missing assembly bases      3535(0.07%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                              
GAGE_Duplicated reference bases  63533                                                                                 
GAGE_Compressed reference bases  239108                                                                                
GAGE_Bad trim                    1833                                                                                  
GAGE_Avg idy                     99.96                                                                                 
GAGE_SNPs                        103                                                                                   
GAGE_Indels < 5bp                1496                                                                                  
GAGE_Indels >= 5                 138                                                                                   
GAGE_Inversions                  6                                                                                     
GAGE_Relocation                  36                                                                                    
GAGE_Translocation               0                                                                                     
GAGE_Corrected contig #          3431                                                                                  
GAGE_Corrected assembly size     5219115                                                                               
GAGE_Min correct contig          202                                                                                   
GAGE_Max correct contig          11454                                                                                 
GAGE_Corrected N50               2171 COUNT: 774                                                                       
