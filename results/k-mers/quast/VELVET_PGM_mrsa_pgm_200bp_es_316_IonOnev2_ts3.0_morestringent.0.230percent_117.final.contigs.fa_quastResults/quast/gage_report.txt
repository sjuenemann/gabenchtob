All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_117.final.contigs
GAGE_Contigs #                   2519                                                                                        
GAGE_Min contig                  233                                                                                         
GAGE_Max contig                  7604                                                                                        
GAGE_N50                         1021 COUNT: 834                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               2437099                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     436611(15.52%)                                                                              
GAGE_Missing assembly bases      2668(0.11%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  4543                                                                                        
GAGE_Compressed reference bases  36218                                                                                       
GAGE_Bad trim                    2668                                                                                        
GAGE_Avg idy                     99.92                                                                                       
GAGE_SNPs                        99                                                                                          
GAGE_Indels < 5bp                1266                                                                                        
GAGE_Indels >= 5                 1                                                                                           
GAGE_Inversions                  8                                                                                           
GAGE_Relocation                  0                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          2513                                                                                        
GAGE_Corrected assembly size     2430275                                                                                     
GAGE_Min correct contig          232                                                                                         
GAGE_Max correct contig          7603                                                                                        
GAGE_Corrected N50               1016 COUNT: 836                                                                             
