All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_117.final.contigs
#Mis_misassemblies               26                                                                                          
#Mis_relocations                 9                                                                                           
#Mis_translocations              0                                                                                           
#Mis_inversions                  17                                                                                          
#Mis_misassembled contigs        26                                                                                          
Mis_Misassembled contigs length  29251                                                                                       
#Mis_local misassemblies         3                                                                                           
#mismatches                      143                                                                                         
#indels                          1478                                                                                        
#Mis_short indels (<= 5 bp)      1478                                                                                        
#Mis_long indels (> 5 bp)        0                                                                                           
Indels length                    1508                                                                                        
