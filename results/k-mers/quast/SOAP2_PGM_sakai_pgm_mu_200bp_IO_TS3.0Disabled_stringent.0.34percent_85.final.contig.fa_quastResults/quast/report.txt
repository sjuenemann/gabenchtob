All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_85.final.contig
#Contigs (>= 0 bp)             49633                                                                              
#Contigs (>= 1000 bp)          889                                                                                
Total length (>= 0 bp)         10922220                                                                           
Total length (>= 1000 bp)      1227848                                                                            
#Contigs                       9312                                                                               
Largest contig                 3223                                                                               
Total length                   4898642                                                                            
Reference length               5594470                                                                            
GC (%)                         50.10                                                                              
Reference GC (%)               50.48                                                                              
N50                            644                                                                                
NG50                           562                                                                                
N75                            391                                                                                
NG75                           297                                                                                
#misassemblies                 14                                                                                 
#local misassemblies           3                                                                                  
#unaligned contigs             20 + 39 part                                                                       
Unaligned contigs length       7370                                                                               
Genome fraction (%)            84.362                                                                             
Duplication ratio              1.028                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        2.29                                                                               
#indels per 100 kbp            30.85                                                                              
#genes                         989 + 4022 part                                                                    
#predicted genes (unique)      11568                                                                              
#predicted genes (>= 0 bp)     11578                                                                              
#predicted genes (>= 300 bp)   5561                                                                               
#predicted genes (>= 1500 bp)  33                                                                                 
#predicted genes (>= 3000 bp)  0                                                                                  
Largest alignment              3223                                                                               
NA50                           644                                                                                
NGA50                          561                                                                                
NA75                           389                                                                                
NGA75                          293                                                                                
