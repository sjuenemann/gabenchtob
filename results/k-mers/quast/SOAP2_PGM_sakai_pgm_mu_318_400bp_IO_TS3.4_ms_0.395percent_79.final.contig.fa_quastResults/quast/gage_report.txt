All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_79.final.contig
GAGE_Contigs #                   12267                                                                    
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  1216                                                                     
GAGE_N50                         238 COUNT: 9506                                                          
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               3397571                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     2712600(48.49%)                                                          
GAGE_Missing assembly bases      53632(1.58%)                                                             
GAGE_Missing assembly contigs    123(1.00%)                                                               
GAGE_Duplicated reference bases  309377                                                                   
GAGE_Compressed reference bases  139003                                                                   
GAGE_Bad trim                    19007                                                                    
GAGE_Avg idy                     99.12                                                                    
GAGE_SNPs                        711                                                                      
GAGE_Indels < 5bp                19761                                                                    
GAGE_Indels >= 5                 12                                                                       
GAGE_Inversions                  47                                                                       
GAGE_Relocation                  24                                                                       
GAGE_Translocation               8                                                                        
GAGE_Corrected contig #          10648                                                                    
GAGE_Corrected assembly size     2967516                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          1216                                                                     
GAGE_Corrected N50               210 COUNT: 9818                                                          
