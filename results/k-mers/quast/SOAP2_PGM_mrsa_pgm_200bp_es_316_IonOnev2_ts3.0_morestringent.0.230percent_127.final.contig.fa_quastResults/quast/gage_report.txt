All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_127.final.contig
GAGE_Contigs #                   3142                                                                                      
GAGE_Min contig                  200                                                                                       
GAGE_Max contig                  3935                                                                                      
GAGE_N50                         633 COUNT: 1376                                                                           
GAGE_Genome size                 2813862                                                                                   
GAGE_Assembly size               2216934                                                                                   
GAGE_Chaff bases                 0                                                                                         
GAGE_Missing reference bases     693360(24.64%)                                                                            
GAGE_Missing assembly bases      1164(0.05%)                                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                                                  
GAGE_Duplicated reference bases  42467                                                                                     
GAGE_Compressed reference bases  51492                                                                                     
GAGE_Bad trim                    1164                                                                                      
GAGE_Avg idy                     99.92                                                                                     
GAGE_SNPs                        74                                                                                        
GAGE_Indels < 5bp                1174                                                                                      
GAGE_Indels >= 5                 1                                                                                         
GAGE_Inversions                  1                                                                                         
GAGE_Relocation                  0                                                                                         
GAGE_Translocation               0                                                                                         
GAGE_Corrected contig #          2972                                                                                      
GAGE_Corrected assembly size     2173345                                                                                   
GAGE_Min correct contig          206                                                                                       
GAGE_Max correct contig          3935                                                                                      
GAGE_Corrected N50               633 COUNT: 1376                                                                           
