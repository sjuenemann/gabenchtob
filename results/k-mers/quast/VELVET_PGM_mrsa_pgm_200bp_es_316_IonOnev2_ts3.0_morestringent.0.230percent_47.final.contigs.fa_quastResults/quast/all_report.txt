All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_47.final.contigs
#Contigs                                   4992                                                                                       
#Contigs (>= 0 bp)                         8802                                                                                       
#Contigs (>= 1000 bp)                      202                                                                                        
Largest contig                             2336                                                                                       
Total length                               2221976                                                                                    
Total length (>= 0 bp)                     2736231                                                                                    
Total length (>= 1000 bp)                  258694                                                                                     
Reference length                           2813862                                                                                    
N50                                        497                                                                                        
NG50                                       405                                                                                        
N75                                        336                                                                                        
NG75                                       225                                                                                        
L50                                        1482                                                                                       
LG50                                       2143                                                                                       
L75                                        2851                                                                                       
LG75                                       4467                                                                                       
#local misassemblies                       2                                                                                          
#misassemblies                             0                                                                                          
#misassembled contigs                      0                                                                                          
Misassembled contigs length                0                                                                                          
Misassemblies inter-contig overlap         403                                                                                        
#unaligned contigs                         2 + 5 part                                                                                 
Unaligned contigs length                   833                                                                                        
#ambiguously mapped contigs                4                                                                                          
Extra bases in ambiguously mapped contigs  -1489                                                                                      
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        78.127                                                                                     
Duplication ratio                          1.010                                                                                      
#genes                                     304 + 2178 part                                                                            
#predicted genes (unique)                  5557                                                                                       
#predicted genes (>= 0 bp)                 5557                                                                                       
#predicted genes (>= 300 bp)               2560                                                                                       
#predicted genes (>= 1500 bp)              8                                                                                          
#predicted genes (>= 3000 bp)              0                                                                                          
#mismatches                                106                                                                                        
#indels                                    707                                                                                        
Indels length                              774                                                                                        
#mismatches per 100 kbp                    4.82                                                                                       
#indels per 100 kbp                        32.16                                                                                      
GC (%)                                     32.76                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.952                                                                                     
Largest alignment                          2336                                                                                       
NA50                                       496                                                                                        
NGA50                                      405                                                                                        
NA75                                       336                                                                                        
NGA75                                      225                                                                                        
LA50                                       1482                                                                                       
LGA50                                      2144                                                                                       
LA75                                       2852                                                                                       
LGA75                                      4471                                                                                       
#Mis_misassemblies                         0                                                                                          
#Mis_relocations                           0                                                                                          
#Mis_translocations                        0                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  0                                                                                          
Mis_Misassembled contigs length            0                                                                                          
#Mis_local misassemblies                   2                                                                                          
#Mis_short indels (<= 5 bp)                707                                                                                        
#Mis_long indels (> 5 bp)                  0                                                                                          
#Una_fully unaligned contigs               2                                                                                          
Una_Fully unaligned length                 668                                                                                        
#Una_partially unaligned contigs           5                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             165                                                                                        
GAGE_Contigs #                             4992                                                                                       
GAGE_Min contig                            200                                                                                        
GAGE_Max contig                            2336                                                                                       
GAGE_N50                                   405 COUNT: 2143                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         2221976                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               609567(21.66%)                                                                             
GAGE_Missing assembly bases                340(0.02%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                   
GAGE_Duplicated reference bases            201                                                                                        
GAGE_Compressed reference bases            4096                                                                                       
GAGE_Bad trim                              340                                                                                        
GAGE_Avg idy                               99.96                                                                                      
GAGE_SNPs                                  78                                                                                         
GAGE_Indels < 5bp                          752                                                                                        
GAGE_Indels >= 5                           3                                                                                          
GAGE_Inversions                            0                                                                                          
GAGE_Relocation                            0                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    4991                                                                                       
GAGE_Corrected assembly size               2221805                                                                                    
GAGE_Min correct contig                    200                                                                                        
GAGE_Max correct contig                    2336                                                                                       
GAGE_Corrected N50                         405 COUNT: 2143                                                                            
