All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K61.final_contigs
#Contigs                                   737                                                                                       
#Contigs (>= 0 bp)                         1274                                                                                      
#Contigs (>= 1000 bp)                      198                                                                                       
Largest contig                             260856                                                                                    
Total length                               5366589                                                                                   
Total length (>= 0 bp)                     5421050                                                                                   
Total length (>= 1000 bp)                  5176540                                                                                   
Reference length                           5594470                                                                                   
N50                                        97769                                                                                     
NG50                                       95331                                                                                     
N75                                        32824                                                                                     
NG75                                       28387                                                                                     
L50                                        18                                                                                        
LG50                                       19                                                                                        
L75                                        41                                                                                        
LG75                                       47                                                                                        
#local misassemblies                       12                                                                                        
#misassemblies                             3                                                                                         
#misassembled contigs                      3                                                                                         
Misassembled contigs length                304080                                                                                    
Misassemblies inter-contig overlap         1723                                                                                      
#unaligned contigs                         164 + 0 part                                                                              
Unaligned contigs length                   37231                                                                                     
#ambiguously mapped contigs                140                                                                                       
Extra bases in ambiguously mapped contigs  -95614                                                                                    
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        93.425                                                                                    
Duplication ratio                          1.002                                                                                     
#genes                                     4808 + 235 part                                                                           
#predicted genes (unique)                  6103                                                                                      
#predicted genes (>= 0 bp)                 6107                                                                                      
#predicted genes (>= 300 bp)               4764                                                                                      
#predicted genes (>= 1500 bp)              532                                                                                       
#predicted genes (>= 3000 bp)              44                                                                                        
#mismatches                                165                                                                                       
#indels                                    1454                                                                                      
Indels length                              1513                                                                                      
#mismatches per 100 kbp                    3.16                                                                                      
#indels per 100 kbp                        27.82                                                                                     
GC (%)                                     50.27                                                                                     
Reference GC (%)                           50.48                                                                                     
Average %IDY                               99.521                                                                                    
Largest alignment                          247602                                                                                    
NA50                                       97769                                                                                     
NGA50                                      95331                                                                                     
NA75                                       30741                                                                                     
NGA75                                      28380                                                                                     
LA50                                       18                                                                                        
LGA50                                      19                                                                                        
LA75                                       43                                                                                        
LGA75                                      48                                                                                        
#Mis_misassemblies                         3                                                                                         
#Mis_relocations                           3                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            0                                                                                         
#Mis_misassembled contigs                  3                                                                                         
Mis_Misassembled contigs length            304080                                                                                    
#Mis_local misassemblies                   12                                                                                        
#Mis_short indels (<= 5 bp)                1453                                                                                      
#Mis_long indels (> 5 bp)                  1                                                                                         
#Una_fully unaligned contigs               164                                                                                       
Una_Fully unaligned length                 37231                                                                                     
#Una_partially unaligned contigs           0                                                                                         
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             0                                                                                         
GAGE_Contigs #                             737                                                                                       
GAGE_Min contig                            201                                                                                       
GAGE_Max contig                            260856                                                                                    
GAGE_N50                                   95331 COUNT: 19                                                                           
GAGE_Genome size                           5594470                                                                                   
GAGE_Assembly size                         5366589                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               31784(0.57%)                                                                              
GAGE_Missing assembly bases                10241(0.19%)                                                                              
GAGE_Missing assembly contigs              34(4.61%)                                                                                 
GAGE_Duplicated reference bases            27411                                                                                     
GAGE_Compressed reference bases            271190                                                                                    
GAGE_Bad trim                              2450                                                                                      
GAGE_Avg idy                               99.96                                                                                     
GAGE_SNPs                                  223                                                                                       
GAGE_Indels < 5bp                          1553                                                                                      
GAGE_Indels >= 5                           12                                                                                        
GAGE_Inversions                            1                                                                                         
GAGE_Relocation                            8                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    590                                                                                       
GAGE_Corrected assembly size               5333058                                                                                   
GAGE_Min correct contig                    201                                                                                       
GAGE_Max correct contig                    226937                                                                                    
GAGE_Corrected N50                         67188 COUNT: 22                                                                           
