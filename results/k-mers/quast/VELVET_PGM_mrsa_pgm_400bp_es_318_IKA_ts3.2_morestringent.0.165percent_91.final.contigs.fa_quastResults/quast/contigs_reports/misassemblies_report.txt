All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_91.final.contigs
#Mis_misassemblies               440                                                                                   
#Mis_relocations                 406                                                                                   
#Mis_translocations              30                                                                                    
#Mis_inversions                  4                                                                                     
#Mis_misassembled contigs        440                                                                                   
Mis_Misassembled contigs length  109507                                                                                
#Mis_local misassemblies         2                                                                                     
#mismatches                      2147                                                                                  
#indels                          34619                                                                                 
#Mis_short indels (<= 5 bp)      34617                                                                                 
#Mis_long indels (> 5 bp)        2                                                                                     
Indels length                    35510                                                                                 
