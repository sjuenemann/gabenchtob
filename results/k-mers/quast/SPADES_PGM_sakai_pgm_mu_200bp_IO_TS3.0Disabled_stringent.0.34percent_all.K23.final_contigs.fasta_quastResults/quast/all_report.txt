All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K23.final_contigs
#Contigs                                   1019                                                                                      
#Contigs (>= 0 bp)                         3067                                                                                      
#Contigs (>= 1000 bp)                      548                                                                                       
Largest contig                             66915                                                                                     
Total length                               5196376                                                                                   
Total length (>= 0 bp)                     5311733                                                                                   
Total length (>= 1000 bp)                  4992005                                                                                   
Reference length                           5594470                                                                                   
N50                                        14074                                                                                     
NG50                                       13008                                                                                     
N75                                        7955                                                                                      
NG75                                       6438                                                                                      
L50                                        109                                                                                       
LG50                                       123                                                                                       
L75                                        232                                                                                       
LG75                                       274                                                                                       
#local misassemblies                       6                                                                                         
#misassemblies                             3                                                                                         
#misassembled contigs                      3                                                                                         
Misassembled contigs length                25283                                                                                     
Misassemblies inter-contig overlap         530                                                                                       
#unaligned contigs                         9 + 0 part                                                                                
Unaligned contigs length                   2064                                                                                      
#ambiguously mapped contigs                124                                                                                       
Extra bases in ambiguously mapped contigs  -84325                                                                                    
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        91.241                                                                                    
Duplication ratio                          1.001                                                                                     
#genes                                     4515 + 346 part                                                                           
#predicted genes (unique)                  5938                                                                                      
#predicted genes (>= 0 bp)                 5938                                                                                      
#predicted genes (>= 300 bp)               4706                                                                                      
#predicted genes (>= 1500 bp)              505                                                                                       
#predicted genes (>= 3000 bp)              38                                                                                        
#mismatches                                227                                                                                       
#indels                                    1140                                                                                      
Indels length                              1191                                                                                      
#mismatches per 100 kbp                    4.45                                                                                      
#indels per 100 kbp                        22.33                                                                                     
GC (%)                                     50.22                                                                                     
Reference GC (%)                           50.48                                                                                     
Average %IDY                               99.612                                                                                    
Largest alignment                          66915                                                                                     
NA50                                       14074                                                                                     
NGA50                                      12881                                                                                     
NA75                                       7903                                                                                      
NGA75                                      6358                                                                                      
LA50                                       109                                                                                       
LGA50                                      124                                                                                       
LA75                                       233                                                                                       
LGA75                                      275                                                                                       
#Mis_misassemblies                         3                                                                                         
#Mis_relocations                           3                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            0                                                                                         
#Mis_misassembled contigs                  3                                                                                         
Mis_Misassembled contigs length            25283                                                                                     
#Mis_local misassemblies                   6                                                                                         
#Mis_short indels (<= 5 bp)                1139                                                                                      
#Mis_long indels (> 5 bp)                  1                                                                                         
#Una_fully unaligned contigs               9                                                                                         
Una_Fully unaligned length                 2064                                                                                      
#Una_partially unaligned contigs           0                                                                                         
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             0                                                                                         
GAGE_Contigs #                             1019                                                                                      
GAGE_Min contig                            200                                                                                       
GAGE_Max contig                            66915                                                                                     
GAGE_N50                                   13008 COUNT: 123                                                                          
GAGE_Genome size                           5594470                                                                                   
GAGE_Assembly size                         5196376                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               170247(3.04%)                                                                             
GAGE_Missing assembly bases                2172(0.04%)                                                                               
GAGE_Missing assembly contigs              9(0.88%)                                                                                  
GAGE_Duplicated reference bases            0                                                                                         
GAGE_Compressed reference bases            239570                                                                                    
GAGE_Bad trim                              108                                                                                       
GAGE_Avg idy                               99.97                                                                                     
GAGE_SNPs                                  525                                                                                       
GAGE_Indels < 5bp                          1203                                                                                      
GAGE_Indels >= 5                           6                                                                                         
GAGE_Inversions                            0                                                                                         
GAGE_Relocation                            4                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    1021                                                                                      
GAGE_Corrected assembly size               5196256                                                                                   
GAGE_Min correct contig                    200                                                                                       
GAGE_Max correct contig                    66922                                                                                     
GAGE_Corrected N50                         12691 COUNT: 126                                                                          
