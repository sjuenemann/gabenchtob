All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K61.final_contigs
GAGE_Contigs #                   872                                                                                             
GAGE_Min contig                  206                                                                                             
GAGE_Max contig                  148392                                                                                          
GAGE_N50                         68823 COUNT: 14                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2930792                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     10767(0.38%)                                                                                    
GAGE_Missing assembly bases      71738(2.45%)                                                                                    
GAGE_Missing assembly contigs    272(31.19%)                                                                                     
GAGE_Duplicated reference bases  90777                                                                                           
GAGE_Compressed reference bases  42625                                                                                           
GAGE_Bad trim                    8843                                                                                            
GAGE_Avg idy                     99.97                                                                                           
GAGE_SNPs                        76                                                                                              
GAGE_Indels < 5bp                492                                                                                             
GAGE_Indels >= 5                 13                                                                                              
GAGE_Inversions                  2                                                                                               
GAGE_Relocation                  5                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          190                                                                                             
GAGE_Corrected assembly size     2771713                                                                                         
GAGE_Min correct contig          206                                                                                             
GAGE_Max correct contig          148091                                                                                          
GAGE_Corrected N50               46445 COUNT: 18                                                                                 
