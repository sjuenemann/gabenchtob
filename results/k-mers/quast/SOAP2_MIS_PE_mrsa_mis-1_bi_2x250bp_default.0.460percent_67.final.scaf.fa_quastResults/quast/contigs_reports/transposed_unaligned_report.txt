All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                      #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's 
SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_67.final.scaf_broken  6022                          1458282                     509                               0                      1                                44144                           772  
SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_67.final.scaf         6082                          1527330                     522                               0                      11                               57558                           18220
