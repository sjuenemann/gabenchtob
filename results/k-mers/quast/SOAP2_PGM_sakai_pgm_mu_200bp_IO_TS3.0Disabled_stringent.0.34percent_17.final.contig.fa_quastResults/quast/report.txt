All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_17.final.contig
#Contigs (>= 0 bp)             432925                                                                             
#Contigs (>= 1000 bp)          0                                                                                  
Total length (>= 0 bp)         15139693                                                                           
Total length (>= 1000 bp)      0                                                                                  
#Contigs                       890                                                                                
Largest contig                 535                                                                                
Total length                   215686                                                                             
Reference length               5594470                                                                            
GC (%)                         50.75                                                                              
Reference GC (%)               50.48                                                                              
N50                            236                                                                                
NG50                           None                                                                               
N75                            214                                                                                
NG75                           None                                                                               
#misassemblies                 0                                                                                  
#local misassemblies           0                                                                                  
#unaligned contigs             7 + 1 part                                                                         
Unaligned contigs length       1564                                                                               
Genome fraction (%)            3.826                                                                              
Duplication ratio              1.000                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        4.67                                                                               
#indels per 100 kbp            44.38                                                                              
#genes                         4 + 746 part                                                                       
#predicted genes (unique)      885                                                                                
#predicted genes (>= 0 bp)     885                                                                                
#predicted genes (>= 300 bp)   60                                                                                 
#predicted genes (>= 1500 bp)  0                                                                                  
#predicted genes (>= 3000 bp)  0                                                                                  
Largest alignment              535                                                                                
NA50                           236                                                                                
NGA50                          None                                                                               
NA75                           213                                                                                
NGA75                          None                                                                               
