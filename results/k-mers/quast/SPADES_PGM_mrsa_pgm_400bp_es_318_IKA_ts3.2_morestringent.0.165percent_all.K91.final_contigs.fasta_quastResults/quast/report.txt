All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K91.final_contigs
#Contigs (>= 0 bp)             5621                                                                                       
#Contigs (>= 1000 bp)          66                                                                                         
Total length (>= 0 bp)         4375451                                                                                    
Total length (>= 1000 bp)      2755617                                                                                    
#Contigs                       5252                                                                                       
Largest contig                 228388                                                                                     
Total length                   4328968                                                                                    
Reference length               2813862                                                                                    
GC (%)                         32.20                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            37955                                                                                      
NG50                           88632                                                                                      
N75                            334                                                                                        
NG75                           39820                                                                                      
#misassemblies                 10                                                                                         
#local misassemblies           9                                                                                          
#unaligned contigs             1588 + 126 part                                                                            
Unaligned contigs length       487450                                                                                     
Genome fraction (%)            98.430                                                                                     
Duplication ratio              1.383                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        6.28                                                                                       
#indels per 100 kbp            16.46                                                                                      
#genes                         2651 + 54 part                                                                             
#predicted genes (unique)      6119                                                                                       
#predicted genes (>= 0 bp)     6123                                                                                       
#predicted genes (>= 300 bp)   2323                                                                                       
#predicted genes (>= 1500 bp)  289                                                                                        
#predicted genes (>= 3000 bp)  29                                                                                         
Largest alignment              228388                                                                                     
NA50                           37955                                                                                      
NGA50                          88632                                                                                      
NA75                           302                                                                                        
NGA75                          39820                                                                                      
