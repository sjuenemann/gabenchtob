All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_115.final.contig
#Mis_misassemblies               10                                                                                  
#Mis_relocations                 5                                                                                   
#Mis_translocations              0                                                                                   
#Mis_inversions                  5                                                                                   
#Mis_misassembled contigs        10                                                                                  
Mis_Misassembled contigs length  6278                                                                                
#Mis_local misassemblies         0                                                                                   
#mismatches                      94                                                                                  
#indels                          2291                                                                                
#Mis_short indels (<= 5 bp)      2291                                                                                
#Mis_long indels (> 5 bp)        0                                                                                   
Indels length                    2363                                                                                
