All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_112-unitigs
#Contigs                                   2315                                                                                    
#Contigs (>= 0 bp)                         4073                                                                                    
#Contigs (>= 1000 bp)                      974                                                                                     
Largest contig                             13769                                                                                   
Total length                               2957418                                                                                 
Total length (>= 0 bp)                     3188598                                                                                 
Total length (>= 1000 bp)                  2386498                                                                                 
Reference length                           2813862                                                                                 
N50                                        2279                                                                                    
NG50                                       2433                                                                                    
N75                                        1210                                                                                    
NG75                                       1362                                                                                    
L50                                        381                                                                                     
LG50                                       351                                                                                     
L75                                        822                                                                                     
LG75                                       738                                                                                     
#local misassemblies                       2                                                                                       
#misassemblies                             37                                                                                      
#misassembled contigs                      37                                                                                      
Misassembled contigs length                53447                                                                                   
Misassemblies inter-contig overlap         483                                                                                     
#unaligned contigs                         0 + 15 part                                                                             
Unaligned contigs length                   498                                                                                     
#ambiguously mapped contigs                74                                                                                      
Extra bases in ambiguously mapped contigs  -19451                                                                                  
#N's                                       0                                                                                       
#N's per 100 kbp                           0.00                                                                                    
Genome fraction (%)                        97.386                                                                                  
Duplication ratio                          1.072                                                                                   
#genes                                     1782 + 912 part                                                                         
#predicted genes (unique)                  4327                                                                                    
#predicted genes (>= 0 bp)                 4344                                                                                    
#predicted genes (>= 300 bp)               2700                                                                                    
#predicted genes (>= 1500 bp)              174                                                                                     
#predicted genes (>= 3000 bp)              12                                                                                      
#mismatches                                83                                                                                      
#indels                                    1285                                                                                    
Indels length                              1317                                                                                    
#mismatches per 100 kbp                    3.03                                                                                    
#indels per 100 kbp                        46.89                                                                                   
GC (%)                                     32.62                                                                                   
Reference GC (%)                           32.81                                                                                   
Average %IDY                               99.504                                                                                  
Largest alignment                          13769                                                                                   
NA50                                       2269                                                                                    
NGA50                                      2422                                                                                    
NA75                                       1201                                                                                    
NGA75                                      1358                                                                                    
LA50                                       382                                                                                     
LGA50                                      351                                                                                     
LA75                                       825                                                                                     
LGA75                                      740                                                                                     
#Mis_misassemblies                         37                                                                                      
#Mis_relocations                           21                                                                                      
#Mis_translocations                        1                                                                                       
#Mis_inversions                            15                                                                                      
#Mis_misassembled contigs                  37                                                                                      
Mis_Misassembled contigs length            53447                                                                                   
#Mis_local misassemblies                   2                                                                                       
#Mis_short indels (<= 5 bp)                1285                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                       
#Una_fully unaligned contigs               0                                                                                       
Una_Fully unaligned length                 0                                                                                       
#Una_partially unaligned contigs           15                                                                                      
#Una_with misassembly                      0                                                                                       
#Una_both parts are significant            0                                                                                       
Una_Partially unaligned length             498                                                                                     
GAGE_Contigs #                             2315                                                                                    
GAGE_Min contig                            200                                                                                     
GAGE_Max contig                            13769                                                                                   
GAGE_N50                                   2433 COUNT: 351                                                                         
GAGE_Genome size                           2813862                                                                                 
GAGE_Assembly size                         2957418                                                                                 
GAGE_Chaff bases                           0                                                                                       
GAGE_Missing reference bases               34658(1.23%)                                                                            
GAGE_Missing assembly bases                907(0.03%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                                
GAGE_Duplicated reference bases            99518                                                                                   
GAGE_Compressed reference bases            39871                                                                                   
GAGE_Bad trim                              906                                                                                     
GAGE_Avg idy                               99.93                                                                                   
GAGE_SNPs                                  56                                                                                      
GAGE_Indels < 5bp                          796                                                                                     
GAGE_Indels >= 5                           3                                                                                       
GAGE_Inversions                            3                                                                                       
GAGE_Relocation                            2                                                                                       
GAGE_Translocation                         0                                                                                       
GAGE_Corrected contig #                    1880                                                                                    
GAGE_Corrected assembly size               2857552                                                                                 
GAGE_Min correct contig                    200                                                                                     
GAGE_Max correct contig                    13771                                                                                   
GAGE_Corrected N50                         2424 COUNT: 351                                                                         
