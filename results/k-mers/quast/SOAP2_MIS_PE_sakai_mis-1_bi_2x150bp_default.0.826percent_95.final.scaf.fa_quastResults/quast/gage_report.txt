All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_95.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_95.final.scaf
GAGE_Contigs #                   657                                                                            405                                                                   
GAGE_Min contig                  200                                                                            200                                                                   
GAGE_Max contig                  221602                                                                         374865                                                                
GAGE_N50                         65838 COUNT: 25                                                                144827 COUNT: 11                                                      
GAGE_Genome size                 5594470                                                                        5594470                                                               
GAGE_Assembly size               5386139                                                                        5445495                                                               
GAGE_Chaff bases                 0                                                                              0                                                                     
GAGE_Missing reference bases     23143(0.41%)                                                                   18044(0.32%)                                                          
GAGE_Missing assembly bases      5446(0.10%)                                                                    53723(0.99%)                                                          
GAGE_Missing assembly contigs    1(0.15%)                                                                       1(0.25%)                                                              
GAGE_Duplicated reference bases  11114                                                                          12478                                                                 
GAGE_Compressed reference bases  281927                                                                         280736                                                                
GAGE_Bad trim                    17                                                                             34350                                                                 
GAGE_Avg idy                     100.00                                                                         99.98                                                                 
GAGE_SNPs                        58                                                                             61                                                                    
GAGE_Indels < 5bp                31                                                                             69                                                                    
GAGE_Indels >= 5                 10                                                                             177                                                                   
GAGE_Inversions                  1                                                                              4                                                                     
GAGE_Relocation                  4                                                                              19                                                                    
GAGE_Translocation               0                                                                              0                                                                     
GAGE_Corrected contig #          634                                                                            619                                                                   
GAGE_Corrected assembly size     5368313                                                                        5368010                                                               
GAGE_Min correct contig          200                                                                            200                                                                   
GAGE_Max correct contig          217366                                                                         217366                                                                
GAGE_Corrected N50               58004 COUNT: 27                                                                63065 COUNT: 26                                                       
