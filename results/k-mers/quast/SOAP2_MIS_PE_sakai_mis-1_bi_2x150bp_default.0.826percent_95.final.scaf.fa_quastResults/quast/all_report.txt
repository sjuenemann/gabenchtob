All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_95.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_95.final.scaf
#Contigs                                   657                                                                            405                                                                   
#Contigs (>= 0 bp)                         1422                                                                           1079                                                                  
#Contigs (>= 1000 bp)                      205                                                                            131                                                                   
Largest contig                             221602                                                                         374865                                                                
Total length                               5386139                                                                        5445495                                                               
Total length (>= 0 bp)                     5495818                                                                        5541444                                                               
Total length (>= 1000 bp)                  5221965                                                                        5339786                                                               
Reference length                           5594470                                                                        5594470                                                               
N50                                        66113                                                                          144827                                                                
NG50                                       65838                                                                          144827                                                                
N75                                        30730                                                                          76161                                                                 
NG75                                       29899                                                                          59325                                                                 
L50                                        24                                                                             11                                                                    
LG50                                       25                                                                             11                                                                    
L75                                        53                                                                             24                                                                    
LG75                                       58                                                                             26                                                                    
#local misassemblies                       2                                                                              83                                                                    
#misassemblies                             6                                                                              40                                                                    
#misassembled contigs                      6                                                                              31                                                                    
Misassembled contigs length                335675                                                                         1234124                                                               
Misassemblies inter-contig overlap         8                                                                              651                                                                   
#unaligned contigs                         4 + 0 part                                                                     6 + 7 part                                                            
Unaligned contigs length                   6879                                                                           15785                                                                 
#ambiguously mapped contigs                239                                                                            201                                                                   
Extra bases in ambiguously mapped contigs  -109246                                                                        -90263                                                                
#N's                                       85                                                                             45711                                                                 
#N's per 100 kbp                           1.58                                                                           839.43                                                                
Genome fraction (%)                        94.157                                                                         94.294                                                                
Duplication ratio                          1.000                                                                          1.012                                                                 
#genes                                     4868 + 265 part                                                                4950 + 194 part                                                       
#predicted genes (unique)                  5558                                                                           5504                                                                  
#predicted genes (>= 0 bp)                 5577                                                                           5517                                                                  
#predicted genes (>= 300 bp)               4473                                                                           4477                                                                  
#predicted genes (>= 1500 bp)              659                                                                            659                                                                   
#predicted genes (>= 3000 bp)              67                                                                             66                                                                    
#mismatches                                54                                                                             207                                                                   
#indels                                    29                                                                             1675                                                                  
Indels length                              179                                                                            4267                                                                  
#mismatches per 100 kbp                    1.03                                                                           3.92                                                                  
#indels per 100 kbp                        0.55                                                                           31.75                                                                 
GC (%)                                     50.29                                                                          50.30                                                                 
Reference GC (%)                           50.48                                                                          50.48                                                                 
Average %IDY                               98.944                                                                         98.897                                                                
Largest alignment                          217366                                                                         361960                                                                
NA50                                       65838                                                                          144790                                                                
NGA50                                      63065                                                                          142739                                                                
NA75                                       30730                                                                          76161                                                                 
NGA75                                      29899                                                                          58776                                                                 
LA50                                       24                                                                             12                                                                    
LGA50                                      26                                                                             13                                                                    
LA75                                       53                                                                             25                                                                    
LGA75                                      58                                                                             27                                                                    
#Mis_misassemblies                         6                                                                              40                                                                    
#Mis_relocations                           6                                                                              40                                                                    
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  6                                                                              31                                                                    
Mis_Misassembled contigs length            335675                                                                         1234124                                                               
#Mis_local misassemblies                   2                                                                              83                                                                    
#Mis_short indels (<= 5 bp)                20                                                                             1572                                                                  
#Mis_long indels (> 5 bp)                  9                                                                              103                                                                   
#Una_fully unaligned contigs               4                                                                              6                                                                     
Una_Fully unaligned length                 6879                                                                           11081                                                                 
#Una_partially unaligned contigs           0                                                                              7                                                                     
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            0                                                                              4                                                                     
Una_Partially unaligned length             0                                                                              4704                                                                  
GAGE_Contigs #                             657                                                                            405                                                                   
GAGE_Min contig                            200                                                                            200                                                                   
GAGE_Max contig                            221602                                                                         374865                                                                
GAGE_N50                                   65838 COUNT: 25                                                                144827 COUNT: 11                                                      
GAGE_Genome size                           5594470                                                                        5594470                                                               
GAGE_Assembly size                         5386139                                                                        5445495                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               23143(0.41%)                                                                   18044(0.32%)                                                          
GAGE_Missing assembly bases                5446(0.10%)                                                                    53723(0.99%)                                                          
GAGE_Missing assembly contigs              1(0.15%)                                                                       1(0.25%)                                                              
GAGE_Duplicated reference bases            11114                                                                          12478                                                                 
GAGE_Compressed reference bases            281927                                                                         280736                                                                
GAGE_Bad trim                              17                                                                             34350                                                                 
GAGE_Avg idy                               100.00                                                                         99.98                                                                 
GAGE_SNPs                                  58                                                                             61                                                                    
GAGE_Indels < 5bp                          31                                                                             69                                                                    
GAGE_Indels >= 5                           10                                                                             177                                                                   
GAGE_Inversions                            1                                                                              4                                                                     
GAGE_Relocation                            4                                                                              19                                                                    
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    634                                                                            619                                                                   
GAGE_Corrected assembly size               5368313                                                                        5368010                                                               
GAGE_Min correct contig                    200                                                                            200                                                                   
GAGE_Max correct contig                    217366                                                                         217366                                                                
GAGE_Corrected N50                         58004 COUNT: 27                                                                63065 COUNT: 26                                                       
