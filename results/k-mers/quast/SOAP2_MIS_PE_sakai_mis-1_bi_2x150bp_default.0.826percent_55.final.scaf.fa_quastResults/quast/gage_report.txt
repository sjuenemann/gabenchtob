All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_55.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_55.final.scaf
GAGE_Contigs #                   1240                                                                           315                                                                   
GAGE_Min contig                  200                                                                            200                                                                   
GAGE_Max contig                  52615                                                                          374550                                                                
GAGE_N50                         8516 COUNT: 193                                                                147905 COUNT: 12                                                      
GAGE_Genome size                 5594470                                                                        5594470                                                               
GAGE_Assembly size               5205407                                                                        5328610                                                               
GAGE_Chaff bases                 0                                                                              0                                                                     
GAGE_Missing reference bases     291445(5.21%)                                                                  222001(3.97%)                                                         
GAGE_Missing assembly bases      5701(0.11%)                                                                    81980(1.54%)                                                          
GAGE_Missing assembly contigs    2(0.16%)                                                                       2(0.63%)                                                              
GAGE_Duplicated reference bases  2332                                                                           5517                                                                  
GAGE_Compressed reference bases  115797                                                                         148926                                                                
GAGE_Bad trim                    65                                                                             10397                                                                 
GAGE_Avg idy                     100.00                                                                         99.85                                                                 
GAGE_SNPs                        93                                                                             105                                                                   
GAGE_Indels < 5bp                41                                                                             460                                                                   
GAGE_Indels >= 5                 11                                                                             1007                                                                  
GAGE_Inversions                  0                                                                              4                                                                     
GAGE_Relocation                  2                                                                              12                                                                    
GAGE_Translocation               0                                                                              0                                                                     
GAGE_Corrected contig #          1241                                                                           1092                                                                  
GAGE_Corrected assembly size     5197126                                                                        5197854                                                               
GAGE_Min correct contig          200                                                                            200                                                                   
GAGE_Max correct contig          52615                                                                          64746                                                                 
GAGE_Corrected N50               8479 COUNT: 194                                                                9898 COUNT: 169                                                       
