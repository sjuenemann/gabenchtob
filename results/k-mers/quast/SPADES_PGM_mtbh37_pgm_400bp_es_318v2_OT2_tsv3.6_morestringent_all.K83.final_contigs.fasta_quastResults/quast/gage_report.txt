All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K83.final_contigs
GAGE_Contigs #                   921                                                                                
GAGE_Min contig                  208                                                                                
GAGE_Max contig                  182941                                                                             
GAGE_N50                         64029 COUNT: 22                                                                    
GAGE_Genome size                 4411532                                                                            
GAGE_Assembly size               4508495                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     51587(1.17%)                                                                       
GAGE_Missing assembly bases      25325(0.56%)                                                                       
GAGE_Missing assembly contigs    69(7.49%)                                                                          
GAGE_Duplicated reference bases  170420                                                                             
GAGE_Compressed reference bases  58180                                                                              
GAGE_Bad trim                    6723                                                                               
GAGE_Avg idy                     99.96                                                                              
GAGE_SNPs                        295                                                                                
GAGE_Indels < 5bp                1404                                                                               
GAGE_Indels >= 5                 21                                                                                 
GAGE_Inversions                  3                                                                                  
GAGE_Relocation                  15                                                                                 
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          251                                                                                
GAGE_Corrected assembly size     4317565                                                                            
GAGE_Min correct contig          208                                                                                
GAGE_Max correct contig          182973                                                                             
GAGE_Corrected N50               52845 COUNT: 27                                                                    
