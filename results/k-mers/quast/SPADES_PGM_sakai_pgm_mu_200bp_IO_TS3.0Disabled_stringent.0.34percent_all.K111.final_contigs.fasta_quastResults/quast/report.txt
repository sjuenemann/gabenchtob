All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_all.K111.final_contigs
#Contigs (>= 0 bp)             1287                                                                                       
#Contigs (>= 1000 bp)          175                                                                                        
Total length (>= 0 bp)         5607672                                                                                    
Total length (>= 1000 bp)      5293834                                                                                    
#Contigs                       1166                                                                                       
Largest contig                 340077                                                                                     
Total length                   5590773                                                                                    
Reference length               5594470                                                                                    
GC (%)                         50.27                                                                                      
Reference GC (%)               50.48                                                                                      
N50                            99746                                                                                      
NG50                           99746                                                                                      
N75                            41304                                                                                      
NG75                           41304                                                                                      
#misassemblies                 66                                                                                         
#local misassemblies           14                                                                                         
#unaligned contigs             16 + 132 part                                                                              
Unaligned contigs length       10394                                                                                      
Genome fraction (%)            94.287                                                                                     
Duplication ratio              1.041                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        8.89                                                                                       
#indels per 100 kbp            36.53                                                                                      
#genes                         4953 + 175 part                                                                            
#predicted genes (unique)      6718                                                                                       
#predicted genes (>= 0 bp)     6732                                                                                       
#predicted genes (>= 300 bp)   4869                                                                                       
#predicted genes (>= 1500 bp)  509                                                                                        
#predicted genes (>= 3000 bp)  36                                                                                         
Largest alignment              259797                                                                                     
NA50                           99746                                                                                      
NGA50                          99746                                                                                      
NA75                           41280                                                                                      
NGA75                          41280                                                                                      
