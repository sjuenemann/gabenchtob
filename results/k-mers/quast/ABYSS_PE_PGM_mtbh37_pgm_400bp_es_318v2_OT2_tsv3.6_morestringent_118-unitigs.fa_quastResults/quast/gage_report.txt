All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_118-unitigs
GAGE_Contigs #                   8551                                                                       
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  13572                                                                      
GAGE_N50                         2206 COUNT: 602                                                            
GAGE_Genome size                 4411532                                                                    
GAGE_Assembly size               5434917                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     138397(3.14%)                                                              
GAGE_Missing assembly bases      1376(0.03%)                                                                
GAGE_Missing assembly contigs    4(0.05%)                                                                   
GAGE_Duplicated reference bases  878469                                                                     
GAGE_Compressed reference bases  53924                                                                      
GAGE_Bad trim                    369                                                                        
GAGE_Avg idy                     99.95                                                                      
GAGE_SNPs                        73                                                                         
GAGE_Indels < 5bp                1381                                                                       
GAGE_Indels >= 5                 4                                                                          
GAGE_Inversions                  0                                                                          
GAGE_Relocation                  6                                                                          
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          4777                                                                       
GAGE_Corrected assembly size     4556295                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          13579                                                                      
GAGE_Corrected N50               2199 COUNT: 607                                                            
