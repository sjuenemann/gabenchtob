reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_93.final.scaf_broken  | 98.2498075599       | 1.00043478041     | 66          | 2638      | 58        | None      | None      |
  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_93.final.scaf  | 98.392991554        | 1.0011077629      | 40          | 2672      | 24        | None      | None      |
