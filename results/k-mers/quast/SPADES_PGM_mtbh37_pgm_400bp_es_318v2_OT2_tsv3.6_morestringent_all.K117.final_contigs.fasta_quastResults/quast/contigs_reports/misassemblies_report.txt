All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_all.K117.final_contigs
#Mis_misassemblies               35                                                                                  
#Mis_relocations                 35                                                                                  
#Mis_translocations              0                                                                                   
#Mis_inversions                  0                                                                                   
#Mis_misassembled contigs        32                                                                                  
Mis_Misassembled contigs length  54098                                                                               
#Mis_local misassemblies         19                                                                                  
#mismatches                      367                                                                                 
#indels                          2070                                                                                
#Mis_short indels (<= 5 bp)      2064                                                                                
#Mis_long indels (> 5 bp)        6                                                                                   
Indels length                    2402                                                                                
