All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_123.final.contig
#Contigs                                   34645                                                                                
#Contigs (>= 0 bp)                         54628                                                                                
#Contigs (>= 1000 bp)                      94                                                                                   
Largest contig                             2065                                                                                 
Total length                               10767580                                                                             
Total length (>= 0 bp)                     13779751                                                                             
Total length (>= 1000 bp)                  114197                                                                               
Reference length                           2813862                                                                              
N50                                        368                                                                                  
NG50                                       427                                                                                  
N75                                        243                                                                                  
NG75                                       409                                                                                  
L50                                        12585                                                                                
LG50                                       2455                                                                                 
L75                                        23038                                                                                
LG75                                       4144                                                                                 
#local misassemblies                       2                                                                                    
#misassemblies                             168                                                                                  
#misassembled contigs                      168                                                                                  
Misassembled contigs length                62557                                                                                
Misassemblies inter-contig overlap         377                                                                                  
#unaligned contigs                         505 + 265 part                                                                       
Unaligned contigs length                   221255                                                                               
#ambiguously mapped contigs                433                                                                                  
Extra bases in ambiguously mapped contigs  -113627                                                                              
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        98.266                                                                               
Duplication ratio                          3.773                                                                                
#genes                                     465 + 2243 part                                                                      
#predicted genes (unique)                  33867                                                                                
#predicted genes (>= 0 bp)                 34730                                                                                
#predicted genes (>= 300 bp)               2335                                                                                 
#predicted genes (>= 1500 bp)              1                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                    
#mismatches                                1625                                                                                 
#indels                                    23584                                                                                
Indels length                              24245                                                                                
#mismatches per 100 kbp                    58.77                                                                                
#indels per 100 kbp                        852.93                                                                               
GC (%)                                     32.67                                                                                
Reference GC (%)                           32.81                                                                                
Average %IDY                               99.143                                                                               
Largest alignment                          2065                                                                                 
NA50                                       310                                                                                  
NGA50                                      424                                                                                  
NA75                                       242                                                                                  
NGA75                                      407                                                                                  
LA50                                       12759                                                                                
LGA50                                      2463                                                                                 
LA75                                       23578                                                                                
LGA75                                      4161                                                                                 
#Mis_misassemblies                         168                                                                                  
#Mis_relocations                           162                                                                                  
#Mis_translocations                        6                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  168                                                                                  
Mis_Misassembled contigs length            62557                                                                                
#Mis_local misassemblies                   2                                                                                    
#Mis_short indels (<= 5 bp)                23584                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                    
#Una_fully unaligned contigs               505                                                                                  
Una_Fully unaligned length                 202609                                                                               
#Una_partially unaligned contigs           265                                                                                  
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            3                                                                                    
Una_Partially unaligned length             18646                                                                                
GAGE_Contigs #                             34645                                                                                
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            2065                                                                                 
GAGE_N50                                   427 COUNT: 2455                                                                      
GAGE_Genome size                           2813862                                                                              
GAGE_Assembly size                         10767580                                                                             
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               17050(0.61%)                                                                         
GAGE_Missing assembly bases                151847(1.41%)                                                                        
GAGE_Missing assembly contigs              219(0.63%)                                                                           
GAGE_Duplicated reference bases            7034226                                                                              
GAGE_Compressed reference bases            34170                                                                                
GAGE_Bad trim                              63351                                                                                
GAGE_Avg idy                               99.91                                                                                
GAGE_SNPs                                  83                                                                                   
GAGE_Indels < 5bp                          1611                                                                                 
GAGE_Indels >= 5                           1                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            0                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    11265                                                                                
GAGE_Corrected assembly size               3578949                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    2065                                                                                 
GAGE_Corrected N50                         365 COUNT: 2558                                                                      
