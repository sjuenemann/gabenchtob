All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_79.final.contig
#Contigs                                   7997                                                                        
#Contigs (>= 0 bp)                         179431                                                                      
#Contigs (>= 1000 bp)                      6                                                                           
Largest contig                             1458                                                                        
Total length                               2300267                                                                     
Total length (>= 0 bp)                     23377414                                                                    
Total length (>= 1000 bp)                  7061                                                                        
Reference length                           4411532                                                                     
N50                                        274                                                                         
NG50                                       208                                                                         
N75                                        245                                                                         
NG75                                       None                                                                        
L50                                        3199                                                                        
LG50                                       7533                                                                        
L75                                        5429                                                                        
LG75                                       None                                                                        
#local misassemblies                       6                                                                           
#misassemblies                             27                                                                          
#misassembled contigs                      27                                                                          
Misassembled contigs length                7331                                                                        
Misassemblies inter-contig overlap         702                                                                         
#unaligned contigs                         428 + 33 part                                                               
Unaligned contigs length                   120771                                                                      
#ambiguously mapped contigs                21                                                                          
Extra bases in ambiguously mapped contigs  -5331                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        43.257                                                                      
Duplication ratio                          1.140                                                                       
#genes                                     63 + 3117 part                                                              
#predicted genes (unique)                  6819                                                                        
#predicted genes (>= 0 bp)                 6822                                                                        
#predicted genes (>= 300 bp)               1115                                                                        
#predicted genes (>= 1500 bp)              0                                                                           
#predicted genes (>= 3000 bp)              0                                                                           
#mismatches                                628                                                                         
#indels                                    14807                                                                       
Indels length                              15243                                                                       
#mismatches per 100 kbp                    32.91                                                                       
#indels per 100 kbp                        775.93                                                                      
GC (%)                                     66.12                                                                       
Reference GC (%)                           65.61                                                                       
Average %IDY                               99.042                                                                      
Largest alignment                          1458                                                                        
NA50                                       270                                                                         
NGA50                                      None                                                                        
NA75                                       240                                                                         
NGA75                                      None                                                                        
LA50                                       3226                                                                        
LGA50                                      None                                                                        
LA75                                       5499                                                                        
LGA75                                      None                                                                        
#Mis_misassemblies                         27                                                                          
#Mis_relocations                           27                                                                          
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  27                                                                          
Mis_Misassembled contigs length            7331                                                                        
#Mis_local misassemblies                   6                                                                           
#Mis_short indels (<= 5 bp)                14807                                                                       
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               428                                                                         
Una_Fully unaligned length                 117931                                                                      
#Una_partially unaligned contigs           33                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             2840                                                                        
GAGE_Contigs #                             7997                                                                        
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            1458                                                                        
GAGE_N50                                   208 COUNT: 7533                                                             
GAGE_Genome size                           4411532                                                                     
GAGE_Assembly size                         2300267                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               2428345(55.05%)                                                             
GAGE_Missing assembly bases                34500(1.50%)                                                                
GAGE_Missing assembly contigs              79(0.99%)                                                                   
GAGE_Duplicated reference bases            147622                                                                      
GAGE_Compressed reference bases            21877                                                                       
GAGE_Bad trim                              12882                                                                       
GAGE_Avg idy                               99.08                                                                       
GAGE_SNPs                                  599                                                                         
GAGE_Indels < 5bp                          14840                                                                       
GAGE_Indels >= 5                           22                                                                          
GAGE_Inversions                            11                                                                          
GAGE_Relocation                            10                                                                          
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    7208                                                                        
GAGE_Corrected assembly size               2089356                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    1458                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                  
