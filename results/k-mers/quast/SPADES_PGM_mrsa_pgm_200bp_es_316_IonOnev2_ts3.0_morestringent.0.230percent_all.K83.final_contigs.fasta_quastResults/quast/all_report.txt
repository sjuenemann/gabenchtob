All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K83.final_contigs
#Contigs                                   2845                                                                                            
#Contigs (>= 0 bp)                         3035                                                                                            
#Contigs (>= 1000 bp)                      63                                                                                              
Largest contig                             239379                                                                                          
Total length                               3456544                                                                                         
Total length (>= 0 bp)                     3478960                                                                                         
Total length (>= 1000 bp)                  2754298                                                                                         
Reference length                           2813862                                                                                         
N50                                        73240                                                                                           
NG50                                       101558                                                                                          
N75                                        19854                                                                                           
NG75                                       46704                                                                                           
L50                                        13                                                                                              
LG50                                       9                                                                                               
L75                                        33                                                                                              
LG75                                       19                                                                                              
#local misassemblies                       14                                                                                              
#misassemblies                             80                                                                                              
#misassembled contigs                      80                                                                                              
Misassembled contigs length                325039                                                                                          
Misassemblies inter-contig overlap         3332                                                                                            
#unaligned contigs                         1043 + 537 part                                                                                 
Unaligned contigs length                   300385                                                                                          
#ambiguously mapped contigs                24                                                                                              
Extra bases in ambiguously mapped contigs  -13144                                                                                          
#N's                                       0                                                                                               
#N's per 100 kbp                           0.00                                                                                            
Genome fraction (%)                        98.240                                                                                          
Duplication ratio                          1.138                                                                                           
#genes                                     2637 + 55 part                                                                                  
#predicted genes (unique)                  4513                                                                                            
#predicted genes (>= 0 bp)                 4516                                                                                            
#predicted genes (>= 300 bp)               2343                                                                                            
#predicted genes (>= 1500 bp)              276                                                                                             
#predicted genes (>= 3000 bp)              23                                                                                              
#mismatches                                227                                                                                             
#indels                                    666                                                                                             
Indels length                              838                                                                                             
#mismatches per 100 kbp                    8.21                                                                                            
#indels per 100 kbp                        24.09                                                                                           
GC (%)                                     32.50                                                                                           
Reference GC (%)                           32.81                                                                                           
Average %IDY                               97.589                                                                                          
Largest alignment                          207834                                                                                          
NA50                                       73240                                                                                           
NGA50                                      89438                                                                                           
NA75                                       19838                                                                                           
NGA75                                      45875                                                                                           
LA50                                       13                                                                                              
LGA50                                      10                                                                                              
LA75                                       35                                                                                              
LGA75                                      20                                                                                              
#Mis_misassemblies                         80                                                                                              
#Mis_relocations                           25                                                                                              
#Mis_translocations                        0                                                                                               
#Mis_inversions                            55                                                                                              
#Mis_misassembled contigs                  80                                                                                              
Mis_Misassembled contigs length            325039                                                                                          
#Mis_local misassemblies                   14                                                                                              
#Mis_short indels (<= 5 bp)                663                                                                                             
#Mis_long indels (> 5 bp)                  3                                                                                               
#Una_fully unaligned contigs               1043                                                                                            
Una_Fully unaligned length                 260362                                                                                          
#Una_partially unaligned contigs           537                                                                                             
#Una_with misassembly                      0                                                                                               
#Una_both parts are significant            0                                                                                               
Una_Partially unaligned length             40023                                                                                           
GAGE_Contigs #                             2845                                                                                            
GAGE_Min contig                            203                                                                                             
GAGE_Max contig                            239379                                                                                          
GAGE_N50                                   101558 COUNT: 9                                                                                 
GAGE_Genome size                           2813862                                                                                         
GAGE_Assembly size                         3456544                                                                                         
GAGE_Chaff bases                           0                                                                                               
GAGE_Missing reference bases               4303(0.15%)                                                                                     
GAGE_Missing assembly bases                149792(4.33%)                                                                                   
GAGE_Missing assembly contigs              389(13.67%)                                                                                     
GAGE_Duplicated reference bases            534537                                                                                          
GAGE_Compressed reference bases            44498                                                                                           
GAGE_Bad trim                              52608                                                                                           
GAGE_Avg idy                               99.97                                                                                           
GAGE_SNPs                                  80                                                                                              
GAGE_Indels < 5bp                          564                                                                                             
GAGE_Indels >= 5                           11                                                                                              
GAGE_Inversions                            2                                                                                               
GAGE_Relocation                            5                                                                                               
GAGE_Translocation                         0                                                                                               
GAGE_Corrected contig #                    129                                                                                             
GAGE_Corrected assembly size               2776744                                                                                         
GAGE_Min correct contig                    203                                                                                             
GAGE_Max correct contig                    207861                                                                                          
GAGE_Corrected N50                         63627 COUNT: 14                                                                                 
