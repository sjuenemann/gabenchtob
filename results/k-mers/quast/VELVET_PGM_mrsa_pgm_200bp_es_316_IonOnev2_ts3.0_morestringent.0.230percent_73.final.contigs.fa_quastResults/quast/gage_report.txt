All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_73.final.contigs
GAGE_Contigs #                   881                                                                                        
GAGE_Min contig                  201                                                                                        
GAGE_Max contig                  23696                                                                                      
GAGE_N50                         5433 COUNT: 154                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2740679                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     62424(2.22%)                                                                               
GAGE_Missing assembly bases      881(0.03%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                   
GAGE_Duplicated reference bases  3523                                                                                       
GAGE_Compressed reference bases  38225                                                                                      
GAGE_Bad trim                    611                                                                                        
GAGE_Avg idy                     99.97                                                                                      
GAGE_SNPs                        103                                                                                        
GAGE_Indels < 5bp                637                                                                                        
GAGE_Indels >= 5                 22                                                                                         
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  12                                                                                         
GAGE_Translocation               2                                                                                          
GAGE_Corrected contig #          904                                                                                        
GAGE_Corrected assembly size     2736673                                                                                    
GAGE_Min correct contig          202                                                                                        
GAGE_Max correct contig          21710                                                                                      
GAGE_Corrected N50               5257 COUNT: 161                                                                            
