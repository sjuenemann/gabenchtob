All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_73.final.contigs
#Una_fully unaligned contigs      0                                                                                          
Una_Fully unaligned length        0                                                                                          
#Una_partially unaligned contigs  8                                                                                          
#Una_with misassembly             0                                                                                          
#Una_both parts are significant   0                                                                                          
Una_Partially unaligned length    554                                                                                        
#N's                              360                                                                                        
