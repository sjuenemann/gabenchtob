All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent.K77.final_contigs
#Contigs                                   122                                                                                         
#Contigs (>= 0 bp)                         402                                                                                         
#Contigs (>= 1000 bp)                      71                                                                                          
Largest contig                             207827                                                                                      
Total length                               2770491                                                                                     
Total length (>= 0 bp)                     2801928                                                                                     
Total length (>= 1000 bp)                  2748178                                                                                     
Reference length                           2813862                                                                                     
N50                                        70756                                                                                       
NG50                                       70756                                                                                       
N75                                        39149                                                                                       
NG75                                       39149                                                                                       
L50                                        11                                                                                          
LG50                                       11                                                                                          
L75                                        25                                                                                          
LG75                                       25                                                                                          
#local misassemblies                       15                                                                                          
#misassemblies                             5                                                                                           
#misassembled contigs                      5                                                                                           
Misassembled contigs length                167287                                                                                      
Misassemblies inter-contig overlap         4213                                                                                        
#unaligned contigs                         0 + 1 part                                                                                  
Unaligned contigs length                   23                                                                                          
#ambiguously mapped contigs                15                                                                                          
Extra bases in ambiguously mapped contigs  -10610                                                                                      
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        98.038                                                                                      
Duplication ratio                          1.002                                                                                       
#genes                                     2624 + 56 part                                                                              
#predicted genes (unique)                  2783                                                                                        
#predicted genes (>= 0 bp)                 2788                                                                                        
#predicted genes (>= 300 bp)               2340                                                                                        
#predicted genes (>= 1500 bp)              283                                                                                         
#predicted genes (>= 3000 bp)              24                                                                                          
#mismatches                                165                                                                                         
#indels                                    506                                                                                         
Indels length                              628                                                                                         
#mismatches per 100 kbp                    5.98                                                                                        
#indels per 100 kbp                        18.34                                                                                       
GC (%)                                     32.65                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               98.921                                                                                      
Largest alignment                          207827                                                                                      
NA50                                       70756                                                                                       
NGA50                                      70756                                                                                       
NA75                                       39149                                                                                       
NGA75                                      38702                                                                                       
LA50                                       11                                                                                          
LGA50                                      11                                                                                          
LA75                                       25                                                                                          
LGA75                                      26                                                                                          
#Mis_misassemblies                         5                                                                                           
#Mis_relocations                           4                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            1                                                                                           
#Mis_misassembled contigs                  5                                                                                           
Mis_Misassembled contigs length            167287                                                                                      
#Mis_local misassemblies                   15                                                                                          
#Mis_short indels (<= 5 bp)                503                                                                                         
#Mis_long indels (> 5 bp)                  3                                                                                           
#Una_fully unaligned contigs               0                                                                                           
Una_Fully unaligned length                 0                                                                                           
#Una_partially unaligned contigs           1                                                                                           
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             23                                                                                          
GAGE_Contigs #                             122                                                                                         
GAGE_Min contig                            200                                                                                         
GAGE_Max contig                            207827                                                                                      
GAGE_N50                                   70756 COUNT: 11                                                                             
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         2770491                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               6244(0.22%)                                                                                 
GAGE_Missing assembly bases                102(0.00%)                                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            628                                                                                         
GAGE_Compressed reference bases            45829                                                                                       
GAGE_Bad trim                              95                                                                                          
GAGE_Avg idy                               99.97                                                                                       
GAGE_SNPs                                  61                                                                                          
GAGE_Indels < 5bp                          522                                                                                         
GAGE_Indels >= 5                           12                                                                                          
GAGE_Inversions                            1                                                                                           
GAGE_Relocation                            4                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    136                                                                                         
GAGE_Corrected assembly size               2772629                                                                                     
GAGE_Min correct contig                    200                                                                                         
GAGE_Max correct contig                    207849                                                                                      
GAGE_Corrected N50                         52991 COUNT: 16                                                                             
