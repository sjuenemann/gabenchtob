All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent.K77.final_contigs
GAGE_Contigs #                   122                                                                                         
GAGE_Min contig                  200                                                                                         
GAGE_Max contig                  207827                                                                                      
GAGE_N50                         70756 COUNT: 11                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               2770491                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     6244(0.22%)                                                                                 
GAGE_Missing assembly bases      102(0.00%)                                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  628                                                                                         
GAGE_Compressed reference bases  45829                                                                                       
GAGE_Bad trim                    95                                                                                          
GAGE_Avg idy                     99.97                                                                                       
GAGE_SNPs                        61                                                                                          
GAGE_Indels < 5bp                522                                                                                         
GAGE_Indels >= 5                 12                                                                                          
GAGE_Inversions                  1                                                                                           
GAGE_Relocation                  4                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          136                                                                                         
GAGE_Corrected assembly size     2772629                                                                                     
GAGE_Min correct contig          200                                                                                         
GAGE_Max correct contig          207849                                                                                      
GAGE_Corrected N50               52991 COUNT: 16                                                                             
