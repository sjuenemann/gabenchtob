All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_83.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_83.final.scaf
GAGE_Contigs #   62685                                                                   62674                                                          
GAGE_Min contig  200                                                                     200                                                            
GAGE_Max contig  15437                                                                   15437                                                          
GAGE_N50         3583 COUNT: 387                                                         3603 COUNT: 386                                                
