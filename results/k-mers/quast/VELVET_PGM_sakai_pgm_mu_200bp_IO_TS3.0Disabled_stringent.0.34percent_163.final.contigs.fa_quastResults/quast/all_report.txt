All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_163.final.contigs
#Contigs                                   4123                                                                                  
#Contigs (>= 0 bp)                         4123                                                                                  
#Contigs (>= 1000 bp)                      970                                                                                   
Largest contig                             4028                                                                                  
Total length                               3450891                                                                               
Total length (>= 0 bp)                     3450891                                                                               
Total length (>= 1000 bp)                  1342070                                                                               
Reference length                           5594470                                                                               
N50                                        859                                                                                   
NG50                                       599                                                                                   
N75                                        637                                                                                   
NG75                                       None                                                                                  
L50                                        1387                                                                                  
LG50                                       2900                                                                                  
L75                                        2561                                                                                  
LG75                                       None                                                                                  
#local misassemblies                       1                                                                                     
#misassemblies                             12                                                                                    
#misassembled contigs                      12                                                                                    
Misassembled contigs length                13152                                                                                 
Misassemblies inter-contig overlap         2151                                                                                  
#unaligned contigs                         0 + 102 part                                                                          
Unaligned contigs length                   2655                                                                                  
#ambiguously mapped contigs                82                                                                                    
Extra bases in ambiguously mapped contigs  -55654                                                                                
#N's                                       20                                                                                    
#N's per 100 kbp                           0.58                                                                                  
Genome fraction (%)                        58.529                                                                                
Duplication ratio                          1.037                                                                                 
#genes                                     905 + 3118 part                                                                       
#predicted genes (unique)                  6316                                                                                  
#predicted genes (>= 0 bp)                 6318                                                                                  
#predicted genes (>= 300 bp)               4253                                                                                  
#predicted genes (>= 1500 bp)              51                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                     
#mismatches                                231                                                                                   
#indels                                    2578                                                                                  
Indels length                              2613                                                                                  
#mismatches per 100 kbp                    7.05                                                                                  
#indels per 100 kbp                        78.73                                                                                 
GC (%)                                     51.48                                                                                 
Reference GC (%)                           50.48                                                                                 
Average %IDY                               99.614                                                                                
Largest alignment                          4028                                                                                  
NA50                                       847                                                                                   
NGA50                                      592                                                                                   
NA75                                       628                                                                                   
NGA75                                      None                                                                                  
LA50                                       1401                                                                                  
LGA50                                      2934                                                                                  
LA75                                       2592                                                                                  
LGA75                                      None                                                                                  
#Mis_misassemblies                         12                                                                                    
#Mis_relocations                           6                                                                                     
#Mis_translocations                        0                                                                                     
#Mis_inversions                            6                                                                                     
#Mis_misassembled contigs                  12                                                                                    
Mis_Misassembled contigs length            13152                                                                                 
#Mis_local misassemblies                   1                                                                                     
#Mis_short indels (<= 5 bp)                2578                                                                                  
#Mis_long indels (> 5 bp)                  0                                                                                     
#Una_fully unaligned contigs               0                                                                                     
Una_Fully unaligned length                 0                                                                                     
#Una_partially unaligned contigs           102                                                                                   
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             2655                                                                                  
GAGE_Contigs #                             4123                                                                                  
GAGE_Min contig                            325                                                                                   
GAGE_Max contig                            4028                                                                                  
GAGE_N50                                   599 COUNT: 2900                                                                       
GAGE_Genome size                           5594470                                                                               
GAGE_Assembly size                         3450891                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               2085846(37.28%)                                                                       
GAGE_Missing assembly bases                3751(0.11%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                              
GAGE_Duplicated reference bases            9687                                                                                  
GAGE_Compressed reference bases            231678                                                                                
GAGE_Bad trim                              3730                                                                                  
GAGE_Avg idy                               99.91                                                                                 
GAGE_SNPs                                  154                                                                                   
GAGE_Indels < 5bp                          2354                                                                                  
GAGE_Indels >= 5                           1                                                                                     
GAGE_Inversions                            8                                                                                     
GAGE_Relocation                            3                                                                                     
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    4110                                                                                  
GAGE_Corrected assembly size               3440435                                                                               
GAGE_Min correct contig                    270                                                                                   
GAGE_Max correct contig                    4030                                                                                  
GAGE_Corrected N50                         599 COUNT: 2904                                                                       
