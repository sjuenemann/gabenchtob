All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_111.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x250bp_default.0.460percent_111.final.scaf
#Contigs                                   1085                                                                           1061                                                                  
#Contigs (>= 0 bp)                         2112                                                                           2085                                                                  
#Contigs (>= 1000 bp)                      195                                                                            180                                                                   
Largest contig                             112303                                                                         112303                                                                
Total length                               2968523                                                                        2969684                                                               
Total length (>= 0 bp)                     3094510                                                                        3095165                                                               
Total length (>= 1000 bp)                  2759703                                                                        2763537                                                               
Reference length                           2813862                                                                        2813862                                                               
N50                                        20620                                                                          23108                                                                 
NG50                                       21776                                                                          23598                                                                 
N75                                        10907                                                                          11643                                                                 
NG75                                       12040                                                                          12898                                                                 
L50                                        41                                                                             38                                                                    
LG50                                       37                                                                             34                                                                    
L75                                        91                                                                             83                                                                    
LG75                                       80                                                                             74                                                                    
#local misassemblies                       15                                                                             32                                                                    
#misassemblies                             19                                                                             22                                                                    
#misassembled contigs                      19                                                                             21                                                                    
Misassembled contigs length                14903                                                                          18008                                                                 
Misassemblies inter-contig overlap         15                                                                             126                                                                   
#unaligned contigs                         6 + 10 part                                                                    6 + 10 part                                                           
Unaligned contigs length                   7393                                                                           7393                                                                  
#ambiguously mapped contigs                70                                                                             67                                                                    
Extra bases in ambiguously mapped contigs  -18213                                                                         -17262                                                                
#N's                                       61                                                                             716                                                                   
#N's per 100 kbp                           2.05                                                                           24.11                                                                 
Genome fraction (%)                        98.650                                                                         98.681                                                                
Duplication ratio                          1.060                                                                          1.061                                                                 
#genes                                     2517 + 191 part                                                                2518 + 187 part                                                       
#predicted genes (unique)                  3506                                                                           3504                                                                  
#predicted genes (>= 0 bp)                 3527                                                                           3524                                                                  
#predicted genes (>= 300 bp)               2353                                                                           2352                                                                  
#predicted genes (>= 1500 bp)              292                                                                            292                                                                   
#predicted genes (>= 3000 bp)              30                                                                             30                                                                    
#mismatches                                119                                                                            130                                                                   
#indels                                    19                                                                             137                                                                   
Indels length                              128                                                                            449                                                                   
#mismatches per 100 kbp                    4.29                                                                           4.68                                                                  
#indels per 100 kbp                        0.68                                                                           4.93                                                                  
GC (%)                                     32.72                                                                          32.72                                                                 
Reference GC (%)                           32.81                                                                          32.81                                                                 
Average %IDY                               99.332                                                                         99.322                                                                
Largest alignment                          112303                                                                         112303                                                                
NA50                                       20620                                                                          23108                                                                 
NGA50                                      21776                                                                          23598                                                                 
NA75                                       10742                                                                          11562                                                                 
NGA75                                      12040                                                                          12898                                                                 
LA50                                       41                                                                             38                                                                    
LGA50                                      37                                                                             34                                                                    
LA75                                       91                                                                             83                                                                    
LGA75                                      80                                                                             74                                                                    
#Mis_misassemblies                         19                                                                             22                                                                    
#Mis_relocations                           18                                                                             21                                                                    
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            1                                                                              1                                                                     
#Mis_misassembled contigs                  19                                                                             21                                                                    
Mis_Misassembled contigs length            14903                                                                          18008                                                                 
#Mis_local misassemblies                   15                                                                             32                                                                    
#Mis_short indels (<= 5 bp)                15                                                                             128                                                                   
#Mis_long indels (> 5 bp)                  4                                                                              9                                                                     
#Una_fully unaligned contigs               6                                                                              6                                                                     
Una_Fully unaligned length                 6973                                                                           6973                                                                  
#Una_partially unaligned contigs           10                                                                             10                                                                    
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            0                                                                              0                                                                     
Una_Partially unaligned length             420                                                                            420                                                                   
GAGE_Contigs #                             1085                                                                           1061                                                                  
GAGE_Min contig                            200                                                                            200                                                                   
GAGE_Max contig                            112303                                                                         112303                                                                
GAGE_N50                                   21776 COUNT: 37                                                                23598 COUNT: 34                                                       
GAGE_Genome size                           2813862                                                                        2813862                                                               
GAGE_Assembly size                         2968523                                                                        2969684                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               451(0.02%)                                                                     425(0.02%)                                                            
GAGE_Missing assembly bases                7444(0.25%)                                                                    8113(0.27%)                                                           
GAGE_Missing assembly contigs              6(0.55%)                                                                       6(0.57%)                                                              
GAGE_Duplicated reference bases            134635                                                                         134978                                                                
GAGE_Compressed reference bases            43059                                                                          43165                                                                 
GAGE_Bad trim                              430                                                                            526                                                                   
GAGE_Avg idy                               99.99                                                                          99.99                                                                 
GAGE_SNPs                                  54                                                                             54                                                                    
GAGE_Indels < 5bp                          23                                                                             30                                                                    
GAGE_Indels >= 5                           2                                                                              8                                                                     
GAGE_Inversions                            0                                                                              0                                                                     
GAGE_Relocation                            1                                                                              4                                                                     
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    490                                                                            487                                                                   
GAGE_Corrected assembly size               2825845                                                                        2825658                                                               
GAGE_Min correct contig                    200                                                                            200                                                                   
GAGE_Max correct contig                    112302                                                                         112302                                                                
GAGE_Corrected N50                         20178 COUNT: 41                                                                20178 COUNT: 41                                                       
