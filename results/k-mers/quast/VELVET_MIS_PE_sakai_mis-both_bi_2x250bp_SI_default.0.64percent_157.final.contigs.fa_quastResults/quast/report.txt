All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_157.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_157.final.contigs
#Contigs (>= 0 bp)             498                                                                                      388                                                                             
#Contigs (>= 1000 bp)          286                                                                                      199                                                                             
Total length (>= 0 bp)         5458484                                                                                  5471840                                                                         
Total length (>= 1000 bp)      5357170                                                                                  5383530                                                                         
#Contigs                       498                                                                                      388                                                                             
Largest contig                 150463                                                                                   245400                                                                          
Total length                   5458484                                                                                  5471840                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.35                                                                                    50.35                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            38297                                                                                    72391                                                                           
NG50                           37879                                                                                    72391                                                                           
N75                            20913                                                                                    35460                                                                           
NG75                           18017                                                                                    31582                                                                           
#misassemblies                 5                                                                                        14                                                                              
#local misassemblies           2                                                                                        93                                                                              
#unaligned contigs             1 + 0 part                                                                               1 + 0 part                                                                      
Unaligned contigs length       5542                                                                                     5542                                                                            
Genome fraction (%)            95.463                                                                                   95.592                                                                          
Duplication ratio              1.001                                                                                    1.003                                                                           
#N's per 100 kbp               0.00                                                                                     244.09                                                                          
#mismatches per 100 kbp        2.34                                                                                     2.51                                                                            
#indels per 100 kbp            0.34                                                                                     6.38                                                                            
#genes                         4965 + 306 part                                                                          4981 + 296 part                                                                 
#predicted genes (unique)      5545                                                                                     5528                                                                            
#predicted genes (>= 0 bp)     5570                                                                                     5553                                                                            
#predicted genes (>= 300 bp)   4635                                                                                     4627                                                                            
#predicted genes (>= 1500 bp)  651                                                                                      652                                                                             
#predicted genes (>= 3000 bp)  60                                                                                       60                                                                              
Largest alignment              150463                                                                                   244385                                                                          
NA50                           38179                                                                                    70045                                                                           
NGA50                          37782                                                                                    70045                                                                           
NA75                           20913                                                                                    34267                                                                           
NGA75                          18017                                                                                    31223                                                                           
