All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_145.final.contigs
#Contigs                                   1677                                                                                        
#Contigs (>= 0 bp)                         1677                                                                                        
#Contigs (>= 1000 bp)                      71                                                                                          
Largest contig                             2706                                                                                        
Total length                               997456                                                                                      
Total length (>= 0 bp)                     997456                                                                                      
Total length (>= 1000 bp)                  87541                                                                                       
Reference length                           2813862                                                                                     
N50                                        568                                                                                         
NG50                                       None                                                                                        
N75                                        492                                                                                         
NG75                                       None                                                                                        
L50                                        663                                                                                         
LG50                                       None                                                                                        
L75                                        1135                                                                                        
LG75                                       None                                                                                        
#local misassemblies                       0                                                                                           
#misassemblies                             2                                                                                           
#misassembled contigs                      2                                                                                           
Misassembled contigs length                1366                                                                                        
Misassemblies inter-contig overlap         250                                                                                         
#unaligned contigs                         0 + 57 part                                                                                 
Unaligned contigs length                   1563                                                                                        
#ambiguously mapped contigs                10                                                                                          
Extra bases in ambiguously mapped contigs  -5414                                                                                       
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        34.358                                                                                      
Duplication ratio                          1.025                                                                                       
#genes                                     157 + 1336 part                                                                             
#predicted genes (unique)                  2133                                                                                        
#predicted genes (>= 0 bp)                 2133                                                                                        
#predicted genes (>= 300 bp)               1475                                                                                        
#predicted genes (>= 1500 bp)              0                                                                                           
#predicted genes (>= 3000 bp)              0                                                                                           
#mismatches                                94                                                                                          
#indels                                    751                                                                                         
Indels length                              760                                                                                         
#mismatches per 100 kbp                    9.72                                                                                        
#indels per 100 kbp                        77.68                                                                                       
GC (%)                                     34.04                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               99.831                                                                                      
Largest alignment                          2706                                                                                        
NA50                                       565                                                                                         
NGA50                                      None                                                                                        
NA75                                       490                                                                                         
NGA75                                      None                                                                                        
LA50                                       666                                                                                         
LGA50                                      None                                                                                        
LA75                                       1140                                                                                        
LGA75                                      None                                                                                        
#Mis_misassemblies                         2                                                                                           
#Mis_relocations                           1                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            1                                                                                           
#Mis_misassembled contigs                  2                                                                                           
Mis_Misassembled contigs length            1366                                                                                        
#Mis_local misassemblies                   0                                                                                           
#Mis_short indels (<= 5 bp)                751                                                                                         
#Mis_long indels (> 5 bp)                  0                                                                                           
#Una_fully unaligned contigs               0                                                                                           
Una_Fully unaligned length                 0                                                                                           
#Una_partially unaligned contigs           57                                                                                          
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             1563                                                                                        
GAGE_Contigs #                             1677                                                                                        
GAGE_Min contig                            289                                                                                         
GAGE_Max contig                            2706                                                                                        
GAGE_N50                                   0 COUNT: 0                                                                                  
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         997456                                                                                      
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               1813600(64.45%)                                                                             
GAGE_Missing assembly bases                1975(0.20%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            1002                                                                                        
GAGE_Compressed reference bases            32765                                                                                       
GAGE_Bad trim                              1975                                                                                        
GAGE_Avg idy                               99.91                                                                                       
GAGE_SNPs                                  83                                                                                          
GAGE_Indels < 5bp                          707                                                                                         
GAGE_Indels >= 5                           0                                                                                           
GAGE_Inversions                            1                                                                                           
GAGE_Relocation                            1                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    1676                                                                                        
GAGE_Corrected assembly size               994915                                                                                      
GAGE_Min correct contig                    288                                                                                         
GAGE_Max correct contig                    2706                                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                                  
