All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_39.final.contigs
#Contigs (>= 0 bp)             31766                                                                                      
#Contigs (>= 1000 bp)          0                                                                                          
Total length (>= 0 bp)         3285088                                                                                    
Total length (>= 1000 bp)      0                                                                                          
#Contigs                       250                                                                                        
Largest contig                 413                                                                                        
Total length                   59460                                                                                      
Reference length               2813862                                                                                    
GC (%)                         34.14                                                                                      
Reference GC (%)               32.81                                                                                      
N50                            231                                                                                        
NG50                           None                                                                                       
N75                            213                                                                                        
NG75                           None                                                                                       
#misassemblies                 0                                                                                          
#local misassemblies           0                                                                                          
#unaligned contigs             19 + 0 part                                                                                
Unaligned contigs length       4039                                                                                       
Genome fraction (%)            1.968                                                                                      
Duplication ratio              1.001                                                                                      
#N's per 100 kbp               0.00                                                                                       
#mismatches per 100 kbp        1.81                                                                                       
#indels per 100 kbp            23.47                                                                                      
#genes                         2 + 203 part                                                                               
#predicted genes (unique)      240                                                                                        
#predicted genes (>= 0 bp)     240                                                                                        
#predicted genes (>= 300 bp)   11                                                                                         
#predicted genes (>= 1500 bp)  0                                                                                          
#predicted genes (>= 3000 bp)  0                                                                                          
Largest alignment              413                                                                                        
NA50                           231                                                                                        
NGA50                          None                                                                                       
NA75                           211                                                                                        
NGA75                          None                                                                                       
