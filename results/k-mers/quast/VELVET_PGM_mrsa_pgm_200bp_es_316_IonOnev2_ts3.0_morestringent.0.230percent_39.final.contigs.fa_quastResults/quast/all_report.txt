All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_39.final.contigs
#Contigs                                   250                                                                                        
#Contigs (>= 0 bp)                         31766                                                                                      
#Contigs (>= 1000 bp)                      0                                                                                          
Largest contig                             413                                                                                        
Total length                               59460                                                                                      
Total length (>= 0 bp)                     3285088                                                                                    
Total length (>= 1000 bp)                  0                                                                                          
Reference length                           2813862                                                                                    
N50                                        231                                                                                        
NG50                                       None                                                                                       
N75                                        213                                                                                        
NG75                                       None                                                                                       
L50                                        111                                                                                        
LG50                                       None                                                                                       
L75                                        178                                                                                        
LG75                                       None                                                                                       
#local misassemblies                       0                                                                                          
#misassemblies                             0                                                                                          
#misassembled contigs                      0                                                                                          
Misassembled contigs length                0                                                                                          
Misassemblies inter-contig overlap         0                                                                                          
#unaligned contigs                         19 + 0 part                                                                                
Unaligned contigs length                   4039                                                                                       
#ambiguously mapped contigs                0                                                                                          
Extra bases in ambiguously mapped contigs  0                                                                                          
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        1.968                                                                                      
Duplication ratio                          1.001                                                                                      
#genes                                     2 + 203 part                                                                               
#predicted genes (unique)                  240                                                                                        
#predicted genes (>= 0 bp)                 240                                                                                        
#predicted genes (>= 300 bp)               11                                                                                         
#predicted genes (>= 1500 bp)              0                                                                                          
#predicted genes (>= 3000 bp)              0                                                                                          
#mismatches                                1                                                                                          
#indels                                    13                                                                                         
Indels length                              15                                                                                         
#mismatches per 100 kbp                    1.81                                                                                       
#indels per 100 kbp                        23.47                                                                                      
GC (%)                                     34.14                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               99.969                                                                                     
Largest alignment                          413                                                                                        
NA50                                       231                                                                                        
NGA50                                      None                                                                                       
NA75                                       211                                                                                        
NGA75                                      None                                                                                       
LA50                                       111                                                                                        
LGA50                                      None                                                                                       
LA75                                       179                                                                                        
LGA75                                      None                                                                                       
#Mis_misassemblies                         0                                                                                          
#Mis_relocations                           0                                                                                          
#Mis_translocations                        0                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  0                                                                                          
Mis_Misassembled contigs length            0                                                                                          
#Mis_local misassemblies                   0                                                                                          
#Mis_short indels (<= 5 bp)                13                                                                                         
#Mis_long indels (> 5 bp)                  0                                                                                          
#Una_fully unaligned contigs               19                                                                                         
Una_Fully unaligned length                 4039                                                                                       
#Una_partially unaligned contigs           0                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             0                                                                                          
GAGE_Contigs #                             250                                                                                        
GAGE_Min contig                            200                                                                                        
GAGE_Max contig                            413                                                                                        
GAGE_N50                                   0 COUNT: 0                                                                                 
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         59460                                                                                      
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               2757075(97.98%)                                                                            
GAGE_Missing assembly bases                2628(4.42%)                                                                                
GAGE_Missing assembly contigs              12(4.80%)                                                                                  
GAGE_Duplicated reference bases            0                                                                                          
GAGE_Compressed reference bases            0                                                                                          
GAGE_Bad trim                              102                                                                                        
GAGE_Avg idy                               99.88                                                                                      
GAGE_SNPs                                  2                                                                                          
GAGE_Indels < 5bp                          70                                                                                         
GAGE_Indels >= 5                           0                                                                                          
GAGE_Inversions                            0                                                                                          
GAGE_Relocation                            0                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    237                                                                                        
GAGE_Corrected assembly size               56724                                                                                      
GAGE_Min correct contig                    200                                                                                        
GAGE_Max correct contig                    413                                                                                        
GAGE_Corrected N50                         0 COUNT: 0                                                                                 
