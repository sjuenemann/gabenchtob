All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_21.final.contigs
#Contigs (>= 0 bp)             3495                                                                                 
#Contigs (>= 1000 bp)          979                                                                                  
Total length (>= 0 bp)         5316443                                                                              
Total length (>= 1000 bp)      4854656                                                                              
#Contigs                       1651                                                                                 
Largest contig                 30756                                                                                
Total length                   5169856                                                                              
Reference length               5594470                                                                              
GC (%)                         50.22                                                                                
Reference GC (%)               50.48                                                                                
N50                            6826                                                                                 
NG50                           6313                                                                                 
N75                            3664                                                                                 
NG75                           2760                                                                                 
#misassemblies                 26                                                                                   
#local misassemblies           13                                                                                   
#unaligned contigs             3 + 5 part                                                                           
Unaligned contigs length       1154                                                                                 
Genome fraction (%)            91.057                                                                               
Duplication ratio              1.002                                                                                
#N's per 100 kbp               6.77                                                                                 
#mismatches per 100 kbp        15.61                                                                                
#indels per 100 kbp            71.34                                                                                
#genes                         4037 + 852 part                                                                      
#predicted genes (unique)      6948                                                                                 
#predicted genes (>= 0 bp)     6948                                                                                 
#predicted genes (>= 300 bp)   5104                                                                                 
#predicted genes (>= 1500 bp)  329                                                                                  
#predicted genes (>= 3000 bp)  10                                                                                   
Largest alignment              30756                                                                                
NA50                           6724                                                                                 
NGA50                          6269                                                                                 
NA75                           3571                                                                                 
NGA75                          2708                                                                                 
