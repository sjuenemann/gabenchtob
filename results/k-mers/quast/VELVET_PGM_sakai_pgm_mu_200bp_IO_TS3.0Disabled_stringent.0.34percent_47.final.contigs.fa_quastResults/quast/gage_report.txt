All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_47.final.contigs
GAGE_Contigs #                   1412                                                                                 
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  31171                                                                                
GAGE_N50                         8205 COUNT: 208                                                                      
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5282820                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     162891(2.91%)                                                                        
GAGE_Missing assembly bases      225(0.00%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  1593                                                                                 
GAGE_Compressed reference bases  180904                                                                               
GAGE_Bad trim                    164                                                                                  
GAGE_Avg idy                     99.94                                                                                
GAGE_SNPs                        258                                                                                  
GAGE_Indels < 5bp                2725                                                                                 
GAGE_Indels >= 5                 9                                                                                    
GAGE_Inversions                  3                                                                                    
GAGE_Relocation                  6                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          1426                                                                                 
GAGE_Corrected assembly size     5284792                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          31177                                                                                
GAGE_Corrected N50               8069 COUNT: 210                                                                      
