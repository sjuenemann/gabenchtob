All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_88-unitigs
#Contigs (>= 0 bp)             4318                                                                             
#Contigs (>= 1000 bp)          770                                                                              
Total length (>= 0 bp)         5764015                                                                          
Total length (>= 1000 bp)      5084470                                                                          
#Contigs                       1380                                                                             
Largest contig                 49768                                                                            
Total length                   5334150                                                                          
Reference length               5594470                                                                          
GC (%)                         50.27                                                                            
Reference GC (%)               50.48                                                                            
N50                            9729                                                                             
NG50                           9422                                                                             
N75                            5150                                                                             
NG75                           4322                                                                             
#misassemblies                 4                                                                                
#local misassemblies           4                                                                                
#unaligned contigs             0 + 1 part                                                                       
Unaligned contigs length       30                                                                               
Genome fraction (%)            93.735                                                                           
Duplication ratio              1.003                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        1.39                                                                             
#indels per 100 kbp            29.10                                                                            
#genes                         4362 + 760 part                                                                  
#predicted genes (unique)      6615                                                                             
#predicted genes (>= 0 bp)     6622                                                                             
#predicted genes (>= 300 bp)   4959                                                                             
#predicted genes (>= 1500 bp)  472                                                                              
#predicted genes (>= 3000 bp)  31                                                                               
Largest alignment              49768                                                                            
NA50                           9729                                                                             
NGA50                          9422                                                                             
NA75                           5150                                                                             
NGA75                          4322                                                                             
