All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_88-unitigs
#Mis_misassemblies               4                                                                                
#Mis_relocations                 3                                                                                
#Mis_translocations              0                                                                                
#Mis_inversions                  1                                                                                
#Mis_misassembled contigs        4                                                                                
Mis_Misassembled contigs length  35278                                                                            
#Mis_local misassemblies         4                                                                                
#mismatches                      73                                                                               
#indels                          1526                                                                             
#Mis_short indels (<= 5 bp)      1524                                                                             
#Mis_long indels (> 5 bp)        2                                                                                
Indels length                    1619                                                                             
