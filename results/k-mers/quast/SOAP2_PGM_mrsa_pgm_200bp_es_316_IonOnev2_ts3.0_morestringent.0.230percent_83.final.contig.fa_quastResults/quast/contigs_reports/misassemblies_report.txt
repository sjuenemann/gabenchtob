All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_83.final.contig
#Mis_misassemblies               61                                                                                       
#Mis_relocations                 19                                                                                       
#Mis_translocations              1                                                                                        
#Mis_inversions                  41                                                                                       
#Mis_misassembled contigs        61                                                                                       
Mis_Misassembled contigs length  27749                                                                                    
#Mis_local misassemblies         2                                                                                        
#mismatches                      214                                                                                      
#indels                          1686                                                                                     
#Mis_short indels (<= 5 bp)      1686                                                                                     
#Mis_long indels (> 5 bp)        0                                                                                        
Indels length                    1740                                                                                     
