All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                         #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_63.final.scaf_broken  68673                         16161202                    4                                 0                      0                                242                             0   
SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_63.final.scaf         68675                         16162394                    7                                 0                      1                                850                             1005
