All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_161.final.contigs
#Contigs                                   4264                                                                                  
#Contigs (>= 0 bp)                         4264                                                                                  
#Contigs (>= 1000 bp)                      1130                                                                                  
Largest contig                             4120                                                                                  
Total length                               3694045                                                                               
Total length (>= 0 bp)                     3694045                                                                               
Total length (>= 1000 bp)                  1599849                                                                               
Reference length                           5594470                                                                               
N50                                        902                                                                                   
NG50                                       649                                                                                   
N75                                        653                                                                                   
NG75                                       None                                                                                  
L50                                        1391                                                                                  
LG50                                       2644                                                                                  
L75                                        2603                                                                                  
LG75                                       None                                                                                  
#local misassemblies                       2                                                                                     
#misassemblies                             13                                                                                    
#misassembled contigs                      13                                                                                    
Misassembled contigs length                14409                                                                                 
Misassemblies inter-contig overlap         2578                                                                                  
#unaligned contigs                         0 + 116 part                                                                          
Unaligned contigs length                   3531                                                                                  
#ambiguously mapped contigs                80                                                                                    
Extra bases in ambiguously mapped contigs  -54480                                                                                
#N's                                       40                                                                                    
#N's per 100 kbp                           1.08                                                                                  
Genome fraction (%)                        62.617                                                                                
Duplication ratio                          1.039                                                                                 
#genes                                     1029 + 3154 part                                                                      
#predicted genes (unique)                  6692                                                                                  
#predicted genes (>= 0 bp)                 6696                                                                                  
#predicted genes (>= 300 bp)               4497                                                                                  
#predicted genes (>= 1500 bp)              68                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                     
#mismatches                                248                                                                                   
#indels                                    2713                                                                                  
Indels length                              2758                                                                                  
#mismatches per 100 kbp                    7.08                                                                                  
#indels per 100 kbp                        77.45                                                                                 
GC (%)                                     51.41                                                                                 
Reference GC (%)                           50.48                                                                                 
Average %IDY                               99.644                                                                                
Largest alignment                          4109                                                                                  
NA50                                       892                                                                                   
NGA50                                      640                                                                                   
NA75                                       646                                                                                   
NGA75                                      None                                                                                  
LA50                                       1404                                                                                  
LGA50                                      2673                                                                                  
LA75                                       2632                                                                                  
LGA75                                      None                                                                                  
#Mis_misassemblies                         13                                                                                    
#Mis_relocations                           5                                                                                     
#Mis_translocations                        0                                                                                     
#Mis_inversions                            8                                                                                     
#Mis_misassembled contigs                  13                                                                                    
Mis_Misassembled contigs length            14409                                                                                 
#Mis_local misassemblies                   2                                                                                     
#Mis_short indels (<= 5 bp)                2713                                                                                  
#Mis_long indels (> 5 bp)                  0                                                                                     
#Una_fully unaligned contigs               0                                                                                     
Una_Fully unaligned length                 0                                                                                     
#Una_partially unaligned contigs           116                                                                                   
#Una_with misassembly                      0                                                                                     
#Una_both parts are significant            0                                                                                     
Una_Partially unaligned length             3531                                                                                  
GAGE_Contigs #                             4264                                                                                  
GAGE_Min contig                            321                                                                                   
GAGE_Max contig                            4120                                                                                  
GAGE_N50                                   649 COUNT: 2644                                                                       
GAGE_Genome size                           5594470                                                                               
GAGE_Assembly size                         3694045                                                                               
GAGE_Chaff bases                           0                                                                                     
GAGE_Missing reference bases               1859721(33.24%)                                                                       
GAGE_Missing assembly bases                4704(0.13%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                              
GAGE_Duplicated reference bases            12592                                                                                 
GAGE_Compressed reference bases            229670                                                                                
GAGE_Bad trim                              4683                                                                                  
GAGE_Avg idy                               99.91                                                                                 
GAGE_SNPs                                  175                                                                                   
GAGE_Indels < 5bp                          2455                                                                                  
GAGE_Indels >= 5                           1                                                                                     
GAGE_Inversions                            5                                                                                     
GAGE_Relocation                            3                                                                                     
GAGE_Translocation                         0                                                                                     
GAGE_Corrected contig #                    4243                                                                                  
GAGE_Corrected assembly size               3680712                                                                               
GAGE_Min correct contig                    270                                                                                   
GAGE_Max correct contig                    4030                                                                                  
GAGE_Corrected N50                         648 COUNT: 2648                                                                       
