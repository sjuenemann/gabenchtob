All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_65.final.contig
#Contigs (>= 0 bp)             42948                                                                                    
#Contigs (>= 1000 bp)          125                                                                                      
Total length (>= 0 bp)         7391832                                                                                  
Total length (>= 1000 bp)      148563                                                                                   
#Contigs                       13040                                                                                    
Largest contig                 1900                                                                                     
Total length                   3938023                                                                                  
Reference length               2813862                                                                                  
GC (%)                         32.32                                                                                    
Reference GC (%)               32.81                                                                                    
N50                            271                                                                                      
NG50                           385                                                                                      
N75                            218                                                                                      
NG75                           255                                                                                      
#misassemblies                 56                                                                                       
#local misassemblies           0                                                                                        
#unaligned contigs             3481 + 1024 part                                                                         
Unaligned contigs length       861578                                                                                   
Genome fraction (%)            86.359                                                                                   
Duplication ratio              1.264                                                                                    
#N's per 100 kbp               0.00                                                                                     
#mismatches per 100 kbp        52.43                                                                                    
#indels per 100 kbp            404.48                                                                                   
#genes                         288 + 2292 part                                                                          
#predicted genes (unique)      10613                                                                                    
#predicted genes (>= 0 bp)     10636                                                                                    
#predicted genes (>= 300 bp)   2624                                                                                     
#predicted genes (>= 1500 bp)  0                                                                                        
#predicted genes (>= 3000 bp)  0                                                                                        
Largest alignment              1900                                                                                     
NA50                           269                                                                                      
NGA50                          384                                                                                      
NA75                           195                                                                                      
NGA75                          247                                                                                      
