All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_127.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_127.final.scaf
#Contigs (>= 0 bp)             3910                                                                            1221                                                                   
#Contigs (>= 1000 bp)          1740                                                                            483                                                                    
Total length (>= 0 bp)         5346736                                                                         5445511                                                                
Total length (>= 1000 bp)      4320418                                                                         5234252                                                                
#Contigs                       3620                                                                            956                                                                    
Largest contig                 11232                                                                           95162                                                                  
Total length                   5301323                                                                         5404192                                                                
Reference length               5594470                                                                         5594470                                                                
GC (%)                         50.48                                                                           50.48                                                                  
Reference GC (%)               50.48                                                                           50.48                                                                  
N50                            2359                                                                            19455                                                                  
NG50                           2241                                                                            19003                                                                  
N75                            1280                                                                            9432                                                                   
NG75                           1091                                                                            8627                                                                   
#misassemblies                 3                                                                               16                                                                     
#local misassemblies           11                                                                              406                                                                    
#unaligned contigs             7 + 0 part                                                                      16 + 12 part                                                           
Unaligned contigs length       9330                                                                            40090                                                                  
Genome fraction (%)            91.954                                                                          92.107                                                                 
Duplication ratio              1.002                                                                           1.018                                                                  
#N's per 100 kbp               15.88                                                                           1843.33                                                                
#mismatches per 100 kbp        2.78                                                                            4.83                                                                   
#indels per 100 kbp            4.41                                                                            407.20                                                                 
#genes                         2942 + 2216 part                                                                4299 + 814 part                                                        
#predicted genes (unique)      7630                                                                            6602                                                                   
#predicted genes (>= 0 bp)     7656                                                                            6623                                                                   
#predicted genes (>= 300 bp)   5323                                                                            5089                                                                   
#predicted genes (>= 1500 bp)  364                                                                             437                                                                    
#predicted genes (>= 3000 bp)  20                                                                              32                                                                     
Largest alignment              11230                                                                           94558                                                                  
NA50                           2332                                                                            19322                                                                  
NGA50                          2215                                                                            18729                                                                  
NA75                           1248                                                                            9258                                                                   
NGA75                          1066                                                                            8374                                                                   
