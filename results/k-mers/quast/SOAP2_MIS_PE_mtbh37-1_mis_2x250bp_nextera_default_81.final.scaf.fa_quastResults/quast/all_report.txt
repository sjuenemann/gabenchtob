All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_81.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_81.final.scaf
#Contigs                                   76934                                                                   76883                                                          
#Contigs (>= 0 bp)                         81297                                                                   81232                                                          
#Contigs (>= 1000 bp)                      1577                                                                    1594                                                           
Largest contig                             8017                                                                    8017                                                           
Total length                               22577425                                                                22582940                                                       
Total length (>= 0 bp)                     23237473                                                                23240679                                                       
Total length (>= 1000 bp)                  2804975                                                                 2836562                                                        
Reference length                           4411532                                                                 4411532                                                        
N50                                        251                                                                     251                                                            
NG50                                       1316                                                                    1330                                                           
N75                                        250                                                                     250                                                            
NG75                                       792                                                                     805                                                            
L50                                        31640                                                                   31580                                                          
LG50                                       1053                                                                    1045                                                           
L75                                        54167                                                                   54112                                                          
LG75                                       2145                                                                    2123                                                           
#local misassemblies                       3                                                                       8                                                              
#misassemblies                             14                                                                      15                                                             
#misassembled contigs                      14                                                                      15                                                             
Misassembled contigs length                4547                                                                    6551                                                           
Misassemblies inter-contig overlap         238                                                                     96                                                             
#unaligned contigs                         62896 + 3346 part                                                       62909 + 3347 part                                              
Unaligned contigs length                   16101546                                                                16121157                                                       
#ambiguously mapped contigs                100                                                                     100                                                            
Extra bases in ambiguously mapped contigs  -29956                                                                  -29956                                                         
#N's                                       22                                                                      3228                                                           
#N's per 100 kbp                           0.10                                                                    14.29                                                          
Genome fraction (%)                        98.002                                                                  97.819                                                         
Duplication ratio                          1.491                                                                   1.490                                                          
#genes                                     1608 + 2443 part                                                        1608 + 2443 part                                               
#predicted genes (unique)                  53738                                                                   53730                                                          
#predicted genes (>= 0 bp)                 53827                                                                   53818                                                          
#predicted genes (>= 300 bp)               5074                                                                    5074                                                           
#predicted genes (>= 1500 bp)              173                                                                     175                                                            
#predicted genes (>= 3000 bp)              1                                                                       1                                                              
#mismatches                                8396                                                                    8470                                                           
#indels                                    246                                                                     526                                                            
Indels length                              294                                                                     872                                                            
#mismatches per 100 kbp                    194.20                                                                  196.28                                                         
#indels per 100 kbp                        5.69                                                                    12.19                                                          
GC (%)                                     70.16                                                                   70.16                                                          
Reference GC (%)                           65.61                                                                   65.61                                                          
Average %IDY                               98.051                                                                  98.041                                                         
Largest alignment                          8017                                                                    8017                                                           
NA50                                       None                                                                    None                                                           
NGA50                                      1316                                                                    1321                                                           
NA75                                       None                                                                    None                                                           
NGA75                                      790                                                                     795                                                            
LA50                                       None                                                                    None                                                           
LGA50                                      1053                                                                    1049                                                           
LA75                                       None                                                                    None                                                           
LGA75                                      2148                                                                    2138                                                           
#Mis_misassemblies                         14                                                                      15                                                             
#Mis_relocations                           7                                                                       8                                                              
#Mis_translocations                        0                                                                       0                                                              
#Mis_inversions                            7                                                                       7                                                              
#Mis_misassembled contigs                  14                                                                      15                                                             
Mis_Misassembled contigs length            4547                                                                    6551                                                           
#Mis_local misassemblies                   3                                                                       8                                                              
#Mis_short indels (<= 5 bp)                243                                                                     512                                                            
#Mis_long indels (> 5 bp)                  3                                                                       14                                                             
#Una_fully unaligned contigs               62896                                                                   62909                                                          
Una_Fully unaligned length                 15748768                                                                15764823                                                       
#Una_partially unaligned contigs           3346                                                                    3347                                                           
#Una_with misassembly                      0                                                                       0                                                              
#Una_both parts are significant            0                                                                       4                                                              
Una_Partially unaligned length             352778                                                                  356334                                                         
GAGE_Contigs #                             76934                                                                   76883                                                          
GAGE_Min contig                            200                                                                     200                                                            
GAGE_Max contig                            8017                                                                    8017                                                           
GAGE_N50                                   1316 COUNT: 1053                                                        1330 COUNT: 1045                                               
GAGE_Genome size                           4411532                                                                 4411532                                                        
GAGE_Assembly size                         22577425                                                                22582940                                                       
GAGE_Chaff bases                           0                                                                       0                                                              
GAGE_Missing reference bases               22797(0.52%)                                                            22275(0.50%)                                                   
GAGE_Missing assembly bases                15215600(67.39%)                                                        15219749(67.39%)                                               
GAGE_Missing assembly contigs              58949(76.62%)                                                           58946(76.67%)                                                  
GAGE_Duplicated reference bases            2686570                                                                 2687392                                                        
GAGE_Compressed reference bases            64839                                                                   64597                                                          
GAGE_Bad trim                              454492                                                                  456503                                                         
GAGE_Avg idy                               99.90                                                                   99.90                                                          
GAGE_SNPs                                  1547                                                                    1531                                                           
GAGE_Indels < 5bp                          65                                                                      72                                                             
GAGE_Indels >= 5                           5                                                                       45                                                             
GAGE_Inversions                            0                                                                       0                                                              
GAGE_Relocation                            1                                                                       2                                                              
GAGE_Translocation                         0                                                                       0                                                              
GAGE_Corrected contig #                    5384                                                                    5379                                                           
GAGE_Corrected assembly size               4654742                                                                 4653522                                                        
GAGE_Min correct contig                    200                                                                     200                                                            
GAGE_Max correct contig                    8017                                                                    8017                                                           
GAGE_Corrected N50                         1316 COUNT: 1054                                                        1316 COUNT: 1054                                               
