All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_115.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_115.final.scaf
#Contigs                                   601                                                                            275                                                                   
#Contigs (>= 0 bp)                         714                                                                            384                                                                   
#Contigs (>= 1000 bp)                      427                                                                            188                                                                   
Largest contig                             39755                                                                          112919                                                                
Total length                               2796540                                                                        2806601                                                               
Total length (>= 0 bp)                     2812607                                                                        2822105                                                               
Total length (>= 1000 bp)                  2718226                                                                        2776616                                                               
Reference length                           2813862                                                                        2813862                                                               
N50                                        11265                                                                          31111                                                                 
NG50                                       11165                                                                          31111                                                                 
N75                                        4869                                                                           13373                                                                 
NG75                                       4842                                                                           13373                                                                 
L50                                        77                                                                             28                                                                    
LG50                                       78                                                                             28                                                                    
L75                                        177                                                                            63                                                                    
LG75                                       179                                                                            63                                                                    
#local misassemblies                       1                                                                              20                                                                    
#misassemblies                             4                                                                              10                                                                    
#misassembled contigs                      4                                                                              7                                                                     
Misassembled contigs length                29835                                                                          103323                                                                
Misassemblies inter-contig overlap         1                                                                              177                                                                   
#unaligned contigs                         3 + 0 part                                                                     4 + 1 part                                                            
Unaligned contigs length                   5847                                                                           11427                                                                 
#ambiguously mapped contigs                48                                                                             44                                                                    
Extra bases in ambiguously mapped contigs  -15403                                                                         -14461                                                                
#N's                                       393                                                                            9891                                                                  
#N's per 100 kbp                           14.05                                                                          352.42                                                                
Genome fraction (%)                        98.159                                                                         98.128                                                                
Duplication ratio                          1.005                                                                          1.007                                                                 
#genes                                     2365 + 336 part                                                                2555 + 138 part                                                       
#predicted genes (unique)                  2974                                                                           2864                                                                  
#predicted genes (>= 0 bp)                 2978                                                                           2867                                                                  
#predicted genes (>= 300 bp)               2380                                                                           2357                                                                  
#predicted genes (>= 1500 bp)              258                                                                            269                                                                   
#predicted genes (>= 3000 bp)              23                                                                             22                                                                    
#mismatches                                35                                                                             42                                                                    
#indels                                    122                                                                            2055                                                                  
Indels length                              1373                                                                           6519                                                                  
#mismatches per 100 kbp                    1.27                                                                           1.52                                                                  
#indels per 100 kbp                        4.42                                                                           74.43                                                                 
GC (%)                                     32.75                                                                          32.75                                                                 
Reference GC (%)                           32.81                                                                          32.81                                                                 
Average %IDY                               99.356                                                                         99.129                                                                
Largest alignment                          39755                                                                          112919                                                                
NA50                                       11165                                                                          30506                                                                 
NGA50                                      11090                                                                          30506                                                                 
NA75                                       4868                                                                           13373                                                                 
NGA75                                      4826                                                                           13373                                                                 
LA50                                       78                                                                             29                                                                    
LGA50                                      79                                                                             29                                                                    
LA75                                       178                                                                            64                                                                    
LGA75                                      180                                                                            64                                                                    
#Mis_misassemblies                         4                                                                              10                                                                    
#Mis_relocations                           4                                                                              10                                                                    
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  4                                                                              7                                                                     
Mis_Misassembled contigs length            29835                                                                          103323                                                                
#Mis_local misassemblies                   1                                                                              20                                                                    
#Mis_short indels (<= 5 bp)                45                                                                             1819                                                                  
#Mis_long indels (> 5 bp)                  77                                                                             236                                                                   
#Una_fully unaligned contigs               3                                                                              4                                                                     
Una_Fully unaligned length                 5847                                                                           7051                                                                  
#Una_partially unaligned contigs           0                                                                              1                                                                     
#Una_with misassembly                      0                                                                              1                                                                     
#Una_both parts are significant            0                                                                              1                                                                     
Una_Partially unaligned length             0                                                                              4376                                                                  
GAGE_Contigs #                             601                                                                            275                                                                   
GAGE_Min contig                            207                                                                            207                                                                   
GAGE_Max contig                            39755                                                                          112919                                                                
GAGE_N50                                   11165 COUNT: 78                                                                31111 COUNT: 28                                                       
GAGE_Genome size                           2813862                                                                        2813862                                                               
GAGE_Assembly size                         2796540                                                                        2806601                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               14715(0.52%)                                                                   13412(0.48%)                                                          
GAGE_Missing assembly bases                6098(0.22%)                                                                    14418(0.51%)                                                          
GAGE_Missing assembly contigs              3(0.50%)                                                                       3(1.09%)                                                              
GAGE_Duplicated reference bases            2831                                                                           3056                                                                  
GAGE_Compressed reference bases            43989                                                                          43712                                                                 
GAGE_Bad trim                              1                                                                              54                                                                    
GAGE_Avg idy                               99.99                                                                          99.95                                                                 
GAGE_SNPs                                  35                                                                             35                                                                    
GAGE_Indels < 5bp                          104                                                                            282                                                                   
GAGE_Indels >= 5                           77                                                                             318                                                                   
GAGE_Inversions                            0                                                                              0                                                                     
GAGE_Relocation                            2                                                                              3                                                                     
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    664                                                                            600                                                                   
GAGE_Corrected assembly size               2787403                                                                        2788438                                                               
GAGE_Min correct contig                    207                                                                            207                                                                   
GAGE_Max correct contig                    39755                                                                          53728                                                                 
GAGE_Corrected N50                         8603 COUNT: 93                                                                 10195 COUNT: 81                                                       
