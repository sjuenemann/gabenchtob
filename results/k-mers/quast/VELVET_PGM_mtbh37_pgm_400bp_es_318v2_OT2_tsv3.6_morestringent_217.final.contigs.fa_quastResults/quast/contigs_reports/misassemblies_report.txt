All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_217.final.contigs
#Mis_misassemblies               15                                                                             
#Mis_relocations                 15                                                                             
#Mis_translocations              0                                                                              
#Mis_inversions                  0                                                                              
#Mis_misassembled contigs        15                                                                             
Mis_Misassembled contigs length  41917                                                                          
#Mis_local misassemblies         7                                                                              
#mismatches                      70                                                                             
#indels                          1884                                                                           
#Mis_short indels (<= 5 bp)      1882                                                                           
#Mis_long indels (> 5 bp)        2                                                                              
Indels length                    1977                                                                           
