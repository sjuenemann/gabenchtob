All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_68-unitigs
#Contigs (>= 0 bp)             10464                                                                     
#Contigs (>= 1000 bp)          1330                                                                      
Total length (>= 0 bp)         5114654                                                                   
Total length (>= 1000 bp)      3595515                                                                   
#Contigs                       2347                                                                      
Largest contig                 13194                                                                     
Total length                   4141015                                                                   
Reference length               4411532                                                                   
GC (%)                         65.35                                                                     
Reference GC (%)               65.61                                                                     
N50                            2843                                                                      
NG50                           2660                                                                      
N75                            1554                                                                      
NG75                           1321                                                                      
#misassemblies                 0                                                                         
#local misassemblies           15                                                                        
#unaligned contigs             1 + 1 part                                                                
Unaligned contigs length       685                                                                       
Genome fraction (%)            93.486                                                                    
Duplication ratio              1.001                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        3.20                                                                      
#indels per 100 kbp            17.43                                                                     
#genes                         2388 + 1628 part                                                          
#predicted genes (unique)      5723                                                                      
#predicted genes (>= 0 bp)     5727                                                                      
#predicted genes (>= 300 bp)   4191                                                                      
#predicted genes (>= 1500 bp)  289                                                                       
#predicted genes (>= 3000 bp)  16                                                                        
Largest alignment              13194                                                                     
NA50                           2843                                                                      
NGA50                          2660                                                                      
NA75                           1554                                                                      
NGA75                          1321                                                                      
