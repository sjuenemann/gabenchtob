All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_61.final.contigs
#Contigs                                   6865                                                                       
#Contigs (>= 0 bp)                         17834                                                                      
#Contigs (>= 1000 bp)                      2                                                                          
Largest contig                             1122                                                                       
Total length                               2049146                                                                    
Total length (>= 0 bp)                     3722027                                                                    
Total length (>= 1000 bp)                  2135                                                                       
Reference length                           5594470                                                                    
N50                                        296                                                                        
NG50                                       None                                                                       
N75                                        239                                                                        
NG75                                       None                                                                       
L50                                        2574                                                                       
LG50                                       None                                                                       
L75                                        4515                                                                       
LG75                                       None                                                                       
#local misassemblies                       3                                                                          
#misassemblies                             0                                                                          
#misassembled contigs                      0                                                                          
Misassembled contigs length                0                                                                          
Misassemblies inter-contig overlap         472                                                                        
#unaligned contigs                         9 + 7 part                                                                 
Unaligned contigs length                   2230                                                                       
#ambiguously mapped contigs                35                                                                         
Extra bases in ambiguously mapped contigs  -8769                                                                      
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        36.248                                                                     
Duplication ratio                          1.005                                                                      
#genes                                     78 + 3507 part                                                             
#predicted genes (unique)                  7012                                                                       
#predicted genes (>= 0 bp)                 7012                                                                       
#predicted genes (>= 300 bp)               1813                                                                       
#predicted genes (>= 1500 bp)              0                                                                          
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                52                                                                         
#indels                                    465                                                                        
Indels length                              496                                                                        
#mismatches per 100 kbp                    2.56                                                                       
#indels per 100 kbp                        22.93                                                                      
GC (%)                                     51.36                                                                      
Reference GC (%)                           50.48                                                                      
Average %IDY                               99.969                                                                     
Largest alignment                          1122                                                                       
NA50                                       296                                                                        
NGA50                                      None                                                                       
NA75                                       238                                                                        
NGA75                                      None                                                                       
LA50                                       2576                                                                       
LGA50                                      None                                                                       
LA75                                       4520                                                                       
LGA75                                      None                                                                       
#Mis_misassemblies                         0                                                                          
#Mis_relocations                           0                                                                          
#Mis_translocations                        0                                                                          
#Mis_inversions                            0                                                                          
#Mis_misassembled contigs                  0                                                                          
Mis_Misassembled contigs length            0                                                                          
#Mis_local misassemblies                   3                                                                          
#Mis_short indels (<= 5 bp)                465                                                                        
#Mis_long indels (> 5 bp)                  0                                                                          
#Una_fully unaligned contigs               9                                                                          
Una_Fully unaligned length                 1857                                                                       
#Una_partially unaligned contigs           7                                                                          
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             373                                                                        
GAGE_Contigs #                             6865                                                                       
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            1122                                                                       
GAGE_N50                                   0 COUNT: 0                                                                 
GAGE_Genome size                           5594470                                                                    
GAGE_Assembly size                         2049146                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               3537048(63.22%)                                                            
GAGE_Missing assembly bases                693(0.03%)                                                                 
GAGE_Missing assembly contigs              1(0.01%)                                                                   
GAGE_Duplicated reference bases            214                                                                        
GAGE_Compressed reference bases            20732                                                                      
GAGE_Bad trim                              478                                                                        
GAGE_Avg idy                               99.97                                                                      
GAGE_SNPs                                  39                                                                         
GAGE_Indels < 5bp                          532                                                                        
GAGE_Indels >= 5                           1                                                                          
GAGE_Inversions                            0                                                                          
GAGE_Relocation                            2                                                                          
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    6854                                                                       
GAGE_Corrected assembly size               2046504                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    1123                                                                       
GAGE_Corrected N50                         0 COUNT: 0                                                                 
