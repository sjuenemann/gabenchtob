All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_21.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_21.final.contigs
#Mis_misassemblies               0                                                                                       0                                                                              
#Mis_relocations                 0                                                                                       0                                                                              
#Mis_translocations              0                                                                                       0                                                                              
#Mis_inversions                  0                                                                                       0                                                                              
#Mis_misassembled contigs        0                                                                                       0                                                                              
Mis_Misassembled contigs length  0                                                                                       0                                                                              
#Mis_local misassemblies         0                                                                                       0                                                                              
#mismatches                      27                                                                                      27                                                                             
#indels                          11                                                                                      11                                                                             
#Mis_short indels (<= 5 bp)      11                                                                                      11                                                                             
#Mis_long indels (> 5 bp)        0                                                                                       0                                                                              
Indels length                    11                                                                                      11                                                                             
