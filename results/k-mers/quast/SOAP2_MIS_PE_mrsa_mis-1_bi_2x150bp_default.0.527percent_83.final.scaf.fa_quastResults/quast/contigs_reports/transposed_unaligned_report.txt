All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                      #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_83.final.scaf_broken  1                             5385                        0                                 0                      0                                0                               41  
SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_83.final.scaf         2                             6395                        0                                 0                      0                                0                               3886
