All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_33.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_33.final.scaf
#Contigs                                   36831                                                                   36832                                                          
#Contigs (>= 0 bp)                         202016                                                                  202015                                                         
#Contigs (>= 1000 bp)                      1                                                                       1                                                              
Largest contig                             1072                                                                    1072                                                           
Total length                               8632249                                                                 8632568                                                        
Total length (>= 0 bp)                     31136514                                                                31136629                                                       
Total length (>= 1000 bp)                  1072                                                                    1072                                                           
Reference length                           4411532                                                                 4411532                                                        
N50                                        235                                                                     235                                                            
NG50                                       250                                                                     250                                                            
N75                                        216                                                                     216                                                            
NG75                                       244                                                                     244                                                            
L50                                        16875                                                                   16875                                                          
LG50                                       8222                                                                    8221                                                           
L75                                        26444                                                                   26445                                                          
LG75                                       12672                                                                   12672                                                          
#local misassemblies                       1                                                                       1                                                              
#misassemblies                             0                                                                       0                                                              
#misassembled contigs                      0                                                                       0                                                              
Misassembled contigs length                0                                                                       0                                                              
Misassemblies inter-contig overlap         8                                                                       8                                                              
#unaligned contigs                         33363 + 10 part                                                         33363 + 11 part                                                
Unaligned contigs length                   7674338                                                                 7674489                                                        
#ambiguously mapped contigs                3                                                                       3                                                              
Extra bases in ambiguously mapped contigs  -657                                                                    -657                                                           
#N's                                       0                                                                       115                                                            
#N's per 100 kbp                           0.00                                                                    1.33                                                           
Genome fraction (%)                        21.517                                                                  21.521                                                         
Duplication ratio                          1.008                                                                   1.008                                                          
#genes                                     32 + 2222 part                                                          32 + 2222 part                                                 
#predicted genes (unique)                  18927                                                                   18928                                                          
#predicted genes (>= 0 bp)                 18927                                                                   18928                                                          
#predicted genes (>= 300 bp)               611                                                                     611                                                            
#predicted genes (>= 1500 bp)              0                                                                       0                                                              
#predicted genes (>= 3000 bp)              0                                                                       0                                                              
#mismatches                                131                                                                     131                                                            
#indels                                    7                                                                       7                                                              
Indels length                              8                                                                       8                                                              
#mismatches per 100 kbp                    13.80                                                                   13.80                                                          
#indels per 100 kbp                        0.74                                                                    0.74                                                           
GC (%)                                     71.82                                                                   71.82                                                          
Reference GC (%)                           65.61                                                                   65.61                                                          
Average %IDY                               99.982                                                                  99.982                                                         
Largest alignment                          1072                                                                    1072                                                           
NA50                                       None                                                                    None                                                           
NGA50                                      None                                                                    None                                                           
NA75                                       None                                                                    None                                                           
NGA75                                      None                                                                    None                                                           
LA50                                       None                                                                    None                                                           
LGA50                                      None                                                                    None                                                           
LA75                                       None                                                                    None                                                           
LGA75                                      None                                                                    None                                                           
#Mis_misassemblies                         0                                                                       0                                                              
#Mis_relocations                           0                                                                       0                                                              
#Mis_translocations                        0                                                                       0                                                              
#Mis_inversions                            0                                                                       0                                                              
#Mis_misassembled contigs                  0                                                                       0                                                              
Mis_Misassembled contigs length            0                                                                       0                                                              
#Mis_local misassemblies                   1                                                                       1                                                              
#Mis_short indels (<= 5 bp)                7                                                                       7                                                              
#Mis_long indels (> 5 bp)                  0                                                                       0                                                              
#Una_fully unaligned contigs               33363                                                                   33363                                                          
Una_Fully unaligned length                 7673728                                                                 7673728                                                        
#Una_partially unaligned contigs           10                                                                      11                                                             
#Una_with misassembly                      0                                                                       0                                                              
#Una_both parts are significant            0                                                                       0                                                              
Una_Partially unaligned length             610                                                                     761                                                            
GAGE_Contigs #                             36831                                                                   36832                                                          
GAGE_Min contig                            200                                                                     200                                                            
GAGE_Max contig                            1072                                                                    1072                                                           
GAGE_N50                                   250 COUNT: 8222                                                         250 COUNT: 8221                                                
GAGE_Genome size                           4411532                                                                 4411532                                                        
GAGE_Assembly size                         8632249                                                                 8632568                                                        
GAGE_Chaff bases                           0                                                                       0                                                              
GAGE_Missing reference bases               3453439(78.28%)                                                         3453271(78.28%)                                                
GAGE_Missing assembly bases                7666424(88.81%)                                                         7666575(88.81%)                                                
GAGE_Missing assembly contigs              33314(90.45%)                                                           33314(90.45%)                                                  
GAGE_Duplicated reference bases            207                                                                     207                                                            
GAGE_Compressed reference bases            768                                                                     768                                                            
GAGE_Bad trim                              3483                                                                    3634                                                           
GAGE_Avg idy                               99.95                                                                   99.95                                                          
GAGE_SNPs                                  415                                                                     415                                                            
GAGE_Indels < 5bp                          8                                                                       8                                                              
GAGE_Indels >= 5                           1                                                                       1                                                              
GAGE_Inversions                            0                                                                       0                                                              
GAGE_Relocation                            0                                                                       0                                                              
GAGE_Translocation                         0                                                                       0                                                              
GAGE_Corrected contig #                    3481                                                                    3481                                                           
GAGE_Corrected assembly size               961001                                                                  961001                                                         
GAGE_Min correct contig                    200                                                                     200                                                            
GAGE_Max correct contig                    1072                                                                    1072                                                           
GAGE_Corrected N50                         0 COUNT: 0                                                              0 COUNT: 0                                                     
