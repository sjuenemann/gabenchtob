All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_85.final.contig
#Contigs                                   3604                                                                                     
#Contigs (>= 0 bp)                         12361                                                                                    
#Contigs (>= 1000 bp)                      1010                                                                                     
Largest contig                             6128                                                                                     
Total length                               2987298                                                                                  
Total length (>= 0 bp)                     4221647                                                                                  
Total length (>= 1000 bp)                  1864097                                                                                  
Reference length                           2813862                                                                                  
N50                                        1326                                                                                     
NG50                                       1409                                                                                     
N75                                        675                                                                                      
NG75                                       788                                                                                      
L50                                        692                                                                                      
LG50                                       629                                                                                      
L75                                        1464                                                                                     
LG75                                       1286                                                                                     
#local misassemblies                       2                                                                                        
#misassemblies                             44                                                                                       
#misassembled contigs                      44                                                                                       
Misassembled contigs length                19939                                                                                    
Misassemblies inter-contig overlap         905                                                                                      
#unaligned contigs                         308 + 192 part                                                                           
Unaligned contigs length                   98741                                                                                    
#ambiguously mapped contigs                13                                                                                       
Extra bases in ambiguously mapped contigs  -4366                                                                                    
#N's                                       0                                                                                        
#N's per 100 kbp                           0.00                                                                                     
Genome fraction (%)                        96.011                                                                                   
Duplication ratio                          1.068                                                                                    
#genes                                     1230 + 1429 part                                                                         
#predicted genes (unique)                  5084                                                                                     
#predicted genes (>= 0 bp)                 5094                                                                                     
#predicted genes (>= 300 bp)               2895                                                                                     
#predicted genes (>= 1500 bp)              102                                                                                      
#predicted genes (>= 3000 bp)              4                                                                                        
#mismatches                                150                                                                                      
#indels                                    1349                                                                                     
Indels length                              1392                                                                                     
#mismatches per 100 kbp                    5.55                                                                                     
#indels per 100 kbp                        49.93                                                                                    
GC (%)                                     32.51                                                                                    
Reference GC (%)                           32.81                                                                                    
Average %IDY                               99.340                                                                                   
Largest alignment                          6128                                                                                     
NA50                                       1324                                                                                     
NGA50                                      1407                                                                                     
NA75                                       673                                                                                      
NGA75                                      788                                                                                      
LA50                                       693                                                                                      
LGA50                                      630                                                                                      
LA75                                       1466                                                                                     
LGA75                                      1287                                                                                     
#Mis_misassemblies                         44                                                                                       
#Mis_relocations                           17                                                                                       
#Mis_translocations                        1                                                                                        
#Mis_inversions                            26                                                                                       
#Mis_misassembled contigs                  44                                                                                       
Mis_Misassembled contigs length            19939                                                                                    
#Mis_local misassemblies                   2                                                                                        
#Mis_short indels (<= 5 bp)                1349                                                                                     
#Mis_long indels (> 5 bp)                  0                                                                                        
#Una_fully unaligned contigs               308                                                                                      
Una_Fully unaligned length                 81846                                                                                    
#Una_partially unaligned contigs           192                                                                                      
#Una_with misassembly                      0                                                                                        
#Una_both parts are significant            0                                                                                        
Una_Partially unaligned length             16895                                                                                    
GAGE_Contigs #                             3604                                                                                     
GAGE_Min contig                            200                                                                                      
GAGE_Max contig                            6128                                                                                     
GAGE_N50                                   1409 COUNT: 629                                                                          
GAGE_Genome size                           2813862                                                                                  
GAGE_Assembly size                         2987298                                                                                  
GAGE_Chaff bases                           0                                                                                        
GAGE_Missing reference bases               91432(3.25%)                                                                             
GAGE_Missing assembly bases                58181(1.95%)                                                                             
GAGE_Missing assembly contigs              133(3.69%)                                                                               
GAGE_Duplicated reference bases            170751                                                                                   
GAGE_Compressed reference bases            22485                                                                                    
GAGE_Bad trim                              22910                                                                                    
GAGE_Avg idy                               99.95                                                                                    
GAGE_SNPs                                  60                                                                                       
GAGE_Indels < 5bp                          719                                                                                      
GAGE_Indels >= 5                           3                                                                                        
GAGE_Inversions                            3                                                                                        
GAGE_Relocation                            2                                                                                        
GAGE_Translocation                         0                                                                                        
GAGE_Corrected contig #                    2742                                                                                     
GAGE_Corrected assembly size               2757734                                                                                  
GAGE_Min correct contig                    200                                                                                      
GAGE_Max correct contig                    6128                                                                                     
GAGE_Corrected N50                         1406 COUNT: 630                                                                          
