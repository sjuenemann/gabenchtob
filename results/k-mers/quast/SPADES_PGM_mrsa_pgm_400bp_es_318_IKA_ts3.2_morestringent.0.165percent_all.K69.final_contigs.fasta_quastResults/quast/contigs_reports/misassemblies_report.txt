All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K69.final_contigs
#Mis_misassemblies               3                                                                                          
#Mis_relocations                 3                                                                                          
#Mis_translocations              0                                                                                          
#Mis_inversions                  0                                                                                          
#Mis_misassembled contigs        3                                                                                          
Mis_Misassembled contigs length  36029                                                                                      
#Mis_local misassemblies         10                                                                                         
#mismatches                      147                                                                                        
#indels                          200                                                                                        
#Mis_short indels (<= 5 bp)      196                                                                                        
#Mis_long indels (> 5 bp)        4                                                                                          
Indels length                    318                                                                                        
