All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K69.final_contigs
#Contigs                                   1377                                                                                       
#Contigs (>= 0 bp)                         1885                                                                                       
#Contigs (>= 1000 bp)                      83                                                                                         
Largest contig                             207816                                                                                     
Total length                               3107721                                                                                    
Total length (>= 0 bp)                     3158364                                                                                    
Total length (>= 1000 bp)                  2742331                                                                                    
Reference length                           2813862                                                                                    
N50                                        51608                                                                                      
NG50                                       63856                                                                                      
N75                                        25884                                                                                      
NG75                                       32291                                                                                      
L50                                        15                                                                                         
LG50                                       12                                                                                         
L75                                        37                                                                                         
LG75                                       30                                                                                         
#local misassemblies                       10                                                                                         
#misassemblies                             3                                                                                          
#misassembled contigs                      3                                                                                          
Misassembled contigs length                36029                                                                                      
Misassemblies inter-contig overlap         2608                                                                                       
#unaligned contigs                         1023 + 9 part                                                                              
Unaligned contigs length                   285124                                                                                     
#ambiguously mapped contigs                15                                                                                         
Extra bases in ambiguously mapped contigs  -10069                                                                                     
#N's                                       0                                                                                          
#N's per 100 kbp                           0.00                                                                                       
Genome fraction (%)                        97.975                                                                                     
Duplication ratio                          1.021                                                                                      
#genes                                     2600 + 62 part                                                                             
#predicted genes (unique)                  3403                                                                                       
#predicted genes (>= 0 bp)                 3410                                                                                       
#predicted genes (>= 300 bp)               2309                                                                                       
#predicted genes (>= 1500 bp)              286                                                                                        
#predicted genes (>= 3000 bp)              26                                                                                         
#mismatches                                147                                                                                        
#indels                                    200                                                                                        
Indels length                              318                                                                                        
#mismatches per 100 kbp                    5.33                                                                                       
#indels per 100 kbp                        7.25                                                                                       
GC (%)                                     32.45                                                                                      
Reference GC (%)                           32.81                                                                                      
Average %IDY                               98.372                                                                                     
Largest alignment                          207816                                                                                     
NA50                                       51608                                                                                      
NGA50                                      63856                                                                                      
NA75                                       25439                                                                                      
NGA75                                      32291                                                                                      
LA50                                       15                                                                                         
LGA50                                      12                                                                                         
LA75                                       37                                                                                         
LGA75                                      30                                                                                         
#Mis_misassemblies                         3                                                                                          
#Mis_relocations                           3                                                                                          
#Mis_translocations                        0                                                                                          
#Mis_inversions                            0                                                                                          
#Mis_misassembled contigs                  3                                                                                          
Mis_Misassembled contigs length            36029                                                                                      
#Mis_local misassemblies                   10                                                                                         
#Mis_short indels (<= 5 bp)                196                                                                                        
#Mis_long indels (> 5 bp)                  4                                                                                          
#Una_fully unaligned contigs               1023                                                                                       
Una_Fully unaligned length                 284566                                                                                     
#Una_partially unaligned contigs           9                                                                                          
#Una_with misassembly                      0                                                                                          
#Una_both parts are significant            0                                                                                          
Una_Partially unaligned length             558                                                                                        
GAGE_Contigs #                             1377                                                                                       
GAGE_Min contig                            203                                                                                        
GAGE_Max contig                            207816                                                                                     
GAGE_N50                                   63856 COUNT: 12                                                                            
GAGE_Genome size                           2813862                                                                                    
GAGE_Assembly size                         3107721                                                                                    
GAGE_Chaff bases                           0                                                                                          
GAGE_Missing reference bases               9482(0.34%)                                                                                
GAGE_Missing assembly bases                131671(4.24%)                                                                              
GAGE_Missing assembly contigs              415(30.14%)                                                                                
GAGE_Duplicated reference bases            206544                                                                                     
GAGE_Compressed reference bases            43458                                                                                      
GAGE_Bad trim                              10489                                                                                      
GAGE_Avg idy                               99.98                                                                                      
GAGE_SNPs                                  42                                                                                         
GAGE_Indels < 5bp                          218                                                                                        
GAGE_Indels >= 5                           11                                                                                         
GAGE_Inversions                            1                                                                                          
GAGE_Relocation                            2                                                                                          
GAGE_Translocation                         0                                                                                          
GAGE_Corrected contig #                    171                                                                                        
GAGE_Corrected assembly size               2772501                                                                                    
GAGE_Min correct contig                    203                                                                                        
GAGE_Max correct contig                    207833                                                                                     
GAGE_Corrected N50                         46562 COUNT: 16                                                                            
