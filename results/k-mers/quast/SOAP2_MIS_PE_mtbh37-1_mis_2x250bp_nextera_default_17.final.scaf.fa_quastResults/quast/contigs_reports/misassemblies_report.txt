All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_17.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_17.final.scaf
#Mis_misassemblies               1                                                                       1                                                              
#Mis_relocations                 1                                                                       1                                                              
#Mis_translocations              0                                                                       0                                                              
#Mis_inversions                  0                                                                       0                                                              
#Mis_misassembled contigs        1                                                                       1                                                              
Mis_Misassembled contigs length  229                                                                     229                                                            
#Mis_local misassemblies         0                                                                       3                                                              
#mismatches                      319                                                                     339                                                            
#indels                          10                                                                      10                                                             
#Mis_short indels (<= 5 bp)      10                                                                      10                                                             
#Mis_long indels (> 5 bp)        0                                                                       0                                                              
Indels length                    10                                                                      10                                                             
