All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_27.final.contigs
#Mis_misassemblies               0                                                                          
#Mis_relocations                 0                                                                          
#Mis_translocations              0                                                                          
#Mis_inversions                  0                                                                          
#Mis_misassembled contigs        0                                                                          
Mis_Misassembled contigs length  0                                                                          
#Mis_local misassemblies         1                                                                          
#mismatches                      110                                                                        
#indels                          993                                                                        
#Mis_short indels (<= 5 bp)      992                                                                        
#Mis_long indels (> 5 bp)        1                                                                          
Indels length                    1059                                                                       
