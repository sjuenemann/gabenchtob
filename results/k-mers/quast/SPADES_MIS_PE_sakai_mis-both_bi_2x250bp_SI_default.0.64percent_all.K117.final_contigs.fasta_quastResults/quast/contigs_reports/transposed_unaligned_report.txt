All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                                      #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_all.K117.final_contigs_broken  372                           132557                      12                                0                      0                                1179                            0   
SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_all.K117.final_contigs         372                           132557                      12                                0                      0                                1179                            0   
