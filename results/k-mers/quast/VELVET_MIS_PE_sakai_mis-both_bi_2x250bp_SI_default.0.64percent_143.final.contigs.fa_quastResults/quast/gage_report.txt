All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_143.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_143.final.contigs
GAGE_Contigs #                   518                                                                                      416                                                                             
GAGE_Min contig                  219                                                                                      285                                                                             
GAGE_Max contig                  144913                                                                                   206545                                                                          
GAGE_N50                         37658 COUNT: 44                                                                          65768 COUNT: 27                                                                 
GAGE_Genome size                 5594470                                                                                  5594470                                                                         
GAGE_Assembly size               5451279                                                                                  5464298                                                                         
GAGE_Chaff bases                 0                                                                                        0                                                                               
GAGE_Missing reference bases     12746(0.23%)                                                                             12588(0.23%)                                                                    
GAGE_Missing assembly bases      5499(0.10%)                                                                              18135(0.33%)                                                                    
GAGE_Missing assembly contigs    1(0.19%)                                                                                 1(0.24%)                                                                        
GAGE_Duplicated reference bases  10742                                                                                    10709                                                                           
GAGE_Compressed reference bases  242521                                                                                   244472                                                                          
GAGE_Bad trim                    114                                                                                      382                                                                             
GAGE_Avg idy                     99.99                                                                                    99.99                                                                           
GAGE_SNPs                        90                                                                                       90                                                                              
GAGE_Indels < 5bp                17                                                                                       27                                                                              
GAGE_Indels >= 5                 1                                                                                        76                                                                              
GAGE_Inversions                  2                                                                                        9                                                                               
GAGE_Relocation                  4                                                                                        12                                                                              
GAGE_Translocation               0                                                                                        0                                                                               
GAGE_Corrected contig #          499                                                                                      498                                                                             
GAGE_Corrected assembly size     5436462                                                                                  5437484                                                                         
GAGE_Min correct contig          219                                                                                      219                                                                             
GAGE_Max correct contig          144914                                                                                   144914                                                                          
GAGE_Corrected N50               37127 COUNT: 44                                                                          37127 COUNT: 44                                                                 
