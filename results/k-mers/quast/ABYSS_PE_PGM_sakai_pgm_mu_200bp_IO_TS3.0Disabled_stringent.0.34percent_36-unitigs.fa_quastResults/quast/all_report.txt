All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_36-unitigs
#Contigs                                   1193                                                                             
#Contigs (>= 0 bp)                         8001                                                                             
#Contigs (>= 1000 bp)                      755                                                                              
Largest contig                             63906                                                                            
Total length                               5189362                                                                          
Total length (>= 0 bp)                     5638382                                                                          
Total length (>= 1000 bp)                  4996087                                                                          
Reference length                           5594470                                                                          
N50                                        9738                                                                             
NG50                                       8965                                                                             
N75                                        5315                                                                             
NG75                                       4260                                                                             
L50                                        161                                                                              
LG50                                       183                                                                              
L75                                        339                                                                              
LG75                                       403                                                                              
#local misassemblies                       3                                                                                
#misassemblies                             2                                                                                
#misassembled contigs                      2                                                                                
Misassembled contigs length                8475                                                                             
Misassemblies inter-contig overlap         164                                                                              
#unaligned contigs                         0 + 1 part                                                                       
Unaligned contigs length                   26                                                                               
#ambiguously mapped contigs                126                                                                              
Extra bases in ambiguously mapped contigs  -59366                                                                           
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        91.631                                                                           
Duplication ratio                          1.001                                                                            
#genes                                     4274 + 633 part                                                                  
#predicted genes (unique)                  6060                                                                             
#predicted genes (>= 0 bp)                 6060                                                                             
#predicted genes (>= 300 bp)               4699                                                                             
#predicted genes (>= 1500 bp)              520                                                                              
#predicted genes (>= 3000 bp)              39                                                                               
#mismatches                                78                                                                               
#indels                                    1066                                                                             
Indels length                              1108                                                                             
#mismatches per 100 kbp                    1.52                                                                             
#indels per 100 kbp                        20.79                                                                            
GC (%)                                     50.20                                                                            
Reference GC (%)                           50.48                                                                            
Average %IDY                               99.880                                                                           
Largest alignment                          63906                                                                            
NA50                                       9738                                                                             
NGA50                                      8965                                                                             
NA75                                       5303                                                                             
NGA75                                      4256                                                                             
LA50                                       161                                                                              
LGA50                                      183                                                                              
LA75                                       340                                                                              
LGA75                                      403                                                                              
#Mis_misassemblies                         2                                                                                
#Mis_relocations                           2                                                                                
#Mis_translocations                        0                                                                                
#Mis_inversions                            0                                                                                
#Mis_misassembled contigs                  2                                                                                
Mis_Misassembled contigs length            8475                                                                             
#Mis_local misassemblies                   3                                                                                
#Mis_short indels (<= 5 bp)                1066                                                                             
#Mis_long indels (> 5 bp)                  0                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           1                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             26                                                                               
GAGE_Contigs #                             1193                                                                             
GAGE_Min contig                            200                                                                              
GAGE_Max contig                            63906                                                                            
GAGE_N50                                   8965 COUNT: 183                                                                  
GAGE_Genome size                           5594470                                                                          
GAGE_Assembly size                         5189362                                                                          
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               308356(5.51%)                                                                    
GAGE_Missing assembly bases                28(0.00%)                                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            0                                                                                
GAGE_Compressed reference bases            103300                                                                           
GAGE_Bad trim                              28                                                                               
GAGE_Avg idy                               99.98                                                                            
GAGE_SNPs                                  146                                                                              
GAGE_Indels < 5bp                          1113                                                                             
GAGE_Indels >= 5                           3                                                                                
GAGE_Inversions                            0                                                                                
GAGE_Relocation                            2                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    1198                                                                             
GAGE_Corrected assembly size               5190590                                                                          
GAGE_Min correct contig                    200                                                                              
GAGE_Max correct contig                    63922                                                                            
GAGE_Corrected N50                         8899 COUNT: 183                                                                  
