All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_199.final.contigs
#Contigs                                   1794                                                                        
#Contigs (>= 0 bp)                         1794                                                                        
#Contigs (>= 1000 bp)                      1346                                                                        
Largest contig                             18497                                                                       
Total length                               4685366                                                                     
Total length (>= 0 bp)                     4685366                                                                     
Total length (>= 1000 bp)                  4371837                                                                     
Reference length                           5594470                                                                     
N50                                        3991                                                                        
NG50                                       3144                                                                        
N75                                        2182                                                                        
NG75                                       1224                                                                        
L50                                        372                                                                         
LG50                                       499                                                                         
L75                                        774                                                                         
LG75                                       1188                                                                        
#local misassemblies                       39                                                                          
#misassemblies                             74                                                                          
#misassembled contigs                      72                                                                          
Misassembled contigs length                385224                                                                      
Misassemblies inter-contig overlap         2770                                                                        
#unaligned contigs                         0 + 2 part                                                                  
Unaligned contigs length                   508                                                                         
#ambiguously mapped contigs                97                                                                          
Extra bases in ambiguously mapped contigs  -61649                                                                      
#N's                                       1000                                                                        
#N's per 100 kbp                           21.34                                                                       
Genome fraction (%)                        80.303                                                                      
Duplication ratio                          1.030                                                                       
#genes                                     3164 + 1463 part                                                            
#predicted genes (unique)                  5894                                                                        
#predicted genes (>= 0 bp)                 5919                                                                        
#predicted genes (>= 300 bp)               4444                                                                        
#predicted genes (>= 1500 bp)              411                                                                         
#predicted genes (>= 3000 bp)              28                                                                          
#mismatches                                96                                                                          
#indels                                    983                                                                         
Indels length                              1022                                                                        
#mismatches per 100 kbp                    2.14                                                                        
#indels per 100 kbp                        21.88                                                                       
GC (%)                                     51.21                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.231                                                                      
Largest alignment                          15140                                                                       
NA50                                       3934                                                                        
NGA50                                      3049                                                                        
NA75                                       2102                                                                        
NGA75                                      1177                                                                        
LA50                                       377                                                                         
LGA50                                      507                                                                         
LA75                                       789                                                                         
LGA75                                      1218                                                                        
#Mis_misassemblies                         74                                                                          
#Mis_relocations                           73                                                                          
#Mis_translocations                        1                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  72                                                                          
Mis_Misassembled contigs length            385224                                                                      
#Mis_local misassemblies                   39                                                                          
#Mis_short indels (<= 5 bp)                982                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           2                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            1                                                                           
Una_Partially unaligned length             508                                                                         
GAGE_Contigs #                             1794                                                                        
GAGE_Min contig                            397                                                                         
GAGE_Max contig                            18497                                                                       
GAGE_N50                                   3144 COUNT: 499                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         4685366                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               890385(15.92%)                                                              
GAGE_Missing assembly bases                1458(0.03%)                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            41568                                                                       
GAGE_Compressed reference bases            231233                                                                      
GAGE_Bad trim                              1068                                                                        
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  71                                                                          
GAGE_Indels < 5bp                          871                                                                         
GAGE_Indels >= 5                           31                                                                          
GAGE_Inversions                            1                                                                           
GAGE_Relocation                            13                                                                          
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    1803                                                                        
GAGE_Corrected assembly size               4645357                                                                     
GAGE_Min correct contig                    397                                                                         
GAGE_Max correct contig                    15141                                                                       
GAGE_Corrected N50                         2966 COUNT: 524                                                             
