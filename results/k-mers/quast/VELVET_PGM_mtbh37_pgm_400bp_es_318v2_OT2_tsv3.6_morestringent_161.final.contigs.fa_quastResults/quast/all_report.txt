All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_161.final.contigs
#Contigs                                   2749                                                                           
#Contigs (>= 0 bp)                         2749                                                                           
#Contigs (>= 1000 bp)                      925                                                                            
Largest contig                             5697                                                                           
Total length                               2590423                                                                        
Total length (>= 0 bp)                     2590423                                                                        
Total length (>= 1000 bp)                  1512161                                                                        
Reference length                           4411532                                                                        
N50                                        1154                                                                           
NG50                                       572                                                                            
N75                                        730                                                                            
NG75                                       None                                                                           
L50                                        724                                                                            
LG50                                       1835                                                                           
L75                                        1431                                                                           
LG75                                       None                                                                           
#local misassemblies                       8                                                                              
#misassemblies                             28                                                                             
#misassembled contigs                      28                                                                             
Misassembled contigs length                32366                                                                          
Misassemblies inter-contig overlap         968                                                                            
#unaligned contigs                         1 + 0 part                                                                     
Unaligned contigs length                   486                                                                            
#ambiguously mapped contigs                15                                                                             
Extra bases in ambiguously mapped contigs  -7296                                                                          
#N's                                       100                                                                            
#N's per 100 kbp                           3.86                                                                           
Genome fraction (%)                        55.469                                                                         
Duplication ratio                          1.056                                                                          
#genes                                     820 + 1960 part                                                                
#predicted genes (unique)                  4647                                                                           
#predicted genes (>= 0 bp)                 4662                                                                           
#predicted genes (>= 300 bp)               2988                                                                           
#predicted genes (>= 1500 bp)              67                                                                             
#predicted genes (>= 3000 bp)              0                                                                              
#mismatches                                73                                                                             
#indels                                    1311                                                                           
Indels length                              1398                                                                           
#mismatches per 100 kbp                    2.98                                                                           
#indels per 100 kbp                        53.58                                                                          
GC (%)                                     65.48                                                                          
Reference GC (%)                           65.61                                                                          
Average %IDY                               99.891                                                                         
Largest alignment                          5697                                                                           
NA50                                       1150                                                                           
NGA50                                      564                                                                            
NA75                                       725                                                                            
NGA75                                      None                                                                           
LA50                                       726                                                                            
LGA50                                      1846                                                                           
LA75                                       1438                                                                           
LGA75                                      None                                                                           
#Mis_misassemblies                         28                                                                             
#Mis_relocations                           28                                                                             
#Mis_translocations                        0                                                                              
#Mis_inversions                            0                                                                              
#Mis_misassembled contigs                  28                                                                             
Mis_Misassembled contigs length            32366                                                                          
#Mis_local misassemblies                   8                                                                              
#Mis_short indels (<= 5 bp)                1309                                                                           
#Mis_long indels (> 5 bp)                  2                                                                              
#Una_fully unaligned contigs               1                                                                              
Una_Fully unaligned length                 486                                                                            
#Una_partially unaligned contigs           0                                                                              
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             0                                                                              
GAGE_Contigs #                             2749                                                                           
GAGE_Min contig                            321                                                                            
GAGE_Max contig                            5697                                                                           
GAGE_N50                                   572 COUNT: 1835                                                                
GAGE_Genome size                           4411532                                                                        
GAGE_Assembly size                         2590423                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               1937815(43.93%)                                                                
GAGE_Missing assembly bases                158(0.01%)                                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                                       
GAGE_Duplicated reference bases            58308                                                                          
GAGE_Compressed reference bases            28103                                                                          
GAGE_Bad trim                              98                                                                             
GAGE_Avg idy                               99.94                                                                          
GAGE_SNPs                                  53                                                                             
GAGE_Indels < 5bp                          1264                                                                           
GAGE_Indels >= 5                           6                                                                              
GAGE_Inversions                            1                                                                              
GAGE_Relocation                            9                                                                              
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    2627                                                                           
GAGE_Corrected assembly size               2532924                                                                        
GAGE_Min correct contig                    239                                                                            
GAGE_Max correct contig                    5697                                                                           
GAGE_Corrected N50                         563 COUNT: 1847                                                                
