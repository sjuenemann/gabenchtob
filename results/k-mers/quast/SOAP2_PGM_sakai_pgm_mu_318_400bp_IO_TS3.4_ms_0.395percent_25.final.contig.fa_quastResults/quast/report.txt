All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_25.final.contig
#Contigs (>= 0 bp)             572880                                                                   
#Contigs (>= 1000 bp)          0                                                                        
Total length (>= 0 bp)         24152237                                                                 
Total length (>= 1000 bp)      0                                                                        
#Contigs                       401                                                                      
Largest contig                 431                                                                      
Total length                   96364                                                                    
Reference length               5594470                                                                  
GC (%)                         54.02                                                                    
Reference GC (%)               50.48                                                                    
N50                            232                                                                      
NG50                           None                                                                     
N75                            213                                                                      
NG75                           None                                                                     
#misassemblies                 0                                                                        
#local misassemblies           0                                                                        
#unaligned contigs             24 + 0 part                                                              
Unaligned contigs length       7306                                                                     
Genome fraction (%)            1.591                                                                    
Duplication ratio              1.000                                                                    
#N's per 100 kbp               0.00                                                                     
#mismatches per 100 kbp        0.00                                                                     
#indels per 100 kbp            42.68                                                                    
#genes                         4 + 343 part                                                             
#predicted genes (unique)      403                                                                      
#predicted genes (>= 0 bp)     403                                                                      
#predicted genes (>= 300 bp)   24                                                                       
#predicted genes (>= 1500 bp)  0                                                                        
#predicted genes (>= 3000 bp)  0                                                                        
Largest alignment              431                                                                      
NA50                           226                                                                      
NGA50                          None                                                                     
NA75                           209                                                                      
NGA75                          None                                                                     
