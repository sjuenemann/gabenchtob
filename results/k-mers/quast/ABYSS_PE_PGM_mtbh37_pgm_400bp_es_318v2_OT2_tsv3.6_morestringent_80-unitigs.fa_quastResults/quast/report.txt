All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_80-unitigs
#Contigs (>= 0 bp)             10515                                                                     
#Contigs (>= 1000 bp)          1364                                                                      
Total length (>= 0 bp)         5270791                                                                   
Total length (>= 1000 bp)      3557138                                                                   
#Contigs                       2463                                                                      
Largest contig                 12431                                                                     
Total length                   4138132                                                                   
Reference length               4411532                                                                   
GC (%)                         65.35                                                                     
Reference GC (%)               65.61                                                                     
N50                            2670                                                                      
NG50                           2501                                                                      
N75                            1496                                                                      
NG75                           1245                                                                      
#misassemblies                 1                                                                         
#local misassemblies           15                                                                        
#unaligned contigs             1 + 1 part                                                                
Unaligned contigs length       701                                                                       
Genome fraction (%)            93.325                                                                    
Duplication ratio              1.002                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        3.42                                                                      
#indels per 100 kbp            19.33                                                                     
#genes                         2324 + 1699 part                                                          
#predicted genes (unique)      5852                                                                      
#predicted genes (>= 0 bp)     5855                                                                      
#predicted genes (>= 300 bp)   4214                                                                      
#predicted genes (>= 1500 bp)  281                                                                       
#predicted genes (>= 3000 bp)  12                                                                        
Largest alignment              12431                                                                     
NA50                           2670                                                                      
NGA50                          2501                                                                      
NA75                           1496                                                                      
NGA75                          1245                                                                      
