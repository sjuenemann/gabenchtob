All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_76-unitigs
#Contigs (>= 0 bp)             9808                                                                      
#Contigs (>= 1000 bp)          1351                                                                      
Total length (>= 0 bp)         5143967                                                                   
Total length (>= 1000 bp)      3600577                                                                   
#Contigs                       2372                                                                      
Largest contig                 12428                                                                     
Total length                   4146690                                                                   
Reference length               4411532                                                                   
GC (%)                         65.33                                                                     
Reference GC (%)               65.61                                                                     
N50                            2772                                                                      
NG50                           2606                                                                      
N75                            1544                                                                      
NG75                           1341                                                                      
#misassemblies                 1                                                                         
#local misassemblies           14                                                                        
#unaligned contigs             1 + 1 part                                                                
Unaligned contigs length       701                                                                       
Genome fraction (%)            93.510                                                                    
Duplication ratio              1.002                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        3.66                                                                      
#indels per 100 kbp            18.93                                                                     
#genes                         2390 + 1629 part                                                          
#predicted genes (unique)      5782                                                                      
#predicted genes (>= 0 bp)     5785                                                                      
#predicted genes (>= 300 bp)   4197                                                                      
#predicted genes (>= 1500 bp)  286                                                                       
#predicted genes (>= 3000 bp)  14                                                                        
Largest alignment              12428                                                                     
NA50                           2772                                                                      
NGA50                          2606                                                                      
NA75                           1540                                                                      
NGA75                          1341                                                                      
