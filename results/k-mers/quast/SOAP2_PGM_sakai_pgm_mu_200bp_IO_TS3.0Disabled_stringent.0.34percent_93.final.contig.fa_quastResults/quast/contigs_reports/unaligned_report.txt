All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_93.final.contig
#Una_fully unaligned contigs      1                                                                                  
Una_Fully unaligned length        214                                                                                
#Una_partially unaligned contigs  4                                                                                  
#Una_with misassembly             0                                                                                  
#Una_both parts are significant   0                                                                                  
Una_Partially unaligned length    159                                                                                
#N's                              0                                                                                  
