All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_77.final.contig
#Mis_misassemblies               93                                                                                       
#Mis_relocations                 26                                                                                       
#Mis_translocations              3                                                                                        
#Mis_inversions                  64                                                                                       
#Mis_misassembled contigs        93                                                                                       
Mis_Misassembled contigs length  25277                                                                                    
#Mis_local misassemblies         4                                                                                        
#mismatches                      493                                                                                      
#indels                          3868                                                                                     
#Mis_short indels (<= 5 bp)      3868                                                                                     
#Mis_long indels (> 5 bp)        0                                                                                        
Indels length                    3994                                                                                     
