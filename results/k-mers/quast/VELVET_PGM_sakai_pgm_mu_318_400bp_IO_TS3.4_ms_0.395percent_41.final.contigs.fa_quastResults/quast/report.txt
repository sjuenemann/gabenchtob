All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_41.final.contigs
#Contigs (>= 0 bp)             27256                                                                      
#Contigs (>= 1000 bp)          1                                                                          
Total length (>= 0 bp)         3976559                                                                    
Total length (>= 1000 bp)      1261                                                                       
#Contigs                       4446                                                                       
Largest contig                 1261                                                                       
Total length                   1227743                                                                    
Reference length               5594470                                                                    
GC (%)                         52.50                                                                      
Reference GC (%)               50.48                                                                      
N50                            268                                                                        
NG50                           None                                                                       
N75                            227                                                                        
NG75                           None                                                                       
#misassemblies                 0                                                                          
#local misassemblies           0                                                                          
#unaligned contigs             0 + 2 part                                                                 
Unaligned contigs length       49                                                                         
Genome fraction (%)            21.811                                                                     
Duplication ratio              1.003                                                                      
#N's per 100 kbp               0.00                                                                       
#mismatches per 100 kbp        2.29                                                                       
#indels per 100 kbp            20.32                                                                      
#genes                         28 + 2654 part                                                             
#predicted genes (unique)      4478                                                                       
#predicted genes (>= 0 bp)     4478                                                                       
#predicted genes (>= 300 bp)   917                                                                        
#predicted genes (>= 1500 bp)  0                                                                          
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              1261                                                                       
NA50                           268                                                                        
NGA50                          None                                                                       
NA75                           227                                                                        
NGA75                          None                                                                       
