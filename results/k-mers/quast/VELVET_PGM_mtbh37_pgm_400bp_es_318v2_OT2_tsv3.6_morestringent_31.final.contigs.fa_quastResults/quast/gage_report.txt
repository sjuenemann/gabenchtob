All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_31.final.contigs
GAGE_Contigs #                   3296                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  10530                                                                         
GAGE_N50                         1853 COUNT: 695                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4174893                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     249858(5.66%)                                                                 
GAGE_Missing assembly bases      1217(0.03%)                                                                   
GAGE_Missing assembly contigs    3(0.09%)                                                                      
GAGE_Duplicated reference bases  0                                                                             
GAGE_Compressed reference bases  18366                                                                         
GAGE_Bad trim                    168                                                                           
GAGE_Avg idy                     99.93                                                                         
GAGE_SNPs                        194                                                                           
GAGE_Indels < 5bp                2481                                                                          
GAGE_Indels >= 5                 5                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  1                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          3299                                                                          
GAGE_Corrected assembly size     4175179                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          10536                                                                         
GAGE_Corrected N50               1849 COUNT: 696                                                               
