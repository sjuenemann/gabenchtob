All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_31.final.contigs
#Contigs (>= 0 bp)             4316                                                                          
#Contigs (>= 1000 bp)          1424                                                                          
Total length (>= 0 bp)         4295366                                                                       
Total length (>= 1000 bp)      3210362                                                                       
#Contigs                       3296                                                                          
Largest contig                 10530                                                                         
Total length                   4174893                                                                       
Reference length               4411532                                                                       
GC (%)                         65.36                                                                         
Reference GC (%)               65.61                                                                         
N50                            1960                                                                          
NG50                           1853                                                                          
N75                            1065                                                                          
NG75                           914                                                                           
#misassemblies                 0                                                                             
#local misassemblies           3                                                                             
#unaligned contigs             3 + 2 part                                                                    
Unaligned contigs length       1105                                                                          
Genome fraction (%)            93.635                                                                        
Duplication ratio              1.007                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        5.06                                                                          
#indels per 100 kbp            56.72                                                                         
#genes                         2090 + 1907 part                                                              
#predicted genes (unique)      6522                                                                          
#predicted genes (>= 0 bp)     6522                                                                          
#predicted genes (>= 300 bp)   4412                                                                          
#predicted genes (>= 1500 bp)  169                                                                           
#predicted genes (>= 3000 bp)  6                                                                             
Largest alignment              10530                                                                         
NA50                           1960                                                                          
NGA50                          1853                                                                          
NA75                           1065                                                                          
NGA75                          911                                                                           
