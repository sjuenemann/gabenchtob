All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_98-unitigs
GAGE_Contigs #                   1233                                                                   
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  51165                                                                  
GAGE_N50                         11417 COUNT: 142                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5333000                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     114809(2.05%)                                                          
GAGE_Missing assembly bases      4(0.00%)                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  12243                                                                  
GAGE_Compressed reference bases  218928                                                                 
GAGE_Bad trim                    4                                                                      
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        52                                                                     
GAGE_Indels < 5bp                274                                                                    
GAGE_Indels >= 5                 4                                                                      
GAGE_Inversions                  1                                                                      
GAGE_Relocation                  6                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          1193                                                                   
GAGE_Corrected assembly size     5321602                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          51167                                                                  
GAGE_Corrected N50               11331 COUNT: 143                                                       
