All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_221.final.contigs
#Mis_misassemblies               27                                                                          
#Mis_relocations                 27                                                                          
#Mis_translocations              0                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        26                                                                          
Mis_Misassembled contigs length  129805                                                                      
#Mis_local misassemblies         4                                                                           
#mismatches                      205                                                                         
#indels                          1184                                                                        
#Mis_short indels (<= 5 bp)      1184                                                                        
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    1194                                                                        
