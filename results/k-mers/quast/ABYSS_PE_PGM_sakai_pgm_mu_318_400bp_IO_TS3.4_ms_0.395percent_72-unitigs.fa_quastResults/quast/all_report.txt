All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_72-unitigs
#Contigs                                   1197                                                                   
#Contigs (>= 0 bp)                         6052                                                                   
#Contigs (>= 1000 bp)                      677                                                                    
Largest contig                             46589                                                                  
Total length                               5281768                                                                
Total length (>= 0 bp)                     5858302                                                                
Total length (>= 1000 bp)                  5061038                                                                
Reference length                           5594470                                                                
N50                                        12371                                                                  
NG50                                       11198                                                                  
N75                                        5912                                                                   
NG75                                       4964                                                                   
L50                                        134                                                                    
LG50                                       148                                                                    
L75                                        287                                                                    
LG75                                       330                                                                    
#local misassemblies                       9                                                                      
#misassemblies                             2                                                                      
#misassembled contigs                      2                                                                      
Misassembled contigs length                12119                                                                  
Misassemblies inter-contig overlap         1094                                                                   
#unaligned contigs                         0 + 0 part                                                             
Unaligned contigs length                   0                                                                      
#ambiguously mapped contigs                145                                                                    
Extra bases in ambiguously mapped contigs  -66183                                                                 
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        93.050                                                                 
Duplication ratio                          1.002                                                                  
#genes                                     4362 + 689 part                                                        
#predicted genes (unique)                  5862                                                                   
#predicted genes (>= 0 bp)                 5865                                                                   
#predicted genes (>= 300 bp)               4596                                                                   
#predicted genes (>= 1500 bp)              582                                                                    
#predicted genes (>= 3000 bp)              55                                                                     
#mismatches                                79                                                                     
#indels                                    237                                                                    
Indels length                              241                                                                    
#mismatches per 100 kbp                    1.52                                                                   
#indels per 100 kbp                        4.55                                                                   
GC (%)                                     50.24                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.336                                                                 
Largest alignment                          46589                                                                  
NA50                                       12371                                                                  
NGA50                                      11197                                                                  
NA75                                       5888                                                                   
NGA75                                      4937                                                                   
LA50                                       134                                                                    
LGA50                                      148                                                                    
LA75                                       288                                                                    
LGA75                                      331                                                                    
#Mis_misassemblies                         2                                                                      
#Mis_relocations                           2                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  2                                                                      
Mis_Misassembled contigs length            12119                                                                  
#Mis_local misassemblies                   9                                                                      
#Mis_short indels (<= 5 bp)                237                                                                    
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           0                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             0                                                                      
GAGE_Contigs #                             1197                                                                   
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            46589                                                                  
GAGE_N50                                   11198 COUNT: 148                                                       
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5281768                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               184515(3.30%)                                                          
GAGE_Missing assembly bases                1(0.00%)                                                               
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            5197                                                                   
GAGE_Compressed reference bases            170344                                                                 
GAGE_Bad trim                              1                                                                      
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  82                                                                     
GAGE_Indels < 5bp                          244                                                                    
GAGE_Indels >= 5                           6                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            5                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    1185                                                                   
GAGE_Corrected assembly size               5277280                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    46589                                                                  
GAGE_Corrected N50                         11197 COUNT: 148                                                       
