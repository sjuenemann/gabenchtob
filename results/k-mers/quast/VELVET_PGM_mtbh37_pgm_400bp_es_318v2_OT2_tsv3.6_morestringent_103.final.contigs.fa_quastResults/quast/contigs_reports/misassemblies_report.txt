All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_103.final.contigs
#Mis_misassemblies               339                                                                            
#Mis_relocations                 339                                                                            
#Mis_translocations              0                                                                              
#Mis_inversions                  0                                                                              
#Mis_misassembled contigs        339                                                                            
Mis_Misassembled contigs length  86525                                                                          
#Mis_local misassemblies         9                                                                              
#mismatches                      4351                                                                           
#indels                          22206                                                                          
#Mis_short indels (<= 5 bp)      22201                                                                          
#Mis_long indels (> 5 bp)        5                                                                              
Indels length                    22719                                                                          
