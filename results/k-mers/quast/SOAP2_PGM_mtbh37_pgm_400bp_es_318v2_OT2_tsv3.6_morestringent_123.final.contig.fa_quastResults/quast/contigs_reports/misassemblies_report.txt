All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_123.final.contig
#Mis_misassemblies               20                                                                           
#Mis_relocations                 19                                                                           
#Mis_translocations              0                                                                            
#Mis_inversions                  1                                                                            
#Mis_misassembled contigs        20                                                                           
Mis_Misassembled contigs length  6408                                                                         
#Mis_local misassemblies         6                                                                            
#mismatches                      180                                                                          
#indels                          6508                                                                         
#Mis_short indels (<= 5 bp)      6508                                                                         
#Mis_long indels (> 5 bp)        0                                                                            
Indels length                    6656                                                                         
