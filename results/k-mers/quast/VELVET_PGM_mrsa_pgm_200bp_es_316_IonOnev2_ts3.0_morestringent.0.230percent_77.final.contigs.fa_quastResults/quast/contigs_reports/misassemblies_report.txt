All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_77.final.contigs
#Mis_misassemblies               42                                                                                         
#Mis_relocations                 27                                                                                         
#Mis_translocations              0                                                                                          
#Mis_inversions                  15                                                                                         
#Mis_misassembled contigs        40                                                                                         
Mis_Misassembled contigs length  136367                                                                                     
#Mis_local misassemblies         21                                                                                         
#mismatches                      220                                                                                        
#indels                          932                                                                                        
#Mis_short indels (<= 5 bp)      924                                                                                        
#Mis_long indels (> 5 bp)        8                                                                                          
Indels length                    1405                                                                                       
