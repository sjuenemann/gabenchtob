All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_119.final.contig
#Mis_misassemblies               140                                                                       
#Mis_relocations                 132                                                                       
#Mis_translocations              8                                                                         
#Mis_inversions                  0                                                                         
#Mis_misassembled contigs        139                                                                       
Mis_Misassembled contigs length  48943                                                                     
#Mis_local misassemblies         2                                                                         
#mismatches                      315                                                                       
#indels                          10691                                                                     
#Mis_short indels (<= 5 bp)      10691                                                                     
#Mis_long indels (> 5 bp)        0                                                                         
Indels length                    10849                                                                     
