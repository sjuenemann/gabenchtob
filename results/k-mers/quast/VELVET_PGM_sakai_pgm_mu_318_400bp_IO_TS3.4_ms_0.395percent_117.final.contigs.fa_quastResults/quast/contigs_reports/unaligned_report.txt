All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_117.final.contigs
#Una_fully unaligned contigs      29                                                                          
Una_Fully unaligned length        7594                                                                        
#Una_partially unaligned contigs  34                                                                          
#Una_with misassembly             0                                                                           
#Una_both parts are significant   0                                                                           
Una_Partially unaligned length    3722                                                                        
#N's                              0                                                                           
