All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_117.final.contigs
GAGE_Contigs #                   26625                                                                       
GAGE_Min contig                  233                                                                         
GAGE_Max contig                  810                                                                         
GAGE_N50                         269 COUNT: 9065                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               7058995                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     1530864(27.36%)                                                             
GAGE_Missing assembly bases      2519(0.04%)                                                                 
GAGE_Missing assembly contigs    2(0.01%)                                                                    
GAGE_Duplicated reference bases  2264366                                                                     
GAGE_Compressed reference bases  309704                                                                      
GAGE_Bad trim                    1917                                                                        
GAGE_Avg idy                     99.12                                                                       
GAGE_SNPs                        2775                                                                        
GAGE_Indels < 5bp                27025                                                                       
GAGE_Indels >= 5                 3                                                                           
GAGE_Inversions                  132                                                                         
GAGE_Relocation                  49                                                                          
GAGE_Translocation               9                                                                           
GAGE_Corrected contig #          17577                                                                       
GAGE_Corrected assembly size     4664196                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          810                                                                         
GAGE_Corrected N50               246 COUNT: 9636                                                             
