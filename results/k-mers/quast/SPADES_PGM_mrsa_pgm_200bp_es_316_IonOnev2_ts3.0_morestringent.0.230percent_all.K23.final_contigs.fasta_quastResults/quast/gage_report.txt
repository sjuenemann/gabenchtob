All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_all.K23.final_contigs
GAGE_Contigs #                   377                                                                                             
GAGE_Min contig                  200                                                                                             
GAGE_Max contig                  74584                                                                                           
GAGE_N50                         19530 COUNT: 46                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2719987                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     62656(2.23%)                                                                                    
GAGE_Missing assembly bases      845(0.03%)                                                                                      
GAGE_Missing assembly contigs    3(0.80%)                                                                                        
GAGE_Duplicated reference bases  0                                                                                               
GAGE_Compressed reference bases  35353                                                                                           
GAGE_Bad trim                    124                                                                                             
GAGE_Avg idy                     99.98                                                                                           
GAGE_SNPs                        89                                                                                              
GAGE_Indels < 5bp                328                                                                                             
GAGE_Indels >= 5                 2                                                                                               
GAGE_Inversions                  0                                                                                               
GAGE_Relocation                  1                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          377                                                                                             
GAGE_Corrected assembly size     2719723                                                                                         
GAGE_Min correct contig          200                                                                                             
GAGE_Max correct contig          74595                                                                                           
GAGE_Corrected N50               19532 COUNT: 46                                                                                 
