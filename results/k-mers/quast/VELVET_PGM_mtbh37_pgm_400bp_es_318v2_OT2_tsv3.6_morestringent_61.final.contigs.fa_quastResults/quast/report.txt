All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_61.final.contigs
#Contigs (>= 0 bp)             13479                                                                         
#Contigs (>= 1000 bp)          73                                                                            
Total length (>= 0 bp)         3368578                                                                       
Total length (>= 1000 bp)      90984                                                                         
#Contigs                       6460                                                                          
Largest contig                 3328                                                                          
Total length                   2297350                                                                       
Reference length               4411532                                                                       
GC (%)                         66.90                                                                         
Reference GC (%)               65.61                                                                         
N50                            366                                                                           
NG50                           209                                                                           
N75                            266                                                                           
NG75                           None                                                                          
#misassemblies                 0                                                                             
#local misassemblies           5                                                                             
#unaligned contigs             19 + 11 part                                                                  
Unaligned contigs length       5103                                                                          
Genome fraction (%)            51.580                                                                        
Duplication ratio              1.006                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        5.10                                                                          
#indels per 100 kbp            59.72                                                                         
#genes                         122 + 3218 part                                                               
#predicted genes (unique)      6883                                                                          
#predicted genes (>= 0 bp)     6883                                                                          
#predicted genes (>= 300 bp)   2320                                                                          
#predicted genes (>= 1500 bp)  1                                                                             
#predicted genes (>= 3000 bp)  0                                                                             
Largest alignment              3328                                                                          
NA50                           365                                                                           
NGA50                          208                                                                           
NA75                           266                                                                           
NGA75                          None                                                                          
