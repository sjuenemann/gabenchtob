All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_127.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_127.final.scaf
#Mis_misassemblies               7                                                                                 18                                                                       
#Mis_relocations                 7                                                                                 17                                                                       
#Mis_translocations              0                                                                                 1                                                                        
#Mis_inversions                  0                                                                                 0                                                                        
#Mis_misassembled contigs        7                                                                                 16                                                                       
Mis_Misassembled contigs length  55633                                                                             118878                                                                   
#Mis_local misassemblies         18                                                                                272                                                                      
#mismatches                      255                                                                               328                                                                      
#indels                          47                                                                                683                                                                      
#Mis_short indels (<= 5 bp)      33                                                                                610                                                                      
#Mis_long indels (> 5 bp)        14                                                                                73                                                                       
Indels length                    489                                                                               2581                                                                     
