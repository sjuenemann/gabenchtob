All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_105.final.scaf_broken  SOAP2_MIS_PE_mrsa_mis-1_bi_2x150bp_default.0.527percent_105.final.scaf
#Contigs                                   277                                                                            167                                                                   
#Contigs (>= 0 bp)                         460                                                                            341                                                                   
#Contigs (>= 1000 bp)                      150                                                                            66                                                                    
Largest contig                             118741                                                                         222037                                                                
Total length                               2799763                                                                        2804663                                                               
Total length (>= 0 bp)                     2824549                                                                        2828145                                                               
Total length (>= 1000 bp)                  2759020                                                                        2774788                                                               
Reference length                           2813862                                                                        2813862                                                               
N50                                        32711                                                                          88029                                                                 
NG50                                       32711                                                                          88029                                                                 
N75                                        18674                                                                          44032                                                                 
NG75                                       18674                                                                          44032                                                                 
L50                                        27                                                                             12                                                                    
LG50                                       27                                                                             12                                                                    
L75                                        56                                                                             22                                                                    
LG75                                       56                                                                             22                                                                    
#local misassemblies                       2                                                                              18                                                                    
#misassemblies                             5                                                                              9                                                                     
#misassembled contigs                      5                                                                              6                                                                     
Misassembled contigs length                28830                                                                          74222                                                                 
Misassemblies inter-contig overlap         1                                                                              106                                                                   
#unaligned contigs                         3 + 0 part                                                                     3 + 0 part                                                            
Unaligned contigs length                   5807                                                                           5807                                                                  
#ambiguously mapped contigs                49                                                                             47                                                                    
Extra bases in ambiguously mapped contigs  -14652                                                                         -14215                                                                
#N's                                       145                                                                            3741                                                                  
#N's per 100 kbp                           5.18                                                                           133.39                                                                
Genome fraction (%)                        98.443                                                                         98.555                                                                
Duplication ratio                          1.003                                                                          1.004                                                                 
#genes                                     2584 + 117 part                                                                2648 + 54 part                                                        
#predicted genes (unique)                  2773                                                                           2734                                                                  
#predicted genes (>= 0 bp)                 2777                                                                           2737                                                                  
#predicted genes (>= 300 bp)               2315                                                                           2310                                                                  
#predicted genes (>= 1500 bp)              292                                                                            292                                                                   
#predicted genes (>= 3000 bp)              28                                                                             28                                                                    
#mismatches                                42                                                                             45                                                                    
#indels                                    49                                                                             942                                                                   
Indels length                              485                                                                            2313                                                                  
#mismatches per 100 kbp                    1.52                                                                           1.62                                                                  
#indels per 100 kbp                        1.77                                                                           33.97                                                                 
GC (%)                                     32.72                                                                          32.72                                                                 
Reference GC (%)                           32.81                                                                          32.81                                                                 
Average %IDY                               99.119                                                                         99.043                                                                
Largest alignment                          118741                                                                         222037                                                                
NA50                                       32711                                                                          88029                                                                 
NGA50                                      32711                                                                          88029                                                                 
NA75                                       18137                                                                          43053                                                                 
NGA75                                      18075                                                                          43053                                                                 
LA50                                       27                                                                             12                                                                    
LGA50                                      27                                                                             12                                                                    
LA75                                       56                                                                             22                                                                    
LGA75                                      57                                                                             22                                                                    
#Mis_misassemblies                         5                                                                              9                                                                     
#Mis_relocations                           5                                                                              9                                                                     
#Mis_translocations                        0                                                                              0                                                                     
#Mis_inversions                            0                                                                              0                                                                     
#Mis_misassembled contigs                  5                                                                              6                                                                     
Mis_Misassembled contigs length            28830                                                                          74222                                                                 
#Mis_local misassemblies                   2                                                                              18                                                                    
#Mis_short indels (<= 5 bp)                28                                                                             874                                                                   
#Mis_long indels (> 5 bp)                  21                                                                             68                                                                    
#Una_fully unaligned contigs               3                                                                              3                                                                     
Una_Fully unaligned length                 5807                                                                           5807                                                                  
#Una_partially unaligned contigs           0                                                                              0                                                                     
#Una_with misassembly                      0                                                                              0                                                                     
#Una_both parts are significant            0                                                                              0                                                                     
Una_Partially unaligned length             0                                                                              0                                                                     
GAGE_Contigs #                             277                                                                            167                                                                   
GAGE_Min contig                            202                                                                            202                                                                   
GAGE_Max contig                            118741                                                                         222037                                                                
GAGE_N50                                   32711 COUNT: 27                                                                88029 COUNT: 12                                                       
GAGE_Genome size                           2813862                                                                        2813862                                                               
GAGE_Assembly size                         2799763                                                                        2804663                                                               
GAGE_Chaff bases                           0                                                                              0                                                                     
GAGE_Missing reference bases               4210(0.15%)                                                                    3658(0.13%)                                                           
GAGE_Missing assembly bases                5918(0.21%)                                                                    9160(0.33%)                                                           
GAGE_Missing assembly contigs              3(1.08%)                                                                       3(1.80%)                                                              
GAGE_Duplicated reference bases            5383                                                                           5805                                                                  
GAGE_Compressed reference bases            46657                                                                          45490                                                                 
GAGE_Bad trim                              15                                                                             164                                                                   
GAGE_Avg idy                               100.00                                                                         99.98                                                                 
GAGE_SNPs                                  35                                                                             35                                                                    
GAGE_Indels < 5bp                          23                                                                             49                                                                    
GAGE_Indels >= 5                           20                                                                             95                                                                    
GAGE_Inversions                            0                                                                              0                                                                     
GAGE_Relocation                            2                                                                              6                                                                     
GAGE_Translocation                         0                                                                              0                                                                     
GAGE_Corrected contig #                    274                                                                            257                                                                   
GAGE_Corrected assembly size               2788267                                                                        2788315                                                               
GAGE_Min correct contig                    202                                                                            202                                                                   
GAGE_Max correct contig                    118741                                                                         118741                                                                
GAGE_Corrected N50                         30030 COUNT: 29                                                                34944 COUNT: 26                                                       
