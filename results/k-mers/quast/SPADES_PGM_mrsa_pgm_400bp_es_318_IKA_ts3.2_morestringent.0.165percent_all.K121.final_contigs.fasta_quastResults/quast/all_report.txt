All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K121.final_contigs
#Contigs                                   19126                                                                                       
#Contigs (>= 0 bp)                         19297                                                                                       
#Contigs (>= 1000 bp)                      52                                                                                          
Largest contig                             236877                                                                                      
Total length                               9214305                                                                                     
Total length (>= 0 bp)                     9239661                                                                                     
Total length (>= 1000 bp)                  2765409                                                                                     
Reference length                           2813862                                                                                     
N50                                        380                                                                                         
NG50                                       116205                                                                                      
N75                                        316                                                                                         
NG75                                       51591                                                                                       
L50                                        4581                                                                                        
LG50                                       9                                                                                           
L75                                        11227                                                                                       
LG75                                       18                                                                                          
#local misassemblies                       7                                                                                           
#misassemblies                             132                                                                                         
#misassembled contigs                      132                                                                                         
Misassembled contigs length                142802                                                                                      
Misassemblies inter-contig overlap         2354                                                                                        
#unaligned contigs                         1804 + 528 part                                                                             
Unaligned contigs length                   625298                                                                                      
#ambiguously mapped contigs                116                                                                                         
Extra bases in ambiguously mapped contigs  -43127                                                                                      
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        98.884                                                                                      
Duplication ratio                          3.072                                                                                       
#genes                                     2671 + 50 part                                                                              
#predicted genes (unique)                  18647                                                                                       
#predicted genes (>= 0 bp)                 18675                                                                                       
#predicted genes (>= 300 bp)               2412                                                                                        
#predicted genes (>= 1500 bp)              290                                                                                         
#predicted genes (>= 3000 bp)              31                                                                                          
#mismatches                                244                                                                                         
#indels                                    776                                                                                         
Indels length                              881                                                                                         
#mismatches per 100 kbp                    8.77                                                                                        
#indels per 100 kbp                        27.89                                                                                       
GC (%)                                     32.01                                                                                       
Reference GC (%)                           32.81                                                                                       
Average %IDY                               97.618                                                                                      
Largest alignment                          236877                                                                                      
NA50                                       374                                                                                         
NGA50                                      116205                                                                                      
NA75                                       304                                                                                         
NGA75                                      50644                                                                                       
LA50                                       4649                                                                                        
LGA50                                      9                                                                                           
LA75                                       11512                                                                                       
LGA75                                      19                                                                                          
#Mis_misassemblies                         132                                                                                         
#Mis_relocations                           124                                                                                         
#Mis_translocations                        8                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  132                                                                                         
Mis_Misassembled contigs length            142802                                                                                      
#Mis_local misassemblies                   7                                                                                           
#Mis_short indels (<= 5 bp)                773                                                                                         
#Mis_long indels (> 5 bp)                  3                                                                                           
#Una_fully unaligned contigs               1804                                                                                        
Una_Fully unaligned length                 598012                                                                                      
#Una_partially unaligned contigs           528                                                                                         
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            1                                                                                           
Una_Partially unaligned length             27286                                                                                       
GAGE_Contigs #                             19126                                                                                       
GAGE_Min contig                            200                                                                                         
GAGE_Max contig                            236877                                                                                      
GAGE_N50                                   116205 COUNT: 9                                                                             
GAGE_Genome size                           2813862                                                                                     
GAGE_Assembly size                         9214305                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               1388(0.05%)                                                                                 
GAGE_Missing assembly bases                330024(3.58%)                                                                               
GAGE_Missing assembly contigs              651(3.40%)                                                                                  
GAGE_Duplicated reference bases            6096718                                                                                     
GAGE_Compressed reference bases            35606                                                                                       
GAGE_Bad trim                              110884                                                                                      
GAGE_Avg idy                               99.98                                                                                       
GAGE_SNPs                                  52                                                                                          
GAGE_Indels < 5bp                          287                                                                                         
GAGE_Indels >= 5                           9                                                                                           
GAGE_Inversions                            0                                                                                           
GAGE_Relocation                            3                                                                                           
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    126                                                                                         
GAGE_Corrected assembly size               2790883                                                                                     
GAGE_Min correct contig                    200                                                                                         
GAGE_Max correct contig                    212216                                                                                      
GAGE_Corrected N50                         77418 COUNT: 11                                                                             
