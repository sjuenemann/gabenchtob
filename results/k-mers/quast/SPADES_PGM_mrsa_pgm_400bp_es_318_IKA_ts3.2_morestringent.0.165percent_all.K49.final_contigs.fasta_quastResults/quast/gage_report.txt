All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K49.final_contigs
GAGE_Contigs #                   412                                                                                        
GAGE_Min contig                  200                                                                                        
GAGE_Max contig                  148064                                                                                     
GAGE_N50                         48811 COUNT: 16                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2818092                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     23290(0.83%)                                                                               
GAGE_Missing assembly bases      56753(2.01%)                                                                               
GAGE_Missing assembly contigs    201(48.79%)                                                                                
GAGE_Duplicated reference bases  6271                                                                                       
GAGE_Compressed reference bases  38470                                                                                      
GAGE_Bad trim                    298                                                                                        
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        48                                                                                         
GAGE_Indels < 5bp                175                                                                                        
GAGE_Indels >= 5                 13                                                                                         
GAGE_Inversions                  0                                                                                          
GAGE_Relocation                  7                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          202                                                                                        
GAGE_Corrected assembly size     2758236                                                                                    
GAGE_Min correct contig          202                                                                                        
GAGE_Max correct contig          148067                                                                                     
GAGE_Corrected N50               42600 COUNT: 20                                                                            
