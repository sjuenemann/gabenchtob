All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_27.final.contig
#Contigs                                   502                                                                      
#Contigs (>= 0 bp)                         549049                                                                   
#Contigs (>= 1000 bp)                      0                                                                        
Largest contig                             461                                                                      
Total length                               121031                                                                   
Total length (>= 0 bp)                     24860695                                                                 
Total length (>= 1000 bp)                  0                                                                        
Reference length                           5594470                                                                  
N50                                        235                                                                      
NG50                                       None                                                                     
N75                                        213                                                                      
NG75                                       None                                                                     
L50                                        220                                                                      
LG50                                       None                                                                     
L75                                        356                                                                      
LG75                                       None                                                                     
#local misassemblies                       0                                                                        
#misassemblies                             0                                                                        
#misassembled contigs                      0                                                                        
Misassembled contigs length                0                                                                        
Misassemblies inter-contig overlap         0                                                                        
#unaligned contigs                         29 + 1 part                                                              
Unaligned contigs length                   8511                                                                     
#ambiguously mapped contigs                0                                                                        
Extra bases in ambiguously mapped contigs  0                                                                        
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        2.011                                                                    
Duplication ratio                          1.000                                                                    
#genes                                     3 + 429 part                                                             
#predicted genes (unique)                  512                                                                      
#predicted genes (>= 0 bp)                 512                                                                      
#predicted genes (>= 300 bp)               30                                                                       
#predicted genes (>= 1500 bp)              0                                                                        
#predicted genes (>= 3000 bp)              0                                                                        
#mismatches                                1                                                                        
#indels                                    44                                                                       
Indels length                              44                                                                       
#mismatches per 100 kbp                    0.89                                                                     
#indels per 100 kbp                        39.11                                                                    
GC (%)                                     53.76                                                                    
Reference GC (%)                           50.48                                                                    
Average %IDY                               99.959                                                                   
Largest alignment                          461                                                                      
NA50                                       229                                                                      
NGA50                                      None                                                                     
NA75                                       209                                                                      
NGA75                                      None                                                                     
LA50                                       228                                                                      
LGA50                                      None                                                                     
LA75                                       367                                                                      
LGA75                                      None                                                                     
#Mis_misassemblies                         0                                                                        
#Mis_relocations                           0                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  0                                                                        
Mis_Misassembled contigs length            0                                                                        
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                44                                                                       
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               29                                                                       
Una_Fully unaligned length                 8462                                                                     
#Una_partially unaligned contigs           1                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             49                                                                       
GAGE_Contigs #                             502                                                                      
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            461                                                                      
GAGE_N50                                   0 COUNT: 0                                                               
GAGE_Genome size                           5594470                                                                  
GAGE_Assembly size                         121031                                                                   
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               5481962(97.99%)                                                          
GAGE_Missing assembly bases                8511(7.03%)                                                              
GAGE_Missing assembly contigs              29(5.78%)                                                                
GAGE_Duplicated reference bases            0                                                                        
GAGE_Compressed reference bases            0                                                                        
GAGE_Bad trim                              49                                                                       
GAGE_Avg idy                               99.96                                                                    
GAGE_SNPs                                  1                                                                        
GAGE_Indels < 5bp                          44                                                                       
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            0                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    472                                                                      
GAGE_Corrected assembly size               112340                                                                   
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    461                                                                      
GAGE_Corrected N50                         0 COUNT: 0                                                               
