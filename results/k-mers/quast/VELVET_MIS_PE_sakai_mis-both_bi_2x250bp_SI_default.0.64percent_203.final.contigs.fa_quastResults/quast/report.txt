All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_203.final.contigs_broken  VELVET_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent_203.final.contigs
#Contigs (>= 0 bp)             1990                                                                                     1284                                                                            
#Contigs (>= 1000 bp)          1503                                                                                     1032                                                                            
Total length (>= 0 bp)         5490474                                                                                  5590492                                                                         
Total length (>= 1000 bp)      5150240                                                                                  5417630                                                                         
#Contigs                       1989                                                                                     1284                                                                            
Largest contig                 20765                                                                                    37919                                                                           
Total length                   5490329                                                                                  5590492                                                                         
Reference length               5594470                                                                                  5594470                                                                         
GC (%)                         50.36                                                                                    50.36                                                                           
Reference GC (%)               50.48                                                                                    50.48                                                                           
N50                            4057                                                                                     7409                                                                            
NG50                           4009                                                                                     7409                                                                            
N75                            2370                                                                                     3928                                                                            
NG75                           2280                                                                                     3926                                                                            
#misassemblies                 7                                                                                        16                                                                              
#local misassemblies           2                                                                                        658                                                                             
#unaligned contigs             1 + 1 part                                                                               1 + 1 part                                                                      
Unaligned contigs length       5862                                                                                     7063                                                                            
Genome fraction (%)            94.254                                                                                   94.318                                                                          
Duplication ratio              1.024                                                                                    1.041                                                                           
#N's per 100 kbp               0.00                                                                                     1789.07                                                                         
#mismatches per 100 kbp        7.81                                                                                     7.83                                                                            
#indels per 100 kbp            0.59                                                                                     19.63                                                                           
#genes                         3830 + 1418 part                                                                         3867 + 1384 part                                                                
#predicted genes (unique)      6662                                                                                     6565                                                                            
#predicted genes (>= 0 bp)     6688                                                                                     6592                                                                            
#predicted genes (>= 300 bp)   5181                                                                                     5181                                                                            
#predicted genes (>= 1500 bp)  490                                                                                      492                                                                             
#predicted genes (>= 3000 bp)  23                                                                                       23                                                                              
Largest alignment              20765                                                                                    37495                                                                           
NA50                           4041                                                                                     6886                                                                            
NGA50                          3992                                                                                     6867                                                                            
NA75                           2309                                                                                     3612                                                                            
NGA75                          2246                                                                                     3612                                                                            
