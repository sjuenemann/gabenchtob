All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SOAP2_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_75.final.contig
#Contigs                                   26662                                                                               
#Contigs (>= 0 bp)                         175620                                                                              
#Contigs (>= 1000 bp)                      0                                                                                   
Largest contig                             659                                                                                 
Total length                               7056180                                                                             
Total length (>= 0 bp)                     23585791                                                                            
Total length (>= 1000 bp)                  0                                                                                   
Reference length                           2813862                                                                             
N50                                        259                                                                                 
NG50                                       298                                                                                 
N75                                        240                                                                                 
NG75                                       282                                                                                 
L50                                        11920                                                                               
LG50                                       4232                                                                                
L75                                        18999                                                                               
LG75                                       6664                                                                                
#local misassemblies                       2                                                                                   
#misassemblies                             144                                                                                 
#misassembled contigs                      144                                                                                 
Misassembled contigs length                38361                                                                               
Misassemblies inter-contig overlap         421                                                                                 
#unaligned contigs                         5214 + 567 part                                                                     
Unaligned contigs length                   1456503                                                                             
#ambiguously mapped contigs                85                                                                                  
Extra bases in ambiguously mapped contigs  -21633                                                                              
#N's                                       0                                                                                   
#N's per 100 kbp                           0.00                                                                                
Genome fraction (%)                        85.939                                                                              
Duplication ratio                          2.307                                                                               
#genes                                     179 + 2438 part                                                                     
#predicted genes (unique)                  19173                                                                               
#predicted genes (>= 0 bp)                 19217                                                                               
#predicted genes (>= 300 bp)               147                                                                                 
#predicted genes (>= 1500 bp)              0                                                                                   
#predicted genes (>= 3000 bp)              0                                                                                   
#mismatches                                3929                                                                                
#indels                                    54618                                                                               
Indels length                              56140                                                                               
#mismatches per 100 kbp                    162.48                                                                              
#indels per 100 kbp                        2258.62                                                                             
GC (%)                                     31.71                                                                               
Reference GC (%)                           32.81                                                                               
Average %IDY                               97.476                                                                              
Largest alignment                          531                                                                                 
NA50                                       246                                                                                 
NGA50                                      285                                                                                 
NA75                                       225                                                                                 
NGA75                                      269                                                                                 
LA50                                       12485                                                                               
LGA50                                      4421                                                                                
LA75                                       19981                                                                               
LGA75                                      6965                                                                                
#Mis_misassemblies                         144                                                                                 
#Mis_relocations                           134                                                                                 
#Mis_translocations                        7                                                                                   
#Mis_inversions                            3                                                                                   
#Mis_misassembled contigs                  144                                                                                 
Mis_Misassembled contigs length            38361                                                                               
#Mis_local misassemblies                   2                                                                                   
#Mis_short indels (<= 5 bp)                54618                                                                               
#Mis_long indels (> 5 bp)                  0                                                                                   
#Una_fully unaligned contigs               5214                                                                                
Una_Fully unaligned length                 1413126                                                                             
#Una_partially unaligned contigs           567                                                                                 
#Una_with misassembly                      0                                                                                   
#Una_both parts are significant            0                                                                                   
Una_Partially unaligned length             43377                                                                               
GAGE_Contigs #                             26662                                                                               
GAGE_Min contig                            200                                                                                 
GAGE_Max contig                            659                                                                                 
GAGE_N50                                   298 COUNT: 4232                                                                     
GAGE_Genome size                           2813862                                                                             
GAGE_Assembly size                         7056180                                                                             
GAGE_Chaff bases                           0                                                                                   
GAGE_Missing reference bases               283661(10.08%)                                                                      
GAGE_Missing assembly bases                742921(10.53%)                                                                      
GAGE_Missing assembly contigs              2061(7.73%)                                                                         
GAGE_Duplicated reference bases            3056589                                                                             
GAGE_Compressed reference bases            52254                                                                               
GAGE_Bad trim                              176542                                                                              
GAGE_Avg idy                               97.66                                                                               
GAGE_SNPs                                  2671                                                                                
GAGE_Indels < 5bp                          42042                                                                               
GAGE_Indels >= 5                           9                                                                                   
GAGE_Inversions                            11                                                                                  
GAGE_Relocation                            11                                                                                  
GAGE_Translocation                         1                                                                                   
GAGE_Corrected contig #                    12160                                                                               
GAGE_Corrected assembly size               3171001                                                                             
GAGE_Min correct contig                    200                                                                                 
GAGE_Max correct contig                    543                                                                                 
GAGE_Corrected N50                         262 COUNT: 4742                                                                     
