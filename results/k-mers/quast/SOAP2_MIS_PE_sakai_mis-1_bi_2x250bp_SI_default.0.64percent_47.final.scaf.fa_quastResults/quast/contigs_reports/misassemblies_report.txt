All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_47.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default.0.64percent_47.final.scaf
#Mis_misassemblies               1                                                                                1                                                                       
#Mis_relocations                 1                                                                                1                                                                       
#Mis_translocations              0                                                                                0                                                                       
#Mis_inversions                  0                                                                                0                                                                       
#Mis_misassembled contigs        1                                                                                1                                                                       
Mis_Misassembled contigs length  338                                                                              338                                                                     
#Mis_local misassemblies         0                                                                                0                                                                       
#mismatches                      125                                                                              125                                                                     
#indels                          7                                                                                7                                                                       
#Mis_short indels (<= 5 bp)      7                                                                                7                                                                       
#Mis_long indels (> 5 bp)        0                                                                                0                                                                       
Indels length                    7                                                                                7                                                                       
