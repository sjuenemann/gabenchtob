All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_95.final.contigs
#Contigs                                   1876                                                                                 
#Contigs (>= 0 bp)                         1903                                                                                 
#Contigs (>= 1000 bp)                      1119                                                                                 
Largest contig                             24691                                                                                
Total length                               5307528                                                                              
Total length (>= 0 bp)                     5312758                                                                              
Total length (>= 1000 bp)                  4930487                                                                              
Reference length                           5594470                                                                              
N50                                        5822                                                                                 
NG50                                       5522                                                                                 
N75                                        2879                                                                                 
NG75                                       2469                                                                                 
L50                                        278                                                                                  
LG50                                       303                                                                                  
L75                                        595                                                                                  
LG75                                       675                                                                                  
#local misassemblies                       42                                                                                   
#misassemblies                             53                                                                                   
#misassembled contigs                      48                                                                                   
Misassembled contigs length                295991                                                                               
Misassemblies inter-contig overlap         1304                                                                                 
#unaligned contigs                         0 + 2 part                                                                           
Unaligned contigs length                   247                                                                                  
#ambiguously mapped contigs                227                                                                                  
Extra bases in ambiguously mapped contigs  -96533                                                                               
#N's                                       860                                                                                  
#N's per 100 kbp                           16.20                                                                                
Genome fraction (%)                        92.356                                                                               
Duplication ratio                          1.009                                                                                
#genes                                     3973 + 1114 part                                                                     
#predicted genes (unique)                  6908                                                                                 
#predicted genes (>= 0 bp)                 6918                                                                                 
#predicted genes (>= 300 bp)               5064                                                                                 
#predicted genes (>= 1500 bp)              430                                                                                  
#predicted genes (>= 3000 bp)              22                                                                                   
#mismatches                                189                                                                                  
#indels                                    1859                                                                                 
Indels length                              2089                                                                                 
#mismatches per 100 kbp                    3.66                                                                                 
#indels per 100 kbp                        35.98                                                                                
GC (%)                                     50.46                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               99.204                                                                               
Largest alignment                          24654                                                                                
NA50                                       5698                                                                                 
NGA50                                      5463                                                                                 
NA75                                       2803                                                                                 
NGA75                                      2418                                                                                 
LA50                                       284                                                                                  
LGA50                                      310                                                                                  
LA75                                       608                                                                                  
LGA75                                      691                                                                                  
#Mis_misassemblies                         53                                                                                   
#Mis_relocations                           49                                                                                   
#Mis_translocations                        0                                                                                    
#Mis_inversions                            4                                                                                    
#Mis_misassembled contigs                  48                                                                                   
Mis_Misassembled contigs length            295991                                                                               
#Mis_local misassemblies                   42                                                                                   
#Mis_short indels (<= 5 bp)                1853                                                                                 
#Mis_long indels (> 5 bp)                  6                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           2                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             247                                                                                  
GAGE_Contigs #                             1876                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            24691                                                                                
GAGE_N50                                   5522 COUNT: 303                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5307528                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               140839(2.52%)                                                                        
GAGE_Missing assembly bases                1099(0.02%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            14531                                                                                
GAGE_Compressed reference bases            274939                                                                               
GAGE_Bad trim                              429                                                                                  
GAGE_Avg idy                               99.96                                                                                
GAGE_SNPs                                  130                                                                                  
GAGE_Indels < 5bp                          1895                                                                                 
GAGE_Indels >= 5                           37                                                                                   
GAGE_Inversions                            1                                                                                    
GAGE_Relocation                            33                                                                                   
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1909                                                                                 
GAGE_Corrected assembly size               5294442                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    24669                                                                                
GAGE_Corrected N50                         5274 COUNT: 320                                                                      
