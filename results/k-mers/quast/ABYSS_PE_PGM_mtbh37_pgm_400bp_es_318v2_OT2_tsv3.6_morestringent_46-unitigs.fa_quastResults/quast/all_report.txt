All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_46-unitigs
#Contigs                                   2278                                                                      
#Contigs (>= 0 bp)                         10953                                                                     
#Contigs (>= 1000 bp)                      1320                                                                      
Largest contig                             12785                                                                     
Total length                               4158472                                                                   
Total length (>= 0 bp)                     4871768                                                                   
Total length (>= 1000 bp)                  3642980                                                                   
Reference length                           4411532                                                                   
N50                                        2911                                                                      
NG50                                       2736                                                                      
N75                                        1599                                                                      
NG75                                       1370                                                                      
L50                                        431                                                                       
LG50                                       476                                                                       
L75                                        909                                                                       
LG75                                       1037                                                                      
#local misassemblies                       10                                                                        
#misassemblies                             0                                                                         
#misassembled contigs                      0                                                                         
Misassembled contigs length                0                                                                         
Misassemblies inter-contig overlap         1191                                                                      
#unaligned contigs                         2 + 1 part                                                                
Unaligned contigs length                   870                                                                       
#ambiguously mapped contigs                29                                                                        
Extra bases in ambiguously mapped contigs  -12084                                                                    
#N's                                       0                                                                         
#N's per 100 kbp                           0.00                                                                      
Genome fraction (%)                        93.906                                                                    
Duplication ratio                          1.001                                                                     
#genes                                     2471 + 1540 part                                                          
#predicted genes (unique)                  5651                                                                      
#predicted genes (>= 0 bp)                 5654                                                                      
#predicted genes (>= 300 bp)               4173                                                                      
#predicted genes (>= 1500 bp)              294                                                                       
#predicted genes (>= 3000 bp)              21                                                                        
#mismatches                                137                                                                       
#indels                                    622                                                                       
Indels length                              656                                                                       
#mismatches per 100 kbp                    3.31                                                                      
#indels per 100 kbp                        15.01                                                                     
GC (%)                                     65.34                                                                     
Reference GC (%)                           65.61                                                                     
Average %IDY                               99.973                                                                    
Largest alignment                          12785                                                                     
NA50                                       2911                                                                      
NGA50                                      2736                                                                      
NA75                                       1599                                                                      
NGA75                                      1370                                                                      
LA50                                       431                                                                       
LGA50                                      476                                                                       
LA75                                       909                                                                       
LGA75                                      1037                                                                      
#Mis_misassemblies                         0                                                                         
#Mis_relocations                           0                                                                         
#Mis_translocations                        0                                                                         
#Mis_inversions                            0                                                                         
#Mis_misassembled contigs                  0                                                                         
Mis_Misassembled contigs length            0                                                                         
#Mis_local misassemblies                   10                                                                        
#Mis_short indels (<= 5 bp)                621                                                                       
#Mis_long indels (> 5 bp)                  1                                                                         
#Una_fully unaligned contigs               2                                                                         
Una_Fully unaligned length                 826                                                                       
#Una_partially unaligned contigs           1                                                                         
#Una_with misassembly                      0                                                                         
#Una_both parts are significant            0                                                                         
Una_Partially unaligned length             44                                                                        
GAGE_Contigs #                             2278                                                                      
GAGE_Min contig                            200                                                                       
GAGE_Max contig                            12785                                                                     
GAGE_N50                                   2736 COUNT: 476                                                           
GAGE_Genome size                           4411532                                                                   
GAGE_Assembly size                         4158472                                                                   
GAGE_Chaff bases                           0                                                                         
GAGE_Missing reference bases               241387(5.47%)                                                             
GAGE_Missing assembly bases                879(0.02%)                                                                
GAGE_Missing assembly contigs              2(0.09%)                                                                  
GAGE_Duplicated reference bases            0                                                                         
GAGE_Compressed reference bases            17064                                                                     
GAGE_Bad trim                              53                                                                        
GAGE_Avg idy                               99.98                                                                     
GAGE_SNPs                                  83                                                                        
GAGE_Indels < 5bp                          640                                                                       
GAGE_Indels >= 5                           8                                                                         
GAGE_Inversions                            0                                                                         
GAGE_Relocation                            4                                                                         
GAGE_Translocation                         0                                                                         
GAGE_Corrected contig #                    2288                                                                      
GAGE_Corrected assembly size               4159378                                                                   
GAGE_Min correct contig                    200                                                                       
GAGE_Max correct contig                    12787                                                                     
GAGE_Corrected N50                         2723 COUNT: 479                                                           
