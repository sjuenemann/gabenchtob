All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_84-unitigs
#Contigs (>= 0 bp)             3658                                                                              
#Contigs (>= 1000 bp)          297                                                                               
Total length (>= 0 bp)         3153769                                                                           
Total length (>= 1000 bp)      2693081                                                                           
#Contigs                       433                                                                               
Largest contig                 67202                                                                             
Total length                   2757560                                                                           
Reference length               2813862                                                                           
GC (%)                         32.60                                                                             
Reference GC (%)               32.81                                                                             
N50                            14016                                                                             
NG50                           13683                                                                             
N75                            8071                                                                              
NG75                           7735                                                                              
#misassemblies                 1                                                                                 
#local misassemblies           1                                                                                 
#unaligned contigs             0 + 0 part                                                                        
Unaligned contigs length       0                                                                                 
Genome fraction (%)            97.691                                                                            
Duplication ratio              1.002                                                                             
#N's per 100 kbp               0.00                                                                              
#mismatches per 100 kbp        1.09                                                                              
#indels per 100 kbp            4.18                                                                              
#genes                         2446 + 235 part                                                                   
#predicted genes (unique)      2876                                                                              
#predicted genes (>= 0 bp)     2879                                                                              
#predicted genes (>= 300 bp)   2372                                                                              
#predicted genes (>= 1500 bp)  266                                                                               
#predicted genes (>= 3000 bp)  23                                                                                
Largest alignment              67202                                                                             
NA50                           14016                                                                             
NGA50                          13683                                                                             
NA75                           8071                                                                              
NGA75                          7735                                                                              
