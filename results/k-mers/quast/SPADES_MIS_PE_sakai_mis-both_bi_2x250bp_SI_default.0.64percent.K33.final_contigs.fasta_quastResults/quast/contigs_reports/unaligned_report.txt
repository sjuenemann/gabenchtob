All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K33.final_contigs_broken  SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K33.final_contigs
#Una_fully unaligned contigs      262                                                                                      262                                                                             
Una_Fully unaligned length        101346                                                                                   101346                                                                          
#Una_partially unaligned contigs  1                                                                                        1                                                                               
#Una_with misassembly             0                                                                                        0                                                                               
#Una_both parts are significant   1                                                                                        1                                                                               
Una_Partially unaligned length    222                                                                                      222                                                                             
#N's                              0                                                                                        0                                                                               
