All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_29.final.contig
GAGE_Contigs #                   601                                                                      
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  494                                                                      
GAGE_N50                         0 COUNT: 0                                                               
GAGE_Genome size                 5594470                                                                  
GAGE_Assembly size               145333                                                                   
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     5458918(97.58%)                                                          
GAGE_Missing assembly bases      9961(6.85%)                                                              
GAGE_Missing assembly contigs    36(5.99%)                                                                
GAGE_Duplicated reference bases  0                                                                        
GAGE_Compressed reference bases  204                                                                      
GAGE_Bad trim                    1                                                                        
GAGE_Avg idy                     99.96                                                                    
GAGE_SNPs                        2                                                                        
GAGE_Indels < 5bp                53                                                                       
GAGE_Indels >= 5                 0                                                                        
GAGE_Inversions                  0                                                                        
GAGE_Relocation                  0                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          565                                                                      
GAGE_Corrected assembly size     135353                                                                   
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          494                                                                      
GAGE_Corrected N50               0 COUNT: 0                                                               
