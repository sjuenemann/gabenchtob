All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent_all.K73.final_contigs
#Contigs                                   959                                                                             
#Contigs (>= 0 bp)                         1377                                                                            
#Contigs (>= 1000 bp)                      191                                                                             
Largest contig                             268243                                                                          
Total length                               5458838                                                                         
Total length (>= 0 bp)                     5506353                                                                         
Total length (>= 1000 bp)                  5204673                                                                         
Reference length                           5594470                                                                         
N50                                        99701                                                                           
NG50                                       99701                                                                           
N75                                        35760                                                                           
NG75                                       32855                                                                           
L50                                        18                                                                              
LG50                                       18                                                                              
L75                                        39                                                                              
LG75                                       41                                                                              
#local misassemblies                       13                                                                              
#misassemblies                             3                                                                               
#misassembled contigs                      3                                                                               
Misassembled contigs length                234713                                                                          
Misassemblies inter-contig overlap         2088                                                                            
#unaligned contigs                         256 + 6 part                                                                    
Unaligned contigs length                   66405                                                                           
#ambiguously mapped contigs                145                                                                             
Extra bases in ambiguously mapped contigs  -90123                                                                          
#N's                                       0                                                                               
#N's per 100 kbp                           0.00                                                                            
Genome fraction (%)                        93.811                                                                          
Duplication ratio                          1.011                                                                           
#genes                                     4846 + 228 part                                                                 
#predicted genes (unique)                  5806                                                                            
#predicted genes (>= 0 bp)                 5811                                                                            
#predicted genes (>= 300 bp)               4534                                                                            
#predicted genes (>= 1500 bp)              639                                                                             
#predicted genes (>= 3000 bp)              62                                                                              
#mismatches                                191                                                                             
#indels                                    384                                                                             
Indels length                              474                                                                             
#mismatches per 100 kbp                    3.64                                                                            
#indels per 100 kbp                        7.32                                                                            
GC (%)                                     50.24                                                                           
Reference GC (%)                           50.48                                                                           
Average %IDY                               98.864                                                                          
Largest alignment                          268243                                                                          
NA50                                       99701                                                                           
NGA50                                      97818                                                                           
NA75                                       35573                                                                           
NGA75                                      32656                                                                           
LA50                                       18                                                                              
LGA50                                      19                                                                              
LA75                                       40                                                                              
LGA75                                      43                                                                              
#Mis_misassemblies                         3                                                                               
#Mis_relocations                           3                                                                               
#Mis_translocations                        0                                                                               
#Mis_inversions                            0                                                                               
#Mis_misassembled contigs                  3                                                                               
Mis_Misassembled contigs length            234713                                                                          
#Mis_local misassemblies                   13                                                                              
#Mis_short indels (<= 5 bp)                378                                                                             
#Mis_long indels (> 5 bp)                  6                                                                               
#Una_fully unaligned contigs               256                                                                             
Una_Fully unaligned length                 65860                                                                           
#Una_partially unaligned contigs           6                                                                               
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            0                                                                               
Una_Partially unaligned length             545                                                                             
GAGE_Contigs #                             959                                                                             
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            268243                                                                          
GAGE_N50                                   99701 COUNT: 18                                                                 
GAGE_Genome size                           5594470                                                                         
GAGE_Assembly size                         5458838                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               17259(0.31%)                                                                    
GAGE_Missing assembly bases                22464(0.41%)                                                                    
GAGE_Missing assembly contigs              70(7.30%)                                                                       
GAGE_Duplicated reference bases            90757                                                                           
GAGE_Compressed reference bases            289308                                                                          
GAGE_Bad trim                              3074                                                                            
GAGE_Avg idy                               99.98                                                                           
GAGE_SNPs                                  193                                                                             
GAGE_Indels < 5bp                          389                                                                             
GAGE_Indels >= 5                           11                                                                              
GAGE_Inversions                            0                                                                               
GAGE_Relocation                            9                                                                               
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    536                                                                             
GAGE_Corrected assembly size               5348089                                                                         
GAGE_Min correct contig                    200                                                                             
GAGE_Max correct contig                    217413                                                                          
GAGE_Corrected N50                         86407 COUNT: 21                                                                 
