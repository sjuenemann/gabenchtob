All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_87.final.contig
GAGE_Contigs #                   8960                                                                               
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  3748                                                                               
GAGE_N50                         612 COUNT: 2793                                                                    
GAGE_Genome size                 5594470                                                                            
GAGE_Assembly size               4949034                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     664273(11.87%)                                                                     
GAGE_Missing assembly bases      1386(0.03%)                                                                        
GAGE_Missing assembly contigs    2(0.02%)                                                                           
GAGE_Duplicated reference bases  74479                                                                              
GAGE_Compressed reference bases  141950                                                                             
GAGE_Bad trim                    858                                                                                
GAGE_Avg idy                     99.97                                                                              
GAGE_SNPs                        50                                                                                 
GAGE_Indels < 5bp                1138                                                                               
GAGE_Indels >= 5                 3                                                                                  
GAGE_Inversions                  0                                                                                  
GAGE_Relocation                  1                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          8648                                                                               
GAGE_Corrected assembly size     4874486                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          3749                                                                               
GAGE_Corrected N50               613 COUNT: 2793                                                                    
