All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_127.final.contigs
GAGE_Contigs #                   4895                                                                           
GAGE_Min contig                  253                                                                            
GAGE_Max contig                  1795                                                                           
GAGE_N50                         0 COUNT: 0                                                                     
GAGE_Genome size                 4411532                                                                        
GAGE_Assembly size               1968020                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     2609488(59.15%)                                                                
GAGE_Missing assembly bases      68(0.00%)                                                                      
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  94815                                                                          
GAGE_Compressed reference bases  33314                                                                          
GAGE_Bad trim                    41                                                                             
GAGE_Avg idy                     99.89                                                                          
GAGE_SNPs                        121                                                                            
GAGE_Indels < 5bp                1660                                                                           
GAGE_Indels >= 5                 2                                                                              
GAGE_Inversions                  62                                                                             
GAGE_Relocation                  27                                                                             
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          4562                                                                           
GAGE_Corrected assembly size     1853561                                                                        
GAGE_Min correct contig          200                                                                            
GAGE_Max correct contig          1795                                                                           
GAGE_Corrected N50               0 COUNT: 0                                                                     
