All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         VELVET_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_127.final.contigs
#Mis_misassemblies               150                                                                            
#Mis_relocations                 148                                                                            
#Mis_translocations              0                                                                              
#Mis_inversions                  2                                                                              
#Mis_misassembled contigs        150                                                                            
Mis_Misassembled contigs length  66020                                                                          
#Mis_local misassemblies         2                                                                              
#mismatches                      148                                                                            
#indels                          1819                                                                           
#Mis_short indels (<= 5 bp)      1819                                                                           
#Mis_long indels (> 5 bp)        0                                                                              
Indels length                    1879                                                                           
