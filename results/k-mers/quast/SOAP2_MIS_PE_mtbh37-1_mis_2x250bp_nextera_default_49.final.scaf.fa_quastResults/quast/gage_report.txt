All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_49.final.scaf_broken  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_49.final.scaf
GAGE_Contigs #                   72136                                                                   72140                                                          
GAGE_Min contig                  200                                                                     200                                                            
GAGE_Max contig                  1262                                                                    1262                                                           
GAGE_N50                         251 COUNT: 6868                                                         251 COUNT: 6865                                                
GAGE_Genome size                 4411532                                                                 4411532                                                        
GAGE_Assembly size               17346692                                                                17348358                                                       
GAGE_Chaff bases                 0                                                                       0                                                              
GAGE_Missing reference bases     1923564(43.60%)                                                         1922311(43.57%)                                                
GAGE_Missing assembly bases      14579054(84.05%)                                                        14579491(84.04%)                                               
GAGE_Missing assembly contigs    62043(86.01%)                                                           62043(86.00%)                                                  
GAGE_Duplicated reference bases  125625                                                                  125625                                                         
GAGE_Compressed reference bases  21503                                                                   21503                                                          
GAGE_Bad trim                    79303                                                                   79303                                                          
GAGE_Avg idy                     99.30                                                                   99.30                                                          
GAGE_SNPs                        13323                                                                   13323                                                          
GAGE_Indels < 5bp                144                                                                     144                                                            
GAGE_Indels >= 5                 2                                                                       5                                                              
GAGE_Inversions                  0                                                                       0                                                              
GAGE_Relocation                  1                                                                       1                                                              
GAGE_Translocation               0                                                                       0                                                              
GAGE_Corrected contig #          8713                                                                    8713                                                           
GAGE_Corrected assembly size     2531474                                                                 2531474                                                        
GAGE_Min correct contig          200                                                                     200                                                            
GAGE_Max correct contig          1262                                                                    1262                                                           
GAGE_Corrected N50               214 COUNT: 7135                                                         214 COUNT: 7135                                                
