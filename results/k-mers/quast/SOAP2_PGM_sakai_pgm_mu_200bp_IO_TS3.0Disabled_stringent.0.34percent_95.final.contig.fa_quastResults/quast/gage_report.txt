All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_95.final.contig
GAGE_Contigs #                   7995                                                                               
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  5064                                                                               
GAGE_N50                         800 COUNT: 2134                                                                    
GAGE_Genome size                 5594470                                                                            
GAGE_Assembly size               5192451                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     444500(7.95%)                                                                      
GAGE_Missing assembly bases      233(0.00%)                                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  94436                                                                              
GAGE_Compressed reference bases  173182                                                                             
GAGE_Bad trim                    233                                                                                
GAGE_Avg idy                     99.97                                                                              
GAGE_SNPs                        54                                                                                 
GAGE_Indels < 5bp                1234                                                                               
GAGE_Indels >= 5                 0                                                                                  
GAGE_Inversions                  1                                                                                  
GAGE_Relocation                  1                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          7606                                                                               
GAGE_Corrected assembly size     5098197                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          5064                                                                               
GAGE_Corrected N50               801 COUNT: 2134                                                                    
