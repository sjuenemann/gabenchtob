All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_72-scaffolds_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_72-scaffolds
#Una_fully unaligned contigs      0                                                                                             0                                                                                    
Una_Fully unaligned length        0                                                                                             0                                                                                    
#Una_partially unaligned contigs  0                                                                                             2                                                                                    
#Una_with misassembly             0                                                                                             0                                                                                    
#Una_both parts are significant   0                                                                                             0                                                                                    
Una_Partially unaligned length    0                                                                                             1386                                                                                 
#N's                              35                                                                                            2339                                                                                 
