reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  VELVET_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default_57.final.contigs_broken  | 95.7526092976       | 1.00098244454     | 363         | 3650      | 377       | None      | None      |
  VELVET_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default_57.final.contigs  | 95.8200915238       | 1.00580369584     | 309         | 3770      | 255       | None      | None      |
