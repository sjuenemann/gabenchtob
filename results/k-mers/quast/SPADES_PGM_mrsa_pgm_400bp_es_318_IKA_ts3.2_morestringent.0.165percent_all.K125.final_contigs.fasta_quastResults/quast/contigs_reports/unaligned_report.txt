All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_all.K125.final_contigs
#Una_fully unaligned contigs      1747                                                                                        
Una_Fully unaligned length        587280                                                                                      
#Una_partially unaligned contigs  607                                                                                         
#Una_with misassembly             0                                                                                           
#Una_both parts are significant   1                                                                                           
Una_Partially unaligned length    31483                                                                                       
#N's                              0                                                                                           
