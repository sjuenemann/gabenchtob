All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.300percent.scf
GAGE_Contigs #                   73                                                                       
GAGE_Min contig                  1021                                                                     
GAGE_Max contig                  314717                                                                   
GAGE_N50                         68271 COUNT: 11                                                          
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2759495                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     50932(1.81%)                                                             
GAGE_Missing assembly bases      26(0.00%)                                                                
GAGE_Missing assembly contigs    0(0.00%)                                                                 
GAGE_Duplicated reference bases  0                                                                        
GAGE_Compressed reference bases  26138                                                                    
GAGE_Bad trim                    26                                                                       
GAGE_Avg idy                     99.97                                                                    
GAGE_SNPs                        37                                                                       
GAGE_Indels < 5bp                487                                                                      
GAGE_Indels >= 5                 4                                                                        
GAGE_Inversions                  1                                                                        
GAGE_Relocation                  2                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          80                                                                       
GAGE_Corrected assembly size     2761422                                                                  
GAGE_Min correct contig          270                                                                      
GAGE_Max correct contig          234576                                                                   
GAGE_Corrected N50               66018 COUNT: 12                                                          
