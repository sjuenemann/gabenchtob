All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.300percent.scf
#Contigs                                   73                                                                       
#Contigs (>= 0 bp)                         73                                                                       
#Contigs (>= 1000 bp)                      73                                                                       
Largest contig                             314717                                                                   
Total length                               2759495                                                                  
Total length (>= 0 bp)                     2759495                                                                  
Total length (>= 1000 bp)                  2759495                                                                  
Reference length                           2813862                                                                  
N50                                        69110                                                                    
NG50                                       68271                                                                    
N75                                        38095                                                                    
NG75                                       36017                                                                    
L50                                        10                                                                       
LG50                                       11                                                                       
L75                                        23                                                                       
LG75                                       24                                                                       
#local misassemblies                       4                                                                        
#misassemblies                             3                                                                        
#misassembled contigs                      3                                                                        
Misassembled contigs length                118563                                                                   
Misassemblies inter-contig overlap         1816                                                                     
#unaligned contigs                         0 + 0 part                                                               
Unaligned contigs length                   0                                                                        
#ambiguously mapped contigs                3                                                                        
Extra bases in ambiguously mapped contigs  -3297                                                                    
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        97.463                                                                   
Duplication ratio                          1.006                                                                    
#genes                                     2629 + 47 part                                                           
#predicted genes (unique)                  2762                                                                     
#predicted genes (>= 0 bp)                 2765                                                                     
#predicted genes (>= 300 bp)               2329                                                                     
#predicted genes (>= 1500 bp)              264                                                                      
#predicted genes (>= 3000 bp)              28                                                                       
#mismatches                                127                                                                      
#indels                                    506                                                                      
Indels length                              517                                                                      
#mismatches per 100 kbp                    4.63                                                                     
#indels per 100 kbp                        18.45                                                                    
GC (%)                                     32.64                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               98.419                                                                   
Largest alignment                          314717                                                                   
NA50                                       68271                                                                    
NGA50                                      66808                                                                    
NA75                                       38095                                                                    
NGA75                                      36017                                                                    
LA50                                       10                                                                       
LGA50                                      11                                                                       
LA75                                       23                                                                       
LGA75                                      24                                                                       
#Mis_misassemblies                         3                                                                        
#Mis_relocations                           3                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  3                                                                        
Mis_Misassembled contigs length            118563                                                                   
#Mis_local misassemblies                   4                                                                        
#Mis_short indels (<= 5 bp)                505                                                                      
#Mis_long indels (> 5 bp)                  1                                                                        
#Una_fully unaligned contigs               0                                                                        
Una_Fully unaligned length                 0                                                                        
#Una_partially unaligned contigs           0                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             0                                                                        
GAGE_Contigs #                             73                                                                       
GAGE_Min contig                            1021                                                                     
GAGE_Max contig                            314717                                                                   
GAGE_N50                                   68271 COUNT: 11                                                          
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         2759495                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               50932(1.81%)                                                             
GAGE_Missing assembly bases                26(0.00%)                                                                
GAGE_Missing assembly contigs              0(0.00%)                                                                 
GAGE_Duplicated reference bases            0                                                                        
GAGE_Compressed reference bases            26138                                                                    
GAGE_Bad trim                              26                                                                       
GAGE_Avg idy                               99.97                                                                    
GAGE_SNPs                                  37                                                                       
GAGE_Indels < 5bp                          487                                                                      
GAGE_Indels >= 5                           4                                                                        
GAGE_Inversions                            1                                                                        
GAGE_Relocation                            2                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    80                                                                       
GAGE_Corrected assembly size               2761422                                                                  
GAGE_Min correct contig                    270                                                                      
GAGE_Max correct contig                    234576                                                                   
GAGE_Corrected N50                         66018 COUNT: 12                                                          
