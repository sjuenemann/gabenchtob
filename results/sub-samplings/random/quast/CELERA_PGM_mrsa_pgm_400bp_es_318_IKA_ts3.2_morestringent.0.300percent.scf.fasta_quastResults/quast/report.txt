All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.300percent.scf
#Contigs (>= 0 bp)             73                                                                       
#Contigs (>= 1000 bp)          73                                                                       
Total length (>= 0 bp)         2759495                                                                  
Total length (>= 1000 bp)      2759495                                                                  
#Contigs                       73                                                                       
Largest contig                 314717                                                                   
Total length                   2759495                                                                  
Reference length               2813862                                                                  
GC (%)                         32.64                                                                    
Reference GC (%)               32.81                                                                    
N50                            69110                                                                    
NG50                           68271                                                                    
N75                            38095                                                                    
NG75                           36017                                                                    
#misassemblies                 3                                                                        
#local misassemblies           4                                                                        
#unaligned contigs             0 + 0 part                                                               
Unaligned contigs length       0                                                                        
Genome fraction (%)            97.463                                                                   
Duplication ratio              1.006                                                                    
#N's per 100 kbp               0.00                                                                     
#mismatches per 100 kbp        4.63                                                                     
#indels per 100 kbp            18.45                                                                    
#genes                         2629 + 47 part                                                           
#predicted genes (unique)      2762                                                                     
#predicted genes (>= 0 bp)     2765                                                                     
#predicted genes (>= 300 bp)   2329                                                                     
#predicted genes (>= 1500 bp)  264                                                                      
#predicted genes (>= 3000 bp)  28                                                                       
Largest alignment              314717                                                                   
NA50                           68271                                                                    
NGA50                          66808                                                                    
NA75                           38095                                                                    
NGA75                          36017                                                                    
