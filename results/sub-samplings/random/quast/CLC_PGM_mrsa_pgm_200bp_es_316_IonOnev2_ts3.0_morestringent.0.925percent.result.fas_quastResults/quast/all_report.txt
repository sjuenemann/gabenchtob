All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.925percent.result
#Contigs                                   283                                                                           
#Contigs (>= 0 bp)                         283                                                                           
#Contigs (>= 1000 bp)                      131                                                                           
Largest contig                             105604                                                                        
Total length                               2777648                                                                       
Total length (>= 0 bp)                     2777648                                                                       
Total length (>= 1000 bp)                  2733711                                                                       
Reference length                           2813862                                                                       
N50                                        33653                                                                         
NG50                                       32644                                                                         
N75                                        19236                                                                         
NG75                                       18555                                                                         
L50                                        25                                                                            
LG50                                       26                                                                            
L75                                        52                                                                            
LG75                                       53                                                                            
#local misassemblies                       0                                                                             
#misassemblies                             4                                                                             
#misassembled contigs                      4                                                                             
Misassembled contigs length                23823                                                                         
Misassemblies inter-contig overlap         14                                                                            
#unaligned contigs                         3 + 5 part                                                                    
Unaligned contigs length                   905                                                                           
#ambiguously mapped contigs                12                                                                            
Extra bases in ambiguously mapped contigs  -8735                                                                         
#N's                                       197                                                                           
#N's per 100 kbp                           7.09                                                                          
Genome fraction (%)                        97.608                                                                        
Duplication ratio                          1.008                                                                         
#genes                                     2603 + 75 part                                                                
#predicted genes (unique)                  2990                                                                          
#predicted genes (>= 0 bp)                 2992                                                                          
#predicted genes (>= 300 bp)               2299                                                                          
#predicted genes (>= 1500 bp)              283                                                                           
#predicted genes (>= 3000 bp)              25                                                                            
#mismatches                                38                                                                            
#indels                                    293                                                                           
Indels length                              376                                                                           
#mismatches per 100 kbp                    1.38                                                                          
#indels per 100 kbp                        10.67                                                                         
GC (%)                                     32.65                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.228                                                                        
Largest alignment                          105604                                                                        
NA50                                       33653                                                                         
NGA50                                      32644                                                                         
NA75                                       18555                                                                         
NGA75                                      17983                                                                         
LA50                                       25                                                                            
LGA50                                      26                                                                            
LA75                                       52                                                                            
LGA75                                      53                                                                            
#Mis_misassemblies                         4                                                                             
#Mis_relocations                           2                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            2                                                                             
#Mis_misassembled contigs                  4                                                                             
Mis_Misassembled contigs length            23823                                                                         
#Mis_local misassemblies                   0                                                                             
#Mis_short indels (<= 5 bp)                292                                                                           
#Mis_long indels (> 5 bp)                  1                                                                             
#Una_fully unaligned contigs               3                                                                             
Una_Fully unaligned length                 690                                                                           
#Una_partially unaligned contigs           5                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             215                                                                           
GAGE_Contigs #                             283                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            105604                                                                        
GAGE_N50                                   32644 COUNT: 26                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2777648                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               22448(0.80%)                                                                  
GAGE_Missing assembly bases                944(0.03%)                                                                    
GAGE_Missing assembly contigs              3(1.06%)                                                                      
GAGE_Duplicated reference bases            18934                                                                         
GAGE_Compressed reference bases            41084                                                                         
GAGE_Bad trim                              254                                                                           
GAGE_Avg idy                               99.99                                                                         
GAGE_SNPs                                  37                                                                            
GAGE_Indels < 5bp                          290                                                                           
GAGE_Indels >= 5                           2                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            2                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    204                                                                           
GAGE_Corrected assembly size               2758014                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    105609                                                                        
GAGE_Corrected N50                         32646 COUNT: 26                                                               
