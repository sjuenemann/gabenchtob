All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.350percent.contigs
#Contigs                                   255                                                                                
#Contigs (>= 0 bp)                         276                                                                                
#Contigs (>= 1000 bp)                      192                                                                                
Largest contig                             182691                                                                             
Total length                               4273867                                                                            
Total length (>= 0 bp)                     4277011                                                                            
Total length (>= 1000 bp)                  4241397                                                                            
Reference length                           4411532                                                                            
N50                                        42255                                                                              
NG50                                       41850                                                                              
N75                                        21062                                                                              
NG75                                       18730                                                                              
L50                                        30                                                                                 
LG50                                       32                                                                                 
L75                                        64                                                                                 
LG75                                       69                                                                                 
#local misassemblies                       18                                                                                 
#misassemblies                             2                                                                                  
#misassembled contigs                      2                                                                                  
Misassembled contigs length                2173                                                                               
Misassemblies inter-contig overlap         32208                                                                              
#unaligned contigs                         2 + 1 part                                                                         
Unaligned contigs length                   1812                                                                               
#ambiguously mapped contigs                25                                                                                 
Extra bases in ambiguously mapped contigs  -18804                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        96.448                                                                             
Duplication ratio                          1.007                                                                              
#genes                                     3885 + 160 part                                                                    
#predicted genes (unique)                  4474                                                                               
#predicted genes (>= 0 bp)                 4474                                                                               
#predicted genes (>= 300 bp)               3744                                                                               
#predicted genes (>= 1500 bp)              475                                                                                
#predicted genes (>= 3000 bp)              55                                                                                 
#mismatches                                151                                                                                
#indels                                    1102                                                                               
Indels length                              1268                                                                               
#mismatches per 100 kbp                    3.55                                                                               
#indels per 100 kbp                        25.90                                                                              
GC (%)                                     65.34                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.635                                                                             
Largest alignment                          182691                                                                             
NA50                                       42255                                                                              
NGA50                                      41850                                                                              
NA75                                       21062                                                                              
NGA75                                      18730                                                                              
LA50                                       30                                                                                 
LGA50                                      32                                                                                 
LA75                                       64                                                                                 
LGA75                                      69                                                                                 
#Mis_misassemblies                         2                                                                                  
#Mis_relocations                           2                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  2                                                                                  
Mis_Misassembled contigs length            2173                                                                               
#Mis_local misassemblies                   18                                                                                 
#Mis_short indels (<= 5 bp)                1099                                                                               
#Mis_long indels (> 5 bp)                  3                                                                                  
#Una_fully unaligned contigs               2                                                                                  
Una_Fully unaligned length                 1061                                                                               
#Una_partially unaligned contigs           1                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            1                                                                                  
Una_Partially unaligned length             751                                                                                
GAGE_Contigs #                             255                                                                                
GAGE_Min contig                            203                                                                                
GAGE_Max contig                            182691                                                                             
GAGE_N50                                   41850 COUNT: 32                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4273867                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               88340(2.00%)                                                                       
GAGE_Missing assembly bases                1108(0.03%)                                                                        
GAGE_Missing assembly contigs              2(0.78%)                                                                           
GAGE_Duplicated reference bases            311                                                                                
GAGE_Compressed reference bases            49991                                                                              
GAGE_Bad trim                              47                                                                                 
GAGE_Avg idy                               99.97                                                                              
GAGE_SNPs                                  102                                                                                
GAGE_Indels < 5bp                          1127                                                                               
GAGE_Indels >= 5                           17                                                                                 
GAGE_Inversions                            1                                                                                  
GAGE_Relocation                            7                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    273                                                                                
GAGE_Corrected assembly size               4275639                                                                            
GAGE_Min correct contig                    201                                                                                
GAGE_Max correct contig                    182725                                                                             
GAGE_Corrected N50                         33930 COUNT: 37                                                                    
