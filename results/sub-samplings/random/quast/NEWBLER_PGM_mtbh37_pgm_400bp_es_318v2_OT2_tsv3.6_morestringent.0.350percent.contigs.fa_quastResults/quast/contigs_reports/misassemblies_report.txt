All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.350percent.contigs
#Mis_misassemblies               2                                                                                  
#Mis_relocations                 2                                                                                  
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        2                                                                                  
Mis_Misassembled contigs length  2173                                                                               
#Mis_local misassemblies         18                                                                                 
#mismatches                      151                                                                                
#indels                          1102                                                                               
#Mis_short indels (<= 5 bp)      1099                                                                               
#Mis_long indels (> 5 bp)        3                                                                                  
Indels length                    1268                                                                               
