All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.450percent_out.unpadded
#Contigs                                   218                                                                                                             
#Contigs (>= 0 bp)                         230                                                                                                             
#Contigs (>= 1000 bp)                      134                                                                                                             
Largest contig                             215363                                                                                                          
Total length                               4417908                                                                                                         
Total length (>= 0 bp)                     4419388                                                                                                         
Total length (>= 1000 bp)                  4372921                                                                                                         
Reference length                           4411532                                                                                                         
N50                                        65937                                                                                                           
NG50                                       65937                                                                                                           
N75                                        34144                                                                                                           
NG75                                       34144                                                                                                           
L50                                        19                                                                                                              
LG50                                       19                                                                                                              
L75                                        42                                                                                                              
LG75                                       42                                                                                                              
#local misassemblies                       20                                                                                                              
#misassemblies                             30                                                                                                              
#misassembled contigs                      22                                                                                                              
Misassembled contigs length                1263443                                                                                                         
Misassemblies inter-contig overlap         40390                                                                                                           
#unaligned contigs                         0 + 7 part                                                                                                      
Unaligned contigs length                   1106                                                                                                            
#ambiguously mapped contigs                6                                                                                                               
Extra bases in ambiguously mapped contigs  -3339                                                                                                           
#N's                                       122                                                                                                             
#N's per 100 kbp                           2.76                                                                                                            
Genome fraction (%)                        99.391                                                                                                          
Duplication ratio                          1.016                                                                                                           
#genes                                     3993 + 117 part                                                                                                 
#predicted genes (unique)                  4177                                                                                                            
#predicted genes (>= 0 bp)                 4198                                                                                                            
#predicted genes (>= 300 bp)               3646                                                                                                            
#predicted genes (>= 1500 bp)              566                                                                                                             
#predicted genes (>= 3000 bp)              79                                                                                                              
#mismatches                                476                                                                                                             
#indels                                    56                                                                                                              
Indels length                              190                                                                                                             
#mismatches per 100 kbp                    10.86                                                                                                           
#indels per 100 kbp                        1.28                                                                                                            
GC (%)                                     65.57                                                                                                           
Reference GC (%)                           65.61                                                                                                           
Average %IDY                               99.710                                                                                                          
Largest alignment                          190504                                                                                                          
NA50                                       56999                                                                                                           
NGA50                                      56999                                                                                                           
NA75                                       30244                                                                                                           
NGA75                                      30748                                                                                                           
LA50                                       24                                                                                                              
LGA50                                      24                                                                                                              
LA75                                       51                                                                                                              
LGA75                                      50                                                                                                              
#Mis_misassemblies                         30                                                                                                              
#Mis_relocations                           30                                                                                                              
#Mis_translocations                        0                                                                                                               
#Mis_inversions                            0                                                                                                               
#Mis_misassembled contigs                  22                                                                                                              
Mis_Misassembled contigs length            1263443                                                                                                         
#Mis_local misassemblies                   20                                                                                                              
#Mis_short indels (<= 5 bp)                50                                                                                                              
#Mis_long indels (> 5 bp)                  6                                                                                                               
#Una_fully unaligned contigs               0                                                                                                               
Una_Fully unaligned length                 0                                                                                                               
#Una_partially unaligned contigs           7                                                                                                               
#Una_with misassembly                      0                                                                                                               
#Una_both parts are significant            1                                                                                                               
Una_Partially unaligned length             1106                                                                                                            
GAGE_Contigs #                             218                                                                                                             
GAGE_Min contig                            224                                                                                                             
GAGE_Max contig                            215363                                                                                                          
GAGE_N50                                   65937 COUNT: 19                                                                                                 
GAGE_Genome size                           4411532                                                                                                         
GAGE_Assembly size                         4417908                                                                                                         
GAGE_Chaff bases                           0                                                                                                               
GAGE_Missing reference bases               19277(0.44%)                                                                                                    
GAGE_Missing assembly bases                1761(0.04%)                                                                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                                                                        
GAGE_Duplicated reference bases            27539                                                                                                           
GAGE_Compressed reference bases            17526                                                                                                           
GAGE_Bad trim                              1761                                                                                                            
GAGE_Avg idy                               99.98                                                                                                           
GAGE_SNPs                                  237                                                                                                             
GAGE_Indels < 5bp                          44                                                                                                              
GAGE_Indels >= 5                           14                                                                                                              
GAGE_Inversions                            6                                                                                                               
GAGE_Relocation                            9                                                                                                               
GAGE_Translocation                         0                                                                                                               
GAGE_Corrected contig #                    211                                                                                                             
GAGE_Corrected assembly size               4413153                                                                                                         
GAGE_Min correct contig                    224                                                                                                             
GAGE_Max correct contig                    163099                                                                                                          
GAGE_Corrected N50                         56167 COUNT: 26                                                                                                 
