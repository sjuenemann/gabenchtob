All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.125percent_out.unpadded
#Contigs                                   1544                                                                                                
#Contigs (>= 0 bp)                         1558                                                                                                
#Contigs (>= 1000 bp)                      1066                                                                                                
Largest contig                             29670                                                                                               
Total length                               5437868                                                                                             
Total length (>= 0 bp)                     5440252                                                                                             
Total length (>= 1000 bp)                  5189149                                                                                             
Reference length                           5594470                                                                                             
N50                                        6555                                                                                                
NG50                                       6302                                                                                                
N75                                        3517                                                                                                
NG75                                       3261                                                                                                
L50                                        252                                                                                                 
LG50                                       265                                                                                                 
L75                                        537                                                                                                 
LG75                                       572                                                                                                 
#local misassemblies                       11                                                                                                  
#misassemblies                             61                                                                                                  
#misassembled contigs                      40                                                                                                  
Misassembled contigs length                394971                                                                                              
Misassemblies inter-contig overlap         64990                                                                                               
#unaligned contigs                         2 + 1 part                                                                                          
Unaligned contigs length                   5460                                                                                                
#ambiguously mapped contigs                80                                                                                                  
Extra bases in ambiguously mapped contigs  -53513                                                                                              
#N's                                       108                                                                                                 
#N's per 100 kbp                           1.99                                                                                                
Genome fraction (%)                        96.198                                                                                              
Duplication ratio                          1.012                                                                                               
#genes                                     4260 + 1065 part                                                                                    
#predicted genes (unique)                  6284                                                                                                
#predicted genes (>= 0 bp)                 6301                                                                                                
#predicted genes (>= 300 bp)               4904                                                                                                
#predicted genes (>= 1500 bp)              540                                                                                                 
#predicted genes (>= 3000 bp)              44                                                                                                  
#mismatches                                2645                                                                                                
#indels                                    162                                                                                                 
Indels length                              262                                                                                                 
#mismatches per 100 kbp                    49.15                                                                                               
#indels per 100 kbp                        3.01                                                                                                
GC (%)                                     50.44                                                                                               
Reference GC (%)                           50.48                                                                                               
Average %IDY                               99.031                                                                                              
Largest alignment                          29670                                                                                               
NA50                                       6297                                                                                                
NGA50                                      6111                                                                                                
NA75                                       3345                                                                                                
NGA75                                      3134                                                                                                
LA50                                       266                                                                                                 
LGA50                                      278                                                                                                 
LA75                                       560                                                                                                 
LGA75                                      596                                                                                                 
#Mis_misassemblies                         61                                                                                                  
#Mis_relocations                           58                                                                                                  
#Mis_translocations                        3                                                                                                   
#Mis_inversions                            0                                                                                                   
#Mis_misassembled contigs                  40                                                                                                  
Mis_Misassembled contigs length            394971                                                                                              
#Mis_local misassemblies                   11                                                                                                  
#Mis_short indels (<= 5 bp)                155                                                                                                 
#Mis_long indels (> 5 bp)                  7                                                                                                   
#Una_fully unaligned contigs               2                                                                                                   
Una_Fully unaligned length                 5432                                                                                                
#Una_partially unaligned contigs           1                                                                                                   
#Una_with misassembly                      0                                                                                                   
#Una_both parts are significant            0                                                                                                   
Una_Partially unaligned length             28                                                                                                  
GAGE_Contigs #                             1544                                                                                                
GAGE_Min contig                            202                                                                                                 
GAGE_Max contig                            29670                                                                                               
GAGE_N50                                   6302 COUNT: 265                                                                                     
GAGE_Genome size                           5594470                                                                                             
GAGE_Assembly size                         5437868                                                                                             
GAGE_Chaff bases                           0                                                                                                   
GAGE_Missing reference bases               71236(1.27%)                                                                                        
GAGE_Missing assembly bases                5701(0.10%)                                                                                         
GAGE_Missing assembly contigs              2(0.13%)                                                                                            
GAGE_Duplicated reference bases            90058                                                                                               
GAGE_Compressed reference bases            201815                                                                                              
GAGE_Bad trim                              168                                                                                                 
GAGE_Avg idy                               99.94                                                                                               
GAGE_SNPs                                  712                                                                                                 
GAGE_Indels < 5bp                          28                                                                                                  
GAGE_Indels >= 5                           8                                                                                                   
GAGE_Inversions                            28                                                                                                  
GAGE_Relocation                            16                                                                                                  
GAGE_Translocation                         3                                                                                                   
GAGE_Corrected contig #                    1461                                                                                                
GAGE_Corrected assembly size               5416031                                                                                             
GAGE_Min correct contig                    203                                                                                                 
GAGE_Max correct contig                    29303                                                                                               
GAGE_Corrected N50                         6099 COUNT: 280                                                                                     
