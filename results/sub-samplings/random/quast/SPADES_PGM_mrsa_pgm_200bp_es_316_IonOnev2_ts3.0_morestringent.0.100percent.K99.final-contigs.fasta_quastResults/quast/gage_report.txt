All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.100percent.K99.final-contigs
GAGE_Contigs #                   89                                                                                          
GAGE_Min contig                  451                                                                                         
GAGE_Max contig                  173655                                                                                      
GAGE_N50                         68521 COUNT: 12                                                                             
GAGE_Genome size                 2813862                                                                                     
GAGE_Assembly size               2772608                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     7388(0.26%)                                                                                 
GAGE_Missing assembly bases      869(0.03%)                                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  2574                                                                                        
GAGE_Compressed reference bases  41667                                                                                       
GAGE_Bad trim                    869                                                                                         
GAGE_Avg idy                     99.94                                                                                       
GAGE_SNPs                        118                                                                                         
GAGE_Indels < 5bp                1083                                                                                        
GAGE_Indels >= 5                 13                                                                                          
GAGE_Inversions                  3                                                                                           
GAGE_Relocation                  8                                                                                           
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          115                                                                                         
GAGE_Corrected assembly size     2775948                                                                                     
GAGE_Min correct contig          264                                                                                         
GAGE_Max correct contig          146317                                                                                      
GAGE_Corrected N50               51940 COUNT: 17                                                                             
