All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.550percent_out.unpadded
GAGE_Contigs #                   3959                                                                            
GAGE_Min contig                  200                                                                             
GAGE_Max contig                  321908                                                                          
GAGE_N50                         113458 COUNT: 8                                                                 
GAGE_Genome size                 2813862                                                                         
GAGE_Assembly size               5443204                                                                         
GAGE_Chaff bases                 0                                                                               
GAGE_Missing reference bases     231(0.01%)                                                                      
GAGE_Missing assembly bases      14673(0.27%)                                                                    
GAGE_Missing assembly contigs    7(0.18%)                                                                        
GAGE_Duplicated reference bases  2592531                                                                         
GAGE_Compressed reference bases  7286                                                                            
GAGE_Bad trim                    11497                                                                           
GAGE_Avg idy                     99.98                                                                           
GAGE_SNPs                        48                                                                              
GAGE_Indels < 5bp                79                                                                              
GAGE_Indels >= 5                 3                                                                               
GAGE_Inversions                  6                                                                               
GAGE_Relocation                  1                                                                               
GAGE_Translocation               0                                                                               
GAGE_Corrected contig #          92                                                                              
GAGE_Corrected assembly size     2860322                                                                         
GAGE_Min correct contig          238                                                                             
GAGE_Max correct contig          321912                                                                          
GAGE_Corrected N50               91032 COUNT: 9                                                                  
