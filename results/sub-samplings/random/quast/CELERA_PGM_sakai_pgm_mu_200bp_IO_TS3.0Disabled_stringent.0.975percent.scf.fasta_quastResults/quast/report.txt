All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.975percent.scf
#Contigs (>= 0 bp)             848                                                                      
#Contigs (>= 1000 bp)          848                                                                      
Total length (>= 0 bp)         5209862                                                                  
Total length (>= 1000 bp)      5209862                                                                  
#Contigs                       848                                                                      
Largest contig                 43393                                                                    
Total length                   5209862                                                                  
Reference length               5594470                                                                  
GC (%)                         50.24                                                                    
Reference GC (%)               50.48                                                                    
N50                            11600                                                                    
NG50                           10564                                                                    
N75                            5932                                                                     
NG75                           4453                                                                     
#misassemblies                 9                                                                        
#local misassemblies           4                                                                        
#unaligned contigs             0 + 13 part                                                              
Unaligned contigs length       441                                                                      
Genome fraction (%)            91.355                                                                   
Duplication ratio              1.019                                                                    
#N's per 100 kbp               0.00                                                                     
#mismatches per 100 kbp        3.54                                                                     
#indels per 100 kbp            15.91                                                                    
#genes                         4312 + 704 part                                                          
#predicted genes (unique)      5821                                                                     
#predicted genes (>= 0 bp)     5822                                                                     
#predicted genes (>= 300 bp)   4681                                                                     
#predicted genes (>= 1500 bp)  519                                                                      
#predicted genes (>= 3000 bp)  41                                                                       
Largest alignment              43393                                                                    
NA50                           11586                                                                    
NGA50                          10543                                                                    
NA75                           5931                                                                     
NGA75                          4385                                                                     
