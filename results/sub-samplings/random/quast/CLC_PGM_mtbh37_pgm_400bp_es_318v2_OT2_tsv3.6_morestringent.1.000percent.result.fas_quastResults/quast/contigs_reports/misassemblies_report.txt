All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.1.000percent.result
#Mis_misassemblies               19                                                                            
#Mis_relocations                 19                                                                            
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        17                                                                            
Mis_Misassembled contigs length  82845                                                                         
#Mis_local misassemblies         9                                                                             
#mismatches                      192                                                                           
#indels                          1165                                                                          
#Mis_short indels (<= 5 bp)      1162                                                                          
#Mis_long indels (> 5 bp)        3                                                                             
Indels length                    1380                                                                          
