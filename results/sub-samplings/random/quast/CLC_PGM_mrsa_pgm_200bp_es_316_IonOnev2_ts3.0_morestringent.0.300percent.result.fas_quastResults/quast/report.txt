All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.300percent.result
#Contigs (>= 0 bp)             308                                                                           
#Contigs (>= 1000 bp)          154                                                                           
Total length (>= 0 bp)         2779109                                                                       
Total length (>= 1000 bp)      2733432                                                                       
#Contigs                       308                                                                           
Largest contig                 132318                                                                        
Total length                   2779109                                                                       
Reference length               2813862                                                                       
GC (%)                         32.62                                                                         
Reference GC (%)               32.81                                                                         
N50                            29524                                                                         
NG50                           29277                                                                         
N75                            15659                                                                         
NG75                           14179                                                                         
#misassemblies                 16                                                                            
#local misassemblies           0                                                                             
#unaligned contigs             0 + 15 part                                                                   
Unaligned contigs length       470                                                                           
Genome fraction (%)            97.567                                                                        
Duplication ratio              1.010                                                                         
#N's per 100 kbp               0.50                                                                          
#mismatches per 100 kbp        2.33                                                                          
#indels per 100 kbp            16.79                                                                         
#genes                         2586 + 94 part                                                                
#predicted genes (unique)      2889                                                                          
#predicted genes (>= 0 bp)     2890                                                                          
#predicted genes (>= 300 bp)   2336                                                                          
#predicted genes (>= 1500 bp)  276                                                                           
#predicted genes (>= 3000 bp)  24                                                                            
Largest alignment              132309                                                                        
NA50                           29524                                                                         
NGA50                          29277                                                                         
NA75                           15659                                                                         
NGA75                          14179                                                                         
