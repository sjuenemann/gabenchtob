All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent.K99.final-contigs
#Mis_misassemblies               268                                                                                    
#Mis_relocations                 93                                                                                     
#Mis_translocations              2                                                                                      
#Mis_inversions                  173                                                                                    
#Mis_misassembled contigs        259                                                                                    
Mis_Misassembled contigs length  439807                                                                                 
#Mis_local misassemblies         27                                                                                     
#mismatches                      3492                                                                                   
#indels                          14461                                                                                  
#Mis_short indels (<= 5 bp)      14445                                                                                  
#Mis_long indels (> 5 bp)        16                                                                                     
Indels length                    15201                                                                                  
