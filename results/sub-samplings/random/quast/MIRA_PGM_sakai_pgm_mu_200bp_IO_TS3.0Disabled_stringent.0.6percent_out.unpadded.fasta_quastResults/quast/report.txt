All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.6percent_out.unpadded
#Contigs (>= 0 bp)             1154                                                                          
#Contigs (>= 1000 bp)          180                                                                           
Total length (>= 0 bp)         5843132                                                                       
Total length (>= 1000 bp)      5493137                                                                       
#Contigs                       1127                                                                          
Largest contig                 410737                                                                        
Total length                   5838746                                                                       
Reference length               5594470                                                                       
GC (%)                         50.44                                                                         
Reference GC (%)               50.48                                                                         
N50                            86462                                                                         
NG50                           90851                                                                         
N75                            41085                                                                         
NG75                           45716                                                                         
#misassemblies                 186                                                                           
#local misassemblies           24                                                                            
#unaligned contigs             16 + 509 part                                                                 
Unaligned contigs length       32886                                                                         
Genome fraction (%)            97.974                                                                        
Duplication ratio              1.055                                                                         
#N's per 100 kbp               18.84                                                                         
#mismatches per 100 kbp        27.96                                                                         
#indels per 100 kbp            6.69                                                                          
#genes                         5211 + 171 part                                                               
#predicted genes (unique)      6416                                                                          
#predicted genes (>= 0 bp)     6447                                                                          
#predicted genes (>= 300 bp)   4825                                                                          
#predicted genes (>= 1500 bp)  668                                                                           
#predicted genes (>= 3000 bp)  63                                                                            
Largest alignment              227296                                                                        
NA50                           78329                                                                         
NGA50                          82196                                                                         
NA75                           35571                                                                         
NGA75                          41079                                                                         
