All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.400percent.result
GAGE_Contigs #                   333                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  107776                                                                        
GAGE_N50                         31360 COUNT: 26                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2754689                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     44302(1.57%)                                                                  
GAGE_Missing assembly bases      1164(0.04%)                                                                   
GAGE_Missing assembly contigs    3(0.90%)                                                                      
GAGE_Duplicated reference bases  16287                                                                         
GAGE_Compressed reference bases  38195                                                                         
GAGE_Bad trim                    391                                                                           
GAGE_Avg idy                     99.98                                                                         
GAGE_SNPs                        49                                                                            
GAGE_Indels < 5bp                377                                                                           
GAGE_Indels >= 5                 3                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  2                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          266                                                                           
GAGE_Corrected assembly size     2738207                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          107783                                                                        
GAGE_Corrected N50               30761 COUNT: 27                                                               
