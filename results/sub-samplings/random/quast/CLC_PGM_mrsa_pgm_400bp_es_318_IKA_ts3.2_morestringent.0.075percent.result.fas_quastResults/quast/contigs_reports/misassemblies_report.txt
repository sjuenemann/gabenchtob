All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.075percent.result
#Mis_misassemblies               7                                                                        
#Mis_relocations                 7                                                                        
#Mis_translocations              0                                                                        
#Mis_inversions                  0                                                                        
#Mis_misassembled contigs        7                                                                        
Mis_Misassembled contigs length  52555                                                                    
#Mis_local misassemblies         5                                                                        
#mismatches                      92                                                                       
#indels                          561                                                                      
#Mis_short indels (<= 5 bp)      561                                                                      
#Mis_long indels (> 5 bp)        0                                                                        
Indels length                    584                                                                      
