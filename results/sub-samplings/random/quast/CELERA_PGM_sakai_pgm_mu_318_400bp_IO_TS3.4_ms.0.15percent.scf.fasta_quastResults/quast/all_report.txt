All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.15percent.scf
#Contigs                                   189                                                          
#Contigs (>= 0 bp)                         189                                                          
#Contigs (>= 1000 bp)                      189                                                          
Largest contig                             282595                                                       
Total length                               5214982                                                      
Total length (>= 0 bp)                     5214982                                                      
Total length (>= 1000 bp)                  5214982                                                      
Reference length                           5594470                                                      
N50                                        73404                                                        
NG50                                       67335                                                        
N75                                        41922                                                        
NG75                                       36123                                                        
L50                                        23                                                           
LG50                                       25                                                           
L75                                        47                                                           
LG75                                       54                                                           
#local misassemblies                       10                                                           
#misassemblies                             2                                                            
#misassembled contigs                      2                                                            
Misassembled contigs length                241217                                                       
Misassemblies inter-contig overlap         2872                                                         
#unaligned contigs                         0 + 0 part                                                   
Unaligned contigs length                   0                                                            
#ambiguously mapped contigs                4                                                            
Extra bases in ambiguously mapped contigs  -4869                                                        
#N's                                       0                                                            
#N's per 100 kbp                           0.00                                                         
Genome fraction (%)                        92.875                                                       
Duplication ratio                          1.003                                                        
#genes                                     4907 + 180 part                                              
#predicted genes (unique)                  5223                                                         
#predicted genes (>= 0 bp)                 5224                                                         
#predicted genes (>= 300 bp)               4434                                                         
#predicted genes (>= 1500 bp)              619                                                          
#predicted genes (>= 3000 bp)              61                                                           
#mismatches                                159                                                          
#indels                                    438                                                          
Indels length                              445                                                          
#mismatches per 100 kbp                    3.06                                                         
#indels per 100 kbp                        8.43                                                         
GC (%)                                     50.29                                                        
Reference GC (%)                           50.48                                                        
Average %IDY                               98.449                                                       
Largest alignment                          282595                                                       
NA50                                       73404                                                        
NGA50                                      67334                                                        
NA75                                       41922                                                        
NGA75                                      36123                                                        
LA50                                       24                                                           
LGA50                                      26                                                           
LA75                                       48                                                           
LGA75                                      55                                                           
#Mis_misassemblies                         2                                                            
#Mis_relocations                           2                                                            
#Mis_translocations                        0                                                            
#Mis_inversions                            0                                                            
#Mis_misassembled contigs                  2                                                            
Mis_Misassembled contigs length            241217                                                       
#Mis_local misassemblies                   10                                                           
#Mis_short indels (<= 5 bp)                438                                                          
#Mis_long indels (> 5 bp)                  0                                                            
#Una_fully unaligned contigs               0                                                            
Una_Fully unaligned length                 0                                                            
#Una_partially unaligned contigs           0                                                            
#Una_with misassembly                      0                                                            
#Una_both parts are significant            0                                                            
Una_Partially unaligned length             0                                                            
GAGE_Contigs #                             189                                                          
GAGE_Min contig                            1001                                                         
GAGE_Max contig                            282595                                                       
GAGE_N50                                   67335 COUNT: 25                                              
GAGE_Genome size                           5594470                                                      
GAGE_Assembly size                         5214982                                                      
GAGE_Chaff bases                           0                                                            
GAGE_Missing reference bases               315750(5.64%)                                                
GAGE_Missing assembly bases                94(0.00%)                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                     
GAGE_Duplicated reference bases            1727                                                         
GAGE_Compressed reference bases            120979                                                       
GAGE_Bad trim                              94                                                           
GAGE_Avg idy                               99.99                                                        
GAGE_SNPs                                  61                                                           
GAGE_Indels < 5bp                          430                                                          
GAGE_Indels >= 5                           9                                                            
GAGE_Inversions                            1                                                            
GAGE_Relocation                            4                                                            
GAGE_Translocation                         0                                                            
GAGE_Corrected contig #                    202                                                          
GAGE_Corrected assembly size               5216618                                                      
GAGE_Min correct contig                    207                                                          
GAGE_Max correct contig                    196968                                                       
GAGE_Corrected N50                         66653 COUNT: 28                                              
