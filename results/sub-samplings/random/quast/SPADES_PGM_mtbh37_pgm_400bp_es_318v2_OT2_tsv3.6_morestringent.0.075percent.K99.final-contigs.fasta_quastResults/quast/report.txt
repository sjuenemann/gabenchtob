All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.075percent.K99.final-contigs
#Contigs (>= 0 bp)             1269                                                                                        
#Contigs (>= 1000 bp)          1023                                                                                        
Total length (>= 0 bp)         4070202                                                                                     
Total length (>= 1000 bp)      3900463                                                                                     
#Contigs                       1257                                                                                        
Largest contig                 28675                                                                                       
Total length                   4068910                                                                                     
Reference length               4411532                                                                                     
GC (%)                         65.12                                                                                       
Reference GC (%)               65.61                                                                                       
N50                            5038                                                                                        
NG50                           4594                                                                                        
N75                            2671                                                                                        
NG75                           2210                                                                                        
#misassemblies                 56                                                                                          
#local misassemblies           19                                                                                          
#unaligned contigs             0 + 13 part                                                                                 
Unaligned contigs length       1883                                                                                        
Genome fraction (%)            91.829                                                                                      
Duplication ratio              1.004                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        33.67                                                                                       
#indels per 100 kbp            178.92                                                                                      
#genes                         2872 + 1133 part                                                                            
#predicted genes (unique)      5928                                                                                        
#predicted genes (>= 0 bp)     5937                                                                                        
#predicted genes (>= 300 bp)   4073                                                                                        
#predicted genes (>= 1500 bp)  158                                                                                         
#predicted genes (>= 3000 bp)  4                                                                                           
Largest alignment              28675                                                                                       
NA50                           4916                                                                                        
NGA50                          4465                                                                                        
NA75                           2558                                                                                        
NGA75                          2110                                                                                        
