All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.990percent_out.unpadded
#Contigs (>= 0 bp)             800                                                                                  
#Contigs (>= 1000 bp)          132                                                                                  
Total length (>= 0 bp)         4647496                                                                              
Total length (>= 1000 bp)      4356831                                                                              
#Contigs                       736                                                                                  
Largest contig                 280406                                                                               
Total length                   4638440                                                                              
Reference length               4411532                                                                              
GC (%)                         65.18                                                                                
Reference GC (%)               65.61                                                                                
N50                            58726                                                                                
NG50                           64937                                                                                
N75                            33982                                                                                
NG75                           37248                                                                                
#misassemblies                 33                                                                                   
#local misassemblies           17                                                                                   
#unaligned contigs             3 + 10 part                                                                          
Unaligned contigs length       1820                                                                                 
Genome fraction (%)            98.531                                                                               
Duplication ratio              1.074                                                                                
#N's per 100 kbp               1.51                                                                                 
#mismatches per 100 kbp        10.93                                                                                
#indels per 100 kbp            11.50                                                                                
#genes                         4003 + 107 part                                                                      
#predicted genes (unique)      4932                                                                                 
#predicted genes (>= 0 bp)     4971                                                                                 
#predicted genes (>= 300 bp)   3856                                                                                 
#predicted genes (>= 1500 bp)  538                                                                                  
#predicted genes (>= 3000 bp)  74                                                                                   
Largest alignment              191653                                                                               
NA50                           53679                                                                                
NGA50                          56045                                                                                
NA75                           31751                                                                                
NGA75                          33914                                                                                
