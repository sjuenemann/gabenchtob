All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.500percent.K99.final-contigs
GAGE_Contigs #                   182                                                                                         
GAGE_Min contig                  220                                                                                         
GAGE_Max contig                  156252                                                                                      
GAGE_N50                         54245 COUNT: 25                                                                             
GAGE_Genome size                 4411532                                                                                     
GAGE_Assembly size               4284411                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     84562(1.92%)                                                                                
GAGE_Missing assembly bases      173(0.00%)                                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  366                                                                                         
GAGE_Compressed reference bases  54966                                                                                       
GAGE_Bad trim                    173                                                                                         
GAGE_Avg idy                     99.94                                                                                       
GAGE_SNPs                        238                                                                                         
GAGE_Indels < 5bp                1810                                                                                        
GAGE_Indels >= 5                 21                                                                                          
GAGE_Inversions                  1                                                                                           
GAGE_Relocation                  13                                                                                          
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          220                                                                                         
GAGE_Corrected assembly size     4290025                                                                                     
GAGE_Min correct contig          220                                                                                         
GAGE_Max correct contig          132420                                                                                      
GAGE_Corrected N50               43101 COUNT: 31                                                                             
