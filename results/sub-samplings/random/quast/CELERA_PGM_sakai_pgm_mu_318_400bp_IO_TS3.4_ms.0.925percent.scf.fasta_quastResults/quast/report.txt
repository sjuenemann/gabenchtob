All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent.scf
#Contigs (>= 0 bp)             335                                                           
#Contigs (>= 1000 bp)          335                                                           
Total length (>= 0 bp)         5290032                                                       
Total length (>= 1000 bp)      5290032                                                       
#Contigs                       335                                                           
Largest contig                 215964                                                        
Total length                   5290032                                                       
Reference length               5594470                                                       
GC (%)                         50.33                                                         
Reference GC (%)               50.48                                                         
N50                            38945                                                         
NG50                           36202                                                         
N75                            18180                                                         
NG75                           15796                                                         
#misassemblies                 3                                                             
#local misassemblies           6                                                             
#unaligned contigs             0 + 0 part                                                    
Unaligned contigs length       0                                                             
Genome fraction (%)            92.860                                                        
Duplication ratio              1.012                                                         
#N's per 100 kbp               0.00                                                          
#mismatches per 100 kbp        2.08                                                          
#indels per 100 kbp            6.91                                                          
#genes                         4834 + 238 part                                               
#predicted genes (unique)      5376                                                          
#predicted genes (>= 0 bp)     5380                                                          
#predicted genes (>= 300 bp)   4523                                                          
#predicted genes (>= 1500 bp)  631                                                           
#predicted genes (>= 3000 bp)  57                                                            
Largest alignment              215961                                                        
NA50                           38764                                                         
NGA50                          35803                                                         
NA75                           18180                                                         
NGA75                          15337                                                         
