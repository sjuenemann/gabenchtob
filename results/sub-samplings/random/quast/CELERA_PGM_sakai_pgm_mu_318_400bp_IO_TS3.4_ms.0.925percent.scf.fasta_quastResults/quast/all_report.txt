All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent.scf
#Contigs                                   335                                                           
#Contigs (>= 0 bp)                         335                                                           
#Contigs (>= 1000 bp)                      335                                                           
Largest contig                             215964                                                        
Total length                               5290032                                                       
Total length (>= 0 bp)                     5290032                                                       
Total length (>= 1000 bp)                  5290032                                                       
Reference length                           5594470                                                       
N50                                        38945                                                         
NG50                                       36202                                                         
N75                                        18180                                                         
NG75                                       15796                                                         
L50                                        40                                                            
LG50                                       44                                                            
L75                                        90                                                            
LG75                                       103                                                           
#local misassemblies                       6                                                             
#misassemblies                             3                                                             
#misassembled contigs                      3                                                             
Misassembled contigs length                55611                                                         
Misassemblies inter-contig overlap         1283                                                          
#unaligned contigs                         0 + 0 part                                                    
Unaligned contigs length                   0                                                             
#ambiguously mapped contigs                21                                                            
Extra bases in ambiguously mapped contigs  -31404                                                        
#N's                                       0                                                             
#N's per 100 kbp                           0.00                                                          
Genome fraction (%)                        92.860                                                        
Duplication ratio                          1.012                                                         
#genes                                     4834 + 238 part                                               
#predicted genes (unique)                  5376                                                          
#predicted genes (>= 0 bp)                 5380                                                          
#predicted genes (>= 300 bp)               4523                                                          
#predicted genes (>= 1500 bp)              631                                                           
#predicted genes (>= 3000 bp)              57                                                            
#mismatches                                108                                                           
#indels                                    359                                                           
Indels length                              364                                                           
#mismatches per 100 kbp                    2.08                                                          
#indels per 100 kbp                        6.91                                                          
GC (%)                                     50.33                                                         
Reference GC (%)                           50.48                                                         
Average %IDY                               98.700                                                        
Largest alignment                          215961                                                        
NA50                                       38764                                                         
NGA50                                      35803                                                         
NA75                                       18180                                                         
NGA75                                      15337                                                         
LA50                                       40                                                            
LGA50                                      44                                                            
LA75                                       90                                                            
LGA75                                      104                                                           
#Mis_misassemblies                         3                                                             
#Mis_relocations                           3                                                             
#Mis_translocations                        0                                                             
#Mis_inversions                            0                                                             
#Mis_misassembled contigs                  3                                                             
Mis_Misassembled contigs length            55611                                                         
#Mis_local misassemblies                   6                                                             
#Mis_short indels (<= 5 bp)                359                                                           
#Mis_long indels (> 5 bp)                  0                                                             
#Una_fully unaligned contigs               0                                                             
Una_Fully unaligned length                 0                                                             
#Una_partially unaligned contigs           0                                                             
#Una_with misassembly                      0                                                             
#Una_both parts are significant            0                                                             
Una_Partially unaligned length             0                                                             
GAGE_Contigs #                             335                                                           
GAGE_Min contig                            1000                                                          
GAGE_Max contig                            215964                                                        
GAGE_N50                                   36202 COUNT: 44                                               
GAGE_Genome size                           5594470                                                       
GAGE_Assembly size                         5290032                                                       
GAGE_Chaff bases                           0                                                             
GAGE_Missing reference bases               264087(4.72%)                                                 
GAGE_Missing assembly bases                59(0.00%)                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                      
GAGE_Duplicated reference bases            224                                                           
GAGE_Compressed reference bases            164380                                                        
GAGE_Bad trim                              59                                                            
GAGE_Avg idy                               99.99                                                         
GAGE_SNPs                                  62                                                            
GAGE_Indels < 5bp                          342                                                           
GAGE_Indels >= 5                           5                                                             
GAGE_Inversions                            1                                                             
GAGE_Relocation                            4                                                             
GAGE_Translocation                         0                                                             
GAGE_Corrected contig #                    344                                                           
GAGE_Corrected assembly size               5293140                                                       
GAGE_Min correct contig                    893                                                           
GAGE_Max correct contig                    215960                                                        
GAGE_Corrected N50                         34784 COUNT: 45                                               
