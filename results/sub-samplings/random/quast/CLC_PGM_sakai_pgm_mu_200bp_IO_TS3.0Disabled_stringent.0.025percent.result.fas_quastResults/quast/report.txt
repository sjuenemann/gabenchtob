All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent.result
#Contigs (>= 0 bp)             5563                                                                     
#Contigs (>= 1000 bp)          1521                                                                     
Total length (>= 0 bp)         4624852                                                                  
Total length (>= 1000 bp)      2468860                                                                  
#Contigs                       5563                                                                     
Largest contig                 5928                                                                     
Total length                   4624852                                                                  
Reference length               5594470                                                                  
GC (%)                         50.39                                                                    
Reference GC (%)               50.48                                                                    
N50                            1058                                                                     
NG50                           864                                                                      
N75                            627                                                                      
NG75                           409                                                                      
#misassemblies                 738                                                                      
#local misassemblies           26                                                                       
#unaligned contigs             12 + 446 part                                                            
Unaligned contigs length       24516                                                                    
Genome fraction (%)            78.008                                                                   
Duplication ratio              1.043                                                                    
#N's per 100 kbp               19.44                                                                    
#mismatches per 100 kbp        91.24                                                                    
#indels per 100 kbp            328.01                                                                   
#genes                         1374 + 3388 part                                                         
#predicted genes (unique)      10131                                                                    
#predicted genes (>= 0 bp)     10137                                                                    
#predicted genes (>= 300 bp)   4914                                                                     
#predicted genes (>= 1500 bp)  11                                                                       
#predicted genes (>= 3000 bp)  0                                                                        
Largest alignment              5609                                                                     
NA50                           975                                                                      
NGA50                          784                                                                      
NA75                           558                                                                      
NGA75                          337                                                                      
