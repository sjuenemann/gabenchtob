All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.025percent.result
GAGE_Contigs #                   300                                                                        
GAGE_Min contig                  211                                                                        
GAGE_Max contig                  1400                                                                       
GAGE_N50                         0 COUNT: 0                                                                 
GAGE_Genome size                 4411532                                                                    
GAGE_Assembly size               122153                                                                     
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     4270774(96.81%)                                                            
GAGE_Missing assembly bases      3826(3.13%)                                                                
GAGE_Missing assembly contigs    1(0.33%)                                                                   
GAGE_Duplicated reference bases  4789                                                                       
GAGE_Compressed reference bases  26513                                                                      
GAGE_Bad trim                    3141                                                                       
GAGE_Avg idy                     99.29                                                                      
GAGE_SNPs                        218                                                                        
GAGE_Indels < 5bp                497                                                                        
GAGE_Indels >= 5                 4                                                                          
GAGE_Inversions                  68                                                                         
GAGE_Relocation                  16                                                                         
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          297                                                                        
GAGE_Corrected assembly size     103603                                                                     
GAGE_Min correct contig          201                                                                        
GAGE_Max correct contig          1362                                                                       
GAGE_Corrected N50               0 COUNT: 0                                                                 
