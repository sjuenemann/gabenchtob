All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.500percent_out.unpadded
GAGE_Contigs #                   3675                                                                            
GAGE_Min contig                  200                                                                             
GAGE_Max contig                  429696                                                                          
GAGE_N50                         152997 COUNT: 6                                                                 
GAGE_Genome size                 2813862                                                                         
GAGE_Assembly size               5222658                                                                         
GAGE_Chaff bases                 0                                                                               
GAGE_Missing reference bases     440(0.02%)                                                                      
GAGE_Missing assembly bases      13058(0.25%)                                                                    
GAGE_Missing assembly contigs    6(0.16%)                                                                        
GAGE_Duplicated reference bases  2385782                                                                         
GAGE_Compressed reference bases  12333                                                                           
GAGE_Bad trim                    10191                                                                           
GAGE_Avg idy                     99.98                                                                           
GAGE_SNPs                        40                                                                              
GAGE_Indels < 5bp                61                                                                              
GAGE_Indels >= 5                 4                                                                               
GAGE_Inversions                  3                                                                               
GAGE_Relocation                  3                                                                               
GAGE_Translocation               0                                                                               
GAGE_Corrected contig #          75                                                                              
GAGE_Corrected assembly size     2848099                                                                         
GAGE_Min correct contig          206                                                                             
GAGE_Max correct contig          429700                                                                          
GAGE_Corrected N50               126917 COUNT: 7                                                                 
