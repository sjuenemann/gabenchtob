All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.975percent_out.unpadded
#Contigs (>= 0 bp)             761                                                                                  
#Contigs (>= 1000 bp)          126                                                                                  
Total length (>= 0 bp)         4630058                                                                              
Total length (>= 1000 bp)      4353809                                                                              
#Contigs                       692                                                                                  
Largest contig                 280207                                                                               
Total length                   4620406                                                                              
Reference length               4411532                                                                              
GC (%)                         65.19                                                                                
Reference GC (%)               65.61                                                                                
N50                            75710                                                                                
NG50                           77689                                                                                
N75                            34303                                                                                
NG75                           38060                                                                                
#misassemblies                 32                                                                                   
#local misassemblies           23                                                                                   
#unaligned contigs             1 + 8 part                                                                           
Unaligned contigs length       1349                                                                                 
Genome fraction (%)            98.553                                                                               
Duplication ratio              1.071                                                                                
#N's per 100 kbp               1.21                                                                                 
#mismatches per 100 kbp        11.96                                                                                
#indels per 100 kbp            12.05                                                                                
#genes                         4000 + 109 part                                                                      
#predicted genes (unique)      4924                                                                                 
#predicted genes (>= 0 bp)     4958                                                                                 
#predicted genes (>= 300 bp)   3865                                                                                 
#predicted genes (>= 1500 bp)  543                                                                                  
#predicted genes (>= 3000 bp)  72                                                                                   
Largest alignment              198578                                                                               
NA50                           63790                                                                                
NGA50                          65404                                                                                
NA75                           29223                                                                                
NGA75                          35716                                                                                
