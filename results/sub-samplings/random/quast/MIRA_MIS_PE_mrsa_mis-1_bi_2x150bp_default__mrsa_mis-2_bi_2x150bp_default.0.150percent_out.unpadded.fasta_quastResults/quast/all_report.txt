All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.150percent_out.unpadded
#Contigs                                   388                                                                                               
#Contigs (>= 0 bp)                         399                                                                                               
#Contigs (>= 1000 bp)                      213                                                                                               
Largest contig                             98183                                                                                             
Total length                               2878622                                                                                           
Total length (>= 0 bp)                     2880312                                                                                           
Total length (>= 1000 bp)                  2804251                                                                                           
Reference length                           2813862                                                                                           
N50                                        20726                                                                                             
NG50                                       21155                                                                                             
N75                                        11197                                                                                             
NG75                                       11462                                                                                             
L50                                        39                                                                                                
LG50                                       37                                                                                                
L75                                        88                                                                                                
LG75                                       84                                                                                                
#local misassemblies                       4                                                                                                 
#misassemblies                             17                                                                                                
#misassembled contigs                      13                                                                                                
Misassembled contigs length                169655                                                                                            
Misassemblies inter-contig overlap         9087                                                                                              
#unaligned contigs                         2 + 0 part                                                                                        
Unaligned contigs length                   6231                                                                                              
#ambiguously mapped contigs                15                                                                                                
Extra bases in ambiguously mapped contigs  -6842                                                                                             
#N's                                       30                                                                                                
#N's per 100 kbp                           1.04                                                                                              
Genome fraction (%)                        99.378                                                                                            
Duplication ratio                          1.028                                                                                             
#genes                                     2563 + 157 part                                                                                   
#predicted genes (unique)                  2915                                                                                              
#predicted genes (>= 0 bp)                 2935                                                                                              
#predicted genes (>= 300 bp)               2407                                                                                              
#predicted genes (>= 1500 bp)              289                                                                                               
#predicted genes (>= 3000 bp)              26                                                                                                
#mismatches                                298                                                                                               
#indels                                    66                                                                                                
Indels length                              201                                                                                               
#mismatches per 100 kbp                    10.66                                                                                             
#indels per 100 kbp                        2.36                                                                                              
GC (%)                                     32.95                                                                                             
Reference GC (%)                           32.81                                                                                             
Average %IDY                               99.086                                                                                            
Largest alignment                          98183                                                                                             
NA50                                       20495                                                                                             
NGA50                                      20726                                                                                             
NA75                                       10691                                                                                             
NGA75                                      11376                                                                                             
LA50                                       40                                                                                                
LGA50                                      38                                                                                                
LA75                                       91                                                                                                
LGA75                                      86                                                                                                
#Mis_misassemblies                         17                                                                                                
#Mis_relocations                           16                                                                                                
#Mis_translocations                        0                                                                                                 
#Mis_inversions                            1                                                                                                 
#Mis_misassembled contigs                  13                                                                                                
Mis_Misassembled contigs length            169655                                                                                            
#Mis_local misassemblies                   4                                                                                                 
#Mis_short indels (<= 5 bp)                60                                                                                                
#Mis_long indels (> 5 bp)                  6                                                                                                 
#Una_fully unaligned contigs               2                                                                                                 
Una_Fully unaligned length                 6231                                                                                              
#Una_partially unaligned contigs           0                                                                                                 
#Una_with misassembly                      0                                                                                                 
#Una_both parts are significant            0                                                                                                 
Una_Partially unaligned length             0                                                                                                 
GAGE_Contigs #                             388                                                                                               
GAGE_Min contig                            216                                                                                               
GAGE_Max contig                            98183                                                                                             
GAGE_N50                                   21155 COUNT: 37                                                                                   
GAGE_Genome size                           2813862                                                                                           
GAGE_Assembly size                         2878622                                                                                           
GAGE_Chaff bases                           0                                                                                                 
GAGE_Missing reference bases               10025(0.36%)                                                                                      
GAGE_Missing assembly bases                6232(0.22%)                                                                                       
GAGE_Missing assembly contigs              2(0.52%)                                                                                          
GAGE_Duplicated reference bases            79051                                                                                             
GAGE_Compressed reference bases            11850                                                                                             
GAGE_Bad trim                              1                                                                                                 
GAGE_Avg idy                               99.98                                                                                             
GAGE_SNPs                                  90                                                                                                
GAGE_Indels < 5bp                          15                                                                                                
GAGE_Indels >= 5                           3                                                                                                 
GAGE_Inversions                            3                                                                                                 
GAGE_Relocation                            6                                                                                                 
GAGE_Translocation                         0                                                                                                 
GAGE_Corrected contig #                    253                                                                                               
GAGE_Corrected assembly size               2817567                                                                                           
GAGE_Min correct contig                    237                                                                                               
GAGE_Max correct contig                    98183                                                                                             
GAGE_Corrected N50                         20726 COUNT: 40                                                                                   
