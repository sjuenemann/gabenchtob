All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.300percent.result
#Mis_misassemblies               20                                                                            
#Mis_relocations                 19                                                                            
#Mis_translocations              0                                                                             
#Mis_inversions                  1                                                                             
#Mis_misassembled contigs        20                                                                            
Mis_Misassembled contigs length  199874                                                                        
#Mis_local misassemblies         9                                                                             
#mismatches                      404                                                                           
#indels                          1908                                                                          
#Mis_short indels (<= 5 bp)      1900                                                                          
#Mis_long indels (> 5 bp)        8                                                                             
Indels length                    2108                                                                          
