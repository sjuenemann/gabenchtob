All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.4percent.result
GAGE_Contigs #                   862                                                         
GAGE_Min contig                  200                                                         
GAGE_Max contig                  130611                                                      
GAGE_N50                         36203 COUNT: 48                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5455688                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     28258(0.51%)                                                
GAGE_Missing assembly bases      2322(0.04%)                                                 
GAGE_Missing assembly contigs    3(0.35%)                                                    
GAGE_Duplicated reference bases  91123                                                       
GAGE_Compressed reference bases  252212                                                      
GAGE_Bad trim                    1061                                                        
GAGE_Avg idy                     99.99                                                       
GAGE_SNPs                        93                                                          
GAGE_Indels < 5bp                421                                                         
GAGE_Indels >= 5                 5                                                           
GAGE_Inversions                  0                                                           
GAGE_Relocation                  5                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          587                                                         
GAGE_Corrected assembly size     5364023                                                     
GAGE_Min correct contig          200                                                         
GAGE_Max correct contig          130616                                                      
GAGE_Corrected N50               35253 COUNT: 49                                             
