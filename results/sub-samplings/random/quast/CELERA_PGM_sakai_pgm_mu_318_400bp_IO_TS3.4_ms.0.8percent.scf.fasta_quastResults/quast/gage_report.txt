All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.8percent.scf
GAGE_Contigs #                   336                                                         
GAGE_Min contig                  1002                                                        
GAGE_Max contig                  220962                                                      
GAGE_N50                         40380 COUNT: 40                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5296987                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     271686(4.86%)                                               
GAGE_Missing assembly bases      86(0.00%)                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                    
GAGE_Duplicated reference bases  449                                                         
GAGE_Compressed reference bases  142748                                                      
GAGE_Bad trim                    86                                                          
GAGE_Avg idy                     99.99                                                       
GAGE_SNPs                        61                                                          
GAGE_Indels < 5bp                253                                                         
GAGE_Indels >= 5                 7                                                           
GAGE_Inversions                  1                                                           
GAGE_Relocation                  2                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          346                                                         
GAGE_Corrected assembly size     5299516                                                     
GAGE_Min correct contig          387                                                         
GAGE_Max correct contig          220959                                                      
GAGE_Corrected N50               38797 COUNT: 42                                             
