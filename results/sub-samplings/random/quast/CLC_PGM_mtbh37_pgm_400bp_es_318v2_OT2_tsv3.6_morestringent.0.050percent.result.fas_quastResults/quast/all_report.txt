All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.050percent.result
#Contigs                                   3430                                                                          
#Contigs (>= 0 bp)                         3430                                                                          
#Contigs (>= 1000 bp)                      1069                                                                          
Largest contig                             11339                                                                         
Total length                               3507127                                                                       
Total length (>= 0 bp)                     3507127                                                                       
Total length (>= 1000 bp)                  2372002                                                                       
Reference length                           4411532                                                                       
N50                                        1658                                                                          
NG50                                       1161                                                                          
N75                                        761                                                                           
NG75                                       341                                                                           
L50                                        588                                                                           
LG50                                       915                                                                           
L75                                        1367                                                                          
LG75                                       2692                                                                          
#local misassemblies                       17                                                                            
#misassemblies                             392                                                                           
#misassembled contigs                      334                                                                           
Misassembled contigs length                806365                                                                        
Misassemblies inter-contig overlap         9351                                                                          
#unaligned contigs                         7 + 59 part                                                                   
Unaligned contigs length                   7950                                                                          
#ambiguously mapped contigs                12                                                                            
Extra bases in ambiguously mapped contigs  -7048                                                                         
#N's                                       1081                                                                          
#N's per 100 kbp                           30.82                                                                         
Genome fraction (%)                        76.304                                                                        
Duplication ratio                          1.040                                                                         
#genes                                     1428 + 2399 part                                                              
#predicted genes (unique)                  6790                                                                          
#predicted genes (>= 0 bp)                 6795                                                                          
#predicted genes (>= 300 bp)               3647                                                                          
#predicted genes (>= 1500 bp)              58                                                                            
#predicted genes (>= 3000 bp)              0                                                                             
#mismatches                                2579                                                                          
#indels                                    7545                                                                          
Indels length                              8075                                                                          
#mismatches per 100 kbp                    76.61                                                                         
#indels per 100 kbp                        224.14                                                                        
GC (%)                                     64.71                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.656                                                                        
Largest alignment                          8616                                                                          
NA50                                       1446                                                                          
NGA50                                      1029                                                                          
NA75                                       683                                                                           
NGA75                                      307                                                                           
LA50                                       687                                                                           
LGA50                                      1057                                                                          
LA75                                       1565                                                                          
LGA75                                      3012                                                                          
#Mis_misassemblies                         392                                                                           
#Mis_relocations                           384                                                                           
#Mis_translocations                        0                                                                             
#Mis_inversions                            8                                                                             
#Mis_misassembled contigs                  334                                                                           
Mis_Misassembled contigs length            806365                                                                        
#Mis_local misassemblies                   17                                                                            
#Mis_short indels (<= 5 bp)                7537                                                                          
#Mis_long indels (> 5 bp)                  8                                                                             
#Una_fully unaligned contigs               7                                                                             
Una_Fully unaligned length                 3451                                                                          
#Una_partially unaligned contigs           59                                                                            
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            6                                                                             
Una_Partially unaligned length             4499                                                                          
GAGE_Contigs #                             3430                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            11339                                                                         
GAGE_N50                                   1161 COUNT: 915                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         3507127                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               993711(22.53%)                                                                
GAGE_Missing assembly bases                13110(0.37%)                                                                  
GAGE_Missing assembly contigs              7(0.20%)                                                                      
GAGE_Duplicated reference bases            47493                                                                         
GAGE_Compressed reference bases            48579                                                                         
GAGE_Bad trim                              8346                                                                          
GAGE_Avg idy                               99.68                                                                         
GAGE_SNPs                                  2063                                                                          
GAGE_Indels < 5bp                          7464                                                                          
GAGE_Indels >= 5                           17                                                                            
GAGE_Inversions                            123                                                                           
GAGE_Relocation                            86                                                                            
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    3560                                                                          
GAGE_Corrected assembly size               3450903                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    8622                                                                          
GAGE_Corrected N50                         1036 COUNT: 1063                                                              
