All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                     #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.450percent.result  7                             3982                        690                               0                      2                                31587                           2713
