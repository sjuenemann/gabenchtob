All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.result
#Contigs                                   319                                                              
#Contigs (>= 0 bp)                         319                                                              
#Contigs (>= 1000 bp)                      128                                                              
Largest contig                             103081                                                           
Total length                               2770010                                                          
Total length (>= 0 bp)                     2770010                                                          
Total length (>= 1000 bp)                  2718888                                                          
Reference length                           2813862                                                          
N50                                        32422                                                            
NG50                                       31924                                                            
N75                                        19710                                                            
NG75                                       19265                                                            
L50                                        27                                                               
LG50                                       28                                                               
L75                                        53                                                               
LG75                                       55                                                               
#local misassemblies                       2                                                                
#misassemblies                             7                                                                
#misassembled contigs                      7                                                                
Misassembled contigs length                25113                                                            
Misassemblies inter-contig overlap         460                                                              
#unaligned contigs                         7 + 11 part                                                      
Unaligned contigs length                   2020                                                             
#ambiguously mapped contigs                10                                                               
Extra bases in ambiguously mapped contigs  -7349                                                            
#N's                                       94                                                               
#N's per 100 kbp                           3.39                                                             
Genome fraction (%)                        97.076                                                           
Duplication ratio                          1.011                                                            
#genes                                     2586 + 78 part                                                   
#predicted genes (unique)                  2819                                                             
#predicted genes (>= 0 bp)                 2824                                                             
#predicted genes (>= 300 bp)               2297                                                             
#predicted genes (>= 1500 bp)              281                                                              
#predicted genes (>= 3000 bp)              24                                                               
#mismatches                                52                                                               
#indels                                    307                                                              
Indels length                              325                                                              
#mismatches per 100 kbp                    1.90                                                             
#indels per 100 kbp                        11.24                                                            
GC (%)                                     32.64                                                            
Reference GC (%)                           32.81                                                            
Average %IDY                               99.255                                                           
Largest alignment                          103081                                                           
NA50                                       32422                                                            
NGA50                                      31924                                                            
NA75                                       19668                                                            
NGA75                                      18553                                                            
LA50                                       27                                                               
LGA50                                      28                                                               
LA75                                       53                                                               
LGA75                                      55                                                               
#Mis_misassemblies                         7                                                                
#Mis_relocations                           4                                                                
#Mis_translocations                        0                                                                
#Mis_inversions                            3                                                                
#Mis_misassembled contigs                  7                                                                
Mis_Misassembled contigs length            25113                                                            
#Mis_local misassemblies                   2                                                                
#Mis_short indels (<= 5 bp)                307                                                              
#Mis_long indels (> 5 bp)                  0                                                                
#Una_fully unaligned contigs               7                                                                
Una_Fully unaligned length                 1587                                                             
#Una_partially unaligned contigs           11                                                               
#Una_with misassembly                      0                                                                
#Una_both parts are significant            0                                                                
Una_Partially unaligned length             433                                                              
GAGE_Contigs #                             319                                                              
GAGE_Min contig                            200                                                              
GAGE_Max contig                            103081                                                           
GAGE_N50                                   31924 COUNT: 28                                                  
GAGE_Genome size                           2813862                                                          
GAGE_Assembly size                         2770010                                                          
GAGE_Chaff bases                           0                                                                
GAGE_Missing reference bases               37164(1.32%)                                                     
GAGE_Missing assembly bases                2200(0.08%)                                                      
GAGE_Missing assembly contigs              7(2.19%)                                                         
GAGE_Duplicated reference bases            27076                                                            
GAGE_Compressed reference bases            42249                                                            
GAGE_Bad trim                              613                                                              
GAGE_Avg idy                               99.98                                                            
GAGE_SNPs                                  47                                                               
GAGE_Indels < 5bp                          294                                                              
GAGE_Indels >= 5                           2                                                                
GAGE_Inversions                            1                                                                
GAGE_Relocation                            3                                                                
GAGE_Translocation                         0                                                                
GAGE_Corrected contig #                    202                                                              
GAGE_Corrected assembly size               2741465                                                          
GAGE_Min correct contig                    203                                                              
GAGE_Max correct contig                    103086                                                           
GAGE_Corrected N50                         31845 COUNT: 28                                                  
