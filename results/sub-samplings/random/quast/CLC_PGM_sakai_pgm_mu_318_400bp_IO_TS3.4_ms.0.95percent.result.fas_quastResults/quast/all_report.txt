All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.95percent.result
#Contigs                                   1187                                                         
#Contigs (>= 0 bp)                         1187                                                         
#Contigs (>= 1000 bp)                      293                                                          
Largest contig                             141102                                                       
Total length                               5513681                                                      
Total length (>= 0 bp)                     5513681                                                      
Total length (>= 1000 bp)                  5207078                                                      
Reference length                           5594470                                                      
N50                                        40296                                                        
NG50                                       39944                                                        
N75                                        19535                                                        
NG75                                       18890                                                        
L50                                        42                                                           
LG50                                       43                                                           
L75                                        93                                                           
LG75                                       96                                                           
#local misassemblies                       9                                                            
#misassemblies                             7                                                            
#misassembled contigs                      7                                                            
Misassembled contigs length                56998                                                        
Misassemblies inter-contig overlap         3622                                                         
#unaligned contigs                         18 + 7 part                                                  
Unaligned contigs length                   5452                                                         
#ambiguously mapped contigs                157                                                          
Extra bases in ambiguously mapped contigs  -95243                                                       
#N's                                       2193                                                         
#N's per 100 kbp                           39.77                                                        
Genome fraction (%)                        94.359                                                       
Duplication ratio                          1.026                                                        
#genes                                     4812 + 323 part                                              
#predicted genes (unique)                  6111                                                         
#predicted genes (>= 0 bp)                 6137                                                         
#predicted genes (>= 300 bp)               4647                                                         
#predicted genes (>= 1500 bp)              638                                                          
#predicted genes (>= 3000 bp)              59                                                           
#mismatches                                272                                                          
#indels                                    518                                                          
Indels length                              615                                                          
#mismatches per 100 kbp                    5.15                                                         
#indels per 100 kbp                        9.81                                                         
GC (%)                                     50.27                                                        
Reference GC (%)                           50.48                                                        
Average %IDY                               98.984                                                       
Largest alignment                          141102                                                       
NA50                                       40296                                                        
NGA50                                      39944                                                        
NA75                                       19400                                                        
NGA75                                      18885                                                        
LA50                                       42                                                           
LGA50                                      43                                                           
LA75                                       93                                                           
LGA75                                      96                                                           
#Mis_misassemblies                         7                                                            
#Mis_relocations                           7                                                            
#Mis_translocations                        0                                                            
#Mis_inversions                            0                                                            
#Mis_misassembled contigs                  7                                                            
Mis_Misassembled contigs length            56998                                                        
#Mis_local misassemblies                   9                                                            
#Mis_short indels (<= 5 bp)                511                                                          
#Mis_long indels (> 5 bp)                  7                                                            
#Una_fully unaligned contigs               18                                                           
Una_Fully unaligned length                 4778                                                         
#Una_partially unaligned contigs           7                                                            
#Una_with misassembly                      0                                                            
#Una_both parts are significant            0                                                            
Una_Partially unaligned length             674                                                          
GAGE_Contigs #                             1187                                                         
GAGE_Min contig                            200                                                          
GAGE_Max contig                            141102                                                       
GAGE_N50                                   39944 COUNT: 43                                              
GAGE_Genome size                           5594470                                                      
GAGE_Assembly size                         5513681                                                      
GAGE_Chaff bases                           0                                                            
GAGE_Missing reference bases               43100(0.77%)                                                 
GAGE_Missing assembly bases                5352(0.10%)                                                  
GAGE_Missing assembly contigs              17(1.43%)                                                    
GAGE_Duplicated reference bases            160231                                                       
GAGE_Compressed reference bases            252026                                                       
GAGE_Bad trim                              788                                                          
GAGE_Avg idy                               99.98                                                        
GAGE_SNPs                                  173                                                          
GAGE_Indels < 5bp                          449                                                          
GAGE_Indels >= 5                           5                                                            
GAGE_Inversions                            0                                                            
GAGE_Relocation                            5                                                            
GAGE_Translocation                         0                                                            
GAGE_Corrected contig #                    661                                                          
GAGE_Corrected assembly size               5351370                                                      
GAGE_Min correct contig                    200                                                          
GAGE_Max correct contig                    141103                                                       
GAGE_Corrected N50                         38736 COUNT: 44                                              
