All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.95percent.result
#Contigs (>= 0 bp)             1187                                                         
#Contigs (>= 1000 bp)          293                                                          
Total length (>= 0 bp)         5513681                                                      
Total length (>= 1000 bp)      5207078                                                      
#Contigs                       1187                                                         
Largest contig                 141102                                                       
Total length                   5513681                                                      
Reference length               5594470                                                      
GC (%)                         50.27                                                        
Reference GC (%)               50.48                                                        
N50                            40296                                                        
NG50                           39944                                                        
N75                            19535                                                        
NG75                           18890                                                        
#misassemblies                 7                                                            
#local misassemblies           9                                                            
#unaligned contigs             18 + 7 part                                                  
Unaligned contigs length       5452                                                         
Genome fraction (%)            94.359                                                       
Duplication ratio              1.026                                                        
#N's per 100 kbp               39.77                                                        
#mismatches per 100 kbp        5.15                                                         
#indels per 100 kbp            9.81                                                         
#genes                         4812 + 323 part                                              
#predicted genes (unique)      6111                                                         
#predicted genes (>= 0 bp)     6137                                                         
#predicted genes (>= 300 bp)   4647                                                         
#predicted genes (>= 1500 bp)  638                                                          
#predicted genes (>= 3000 bp)  59                                                           
Largest alignment              141102                                                       
NA50                           40296                                                        
NGA50                          39944                                                        
NA75                           19400                                                        
NGA75                          18885                                                        
