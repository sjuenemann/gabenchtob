All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.650percent.contigs
#Contigs (>= 0 bp)             563                                                                           
#Contigs (>= 1000 bp)          78                                                                            
Total length (>= 0 bp)         2835254                                                                       
Total length (>= 1000 bp)      2755623                                                                       
#Contigs                       135                                                                           
Largest contig                 127122                                                                        
Total length                   2772107                                                                       
Reference length               2813862                                                                       
GC (%)                         32.65                                                                         
Reference GC (%)               32.81                                                                         
N50                            52830                                                                         
NG50                           51617                                                                         
N75                            35960                                                                         
NG75                           35833                                                                         
#misassemblies                 5                                                                             
#local misassemblies           0                                                                             
#unaligned contigs             0 + 0 part                                                                    
Unaligned contigs length       0                                                                             
Genome fraction (%)            97.975                                                                        
Duplication ratio              1.002                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        1.09                                                                          
#indels per 100 kbp            7.00                                                                          
#genes                         2635 + 52 part                                                                
#predicted genes (unique)      2731                                                                          
#predicted genes (>= 0 bp)     2733                                                                          
#predicted genes (>= 300 bp)   2317                                                                          
#predicted genes (>= 1500 bp)  286                                                                           
#predicted genes (>= 3000 bp)  29                                                                            
Largest alignment              127122                                                                        
NA50                           51617                                                                         
NGA50                          51617                                                                         
NA75                           35833                                                                         
NGA75                          32753                                                                         
