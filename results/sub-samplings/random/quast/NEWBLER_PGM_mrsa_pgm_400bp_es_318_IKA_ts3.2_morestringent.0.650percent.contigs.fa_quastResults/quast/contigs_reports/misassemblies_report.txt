All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.650percent.contigs
#Mis_misassemblies               5                                                                             
#Mis_relocations                 5                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        5                                                                             
Mis_Misassembled contigs length  75322                                                                         
#Mis_local misassemblies         0                                                                             
#mismatches                      30                                                                            
#indels                          193                                                                           
#Mis_short indels (<= 5 bp)      193                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    198                                                                           
