All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.050percent.result
#Contigs (>= 0 bp)             1197                                                                       
#Contigs (>= 1000 bp)          4                                                                          
Total length (>= 0 bp)         506335                                                                     
Total length (>= 1000 bp)      5600                                                                       
#Contigs                       1197                                                                       
Largest contig                 1801                                                                       
Total length                   506335                                                                     
Reference length               4411532                                                                    
GC (%)                         62.97                                                                      
Reference GC (%)               65.61                                                                      
N50                            437                                                                        
NG50                           None                                                                       
N75                            355                                                                        
NG75                           None                                                                       
#misassemblies                 362                                                                        
#local misassemblies           10                                                                         
#unaligned contigs             6 + 193 part                                                               
Unaligned contigs length       13193                                                                      
Genome fraction (%)            10.449                                                                     
Duplication ratio              1.074                                                                      
#N's per 100 kbp               2.17                                                                       
#mismatches per 100 kbp        228.43                                                                     
#indels per 100 kbp            405.01                                                                     
#genes                         31 + 1114 part                                                             
#predicted genes (unique)      1443                                                                       
#predicted genes (>= 0 bp)     1447                                                                       
#predicted genes (>= 300 bp)   436                                                                        
#predicted genes (>= 1500 bp)  0                                                                          
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              1767                                                                       
NA50                           358                                                                        
NGA50                          None                                                                       
NA75                           267                                                                        
NGA75                          None                                                                       
