All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.050percent.result
#Una_fully unaligned contigs      6                                                                          
Una_Fully unaligned length        2334                                                                       
#Una_partially unaligned contigs  193                                                                        
#Una_with misassembly             0                                                                          
#Una_both parts are significant   2                                                                          
Una_Partially unaligned length    10859                                                                      
#N's                              11                                                                         
