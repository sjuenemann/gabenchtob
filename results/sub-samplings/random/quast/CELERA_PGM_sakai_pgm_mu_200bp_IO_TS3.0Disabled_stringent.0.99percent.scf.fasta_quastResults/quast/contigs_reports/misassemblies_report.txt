All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.99percent.scf
#Mis_misassemblies               11                                                                      
#Mis_relocations                 8                                                                       
#Mis_translocations              0                                                                       
#Mis_inversions                  3                                                                       
#Mis_misassembled contigs        11                                                                      
Mis_Misassembled contigs length  68457                                                                   
#Mis_local misassemblies         9                                                                       
#mismatches                      184                                                                     
#indels                          831                                                                     
#Mis_short indels (<= 5 bp)      830                                                                     
#Mis_long indels (> 5 bp)        1                                                                       
Indels length                    851                                                                     
