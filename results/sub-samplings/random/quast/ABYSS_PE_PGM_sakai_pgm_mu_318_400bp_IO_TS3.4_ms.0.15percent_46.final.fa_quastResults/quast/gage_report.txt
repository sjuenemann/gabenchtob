All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.15percent_46.final
GAGE_Contigs #                   1092                                                                
GAGE_Min contig                  200                                                                 
GAGE_Max contig                  70812                                                               
GAGE_N50                         13649 COUNT: 116                                                    
GAGE_Genome size                 5594470                                                             
GAGE_Assembly size               5235856                                                             
GAGE_Chaff bases                 0                                                                   
GAGE_Missing reference bases     224448(4.01%)                                                       
GAGE_Missing assembly bases      7(0.00%)                                                            
GAGE_Missing assembly contigs    0(0.00%)                                                            
GAGE_Duplicated reference bases  0                                                                   
GAGE_Compressed reference bases  150482                                                              
GAGE_Bad trim                    5                                                                   
GAGE_Avg idy                     99.99                                                               
GAGE_SNPs                        175                                                                 
GAGE_Indels < 5bp                345                                                                 
GAGE_Indels >= 5                 6                                                                   
GAGE_Inversions                  0                                                                   
GAGE_Relocation                  3                                                                   
GAGE_Translocation               0                                                                   
GAGE_Corrected contig #          1097                                                                
GAGE_Corrected assembly size     5235894                                                             
GAGE_Min correct contig          200                                                                 
GAGE_Max correct contig          70816                                                               
GAGE_Corrected N50               13457 COUNT: 117                                                    
