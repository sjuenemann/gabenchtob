All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.15percent_46.final
#Contigs (>= 0 bp)             4561                                                                
#Contigs (>= 1000 bp)          599                                                                 
Total length (>= 0 bp)         5525595                                                             
Total length (>= 1000 bp)      5013587                                                             
#Contigs                       1092                                                                
Largest contig                 70812                                                               
Total length                   5235856                                                             
Reference length               5594470                                                             
GC (%)                         50.26                                                               
Reference GC (%)               50.48                                                               
N50                            15133                                                               
NG50                           13649                                                               
N75                            6748                                                                
NG75                           5505                                                                
#misassemblies                 2                                                                   
#local misassemblies           7                                                                   
#unaligned contigs             0 + 0 part                                                          
Unaligned contigs length       0                                                                   
Genome fraction (%)            92.168                                                              
Duplication ratio              1.002                                                               
#N's per 100 kbp               0.00                                                                
#mismatches per 100 kbp        2.23                                                                
#indels per 100 kbp            6.61                                                                
#genes                         4437 + 523 part                                                     
#predicted genes (unique)      5700                                                                
#predicted genes (>= 0 bp)     5701                                                                
#predicted genes (>= 300 bp)   4508                                                                
#predicted genes (>= 1500 bp)  580                                                                 
#predicted genes (>= 3000 bp)  55                                                                  
Largest alignment              70812                                                               
NA50                           14860                                                               
NGA50                          13624                                                               
NA75                           6748                                                                
NGA75                          5505                                                                
