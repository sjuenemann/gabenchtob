All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.975percent.result
#Contigs (>= 0 bp)             1202                                                          
#Contigs (>= 1000 bp)          303                                                           
Total length (>= 0 bp)         5528423                                                       
Total length (>= 1000 bp)      5216004                                                       
#Contigs                       1202                                                          
Largest contig                 245477                                                        
Total length                   5528423                                                       
Reference length               5594470                                                       
GC (%)                         50.25                                                         
Reference GC (%)               50.48                                                         
N50                            39816                                                         
NG50                           37647                                                         
N75                            17581                                                         
NG75                           17347                                                         
#misassemblies                 8                                                             
#local misassemblies           8                                                             
#unaligned contigs             17 + 6 part                                                   
Unaligned contigs length       5033                                                          
Genome fraction (%)            94.505                                                        
Duplication ratio              1.028                                                         
#N's per 100 kbp               27.77                                                         
#mismatches per 100 kbp        6.17                                                          
#indels per 100 kbp            9.51                                                          
#genes                         4817 + 336 part                                               
#predicted genes (unique)      6119                                                          
#predicted genes (>= 0 bp)     6145                                                          
#predicted genes (>= 300 bp)   4658                                                          
#predicted genes (>= 1500 bp)  643                                                           
#predicted genes (>= 3000 bp)  57                                                            
Largest alignment              245477                                                        
NA50                           39816                                                         
NGA50                          37647                                                         
NA75                           17581                                                         
NGA75                          17347                                                         
