All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.975percent_out.unpadded
#Contigs                                   234                                                                                                 
#Contigs (>= 0 bp)                         249                                                                                                 
#Contigs (>= 1000 bp)                      98                                                                                                  
Largest contig                             936237                                                                                              
Total length                               5603173                                                                                             
Total length (>= 0 bp)                     5605661                                                                                             
Total length (>= 1000 bp)                  5540305                                                                                             
Reference length                           5594470                                                                                             
N50                                        161807                                                                                              
NG50                                       161807                                                                                              
N75                                        107833                                                                                              
NG75                                       107833                                                                                              
L50                                        7                                                                                                   
LG50                                       7                                                                                                   
L75                                        17                                                                                                  
LG75                                       17                                                                                                  
#local misassemblies                       13                                                                                                  
#misassemblies                             72                                                                                                  
#misassembled contigs                      35                                                                                                  
Misassembled contigs length                3751802                                                                                             
Misassemblies inter-contig overlap         65533                                                                                               
#unaligned contigs                         2 + 4 part                                                                                          
Unaligned contigs length                   6945                                                                                                
#ambiguously mapped contigs                81                                                                                                  
Extra bases in ambiguously mapped contigs  -54441                                                                                              
#N's                                       85                                                                                                  
#N's per 100 kbp                           1.52                                                                                                
Genome fraction (%)                        98.826                                                                                              
Duplication ratio                          1.014                                                                                               
#genes                                     5334 + 70 part                                                                                      
#predicted genes (unique)                  5447                                                                                                
#predicted genes (>= 0 bp)                 5503                                                                                                
#predicted genes (>= 300 bp)               4649                                                                                                
#predicted genes (>= 1500 bp)              695                                                                                                 
#predicted genes (>= 3000 bp)              76                                                                                                  
#mismatches                                2390                                                                                                
#indels                                    107                                                                                                 
Indels length                              115                                                                                                 
#mismatches per 100 kbp                    43.23                                                                                               
#indels per 100 kbp                        1.94                                                                                                
GC (%)                                     50.45                                                                                               
Reference GC (%)                           50.48                                                                                               
Average %IDY                               98.388                                                                                              
Largest alignment                          392251                                                                                              
NA50                                       140202                                                                                              
NGA50                                      140202                                                                                              
NA75                                       75266                                                                                               
NGA75                                      75266                                                                                               
LA50                                       14                                                                                                  
LGA50                                      14                                                                                                  
LA75                                       27                                                                                                  
LGA75                                      27                                                                                                  
#Mis_misassemblies                         72                                                                                                  
#Mis_relocations                           69                                                                                                  
#Mis_translocations                        2                                                                                                   
#Mis_inversions                            1                                                                                                   
#Mis_misassembled contigs                  35                                                                                                  
Mis_Misassembled contigs length            3751802                                                                                             
#Mis_local misassemblies                   13                                                                                                  
#Mis_short indels (<= 5 bp)                107                                                                                                 
#Mis_long indels (> 5 bp)                  0                                                                                                   
#Una_fully unaligned contigs               2                                                                                                   
Una_Fully unaligned length                 6265                                                                                                
#Una_partially unaligned contigs           4                                                                                                   
#Una_with misassembly                      0                                                                                                   
#Una_both parts are significant            1                                                                                                   
Una_Partially unaligned length             680                                                                                                 
GAGE_Contigs #                             234                                                                                                 
GAGE_Min contig                            203                                                                                                 
GAGE_Max contig                            936237                                                                                              
GAGE_N50                                   161807 COUNT: 7                                                                                     
GAGE_Genome size                           5594470                                                                                             
GAGE_Assembly size                         5603173                                                                                             
GAGE_Chaff bases                           0                                                                                                   
GAGE_Missing reference bases               776(0.01%)                                                                                          
GAGE_Missing assembly bases                6984(0.12%)                                                                                         
GAGE_Missing assembly contigs              2(0.85%)                                                                                            
GAGE_Duplicated reference bases            117637                                                                                              
GAGE_Compressed reference bases            150420                                                                                              
GAGE_Bad trim                              719                                                                                                 
GAGE_Avg idy                               99.94                                                                                               
GAGE_SNPs                                  1153                                                                                                
GAGE_Indels < 5bp                          68                                                                                                  
GAGE_Indels >= 5                           4                                                                                                   
GAGE_Inversions                            16                                                                                                  
GAGE_Relocation                            16                                                                                                  
GAGE_Translocation                         3                                                                                                   
GAGE_Corrected contig #                    145                                                                                                 
GAGE_Corrected assembly size               5565954                                                                                             
GAGE_Min correct contig                    318                                                                                                 
GAGE_Max correct contig                    392593                                                                                              
GAGE_Corrected N50                         140202 COUNT: 14                                                                                    
