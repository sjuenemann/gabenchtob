All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.35percent.contigs
GAGE_Contigs #                   281                                                               
GAGE_Min contig                  200                                                               
GAGE_Max contig                  377171                                                            
GAGE_N50                         142755 COUNT: 14                                                  
GAGE_Genome size                 5594470                                                           
GAGE_Assembly size               5353401                                                           
GAGE_Chaff bases                 0                                                                 
GAGE_Missing reference bases     37009(0.66%)                                                      
GAGE_Missing assembly bases      120(0.00%)                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                          
GAGE_Duplicated reference bases  2505                                                              
GAGE_Compressed reference bases  225245                                                            
GAGE_Bad trim                    120                                                               
GAGE_Avg idy                     99.99                                                             
GAGE_SNPs                        67                                                                
GAGE_Indels < 5bp                214                                                               
GAGE_Indels >= 5                 9                                                                 
GAGE_Inversions                  0                                                                 
GAGE_Relocation                  6                                                                 
GAGE_Translocation               0                                                                 
GAGE_Corrected contig #          286                                                               
GAGE_Corrected assembly size     5353747                                                           
GAGE_Min correct contig          201                                                               
GAGE_Max correct contig          313770                                                            
GAGE_Corrected N50               95955 COUNT: 19                                                   
