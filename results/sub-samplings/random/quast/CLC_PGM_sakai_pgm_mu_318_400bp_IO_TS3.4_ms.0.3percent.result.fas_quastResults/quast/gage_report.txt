All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.3percent.result
GAGE_Contigs #                   707                                                         
GAGE_Min contig                  200                                                         
GAGE_Max contig                  136008                                                      
GAGE_N50                         41073 COUNT: 43                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5434748                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     22770(0.41%)                                                
GAGE_Missing assembly bases      1291(0.02%)                                                 
GAGE_Missing assembly contigs    5(0.71%)                                                    
GAGE_Duplicated reference bases  65025                                                       
GAGE_Compressed reference bases  240210                                                      
GAGE_Bad trim                    193                                                         
GAGE_Avg idy                     99.98                                                       
GAGE_SNPs                        83                                                          
GAGE_Indels < 5bp                454                                                         
GAGE_Indels >= 5                 4                                                           
GAGE_Inversions                  3                                                           
GAGE_Relocation                  6                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          498                                                         
GAGE_Corrected assembly size     5372254                                                     
GAGE_Min correct contig          200                                                         
GAGE_Max correct contig          136014                                                      
GAGE_Corrected N50               37883 COUNT: 44                                             
