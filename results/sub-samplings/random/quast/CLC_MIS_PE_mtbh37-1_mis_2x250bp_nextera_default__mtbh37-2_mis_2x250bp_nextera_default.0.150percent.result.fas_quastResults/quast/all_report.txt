All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.150percent.result_broken  CLC_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.150percent.result
#Contigs                                   799                                                                                                               752                                                                                                      
#Contigs (>= 0 bp)                         944                                                                                                               752                                                                                                      
#Contigs (>= 1000 bp)                      359                                                                                                               346                                                                                                      
Largest contig                             74747                                                                                                             97010                                                                                                    
Total length                               4307671                                                                                                           4332403                                                                                                  
Total length (>= 0 bp)                     4320189                                                                                                           4332403                                                                                                  
Total length (>= 1000 bp)                  4121858                                                                                                           4162972                                                                                                  
Reference length                           4411532                                                                                                           4411532                                                                                                  
N50                                        19114                                                                                                             21466                                                                                                    
NG50                                       18461                                                                                                             21307                                                                                                    
N75                                        9954                                                                                                              10501                                                                                                    
NG75                                       9179                                                                                                              10257                                                                                                    
L50                                        72                                                                                                                65                                                                                                       
LG50                                       75                                                                                                                66                                                                                                       
L75                                        148                                                                                                               134                                                                                                      
LG75                                       156                                                                                                               140                                                                                                      
#local misassemblies                       8                                                                                                                 63                                                                                                       
#misassemblies                             31                                                                                                                37                                                                                                       
#misassembled contigs                      29                                                                                                                35                                                                                                       
Misassembled contigs length                430607                                                                                                            472772                                                                                                   
Misassemblies inter-contig overlap         2681                                                                                                              2737                                                                                                     
#unaligned contigs                         26 + 56 part                                                                                                      38 + 94 part                                                                                             
Unaligned contigs length                   14802                                                                                                             28143                                                                                                    
#ambiguously mapped contigs                16                                                                                                                14                                                                                                       
Extra bases in ambiguously mapped contigs  -8399                                                                                                             -7838                                                                                                    
#N's                                       269                                                                                                               12539                                                                                                    
#N's per 100 kbp                           6.24                                                                                                              289.42                                                                                                   
Genome fraction (%)                        96.354                                                                                                            96.385                                                                                                   
Duplication ratio                          1.009                                                                                                             1.011                                                                                                    
#genes                                     3621 + 422 part                                                                                                   3634 + 408 part                                                                                          
#predicted genes (unique)                  4470                                                                                                              4460                                                                                                     
#predicted genes (>= 0 bp)                 4471                                                                                                              4462                                                                                                     
#predicted genes (>= 300 bp)               3703                                                                                                              3701                                                                                                     
#predicted genes (>= 1500 bp)              514                                                                                                               516                                                                                                      
#predicted genes (>= 3000 bp)              63                                                                                                                63                                                                                                       
#mismatches                                1721                                                                                                              1749                                                                                                     
#indels                                    211                                                                                                               555                                                                                                      
Indels length                              818                                                                                                               1639                                                                                                     
#mismatches per 100 kbp                    40.49                                                                                                             41.13                                                                                                    
#indels per 100 kbp                        4.96                                                                                                              13.05                                                                                                    
GC (%)                                     65.55                                                                                                             65.60                                                                                                    
Reference GC (%)                           65.61                                                                                                             65.61                                                                                                    
Average %IDY                               99.607                                                                                                            99.523                                                                                                   
Largest alignment                          74705                                                                                                             97007                                                                                                    
NA50                                       18329                                                                                                             19752                                                                                                    
NGA50                                      17864                                                                                                             19113                                                                                                    
NA75                                       9460                                                                                                              10200                                                                                                    
NGA75                                      8807                                                                                                              9674                                                                                                     
LA50                                       75                                                                                                                69                                                                                                       
LGA50                                      78                                                                                                                71                                                                                                       
LA75                                       155                                                                                                               143                                                                                                      
LGA75                                      163                                                                                                               149                                                                                                      
#Mis_misassemblies                         31                                                                                                                37                                                                                                       
#Mis_relocations                           31                                                                                                                37                                                                                                       
#Mis_translocations                        0                                                                                                                 0                                                                                                        
#Mis_inversions                            0                                                                                                                 0                                                                                                        
#Mis_misassembled contigs                  29                                                                                                                35                                                                                                       
Mis_Misassembled contigs length            430607                                                                                                            472772                                                                                                   
#Mis_local misassemblies                   8                                                                                                                 63                                                                                                       
#Mis_short indels (<= 5 bp)                187                                                                                                               510                                                                                                      
#Mis_long indels (> 5 bp)                  24                                                                                                                45                                                                                                       
#Una_fully unaligned contigs               26                                                                                                                38                                                                                                       
Una_Fully unaligned length                 8822                                                                                                              14619                                                                                                    
#Una_partially unaligned contigs           56                                                                                                                94                                                                                                       
#Una_with misassembly                      0                                                                                                                 0                                                                                                        
#Una_both parts are significant            4                                                                                                                 7                                                                                                        
Una_Partially unaligned length             5980                                                                                                              13524                                                                                                    
GAGE_Contigs #                             799                                                                                                               752                                                                                                      
GAGE_Min contig                            200                                                                                                               200                                                                                                      
GAGE_Max contig                            74747                                                                                                             97010                                                                                                    
GAGE_N50                                   18461 COUNT: 75                                                                                                   21307 COUNT: 66                                                                                          
GAGE_Genome size                           4411532                                                                                                           4411532                                                                                                  
GAGE_Assembly size                         4307671                                                                                                           4332403                                                                                                  
GAGE_Chaff bases                           0                                                                                                                 0                                                                                                        
GAGE_Missing reference bases               105211(2.38%)                                                                                                     103324(2.34%)                                                                                            
GAGE_Missing assembly bases                16840(0.39%)                                                                                                      36853(0.85%)                                                                                             
GAGE_Missing assembly contigs              25(3.13%)                                                                                                         35(4.65%)                                                                                                
GAGE_Duplicated reference bases            10237                                                                                                             12763                                                                                                    
GAGE_Compressed reference bases            50286                                                                                                             50365                                                                                                    
GAGE_Bad trim                              8358                                                                                                              16707                                                                                                    
GAGE_Avg idy                               99.95                                                                                                             99.94                                                                                                    
GAGE_SNPs                                  1096                                                                                                              1119                                                                                                     
GAGE_Indels < 5bp                          95                                                                                                                119                                                                                                      
GAGE_Indels >= 5                           38                                                                                                                114                                                                                                      
GAGE_Inversions                            11                                                                                                                12                                                                                                       
GAGE_Relocation                            9                                                                                                                 11                                                                                                       
GAGE_Translocation                         0                                                                                                                 0                                                                                                        
GAGE_Corrected contig #                    785                                                                                                               776                                                                                                      
GAGE_Corrected assembly size               4281487                                                                                                           4280722                                                                                                  
GAGE_Min correct contig                    200                                                                                                               200                                                                                                      
GAGE_Max correct contig                    74705                                                                                                             74705                                                                                                    
GAGE_Corrected N50                         17452 COUNT: 80                                                                                                   17625 COUNT: 78                                                                                          
