reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.550percent.contigs  | 96.6992192282       | 1.00021824147     | 157         | 3916      | 128       | None      | None      |
