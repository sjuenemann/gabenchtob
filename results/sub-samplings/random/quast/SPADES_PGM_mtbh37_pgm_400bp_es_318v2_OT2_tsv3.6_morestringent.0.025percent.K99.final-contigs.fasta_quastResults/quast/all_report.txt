All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.025percent.K99.final-contigs
#Contigs                                   2493                                                                                        
#Contigs (>= 0 bp)                         2493                                                                                        
#Contigs (>= 1000 bp)                      898                                                                                         
Largest contig                             7364                                                                                        
Total length                               2515797                                                                                     
Total length (>= 0 bp)                     2515797                                                                                     
Total length (>= 1000 bp)                  1382713                                                                                     
Reference length                           4411532                                                                                     
N50                                        1084                                                                                        
NG50                                       621                                                                                         
N75                                        759                                                                                         
NG75                                       None                                                                                        
L50                                        778                                                                                         
LG50                                       1939                                                                                        
L75                                        1476                                                                                        
LG75                                       None                                                                                        
#local misassemblies                       9                                                                                           
#misassemblies                             86                                                                                          
#misassembled contigs                      80                                                                                          
Misassembled contigs length                128900                                                                                      
Misassemblies inter-contig overlap         7868                                                                                        
#unaligned contigs                         1 + 18 part                                                                                 
Unaligned contigs length                   7031                                                                                        
#ambiguously mapped contigs                1                                                                                           
Extra bases in ambiguously mapped contigs  -787                                                                                        
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        56.869                                                                                      
Duplication ratio                          1.003                                                                                       
#genes                                     816 + 2422 part                                                                             
#predicted genes (unique)                  4970                                                                                        
#predicted genes (>= 0 bp)                 4976                                                                                        
#predicted genes (>= 300 bp)               2633                                                                                        
#predicted genes (>= 1500 bp)              9                                                                                           
#predicted genes (>= 3000 bp)              0                                                                                           
#mismatches                                1358                                                                                        
#indels                                    8270                                                                                        
Indels length                              8466                                                                                        
#mismatches per 100 kbp                    54.13                                                                                       
#indels per 100 kbp                        329.64                                                                                      
GC (%)                                     64.35                                                                                       
Reference GC (%)                           65.61                                                                                       
Average %IDY                               99.580                                                                                      
Largest alignment                          5687                                                                                        
NA50                                       1041                                                                                        
NGA50                                      609                                                                                         
NA75                                       742                                                                                         
NGA75                                      None                                                                                        
LA50                                       810                                                                                         
LGA50                                      2003                                                                                        
LA75                                       1528                                                                                        
LGA75                                      None                                                                                        
#Mis_misassemblies                         86                                                                                          
#Mis_relocations                           83                                                                                          
#Mis_translocations                        0                                                                                           
#Mis_inversions                            3                                                                                           
#Mis_misassembled contigs                  80                                                                                          
Mis_Misassembled contigs length            128900                                                                                      
#Mis_local misassemblies                   9                                                                                           
#Mis_short indels (<= 5 bp)                8269                                                                                        
#Mis_long indels (> 5 bp)                  1                                                                                           
#Una_fully unaligned contigs               1                                                                                           
Una_Fully unaligned length                 1814                                                                                        
#Una_partially unaligned contigs           18                                                                                          
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            9                                                                                           
Una_Partially unaligned length             5217                                                                                        
GAGE_Contigs #                             2493                                                                                        
GAGE_Min contig                            230                                                                                         
GAGE_Max contig                            7364                                                                                        
GAGE_N50                                   621 COUNT: 1939                                                                             
GAGE_Genome size                           4411532                                                                                     
GAGE_Assembly size                         2515797                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               1858414(42.13%)                                                                             
GAGE_Missing assembly bases                9489(0.38%)                                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                                    
GAGE_Duplicated reference bases            2868                                                                                        
GAGE_Compressed reference bases            44326                                                                                       
GAGE_Bad trim                              9473                                                                                        
GAGE_Avg idy                               99.61                                                                                       
GAGE_SNPs                                  797                                                                                         
GAGE_Indels < 5bp                          8314                                                                                        
GAGE_Indels >= 5                           11                                                                                          
GAGE_Inversions                            41                                                                                          
GAGE_Relocation                            23                                                                                          
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    2573                                                                                        
GAGE_Corrected assembly size               2511651                                                                                     
GAGE_Min correct contig                    201                                                                                         
GAGE_Max correct contig                    6413                                                                                        
GAGE_Corrected N50                         612 COUNT: 2000                                                                             
