All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.7percent_98.final
#Contigs (>= 0 bp)             7002                                                               
#Contigs (>= 1000 bp)          862                                                                
Total length (>= 0 bp)         6199586                                                            
Total length (>= 1000 bp)      5058626                                                            
#Contigs                       1456                                                               
Largest contig                 36757                                                              
Total length                   5308100                                                            
Reference length               5594470                                                            
GC (%)                         50.25                                                              
Reference GC (%)               50.48                                                              
N50                            8788                                                               
NG50                           7991                                                               
N75                            4291                                                               
NG75                           3667                                                               
#misassemblies                 2                                                                  
#local misassemblies           4                                                                  
#unaligned contigs             0 + 0 part                                                         
Unaligned contigs length       0                                                                  
Genome fraction (%)            93.405                                                             
Duplication ratio              1.002                                                              
#N's per 100 kbp               0.00                                                               
#mismatches per 100 kbp        1.51                                                               
#indels per 100 kbp            3.75                                                               
#genes                         4197 + 935 part                                                    
#predicted genes (unique)      6161                                                               
#predicted genes (>= 0 bp)     6172                                                               
#predicted genes (>= 300 bp)   4717                                                               
#predicted genes (>= 1500 bp)  554                                                                
#predicted genes (>= 3000 bp)  42                                                                 
Largest alignment              36757                                                              
NA50                           8721                                                               
NGA50                          7991                                                               
NA75                           4279                                                               
NGA75                          3655                                                               
