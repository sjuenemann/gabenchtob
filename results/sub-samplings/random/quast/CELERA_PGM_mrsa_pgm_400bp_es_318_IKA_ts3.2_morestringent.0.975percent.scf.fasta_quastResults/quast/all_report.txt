All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.975percent.scf
#Contigs                                   41                                                                       
#Contigs (>= 0 bp)                         41                                                                       
#Contigs (>= 1000 bp)                      41                                                                       
Largest contig                             21449                                                                    
Total length                               82497                                                                    
Total length (>= 0 bp)                     82497                                                                    
Total length (>= 1000 bp)                  82497                                                                    
Reference length                           2813862                                                                  
N50                                        1785                                                                     
NG50                                       None                                                                     
N75                                        1182                                                                     
NG75                                       None                                                                     
L50                                        8                                                                        
LG50                                       None                                                                     
L75                                        22                                                                       
LG75                                       None                                                                     
#local misassemblies                       0                                                                        
#misassemblies                             1                                                                        
#misassembled contigs                      1                                                                        
Misassembled contigs length                1072                                                                     
Misassemblies inter-contig overlap         0                                                                        
#unaligned contigs                         0 + 0 part                                                               
Unaligned contigs length                   0                                                                        
#ambiguously mapped contigs                3                                                                        
Extra bases in ambiguously mapped contigs  -3315                                                                    
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        2.634                                                                    
Duplication ratio                          1.068                                                                    
#genes                                     55 + 38 part                                                             
#predicted genes (unique)                  121                                                                      
#predicted genes (>= 0 bp)                 121                                                                      
#predicted genes (>= 300 bp)               66                                                                       
#predicted genes (>= 1500 bp)              9                                                                        
#predicted genes (>= 3000 bp)              1                                                                        
#mismatches                                10                                                                       
#indels                                    62                                                                       
Indels length                              62                                                                       
#mismatches per 100 kbp                    13.49                                                                    
#indels per 100 kbp                        83.65                                                                    
GC (%)                                     33.53                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               99.019                                                                   
Largest alignment                          21449                                                                    
NA50                                       1785                                                                     
NGA50                                      None                                                                     
NA75                                       1177                                                                     
NGA75                                      None                                                                     
LA50                                       8                                                                        
LGA50                                      None                                                                     
LA75                                       22                                                                       
LGA75                                      None                                                                     
#Mis_misassemblies                         1                                                                        
#Mis_relocations                           1                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  1                                                                        
Mis_Misassembled contigs length            1072                                                                     
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                62                                                                       
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               0                                                                        
Una_Fully unaligned length                 0                                                                        
#Una_partially unaligned contigs           0                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             0                                                                        
GAGE_Contigs #                             41                                                                       
GAGE_Min contig                            1004                                                                     
GAGE_Max contig                            21449                                                                    
GAGE_N50                                   0 COUNT: 0                                                               
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         82497                                                                    
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               2717162(96.56%)                                                          
GAGE_Missing assembly bases                15(0.02%)                                                                
GAGE_Missing assembly contigs              0(0.00%)                                                                 
GAGE_Duplicated reference bases            1008                                                                     
GAGE_Compressed reference bases            28961                                                                    
GAGE_Bad trim                              15                                                                       
GAGE_Avg idy                               99.89                                                                    
GAGE_SNPs                                  10                                                                       
GAGE_Indels < 5bp                          59                                                                       
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            1                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    40                                                                       
GAGE_Corrected assembly size               81340                                                                    
GAGE_Min correct contig                    917                                                                      
GAGE_Max correct contig                    21451                                                                    
GAGE_Corrected N50                         0 COUNT: 0                                                               
