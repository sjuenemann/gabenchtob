All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.45percent.K99.final-contigs
GAGE_Contigs #                   443                                                                        
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  316884                                                                     
GAGE_N50                         124344 COUNT: 15                                                           
GAGE_Genome size                 5594470                                                                    
GAGE_Assembly size               5381878                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     6828(0.12%)                                                                
GAGE_Missing assembly bases      63(0.00%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                   
GAGE_Duplicated reference bases  1897                                                                       
GAGE_Compressed reference bases  272623                                                                     
GAGE_Bad trim                    63                                                                         
GAGE_Avg idy                     99.99                                                                      
GAGE_SNPs                        216                                                                        
GAGE_Indels < 5bp                334                                                                        
GAGE_Indels >= 5                 13                                                                         
GAGE_Inversions                  0                                                                          
GAGE_Relocation                  9                                                                          
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          458                                                                        
GAGE_Corrected assembly size     5382761                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          224217                                                                     
GAGE_Corrected N50               95651 COUNT: 19                                                            
