All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.100percent.result
#Contigs (>= 0 bp)             2200                                                                          
#Contigs (>= 1000 bp)          1010                                                                          
Total length (>= 0 bp)         4150091                                                                       
Total length (>= 1000 bp)      3616475                                                                       
#Contigs                       2200                                                                          
Largest contig                 37588                                                                         
Total length                   4150091                                                                       
Reference length               4411532                                                                       
GC (%)                         65.17                                                                         
Reference GC (%)               65.61                                                                         
N50                            4064                                                                          
NG50                           3732                                                                          
N75                            2013                                                                          
NG75                           1612                                                                          
#misassemblies                 126                                                                           
#local misassemblies           13                                                                            
#unaligned contigs             5 + 18 part                                                                   
Unaligned contigs length       3881                                                                          
Genome fraction (%)            90.827                                                                        
Duplication ratio              1.035                                                                         
#N's per 100 kbp               17.35                                                                         
#mismatches per 100 kbp        35.69                                                                         
#indels per 100 kbp            119.12                                                                        
#genes                         2616 + 1372 part                                                              
#predicted genes (unique)      6343                                                                          
#predicted genes (>= 0 bp)     6346                                                                          
#predicted genes (>= 300 bp)   4171                                                                          
#predicted genes (>= 1500 bp)  200                                                                           
#predicted genes (>= 3000 bp)  6                                                                             
Largest alignment              27712                                                                         
NA50                           3822                                                                          
NGA50                          3500                                                                          
NA75                           1857                                                                          
NGA75                          1484                                                                          
