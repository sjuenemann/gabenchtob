All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.100percent.result
#Una_fully unaligned contigs      5                                                                             
Una_Fully unaligned length        2574                                                                          
#Una_partially unaligned contigs  18                                                                            
#Una_with misassembly             0                                                                             
#Una_both parts are significant   0                                                                             
Una_Partially unaligned length    1307                                                                          
#N's                              720                                                                           
