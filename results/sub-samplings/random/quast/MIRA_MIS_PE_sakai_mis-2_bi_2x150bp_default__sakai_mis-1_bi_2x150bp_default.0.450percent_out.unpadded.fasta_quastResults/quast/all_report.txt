All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.450percent_out.unpadded
#Contigs                                   195                                                                                                 
#Contigs (>= 0 bp)                         201                                                                                                 
#Contigs (>= 1000 bp)                      95                                                                                                  
Largest contig                             381520                                                                                              
Total length                               5576236                                                                                             
Total length (>= 0 bp)                     5577142                                                                                             
Total length (>= 1000 bp)                  5526906                                                                                             
Reference length                           5594470                                                                                             
N50                                        186697                                                                                              
NG50                                       186697                                                                                              
N75                                        135577                                                                                              
NG75                                       135577                                                                                              
L50                                        10                                                                                                  
LG50                                       10                                                                                                  
L75                                        19                                                                                                  
LG75                                       19                                                                                                  
#local misassemblies                       7                                                                                                   
#misassemblies                             77                                                                                                  
#misassembled contigs                      29                                                                                                  
Misassembled contigs length                3850728                                                                                             
Misassemblies inter-contig overlap         71279                                                                                               
#unaligned contigs                         3 + 1 part                                                                                          
Unaligned contigs length                   6746                                                                                                
#ambiguously mapped contigs                62                                                                                                  
Extra bases in ambiguously mapped contigs  -52315                                                                                              
#N's                                       17                                                                                                  
#N's per 100 kbp                           0.30                                                                                                
Genome fraction (%)                        98.594                                                                                              
Duplication ratio                          1.013                                                                                               
#genes                                     5305 + 89 part                                                                                      
#predicted genes (unique)                  5393                                                                                                
#predicted genes (>= 0 bp)                 5449                                                                                                
#predicted genes (>= 300 bp)               4628                                                                                                
#predicted genes (>= 1500 bp)              693                                                                                                 
#predicted genes (>= 3000 bp)              74                                                                                                  
#mismatches                                2483                                                                                                
#indels                                    121                                                                                                 
Indels length                              169                                                                                                 
#mismatches per 100 kbp                    45.02                                                                                               
#indels per 100 kbp                        2.19                                                                                                
GC (%)                                     50.44                                                                                               
Reference GC (%)                           50.48                                                                                               
Average %IDY                               98.512                                                                                              
Largest alignment                          314977                                                                                              
NA50                                       134663                                                                                              
NGA50                                      134663                                                                                              
NA75                                       65462                                                                                               
NGA75                                      65462                                                                                               
LA50                                       17                                                                                                  
LGA50                                      17                                                                                                  
LA75                                       30                                                                                                  
LGA75                                      30                                                                                                  
#Mis_misassemblies                         77                                                                                                  
#Mis_relocations                           74                                                                                                  
#Mis_translocations                        3                                                                                                   
#Mis_inversions                            0                                                                                                   
#Mis_misassembled contigs                  29                                                                                                  
Mis_Misassembled contigs length            3850728                                                                                             
#Mis_local misassemblies                   7                                                                                                   
#Mis_short indels (<= 5 bp)                118                                                                                                 
#Mis_long indels (> 5 bp)                  3                                                                                                   
#Una_fully unaligned contigs               3                                                                                                   
Una_Fully unaligned length                 6659                                                                                                
#Una_partially unaligned contigs           1                                                                                                   
#Una_with misassembly                      0                                                                                                   
#Una_both parts are significant            0                                                                                                   
Una_Partially unaligned length             87                                                                                                  
GAGE_Contigs #                             195                                                                                                 
GAGE_Min contig                            205                                                                                                 
GAGE_Max contig                            381520                                                                                              
GAGE_N50                                   186697 COUNT: 10                                                                                    
GAGE_Genome size                           5594470                                                                                             
GAGE_Assembly size                         5576236                                                                                             
GAGE_Chaff bases                           0                                                                                                   
GAGE_Missing reference bases               356(0.01%)                                                                                          
GAGE_Missing assembly bases                6757(0.12%)                                                                                         
GAGE_Missing assembly contigs              3(1.54%)                                                                                            
GAGE_Duplicated reference bases            91841                                                                                               
GAGE_Compressed reference bases            143675                                                                                              
GAGE_Bad trim                              92                                                                                                  
GAGE_Avg idy                               99.94                                                                                               
GAGE_SNPs                                  871                                                                                                 
GAGE_Indels < 5bp                          35                                                                                                  
GAGE_Indels >= 5                           6                                                                                                   
GAGE_Inversions                            33                                                                                                  
GAGE_Relocation                            16                                                                                                  
GAGE_Translocation                         4                                                                                                   
GAGE_Corrected contig #                    165                                                                                                 
GAGE_Corrected assembly size               5568520                                                                                             
GAGE_Min correct contig                    244                                                                                                 
GAGE_Max correct contig                    314977                                                                                              
GAGE_Corrected N50                         134663 COUNT: 17                                                                                    
