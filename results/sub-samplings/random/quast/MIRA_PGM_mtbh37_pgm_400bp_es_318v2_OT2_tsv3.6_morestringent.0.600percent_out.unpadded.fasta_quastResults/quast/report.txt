All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.600percent_out.unpadded
#Contigs (>= 0 bp)             583                                                                                  
#Contigs (>= 1000 bp)          157                                                                                  
Total length (>= 0 bp)         4525966                                                                              
Total length (>= 1000 bp)      4337522                                                                              
#Contigs                       537                                                                                  
Largest contig                 245160                                                                               
Total length                   4519565                                                                              
Reference length               4411532                                                                              
GC (%)                         65.20                                                                                
Reference GC (%)               65.61                                                                                
N50                            53846                                                                                
NG50                           54124                                                                                
N75                            27292                                                                                
NG75                           28360                                                                                
#misassemblies                 40                                                                                   
#local misassemblies           16                                                                                   
#unaligned contigs             3 + 3 part                                                                           
Unaligned contigs length       1285                                                                                 
Genome fraction (%)            98.321                                                                               
Duplication ratio              1.047                                                                                
#N's per 100 kbp               1.33                                                                                 
#mismatches per 100 kbp        13.21                                                                                
#indels per 100 kbp            14.55                                                                                
#genes                         3980 + 129 part                                                                      
#predicted genes (unique)      4667                                                                                 
#predicted genes (>= 0 bp)     4704                                                                                 
#predicted genes (>= 300 bp)   3839                                                                                 
#predicted genes (>= 1500 bp)  523                                                                                  
#predicted genes (>= 3000 bp)  69                                                                                   
Largest alignment              158816                                                                               
NA50                           46123                                                                                
NGA50                          49584                                                                                
NA75                           24305                                                                                
NGA75                          26387                                                                                
