All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.175percent.result
#Contigs (>= 0 bp)             402                                                                           
#Contigs (>= 1000 bp)          205                                                                           
Total length (>= 0 bp)         2783573                                                                       
Total length (>= 1000 bp)      2721517                                                                       
#Contigs                       402                                                                           
Largest contig                 72728                                                                         
Total length                   2783573                                                                       
Reference length               2813862                                                                       
GC (%)                         32.61                                                                         
Reference GC (%)               32.81                                                                         
N50                            19767                                                                         
NG50                           19602                                                                         
N75                            11146                                                                         
NG75                           11090                                                                         
#misassemblies                 20                                                                            
#local misassemblies           0                                                                             
#unaligned contigs             0 + 24 part                                                                   
Unaligned contigs length       990                                                                           
Genome fraction (%)            97.389                                                                        
Duplication ratio              1.012                                                                         
#N's per 100 kbp               2.55                                                                          
#mismatches per 100 kbp        2.88                                                                          
#indels per 100 kbp            21.06                                                                         
#genes                         2539 + 145 part                                                               
#predicted genes (unique)      3000                                                                          
#predicted genes (>= 0 bp)     3006                                                                          
#predicted genes (>= 300 bp)   2389                                                                          
#predicted genes (>= 1500 bp)  256                                                                           
#predicted genes (>= 3000 bp)  22                                                                            
Largest alignment              72718                                                                         
NA50                           19767                                                                         
NGA50                          19602                                                                         
NA75                           11094                                                                         
NGA75                          10973                                                                         
