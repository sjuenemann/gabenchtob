All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.800percent.contigs
#Mis_misassemblies               10                                                                                 
#Mis_relocations                 3                                                                                  
#Mis_translocations              2                                                                                  
#Mis_inversions                  5                                                                                  
#Mis_misassembled contigs        9                                                                                  
Mis_Misassembled contigs length  11341                                                                              
#Mis_local misassemblies         5                                                                                  
#mismatches                      44                                                                                 
#indels                          194                                                                                
#Mis_short indels (<= 5 bp)      193                                                                                
#Mis_long indels (> 5 bp)        1                                                                                  
Indels length                    289                                                                                
