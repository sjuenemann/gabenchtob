All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent_out.unpadded
#Mis_misassemblies               168                                                                  
#Mis_relocations                 159                                                                  
#Mis_translocations              9                                                                    
#Mis_inversions                  0                                                                    
#Mis_misassembled contigs        129                                                                  
Mis_Misassembled contigs length  3959256                                                              
#Mis_local misassemblies         25                                                                   
#mismatches                      1971                                                                 
#indels                          931                                                                  
#Mis_short indels (<= 5 bp)      928                                                                  
#Mis_long indels (> 5 bp)        3                                                                    
Indels length                    979                                                                  
