All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.45percent_out.unpadded
#Contigs                                   931                                                                 
#Contigs (>= 0 bp)                         978                                                                 
#Contigs (>= 1000 bp)                      125                                                                 
Largest contig                             439400                                                              
Total length                               5966058                                                             
Total length (>= 0 bp)                     5973621                                                             
Total length (>= 1000 bp)                  5588434                                                             
Reference length                           5594470                                                             
N50                                        189050                                                              
NG50                                       189270                                                              
N75                                        70693                                                               
NG75                                       80118                                                               
L50                                        11                                                                  
LG50                                       10                                                                  
L75                                        25                                                                  
LG75                                       21                                                                  
#local misassemblies                       18                                                                  
#misassemblies                             64                                                                  
#misassembled contigs                      43                                                                  
Misassembled contigs length                3850788                                                             
Misassemblies inter-contig overlap         58322                                                               
#unaligned contigs                         0 + 3 part                                                          
Unaligned contigs length                   241                                                                 
#ambiguously mapped contigs                83                                                                  
Extra bases in ambiguously mapped contigs  -61577                                                              
#N's                                       110                                                                 
#N's per 100 kbp                           1.84                                                                
Genome fraction (%)                        98.752                                                              
Duplication ratio                          1.079                                                               
#genes                                     5336 + 69 part                                                      
#predicted genes (unique)                  6459                                                                
#predicted genes (>= 0 bp)                 6537                                                                
#predicted genes (>= 300 bp)               4954                                                                
#predicted genes (>= 1500 bp)              682                                                                 
#predicted genes (>= 3000 bp)              70                                                                  
#mismatches                                1219                                                                
#indels                                    415                                                                 
Indels length                              431                                                                 
#mismatches per 100 kbp                    22.06                                                               
#indels per 100 kbp                        7.51                                                                
GC (%)                                     50.34                                                               
Reference GC (%)                           50.48                                                               
Average %IDY                               98.601                                                              
Largest alignment                          379059                                                              
NA50                                       104144                                                              
NGA50                                      111224                                                              
NA75                                       57422                                                               
NGA75                                      69328                                                               
LA50                                       16                                                                  
LGA50                                      14                                                                  
LA75                                       35                                                                  
LGA75                                      30                                                                  
#Mis_misassemblies                         64                                                                  
#Mis_relocations                           60                                                                  
#Mis_translocations                        4                                                                   
#Mis_inversions                            0                                                                   
#Mis_misassembled contigs                  43                                                                  
Mis_Misassembled contigs length            3850788                                                             
#Mis_local misassemblies                   18                                                                  
#Mis_short indels (<= 5 bp)                415                                                                 
#Mis_long indels (> 5 bp)                  0                                                                   
#Una_fully unaligned contigs               0                                                                   
Una_Fully unaligned length                 0                                                                   
#Una_partially unaligned contigs           3                                                                   
#Una_with misassembly                      0                                                                   
#Una_both parts are significant            0                                                                   
Una_Partially unaligned length             241                                                                 
GAGE_Contigs #                             931                                                                 
GAGE_Min contig                            202                                                                 
GAGE_Max contig                            439400                                                              
GAGE_N50                                   189270 COUNT: 10                                                    
GAGE_Genome size                           5594470                                                             
GAGE_Assembly size                         5966058                                                             
GAGE_Chaff bases                           0                                                                   
GAGE_Missing reference bases               107(0.00%)                                                          
GAGE_Missing assembly bases                275(0.00%)                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                            
GAGE_Duplicated reference bases            439280                                                              
GAGE_Compressed reference bases            111268                                                              
GAGE_Bad trim                              275                                                                 
GAGE_Avg idy                               99.97                                                               
GAGE_SNPs                                  362                                                                 
GAGE_Indels < 5bp                          351                                                                 
GAGE_Indels >= 5                           6                                                                   
GAGE_Inversions                            16                                                                  
GAGE_Relocation                            11                                                                  
GAGE_Translocation                         2                                                                   
GAGE_Corrected contig #                    160                                                                 
GAGE_Corrected assembly size               5594808                                                             
GAGE_Min correct contig                    358                                                                 
GAGE_Max correct contig                    243715                                                              
GAGE_Corrected N50                         104139 COUNT: 18                                                    
