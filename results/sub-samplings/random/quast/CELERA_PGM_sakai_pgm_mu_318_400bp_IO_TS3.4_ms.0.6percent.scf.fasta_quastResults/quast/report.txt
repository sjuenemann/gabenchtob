All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.6percent.scf
#Contigs (>= 0 bp)             300                                                         
#Contigs (>= 1000 bp)          300                                                         
Total length (>= 0 bp)         5274605                                                     
Total length (>= 1000 bp)      5274605                                                     
#Contigs                       300                                                         
Largest contig                 244099                                                      
Total length                   5274605                                                     
Reference length               5594470                                                     
GC (%)                         50.32                                                       
Reference GC (%)               50.48                                                       
N50                            40469                                                       
NG50                           38450                                                       
N75                            22217                                                       
NG75                           19094                                                       
#misassemblies                 2                                                           
#local misassemblies           9                                                           
#unaligned contigs             0 + 0 part                                                  
Unaligned contigs length       0                                                           
Genome fraction (%)            92.880                                                      
Duplication ratio              1.011                                                       
#N's per 100 kbp               0.00                                                        
#mismatches per 100 kbp        1.98                                                        
#indels per 100 kbp            4.73                                                        
#genes                         4855 + 223 part                                             
#predicted genes (unique)      5292                                                        
#predicted genes (>= 0 bp)     5299                                                        
#predicted genes (>= 300 bp)   4472                                                        
#predicted genes (>= 1500 bp)  642                                                         
#predicted genes (>= 3000 bp)  64                                                          
Largest alignment              243724                                                      
NA50                           40290                                                       
NGA50                          38450                                                       
NA75                           22217                                                       
NGA75                          19094                                                       
