All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.925percent.contigs
#Contigs                                   623                                                                                
#Contigs (>= 0 bp)                         1165                                                                               
#Contigs (>= 1000 bp)                      465                                                                                
Largest contig                             28435                                                                              
Total length                               2717651                                                                            
Total length (>= 0 bp)                     2786149                                                                            
Total length (>= 1000 bp)                  2643269                                                                            
Reference length                           2813862                                                                            
N50                                        7941                                                                               
NG50                                       7742                                                                               
N75                                        4522                                                                               
NG75                                       4289                                                                               
L50                                        110                                                                                
LG50                                       116                                                                                
L75                                        221                                                                                
LG75                                       238                                                                                
#local misassemblies                       2                                                                                  
#misassemblies                             14                                                                                 
#misassembled contigs                      13                                                                                 
Misassembled contigs length                12148                                                                              
Misassemblies inter-contig overlap         677                                                                                
#unaligned contigs                         1 + 3 part                                                                         
Unaligned contigs length                   348                                                                                
#ambiguously mapped contigs                12                                                                                 
Extra bases in ambiguously mapped contigs  -6221                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        96.297                                                                             
Duplication ratio                          1.001                                                                              
#genes                                     2252 + 426 part                                                                    
#predicted genes (unique)                  3006                                                                               
#predicted genes (>= 0 bp)                 3006                                                                               
#predicted genes (>= 300 bp)               2410                                                                               
#predicted genes (>= 1500 bp)              243                                                                                
#predicted genes (>= 3000 bp)              19                                                                                 
#mismatches                                75                                                                                 
#indels                                    292                                                                                
Indels length                              383                                                                                
#mismatches per 100 kbp                    2.77                                                                               
#indels per 100 kbp                        10.78                                                                              
GC (%)                                     32.65                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               99.738                                                                             
Largest alignment                          28435                                                                              
NA50                                       7904                                                                               
NGA50                                      7710                                                                               
NA75                                       4512                                                                               
NGA75                                      4289                                                                               
LA50                                       110                                                                                
LGA50                                      117                                                                                
LA75                                       222                                                                                
LGA75                                      238                                                                                
#Mis_misassemblies                         14                                                                                 
#Mis_relocations                           5                                                                                  
#Mis_translocations                        1                                                                                  
#Mis_inversions                            8                                                                                  
#Mis_misassembled contigs                  13                                                                                 
Mis_Misassembled contigs length            12148                                                                              
#Mis_local misassemblies                   2                                                                                  
#Mis_short indels (<= 5 bp)                290                                                                                
#Mis_long indels (> 5 bp)                  2                                                                                  
#Una_fully unaligned contigs               1                                                                                  
Una_Fully unaligned length                 237                                                                                
#Una_partially unaligned contigs           3                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             111                                                                                
GAGE_Contigs #                             623                                                                                
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            28435                                                                              
GAGE_N50                                   7742 COUNT: 116                                                                    
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2717651                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               69443(2.47%)                                                                       
GAGE_Missing assembly bases                286(0.01%)                                                                         
GAGE_Missing assembly contigs              1(0.16%)                                                                           
GAGE_Duplicated reference bases            2276                                                                               
GAGE_Compressed reference bases            30608                                                                              
GAGE_Bad trim                              49                                                                                 
GAGE_Avg idy                               99.99                                                                              
GAGE_SNPs                                  34                                                                                 
GAGE_Indels < 5bp                          321                                                                                
GAGE_Indels >= 5                           3                                                                                  
GAGE_Inversions                            3                                                                                  
GAGE_Relocation                            2                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    612                                                                                
GAGE_Corrected assembly size               2714949                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    28437                                                                              
GAGE_Corrected N50                         7626 COUNT: 117                                                                    
