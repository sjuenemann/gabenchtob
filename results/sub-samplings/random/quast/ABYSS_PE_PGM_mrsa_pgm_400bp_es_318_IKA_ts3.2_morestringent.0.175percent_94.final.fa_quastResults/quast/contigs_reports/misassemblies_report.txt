All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent_94.final
#Mis_misassemblies               1                                                                               
#Mis_relocations                 1                                                                               
#Mis_translocations              0                                                                               
#Mis_inversions                  0                                                                               
#Mis_misassembled contigs        1                                                                               
Mis_Misassembled contigs length  8571                                                                            
#Mis_local misassemblies         1                                                                               
#mismatches                      39                                                                              
#indels                          241                                                                             
#Mis_short indels (<= 5 bp)      241                                                                             
#Mis_long indels (> 5 bp)        0                                                                               
Indels length                    245                                                                             
