All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent_94.final
#Contigs (>= 0 bp)             4626                                                                            
#Contigs (>= 1000 bp)          427                                                                             
Total length (>= 0 bp)         3307357                                                                         
Total length (>= 1000 bp)      2657111                                                                         
#Contigs                       616                                                                             
Largest contig                 51391                                                                           
Total length                   2749441                                                                         
Reference length               2813862                                                                         
GC (%)                         32.58                                                                           
Reference GC (%)               32.81                                                                           
N50                            9910                                                                            
NG50                           9746                                                                            
N75                            4534                                                                            
NG75                           4302                                                                            
#misassemblies                 1                                                                               
#local misassemblies           1                                                                               
#unaligned contigs             0 + 0 part                                                                      
Unaligned contigs length       0                                                                               
Genome fraction (%)            97.210                                                                          
Duplication ratio              1.004                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        1.43                                                                            
#indels per 100 kbp            8.81                                                                            
#genes                         2324 + 346 part                                                                 
#predicted genes (unique)      3001                                                                            
#predicted genes (>= 0 bp)     3002                                                                            
#predicted genes (>= 300 bp)   2411                                                                            
#predicted genes (>= 1500 bp)  239                                                                             
#predicted genes (>= 3000 bp)  19                                                                              
Largest alignment              51391                                                                           
NA50                           9910                                                                            
NGA50                          9746                                                                            
NA75                           4525                                                                            
NGA75                          4272                                                                            
