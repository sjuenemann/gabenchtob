All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.15percent.result
#Contigs (>= 0 bp)             745                                                          
#Contigs (>= 1000 bp)          263                                                          
Total length (>= 0 bp)         5403141                                                      
Total length (>= 1000 bp)      5221132                                                      
#Contigs                       745                                                          
Largest contig                 144652                                                       
Total length                   5403141                                                      
Reference length               5594470                                                      
GC (%)                         50.30                                                        
Reference GC (%)               50.48                                                        
N50                            45239                                                        
NG50                           44536                                                        
N75                            24909                                                        
NG75                           20259                                                        
#misassemblies                 6                                                            
#local misassemblies           8                                                            
#unaligned contigs             3 + 7 part                                                   
Unaligned contigs length       2203                                                         
Genome fraction (%)            94.162                                                       
Duplication ratio              1.008                                                        
#N's per 100 kbp               14.77                                                        
#mismatches per 100 kbp        3.32                                                         
#indels per 100 kbp            12.23                                                        
#genes                         4855 + 269 part                                              
#predicted genes (unique)      5821                                                         
#predicted genes (>= 0 bp)     5828                                                         
#predicted genes (>= 300 bp)   4629                                                         
#predicted genes (>= 1500 bp)  608                                                          
#predicted genes (>= 3000 bp)  58                                                           
Largest alignment              144652                                                       
NA50                           45239                                                        
NGA50                          44064                                                        
NA75                           24909                                                        
NGA75                          20259                                                        
