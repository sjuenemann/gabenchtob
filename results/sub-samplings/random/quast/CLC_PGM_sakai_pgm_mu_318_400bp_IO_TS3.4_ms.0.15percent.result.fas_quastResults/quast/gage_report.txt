All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.15percent.result
GAGE_Contigs #                   745                                                          
GAGE_Min contig                  200                                                          
GAGE_Max contig                  144652                                                       
GAGE_N50                         44536 COUNT: 41                                              
GAGE_Genome size                 5594470                                                      
GAGE_Assembly size               5403141                                                      
GAGE_Chaff bases                 0                                                            
GAGE_Missing reference bases     34625(0.62%)                                                 
GAGE_Missing assembly bases      2377(0.04%)                                                  
GAGE_Missing assembly contigs    3(0.40%)                                                     
GAGE_Duplicated reference bases  50519                                                        
GAGE_Compressed reference bases  242657                                                       
GAGE_Bad trim                    1230                                                         
GAGE_Avg idy                     99.98                                                        
GAGE_SNPs                        142                                                          
GAGE_Indels < 5bp                619                                                          
GAGE_Indels >= 5                 4                                                            
GAGE_Inversions                  1                                                            
GAGE_Relocation                  7                                                            
GAGE_Translocation               0                                                            
GAGE_Corrected contig #          582                                                          
GAGE_Corrected assembly size     5351463                                                      
GAGE_Min correct contig          201                                                          
GAGE_Max correct contig          144661                                                       
GAGE_Corrected N50               42217 COUNT: 44                                              
