All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.025percent.scf
#Contigs                                   113                                                                           
#Contigs (>= 0 bp)                         113                                                                           
#Contigs (>= 1000 bp)                      113                                                                           
Largest contig                             2439                                                                          
Total length                               139966                                                                        
Total length (>= 0 bp)                     139966                                                                        
Total length (>= 1000 bp)                  139966                                                                        
Reference length                           4411532                                                                       
N50                                        1217                                                                          
NG50                                       None                                                                          
N75                                        1085                                                                          
NG75                                       None                                                                          
L50                                        49                                                                            
LG50                                       None                                                                          
L75                                        80                                                                            
LG75                                       None                                                                          
#local misassemblies                       1                                                                             
#misassemblies                             0                                                                             
#misassembled contigs                      0                                                                             
Misassembled contigs length                0                                                                             
Misassemblies inter-contig overlap         105                                                                           
#unaligned contigs                         0 + 0 part                                                                    
Unaligned contigs length                   0                                                                             
#ambiguously mapped contigs                0                                                                             
Extra bases in ambiguously mapped contigs  0                                                                             
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        3.176                                                                         
Duplication ratio                          1.000                                                                         
#genes                                     52 + 165 part                                                                 
#predicted genes (unique)                  239                                                                           
#predicted genes (>= 0 bp)                 239                                                                           
#predicted genes (>= 300 bp)               153                                                                           
#predicted genes (>= 1500 bp)              0                                                                             
#predicted genes (>= 3000 bp)              0                                                                             
#mismatches                                9                                                                             
#indels                                    130                                                                           
Indels length                              132                                                                           
#mismatches per 100 kbp                    6.42                                                                          
#indels per 100 kbp                        92.80                                                                         
GC (%)                                     62.61                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.879                                                                        
Largest alignment                          2439                                                                          
NA50                                       1217                                                                          
NGA50                                      None                                                                          
NA75                                       1085                                                                          
NGA75                                      None                                                                          
LA50                                       49                                                                            
LGA50                                      None                                                                          
LA75                                       80                                                                            
LGA75                                      None                                                                          
#Mis_misassemblies                         0                                                                             
#Mis_relocations                           0                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  0                                                                             
Mis_Misassembled contigs length            0                                                                             
#Mis_local misassemblies                   1                                                                             
#Mis_short indels (<= 5 bp)                130                                                                           
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           0                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             0                                                                             
GAGE_Contigs #                             113                                                                           
GAGE_Min contig                            1000                                                                          
GAGE_Max contig                            2439                                                                          
GAGE_N50                                   0 COUNT: 0                                                                    
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         139966                                                                        
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               4268753(96.76%)                                                               
GAGE_Missing assembly bases                5(0.00%)                                                                      
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            0                                                                             
GAGE_Compressed reference bases            2793                                                                          
GAGE_Bad trim                              5                                                                             
GAGE_Avg idy                               99.90                                                                         
GAGE_SNPs                                  8                                                                             
GAGE_Indels < 5bp                          132                                                                           
GAGE_Indels >= 5                           0                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            1                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    114                                                                           
GAGE_Corrected assembly size               140090                                                                        
GAGE_Min correct contig                    529                                                                           
GAGE_Max correct contig                    2440                                                                          
GAGE_Corrected N50                         0 COUNT: 0                                                                    
