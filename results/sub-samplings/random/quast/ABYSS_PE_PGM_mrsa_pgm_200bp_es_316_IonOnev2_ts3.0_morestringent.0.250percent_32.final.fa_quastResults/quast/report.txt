All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.250percent_32.final
#Contigs (>= 0 bp)             5692                                                                                 
#Contigs (>= 1000 bp)          243                                                                                  
Total length (>= 0 bp)         2980524                                                                              
Total length (>= 1000 bp)      2673662                                                                              
#Contigs                       367                                                                                  
Largest contig                 75386                                                                                
Total length                   2724772                                                                              
Reference length               2813862                                                                              
GC (%)                         32.59                                                                                
Reference GC (%)               32.81                                                                                
N50                            18740                                                                                
NG50                           17777                                                                                
N75                            10352                                                                                
NG75                           9665                                                                                 
#misassemblies                 1                                                                                    
#local misassemblies           1                                                                                    
#unaligned contigs             0 + 0 part                                                                           
Unaligned contigs length       0                                                                                    
Genome fraction (%)            96.564                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        1.77                                                                                 
#indels per 100 kbp            8.50                                                                                 
#genes                         2472 + 148 part                                                                      
#predicted genes (unique)      2784                                                                                 
#predicted genes (>= 0 bp)     2784                                                                                 
#predicted genes (>= 300 bp)   2320                                                                                 
#predicted genes (>= 1500 bp)  277                                                                                  
#predicted genes (>= 3000 bp)  22                                                                                   
Largest alignment              75386                                                                                
NA50                           18740                                                                                
NGA50                          17777                                                                                
NA75                           10161                                                                                
NGA75                          9552                                                                                 
