All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent.K99.final-contigs
#Mis_misassemblies               2                                                                                      
#Mis_relocations                 2                                                                                      
#Mis_translocations              0                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        2                                                                                      
Mis_Misassembled contigs length  93420                                                                                  
#Mis_local misassemblies         10                                                                                     
#mismatches                      183                                                                                    
#indels                          344                                                                                    
#Mis_short indels (<= 5 bp)      343                                                                                    
#Mis_long indels (> 5 bp)        1                                                                                      
Indels length                    363                                                                                    
