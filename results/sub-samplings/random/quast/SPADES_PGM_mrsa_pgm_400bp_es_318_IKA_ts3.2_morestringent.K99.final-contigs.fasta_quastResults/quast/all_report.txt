All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.K99.final-contigs
#Contigs                                   114                                                                       
#Contigs (>= 0 bp)                         369                                                                       
#Contigs (>= 1000 bp)                      64                                                                        
Largest contig                             175229                                                                    
Total length                               2782849                                                                   
Total length (>= 0 bp)                     2818681                                                                   
Total length (>= 1000 bp)                  2760126                                                                   
Reference length                           2813862                                                                   
N50                                        83047                                                                     
NG50                                       80139                                                                     
N75                                        42741                                                                     
NG75                                       42741                                                                     
L50                                        11                                                                        
LG50                                       12                                                                        
L75                                        23                                                                        
LG75                                       23                                                                        
#local misassemblies                       7                                                                         
#misassemblies                             2                                                                         
#misassembled contigs                      2                                                                         
Misassembled contigs length                74584                                                                     
Misassemblies inter-contig overlap         2064                                                                      
#unaligned contigs                         1 + 0 part                                                                
Unaligned contigs length                   1144                                                                      
#ambiguously mapped contigs                15                                                                        
Extra bases in ambiguously mapped contigs  -9418                                                                     
#N's                                       0                                                                         
#N's per 100 kbp                           0.00                                                                      
Genome fraction (%)                        98.382                                                                    
Duplication ratio                          1.002                                                                     
#genes                                     2659 + 41 part                                                            
#predicted genes (unique)                  2669                                                                      
#predicted genes (>= 0 bp)                 2673                                                                      
#predicted genes (>= 300 bp)               2297                                                                      
#predicted genes (>= 1500 bp)              295                                                                       
#predicted genes (>= 3000 bp)              29                                                                        
#mismatches                                180                                                                       
#indels                                    155                                                                       
Indels length                              239                                                                       
#mismatches per 100 kbp                    6.50                                                                      
#indels per 100 kbp                        5.60                                                                      
GC (%)                                     32.68                                                                     
Reference GC (%)                           32.81                                                                     
Average %IDY                               98.738                                                                    
Largest alignment                          175229                                                                    
NA50                                       83037                                                                     
NGA50                                      80139                                                                     
NA75                                       42741                                                                     
NGA75                                      39839                                                                     
LA50                                       11                                                                        
LGA50                                      12                                                                        
LA75                                       23                                                                        
LGA75                                      24                                                                        
#Mis_misassemblies                         2                                                                         
#Mis_relocations                           2                                                                         
#Mis_translocations                        0                                                                         
#Mis_inversions                            0                                                                         
#Mis_misassembled contigs                  2                                                                         
Mis_Misassembled contigs length            74584                                                                     
#Mis_local misassemblies                   7                                                                         
#Mis_short indels (<= 5 bp)                151                                                                       
#Mis_long indels (> 5 bp)                  4                                                                         
#Una_fully unaligned contigs               1                                                                         
Una_Fully unaligned length                 1144                                                                      
#Una_partially unaligned contigs           0                                                                         
#Una_with misassembly                      0                                                                         
#Una_both parts are significant            0                                                                         
Una_Partially unaligned length             0                                                                         
GAGE_Contigs #                             114                                                                       
GAGE_Min contig                            202                                                                       
GAGE_Max contig                            175229                                                                    
GAGE_N50                                   80139 COUNT: 12                                                           
GAGE_Genome size                           2813862                                                                   
GAGE_Assembly size                         2782849                                                                   
GAGE_Chaff bases                           0                                                                         
GAGE_Missing reference bases               5820(0.21%)                                                               
GAGE_Missing assembly bases                1332(0.05%)                                                               
GAGE_Missing assembly contigs              1(0.88%)                                                                  
GAGE_Duplicated reference bases            1276                                                                      
GAGE_Compressed reference bases            37557                                                                     
GAGE_Bad trim                              188                                                                       
GAGE_Avg idy                               99.98                                                                     
GAGE_SNPs                                  74                                                                        
GAGE_Indels < 5bp                          133                                                                       
GAGE_Indels >= 5                           7                                                                         
GAGE_Inversions                            0                                                                         
GAGE_Relocation                            2                                                                         
GAGE_Translocation                         0                                                                         
GAGE_Corrected contig #                    121                                                                       
GAGE_Corrected assembly size               2782563                                                                   
GAGE_Min correct contig                    202                                                                       
GAGE_Max correct contig                    175236                                                                    
GAGE_Corrected N50                         74437 COUNT: 13                                                           
