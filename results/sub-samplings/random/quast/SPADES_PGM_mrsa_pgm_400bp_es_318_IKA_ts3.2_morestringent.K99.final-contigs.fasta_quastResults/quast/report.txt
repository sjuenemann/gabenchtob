All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.K99.final-contigs
#Contigs (>= 0 bp)             369                                                                       
#Contigs (>= 1000 bp)          64                                                                        
Total length (>= 0 bp)         2818681                                                                   
Total length (>= 1000 bp)      2760126                                                                   
#Contigs                       114                                                                       
Largest contig                 175229                                                                    
Total length                   2782849                                                                   
Reference length               2813862                                                                   
GC (%)                         32.68                                                                     
Reference GC (%)               32.81                                                                     
N50                            83047                                                                     
NG50                           80139                                                                     
N75                            42741                                                                     
NG75                           42741                                                                     
#misassemblies                 2                                                                         
#local misassemblies           7                                                                         
#unaligned contigs             1 + 0 part                                                                
Unaligned contigs length       1144                                                                      
Genome fraction (%)            98.382                                                                    
Duplication ratio              1.002                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        6.50                                                                      
#indels per 100 kbp            5.60                                                                      
#genes                         2659 + 41 part                                                            
#predicted genes (unique)      2669                                                                      
#predicted genes (>= 0 bp)     2673                                                                      
#predicted genes (>= 300 bp)   2297                                                                      
#predicted genes (>= 1500 bp)  295                                                                       
#predicted genes (>= 3000 bp)  29                                                                        
Largest alignment              175229                                                                    
NA50                           83037                                                                     
NGA50                          80139                                                                     
NA75                           42741                                                                     
NGA75                          39839                                                                     
