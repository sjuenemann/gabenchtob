All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.500percent.scf
#Mis_misassemblies               6                                                                             
#Mis_relocations                 4                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  2                                                                             
#Mis_misassembled contigs        6                                                                             
Mis_Misassembled contigs length  28088                                                                         
#Mis_local misassemblies         5                                                                             
#mismatches                      228                                                                           
#indels                          574                                                                           
#Mis_short indels (<= 5 bp)      572                                                                           
#Mis_long indels (> 5 bp)        2                                                                             
Indels length                    620                                                                           
