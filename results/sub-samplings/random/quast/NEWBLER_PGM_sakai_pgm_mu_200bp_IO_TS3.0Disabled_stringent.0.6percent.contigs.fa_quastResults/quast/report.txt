All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.6percent.contigs
#Contigs (>= 0 bp)             456                                                                         
#Contigs (>= 1000 bp)          163                                                                         
Total length (>= 0 bp)         5332351                                                                     
Total length (>= 1000 bp)      5236041                                                                     
#Contigs                       350                                                                         
Largest contig                 365724                                                                      
Total length                   5317687                                                                     
Reference length               5594470                                                                     
GC (%)                         50.28                                                                       
Reference GC (%)               50.48                                                                       
N50                            105952                                                                      
NG50                           102764                                                                      
N75                            42839                                                                       
NG75                           39500                                                                       
#misassemblies                 5                                                                           
#local misassemblies           4                                                                           
#unaligned contigs             0 + 1 part                                                                  
Unaligned contigs length       64                                                                          
Genome fraction (%)            93.512                                                                      
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        1.24                                                                        
#indels per 100 kbp            19.21                                                                       
#genes                         4904 + 179 part                                                             
#predicted genes (unique)      5703                                                                        
#predicted genes (>= 0 bp)     5703                                                                        
#predicted genes (>= 300 bp)   4650                                                                        
#predicted genes (>= 1500 bp)  572                                                                         
#predicted genes (>= 3000 bp)  51                                                                          
Largest alignment              313732                                                                      
NA50                           105952                                                                      
NGA50                          102764                                                                      
NA75                           42839                                                                       
NGA75                          38559                                                                       
