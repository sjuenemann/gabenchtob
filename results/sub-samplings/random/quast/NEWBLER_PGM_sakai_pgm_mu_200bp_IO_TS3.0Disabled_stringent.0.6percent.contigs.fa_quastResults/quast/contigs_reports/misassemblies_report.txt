All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.6percent.contigs
#Mis_misassemblies               5                                                                           
#Mis_relocations                 3                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  2                                                                           
#Mis_misassembled contigs        5                                                                           
Mis_Misassembled contigs length  409033                                                                      
#Mis_local misassemblies         4                                                                           
#mismatches                      65                                                                          
#indels                          1005                                                                        
#Mis_short indels (<= 5 bp)      1004                                                                        
#Mis_long indels (> 5 bp)        1                                                                           
Indels length                    1052                                                                        
