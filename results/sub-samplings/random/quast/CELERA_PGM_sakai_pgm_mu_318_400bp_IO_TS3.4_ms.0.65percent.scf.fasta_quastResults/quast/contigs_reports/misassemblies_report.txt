All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.65percent.scf
#Mis_misassemblies               1                                                            
#Mis_relocations                 1                                                            
#Mis_translocations              0                                                            
#Mis_inversions                  0                                                            
#Mis_misassembled contigs        1                                                            
Mis_Misassembled contigs length  134815                                                       
#Mis_local misassemblies         6                                                            
#mismatches                      75                                                           
#indels                          248                                                          
#Mis_short indels (<= 5 bp)      248                                                          
#Mis_long indels (> 5 bp)        0                                                            
Indels length                    252                                                          
