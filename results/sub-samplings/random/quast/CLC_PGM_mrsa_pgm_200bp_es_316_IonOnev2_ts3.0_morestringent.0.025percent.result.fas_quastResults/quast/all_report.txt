All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.025percent.result
#Contigs                                   2793                                                                          
#Contigs (>= 0 bp)                         2793                                                                          
#Contigs (>= 1000 bp)                      814                                                                           
Largest contig                             8930                                                                          
Total length                               2485480                                                                       
Total length (>= 0 bp)                     2485480                                                                       
Total length (>= 1000 bp)                  1487932                                                                       
Reference length                           2813862                                                                       
N50                                        1252                                                                          
NG50                                       1082                                                                          
N75                                        652                                                                           
NG75                                       483                                                                           
L50                                        596                                                                           
LG50                                       737                                                                           
L75                                        1278                                                                          
LG75                                       1716                                                                          
#local misassemblies                       13                                                                            
#misassemblies                             475                                                                           
#misassembled contigs                      416                                                                           
Misassembled contigs length                595576                                                                        
Misassemblies inter-contig overlap         9003                                                                          
#unaligned contigs                         6 + 294 part                                                                  
Unaligned contigs length                   18927                                                                         
#ambiguously mapped contigs                8                                                                             
Extra bases in ambiguously mapped contigs  -6724                                                                         
#N's                                       1177                                                                          
#N's per 100 kbp                           47.36                                                                         
Genome fraction (%)                        82.815                                                                        
Duplication ratio                          1.059                                                                         
#genes                                     886 + 1636 part                                                               
#predicted genes (unique)                  5361                                                                          
#predicted genes (>= 0 bp)                 5365                                                                          
#predicted genes (>= 300 bp)               2394                                                                          
#predicted genes (>= 1500 bp)              10                                                                            
#predicted genes (>= 3000 bp)              0                                                                             
#mismatches                                3269                                                                          
#indels                                    9783                                                                          
Indels length                              10611                                                                         
#mismatches per 100 kbp                    140.28                                                                        
#indels per 100 kbp                        419.82                                                                        
GC (%)                                     32.76                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.292                                                                        
Largest alignment                          8601                                                                          
NA50                                       1113                                                                          
NGA50                                      939                                                                           
NA75                                       565                                                                           
NGA75                                      415                                                                           
LA50                                       676                                                                           
LGA50                                      836                                                                           
LA75                                       1462                                                                          
LGA75                                      1969                                                                          
#Mis_misassemblies                         475                                                                           
#Mis_relocations                           225                                                                           
#Mis_translocations                        1                                                                             
#Mis_inversions                            249                                                                           
#Mis_misassembled contigs                  416                                                                           
Mis_Misassembled contigs length            595576                                                                        
#Mis_local misassemblies                   13                                                                            
#Mis_short indels (<= 5 bp)                9774                                                                          
#Mis_long indels (> 5 bp)                  9                                                                             
#Una_fully unaligned contigs               6                                                                             
Una_Fully unaligned length                 3148                                                                          
#Una_partially unaligned contigs           294                                                                           
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            6                                                                             
Una_Partially unaligned length             15779                                                                         
GAGE_Contigs #                             2793                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            8930                                                                          
GAGE_N50                                   1082 COUNT: 737                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2485480                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               437050(15.53%)                                                                
GAGE_Missing assembly bases                24277(0.98%)                                                                  
GAGE_Missing assembly contigs              5(0.18%)                                                                      
GAGE_Duplicated reference bases            44896                                                                         
GAGE_Compressed reference bases            42194                                                                         
GAGE_Bad trim                              21488                                                                         
GAGE_Avg idy                               99.38                                                                         
GAGE_SNPs                                  2811                                                                          
GAGE_Indels < 5bp                          9313                                                                          
GAGE_Indels >= 5                           14                                                                            
GAGE_Inversions                            180                                                                           
GAGE_Relocation                            54                                                                            
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    2922                                                                          
GAGE_Corrected assembly size               2413205                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    8605                                                                          
GAGE_Corrected N50                         948 COUNT: 836                                                                
