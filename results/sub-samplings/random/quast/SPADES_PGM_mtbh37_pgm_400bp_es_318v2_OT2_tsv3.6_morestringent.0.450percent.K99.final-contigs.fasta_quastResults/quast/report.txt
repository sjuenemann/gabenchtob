All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.450percent.K99.final-contigs
#Contigs (>= 0 bp)             204                                                                                         
#Contigs (>= 1000 bp)          155                                                                                         
Total length (>= 0 bp)         4286959                                                                                     
Total length (>= 1000 bp)      4267690                                                                                     
#Contigs                       182                                                                                         
Largest contig                 151389                                                                                      
Total length                   4284418                                                                                     
Reference length               4411532                                                                                     
GC (%)                         65.38                                                                                       
Reference GC (%)               65.61                                                                                       
N50                            52850                                                                                       
NG50                           50747                                                                                       
N75                            26023                                                                                       
NG75                           24959                                                                                       
#misassemblies                 12                                                                                          
#local misassemblies           25                                                                                          
#unaligned contigs             0 + 0 part                                                                                  
Unaligned contigs length       0                                                                                           
Genome fraction (%)            96.830                                                                                      
Duplication ratio              1.001                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        9.04                                                                                        
#indels per 100 kbp            42.63                                                                                       
#genes                         3916 + 130 part                                                                             
#predicted genes (unique)      4593                                                                                        
#predicted genes (>= 0 bp)     4600                                                                                        
#predicted genes (>= 300 bp)   3831                                                                                        
#predicted genes (>= 1500 bp)  433                                                                                         
#predicted genes (>= 3000 bp)  38                                                                                          
Largest alignment              151386                                                                                      
NA50                           50747                                                                                       
NGA50                          46554                                                                                       
NA75                           25803                                                                                       
NGA75                          24602                                                                                       
