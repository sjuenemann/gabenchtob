All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.100percent_out.unpadded
#Mis_misassemblies               23                                                                                                
#Mis_relocations                 23                                                                                                
#Mis_translocations              0                                                                                                 
#Mis_inversions                  0                                                                                                 
#Mis_misassembled contigs        19                                                                                                
Mis_Misassembled contigs length  120353                                                                                            
#Mis_local misassemblies         7                                                                                                 
#mismatches                      483                                                                                               
#indels                          29                                                                                                
#Mis_short indels (<= 5 bp)      26                                                                                                
#Mis_long indels (> 5 bp)        3                                                                                                 
Indels length                    72                                                                                                
