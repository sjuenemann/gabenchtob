All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.600percent_out.unpadded
#Contigs                                   4312                                                                            
#Contigs (>= 0 bp)                         4446                                                                            
#Contigs (>= 1000 bp)                      543                                                                             
Largest contig                             305684                                                                          
Total length                               5741875                                                                         
Total length (>= 0 bp)                     5761050                                                                         
Total length (>= 1000 bp)                  3656375                                                                         
Reference length                           2813862                                                                         
N50                                        5357                                                                            
NG50                                       112043                                                                          
N75                                        685                                                                             
NG75                                       48324                                                                           
L50                                        59                                                                              
LG50                                       10                                                                              
L75                                        1352                                                                            
LG75                                       19                                                                              
#local misassemblies                       22                                                                              
#misassemblies                             164                                                                             
#misassembled contigs                      157                                                                             
Misassembled contigs length                610955                                                                          
Misassemblies inter-contig overlap         25503                                                                           
#unaligned contigs                         8 + 84 part                                                                     
Unaligned contigs length                   9266                                                                            
#ambiguously mapped contigs                107                                                                             
Extra bases in ambiguously mapped contigs  -64562                                                                          
#N's                                       1271                                                                            
#N's per 100 kbp                           22.14                                                                           
Genome fraction (%)                        99.893                                                                          
Duplication ratio                          2.026                                                                           
#genes                                     2699 + 27 part                                                                  
#predicted genes (unique)                  9312                                                                            
#predicted genes (>= 0 bp)                 9584                                                                            
#predicted genes (>= 300 bp)               4356                                                                            
#predicted genes (>= 1500 bp)              303                                                                             
#predicted genes (>= 3000 bp)              30                                                                              
#mismatches                                177                                                                             
#indels                                    189                                                                             
Indels length                              320                                                                             
#mismatches per 100 kbp                    6.30                                                                            
#indels per 100 kbp                        6.72                                                                            
GC (%)                                     32.74                                                                           
Reference GC (%)                           32.81                                                                           
Average %IDY                               98.891                                                                          
Largest alignment                          305682                                                                          
NA50                                       4886                                                                            
NGA50                                      98834                                                                           
NA75                                       668                                                                             
NGA75                                      47409                                                                           
LA50                                       65                                                                              
LGA50                                      10                                                                              
LA75                                       1389                                                                            
LGA75                                      20                                                                              
#Mis_misassemblies                         164                                                                             
#Mis_relocations                           149                                                                             
#Mis_translocations                        11                                                                              
#Mis_inversions                            4                                                                               
#Mis_misassembled contigs                  157                                                                             
Mis_Misassembled contigs length            610955                                                                          
#Mis_local misassemblies                   22                                                                              
#Mis_short indels (<= 5 bp)                181                                                                             
#Mis_long indels (> 5 bp)                  8                                                                               
#Una_fully unaligned contigs               8                                                                               
Una_Fully unaligned length                 2936                                                                            
#Una_partially unaligned contigs           84                                                                              
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            3                                                                               
Una_Partially unaligned length             6330                                                                            
GAGE_Contigs #                             4312                                                                            
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            305684                                                                          
GAGE_N50                                   112043 COUNT: 10                                                                
GAGE_Genome size                           2813862                                                                         
GAGE_Assembly size                         5741875                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               37(0.00%)                                                                       
GAGE_Missing assembly bases                15993(0.28%)                                                                    
GAGE_Missing assembly contigs              7(0.16%)                                                                        
GAGE_Duplicated reference bases            2900279                                                                         
GAGE_Compressed reference bases            14689                                                                           
GAGE_Bad trim                              12865                                                                           
GAGE_Avg idy                               99.98                                                                           
GAGE_SNPs                                  47                                                                              
GAGE_Indels < 5bp                          74                                                                              
GAGE_Indels >= 5                           4                                                                               
GAGE_Inversions                            4                                                                               
GAGE_Relocation                            3                                                                               
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    90                                                                              
GAGE_Corrected assembly size               2860329                                                                         
GAGE_Min correct contig                    220                                                                             
GAGE_Max correct contig                    305682                                                                          
GAGE_Corrected N50                         98834 COUNT: 10                                                                 
