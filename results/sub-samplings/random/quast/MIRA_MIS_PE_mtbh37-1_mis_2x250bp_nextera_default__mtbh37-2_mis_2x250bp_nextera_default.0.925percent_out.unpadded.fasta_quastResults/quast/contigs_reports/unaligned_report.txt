All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.925percent_out.unpadded
#Una_fully unaligned contigs      0                                                                                                               
Una_Fully unaligned length        0                                                                                                               
#Una_partially unaligned contigs  13                                                                                                              
#Una_with misassembly             0                                                                                                               
#Una_both parts are significant   0                                                                                                               
Una_Partially unaligned length    1078                                                                                                            
#N's                              202                                                                                                             
