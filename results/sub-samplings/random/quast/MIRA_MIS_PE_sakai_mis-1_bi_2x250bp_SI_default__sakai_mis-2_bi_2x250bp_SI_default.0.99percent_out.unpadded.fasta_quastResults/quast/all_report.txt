All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.99percent_out.unpadded
#Contigs                                   4000                                                                                                     
#Contigs (>= 0 bp)                         4054                                                                                                     
#Contigs (>= 1000 bp)                      122                                                                                                      
Largest contig                             485107                                                                                                   
Total length                               6963732                                                                                                  
Total length (>= 0 bp)                     6973175                                                                                                  
Total length (>= 1000 bp)                  5638190                                                                                                  
Reference length                           5594470                                                                                                  
N50                                        190656                                                                                                   
NG50                                       273757                                                                                                   
N75                                        37925                                                                                                    
NG75                                       141033                                                                                                   
L50                                        11                                                                                                       
LG50                                       8                                                                                                        
L75                                        27                                                                                                       
LG75                                       15                                                                                                       
#local misassemblies                       17                                                                                                       
#misassemblies                             92                                                                                                       
#misassembled contigs                      50                                                                                                       
Misassembled contigs length                3693474                                                                                                  
Misassemblies inter-contig overlap         104729                                                                                                   
#unaligned contigs                         5 + 12 part                                                                                              
Unaligned contigs length                   11533                                                                                                    
#ambiguously mapped contigs                179                                                                                                      
Extra bases in ambiguously mapped contigs  -103722                                                                                                  
#N's                                       470                                                                                                      
#N's per 100 kbp                           6.75                                                                                                     
Genome fraction (%)                        99.451                                                                                                   
Duplication ratio                          1.250                                                                                                    
#genes                                     5380 + 57 part                                                                                           
#predicted genes (unique)                  9443                                                                                                     
#predicted genes (>= 0 bp)                 9566                                                                                                     
#predicted genes (>= 300 bp)               6096                                                                                                     
#predicted genes (>= 1500 bp)              711                                                                                                      
#predicted genes (>= 3000 bp)              76                                                                                                       
#mismatches                                2651                                                                                                     
#indels                                    151                                                                                                      
Indels length                              274                                                                                                      
#mismatches per 100 kbp                    47.65                                                                                                    
#indels per 100 kbp                        2.71                                                                                                     
GC (%)                                     50.63                                                                                                    
Reference GC (%)                           50.48                                                                                                    
Average %IDY                               99.219                                                                                                   
Largest alignment                          378810                                                                                                   
NA50                                       112542                                                                                                   
NGA50                                      148753                                                                                                   
NA75                                       17603                                                                                                    
NGA75                                      83549                                                                                                    
LA50                                       17                                                                                                       
LGA50                                      11                                                                                                       
LA75                                       47                                                                                                       
LGA75                                      24                                                                                                       
#Mis_misassemblies                         92                                                                                                       
#Mis_relocations                           87                                                                                                       
#Mis_translocations                        4                                                                                                        
#Mis_inversions                            1                                                                                                        
#Mis_misassembled contigs                  50                                                                                                       
Mis_Misassembled contigs length            3693474                                                                                                  
#Mis_local misassemblies                   17                                                                                                       
#Mis_short indels (<= 5 bp)                144                                                                                                      
#Mis_long indels (> 5 bp)                  7                                                                                                        
#Una_fully unaligned contigs               5                                                                                                        
Una_Fully unaligned length                 7867                                                                                                     
#Una_partially unaligned contigs           12                                                                                                       
#Una_with misassembly                      0                                                                                                        
#Una_both parts are significant            3                                                                                                        
Una_Partially unaligned length             3666                                                                                                     
GAGE_Contigs #                             4000                                                                                                     
GAGE_Min contig                            200                                                                                                      
GAGE_Max contig                            485107                                                                                                   
GAGE_N50                                   273757 COUNT: 8                                                                                          
GAGE_Genome size                           5594470                                                                                                  
GAGE_Assembly size                         6963732                                                                                                  
GAGE_Chaff bases                           0                                                                                                        
GAGE_Missing reference bases               128(0.00%)                                                                                               
GAGE_Missing assembly bases                12466(0.18%)                                                                                             
GAGE_Missing assembly contigs              6(0.15%)                                                                                                 
GAGE_Duplicated reference bases            1477638                                                                                                  
GAGE_Compressed reference bases            103838                                                                                                   
GAGE_Bad trim                              4128                                                                                                     
GAGE_Avg idy                               99.95                                                                                                    
GAGE_SNPs                                  382                                                                                                      
GAGE_Indels < 5bp                          33                                                                                                       
GAGE_Indels >= 5                           4                                                                                                        
GAGE_Inversions                            23                                                                                                       
GAGE_Relocation                            10                                                                                                       
GAGE_Translocation                         1                                                                                                        
GAGE_Corrected contig #                    143                                                                                                      
GAGE_Corrected assembly size               5599448                                                                                                  
GAGE_Min correct contig                    249                                                                                                      
GAGE_Max correct contig                    378810                                                                                                   
GAGE_Corrected N50                         147371 COUNT: 12                                                                                         
