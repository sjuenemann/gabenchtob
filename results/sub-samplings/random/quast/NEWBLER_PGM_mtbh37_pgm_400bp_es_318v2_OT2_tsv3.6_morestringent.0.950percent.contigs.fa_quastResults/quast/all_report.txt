All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.950percent.contigs
#Contigs                                   208                                                                                
#Contigs (>= 0 bp)                         249                                                                                
#Contigs (>= 1000 bp)                      137                                                                                
Largest contig                             182926                                                                             
Total length                               4299555                                                                            
Total length (>= 0 bp)                     4305593                                                                            
Total length (>= 1000 bp)                  4265949                                                                            
Reference length                           4411532                                                                            
N50                                        64014                                                                              
NG50                                       63169                                                                              
N75                                        34939                                                                              
NG75                                       33589                                                                              
L50                                        21                                                                                 
LG50                                       22                                                                                 
L75                                        44                                                                                 
LG75                                       46                                                                                 
#local misassemblies                       17                                                                                 
#misassemblies                             0                                                                                  
#misassembled contigs                      0                                                                                  
Misassembled contigs length                0                                                                                  
Misassemblies inter-contig overlap         2333                                                                               
#unaligned contigs                         1 + 1 part                                                                         
Unaligned contigs length                   1609                                                                               
#ambiguously mapped contigs                29                                                                                 
Extra bases in ambiguously mapped contigs  -20857                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        96.986                                                                             
Duplication ratio                          1.000                                                                              
#genes                                     3939 + 112 part                                                                    
#predicted genes (unique)                  4344                                                                               
#predicted genes (>= 0 bp)                 4344                                                                               
#predicted genes (>= 300 bp)               3689                                                                               
#predicted genes (>= 1500 bp)              501                                                                                
#predicted genes (>= 3000 bp)              61                                                                                 
#mismatches                                169                                                                                
#indels                                    746                                                                                
Indels length                              842                                                                                
#mismatches per 100 kbp                    3.95                                                                               
#indels per 100 kbp                        17.44                                                                              
GC (%)                                     65.41                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.561                                                                             
Largest alignment                          182926                                                                             
NA50                                       64014                                                                              
NGA50                                      63169                                                                              
NA75                                       34939                                                                              
NGA75                                      33589                                                                              
LA50                                       21                                                                                 
LGA50                                      22                                                                                 
LA75                                       44                                                                                 
LGA75                                      46                                                                                 
#Mis_misassemblies                         0                                                                                  
#Mis_relocations                           0                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  0                                                                                  
Mis_Misassembled contigs length            0                                                                                  
#Mis_local misassemblies                   17                                                                                 
#Mis_short indels (<= 5 bp)                744                                                                                
#Mis_long indels (> 5 bp)                  2                                                                                  
#Una_fully unaligned contigs               1                                                                                  
Una_Fully unaligned length                 855                                                                                
#Una_partially unaligned contigs           1                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            1                                                                                  
Una_Partially unaligned length             754                                                                                
GAGE_Contigs #                             208                                                                                
GAGE_Min contig                            207                                                                                
GAGE_Max contig                            182926                                                                             
GAGE_N50                                   63169 COUNT: 22                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4299555                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               62495(1.42%)                                                                       
GAGE_Missing assembly bases                879(0.02%)                                                                         
GAGE_Missing assembly contigs              1(0.48%)                                                                           
GAGE_Duplicated reference bases            430                                                                                
GAGE_Compressed reference bases            48552                                                                              
GAGE_Bad trim                              24                                                                                 
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  122                                                                                
GAGE_Indels < 5bp                          780                                                                                
GAGE_Indels >= 5                           14                                                                                 
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            7                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    228                                                                                
GAGE_Corrected assembly size               4302978                                                                            
GAGE_Min correct contig                    207                                                                                
GAGE_Max correct contig                    182944                                                                             
GAGE_Corrected N50                         52743 COUNT: 27                                                                    
