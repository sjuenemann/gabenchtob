All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.85percent.scf
#Contigs                                   321                                                          
#Contigs (>= 0 bp)                         321                                                          
#Contigs (>= 1000 bp)                      321                                                          
Largest contig                             215053                                                       
Total length                               5291642                                                      
Total length (>= 0 bp)                     5291642                                                      
Total length (>= 1000 bp)                  5291642                                                      
Reference length                           5594470                                                      
N50                                        41361                                                        
NG50                                       40380                                                        
N75                                        18487                                                        
NG75                                       16422                                                        
L50                                        36                                                           
LG50                                       39                                                           
L75                                        84                                                           
LG75                                       97                                                           
#local misassemblies                       6                                                            
#misassemblies                             3                                                            
#misassembled contigs                      3                                                            
Misassembled contigs length                42183                                                        
Misassemblies inter-contig overlap         1778                                                         
#unaligned contigs                         0 + 0 part                                                   
Unaligned contigs length                   0                                                            
#ambiguously mapped contigs                23                                                           
Extra bases in ambiguously mapped contigs  -33749                                                       
#N's                                       0                                                            
#N's per 100 kbp                           0.00                                                         
Genome fraction (%)                        92.929                                                       
Duplication ratio                          1.012                                                        
#genes                                     4861 + 228 part                                              
#predicted genes (unique)                  5339                                                         
#predicted genes (>= 0 bp)                 5344                                                         
#predicted genes (>= 300 bp)               4508                                                         
#predicted genes (>= 1500 bp)              636                                                          
#predicted genes (>= 3000 bp)              61                                                           
#mismatches                                122                                                          
#indels                                    303                                                          
Indels length                              309                                                          
#mismatches per 100 kbp                    2.35                                                         
#indels per 100 kbp                        5.83                                                         
GC (%)                                     50.31                                                        
Reference GC (%)                           50.48                                                        
Average %IDY                               98.746                                                       
Largest alignment                          215053                                                       
NA50                                       41361                                                        
NGA50                                      40380                                                        
NA75                                       18487                                                        
NGA75                                      16351                                                        
LA50                                       36                                                           
LGA50                                      39                                                           
LA75                                       84                                                           
LGA75                                      98                                                           
#Mis_misassemblies                         3                                                            
#Mis_relocations                           3                                                            
#Mis_translocations                        0                                                            
#Mis_inversions                            0                                                            
#Mis_misassembled contigs                  3                                                            
Mis_Misassembled contigs length            42183                                                        
#Mis_local misassemblies                   6                                                            
#Mis_short indels (<= 5 bp)                303                                                          
#Mis_long indels (> 5 bp)                  0                                                            
#Una_fully unaligned contigs               0                                                            
Una_Fully unaligned length                 0                                                            
#Una_partially unaligned contigs           0                                                            
#Una_with misassembly                      0                                                            
#Una_both parts are significant            0                                                            
Una_Partially unaligned length             0                                                            
GAGE_Contigs #                             321                                                          
GAGE_Min contig                            1003                                                         
GAGE_Max contig                            215053                                                       
GAGE_N50                                   40380 COUNT: 39                                              
GAGE_Genome size                           5594470                                                      
GAGE_Assembly size                         5291642                                                      
GAGE_Chaff bases                           0                                                            
GAGE_Missing reference bases               262795(4.70%)                                                
GAGE_Missing assembly bases                66(0.00%)                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                     
GAGE_Duplicated reference bases            542                                                          
GAGE_Compressed reference bases            154022                                                       
GAGE_Bad trim                              64                                                           
GAGE_Avg idy                               99.99                                                        
GAGE_SNPs                                  106                                                          
GAGE_Indels < 5bp                          295                                                          
GAGE_Indels >= 5                           6                                                            
GAGE_Inversions                            1                                                            
GAGE_Relocation                            2                                                            
GAGE_Translocation                         0                                                            
GAGE_Corrected contig #                    330                                                          
GAGE_Corrected assembly size               5293356                                                      
GAGE_Min correct contig                    550                                                          
GAGE_Max correct contig                    215058                                                       
GAGE_Corrected N50                         38797 COUNT: 40                                              
