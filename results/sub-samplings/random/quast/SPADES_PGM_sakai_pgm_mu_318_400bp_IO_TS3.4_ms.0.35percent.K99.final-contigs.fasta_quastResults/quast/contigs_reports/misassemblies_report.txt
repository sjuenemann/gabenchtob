All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.35percent.K99.final-contigs
#Mis_misassemblies               4                                                                          
#Mis_relocations                 4                                                                          
#Mis_translocations              0                                                                          
#Mis_inversions                  0                                                                          
#Mis_misassembled contigs        4                                                                          
Mis_Misassembled contigs length  382404                                                                     
#Mis_local misassemblies         17                                                                         
#mismatches                      251                                                                        
#indels                          391                                                                        
#Mis_short indels (<= 5 bp)      385                                                                        
#Mis_long indels (> 5 bp)        6                                                                          
Indels length                    483                                                                        
