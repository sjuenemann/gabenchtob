All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.7percent_out.unpadded
#Mis_misassemblies               79                                                                                                      
#Mis_relocations                 75                                                                                                      
#Mis_translocations              3                                                                                                       
#Mis_inversions                  1                                                                                                       
#Mis_misassembled contigs        45                                                                                                      
Mis_Misassembled contigs length  3848847                                                                                                 
#Mis_local misassemblies         17                                                                                                      
#mismatches                      1855                                                                                                    
#indels                          106                                                                                                     
#Mis_short indels (<= 5 bp)      103                                                                                                     
#Mis_long indels (> 5 bp)        3                                                                                                       
Indels length                    189                                                                                                     
