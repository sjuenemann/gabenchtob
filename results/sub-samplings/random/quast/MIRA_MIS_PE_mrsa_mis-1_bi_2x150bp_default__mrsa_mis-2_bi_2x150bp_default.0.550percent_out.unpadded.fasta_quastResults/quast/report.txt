All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.550percent_out.unpadded
#Contigs (>= 0 bp)             267                                                                                               
#Contigs (>= 1000 bp)          38                                                                                                
Total length (>= 0 bp)         2908221                                                                                           
Total length (>= 1000 bp)      2834168                                                                                           
#Contigs                       230                                                                                               
Largest contig                 534807                                                                                            
Total length                   2902234                                                                                           
Reference length               2813862                                                                                           
GC (%)                         32.98                                                                                             
Reference GC (%)               32.81                                                                                             
N50                            220184                                                                                            
NG50                           220184                                                                                            
N75                            89081                                                                                             
NG75                           94009                                                                                             
#misassemblies                 24                                                                                                
#local misassemblies           4                                                                                                 
#unaligned contigs             2 + 0 part                                                                                        
Unaligned contigs length       6048                                                                                              
Genome fraction (%)            99.881                                                                                            
Duplication ratio              1.033                                                                                             
#N's per 100 kbp               0.79                                                                                              
#mismatches per 100 kbp        6.83                                                                                              
#indels per 100 kbp            5.23                                                                                              
#genes                         2706 + 19 part                                                                                    
#predicted genes (unique)      2837                                                                                              
#predicted genes (>= 0 bp)     2861                                                                                              
#predicted genes (>= 300 bp)   2375                                                                                              
#predicted genes (>= 1500 bp)  302                                                                                               
#predicted genes (>= 3000 bp)  30                                                                                                
Largest alignment              534807                                                                                            
NA50                           112612                                                                                            
NGA50                          220184                                                                                            
NA75                           69238                                                                                             
NGA75                          75825                                                                                             
