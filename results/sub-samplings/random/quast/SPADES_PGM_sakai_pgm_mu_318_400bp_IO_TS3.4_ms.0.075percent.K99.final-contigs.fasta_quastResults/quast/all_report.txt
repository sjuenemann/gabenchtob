All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.075percent.K99.final-contigs
#Contigs                                   222                                                                         
#Contigs (>= 0 bp)                         273                                                                         
#Contigs (>= 1000 bp)                      188                                                                         
Largest contig                             316894                                                                      
Total length                               5312478                                                                     
Total length (>= 0 bp)                     5318773                                                                     
Total length (>= 1000 bp)                  5297871                                                                     
Reference length                           5594470                                                                     
N50                                        96224                                                                       
NG50                                       92226                                                                       
N75                                        42996                                                                       
NG75                                       38255                                                                       
L50                                        17                                                                          
LG50                                       19                                                                          
L75                                        37                                                                          
LG75                                       42                                                                          
#local misassemblies                       21                                                                          
#misassemblies                             24                                                                          
#misassembled contigs                      22                                                                          
Misassembled contigs length                746559                                                                      
Misassemblies inter-contig overlap         14317                                                                       
#unaligned contigs                         0 + 0 part                                                                  
Unaligned contigs length                   0                                                                           
#ambiguously mapped contigs                44                                                                          
Extra bases in ambiguously mapped contigs  -51252                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        94.264                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     4951 + 160 part                                                             
#predicted genes (unique)                  5634                                                                        
#predicted genes (>= 0 bp)                 5641                                                                        
#predicted genes (>= 300 bp)               4658                                                                        
#predicted genes (>= 1500 bp)              563                                                                         
#predicted genes (>= 3000 bp)              49                                                                          
#mismatches                                1338                                                                        
#indels                                    1661                                                                        
Indels length                              1720                                                                        
#mismatches per 100 kbp                    25.37                                                                       
#indels per 100 kbp                        31.50                                                                       
GC (%)                                     50.29                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               98.836                                                                      
Largest alignment                          238369                                                                      
NA50                                       96224                                                                       
NGA50                                      92226                                                                       
NA75                                       41385                                                                       
NGA75                                      33002                                                                       
LA50                                       18                                                                          
LGA50                                      20                                                                          
LA75                                       39                                                                          
LGA75                                      44                                                                          
#Mis_misassemblies                         24                                                                          
#Mis_relocations                           24                                                                          
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  22                                                                          
Mis_Misassembled contigs length            746559                                                                      
#Mis_local misassemblies                   21                                                                          
#Mis_short indels (<= 5 bp)                1660                                                                        
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           0                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             0                                                                           
GAGE_Contigs #                             222                                                                         
GAGE_Min contig                            201                                                                         
GAGE_Max contig                            316894                                                                      
GAGE_N50                                   92226 COUNT: 19                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         5312478                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               26161(0.47%)                                                                
GAGE_Missing assembly bases                880(0.02%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            2417                                                                        
GAGE_Compressed reference bases            286328                                                                      
GAGE_Bad trim                              871                                                                         
GAGE_Avg idy                               99.95                                                                       
GAGE_SNPs                                  569                                                                         
GAGE_Indels < 5bp                          1644                                                                        
GAGE_Indels >= 5                           17                                                                          
GAGE_Inversions                            7                                                                           
GAGE_Relocation                            12                                                                          
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    258                                                                         
GAGE_Corrected assembly size               5325441                                                                     
GAGE_Min correct contig                    201                                                                         
GAGE_Max correct contig                    238374                                                                      
GAGE_Corrected N50                         74219 COUNT: 23                                                             
