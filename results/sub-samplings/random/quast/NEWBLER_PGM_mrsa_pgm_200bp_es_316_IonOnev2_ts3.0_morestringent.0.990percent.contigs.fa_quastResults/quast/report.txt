All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.990percent.contigs
#Contigs (>= 0 bp)             1373                                                                               
#Contigs (>= 1000 bp)          528                                                                                
Total length (>= 0 bp)         2791740                                                                            
Total length (>= 1000 bp)      2610397                                                                            
#Contigs                       736                                                                                
Largest contig                 25648                                                                              
Total length                   2711035                                                                            
Reference length               2813862                                                                            
GC (%)                         32.66                                                                              
Reference GC (%)               32.81                                                                              
N50                            6579                                                                               
NG50                           6447                                                                               
N75                            3709                                                                               
NG75                           3484                                                                               
#misassemblies                 17                                                                                 
#local misassemblies           3                                                                                  
#unaligned contigs             1 + 6 part                                                                         
Unaligned contigs length       615                                                                                
Genome fraction (%)            96.009                                                                             
Duplication ratio              1.001                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        3.15                                                                               
#indels per 100 kbp            12.14                                                                              
#genes                         2179 + 511 part                                                                    
#predicted genes (unique)      3104                                                                               
#predicted genes (>= 0 bp)     3104                                                                               
#predicted genes (>= 300 bp)   2443                                                                               
#predicted genes (>= 1500 bp)  235                                                                                
#predicted genes (>= 3000 bp)  19                                                                                 
Largest alignment              25648                                                                              
NA50                           6573                                                                               
NGA50                          6351                                                                               
NA75                           3709                                                                               
NGA75                          3484                                                                               
