All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.9percent.K99.final-contigs
#Contigs (>= 0 bp)             764                                                                                  
#Contigs (>= 1000 bp)          172                                                                                  
Total length (>= 0 bp)         5428387                                                                              
Total length (>= 1000 bp)      5257628                                                                              
#Contigs                       457                                                                                  
Largest contig                 374870                                                                               
Total length                   5385089                                                                              
Reference length               5594470                                                                              
GC (%)                         50.30                                                                                
Reference GC (%)               50.48                                                                                
N50                            142583                                                                               
NG50                           140505                                                                               
N75                            50285                                                                                
NG75                           41514                                                                                
#misassemblies                 3                                                                                    
#local misassemblies           7                                                                                    
#unaligned contigs             0 + 1 part                                                                           
Unaligned contigs length       31                                                                                   
Genome fraction (%)            94.450                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        2.18                                                                                 
#indels per 100 kbp            24.51                                                                                
#genes                         4938 + 209 part                                                                      
#predicted genes (unique)      5971                                                                                 
#predicted genes (>= 0 bp)     5989                                                                                 
#predicted genes (>= 300 bp)   4784                                                                                 
#predicted genes (>= 1500 bp)  550                                                                                  
#predicted genes (>= 3000 bp)  40                                                                                   
Largest alignment              247775                                                                               
NA50                           142583                                                                               
NGA50                          140505                                                                               
NA75                           50285                                                                                
NGA75                          41514                                                                                
