All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.990percent_112.final
#Mis_misassemblies               16                                                                                    
#Mis_relocations                 14                                                                                    
#Mis_translocations              0                                                                                     
#Mis_inversions                  2                                                                                     
#Mis_misassembled contigs        16                                                                                    
Mis_Misassembled contigs length  17048                                                                                 
#Mis_local misassemblies         3                                                                                     
#mismatches                      39                                                                                    
#indels                          313                                                                                   
#Mis_short indels (<= 5 bp)      313                                                                                   
#Mis_long indels (> 5 bp)        0                                                                                     
Indels length                    330                                                                                   
