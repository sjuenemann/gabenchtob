All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.990percent_112.final
GAGE_Contigs #                   1454                                                                                  
GAGE_Min contig                  200                                                                                   
GAGE_Max contig                  60323                                                                                 
GAGE_N50                         17242 COUNT: 51                                                                       
GAGE_Genome size                 2813862                                                                               
GAGE_Assembly size               3000290                                                                               
GAGE_Chaff bases                 0                                                                                     
GAGE_Missing reference bases     6161(0.22%)                                                                           
GAGE_Missing assembly bases      497(0.02%)                                                                            
GAGE_Missing assembly contigs    0(0.00%)                                                                              
GAGE_Duplicated reference bases  181904                                                                                
GAGE_Compressed reference bases  41665                                                                                 
GAGE_Bad trim                    497                                                                                   
GAGE_Avg idy                     99.98                                                                                 
GAGE_SNPs                        35                                                                                    
GAGE_Indels < 5bp                326                                                                                   
GAGE_Indels >= 5                 3                                                                                     
GAGE_Inversions                  0                                                                                     
GAGE_Relocation                  1                                                                                     
GAGE_Translocation               0                                                                                     
GAGE_Corrected contig #          626                                                                                   
GAGE_Corrected assembly size     2818638                                                                               
GAGE_Min correct contig          200                                                                                   
GAGE_Max correct contig          60323                                                                                 
GAGE_Corrected N50               16579 COUNT: 52                                                                       
