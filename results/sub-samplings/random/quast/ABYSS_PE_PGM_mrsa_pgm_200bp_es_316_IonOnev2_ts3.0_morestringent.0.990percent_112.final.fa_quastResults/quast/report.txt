All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.990percent_112.final
#Contigs (>= 0 bp)             3266                                                                                  
#Contigs (>= 1000 bp)          264                                                                                   
Total length (>= 0 bp)         3245106                                                                               
Total length (>= 1000 bp)      2717011                                                                               
#Contigs                       1454                                                                                  
Largest contig                 60323                                                                                 
Total length                   3000290                                                                               
Reference length               2813862                                                                               
GC (%)                         32.63                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            16244                                                                                 
NG50                           17242                                                                                 
N75                            7336                                                                                  
NG75                           9277                                                                                  
#misassemblies                 16                                                                                    
#local misassemblies           3                                                                                     
#unaligned contigs             0 + 19 part                                                                           
Unaligned contigs length       784                                                                                   
Genome fraction (%)            98.407                                                                                
Duplication ratio              1.075                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        1.41                                                                                  
#indels per 100 kbp            11.30                                                                                 
#genes                         2485 + 215 part                                                                       
#predicted genes (unique)      3899                                                                                  
#predicted genes (>= 0 bp)     3929                                                                                  
#predicted genes (>= 300 bp)   2361                                                                                  
#predicted genes (>= 1500 bp)  270                                                                                   
#predicted genes (>= 3000 bp)  20                                                                                    
Largest alignment              60323                                                                                 
NA50                           16244                                                                                 
NGA50                          17242                                                                                 
NA75                           7336                                                                                  
NGA75                          9045                                                                                  
