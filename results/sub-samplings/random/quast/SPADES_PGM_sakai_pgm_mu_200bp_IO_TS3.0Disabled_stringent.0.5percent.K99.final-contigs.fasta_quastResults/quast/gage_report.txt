All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.5percent.K99.final-contigs
GAGE_Contigs #                   400                                                                                  
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  374854                                                                               
GAGE_N50                         142463 COUNT: 14                                                                     
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5370655                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     7831(0.14%)                                                                          
GAGE_Missing assembly bases      732(0.01%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  2803                                                                                 
GAGE_Compressed reference bases  282780                                                                               
GAGE_Bad trim                    732                                                                                  
GAGE_Avg idy                     99.96                                                                                
GAGE_SNPs                        227                                                                                  
GAGE_Indels < 5bp                1596                                                                                 
GAGE_Indels >= 5                 7                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  10                                                                                   
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          410                                                                                  
GAGE_Corrected assembly size     5371606                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          238374                                                                               
GAGE_Corrected N50               124348 COUNT: 17                                                                     
