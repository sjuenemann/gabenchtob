All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.25percent.contigs
#Contigs                                   289                                                               
#Contigs (>= 0 bp)                         368                                                               
#Contigs (>= 1000 bp)                      150                                                               
Largest contig                             327101                                                            
Total length                               5356285                                                           
Total length (>= 0 bp)                     5367666                                                           
Total length (>= 1000 bp)                  5293173                                                           
Reference length                           5594470                                                           
N50                                        124161                                                            
NG50                                       105968                                                            
N75                                        54999                                                             
NG75                                       49573                                                             
L50                                        13                                                                
LG50                                       14                                                                
L75                                        29                                                                
LG75                                       33                                                                
#local misassemblies                       14                                                                
#misassemblies                             3                                                                 
#misassembled contigs                      3                                                                 
Misassembled contigs length                348577                                                            
Misassemblies inter-contig overlap         3052                                                              
#unaligned contigs                         0 + 0 part                                                        
Unaligned contigs length                   0                                                                 
#ambiguously mapped contigs                99                                                                
Extra bases in ambiguously mapped contigs  -82206                                                            
#N's                                       0                                                                 
#N's per 100 kbp                           0.00                                                              
Genome fraction (%)                        94.297                                                            
Duplication ratio                          1.000                                                             
#genes                                     4982 + 147 part                                                   
#predicted genes (unique)                  5357                                                              
#predicted genes (>= 0 bp)                 5359                                                              
#predicted genes (>= 300 bp)               4512                                                              
#predicted genes (>= 1500 bp)              648                                                               
#predicted genes (>= 3000 bp)              61                                                                
#mismatches                                175                                                               
#indels                                    256                                                               
Indels length                              265                                                               
#mismatches per 100 kbp                    3.32                                                              
#indels per 100 kbp                        4.85                                                              
GC (%)                                     50.30                                                             
Reference GC (%)                           50.48                                                             
Average %IDY                               98.706                                                            
Largest alignment                          327101                                                            
NA50                                       124161                                                            
NGA50                                      105968                                                            
NA75                                       54999                                                             
NGA75                                      49573                                                             
LA50                                       14                                                                
LGA50                                      15                                                                
LA75                                       30                                                                
LGA75                                      34                                                                
#Mis_misassemblies                         3                                                                 
#Mis_relocations                           3                                                                 
#Mis_translocations                        0                                                                 
#Mis_inversions                            0                                                                 
#Mis_misassembled contigs                  3                                                                 
Mis_Misassembled contigs length            348577                                                            
#Mis_local misassemblies                   14                                                                
#Mis_short indels (<= 5 bp)                256                                                               
#Mis_long indels (> 5 bp)                  0                                                                 
#Una_fully unaligned contigs               0                                                                 
Una_Fully unaligned length                 0                                                                 
#Una_partially unaligned contigs           0                                                                 
#Una_with misassembly                      0                                                                 
#Una_both parts are significant            0                                                                 
Una_Partially unaligned length             0                                                                 
GAGE_Contigs #                             289                                                               
GAGE_Min contig                            202                                                               
GAGE_Max contig                            327101                                                            
GAGE_N50                                   105968 COUNT: 14                                                  
GAGE_Genome size                           5594470                                                           
GAGE_Assembly size                         5356285                                                           
GAGE_Chaff bases                           0                                                                 
GAGE_Missing reference bases               28267(0.51%)                                                      
GAGE_Missing assembly bases                5(0.00%)                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                          
GAGE_Duplicated reference bases            2200                                                              
GAGE_Compressed reference bases            228037                                                            
GAGE_Bad trim                              5                                                                 
GAGE_Avg idy                               99.99                                                             
GAGE_SNPs                                  86                                                                
GAGE_Indels < 5bp                          273                                                               
GAGE_Indels >= 5                           10                                                                
GAGE_Inversions                            0                                                                 
GAGE_Relocation                            7                                                                 
GAGE_Translocation                         0                                                                 
GAGE_Corrected contig #                    297                                                               
GAGE_Corrected assembly size               5357063                                                           
GAGE_Min correct contig                    202                                                               
GAGE_Max correct contig                    313660                                                            
GAGE_Corrected N50                         99586 COUNT: 18                                                   
