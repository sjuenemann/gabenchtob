All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.550percent.scf
GAGE_Contigs #                   123                                                                      
GAGE_Min contig                  1010                                                                     
GAGE_Max contig                  219753                                                                   
GAGE_N50                         47896 COUNT: 16                                                          
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2778884                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     51500(1.83%)                                                             
GAGE_Missing assembly bases      51(0.00%)                                                                
GAGE_Missing assembly contigs    0(0.00%)                                                                 
GAGE_Duplicated reference bases  1134                                                                     
GAGE_Compressed reference bases  25458                                                                    
GAGE_Bad trim                    51                                                                       
GAGE_Avg idy                     99.98                                                                    
GAGE_SNPs                        50                                                                       
GAGE_Indels < 5bp                315                                                                      
GAGE_Indels >= 5                 4                                                                        
GAGE_Inversions                  0                                                                        
GAGE_Relocation                  2                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          127                                                                      
GAGE_Corrected assembly size     2778732                                                                  
GAGE_Min correct contig          408                                                                      
GAGE_Max correct contig          175729                                                                   
GAGE_Corrected N50               47790 COUNT: 17                                                          
