All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.45percent.result
#Contigs                                   806                                                                     
#Contigs (>= 0 bp)                         806                                                                     
#Contigs (>= 1000 bp)                      340                                                                     
Largest contig                             107449                                                                  
Total length                               5380818                                                                 
Total length (>= 0 bp)                     5380818                                                                 
Total length (>= 1000 bp)                  5217824                                                                 
Reference length                           5594470                                                                 
N50                                        30423                                                                   
NG50                                       28962                                                                   
N75                                        16850                                                                   
NG75                                       14893                                                                   
L50                                        57                                                                      
LG50                                       60                                                                      
L75                                        116                                                                     
LG75                                       126                                                                     
#local misassemblies                       1                                                                       
#misassemblies                             11                                                                      
#misassembled contigs                      11                                                                      
Misassembled contigs length                81359                                                                   
Misassemblies inter-contig overlap         166                                                                     
#unaligned contigs                         0 + 31 part                                                             
Unaligned contigs length                   1139                                                                    
#ambiguously mapped contigs                137                                                                     
Extra bases in ambiguously mapped contigs  -86453                                                                  
#N's                                       52                                                                      
#N's per 100 kbp                           0.97                                                                    
Genome fraction (%)                        93.906                                                                  
Duplication ratio                          1.008                                                                   
#genes                                     4779 + 340 part                                                         
#predicted genes (unique)                  6164                                                                    
#predicted genes (>= 0 bp)                 6169                                                                    
#predicted genes (>= 300 bp)               4791                                                                    
#predicted genes (>= 1500 bp)              527                                                                     
#predicted genes (>= 3000 bp)              39                                                                      
#mismatches                                83                                                                      
#indels                                    1358                                                                    
Indels length                              1428                                                                    
#mismatches per 100 kbp                    1.58                                                                    
#indels per 100 kbp                        25.85                                                                   
GC (%)                                     50.29                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               98.927                                                                  
Largest alignment                          107449                                                                  
NA50                                       30423                                                                   
NGA50                                      28725                                                                   
NA75                                       16768                                                                   
NGA75                                      14835                                                                   
LA50                                       57                                                                      
LGA50                                      61                                                                      
LA75                                       117                                                                     
LGA75                                      127                                                                     
#Mis_misassemblies                         11                                                                      
#Mis_relocations                           4                                                                       
#Mis_translocations                        0                                                                       
#Mis_inversions                            7                                                                       
#Mis_misassembled contigs                  11                                                                      
Mis_Misassembled contigs length            81359                                                                   
#Mis_local misassemblies                   1                                                                       
#Mis_short indels (<= 5 bp)                1357                                                                    
#Mis_long indels (> 5 bp)                  1                                                                       
#Una_fully unaligned contigs               0                                                                       
Una_Fully unaligned length                 0                                                                       
#Una_partially unaligned contigs           31                                                                      
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             1139                                                                    
GAGE_Contigs #                             806                                                                     
GAGE_Min contig                            200                                                                     
GAGE_Max contig                            107449                                                                  
GAGE_N50                                   28962 COUNT: 60                                                         
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5380818                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               46852(0.84%)                                                            
GAGE_Missing assembly bases                1251(0.02%)                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                                
GAGE_Duplicated reference bases            47083                                                                   
GAGE_Compressed reference bases            245814                                                                  
GAGE_Bad trim                              1251                                                                    
GAGE_Avg idy                               99.97                                                                   
GAGE_SNPs                                  85                                                                      
GAGE_Indels < 5bp                          1401                                                                    
GAGE_Indels >= 5                           2                                                                       
GAGE_Inversions                            0                                                                       
GAGE_Relocation                            3                                                                       
GAGE_Translocation                         0                                                                       
GAGE_Corrected contig #                    625                                                                     
GAGE_Corrected assembly size               5334247                                                                 
GAGE_Min correct contig                    201                                                                     
GAGE_Max correct contig                    107468                                                                  
GAGE_Corrected N50                         28731 COUNT: 61                                                         
