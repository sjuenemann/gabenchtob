All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.scf
#Contigs (>= 0 bp)             40                                                          
#Contigs (>= 1000 bp)          40                                                          
Total length (>= 0 bp)         76866                                                       
Total length (>= 1000 bp)      76866                                                       
#Contigs                       40                                                          
Largest contig                 12004                                                       
Total length                   76866                                                       
Reference length               2813862                                                     
GC (%)                         32.07                                                       
Reference GC (%)               32.81                                                       
N50                            1738                                                        
NG50                           None                                                        
N75                            1168                                                        
NG75                           None                                                        
#misassemblies                 1                                                           
#local misassemblies           0                                                           
#unaligned contigs             0 + 0 part                                                  
Unaligned contigs length       0                                                           
Genome fraction (%)            2.499                                                       
Duplication ratio              1.060                                                       
#N's per 100 kbp               0.00                                                        
#mismatches per 100 kbp        15.64                                                       
#indels per 100 kbp            110.91                                                      
#genes                         56 + 44 part                                                
#predicted genes (unique)      128                                                         
#predicted genes (>= 0 bp)     129                                                         
#predicted genes (>= 300 bp)   65                                                          
#predicted genes (>= 1500 bp)  4                                                           
#predicted genes (>= 3000 bp)  0                                                           
Largest alignment              12002                                                       
NA50                           1738                                                        
NGA50                          None                                                        
NA75                           1162                                                        
NGA75                          None                                                        
