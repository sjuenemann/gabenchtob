All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.050percent_out.unpadded
#Contigs (>= 0 bp)             2685                                                                                              
#Contigs (>= 1000 bp)          679                                                                                               
Total length (>= 0 bp)         2188738                                                                                           
Total length (>= 1000 bp)      1152476                                                                                           
#Contigs                       2649                                                                                              
Largest contig                 7255                                                                                              
Total length                   2182618                                                                                           
Reference length               2813862                                                                                           
GC (%)                         33.35                                                                                             
Reference GC (%)               32.81                                                                                             
N50                            1072                                                                                              
NG50                           782                                                                                               
N75                            610                                                                                               
NG75                           302                                                                                               
#misassemblies                 21                                                                                                
#local misassemblies           5                                                                                                 
#unaligned contigs             8 + 4 part                                                                                        
Unaligned contigs length       4813                                                                                              
Genome fraction (%)            76.745                                                                                            
Duplication ratio              1.011                                                                                             
#N's per 100 kbp               8.16                                                                                              
#mismatches per 100 kbp        16.62                                                                                             
#indels per 100 kbp            4.72                                                                                              
#genes                         762 + 1731 part                                                                                   
#predicted genes (unique)      3815                                                                                              
#predicted genes (>= 0 bp)     3816                                                                                              
#predicted genes (>= 300 bp)   2443                                                                                              
#predicted genes (>= 1500 bp)  71                                                                                                
#predicted genes (>= 3000 bp)  4                                                                                                 
Largest alignment              7255                                                                                              
NA50                           1060                                                                                              
NGA50                          779                                                                                               
NA75                           604                                                                                               
NGA75                          293                                                                                               
