All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.55percent.scf
#Contigs (>= 0 bp)             273                                                          
#Contigs (>= 1000 bp)          273                                                          
Total length (>= 0 bp)         5265452                                                      
Total length (>= 1000 bp)      5265452                                                      
#Contigs                       273                                                          
Largest contig                 184833                                                       
Total length                   5265452                                                      
Reference length               5594470                                                      
GC (%)                         50.31                                                        
Reference GC (%)               50.48                                                        
N50                            47729                                                        
NG50                           43715                                                        
N75                            26108                                                        
NG75                           20415                                                        
#misassemblies                 1                                                            
#local misassemblies           8                                                            
#unaligned contigs             0 + 0 part                                                   
Unaligned contigs length       0                                                            
Genome fraction (%)            92.854                                                       
Duplication ratio              1.010                                                        
#N's per 100 kbp               0.00                                                         
#mismatches per 100 kbp        1.69                                                         
#indels per 100 kbp            4.37                                                         
#genes                         4875 + 206 part                                              
#predicted genes (unique)      5270                                                         
#predicted genes (>= 0 bp)     5276                                                         
#predicted genes (>= 300 bp)   4473                                                         
#predicted genes (>= 1500 bp)  636                                                          
#predicted genes (>= 3000 bp)  63                                                           
Largest alignment              184833                                                       
NA50                           45288                                                        
NGA50                          43539                                                        
NA75                           26108                                                        
NGA75                          20413                                                        
