All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.1.000percent_out.unpadded
#Contigs                                   238                                                                                                 
#Contigs (>= 0 bp)                         245                                                                                                 
#Contigs (>= 1000 bp)                      107                                                                                                 
Largest contig                             564973                                                                                              
Total length                               5596291                                                                                             
Total length (>= 0 bp)                     5597431                                                                                             
Total length (>= 1000 bp)                  5537777                                                                                             
Reference length                           5594470                                                                                             
N50                                        252781                                                                                              
NG50                                       252781                                                                                              
N75                                        92833                                                                                               
NG75                                       92833                                                                                               
L50                                        8                                                                                                   
LG50                                       8                                                                                                   
L75                                        18                                                                                                  
LG75                                       18                                                                                                  
#local misassemblies                       10                                                                                                  
#misassemblies                             72                                                                                                  
#misassembled contigs                      39                                                                                                  
Misassembled contigs length                3438610                                                                                             
Misassemblies inter-contig overlap         82210                                                                                               
#unaligned contigs                         3 + 1 part                                                                                          
Unaligned contigs length                   6223                                                                                                
#ambiguously mapped contigs                74                                                                                                  
Extra bases in ambiguously mapped contigs  -60692                                                                                              
#N's                                       85                                                                                                  
#N's per 100 kbp                           1.52                                                                                                
Genome fraction (%)                        98.480                                                                                              
Duplication ratio                          1.019                                                                                               
#genes                                     5300 + 75 part                                                                                      
#predicted genes (unique)                  5443                                                                                                
#predicted genes (>= 0 bp)                 5493                                                                                                
#predicted genes (>= 300 bp)               4639                                                                                                
#predicted genes (>= 1500 bp)              695                                                                                                 
#predicted genes (>= 3000 bp)              76                                                                                                  
#mismatches                                2558                                                                                                
#indels                                    154                                                                                                 
Indels length                              191                                                                                                 
#mismatches per 100 kbp                    46.43                                                                                               
#indels per 100 kbp                        2.80                                                                                                
GC (%)                                     50.45                                                                                               
Reference GC (%)                           50.48                                                                                               
Average %IDY                               98.419                                                                                              
Largest alignment                          345084                                                                                              
NA50                                       125611                                                                                              
NGA50                                      125611                                                                                              
NA75                                       64622                                                                                               
NGA75                                      64622                                                                                               
LA50                                       15                                                                                                  
LGA50                                      15                                                                                                  
LA75                                       30                                                                                                  
LGA75                                      30                                                                                                  
#Mis_misassemblies                         72                                                                                                  
#Mis_relocations                           66                                                                                                  
#Mis_translocations                        4                                                                                                   
#Mis_inversions                            2                                                                                                   
#Mis_misassembled contigs                  39                                                                                                  
Mis_Misassembled contigs length            3438610                                                                                             
#Mis_local misassemblies                   10                                                                                                  
#Mis_short indels (<= 5 bp)                152                                                                                                 
#Mis_long indels (> 5 bp)                  2                                                                                                   
#Una_fully unaligned contigs               3                                                                                                   
Una_Fully unaligned length                 6137                                                                                                
#Una_partially unaligned contigs           1                                                                                                   
#Una_with misassembly                      0                                                                                                   
#Una_both parts are significant            0                                                                                                   
Una_Partially unaligned length             86                                                                                                  
GAGE_Contigs #                             238                                                                                                 
GAGE_Min contig                            206                                                                                                 
GAGE_Max contig                            564973                                                                                              
GAGE_N50                                   252781 COUNT: 8                                                                                     
GAGE_Genome size                           5594470                                                                                             
GAGE_Assembly size                         5596291                                                                                             
GAGE_Chaff bases                           0                                                                                                   
GAGE_Missing reference bases               619(0.01%)                                                                                          
GAGE_Missing assembly bases                6551(0.12%)                                                                                         
GAGE_Missing assembly contigs              3(1.26%)                                                                                            
GAGE_Duplicated reference bases            120700                                                                                              
GAGE_Compressed reference bases            139630                                                                                              
GAGE_Bad trim                              253                                                                                                 
GAGE_Avg idy                               99.94                                                                                               
GAGE_SNPs                                  734                                                                                                 
GAGE_Indels < 5bp                          33                                                                                                  
GAGE_Indels >= 5                           5                                                                                                   
GAGE_Inversions                            28                                                                                                  
GAGE_Relocation                            17                                                                                                  
GAGE_Translocation                         4                                                                                                   
GAGE_Corrected contig #                    166                                                                                                 
GAGE_Corrected assembly size               5580961                                                                                             
GAGE_Min correct contig                    221                                                                                                 
GAGE_Max correct contig                    345084                                                                                              
GAGE_Corrected N50                         118831 COUNT: 16                                                                                    
