All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.900percent.K99.final-contigs
#Mis_misassemblies               2                                                                                           
#Mis_relocations                 2                                                                                           
#Mis_translocations              0                                                                                           
#Mis_inversions                  0                                                                                           
#Mis_misassembled contigs        2                                                                                           
Mis_Misassembled contigs length  226111                                                                                      
#Mis_local misassemblies         8                                                                                           
#mismatches                      174                                                                                         
#indels                          378                                                                                         
#Mis_short indels (<= 5 bp)      375                                                                                         
#Mis_long indels (> 5 bp)        3                                                                                           
Indels length                    413                                                                                         
