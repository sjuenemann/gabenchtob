All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.5percent_out.unpadded
GAGE_Contigs #                   949                                                                                                     
GAGE_Min contig                  204                                                                                                     
GAGE_Max contig                  478738                                                                                                  
GAGE_N50                         197998 COUNT: 9                                                                                         
GAGE_Genome size                 5594470                                                                                                 
GAGE_Assembly size               5924845                                                                                                 
GAGE_Chaff bases                 0                                                                                                       
GAGE_Missing reference bases     568(0.01%)                                                                                              
GAGE_Missing assembly bases      8614(0.15%)                                                                                             
GAGE_Missing assembly contigs    3(0.32%)                                                                                                
GAGE_Duplicated reference bases  437946                                                                                                  
GAGE_Compressed reference bases  118540                                                                                                  
GAGE_Bad trim                    435                                                                                                     
GAGE_Avg idy                     99.95                                                                                                   
GAGE_SNPs                        533                                                                                                     
GAGE_Indels < 5bp                34                                                                                                      
GAGE_Indels >= 5                 5                                                                                                       
GAGE_Inversions                  23                                                                                                      
GAGE_Relocation                  13                                                                                                      
GAGE_Translocation               2                                                                                                       
GAGE_Corrected contig #          155                                                                                                     
GAGE_Corrected assembly size     5582296                                                                                                 
GAGE_Min correct contig          238                                                                                                     
GAGE_Max correct contig          318105                                                                                                  
GAGE_Corrected N50               143746 COUNT: 14                                                                                        
