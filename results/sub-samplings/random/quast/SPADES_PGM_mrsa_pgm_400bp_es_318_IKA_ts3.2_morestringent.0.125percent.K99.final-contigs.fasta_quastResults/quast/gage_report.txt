All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.125percent.K99.final-contigs
GAGE_Contigs #                   102                                                                                    
GAGE_Min contig                  202                                                                                    
GAGE_Max contig                  209255                                                                                 
GAGE_N50                         112396 COUNT: 9                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2776193                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     6978(0.25%)                                                                            
GAGE_Missing assembly bases      42(0.00%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  388                                                                                    
GAGE_Compressed reference bases  39471                                                                                  
GAGE_Bad trim                    42                                                                                     
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        57                                                                                     
GAGE_Indels < 5bp                340                                                                                    
GAGE_Indels >= 5                 10                                                                                     
GAGE_Inversions                  1                                                                                      
GAGE_Relocation                  3                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          117                                                                                    
GAGE_Corrected assembly size     2778521                                                                                
GAGE_Min correct contig          202                                                                                    
GAGE_Max correct contig          207893                                                                                 
GAGE_Corrected N50               73485 COUNT: 11                                                                        
