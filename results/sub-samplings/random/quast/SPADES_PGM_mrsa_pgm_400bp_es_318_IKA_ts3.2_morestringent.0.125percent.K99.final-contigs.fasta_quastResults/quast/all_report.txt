All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.125percent.K99.final-contigs
#Contigs                                   102                                                                                    
#Contigs (>= 0 bp)                         454                                                                                    
#Contigs (>= 1000 bp)                      58                                                                                     
Largest contig                             209255                                                                                 
Total length                               2776193                                                                                
Total length (>= 0 bp)                     2824184                                                                                
Total length (>= 1000 bp)                  2756982                                                                                
Reference length                           2813862                                                                                
N50                                        112396                                                                                 
NG50                                       112396                                                                                 
N75                                        46972                                                                                  
NG75                                       46751                                                                                  
L50                                        9                                                                                      
LG50                                       9                                                                                      
L75                                        19                                                                                     
LG75                                       20                                                                                     
#local misassemblies                       11                                                                                     
#misassemblies                             2                                                                                      
#misassembled contigs                      2                                                                                      
Misassembled contigs length                72283                                                                                  
Misassemblies inter-contig overlap         2062                                                                                   
#unaligned contigs                         0 + 1 part                                                                             
Unaligned contigs length                   30                                                                                     
#ambiguously mapped contigs                13                                                                                     
Extra bases in ambiguously mapped contigs  -9304                                                                                  
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        98.274                                                                                 
Duplication ratio                          1.001                                                                                  
#genes                                     2655 + 45 part                                                                         
#predicted genes (unique)                  2741                                                                                   
#predicted genes (>= 0 bp)                 2743                                                                                   
#predicted genes (>= 300 bp)               2327                                                                                   
#predicted genes (>= 1500 bp)              281                                                                                    
#predicted genes (>= 3000 bp)              28                                                                                     
#mismatches                                159                                                                                    
#indels                                    345                                                                                    
Indels length                              413                                                                                    
#mismatches per 100 kbp                    5.75                                                                                   
#indels per 100 kbp                        12.48                                                                                  
GC (%)                                     32.67                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               98.745                                                                                 
Largest alignment                          209255                                                                                 
NA50                                       112396                                                                                 
NGA50                                      112396                                                                                 
NA75                                       46963                                                                                  
NGA75                                      46751                                                                                  
LA50                                       9                                                                                      
LGA50                                      9                                                                                      
LA75                                       19                                                                                     
LGA75                                      20                                                                                     
#Mis_misassemblies                         2                                                                                      
#Mis_relocations                           2                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  2                                                                                      
Mis_Misassembled contigs length            72283                                                                                  
#Mis_local misassemblies                   11                                                                                     
#Mis_short indels (<= 5 bp)                341                                                                                    
#Mis_long indels (> 5 bp)                  4                                                                                      
#Una_fully unaligned contigs               0                                                                                      
Una_Fully unaligned length                 0                                                                                      
#Una_partially unaligned contigs           1                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             30                                                                                     
GAGE_Contigs #                             102                                                                                    
GAGE_Min contig                            202                                                                                    
GAGE_Max contig                            209255                                                                                 
GAGE_N50                                   112396 COUNT: 9                                                                        
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2776193                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               6978(0.25%)                                                                            
GAGE_Missing assembly bases                42(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            388                                                                                    
GAGE_Compressed reference bases            39471                                                                                  
GAGE_Bad trim                              42                                                                                     
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  57                                                                                     
GAGE_Indels < 5bp                          340                                                                                    
GAGE_Indels >= 5                           10                                                                                     
GAGE_Inversions                            1                                                                                      
GAGE_Relocation                            3                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    117                                                                                    
GAGE_Corrected assembly size               2778521                                                                                
GAGE_Min correct contig                    202                                                                                    
GAGE_Max correct contig                    207893                                                                                 
GAGE_Corrected N50                         73485 COUNT: 11                                                                        
