All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.200percent_out.unpadded
#Contigs (>= 0 bp)             976                                                                                  
#Contigs (>= 1000 bp)          449                                                                                  
Total length (>= 0 bp)         4494051                                                                              
Total length (>= 1000 bp)      4262570                                                                              
#Contigs                       954                                                                                  
Largest contig                 74562                                                                                
Total length                   4490735                                                                              
Reference length               4411532                                                                              
GC (%)                         65.12                                                                                
Reference GC (%)               65.61                                                                                
N50                            15192                                                                                
NG50                           15391                                                                                
N75                            7530                                                                                 
NG75                           7832                                                                                 
#misassemblies                 26                                                                                   
#local misassemblies           22                                                                                   
#unaligned contigs             2 + 8 part                                                                           
Unaligned contigs length       1469                                                                                 
Genome fraction (%)            97.088                                                                               
Duplication ratio              1.056                                                                                
#N's per 100 kbp               1.71                                                                                 
#mismatches per 100 kbp        19.96                                                                                
#indels per 100 kbp            34.18                                                                                
#genes                         3658 + 443 part                                                                      
#predicted genes (unique)      5180                                                                                 
#predicted genes (>= 0 bp)     5205                                                                                 
#predicted genes (>= 300 bp)   4067                                                                                 
#predicted genes (>= 1500 bp)  459                                                                                  
#predicted genes (>= 3000 bp)  39                                                                                   
Largest alignment              74562                                                                                
NA50                           14144                                                                                
NGA50                          14766                                                                                
NA75                           6984                                                                                 
NGA75                          7553                                                                                 
