All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.175percent.K99.final-contigs
#Contigs (>= 0 bp)             111                                                                                         
#Contigs (>= 1000 bp)          83                                                                                          
Total length (>= 0 bp)         2772348                                                                                     
Total length (>= 1000 bp)      2766704                                                                                     
#Contigs                       91                                                                                          
Largest contig                 148093                                                                                      
Total length                   2769769                                                                                     
Reference length               2813862                                                                                     
GC (%)                         32.65                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            56215                                                                                       
NG50                           53129                                                                                       
N75                            37300                                                                                       
NG75                           36118                                                                                       
#misassemblies                 13                                                                                          
#local misassemblies           10                                                                                          
#unaligned contigs             1 + 2 part                                                                                  
Unaligned contigs length       683                                                                                         
Genome fraction (%)            98.182                                                                                      
Duplication ratio              1.003                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        13.94                                                                                       
#indels per 100 kbp            28.49                                                                                       
#genes                         2619 + 49 part                                                                              
#predicted genes (unique)      2789                                                                                        
#predicted genes (>= 0 bp)     2790                                                                                        
#predicted genes (>= 300 bp)   2356                                                                                        
#predicted genes (>= 1500 bp)  275                                                                                         
#predicted genes (>= 3000 bp)  25                                                                                          
Largest alignment              148077                                                                                      
NA50                           50631                                                                                       
NGA50                          50631                                                                                       
NA75                           34117                                                                                       
NGA75                          32754                                                                                       
