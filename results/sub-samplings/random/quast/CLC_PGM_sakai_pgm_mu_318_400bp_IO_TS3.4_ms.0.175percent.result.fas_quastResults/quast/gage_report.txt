All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent.result
GAGE_Contigs #                   668                                                           
GAGE_Min contig                  200                                                           
GAGE_Max contig                  137479                                                        
GAGE_N50                         39404 COUNT: 41                                               
GAGE_Genome size                 5594470                                                       
GAGE_Assembly size               5407335                                                       
GAGE_Chaff bases                 0                                                             
GAGE_Missing reference bases     29187(0.52%)                                                  
GAGE_Missing assembly bases      1184(0.02%)                                                   
GAGE_Missing assembly contigs    2(0.30%)                                                      
GAGE_Duplicated reference bases  51341                                                         
GAGE_Compressed reference bases  246160                                                        
GAGE_Bad trim                    503                                                           
GAGE_Avg idy                     99.99                                                         
GAGE_SNPs                        102                                                           
GAGE_Indels < 5bp                563                                                           
GAGE_Indels >= 5                 3                                                             
GAGE_Inversions                  0                                                             
GAGE_Relocation                  5                                                             
GAGE_Translocation               0                                                             
GAGE_Corrected contig #          515                                                           
GAGE_Corrected assembly size     5355962                                                       
GAGE_Min correct contig          200                                                           
GAGE_Max correct contig          137485                                                        
GAGE_Corrected N50               39255 COUNT: 41                                               
