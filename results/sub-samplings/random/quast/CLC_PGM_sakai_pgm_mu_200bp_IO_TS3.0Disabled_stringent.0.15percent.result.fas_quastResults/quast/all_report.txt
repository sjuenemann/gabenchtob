All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.15percent.result
#Contigs                                   879                                                                     
#Contigs (>= 0 bp)                         879                                                                     
#Contigs (>= 1000 bp)                      434                                                                     
Largest contig                             83956                                                                   
Total length                               5346011                                                                 
Total length (>= 0 bp)                     5346011                                                                 
Total length (>= 1000 bp)                  5185166                                                                 
Reference length                           5594470                                                                 
N50                                        22204                                                                   
NG50                                       21283                                                                   
N75                                        10259                                                                   
NG75                                       8828                                                                    
L50                                        76                                                                      
LG50                                       82                                                                      
L75                                        163                                                                     
LG75                                       183                                                                     
#local misassemblies                       7                                                                       
#misassemblies                             17                                                                      
#misassembled contigs                      17                                                                      
Misassembled contigs length                78869                                                                   
Misassemblies inter-contig overlap         1081                                                                    
#unaligned contigs                         3 + 38 part                                                             
Unaligned contigs length                   2417                                                                    
#ambiguously mapped contigs                130                                                                     
Extra bases in ambiguously mapped contigs  -78423                                                                  
#N's                                       243                                                                     
#N's per 100 kbp                           4.55                                                                    
Genome fraction (%)                        93.469                                                                  
Duplication ratio                          1.007                                                                   
#genes                                     4665 + 411 part                                                         
#predicted genes (unique)                  6493                                                                    
#predicted genes (>= 0 bp)                 6494                                                                    
#predicted genes (>= 300 bp)               4919                                                                    
#predicted genes (>= 1500 bp)              460                                                                     
#predicted genes (>= 3000 bp)              34                                                                      
#mismatches                                167                                                                     
#indels                                    2037                                                                    
Indels length                              2127                                                                    
#mismatches per 100 kbp                    3.19                                                                    
#indels per 100 kbp                        38.96                                                                   
GC (%)                                     50.27                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               99.130                                                                  
Largest alignment                          83956                                                                   
NA50                                       22112                                                                   
NGA50                                      21146                                                                   
NA75                                       10243                                                                   
NGA75                                      8828                                                                    
LA50                                       77                                                                      
LGA50                                      83                                                                      
LA75                                       165                                                                     
LGA75                                      185                                                                     
#Mis_misassemblies                         17                                                                      
#Mis_relocations                           3                                                                       
#Mis_translocations                        0                                                                       
#Mis_inversions                            14                                                                      
#Mis_misassembled contigs                  17                                                                      
Mis_Misassembled contigs length            78869                                                                   
#Mis_local misassemblies                   7                                                                       
#Mis_short indels (<= 5 bp)                2036                                                                    
#Mis_long indels (> 5 bp)                  1                                                                       
#Una_fully unaligned contigs               3                                                                       
Una_Fully unaligned length                 938                                                                     
#Una_partially unaligned contigs           38                                                                      
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             1479                                                                    
GAGE_Contigs #                             879                                                                     
GAGE_Min contig                            200                                                                     
GAGE_Max contig                            83956                                                                   
GAGE_N50                                   21283 COUNT: 82                                                         
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5346011                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               63540(1.14%)                                                            
GAGE_Missing assembly bases                3189(0.06%)                                                             
GAGE_Missing assembly contigs              3(0.34%)                                                                
GAGE_Duplicated reference bases            33801                                                                   
GAGE_Compressed reference bases            255177                                                                  
GAGE_Bad trim                              2251                                                                    
GAGE_Avg idy                               99.95                                                                   
GAGE_SNPs                                  162                                                                     
GAGE_Indels < 5bp                          2103                                                                    
GAGE_Indels >= 5                           8                                                                       
GAGE_Inversions                            0                                                                       
GAGE_Relocation                            3                                                                       
GAGE_Translocation                         0                                                                       
GAGE_Corrected contig #                    754                                                                     
GAGE_Corrected assembly size               5312099                                                                 
GAGE_Min correct contig                    200                                                                     
GAGE_Max correct contig                    83981                                                                   
GAGE_Corrected N50                         21150 COUNT: 83                                                         
