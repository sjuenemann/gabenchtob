All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.075percent.scf
#Mis_misassemblies               13                                                                       
#Mis_relocations                 4                                                                        
#Mis_translocations              0                                                                        
#Mis_inversions                  9                                                                        
#Mis_misassembled contigs        13                                                                       
Mis_Misassembled contigs length  61693                                                                    
#Mis_local misassemblies         4                                                                        
#mismatches                      157                                                                      
#indels                          2576                                                                     
#Mis_short indels (<= 5 bp)      2576                                                                     
#Mis_long indels (> 5 bp)        0                                                                        
Indels length                    2632                                                                     
