All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.05percent.result
#Contigs                                   1488                                                         
#Contigs (>= 0 bp)                         1488                                                         
#Contigs (>= 1000 bp)                      665                                                          
Largest contig                             61784                                                        
Total length                               5360090                                                      
Total length (>= 0 bp)                     5360090                                                      
Total length (>= 1000 bp)                  5016024                                                      
Reference length                           5594470                                                      
N50                                        11448                                                        
NG50                                       10554                                                        
N75                                        5549                                                         
NG75                                       4974                                                         
L50                                        143                                                          
LG50                                       154                                                          
L75                                        310                                                          
LG75                                       343                                                          
#local misassemblies                       12                                                           
#misassemblies                             29                                                           
#misassembled contigs                      29                                                           
Misassembled contigs length                368948                                                       
Misassemblies inter-contig overlap         2713                                                         
#unaligned contigs                         0 + 4 part                                                   
Unaligned contigs length                   198                                                          
#ambiguously mapped contigs                123                                                          
Extra bases in ambiguously mapped contigs  -75629                                                       
#N's                                       199                                                          
#N's per 100 kbp                           3.71                                                         
Genome fraction (%)                        92.612                                                       
Duplication ratio                          1.020                                                        
#genes                                     4343 + 702 part                                              
#predicted genes (unique)                  6634                                                         
#predicted genes (>= 0 bp)                 6637                                                         
#predicted genes (>= 300 bp)               4868                                                         
#predicted genes (>= 1500 bp)              476                                                          
#predicted genes (>= 3000 bp)              32                                                           
#mismatches                                620                                                          
#indels                                    2674                                                         
Indels length                              2772                                                         
#mismatches per 100 kbp                    11.97                                                        
#indels per 100 kbp                        51.61                                                        
GC (%)                                     50.28                                                        
Reference GC (%)                           50.48                                                        
Average %IDY                               99.297                                                       
Largest alignment                          61784                                                        
NA50                                       10728                                                        
NGA50                                      10210                                                        
NA75                                       5431                                                         
NGA75                                      4805                                                         
LA50                                       149                                                          
LGA50                                      161                                                          
LA75                                       321                                                          
LGA75                                      355                                                          
#Mis_misassemblies                         29                                                           
#Mis_relocations                           29                                                           
#Mis_translocations                        0                                                            
#Mis_inversions                            0                                                            
#Mis_misassembled contigs                  29                                                           
Mis_Misassembled contigs length            368948                                                       
#Mis_local misassemblies                   12                                                           
#Mis_short indels (<= 5 bp)                2673                                                         
#Mis_long indels (> 5 bp)                  1                                                            
#Una_fully unaligned contigs               0                                                            
Una_Fully unaligned length                 0                                                            
#Una_partially unaligned contigs           4                                                            
#Una_with misassembly                      0                                                            
#Una_both parts are significant            0                                                            
Una_Partially unaligned length             198                                                          
GAGE_Contigs #                             1488                                                         
GAGE_Min contig                            200                                                          
GAGE_Max contig                            61784                                                        
GAGE_N50                                   10554 COUNT: 154                                             
GAGE_Genome size                           5594470                                                      
GAGE_Assembly size                         5360090                                                      
GAGE_Chaff bases                           0                                                            
GAGE_Missing reference bases               110748(1.98%)                                                
GAGE_Missing assembly bases                610(0.01%)                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                     
GAGE_Duplicated reference bases            35073                                                        
GAGE_Compressed reference bases            263957                                                       
GAGE_Bad trim                              610                                                          
GAGE_Avg idy                               99.93                                                        
GAGE_SNPs                                  511                                                          
GAGE_Indels < 5bp                          2453                                                         
GAGE_Indels >= 5                           9                                                            
GAGE_Inversions                            7                                                            
GAGE_Relocation                            12                                                           
GAGE_Translocation                         0                                                            
GAGE_Corrected contig #                    1410                                                         
GAGE_Corrected assembly size               5326838                                                      
GAGE_Min correct contig                    200                                                          
GAGE_Max correct contig                    61781                                                        
GAGE_Corrected N50                         10147 COUNT: 164                                             
