All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.3percent_out.unpadded
#Contigs (>= 0 bp)             524                                                                
#Contigs (>= 1000 bp)          153                                                                
Total length (>= 0 bp)         5743716                                                            
Total length (>= 1000 bp)      5569821                                                            
#Contigs                       502                                                                
Largest contig                 463068                                                             
Total length                   5740379                                                            
Reference length               5594470                                                            
GC (%)                         50.43                                                              
Reference GC (%)               50.48                                                              
N50                            165270                                                             
NG50                           165270                                                             
N75                            58738                                                              
NG75                           68638                                                              
#misassemblies                 45                                                                 
#local misassemblies           22                                                                 
#unaligned contigs             0 + 3 part                                                         
Unaligned contigs length       162                                                                
Genome fraction (%)            98.323                                                             
Duplication ratio              1.038                                                              
#N's per 100 kbp               0.54                                                               
#mismatches per 100 kbp        27.02                                                              
#indels per 100 kbp            6.07                                                               
#genes                         5294 + 102 part                                                    
#predicted genes (unique)      5844                                                               
#predicted genes (>= 0 bp)     5915                                                               
#predicted genes (>= 300 bp)   4790                                                               
#predicted genes (>= 1500 bp)  685                                                                
#predicted genes (>= 3000 bp)  66                                                                 
Largest alignment              463068                                                             
NA50                           140923                                                             
NGA50                          140923                                                             
NA75                           52877                                                              
NGA75                          57350                                                              
