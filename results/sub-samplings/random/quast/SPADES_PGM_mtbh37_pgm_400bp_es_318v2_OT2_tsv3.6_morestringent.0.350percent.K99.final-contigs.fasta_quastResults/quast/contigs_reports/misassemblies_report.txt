All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.350percent.K99.final-contigs
#Mis_misassemblies               7                                                                                           
#Mis_relocations                 7                                                                                           
#Mis_translocations              0                                                                                           
#Mis_inversions                  0                                                                                           
#Mis_misassembled contigs        7                                                                                           
Mis_Misassembled contigs length  185958                                                                                      
#Mis_local misassemblies         30                                                                                          
#mismatches                      391                                                                                         
#indels                          2130                                                                                        
#Mis_short indels (<= 5 bp)      2118                                                                                        
#Mis_long indels (> 5 bp)        12                                                                                          
Indels length                    2738                                                                                        
