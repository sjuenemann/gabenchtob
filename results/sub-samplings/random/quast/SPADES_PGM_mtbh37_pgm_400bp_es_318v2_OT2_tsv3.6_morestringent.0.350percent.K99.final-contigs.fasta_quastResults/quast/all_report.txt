All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.350percent.K99.final-contigs
#Contigs                                   187                                                                                         
#Contigs (>= 0 bp)                         211                                                                                         
#Contigs (>= 1000 bp)                      167                                                                                         
Largest contig                             157977                                                                                      
Total length                               4277109                                                                                     
Total length (>= 0 bp)                     4279929                                                                                     
Total length (>= 1000 bp)                  4264864                                                                                     
Reference length                           4411532                                                                                     
N50                                        51534                                                                                       
NG50                                       50738                                                                                       
N75                                        25250                                                                                       
NG75                                       23525                                                                                       
L50                                        26                                                                                          
LG50                                       27                                                                                          
L75                                        56                                                                                          
LG75                                       60                                                                                          
#local misassemblies                       30                                                                                          
#misassemblies                             7                                                                                           
#misassembled contigs                      7                                                                                           
Misassembled contigs length                185958                                                                                      
Misassemblies inter-contig overlap         25263                                                                                       
#unaligned contigs                         1 + 0 part                                                                                  
Unaligned contigs length                   202                                                                                         
#ambiguously mapped contigs                16                                                                                          
Extra bases in ambiguously mapped contigs  -11549                                                                                      
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        96.730                                                                                      
Duplication ratio                          1.005                                                                                       
#genes                                     3908 + 142 part                                                                             
#predicted genes (unique)                  4675                                                                                        
#predicted genes (>= 0 bp)                 4681                                                                                        
#predicted genes (>= 300 bp)               3831                                                                                        
#predicted genes (>= 1500 bp)              427                                                                                         
#predicted genes (>= 3000 bp)              31                                                                                          
#mismatches                                391                                                                                         
#indels                                    2130                                                                                        
Indels length                              2738                                                                                        
#mismatches per 100 kbp                    9.16                                                                                        
#indels per 100 kbp                        49.91                                                                                       
GC (%)                                     65.35                                                                                       
Reference GC (%)                           65.61                                                                                       
Average %IDY                               99.754                                                                                      
Largest alignment                          157966                                                                                      
NA50                                       46871                                                                                       
NGA50                                      46601                                                                                       
NA75                                       25119                                                                                       
NGA75                                      23130                                                                                       
LA50                                       27                                                                                          
LGA50                                      28                                                                                          
LA75                                       57                                                                                          
LGA75                                      61                                                                                          
#Mis_misassemblies                         7                                                                                           
#Mis_relocations                           7                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  7                                                                                           
Mis_Misassembled contigs length            185958                                                                                      
#Mis_local misassemblies                   30                                                                                          
#Mis_short indels (<= 5 bp)                2118                                                                                        
#Mis_long indels (> 5 bp)                  12                                                                                          
#Una_fully unaligned contigs               1                                                                                           
Una_Fully unaligned length                 202                                                                                         
#Una_partially unaligned contigs           0                                                                                           
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             0                                                                                           
GAGE_Contigs #                             187                                                                                         
GAGE_Min contig                            202                                                                                         
GAGE_Max contig                            157977                                                                                      
GAGE_N50                                   50738 COUNT: 27                                                                             
GAGE_Genome size                           4411532                                                                                     
GAGE_Assembly size                         4277109                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               92700(2.10%)                                                                                
GAGE_Missing assembly bases                378(0.01%)                                                                                  
GAGE_Missing assembly contigs              1(0.53%)                                                                                    
GAGE_Duplicated reference bases            409                                                                                         
GAGE_Compressed reference bases            51727                                                                                       
GAGE_Bad trim                              174                                                                                         
GAGE_Avg idy                               99.94                                                                                       
GAGE_SNPs                                  265                                                                                         
GAGE_Indels < 5bp                          2157                                                                                        
GAGE_Indels >= 5                           25                                                                                          
GAGE_Inversions                            3                                                                                           
GAGE_Relocation                            14                                                                                          
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    229                                                                                         
GAGE_Corrected assembly size               4283836                                                                                     
GAGE_Min correct contig                    220                                                                                         
GAGE_Max correct contig                    128771                                                                                      
GAGE_Corrected N50                         40074 COUNT: 35                                                                             
