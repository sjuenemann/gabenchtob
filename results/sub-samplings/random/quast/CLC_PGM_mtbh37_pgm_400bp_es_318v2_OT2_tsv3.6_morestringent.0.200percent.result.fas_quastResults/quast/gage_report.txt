All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.200percent.result
GAGE_Contigs #                   1251                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  49364                                                                         
GAGE_N50                         9678 COUNT: 134                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4270348                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     186853(4.24%)                                                                 
GAGE_Missing assembly bases      2185(0.05%)                                                                   
GAGE_Missing assembly contigs    2(0.16%)                                                                      
GAGE_Duplicated reference bases  46962                                                                         
GAGE_Compressed reference bases  50545                                                                         
GAGE_Bad trim                    1501                                                                          
GAGE_Avg idy                     99.92                                                                         
GAGE_SNPs                        365                                                                           
GAGE_Indels < 5bp                2441                                                                          
GAGE_Indels >= 5                 8                                                                             
GAGE_Inversions                  11                                                                            
GAGE_Relocation                  5                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          1119                                                                          
GAGE_Corrected assembly size     4226023                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          36953                                                                         
GAGE_Corrected N50               9292 COUNT: 143                                                               
