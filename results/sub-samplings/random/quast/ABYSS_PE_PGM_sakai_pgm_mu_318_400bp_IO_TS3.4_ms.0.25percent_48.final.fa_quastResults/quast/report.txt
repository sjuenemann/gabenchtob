All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.25percent_48.final
#Contigs (>= 0 bp)             5187                                                                
#Contigs (>= 1000 bp)          479                                                                 
Total length (>= 0 bp)         5605201                                                             
Total length (>= 1000 bp)      5063404                                                             
#Contigs                       914                                                                 
Largest contig                 80681                                                               
Total length                   5245442                                                             
Reference length               5594470                                                             
GC (%)                         50.23                                                               
Reference GC (%)               50.48                                                               
N50                            20156                                                               
NG50                           18712                                                               
N75                            9939                                                                
NG75                           8017                                                                
#misassemblies                 2                                                                   
#local misassemblies           6                                                                   
#unaligned contigs             0 + 0 part                                                          
Unaligned contigs length       0                                                                   
Genome fraction (%)            92.478                                                              
Duplication ratio              1.001                                                               
#N's per 100 kbp               0.00                                                                
#mismatches per 100 kbp        1.57                                                                
#indels per 100 kbp            5.32                                                                
#genes                         4526 + 455 part                                                     
#predicted genes (unique)      5622                                                                
#predicted genes (>= 0 bp)     5623                                                                
#predicted genes (>= 300 bp)   4509                                                                
#predicted genes (>= 1500 bp)  596                                                                 
#predicted genes (>= 3000 bp)  52                                                                  
Largest alignment              80681                                                               
NA50                           20156                                                               
NGA50                          18712                                                               
NA75                           9939                                                                
NGA75                          7986                                                                
