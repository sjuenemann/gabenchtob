All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.300percent.scf
GAGE_Contigs #                   283                                                                           
GAGE_Min contig                  1039                                                                          
GAGE_Max contig                  102511                                                                        
GAGE_N50                         25887 COUNT: 53                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4265725                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     138026(3.13%)                                                                 
GAGE_Missing assembly bases      907(0.02%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  735                                                                           
GAGE_Compressed reference bases  34385                                                                         
GAGE_Bad trim                    907                                                                           
GAGE_Avg idy                     99.97                                                                         
GAGE_SNPs                        95                                                                            
GAGE_Indels < 5bp                1041                                                                          
GAGE_Indels >= 5                 15                                                                            
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  9                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          307                                                                           
GAGE_Corrected assembly size     4267445                                                                       
GAGE_Min correct contig          309                                                                           
GAGE_Max correct contig          102530                                                                        
GAGE_Corrected N50               24548 COUNT: 57                                                               
