All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent_out.unpadded
#Mis_misassemblies               2483                                                                    
#Mis_relocations                 238                                                                     
#Mis_translocations              7                                                                       
#Mis_inversions                  2238                                                                    
#Mis_misassembled contigs        2215                                                                    
Mis_Misassembled contigs length  1028811                                                                 
#Mis_local misassemblies         201                                                                     
#mismatches                      234                                                                     
#indels                          332                                                                     
#Mis_short indels (<= 5 bp)      326                                                                     
#Mis_long indels (> 5 bp)        6                                                                       
Indels length                    505                                                                     
