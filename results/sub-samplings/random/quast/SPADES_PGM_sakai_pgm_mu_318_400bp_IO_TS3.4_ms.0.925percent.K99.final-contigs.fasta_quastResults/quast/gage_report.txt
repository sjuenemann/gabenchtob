All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent.K99.final-contigs
GAGE_Contigs #                   424                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  316884                                                                      
GAGE_N50                         111453 COUNT: 16                                                            
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               5375604                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     9369(0.17%)                                                                 
GAGE_Missing assembly bases      69(0.00%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  1393                                                                        
GAGE_Compressed reference bases  275823                                                                      
GAGE_Bad trim                    69                                                                          
GAGE_Avg idy                     99.99                                                                       
GAGE_SNPs                        204                                                                         
GAGE_Indels < 5bp                273                                                                         
GAGE_Indels >= 5                 10                                                                          
GAGE_Inversions                  0                                                                           
GAGE_Relocation                  6                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          436                                                                         
GAGE_Corrected assembly size     5376722                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          238374                                                                      
GAGE_Corrected N50               99753 COUNT: 18                                                             
