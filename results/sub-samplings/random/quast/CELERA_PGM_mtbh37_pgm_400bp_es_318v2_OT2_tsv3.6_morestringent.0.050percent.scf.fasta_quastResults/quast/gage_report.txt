All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.050percent.scf
GAGE_Contigs #                   773                                                                           
GAGE_Min contig                  1000                                                                          
GAGE_Max contig                  6685                                                                          
GAGE_N50                         0 COUNT: 0                                                                    
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               1330528                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     3061126(69.39%)                                                               
GAGE_Missing assembly bases      1224(0.09%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  0                                                                             
GAGE_Compressed reference bases  23481                                                                         
GAGE_Bad trim                    1222                                                                          
GAGE_Avg idy                     99.91                                                                         
GAGE_SNPs                        42                                                                            
GAGE_Indels < 5bp                1102                                                                          
GAGE_Indels >= 5                 3                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  1                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          778                                                                           
GAGE_Corrected assembly size     1330061                                                                       
GAGE_Min correct contig          204                                                                           
GAGE_Max correct contig          6688                                                                          
GAGE_Corrected N50               0 COUNT: 0                                                                    
