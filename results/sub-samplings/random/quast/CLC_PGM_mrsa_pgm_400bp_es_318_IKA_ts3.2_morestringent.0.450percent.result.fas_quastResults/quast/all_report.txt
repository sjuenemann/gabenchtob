All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.450percent.result
#Contigs                                   927                                                                      
#Contigs (>= 0 bp)                         927                                                                      
#Contigs (>= 1000 bp)                      185                                                                      
Largest contig                             83809                                                                    
Total length                               2928179                                                                  
Total length (>= 0 bp)                     2928179                                                                  
Total length (>= 1000 bp)                  2695408                                                                  
Reference length                           2813862                                                                  
N50                                        21569                                                                    
NG50                                       21723                                                                    
N75                                        11747                                                                    
NG75                                       12674                                                                    
L50                                        42                                                                       
LG50                                       40                                                                       
L75                                        89                                                                       
LG75                                       82                                                                       
#local misassemblies                       4                                                                        
#misassemblies                             4                                                                        
#misassembled contigs                      4                                                                        
Misassembled contigs length                66328                                                                    
Misassemblies inter-contig overlap         974                                                                      
#unaligned contigs                         20 + 10 part                                                             
Unaligned contigs length                   5945                                                                     
#ambiguously mapped contigs                33                                                                       
Extra bases in ambiguously mapped contigs  -13388                                                                   
#N's                                       1657                                                                     
#N's per 100 kbp                           56.59                                                                    
Genome fraction (%)                        97.694                                                                   
Duplication ratio                          1.059                                                                    
#genes                                     2516 + 167 part                                                          
#predicted genes (unique)                  3308                                                                     
#predicted genes (>= 0 bp)                 3329                                                                     
#predicted genes (>= 300 bp)               2383                                                                     
#predicted genes (>= 1500 bp)              269                                                                      
#predicted genes (>= 3000 bp)              27                                                                       
#mismatches                                97                                                                       
#indels                                    519                                                                      
Indels length                              533                                                                      
#mismatches per 100 kbp                    3.53                                                                     
#indels per 100 kbp                        18.88                                                                    
GC (%)                                     32.60                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               99.293                                                                   
Largest alignment                          83505                                                                    
NA50                                       21154                                                                    
NGA50                                      21723                                                                    
NA75                                       11509                                                                    
NGA75                                      12520                                                                    
LA50                                       43                                                                       
LGA50                                      40                                                                       
LA75                                       90                                                                       
LGA75                                      83                                                                       
#Mis_misassemblies                         4                                                                        
#Mis_relocations                           4                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  4                                                                        
Mis_Misassembled contigs length            66328                                                                    
#Mis_local misassemblies                   4                                                                        
#Mis_short indels (<= 5 bp)                519                                                                      
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               20                                                                       
Una_Fully unaligned length                 5136                                                                     
#Una_partially unaligned contigs           10                                                                       
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             809                                                                      
GAGE_Contigs #                             927                                                                      
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            83809                                                                    
GAGE_N50                                   21723 COUNT: 40                                                          
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         2928179                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               24314(0.86%)                                                             
GAGE_Missing assembly bases                5329(0.18%)                                                              
GAGE_Missing assembly contigs              17(1.83%)                                                                
GAGE_Duplicated reference bases            159136                                                                   
GAGE_Compressed reference bases            42593                                                                    
GAGE_Bad trim                              940                                                                      
GAGE_Avg idy                               99.98                                                                    
GAGE_SNPs                                  45                                                                       
GAGE_Indels < 5bp                          396                                                                      
GAGE_Indels >= 5                           2                                                                        
GAGE_Inversions                            1                                                                        
GAGE_Relocation                            1                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    395                                                                      
GAGE_Corrected assembly size               2765247                                                                  
GAGE_Min correct contig                    200                                                                      
GAGE_Max correct contig                    83515                                                                    
GAGE_Corrected N50                         21725 COUNT: 40                                                          
