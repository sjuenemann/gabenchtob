reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.400percent.result  | 76.9143236409       | 1.06617016887     | 2725        | 1017      | 2844      | None      | None      |
