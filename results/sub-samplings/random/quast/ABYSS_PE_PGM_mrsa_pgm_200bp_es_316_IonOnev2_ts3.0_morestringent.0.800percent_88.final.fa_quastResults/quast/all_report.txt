All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.800percent_88.final
#Contigs                                   335                                                                                  
#Contigs (>= 0 bp)                         3532                                                                                 
#Contigs (>= 1000 bp)                      245                                                                                  
Largest contig                             83054                                                                                
Total length                               2758374                                                                              
Total length (>= 0 bp)                     3170713                                                                              
Total length (>= 1000 bp)                  2717534                                                                              
Reference length                           2813862                                                                              
N50                                        20323                                                                                
NG50                                       19690                                                                                
N75                                        10554                                                                                
NG75                                       9916                                                                                 
L50                                        45                                                                                   
LG50                                       47                                                                                   
L75                                        94                                                                                   
LG75                                       98                                                                                   
#local misassemblies                       4                                                                                    
#misassemblies                             2                                                                                    
#misassembled contigs                      2                                                                                    
Misassembled contigs length                4179                                                                                 
Misassemblies inter-contig overlap         918                                                                                  
#unaligned contigs                         0 + 0 part                                                                           
Unaligned contigs length                   0                                                                                    
#ambiguously mapped contigs                10                                                                                   
Extra bases in ambiguously mapped contigs  -4294                                                                                
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        97.737                                                                               
Duplication ratio                          1.002                                                                                
#genes                                     2497 + 179 part                                                                      
#predicted genes (unique)                  2839                                                                                 
#predicted genes (>= 0 bp)                 2842                                                                                 
#predicted genes (>= 300 bp)               2346                                                                                 
#predicted genes (>= 1500 bp)              275                                                                                  
#predicted genes (>= 3000 bp)              20                                                                                   
#mismatches                                56                                                                                   
#indels                                    261                                                                                  
Indels length                              268                                                                                  
#mismatches per 100 kbp                    2.04                                                                                 
#indels per 100 kbp                        9.49                                                                                 
GC (%)                                     32.61                                                                                
Reference GC (%)                           32.81                                                                                
Average %IDY                               99.145                                                                               
Largest alignment                          83054                                                                                
NA50                                       20321                                                                                
NGA50                                      19690                                                                                
NA75                                       10554                                                                                
NGA75                                      9916                                                                                 
LA50                                       45                                                                                   
LGA50                                      47                                                                                   
LA75                                       94                                                                                   
LGA75                                      98                                                                                   
#Mis_misassemblies                         2                                                                                    
#Mis_relocations                           1                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            1                                                                                    
#Mis_misassembled contigs                  2                                                                                    
Mis_Misassembled contigs length            4179                                                                                 
#Mis_local misassemblies                   4                                                                                    
#Mis_short indels (<= 5 bp)                261                                                                                  
#Mis_long indels (> 5 bp)                  0                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           0                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             0                                                                                    
GAGE_Contigs #                             335                                                                                  
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            83054                                                                                
GAGE_N50                                   19690 COUNT: 47                                                                      
GAGE_Genome size                           2813862                                                                              
GAGE_Assembly size                         2758374                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               45874(1.63%)                                                                         
GAGE_Missing assembly bases                13(0.00%)                                                                            
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            1878                                                                                 
GAGE_Compressed reference bases            21099                                                                                
GAGE_Bad trim                              13                                                                                   
GAGE_Avg idy                               99.99                                                                                
GAGE_SNPs                                  30                                                                                   
GAGE_Indels < 5bp                          255                                                                                  
GAGE_Indels >= 5                           5                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            1                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    334                                                                                  
GAGE_Corrected assembly size               2757790                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    83064                                                                                
GAGE_Corrected N50                         19197 COUNT: 48                                                                      
