All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.800percent_88.final
GAGE_Contigs #                   335                                                                                  
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  83054                                                                                
GAGE_N50                         19690 COUNT: 47                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               2758374                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     45874(1.63%)                                                                         
GAGE_Missing assembly bases      13(0.00%)                                                                            
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  1878                                                                                 
GAGE_Compressed reference bases  21099                                                                                
GAGE_Bad trim                    13                                                                                   
GAGE_Avg idy                     99.99                                                                                
GAGE_SNPs                        30                                                                                   
GAGE_Indels < 5bp                255                                                                                  
GAGE_Indels >= 5                 5                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  1                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          334                                                                                  
GAGE_Corrected assembly size     2757790                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          83064                                                                                
GAGE_Corrected N50               19197 COUNT: 48                                                                      
