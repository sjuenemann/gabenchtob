All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.300percent.K99.final-contigs
GAGE_Contigs #                   115                                                                                    
GAGE_Min contig                  205                                                                                    
GAGE_Max contig                  236807                                                                                 
GAGE_N50                         112340 COUNT: 9                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2775912                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     19465(0.69%)                                                                           
GAGE_Missing assembly bases      13(0.00%)                                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  588                                                                                    
GAGE_Compressed reference bases  26695                                                                                  
GAGE_Bad trim                    13                                                                                     
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        52                                                                                     
GAGE_Indels < 5bp                452                                                                                    
GAGE_Indels >= 5                 8                                                                                      
GAGE_Inversions                  0                                                                                      
GAGE_Relocation                  2                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          124                                                                                    
GAGE_Corrected assembly size     2777384                                                                                
GAGE_Min correct contig          205                                                                                    
GAGE_Max correct contig          207893                                                                                 
GAGE_Corrected N50               80105 COUNT: 11                                                                        
