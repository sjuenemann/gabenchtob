All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent.scf
#Contigs                                   205                                                           
#Contigs (>= 0 bp)                         205                                                           
#Contigs (>= 1000 bp)                      205                                                           
Largest contig                             217914                                                        
Total length                               5218755                                                       
Total length (>= 0 bp)                     5218755                                                       
Total length (>= 1000 bp)                  5218755                                                       
Reference length                           5594470                                                       
N50                                        72455                                                         
NG50                                       66767                                                         
N75                                        38327                                                         
NG75                                       31007                                                         
L50                                        23                                                            
LG50                                       26                                                            
L75                                        47                                                            
LG75                                       55                                                            
#local misassemblies                       7                                                             
#misassemblies                             2                                                             
#misassembled contigs                      2                                                             
Misassembled contigs length                244284                                                        
Misassemblies inter-contig overlap         1614                                                          
#unaligned contigs                         0 + 0 part                                                    
Unaligned contigs length                   0                                                             
#ambiguously mapped contigs                7                                                             
Extra bases in ambiguously mapped contigs  -7733                                                         
#N's                                       0                                                             
#N's per 100 kbp                           0.00                                                          
Genome fraction (%)                        92.810                                                        
Duplication ratio                          1.004                                                         
#genes                                     4891 + 192 part                                               
#predicted genes (unique)                  5219                                                          
#predicted genes (>= 0 bp)                 5221                                                          
#predicted genes (>= 300 bp)               4406                                                          
#predicted genes (>= 1500 bp)              632                                                           
#predicted genes (>= 3000 bp)              61                                                            
#mismatches                                94                                                            
#indels                                    368                                                           
Indels length                              377                                                           
#mismatches per 100 kbp                    1.81                                                          
#indels per 100 kbp                        7.09                                                          
GC (%)                                     50.30                                                         
Reference GC (%)                           50.48                                                         
Average %IDY                               98.496                                                        
Largest alignment                          217914                                                        
NA50                                       67358                                                         
NGA50                                      66592                                                         
NA75                                       38327                                                         
NGA75                                      31007                                                         
LA50                                       25                                                            
LGA50                                      28                                                            
LA75                                       49                                                            
LGA75                                      57                                                            
#Mis_misassemblies                         2                                                             
#Mis_relocations                           2                                                             
#Mis_translocations                        0                                                             
#Mis_inversions                            0                                                             
#Mis_misassembled contigs                  2                                                             
Mis_Misassembled contigs length            244284                                                        
#Mis_local misassemblies                   7                                                             
#Mis_short indels (<= 5 bp)                368                                                           
#Mis_long indels (> 5 bp)                  0                                                             
#Una_fully unaligned contigs               0                                                             
Una_Fully unaligned length                 0                                                             
#Una_partially unaligned contigs           0                                                             
#Una_with misassembly                      0                                                             
#Una_both parts are significant            0                                                             
Una_Partially unaligned length             0                                                             
GAGE_Contigs #                             205                                                           
GAGE_Min contig                            1014                                                          
GAGE_Max contig                            217914                                                        
GAGE_N50                                   66767 COUNT: 26                                               
GAGE_Genome size                           5594470                                                       
GAGE_Assembly size                         5218755                                                       
GAGE_Chaff bases                           0                                                             
GAGE_Missing reference bases               320093(5.72%)                                                 
GAGE_Missing assembly bases                52(0.00%)                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                      
GAGE_Duplicated reference bases            1166                                                          
GAGE_Compressed reference bases            120809                                                        
GAGE_Bad trim                              52                                                            
GAGE_Avg idy                               99.99                                                         
GAGE_SNPs                                  47                                                            
GAGE_Indels < 5bp                          350                                                           
GAGE_Indels >= 5                           8                                                             
GAGE_Inversions                            1                                                             
GAGE_Relocation                            4                                                             
GAGE_Translocation                         0                                                             
GAGE_Corrected contig #                    217                                                           
GAGE_Corrected assembly size               5221840                                                       
GAGE_Min correct contig                    267                                                           
GAGE_Max correct contig                    217915                                                        
GAGE_Corrected N50                         66593 COUNT: 28                                               
