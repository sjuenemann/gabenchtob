All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.950percent.result
#Contigs                                   858                                                                           
#Contigs (>= 0 bp)                         858                                                                           
#Contigs (>= 1000 bp)                      331                                                                           
Largest contig                             65498                                                                         
Total length                               4346833                                                                       
Total length (>= 0 bp)                     4346833                                                                       
Total length (>= 1000 bp)                  4174841                                                                       
Reference length                           4411532                                                                       
N50                                        20231                                                                         
NG50                                       20177                                                                         
N75                                        10610                                                                         
NG75                                       10351                                                                         
L50                                        69                                                                            
LG50                                       70                                                                            
L75                                        141                                                                           
LG75                                       146                                                                           
#local misassemblies                       7                                                                             
#misassemblies                             18                                                                            
#misassembled contigs                      14                                                                            
Misassembled contigs length                141477                                                                        
Misassemblies inter-contig overlap         20740                                                                         
#unaligned contigs                         2 + 16 part                                                                   
Unaligned contigs length                   2023                                                                          
#ambiguously mapped contigs                22                                                                            
Extra bases in ambiguously mapped contigs  -15205                                                                        
#N's                                       276                                                                           
#N's per 100 kbp                           6.35                                                                          
Genome fraction (%)                        96.210                                                                        
Duplication ratio                          1.025                                                                         
#genes                                     3723 + 328 part                                                               
#predicted genes (unique)                  4946                                                                          
#predicted genes (>= 0 bp)                 4955                                                                          
#predicted genes (>= 300 bp)               3796                                                                          
#predicted genes (>= 1500 bp)              443                                                                           
#predicted genes (>= 3000 bp)              47                                                                            
#mismatches                                142                                                                           
#indels                                    1160                                                                          
Indels length                              1288                                                                          
#mismatches per 100 kbp                    3.35                                                                          
#indels per 100 kbp                        27.33                                                                         
GC (%)                                     65.35                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.595                                                                        
Largest alignment                          65498                                                                         
NA50                                       20177                                                                         
NGA50                                      19681                                                                         
NA75                                       10498                                                                         
NGA75                                      10182                                                                         
LA50                                       69                                                                            
LGA50                                      71                                                                            
LA75                                       143                                                                           
LGA75                                      148                                                                           
#Mis_misassemblies                         18                                                                            
#Mis_relocations                           18                                                                            
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  14                                                                            
Mis_Misassembled contigs length            141477                                                                        
#Mis_local misassemblies                   7                                                                             
#Mis_short indels (<= 5 bp)                1158                                                                          
#Mis_long indels (> 5 bp)                  2                                                                             
#Una_fully unaligned contigs               2                                                                             
Una_Fully unaligned length                 1214                                                                          
#Una_partially unaligned contigs           16                                                                            
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             809                                                                           
GAGE_Contigs #                             858                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            65498                                                                         
GAGE_N50                                   20177 COUNT: 70                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         4346833                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               109061(2.47%)                                                                 
GAGE_Missing assembly bases                2351(0.05%)                                                                   
GAGE_Missing assembly contigs              3(0.35%)                                                                      
GAGE_Duplicated reference bases            77764                                                                         
GAGE_Compressed reference bases            49623                                                                         
GAGE_Bad trim                              867                                                                           
GAGE_Avg idy                               99.97                                                                         
GAGE_SNPs                                  111                                                                           
GAGE_Indels < 5bp                          1112                                                                          
GAGE_Indels >= 5                           7                                                                             
GAGE_Inversions                            11                                                                            
GAGE_Relocation                            4                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    612                                                                           
GAGE_Corrected assembly size               4267626                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    61746                                                                         
GAGE_Corrected N50                         19176 COUNT: 72                                                               
