All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.850percent_88.final
#Contigs (>= 0 bp)             3648                                                                                 
#Contigs (>= 1000 bp)          264                                                                                  
Total length (>= 0 bp)         3183667                                                                              
Total length (>= 1000 bp)      2715553                                                                              
#Contigs                       357                                                                                  
Largest contig                 83057                                                                                
Total length                   2758252                                                                              
Reference length               2813862                                                                              
GC (%)                         32.61                                                                                
Reference GC (%)               32.81                                                                                
N50                            19585                                                                                
NG50                           19310                                                                                
N75                            9143                                                                                 
NG75                           8600                                                                                 
#misassemblies                 2                                                                                    
#local misassemblies           5                                                                                    
#unaligned contigs             0 + 0 part                                                                           
Unaligned contigs length       0                                                                                    
Genome fraction (%)            97.723                                                                               
Duplication ratio              1.002                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        2.04                                                                                 
#indels per 100 kbp            9.06                                                                                 
#genes                         2489 + 190 part                                                                      
#predicted genes (unique)      2860                                                                                 
#predicted genes (>= 0 bp)     2863                                                                                 
#predicted genes (>= 300 bp)   2351                                                                                 
#predicted genes (>= 1500 bp)  273                                                                                  
#predicted genes (>= 3000 bp)  21                                                                                   
Largest alignment              83057                                                                                
NA50                           19585                                                                                
NGA50                          19310                                                                                
NA75                           9143                                                                                 
NGA75                          8600                                                                                 
