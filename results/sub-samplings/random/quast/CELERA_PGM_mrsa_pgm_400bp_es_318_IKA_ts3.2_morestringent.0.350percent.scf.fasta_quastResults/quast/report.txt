All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.350percent.scf
#Contigs (>= 0 bp)             93                                                                       
#Contigs (>= 1000 bp)          93                                                                       
Total length (>= 0 bp)         2767180                                                                  
Total length (>= 1000 bp)      2767180                                                                  
#Contigs                       93                                                                       
Largest contig                 250048                                                                   
Total length                   2767180                                                                  
Reference length               2813862                                                                  
GC (%)                         32.64                                                                    
Reference GC (%)               32.81                                                                    
N50                            53584                                                                    
NG50                           53584                                                                    
N75                            29677                                                                    
NG75                           28355                                                                    
#misassemblies                 2                                                                        
#local misassemblies           3                                                                        
#unaligned contigs             0 + 0 part                                                               
Unaligned contigs length       0                                                                        
Genome fraction (%)            97.511                                                                   
Duplication ratio              1.009                                                                    
#N's per 100 kbp               0.00                                                                     
#mismatches per 100 kbp        3.75                                                                     
#indels per 100 kbp            16.58                                                                    
#genes                         2613 + 59 part                                                           
#predicted genes (unique)      2761                                                                     
#predicted genes (>= 0 bp)     2763                                                                     
#predicted genes (>= 300 bp)   2326                                                                     
#predicted genes (>= 1500 bp)  278                                                                      
#predicted genes (>= 3000 bp)  23                                                                       
Largest alignment              250048                                                                   
NA50                           53584                                                                    
NGA50                          47000                                                                    
NA75                           28105                                                                    
NGA75                          26758                                                                    
