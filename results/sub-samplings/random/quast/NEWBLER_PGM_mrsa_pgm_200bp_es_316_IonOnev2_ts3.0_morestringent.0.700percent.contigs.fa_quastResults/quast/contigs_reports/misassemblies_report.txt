All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.700percent.contigs
#Mis_misassemblies               14                                                                                 
#Mis_relocations                 2                                                                                  
#Mis_translocations              2                                                                                  
#Mis_inversions                  10                                                                                 
#Mis_misassembled contigs        14                                                                                 
Mis_Misassembled contigs length  16549                                                                              
#Mis_local misassemblies         5                                                                                  
#mismatches                      79                                                                                 
#indels                          300                                                                                
#Mis_short indels (<= 5 bp)      299                                                                                
#Mis_long indels (> 5 bp)        1                                                                                  
Indels length                    398                                                                                
