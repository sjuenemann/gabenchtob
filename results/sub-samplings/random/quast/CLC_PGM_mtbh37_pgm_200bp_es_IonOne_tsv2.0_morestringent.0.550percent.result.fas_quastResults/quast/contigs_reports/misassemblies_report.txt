All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.550percent.result
#Mis_misassemblies               731                                                                        
#Mis_relocations                 168                                                                        
#Mis_translocations              0                                                                          
#Mis_inversions                  563                                                                        
#Mis_misassembled contigs        686                                                                        
Mis_Misassembled contigs length  735966                                                                     
#Mis_local misassemblies         35                                                                         
#mismatches                      1784                                                                       
#indels                          5288                                                                       
#Mis_short indels (<= 5 bp)      5280                                                                       
#Mis_long indels (> 5 bp)        8                                                                          
Indels length                    5676                                                                       
