All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.450percent.K99.final-contigs
#Contigs                                   104                                                                                    
#Contigs (>= 0 bp)                         478                                                                                    
#Contigs (>= 1000 bp)                      58                                                                                     
Largest contig                             236826                                                                                 
Total length                               2778116                                                                                
Total length (>= 0 bp)                     2828853                                                                                
Total length (>= 1000 bp)                  2758067                                                                                
Reference length                           2813862                                                                                
N50                                        90520                                                                                  
NG50                                       90520                                                                                  
N75                                        46973                                                                                  
NG75                                       46973                                                                                  
L50                                        9                                                                                      
LG50                                       9                                                                                      
L75                                        19                                                                                     
LG75                                       19                                                                                     
#local misassemblies                       7                                                                                      
#misassemblies                             2                                                                                      
#misassembled contigs                      2                                                                                      
Misassembled contigs length                74670                                                                                  
Misassemblies inter-contig overlap         1737                                                                                   
#unaligned contigs                         1 + 0 part                                                                             
Unaligned contigs length                   223                                                                                    
#ambiguously mapped contigs                15                                                                                     
Extra bases in ambiguously mapped contigs  -10012                                                                                 
#N's                                       0                                                                                      
#N's per 100 kbp                           0.00                                                                                   
Genome fraction (%)                        98.283                                                                                 
Duplication ratio                          1.001                                                                                  
#genes                                     2658 + 42 part                                                                         
#predicted genes (unique)                  2705                                                                                   
#predicted genes (>= 0 bp)                 2707                                                                                   
#predicted genes (>= 300 bp)               2309                                                                                   
#predicted genes (>= 1500 bp)              285                                                                                    
#predicted genes (>= 3000 bp)              30                                                                                     
#mismatches                                169                                                                                    
#indels                                    277                                                                                    
Indels length                              345                                                                                    
#mismatches per 100 kbp                    6.11                                                                                   
#indels per 100 kbp                        10.02                                                                                  
GC (%)                                     32.68                                                                                  
Reference GC (%)                           32.81                                                                                  
Average %IDY                               98.716                                                                                 
Largest alignment                          236826                                                                                 
NA50                                       90520                                                                                  
NGA50                                      90520                                                                                  
NA75                                       46964                                                                                  
NGA75                                      46964                                                                                  
LA50                                       9                                                                                      
LGA50                                      9                                                                                      
LA75                                       19                                                                                     
LGA75                                      19                                                                                     
#Mis_misassemblies                         2                                                                                      
#Mis_relocations                           2                                                                                      
#Mis_translocations                        0                                                                                      
#Mis_inversions                            0                                                                                      
#Mis_misassembled contigs                  2                                                                                      
Mis_Misassembled contigs length            74670                                                                                  
#Mis_local misassemblies                   7                                                                                      
#Mis_short indels (<= 5 bp)                274                                                                                    
#Mis_long indels (> 5 bp)                  3                                                                                      
#Una_fully unaligned contigs               1                                                                                      
Una_Fully unaligned length                 223                                                                                    
#Una_partially unaligned contigs           0                                                                                      
#Una_with misassembly                      0                                                                                      
#Una_both parts are significant            0                                                                                      
Una_Partially unaligned length             0                                                                                      
GAGE_Contigs #                             104                                                                                    
GAGE_Min contig                            202                                                                                    
GAGE_Max contig                            236826                                                                                 
GAGE_N50                                   90520 COUNT: 9                                                                         
GAGE_Genome size                           2813862                                                                                
GAGE_Assembly size                         2778116                                                                                
GAGE_Chaff bases                           0                                                                                      
GAGE_Missing reference bases               7705(0.27%)                                                                            
GAGE_Missing assembly bases                37(0.00%)                                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                               
GAGE_Duplicated reference bases            546                                                                                    
GAGE_Compressed reference bases            38004                                                                                  
GAGE_Bad trim                              37                                                                                     
GAGE_Avg idy                               99.98                                                                                  
GAGE_SNPs                                  70                                                                                     
GAGE_Indels < 5bp                          263                                                                                    
GAGE_Indels >= 5                           8                                                                                      
GAGE_Inversions                            0                                                                                      
GAGE_Relocation                            3                                                                                      
GAGE_Translocation                         0                                                                                      
GAGE_Corrected contig #                    113                                                                                    
GAGE_Corrected assembly size               2779751                                                                                
GAGE_Min correct contig                    202                                                                                    
GAGE_Max correct contig                    207893                                                                                 
GAGE_Corrected N50                         80161 COUNT: 11                                                                        
