All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.850percent_out.unpadded
#Una_fully unaligned contigs      132                                                                                  
Una_Fully unaligned length        45468                                                                                
#Una_partially unaligned contigs  1859                                                                                 
#Una_with misassembly             0                                                                                    
#Una_both parts are significant   10                                                                                   
Una_Partially unaligned length    127881                                                                               
#N's                              8934                                                                                 
