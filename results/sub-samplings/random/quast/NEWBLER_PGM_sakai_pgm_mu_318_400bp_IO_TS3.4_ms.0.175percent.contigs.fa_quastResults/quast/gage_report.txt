All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent.contigs
GAGE_Contigs #                   307                                                                
GAGE_Min contig                  201                                                                
GAGE_Max contig                  316765                                                             
GAGE_N50                         99565 COUNT: 16                                                    
GAGE_Genome size                 5594470                                                            
GAGE_Assembly size               5352090                                                            
GAGE_Chaff bases                 0                                                                  
GAGE_Missing reference bases     27347(0.49%)                                                       
GAGE_Missing assembly bases      9(0.00%)                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                           
GAGE_Duplicated reference bases  2776                                                               
GAGE_Compressed reference bases  235245                                                             
GAGE_Bad trim                    9                                                                  
GAGE_Avg idy                     99.99                                                              
GAGE_SNPs                        82                                                                 
GAGE_Indels < 5bp                330                                                                
GAGE_Indels >= 5                 6                                                                  
GAGE_Inversions                  0                                                                  
GAGE_Relocation                  7                                                                  
GAGE_Translocation               0                                                                  
GAGE_Corrected contig #          311                                                                
GAGE_Corrected assembly size     5351141                                                            
GAGE_Min correct contig          201                                                                
GAGE_Max correct contig          313854                                                             
GAGE_Corrected N50               95327 COUNT: 19                                                    
