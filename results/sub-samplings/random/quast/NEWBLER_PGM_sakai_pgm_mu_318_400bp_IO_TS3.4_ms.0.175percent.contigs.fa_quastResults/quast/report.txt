All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent.contigs
#Contigs (>= 0 bp)             375                                                                
#Contigs (>= 1000 bp)          158                                                                
Total length (>= 0 bp)         5361756                                                            
Total length (>= 1000 bp)      5282765                                                            
#Contigs                       307                                                                
Largest contig                 316765                                                             
Total length                   5352090                                                            
Reference length               5594470                                                            
GC (%)                         50.30                                                              
Reference GC (%)               50.48                                                              
N50                            105963                                                             
NG50                           99565                                                              
N75                            53933                                                              
NG75                           46277                                                              
#misassemblies                 3                                                                  
#local misassemblies           10                                                                 
#unaligned contigs             0 + 0 part                                                         
Unaligned contigs length       0                                                                  
Genome fraction (%)            94.153                                                             
Duplication ratio              1.000                                                              
#N's per 100 kbp               0.00                                                               
#mismatches per 100 kbp        1.75                                                               
#indels per 100 kbp            5.92                                                               
#genes                         4945 + 165 part                                                    
#predicted genes (unique)      5385                                                               
#predicted genes (>= 0 bp)     5386                                                               
#predicted genes (>= 300 bp)   4515                                                               
#predicted genes (>= 1500 bp)  644                                                                
#predicted genes (>= 3000 bp)  61                                                                 
Largest alignment              313841                                                             
NA50                           105963                                                             
NGA50                          99565                                                              
NA75                           53933                                                              
NGA75                          46277                                                              
