All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent.contigs
#Contigs                                   307                                                                
#Contigs (>= 0 bp)                         375                                                                
#Contigs (>= 1000 bp)                      158                                                                
Largest contig                             316765                                                             
Total length                               5352090                                                            
Total length (>= 0 bp)                     5361756                                                            
Total length (>= 1000 bp)                  5282765                                                            
Reference length                           5594470                                                            
N50                                        105963                                                             
NG50                                       99565                                                              
N75                                        53933                                                              
NG75                                       46277                                                              
L50                                        15                                                                 
LG50                                       16                                                                 
L75                                        33                                                                 
LG75                                       37                                                                 
#local misassemblies                       10                                                                 
#misassemblies                             3                                                                  
#misassembled contigs                      3                                                                  
Misassembled contigs length                348492                                                             
Misassemblies inter-contig overlap         1535                                                               
#unaligned contigs                         0 + 0 part                                                         
Unaligned contigs length                   0                                                                  
#ambiguously mapped contigs                104                                                                
Extra bases in ambiguously mapped contigs  -85834                                                             
#N's                                       0                                                                  
#N's per 100 kbp                           0.00                                                               
Genome fraction (%)                        94.153                                                             
Duplication ratio                          1.000                                                              
#genes                                     4945 + 165 part                                                    
#predicted genes (unique)                  5385                                                               
#predicted genes (>= 0 bp)                 5386                                                               
#predicted genes (>= 300 bp)               4515                                                               
#predicted genes (>= 1500 bp)              644                                                                
#predicted genes (>= 3000 bp)              61                                                                 
#mismatches                                92                                                                 
#indels                                    312                                                                
Indels length                              321                                                                
#mismatches per 100 kbp                    1.75                                                               
#indels per 100 kbp                        5.92                                                               
GC (%)                                     50.30                                                              
Reference GC (%)                           50.48                                                              
Average %IDY                               98.732                                                             
Largest alignment                          313841                                                             
NA50                                       105963                                                             
NGA50                                      99565                                                              
NA75                                       53933                                                              
NGA75                                      46277                                                              
LA50                                       16                                                                 
LGA50                                      17                                                                 
LA75                                       34                                                                 
LGA75                                      38                                                                 
#Mis_misassemblies                         3                                                                  
#Mis_relocations                           3                                                                  
#Mis_translocations                        0                                                                  
#Mis_inversions                            0                                                                  
#Mis_misassembled contigs                  3                                                                  
Mis_Misassembled contigs length            348492                                                             
#Mis_local misassemblies                   10                                                                 
#Mis_short indels (<= 5 bp)                312                                                                
#Mis_long indels (> 5 bp)                  0                                                                  
#Una_fully unaligned contigs               0                                                                  
Una_Fully unaligned length                 0                                                                  
#Una_partially unaligned contigs           0                                                                  
#Una_with misassembly                      0                                                                  
#Una_both parts are significant            0                                                                  
Una_Partially unaligned length             0                                                                  
GAGE_Contigs #                             307                                                                
GAGE_Min contig                            201                                                                
GAGE_Max contig                            316765                                                             
GAGE_N50                                   99565 COUNT: 16                                                    
GAGE_Genome size                           5594470                                                            
GAGE_Assembly size                         5352090                                                            
GAGE_Chaff bases                           0                                                                  
GAGE_Missing reference bases               27347(0.49%)                                                       
GAGE_Missing assembly bases                9(0.00%)                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                           
GAGE_Duplicated reference bases            2776                                                               
GAGE_Compressed reference bases            235245                                                             
GAGE_Bad trim                              9                                                                  
GAGE_Avg idy                               99.99                                                              
GAGE_SNPs                                  82                                                                 
GAGE_Indels < 5bp                          330                                                                
GAGE_Indels >= 5                           6                                                                  
GAGE_Inversions                            0                                                                  
GAGE_Relocation                            7                                                                  
GAGE_Translocation                         0                                                                  
GAGE_Corrected contig #                    311                                                                
GAGE_Corrected assembly size               5351141                                                            
GAGE_Min correct contig                    201                                                                
GAGE_Max correct contig                    313854                                                             
GAGE_Corrected N50                         95327 COUNT: 19                                                    
