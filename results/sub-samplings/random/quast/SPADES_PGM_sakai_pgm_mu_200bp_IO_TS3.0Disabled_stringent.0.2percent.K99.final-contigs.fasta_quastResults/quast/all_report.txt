All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.2percent.K99.final-contigs
#Contigs                                   324                                                                                  
#Contigs (>= 0 bp)                         466                                                                                  
#Contigs (>= 1000 bp)                      193                                                                                  
Largest contig                             374821                                                                               
Total length                               5340762                                                                              
Total length (>= 0 bp)                     5360402                                                                              
Total length (>= 1000 bp)                  5281662                                                                              
Reference length                           5594470                                                                              
N50                                        99718                                                                                
NG50                                       96030                                                                                
N75                                        54980                                                                                
NG75                                       43350                                                                                
L50                                        16                                                                                   
LG50                                       18                                                                                   
L75                                        34                                                                                   
LG75                                       38                                                                                   
#local misassemblies                       15                                                                                   
#misassemblies                             7                                                                                    
#misassembled contigs                      7                                                                                    
Misassembled contigs length                506418                                                                               
Misassemblies inter-contig overlap         6506                                                                                 
#unaligned contigs                         0 + 1 part                                                                           
Unaligned contigs length                   58                                                                                   
#ambiguously mapped contigs                101                                                                                  
Extra bases in ambiguously mapped contigs  -76495                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.093                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     4920 + 178 part                                                                      
#predicted genes (unique)                  6146                                                                                 
#predicted genes (>= 0 bp)                 6157                                                                                 
#predicted genes (>= 300 bp)               4918                                                                                 
#predicted genes (>= 1500 bp)              486                                                                                  
#predicted genes (>= 3000 bp)              33                                                                                   
#mismatches                                829                                                                                  
#indels                                    2134                                                                                 
Indels length                              2226                                                                                 
#mismatches per 100 kbp                    15.75                                                                                
#indels per 100 kbp                        40.54                                                                                
GC (%)                                     50.29                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               98.800                                                                               
Largest alignment                          259641                                                                               
NA50                                       99718                                                                                
NGA50                                      96030                                                                                
NA75                                       54633                                                                                
NGA75                                      41180                                                                                
LA50                                       17                                                                                   
LGA50                                      19                                                                                   
LA75                                       35                                                                                   
LGA75                                      39                                                                                   
#Mis_misassemblies                         7                                                                                    
#Mis_relocations                           7                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  7                                                                                    
Mis_Misassembled contigs length            506418                                                                               
#Mis_local misassemblies                   15                                                                                   
#Mis_short indels (<= 5 bp)                2133                                                                                 
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           1                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             58                                                                                   
GAGE_Contigs #                             324                                                                                  
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            374821                                                                               
GAGE_N50                                   96030 COUNT: 18                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5340762                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               16662(0.30%)                                                                         
GAGE_Missing assembly bases                200(0.00%)                                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            2232                                                                                 
GAGE_Compressed reference bases            294898                                                                               
GAGE_Bad trim                              200                                                                                  
GAGE_Avg idy                               99.94                                                                                
GAGE_SNPs                                  663                                                                                  
GAGE_Indels < 5bp                          2217                                                                                 
GAGE_Indels >= 5                           12                                                                                   
GAGE_Inversions                            1                                                                                    
GAGE_Relocation                            9                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    342                                                                                  
GAGE_Corrected assembly size               5345691                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    226975                                                                               
GAGE_Corrected N50                         87714 COUNT: 21                                                                      
