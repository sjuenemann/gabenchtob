All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.100percent_34.final
#Contigs (>= 0 bp)             6229                                                                            
#Contigs (>= 1000 bp)          325                                                                             
Total length (>= 0 bp)         3018807                                                                         
Total length (>= 1000 bp)      2630766                                                                         
#Contigs                       505                                                                             
Largest contig                 45607                                                                           
Total length                   2717944                                                                         
Reference length               2813862                                                                         
GC (%)                         32.58                                                                           
Reference GC (%)               32.81                                                                           
N50                            13231                                                                           
NG50                           12571                                                                           
N75                            6324                                                                            
NG75                           5524                                                                            
#misassemblies                 1                                                                               
#local misassemblies           3                                                                               
#unaligned contigs             0 + 0 part                                                                      
Unaligned contigs length       0                                                                               
Genome fraction (%)            96.438                                                                          
Duplication ratio              1.001                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        2.28                                                                            
#indels per 100 kbp            6.34                                                                            
#genes                         2366 + 249 part                                                                 
#predicted genes (unique)      2863                                                                            
#predicted genes (>= 0 bp)     2863                                                                            
#predicted genes (>= 300 bp)   2343                                                                            
#predicted genes (>= 1500 bp)  261                                                                             
#predicted genes (>= 3000 bp)  24                                                                              
Largest alignment              45607                                                                           
NA50                           13231                                                                           
NGA50                          12571                                                                           
NA75                           6253                                                                            
NGA75                          5449                                                                            
