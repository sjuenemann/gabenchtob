All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.300percent.result
#Mis_misassemblies               1145                                                                       
#Mis_relocations                 363                                                                        
#Mis_translocations              0                                                                          
#Mis_inversions                  782                                                                        
#Mis_misassembled contigs        1020                                                                       
Mis_Misassembled contigs length  839362                                                                     
#Mis_local misassemblies         60                                                                         
#mismatches                      2557                                                                       
#indels                          6667                                                                       
#Mis_short indels (<= 5 bp)      6655                                                                       
#Mis_long indels (> 5 bp)        12                                                                         
Indels length                    7105                                                                       
