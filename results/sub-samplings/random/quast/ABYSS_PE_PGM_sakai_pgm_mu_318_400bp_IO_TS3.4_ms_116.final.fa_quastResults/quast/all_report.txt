All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_116.final
#Contigs                                   6840                                                     
#Contigs (>= 0 bp)                         10704                                                    
#Contigs (>= 1000 bp)                      1248                                                     
Largest contig                             22615                                                    
Total length                               6321764                                                  
Total length (>= 0 bp)                     6899200                                                  
Total length (>= 1000 bp)                  4852786                                                  
Reference length                           5594470                                                  
N50                                        3932                                                     
NG50                                       4569                                                     
N75                                        1223                                                     
NG75                                       2160                                                     
L50                                        462                                                      
LG50                                       376                                                      
L75                                        1149                                                     
LG75                                       817                                                      
#local misassemblies                       6                                                        
#misassemblies                             8                                                        
#misassembled contigs                      8                                                        
Misassembled contigs length                7427                                                     
Misassemblies inter-contig overlap         1122                                                     
#unaligned contigs                         0 + 4 part                                               
Unaligned contigs length                   185                                                      
#ambiguously mapped contigs                797                                                      
Extra bases in ambiguously mapped contigs  -209104                                                  
#N's                                       0                                                        
#N's per 100 kbp                           0.00                                                     
Genome fraction (%)                        94.583                                                   
Duplication ratio                          1.155                                                    
#genes                                     3709 + 1470 part                                         
#predicted genes (unique)                  10963                                                    
#predicted genes (>= 0 bp)                 11081                                                    
#predicted genes (>= 300 bp)               4840                                                     
#predicted genes (>= 1500 bp)              478                                                      
#predicted genes (>= 3000 bp)              32                                                       
#mismatches                                77                                                       
#indels                                    556                                                      
Indels length                              569                                                      
#mismatches per 100 kbp                    1.46                                                     
#indels per 100 kbp                        10.51                                                    
GC (%)                                     50.54                                                    
Reference GC (%)                           50.48                                                    
Average %IDY                               99.282                                                   
Largest alignment                          22615                                                    
NA50                                       3912                                                     
NGA50                                      4569                                                     
NA75                                       1208                                                     
NGA75                                      2158                                                     
LA50                                       462                                                      
LGA50                                      376                                                      
LA75                                       1152                                                     
LGA75                                      818                                                      
#Mis_misassemblies                         8                                                        
#Mis_relocations                           8                                                        
#Mis_translocations                        0                                                        
#Mis_inversions                            0                                                        
#Mis_misassembled contigs                  8                                                        
Mis_Misassembled contigs length            7427                                                     
#Mis_local misassemblies                   6                                                        
#Mis_short indels (<= 5 bp)                556                                                      
#Mis_long indels (> 5 bp)                  0                                                        
#Una_fully unaligned contigs               0                                                        
Una_Fully unaligned length                 0                                                        
#Una_partially unaligned contigs           4                                                        
#Una_with misassembly                      0                                                        
#Una_both parts are significant            0                                                        
Una_Partially unaligned length             185                                                      
GAGE_Contigs #                             6840                                                     
GAGE_Min contig                            200                                                      
GAGE_Max contig                            22615                                                    
GAGE_N50                                   4569 COUNT: 376                                          
GAGE_Genome size                           5594470                                                  
GAGE_Assembly size                         6321764                                                  
GAGE_Chaff bases                           0                                                        
GAGE_Missing reference bases               14580(0.26%)                                             
GAGE_Missing assembly bases                245(0.00%)                                               
GAGE_Missing assembly contigs              0(0.00%)                                                 
GAGE_Duplicated reference bases            683144                                                   
GAGE_Compressed reference bases            272497                                                   
GAGE_Bad trim                              245                                                      
GAGE_Avg idy                               99.98                                                    
GAGE_SNPs                                  52                                                       
GAGE_Indels < 5bp                          510                                                      
GAGE_Indels >= 5                           3                                                        
GAGE_Inversions                            0                                                        
GAGE_Relocation                            5                                                        
GAGE_Translocation                         0                                                        
GAGE_Corrected contig #                    3809                                                     
GAGE_Corrected assembly size               5636231                                                  
GAGE_Min correct contig                    200                                                      
GAGE_Max correct contig                    22614                                                    
GAGE_Corrected N50                         4569 COUNT: 377                                          
