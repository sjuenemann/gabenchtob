All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.900percent_out.unpadded
#Contigs (>= 0 bp)             223                                                                                                             
#Contigs (>= 1000 bp)          103                                                                                                             
Total length (>= 0 bp)         4444536                                                                                                         
Total length (>= 1000 bp)      4384163                                                                                                         
#Contigs                       212                                                                                                             
Largest contig                 433119                                                                                                          
Total length                   4443363                                                                                                         
Reference length               4411532                                                                                                         
GC (%)                         65.61                                                                                                           
Reference GC (%)               65.61                                                                                                           
N50                            91872                                                                                                           
NG50                           91872                                                                                                           
N75                            50081                                                                                                           
NG75                           50081                                                                                                           
#misassemblies                 43                                                                                                              
#local misassemblies           16                                                                                                              
#unaligned contigs             0 + 13 part                                                                                                     
Unaligned contigs length       1033                                                                                                            
Genome fraction (%)            99.518                                                                                                          
Duplication ratio              1.027                                                                                                           
#N's per 100 kbp               4.37                                                                                                            
#mismatches per 100 kbp        10.84                                                                                                           
#indels per 100 kbp            2.19                                                                                                            
#genes                         4004 + 106 part                                                                                                 
#predicted genes (unique)      4202                                                                                                            
#predicted genes (>= 0 bp)     4226                                                                                                            
#predicted genes (>= 300 bp)   3657                                                                                                            
#predicted genes (>= 1500 bp)  564                                                                                                             
#predicted genes (>= 3000 bp)  79                                                                                                              
Largest alignment              239919                                                                                                          
NA50                           64424                                                                                                           
NGA50                          64424                                                                                                           
NA75                           36185                                                                                                           
NGA75                          36214                                                                                                           
