All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.125percent_out.unpadded
#Una_fully unaligned contigs      1                                                                                                 
Una_Fully unaligned length        5622                                                                                              
#Una_partially unaligned contigs  2                                                                                                 
#Una_with misassembly             0                                                                                                 
#Una_both parts are significant   1                                                                                                 
Una_Partially unaligned length    602                                                                                               
#N's                              65                                                                                                
