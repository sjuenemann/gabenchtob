All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.65percent_out.unpadded
#Mis_misassemblies               109                                                                 
#Mis_relocations                 100                                                                 
#Mis_translocations              9                                                                   
#Mis_inversions                  0                                                                   
#Mis_misassembled contigs        77                                                                  
Mis_Misassembled contigs length  4110171                                                             
#Mis_local misassemblies         22                                                                  
#mismatches                      1626                                                                
#indels                          547                                                                 
#Mis_short indels (<= 5 bp)      546                                                                 
#Mis_long indels (> 5 bp)        1                                                                   
Indels length                    571                                                                 
