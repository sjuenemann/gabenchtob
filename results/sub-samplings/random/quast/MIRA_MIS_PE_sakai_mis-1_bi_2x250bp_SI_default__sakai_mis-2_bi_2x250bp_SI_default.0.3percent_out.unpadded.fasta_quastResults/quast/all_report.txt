All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.3percent_out.unpadded
#Contigs                                   411                                                                                                     
#Contigs (>= 0 bp)                         411                                                                                                     
#Contigs (>= 1000 bp)                      126                                                                                                     
Largest contig                             522601                                                                                                  
Total length                               5699906                                                                                                 
Total length (>= 0 bp)                     5699906                                                                                                 
Total length (>= 1000 bp)                  5558410                                                                                                 
Reference length                           5594470                                                                                                 
N50                                        163832                                                                                                  
NG50                                       163832                                                                                                  
N75                                        65144                                                                                                   
NG75                                       66588                                                                                                   
L50                                        11                                                                                                      
LG50                                       11                                                                                                      
L75                                        24                                                                                                      
LG75                                       23                                                                                                      
#local misassemblies                       10                                                                                                      
#misassemblies                             63                                                                                                      
#misassembled contigs                      34                                                                                                      
Misassembled contigs length                3017869                                                                                                 
Misassemblies inter-contig overlap         81841                                                                                                   
#unaligned contigs                         1 + 2 part                                                                                              
Unaligned contigs length                   5765                                                                                                    
#ambiguously mapped contigs                82                                                                                                      
Extra bases in ambiguously mapped contigs  -71535                                                                                                  
#N's                                       60                                                                                                      
#N's per 100 kbp                           1.05                                                                                                    
Genome fraction (%)                        98.577                                                                                                  
Duplication ratio                          1.034                                                                                                   
#genes                                     5303 + 95 part                                                                                          
#predicted genes (unique)                  5687                                                                                                    
#predicted genes (>= 0 bp)                 5738                                                                                                    
#predicted genes (>= 300 bp)               4788                                                                                                    
#predicted genes (>= 1500 bp)              701                                                                                                     
#predicted genes (>= 3000 bp)              76                                                                                                      
#mismatches                                2569                                                                                                    
#indels                                    110                                                                                                     
Indels length                              139                                                                                                     
#mismatches per 100 kbp                    46.58                                                                                                   
#indels per 100 kbp                        1.99                                                                                                    
GC (%)                                     50.50                                                                                                   
Reference GC (%)                           50.48                                                                                                   
Average %IDY                               98.661                                                                                                  
Largest alignment                          318548                                                                                                  
NA50                                       108981                                                                                                  
NGA50                                      124804                                                                                                  
NA75                                       55538                                                                                                   
NGA75                                      58676                                                                                                   
LA50                                       15                                                                                                      
LGA50                                      14                                                                                                      
LA75                                       32                                                                                                      
LGA75                                      31                                                                                                      
#Mis_misassemblies                         63                                                                                                      
#Mis_relocations                           61                                                                                                      
#Mis_translocations                        1                                                                                                       
#Mis_inversions                            1                                                                                                       
#Mis_misassembled contigs                  34                                                                                                      
Mis_Misassembled contigs length            3017869                                                                                                 
#Mis_local misassemblies                   10                                                                                                      
#Mis_short indels (<= 5 bp)                108                                                                                                     
#Mis_long indels (> 5 bp)                  2                                                                                                       
#Una_fully unaligned contigs               1                                                                                                       
Una_Fully unaligned length                 5610                                                                                                    
#Una_partially unaligned contigs           2                                                                                                       
#Una_with misassembly                      0                                                                                                       
#Una_both parts are significant            0                                                                                                       
Una_Partially unaligned length             155                                                                                                     
GAGE_Contigs #                             411                                                                                                     
GAGE_Min contig                            215                                                                                                     
GAGE_Max contig                            522601                                                                                                  
GAGE_N50                                   163832 COUNT: 11                                                                                        
GAGE_Genome size                           5594470                                                                                                 
GAGE_Assembly size                         5699906                                                                                                 
GAGE_Chaff bases                           0                                                                                                       
GAGE_Missing reference bases               497(0.01%)                                                                                              
GAGE_Missing assembly bases                6038(0.11%)                                                                                             
GAGE_Missing assembly contigs              1(0.24%)                                                                                                
GAGE_Duplicated reference bases            214286                                                                                                  
GAGE_Compressed reference bases            135473                                                                                                  
GAGE_Bad trim                              422                                                                                                     
GAGE_Avg idy                               99.95                                                                                                   
GAGE_SNPs                                  490                                                                                                     
GAGE_Indels < 5bp                          36                                                                                                      
GAGE_Indels >= 5                           4                                                                                                       
GAGE_Inversions                            19                                                                                                      
GAGE_Relocation                            11                                                                                                      
GAGE_Translocation                         0                                                                                                       
GAGE_Corrected contig #                    157                                                                                                     
GAGE_Corrected assembly size               5569587                                                                                                 
GAGE_Min correct contig                    287                                                                                                     
GAGE_Max correct contig                    318548                                                                                                  
GAGE_Corrected N50                         128052 COUNT: 14                                                                                        
