All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.450percent_out.unpadded
#Mis_misassemblies               27                                                                                   
#Mis_relocations                 27                                                                                   
#Mis_translocations              0                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        24                                                                                   
Mis_Misassembled contigs length  944271                                                                               
#Mis_local misassemblies         24                                                                                   
#mismatches                      423                                                                                  
#indels                          728                                                                                  
#Mis_short indels (<= 5 bp)      725                                                                                  
#Mis_long indels (> 5 bp)        3                                                                                    
Indels length                    895                                                                                  
