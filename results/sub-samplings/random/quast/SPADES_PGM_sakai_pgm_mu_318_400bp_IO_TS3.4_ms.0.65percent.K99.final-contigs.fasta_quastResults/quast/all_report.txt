All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.65percent.K99.final-contigs
#Contigs                                   430                                                                        
#Contigs (>= 0 bp)                         693                                                                        
#Contigs (>= 1000 bp)                      163                                                                        
Largest contig                             316886                                                                     
Total length                               5379995                                                                    
Total length (>= 0 bp)                     5416409                                                                    
Total length (>= 1000 bp)                  5256472                                                                    
Reference length                           5594470                                                                    
N50                                        146550                                                                     
NG50                                       142119                                                                     
N75                                        56375                                                                      
NG75                                       42995                                                                      
L50                                        13                                                                         
LG50                                       14                                                                         
L75                                        28                                                                         
LG75                                       32                                                                         
#local misassemblies                       13                                                                         
#misassemblies                             3                                                                          
#misassembled contigs                      3                                                                          
Misassembled contigs length                360374                                                                     
Misassemblies inter-contig overlap         2180                                                                       
#unaligned contigs                         0 + 0 part                                                                 
Unaligned contigs length                   0                                                                          
#ambiguously mapped contigs                146                                                                        
Extra bases in ambiguously mapped contigs  -91558                                                                     
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        94.401                                                                     
Duplication ratio                          1.002                                                                      
#genes                                     4925 + 202 part                                                            
#predicted genes (unique)                  5490                                                                       
#predicted genes (>= 0 bp)                 5507                                                                       
#predicted genes (>= 300 bp)               4535                                                                       
#predicted genes (>= 1500 bp)              649                                                                        
#predicted genes (>= 3000 bp)              62                                                                         
#mismatches                                282                                                                        
#indels                                    283                                                                        
Indels length                              292                                                                        
#mismatches per 100 kbp                    5.34                                                                       
#indels per 100 kbp                        5.36                                                                       
GC (%)                                     50.32                                                                      
Reference GC (%)                           50.48                                                                      
Average %IDY                               98.824                                                                     
Largest alignment                          311133                                                                     
NA50                                       145548                                                                     
NGA50                                      142119                                                                     
NA75                                       56375                                                                      
NGA75                                      41260                                                                      
LA50                                       14                                                                         
LGA50                                      15                                                                         
LA75                                       29                                                                         
LGA75                                      33                                                                         
#Mis_misassemblies                         3                                                                          
#Mis_relocations                           3                                                                          
#Mis_translocations                        0                                                                          
#Mis_inversions                            0                                                                          
#Mis_misassembled contigs                  3                                                                          
Mis_Misassembled contigs length            360374                                                                     
#Mis_local misassemblies                   13                                                                         
#Mis_short indels (<= 5 bp)                283                                                                        
#Mis_long indels (> 5 bp)                  0                                                                          
#Una_fully unaligned contigs               0                                                                          
Una_Fully unaligned length                 0                                                                          
#Una_partially unaligned contigs           0                                                                          
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             0                                                                          
GAGE_Contigs #                             430                                                                        
GAGE_Min contig                            201                                                                        
GAGE_Max contig                            316886                                                                     
GAGE_N50                                   142119 COUNT: 14                                                           
GAGE_Genome size                           5594470                                                                    
GAGE_Assembly size                         5379995                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               6864(0.12%)                                                                
GAGE_Missing assembly bases                67(0.00%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                   
GAGE_Duplicated reference bases            2149                                                                       
GAGE_Compressed reference bases            276984                                                                     
GAGE_Bad trim                              67                                                                         
GAGE_Avg idy                               99.99                                                                      
GAGE_SNPs                                  217                                                                        
GAGE_Indels < 5bp                          287                                                                        
GAGE_Indels >= 5                           12                                                                         
GAGE_Inversions                            0                                                                          
GAGE_Relocation                            7                                                                          
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    441                                                                        
GAGE_Corrected assembly size               5380586                                                                    
GAGE_Min correct contig                    201                                                                        
GAGE_Max correct contig                    224265                                                                     
GAGE_Corrected N50                         111195 COUNT: 18                                                           
