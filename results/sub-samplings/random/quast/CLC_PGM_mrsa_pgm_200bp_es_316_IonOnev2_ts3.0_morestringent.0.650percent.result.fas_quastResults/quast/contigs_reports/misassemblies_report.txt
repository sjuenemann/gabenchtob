All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.650percent.result
#Mis_misassemblies               9                                                                             
#Mis_relocations                 3                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  6                                                                             
#Mis_misassembled contigs        9                                                                             
Mis_Misassembled contigs length  97491                                                                         
#Mis_local misassemblies         0                                                                             
#mismatches                      44                                                                            
#indels                          317                                                                           
#Mis_short indels (<= 5 bp)      317                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    329                                                                           
