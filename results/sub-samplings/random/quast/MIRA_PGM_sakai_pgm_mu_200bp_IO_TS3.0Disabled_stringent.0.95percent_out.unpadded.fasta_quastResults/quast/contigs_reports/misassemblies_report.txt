All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.95percent_out.unpadded
#Mis_misassemblies               801                                                                            
#Mis_relocations                 68                                                                             
#Mis_translocations              2                                                                              
#Mis_inversions                  731                                                                            
#Mis_misassembled contigs        716                                                                            
Mis_Misassembled contigs length  1634650                                                                        
#Mis_local misassemblies         79                                                                             
#mismatches                      1761                                                                           
#indels                          377                                                                            
#Mis_short indels (<= 5 bp)      375                                                                            
#Mis_long indels (> 5 bp)        2                                                                              
Indels length                    438                                                                            
