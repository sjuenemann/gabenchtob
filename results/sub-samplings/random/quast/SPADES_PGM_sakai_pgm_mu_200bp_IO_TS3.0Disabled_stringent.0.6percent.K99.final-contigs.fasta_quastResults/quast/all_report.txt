All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.6percent.K99.final-contigs
#Contigs                                   427                                                                                  
#Contigs (>= 0 bp)                         680                                                                                  
#Contigs (>= 1000 bp)                      174                                                                                  
Largest contig                             374859                                                                               
Total length                               5378569                                                                              
Total length (>= 0 bp)                     5413815                                                                              
Total length (>= 1000 bp)                  5262948                                                                              
Reference length                           5594470                                                                              
N50                                        142468                                                                               
NG50                                       140503                                                                               
N75                                        50269                                                                                
NG75                                       43331                                                                                
L50                                        14                                                                                   
LG50                                       15                                                                                   
L75                                        29                                                                                   
LG75                                       32                                                                                   
#local misassemblies                       9                                                                                    
#misassemblies                             5                                                                                    
#misassembled contigs                      5                                                                                    
Misassembled contigs length                853596                                                                               
Misassemblies inter-contig overlap         1604                                                                                 
#unaligned contigs                         0 + 0 part                                                                           
Unaligned contigs length                   0                                                                                    
#ambiguously mapped contigs                138                                                                                  
Extra bases in ambiguously mapped contigs  -86331                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.488                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     4948 + 204 part                                                                      
#predicted genes (unique)                  5992                                                                                 
#predicted genes (>= 0 bp)                 6011                                                                                 
#predicted genes (>= 300 bp)               4794                                                                                 
#predicted genes (>= 1500 bp)              538                                                                                  
#predicted genes (>= 3000 bp)              41                                                                                   
#mismatches                                201                                                                                  
#indels                                    1445                                                                                 
Indels length                              1506                                                                                 
#mismatches per 100 kbp                    3.80                                                                                 
#indels per 100 kbp                        27.34                                                                                
GC (%)                                     50.29                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               98.822                                                                               
Largest alignment                          259672                                                                               
NA50                                       127581                                                                               
NGA50                                      127581                                                                               
NA75                                       50269                                                                                
NGA75                                      41502                                                                                
LA50                                       16                                                                                   
LGA50                                      16                                                                                   
LA75                                       31                                                                                   
LGA75                                      35                                                                                   
#Mis_misassemblies                         5                                                                                    
#Mis_relocations                           5                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  5                                                                                    
Mis_Misassembled contigs length            853596                                                                               
#Mis_local misassemblies                   9                                                                                    
#Mis_short indels (<= 5 bp)                1444                                                                                 
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           0                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             0                                                                                    
GAGE_Contigs #                             427                                                                                  
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            374859                                                                               
GAGE_N50                                   140503 COUNT: 15                                                                     
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5378569                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               5743(0.10%)                                                                          
GAGE_Missing assembly bases                7(0.00%)                                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            1085                                                                                 
GAGE_Compressed reference bases            279775                                                                               
GAGE_Bad trim                              7                                                                                    
GAGE_Avg idy                               99.97                                                                                
GAGE_SNPs                                  172                                                                                  
GAGE_Indels < 5bp                          1513                                                                                 
GAGE_Indels >= 5                           7                                                                                    
GAGE_Inversions                            2                                                                                    
GAGE_Relocation                            7                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    439                                                                                  
GAGE_Corrected assembly size               5380966                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    226975                                                                               
GAGE_Corrected N50                         111195 COUNT: 18                                                                     
