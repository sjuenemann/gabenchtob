All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.200percent.scf
GAGE_Contigs #                   76                                                                       
GAGE_Min contig                  1034                                                                     
GAGE_Max contig                  247861                                                                   
GAGE_N50                         80725 COUNT: 12                                                          
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2758598                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     59262(2.11%)                                                             
GAGE_Missing assembly bases      15(0.00%)                                                                
GAGE_Missing assembly contigs    0(0.00%)                                                                 
GAGE_Duplicated reference bases  0                                                                        
GAGE_Compressed reference bases  19007                                                                    
GAGE_Bad trim                    15                                                                       
GAGE_Avg idy                     99.98                                                                    
GAGE_SNPs                        41                                                                       
GAGE_Indels < 5bp                400                                                                      
GAGE_Indels >= 5                 5                                                                        
GAGE_Inversions                  1                                                                        
GAGE_Relocation                  2                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          84                                                                       
GAGE_Corrected assembly size     2760373                                                                  
GAGE_Min correct contig          229                                                                      
GAGE_Max correct contig          247895                                                                   
GAGE_Corrected N50               73826 COUNT: 13                                                          
