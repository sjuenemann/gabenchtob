All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.990percent_out.unpadded
#Contigs (>= 0 bp)             8016                                                                            
#Contigs (>= 1000 bp)          1359                                                                            
Total length (>= 0 bp)         9398079                                                                         
Total length (>= 1000 bp)      5644662                                                                         
#Contigs                       7872                                                                            
Largest contig                 180987                                                                          
Total length                   9376200                                                                         
Reference length               2813862                                                                         
GC (%)                         32.81                                                                           
Reference GC (%)               32.81                                                                           
N50                            1804                                                                            
NG50                           51915                                                                           
N75                            687                                                                             
NG75                           22143                                                                           
#misassemblies                 528                                                                             
#local misassemblies           25                                                                              
#unaligned contigs             24 + 256 part                                                                   
Unaligned contigs length       28654                                                                           
Genome fraction (%)            99.819                                                                          
Duplication ratio              3.248                                                                           
#N's per 100 kbp               24.90                                                                           
#mismatches per 100 kbp        7.80                                                                            
#indels per 100 kbp            13.46                                                                           
#genes                         2677 + 49 part                                                                  
#predicted genes (unique)      15183                                                                           
#predicted genes (>= 0 bp)     16414                                                                           
#predicted genes (>= 300 bp)   7567                                                                            
#predicted genes (>= 1500 bp)  356                                                                             
#predicted genes (>= 3000 bp)  31                                                                              
Largest alignment              180987                                                                          
NA50                           1746                                                                            
NGA50                          51915                                                                           
NA75                           644                                                                             
NGA75                          20577                                                                           
