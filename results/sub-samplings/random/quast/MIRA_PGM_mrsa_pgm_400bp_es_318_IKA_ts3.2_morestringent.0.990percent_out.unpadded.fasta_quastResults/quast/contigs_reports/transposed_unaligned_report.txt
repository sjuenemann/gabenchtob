All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                          #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.990percent_out.unpadded  24                            12025                       256                               0                      7                                16629                           2335
