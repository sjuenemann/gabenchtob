All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent_out.unpadded
#Contigs (>= 0 bp)             559                                                                             
#Contigs (>= 1000 bp)          33                                                                              
Total length (>= 0 bp)         3117681                                                                         
Total length (>= 1000 bp)      2862714                                                                         
#Contigs                       528                                                                             
Largest contig                 1251950                                                                         
Total length                   3113419                                                                         
Reference length               2813862                                                                         
GC (%)                         32.85                                                                           
Reference GC (%)               32.81                                                                           
N50                            196573                                                                          
NG50                           235701                                                                          
N75                            136182                                                                          
NG75                           151489                                                                          
#misassemblies                 32                                                                              
#local misassemblies           17                                                                              
#unaligned contigs             3 + 8 part                                                                      
Unaligned contigs length       1829                                                                            
Genome fraction (%)            99.849                                                                          
Duplication ratio              1.107                                                                           
#N's per 100 kbp               4.95                                                                            
#mismatches per 100 kbp        8.36                                                                            
#indels per 100 kbp            5.55                                                                            
#genes                         2710 + 16 part                                                                  
#predicted genes (unique)      3317                                                                            
#predicted genes (>= 0 bp)     3347                                                                            
#predicted genes (>= 300 bp)   2448                                                                            
#predicted genes (>= 1500 bp)  300                                                                             
#predicted genes (>= 3000 bp)  28                                                                              
Largest alignment              1251950                                                                         
NA50                           196573                                                                          
NGA50                          235701                                                                          
NA75                           97611                                                                           
NGA75                          111488                                                                          
