All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.175percent.contigs
#Contigs                                   104                                                                                
#Contigs (>= 0 bp)                         133                                                                                
#Contigs (>= 1000 bp)                      81                                                                                 
Largest contig                             174988                                                                             
Total length                               2765644                                                                            
Total length (>= 0 bp)                     2769553                                                                            
Total length (>= 1000 bp)                  2753920                                                                            
Reference length                           2813862                                                                            
N50                                        54097                                                                              
NG50                                       52964                                                                              
N75                                        31960                                                                              
NG75                                       30731                                                                              
L50                                        13                                                                                 
LG50                                       14                                                                                 
L75                                        30                                                                                 
LG75                                       31                                                                                 
#local misassemblies                       6                                                                                  
#misassemblies                             3                                                                                  
#misassembled contigs                      3                                                                                  
Misassembled contigs length                48121                                                                              
Misassemblies inter-contig overlap         802                                                                                
#unaligned contigs                         0 + 0 part                                                                         
Unaligned contigs length                   0                                                                                  
#ambiguously mapped contigs                11                                                                                 
Extra bases in ambiguously mapped contigs  -9741                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.924                                                                             
Duplication ratio                          1.000                                                                              
#genes                                     2639 + 47 part                                                                     
#predicted genes (unique)                  2716                                                                               
#predicted genes (>= 0 bp)                 2716                                                                               
#predicted genes (>= 300 bp)               2327                                                                               
#predicted genes (>= 1500 bp)              285                                                                                
#predicted genes (>= 3000 bp)              25                                                                                 
#mismatches                                58                                                                                 
#indels                                    297                                                                                
Indels length                              350                                                                                
#mismatches per 100 kbp                    2.10                                                                               
#indels per 100 kbp                        10.78                                                                              
GC (%)                                     32.64                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               99.018                                                                             
Largest alignment                          174988                                                                             
NA50                                       54097                                                                              
NGA50                                      52964                                                                              
NA75                                       31960                                                                              
NGA75                                      30731                                                                              
LA50                                       13                                                                                 
LGA50                                      14                                                                                 
LA75                                       30                                                                                 
LGA75                                      31                                                                                 
#Mis_misassemblies                         3                                                                                  
#Mis_relocations                           2                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            1                                                                                  
#Mis_misassembled contigs                  3                                                                                  
Mis_Misassembled contigs length            48121                                                                              
#Mis_local misassemblies                   6                                                                                  
#Mis_short indels (<= 5 bp)                297                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               0                                                                                  
Una_Fully unaligned length                 0                                                                                  
#Una_partially unaligned contigs           0                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             0                                                                                  
GAGE_Contigs #                             104                                                                                
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            174988                                                                             
GAGE_N50                                   52964 COUNT: 14                                                                    
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2765644                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               12590(0.45%)                                                                       
GAGE_Missing assembly bases                61(0.00%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            358                                                                                
GAGE_Compressed reference bases            39109                                                                              
GAGE_Bad trim                              61                                                                                 
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  59                                                                                 
GAGE_Indels < 5bp                          347                                                                                
GAGE_Indels >= 5                           6                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            2                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    110                                                                                
GAGE_Corrected assembly size               2766265                                                                            
GAGE_Min correct contig                    287                                                                                
GAGE_Max correct contig                    175001                                                                             
GAGE_Corrected N50                         51643 COUNT: 16                                                                    
