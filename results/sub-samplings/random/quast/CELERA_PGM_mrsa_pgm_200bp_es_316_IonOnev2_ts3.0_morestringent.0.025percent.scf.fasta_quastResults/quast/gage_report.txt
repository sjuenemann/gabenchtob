All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.025percent.scf
GAGE_Contigs #                   409                                                                           
GAGE_Min contig                  1000                                                                          
GAGE_Max contig                  4214                                                                          
GAGE_N50                         0 COUNT: 0                                                                    
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               573140                                                                        
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     2231247(79.29%)                                                               
GAGE_Missing assembly bases      267(0.05%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  382                                                                           
GAGE_Compressed reference bases  11907                                                                         
GAGE_Bad trim                    267                                                                           
GAGE_Avg idy                     99.85                                                                         
GAGE_SNPs                        84                                                                            
GAGE_Indels < 5bp                766                                                                           
GAGE_Indels >= 5                 0                                                                             
GAGE_Inversions                  9                                                                             
GAGE_Relocation                  2                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          414                                                                           
GAGE_Corrected assembly size     572246                                                                        
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          4215                                                                          
GAGE_Corrected N50               0 COUNT: 0                                                                    
