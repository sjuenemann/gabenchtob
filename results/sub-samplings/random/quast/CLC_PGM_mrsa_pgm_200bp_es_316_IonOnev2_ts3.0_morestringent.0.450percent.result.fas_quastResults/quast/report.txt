All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.450percent.result
#Contigs (>= 0 bp)             256                                                                           
#Contigs (>= 1000 bp)          124                                                                           
Total length (>= 0 bp)         2779580                                                                       
Total length (>= 1000 bp)      2740141                                                                       
#Contigs                       256                                                                           
Largest contig                 121472                                                                        
Total length                   2779580                                                                       
Reference length               2813862                                                                       
GC (%)                         32.63                                                                         
Reference GC (%)               32.81                                                                         
N50                            43617                                                                         
NG50                           43617                                                                         
N75                            21942                                                                         
NG75                           21200                                                                         
#misassemblies                 14                                                                            
#local misassemblies           4                                                                             
#unaligned contigs             1 + 17 part                                                                   
Unaligned contigs length       887                                                                           
Genome fraction (%)            97.773                                                                        
Duplication ratio              1.007                                                                         
#N's per 100 kbp               1.58                                                                          
#mismatches per 100 kbp        2.47                                                                          
#indels per 100 kbp            13.19                                                                         
#genes                         2605 + 72 part                                                                
#predicted genes (unique)      2819                                                                          
#predicted genes (>= 0 bp)     2820                                                                          
#predicted genes (>= 300 bp)   2325                                                                          
#predicted genes (>= 1500 bp)  276                                                                           
#predicted genes (>= 3000 bp)  26                                                                            
Largest alignment              118967                                                                        
NA50                           42772                                                                         
NGA50                          42772                                                                         
NA75                           21942                                                                         
NGA75                          20575                                                                         
