All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.450percent.result
#Contigs                                   256                                                                           
#Contigs (>= 0 bp)                         256                                                                           
#Contigs (>= 1000 bp)                      124                                                                           
Largest contig                             121472                                                                        
Total length                               2779580                                                                       
Total length (>= 0 bp)                     2779580                                                                       
Total length (>= 1000 bp)                  2740141                                                                       
Reference length                           2813862                                                                       
N50                                        43617                                                                         
NG50                                       43617                                                                         
N75                                        21942                                                                         
NG75                                       21200                                                                         
L50                                        22                                                                            
LG50                                       22                                                                            
L75                                        45                                                                            
LG75                                       46                                                                            
#local misassemblies                       4                                                                             
#misassemblies                             14                                                                            
#misassembled contigs                      14                                                                            
Misassembled contigs length                159190                                                                        
Misassemblies inter-contig overlap         747                                                                           
#unaligned contigs                         1 + 17 part                                                                   
Unaligned contigs length                   887                                                                           
#ambiguously mapped contigs                11                                                                            
Extra bases in ambiguously mapped contigs  -9086                                                                         
#N's                                       44                                                                            
#N's per 100 kbp                           1.58                                                                          
Genome fraction (%)                        97.773                                                                        
Duplication ratio                          1.007                                                                         
#genes                                     2605 + 72 part                                                                
#predicted genes (unique)                  2819                                                                          
#predicted genes (>= 0 bp)                 2820                                                                          
#predicted genes (>= 300 bp)               2325                                                                          
#predicted genes (>= 1500 bp)              276                                                                           
#predicted genes (>= 3000 bp)              26                                                                            
#mismatches                                68                                                                            
#indels                                    363                                                                           
Indels length                              385                                                                           
#mismatches per 100 kbp                    2.47                                                                          
#indels per 100 kbp                        13.19                                                                         
GC (%)                                     32.63                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.210                                                                        
Largest alignment                          118967                                                                        
NA50                                       42772                                                                         
NGA50                                      42772                                                                         
NA75                                       21942                                                                         
NGA75                                      20575                                                                         
LA50                                       23                                                                            
LGA50                                      23                                                                            
LA75                                       46                                                                            
LGA75                                      48                                                                            
#Mis_misassemblies                         14                                                                            
#Mis_relocations                           4                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            10                                                                            
#Mis_misassembled contigs                  14                                                                            
Mis_Misassembled contigs length            159190                                                                        
#Mis_local misassemblies                   4                                                                             
#Mis_short indels (<= 5 bp)                363                                                                           
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               1                                                                             
Una_Fully unaligned length                 225                                                                           
#Una_partially unaligned contigs           17                                                                            
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             662                                                                           
GAGE_Contigs #                             256                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            121472                                                                        
GAGE_N50                                   43617 COUNT: 22                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2779580                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               17944(0.64%)                                                                  
GAGE_Missing assembly bases                1099(0.04%)                                                                   
GAGE_Missing assembly contigs              2(0.78%)                                                                      
GAGE_Duplicated reference bases            18171                                                                         
GAGE_Compressed reference bases            39546                                                                         
GAGE_Bad trim                              666                                                                           
GAGE_Avg idy                               99.98                                                                         
GAGE_SNPs                                  45                                                                            
GAGE_Indels < 5bp                          370                                                                           
GAGE_Indels >= 5                           4                                                                             
GAGE_Inversions                            1                                                                             
GAGE_Relocation                            2                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    185                                                                           
GAGE_Corrected assembly size               2761387                                                                       
GAGE_Min correct contig                    201                                                                           
GAGE_Max correct contig                    118977                                                                        
GAGE_Corrected N50                         37928 COUNT: 24                                                               
