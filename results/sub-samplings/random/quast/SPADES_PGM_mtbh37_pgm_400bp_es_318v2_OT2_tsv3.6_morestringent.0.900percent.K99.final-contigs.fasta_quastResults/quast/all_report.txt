All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.900percent.K99.final-contigs
#Contigs                                   204                                                                                         
#Contigs (>= 0 bp)                         229                                                                                         
#Contigs (>= 1000 bp)                      176                                                                                         
Largest contig                             135733                                                                                      
Total length                               4299012                                                                                     
Total length (>= 0 bp)                     4301968                                                                                     
Total length (>= 1000 bp)                  4282244                                                                                     
Reference length                           4411532                                                                                     
N50                                        48043                                                                                       
NG50                                       45615                                                                                       
N75                                        24368                                                                                       
NG75                                       22529                                                                                       
L50                                        25                                                                                          
LG50                                       27                                                                                          
L75                                        56                                                                                          
LG75                                       60                                                                                          
#local misassemblies                       26                                                                                          
#misassemblies                             5                                                                                           
#misassembled contigs                      4                                                                                           
Misassembled contigs length                146263                                                                                      
Misassemblies inter-contig overlap         24235                                                                                       
#unaligned contigs                         1 + 1 part                                                                                  
Unaligned contigs length                   217                                                                                         
#ambiguously mapped contigs                17                                                                                          
Extra bases in ambiguously mapped contigs  -13739                                                                                      
#N's                                       0                                                                                           
#N's per 100 kbp                           0.00                                                                                        
Genome fraction (%)                        97.028                                                                                      
Duplication ratio                          1.007                                                                                       
#genes                                     3912 + 134 part                                                                             
#predicted genes (unique)                  4511                                                                                        
#predicted genes (>= 0 bp)                 4517                                                                                        
#predicted genes (>= 300 bp)               3791                                                                                        
#predicted genes (>= 1500 bp)              454                                                                                         
#predicted genes (>= 3000 bp)              48                                                                                          
#mismatches                                376                                                                                         
#indels                                    1534                                                                                        
Indels length                              1769                                                                                        
#mismatches per 100 kbp                    8.78                                                                                        
#indels per 100 kbp                        35.84                                                                                       
GC (%)                                     65.41                                                                                       
Reference GC (%)                           65.61                                                                                       
Average %IDY                               99.751                                                                                      
Largest alignment                          135730                                                                                      
NA50                                       46570                                                                                       
NGA50                                      45614                                                                                       
NA75                                       24368                                                                                       
NGA75                                      22446                                                                                       
LA50                                       26                                                                                          
LGA50                                      27                                                                                          
LA75                                       56                                                                                          
LGA75                                      60                                                                                          
#Mis_misassemblies                         5                                                                                           
#Mis_relocations                           5                                                                                           
#Mis_translocations                        0                                                                                           
#Mis_inversions                            0                                                                                           
#Mis_misassembled contigs                  4                                                                                           
Mis_Misassembled contigs length            146263                                                                                      
#Mis_local misassemblies                   26                                                                                          
#Mis_short indels (<= 5 bp)                1529                                                                                        
#Mis_long indels (> 5 bp)                  5                                                                                           
#Una_fully unaligned contigs               1                                                                                           
Una_Fully unaligned length                 203                                                                                         
#Una_partially unaligned contigs           1                                                                                           
#Una_with misassembly                      0                                                                                           
#Una_both parts are significant            0                                                                                           
Una_Partially unaligned length             14                                                                                          
GAGE_Contigs #                             204                                                                                         
GAGE_Min contig                            203                                                                                         
GAGE_Max contig                            135733                                                                                      
GAGE_N50                                   45615 COUNT: 27                                                                             
GAGE_Genome size                           4411532                                                                                     
GAGE_Assembly size                         4299012                                                                                     
GAGE_Chaff bases                           0                                                                                           
GAGE_Missing reference bases               74103(1.68%)                                                                                
GAGE_Missing assembly bases                374(0.01%)                                                                                  
GAGE_Missing assembly contigs              1(0.49%)                                                                                    
GAGE_Duplicated reference bases            353                                                                                         
GAGE_Compressed reference bases            56395                                                                                       
GAGE_Bad trim                              171                                                                                         
GAGE_Avg idy                               99.95                                                                                       
GAGE_SNPs                                  295                                                                                         
GAGE_Indels < 5bp                          1539                                                                                        
GAGE_Indels >= 5                           21                                                                                          
GAGE_Inversions                            1                                                                                           
GAGE_Relocation                            10                                                                                          
GAGE_Translocation                         0                                                                                           
GAGE_Corrected contig #                    236                                                                                         
GAGE_Corrected assembly size               4303323                                                                                     
GAGE_Min correct contig                    212                                                                                         
GAGE_Max correct contig                    117839                                                                                      
GAGE_Corrected N50                         42101 COUNT: 33                                                                             
