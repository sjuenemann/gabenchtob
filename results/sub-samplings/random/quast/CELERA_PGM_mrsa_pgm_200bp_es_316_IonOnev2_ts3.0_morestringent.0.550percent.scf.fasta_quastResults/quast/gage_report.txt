All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.550percent.scf
GAGE_Contigs #                   511                                                                           
GAGE_Min contig                  1000                                                                          
GAGE_Max contig                  46633                                                                         
GAGE_N50                         7873 COUNT: 108                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2611255                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     230969(8.21%)                                                                 
GAGE_Missing assembly bases      826(0.03%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  932                                                                           
GAGE_Compressed reference bases  19164                                                                         
GAGE_Bad trim                    826                                                                           
GAGE_Avg idy                     99.97                                                                         
GAGE_SNPs                        110                                                                           
GAGE_Indels < 5bp                503                                                                           
GAGE_Indels >= 5                 4                                                                             
GAGE_Inversions                  1                                                                             
GAGE_Relocation                  1                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          516                                                                           
GAGE_Corrected assembly size     2610448                                                                       
GAGE_Min correct contig          353                                                                           
GAGE_Max correct contig          46638                                                                         
GAGE_Corrected N50               7773 COUNT: 109                                                               
