All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                               #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.850percent_out.unpadded  2                             689                         7                                 0                      2                                1323                            43  
