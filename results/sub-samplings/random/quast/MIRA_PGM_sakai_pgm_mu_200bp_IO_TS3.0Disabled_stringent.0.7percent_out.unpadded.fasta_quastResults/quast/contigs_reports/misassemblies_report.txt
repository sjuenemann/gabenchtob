All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.7percent_out.unpadded
#Mis_misassemblies               263                                                                           
#Mis_relocations                 40                                                                            
#Mis_translocations              4                                                                             
#Mis_inversions                  219                                                                           
#Mis_misassembled contigs        238                                                                           
Mis_Misassembled contigs length  1718343                                                                       
#Mis_local misassemblies         40                                                                            
#mismatches                      1353                                                                          
#indels                          373                                                                           
#Mis_short indels (<= 5 bp)      372                                                                           
#Mis_long indels (> 5 bp)        1                                                                             
Indels length                    408                                                                           
