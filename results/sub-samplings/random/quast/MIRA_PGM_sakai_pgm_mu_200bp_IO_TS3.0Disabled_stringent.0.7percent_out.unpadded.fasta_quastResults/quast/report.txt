All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.7percent_out.unpadded
#Contigs (>= 0 bp)             1619                                                                          
#Contigs (>= 1000 bp)          193                                                                           
Total length (>= 0 bp)         6001132                                                                       
Total length (>= 1000 bp)      5509474                                                                       
#Contigs                       1584                                                                          
Largest contig                 352941                                                                        
Total length                   5995228                                                                       
Reference length               5594470                                                                       
GC (%)                         50.43                                                                         
Reference GC (%)               50.48                                                                         
N50                            72130                                                                         
NG50                           85278                                                                         
N75                            31703                                                                         
NG75                           42672                                                                         
#misassemblies                 263                                                                           
#local misassemblies           40                                                                            
#unaligned contigs             27 + 803 part                                                                 
Unaligned contigs length       53887                                                                         
Genome fraction (%)            98.130                                                                        
Duplication ratio              1.079                                                                         
#N's per 100 kbp               31.49                                                                         
#mismatches per 100 kbp        24.66                                                                         
#indels per 100 kbp            6.80                                                                          
#genes                         5222 + 174 part                                                               
#predicted genes (unique)      6854                                                                          
#predicted genes (>= 0 bp)     6892                                                                          
#predicted genes (>= 300 bp)   4864                                                                          
#predicted genes (>= 1500 bp)  658                                                                           
#predicted genes (>= 3000 bp)  64                                                                            
Largest alignment              328313                                                                        
NA50                           68135                                                                         
NGA50                          72421                                                                         
NA75                           30158                                                                         
NGA75                          38782                                                                         
