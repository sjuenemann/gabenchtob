All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.950percent_out.unpadded
#Contigs                                   229                                                                                                             
#Contigs (>= 0 bp)                         248                                                                                                             
#Contigs (>= 1000 bp)                      103                                                                                                             
Largest contig                             294953                                                                                                          
Total length                               4454919                                                                                                         
Total length (>= 0 bp)                     4457225                                                                                                         
Total length (>= 1000 bp)                  4387378                                                                                                         
Reference length                           4411532                                                                                                         
N50                                        91009                                                                                                           
NG50                                       91009                                                                                                           
N75                                        57532                                                                                                           
NG75                                       57532                                                                                                           
L50                                        15                                                                                                              
LG50                                       15                                                                                                              
L75                                        31                                                                                                              
LG75                                       31                                                                                                              
#local misassemblies                       15                                                                                                              
#misassemblies                             43                                                                                                              
#misassembled contigs                      30                                                                                                              
Misassembled contigs length                1690062                                                                                                         
Misassemblies inter-contig overlap         44647                                                                                                           
#unaligned contigs                         2 + 15 part                                                                                                     
Unaligned contigs length                   2221                                                                                                            
#ambiguously mapped contigs                6                                                                                                               
Extra bases in ambiguously mapped contigs  -2871                                                                                                           
#N's                                       202                                                                                                             
#N's per 100 kbp                           4.53                                                                                                            
Genome fraction (%)                        99.613                                                                                                          
Duplication ratio                          1.023                                                                                                           
#genes                                     4016 + 93 part                                                                                                  
#predicted genes (unique)                  4205                                                                                                            
#predicted genes (>= 0 bp)                 4234                                                                                                            
#predicted genes (>= 300 bp)               3667                                                                                                            
#predicted genes (>= 1500 bp)              566                                                                                                             
#predicted genes (>= 3000 bp)              77                                                                                                              
#mismatches                                430                                                                                                             
#indels                                    83                                                                                                              
Indels length                              378                                                                                                             
#mismatches per 100 kbp                    9.79                                                                                                            
#indels per 100 kbp                        1.89                                                                                                            
GC (%)                                     65.63                                                                                                           
Reference GC (%)                           65.61                                                                                                           
Average %IDY                               99.696                                                                                                          
Largest alignment                          268789                                                                                                          
NA50                                       71248                                                                                                           
NGA50                                      71248                                                                                                           
NA75                                       36357                                                                                                           
NGA75                                      36859                                                                                                           
LA50                                       19                                                                                                              
LGA50                                      19                                                                                                              
LA75                                       40                                                                                                              
LGA75                                      39                                                                                                              
#Mis_misassemblies                         43                                                                                                              
#Mis_relocations                           41                                                                                                              
#Mis_translocations                        0                                                                                                               
#Mis_inversions                            2                                                                                                               
#Mis_misassembled contigs                  30                                                                                                              
Mis_Misassembled contigs length            1690062                                                                                                         
#Mis_local misassemblies                   15                                                                                                              
#Mis_short indels (<= 5 bp)                74                                                                                                              
#Mis_long indels (> 5 bp)                  9                                                                                                               
#Una_fully unaligned contigs               2                                                                                                               
Una_Fully unaligned length                 1003                                                                                                            
#Una_partially unaligned contigs           15                                                                                                              
#Una_with misassembly                      0                                                                                                               
#Una_both parts are significant            1                                                                                                               
Una_Partially unaligned length             1218                                                                                                            
GAGE_Contigs #                             229                                                                                                             
GAGE_Min contig                            203                                                                                                             
GAGE_Max contig                            294953                                                                                                          
GAGE_N50                                   91009 COUNT: 15                                                                                                 
GAGE_Genome size                           4411532                                                                                                         
GAGE_Assembly size                         4454919                                                                                                         
GAGE_Chaff bases                           0                                                                                                               
GAGE_Missing reference bases               12505(0.28%)                                                                                                    
GAGE_Missing assembly bases                3603(0.08%)                                                                                                     
GAGE_Missing assembly contigs              2(0.87%)                                                                                                        
GAGE_Duplicated reference bases            55175                                                                                                           
GAGE_Compressed reference bases            14541                                                                                                           
GAGE_Bad trim                              2600                                                                                                            
GAGE_Avg idy                               99.98                                                                                                           
GAGE_SNPs                                  173                                                                                                             
GAGE_Indels < 5bp                          32                                                                                                              
GAGE_Indels >= 5                           11                                                                                                              
GAGE_Inversions                            15                                                                                                              
GAGE_Relocation                            4                                                                                                               
GAGE_Translocation                         0                                                                                                               
GAGE_Corrected contig #                    173                                                                                                             
GAGE_Corrected assembly size               4425316                                                                                                         
GAGE_Min correct contig                    212                                                                                                             
GAGE_Max correct contig                    226098                                                                                                          
GAGE_Corrected N50                         66148 COUNT: 21                                                                                                 
