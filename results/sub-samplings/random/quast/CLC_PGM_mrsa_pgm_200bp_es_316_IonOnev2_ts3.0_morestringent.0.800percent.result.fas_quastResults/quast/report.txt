All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.800percent.result
#Contigs (>= 0 bp)             302                                                                           
#Contigs (>= 1000 bp)          130                                                                           
Total length (>= 0 bp)         2787126                                                                       
Total length (>= 1000 bp)      2740607                                                                       
#Contigs                       302                                                                           
Largest contig                 102534                                                                        
Total length                   2787126                                                                       
Reference length               2813862                                                                       
GC (%)                         32.64                                                                         
Reference GC (%)               32.81                                                                         
N50                            36808                                                                         
NG50                           36713                                                                         
N75                            21400                                                                         
NG75                           19983                                                                         
#misassemblies                 3                                                                             
#local misassemblies           1                                                                             
#unaligned contigs             4 + 10 part                                                                   
Unaligned contigs length       1356                                                                          
Genome fraction (%)            97.753                                                                        
Duplication ratio              1.010                                                                         
#N's per 100 kbp               2.33                                                                          
#mismatches per 100 kbp        1.45                                                                          
#indels per 100 kbp            10.65                                                                         
#genes                         2613 + 76 part                                                                
#predicted genes (unique)      2825                                                                          
#predicted genes (>= 0 bp)     2830                                                                          
#predicted genes (>= 300 bp)   2319                                                                          
#predicted genes (>= 1500 bp)  279                                                                           
#predicted genes (>= 3000 bp)  23                                                                            
Largest alignment              102534                                                                        
NA50                           36808                                                                         
NGA50                          36713                                                                         
NA75                           21400                                                                         
NGA75                          19983                                                                         
