All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.050percent.scf
#Contigs                                   62                                                                       
#Contigs (>= 0 bp)                         62                                                                       
#Contigs (>= 1000 bp)                      62                                                                       
Largest contig                             240521                                                                   
Total length                               2734731                                                                  
Total length (>= 0 bp)                     2734731                                                                  
Total length (>= 1000 bp)                  2734731                                                                  
Reference length                           2813862                                                                  
N50                                        96728                                                                    
NG50                                       96728                                                                    
N75                                        39229                                                                    
NG75                                       35661                                                                    
L50                                        10                                                                       
LG50                                       10                                                                       
L75                                        22                                                                       
LG75                                       24                                                                       
#local misassemblies                       2                                                                        
#misassemblies                             1                                                                        
#misassembled contigs                      1                                                                        
Misassembled contigs length                155204                                                                   
Misassemblies inter-contig overlap         305                                                                      
#unaligned contigs                         0 + 0 part                                                               
Unaligned contigs length                   0                                                                        
#ambiguously mapped contigs                0                                                                        
Extra bases in ambiguously mapped contigs  0                                                                        
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        96.947                                                                   
Duplication ratio                          1.003                                                                    
#genes                                     2612 + 42 part                                                           
#predicted genes (unique)                  2716                                                                     
#predicted genes (>= 0 bp)                 2718                                                                     
#predicted genes (>= 300 bp)               2315                                                                     
#predicted genes (>= 1500 bp)              273                                                                      
#predicted genes (>= 3000 bp)              23                                                                       
#mismatches                                66                                                                       
#indels                                    482                                                                      
Indels length                              613                                                                      
#mismatches per 100 kbp                    2.42                                                                     
#indels per 100 kbp                        17.67                                                                    
GC (%)                                     32.62                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               98.229                                                                   
Largest alignment                          240515                                                                   
NA50                                       96728                                                                    
NGA50                                      73788                                                                    
NA75                                       39229                                                                    
NGA75                                      35661                                                                    
LA50                                       10                                                                       
LGA50                                      11                                                                       
LA75                                       23                                                                       
LGA75                                      25                                                                       
#Mis_misassemblies                         1                                                                        
#Mis_relocations                           1                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  1                                                                        
Mis_Misassembled contigs length            155204                                                                   
#Mis_local misassemblies                   2                                                                        
#Mis_short indels (<= 5 bp)                479                                                                      
#Mis_long indels (> 5 bp)                  3                                                                        
#Una_fully unaligned contigs               0                                                                        
Una_Fully unaligned length                 0                                                                        
#Una_partially unaligned contigs           0                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             0                                                                        
GAGE_Contigs #                             62                                                                       
GAGE_Min contig                            1172                                                                     
GAGE_Max contig                            240521                                                                   
GAGE_N50                                   96728 COUNT: 10                                                          
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         2734731                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               77910(2.77%)                                                             
GAGE_Missing assembly bases                163(0.01%)                                                               
GAGE_Missing assembly contigs              0(0.00%)                                                                 
GAGE_Duplicated reference bases            0                                                                        
GAGE_Compressed reference bases            13910                                                                    
GAGE_Bad trim                              163                                                                      
GAGE_Avg idy                               99.98                                                                    
GAGE_SNPs                                  51                                                                       
GAGE_Indels < 5bp                          444                                                                      
GAGE_Indels >= 5                           4                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            1                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    67                                                                       
GAGE_Corrected assembly size               2735673                                                                  
GAGE_Min correct contig                    1172                                                                     
GAGE_Max correct contig                    160261                                                                   
GAGE_Corrected N50                         73797 COUNT: 12                                                          
