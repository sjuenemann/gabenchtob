All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.925percent.contigs
GAGE_Contigs #                   153                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  166118                                                                        
GAGE_N50                         61060 COUNT: 18                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2773865                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     19980(0.71%)                                                                  
GAGE_Missing assembly bases      186(0.01%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  8585                                                                          
GAGE_Compressed reference bases  32752                                                                         
GAGE_Bad trim                    181                                                                           
GAGE_Avg idy                     99.99                                                                         
GAGE_SNPs                        42                                                                            
GAGE_Indels < 5bp                70                                                                            
GAGE_Indels >= 5                 2                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  1                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          119                                                                           
GAGE_Corrected assembly size     2765171                                                                       
GAGE_Min correct contig          203                                                                           
GAGE_Max correct contig          166123                                                                        
GAGE_Corrected N50               61064 COUNT: 18                                                               
