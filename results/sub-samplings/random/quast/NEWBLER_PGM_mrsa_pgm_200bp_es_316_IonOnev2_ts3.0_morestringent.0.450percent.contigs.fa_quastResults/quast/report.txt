All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.450percent.contigs
#Contigs (>= 0 bp)             271                                                                                
#Contigs (>= 1000 bp)          122                                                                                
Total length (>= 0 bp)         2773812                                                                            
Total length (>= 1000 bp)      2747911                                                                            
#Contigs                       151                                                                                
Largest contig                 116283                                                                             
Total length                   2758572                                                                            
Reference length               2813862                                                                            
GC (%)                         32.65                                                                              
Reference GC (%)               32.81                                                                              
N50                            39260                                                                              
NG50                           39074                                                                              
N75                            19449                                                                              
NG75                           19086                                                                              
#misassemblies                 5                                                                                  
#local misassemblies           4                                                                                  
#unaligned contigs             0 + 1 part                                                                         
Unaligned contigs length       39                                                                                 
Genome fraction (%)            97.767                                                                             
Duplication ratio              1.000                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        2.76                                                                               
#indels per 100 kbp            8.11                                                                               
#genes                         2609 + 92 part                                                                     
#predicted genes (unique)      2735                                                                               
#predicted genes (>= 0 bp)     2735                                                                               
#predicted genes (>= 300 bp)   2324                                                                               
#predicted genes (>= 1500 bp)  287                                                                                
#predicted genes (>= 3000 bp)  26                                                                                 
Largest alignment              116283                                                                             
NA50                           39074                                                                              
NGA50                          38555                                                                              
NA75                           19381                                                                              
NGA75                          19000                                                                              
