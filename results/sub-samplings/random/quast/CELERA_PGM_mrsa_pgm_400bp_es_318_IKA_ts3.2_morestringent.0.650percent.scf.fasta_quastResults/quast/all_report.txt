All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.650percent.scf
#Contigs                                   141                                                                      
#Contigs (>= 0 bp)                         141                                                                      
#Contigs (>= 1000 bp)                      141                                                                      
Largest contig                             242737                                                                   
Total length                               2777932                                                                  
Total length (>= 0 bp)                     2777932                                                                  
Total length (>= 1000 bp)                  2777932                                                                  
Reference length                           2813862                                                                  
N50                                        43913                                                                    
NG50                                       43913                                                                    
N75                                        20351                                                                    
NG75                                       20307                                                                    
L50                                        15                                                                       
LG50                                       15                                                                       
L75                                        37                                                                       
LG75                                       38                                                                       
#local misassemblies                       0                                                                        
#misassemblies                             2                                                                        
#misassembled contigs                      2                                                                        
Misassembled contigs length                39565                                                                    
Misassemblies inter-contig overlap         1                                                                        
#unaligned contigs                         0 + 0 part                                                               
Unaligned contigs length                   0                                                                        
#ambiguously mapped contigs                1                                                                        
Extra bases in ambiguously mapped contigs  -1174                                                                    
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        97.496                                                                   
Duplication ratio                          1.012                                                                    
#genes                                     2586 + 87 part                                                           
#predicted genes (unique)                  2786                                                                     
#predicted genes (>= 0 bp)                 2787                                                                     
#predicted genes (>= 300 bp)               2337                                                                     
#predicted genes (>= 1500 bp)              269                                                                      
#predicted genes (>= 3000 bp)              25                                                                       
#mismatches                                56                                                                       
#indels                                    317                                                                      
Indels length                              324                                                                      
#mismatches per 100 kbp                    2.04                                                                     
#indels per 100 kbp                        11.56                                                                    
GC (%)                                     32.64                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               98.572                                                                   
Largest alignment                          242737                                                                   
NA50                                       43913                                                                    
NGA50                                      43913                                                                    
NA75                                       20351                                                                    
NGA75                                      20307                                                                    
LA50                                       15                                                                       
LGA50                                      15                                                                       
LA75                                       37                                                                       
LGA75                                      38                                                                       
#Mis_misassemblies                         2                                                                        
#Mis_relocations                           2                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  2                                                                        
Mis_Misassembled contigs length            39565                                                                    
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                317                                                                      
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               0                                                                        
Una_Fully unaligned length                 0                                                                        
#Una_partially unaligned contigs           0                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             0                                                                        
GAGE_Contigs #                             141                                                                      
GAGE_Min contig                            1000                                                                     
GAGE_Max contig                            242737                                                                   
GAGE_N50                                   43913 COUNT: 15                                                          
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         2777932                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               57566(2.05%)                                                             
GAGE_Missing assembly bases                42(0.00%)                                                                
GAGE_Missing assembly contigs              0(0.00%)                                                                 
GAGE_Duplicated reference bases            99                                                                       
GAGE_Compressed reference bases            19566                                                                    
GAGE_Bad trim                              42                                                                       
GAGE_Avg idy                               99.98                                                                    
GAGE_SNPs                                  57                                                                       
GAGE_Indels < 5bp                          304                                                                      
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            1                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    142                                                                      
GAGE_Corrected assembly size               2778057                                                                  
GAGE_Min correct contig                    772                                                                      
GAGE_Max correct contig                    242756                                                                   
GAGE_Corrected N50                         43913 COUNT: 15                                                          
