All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.4percent_out.unpadded
#Contigs                                   603                                                                                                     
#Contigs (>= 0 bp)                         604                                                                                                     
#Contigs (>= 1000 bp)                      108                                                                                                     
Largest contig                             394431                                                                                                  
Total length                               5787253                                                                                                 
Total length (>= 0 bp)                     5787443                                                                                                 
Total length (>= 1000 bp)                  5564197                                                                                                 
Reference length                           5594470                                                                                                 
N50                                        216966                                                                                                  
NG50                                       244085                                                                                                  
N75                                        105458                                                                                                  
NG75                                       106852                                                                                                  
L50                                        10                                                                                                      
LG50                                       9                                                                                                       
L75                                        19                                                                                                      
LG75                                       18                                                                                                      
#local misassemblies                       17                                                                                                      
#misassemblies                             69                                                                                                      
#misassembled contigs                      36                                                                                                      
Misassembled contigs length                3650301                                                                                                 
Misassemblies inter-contig overlap         71179                                                                                                   
#unaligned contigs                         2 + 2 part                                                                                              
Unaligned contigs length                   6444                                                                                                    
#ambiguously mapped contigs                110                                                                                                     
Extra bases in ambiguously mapped contigs  -86913                                                                                                  
#N's                                       119                                                                                                     
#N's per 100 kbp                           2.06                                                                                                    
Genome fraction (%)                        98.651                                                                                                  
Duplication ratio                          1.045                                                                                                   
#genes                                     5323 + 66 part                                                                                          
#predicted genes (unique)                  5915                                                                                                    
#predicted genes (>= 0 bp)                 5972                                                                                                    
#predicted genes (>= 300 bp)               4891                                                                                                    
#predicted genes (>= 1500 bp)              700                                                                                                     
#predicted genes (>= 3000 bp)              78                                                                                                      
#mismatches                                2920                                                                                                    
#indels                                    136                                                                                                     
Indels length                              176                                                                                                     
#mismatches per 100 kbp                    52.91                                                                                                   
#indels per 100 kbp                        2.46                                                                                                    
GC (%)                                     50.53                                                                                                   
Reference GC (%)                           50.48                                                                                                   
Average %IDY                               98.741                                                                                                  
Largest alignment                          394431                                                                                                  
NA50                                       141182                                                                                                  
NGA50                                      141182                                                                                                  
NA75                                       62835                                                                                                   
NGA75                                      76701                                                                                                   
LA50                                       13                                                                                                      
LGA50                                      13                                                                                                      
LA75                                       28                                                                                                      
LGA75                                      26                                                                                                      
#Mis_misassemblies                         69                                                                                                      
#Mis_relocations                           66                                                                                                      
#Mis_translocations                        2                                                                                                       
#Mis_inversions                            1                                                                                                       
#Mis_misassembled contigs                  36                                                                                                      
Mis_Misassembled contigs length            3650301                                                                                                 
#Mis_local misassemblies                   17                                                                                                      
#Mis_short indels (<= 5 bp)                134                                                                                                     
#Mis_long indels (> 5 bp)                  2                                                                                                       
#Una_fully unaligned contigs               2                                                                                                       
Una_Fully unaligned length                 6380                                                                                                    
#Una_partially unaligned contigs           2                                                                                                       
#Una_with misassembly                      0                                                                                                       
#Una_both parts are significant            0                                                                                                       
Una_Partially unaligned length             64                                                                                                      
GAGE_Contigs #                             603                                                                                                     
GAGE_Min contig                            203                                                                                                     
GAGE_Max contig                            394431                                                                                                  
GAGE_N50                                   244085 COUNT: 9                                                                                         
GAGE_Genome size                           5594470                                                                                                 
GAGE_Assembly size                         5787253                                                                                                 
GAGE_Chaff bases                           0                                                                                                       
GAGE_Missing reference bases               357(0.01%)                                                                                              
GAGE_Missing assembly bases                6665(0.12%)                                                                                             
GAGE_Missing assembly contigs              2(0.33%)                                                                                                
GAGE_Duplicated reference bases            309607                                                                                                  
GAGE_Compressed reference bases            134310                                                                                                  
GAGE_Bad trim                              285                                                                                                     
GAGE_Avg idy                               99.95                                                                                                   
GAGE_SNPs                                  475                                                                                                     
GAGE_Indels < 5bp                          25                                                                                                      
GAGE_Indels >= 5                           3                                                                                                       
GAGE_Inversions                            24                                                                                                      
GAGE_Relocation                            14                                                                                                      
GAGE_Translocation                         2                                                                                                       
GAGE_Corrected contig #                    139                                                                                                     
GAGE_Corrected assembly size               5575054                                                                                                 
GAGE_Min correct contig                    250                                                                                                     
GAGE_Max correct contig                    394431                                                                                                  
GAGE_Corrected N50                         144431 COUNT: 13                                                                                        
