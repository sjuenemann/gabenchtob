All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.900percent.scf
GAGE_Contigs #                   366                                                                           
GAGE_Min contig                  1000                                                                          
GAGE_Max contig                  26569                                                                         
GAGE_N50                         1029 COUNT: 355                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               1418270                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     1406315(49.98%)                                                               
GAGE_Missing assembly bases      661(0.05%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  929                                                                           
GAGE_Compressed reference bases  14054                                                                         
GAGE_Bad trim                    661                                                                           
GAGE_Avg idy                     99.96                                                                         
GAGE_SNPs                        85                                                                            
GAGE_Indels < 5bp                291                                                                           
GAGE_Indels >= 5                 0                                                                             
GAGE_Inversions                  3                                                                             
GAGE_Relocation                  0                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          369                                                                           
GAGE_Corrected assembly size     1416669                                                                       
GAGE_Min correct contig          233                                                                           
GAGE_Max correct contig          23948                                                                         
GAGE_Corrected N50               1013 COUNT: 358                                                               
