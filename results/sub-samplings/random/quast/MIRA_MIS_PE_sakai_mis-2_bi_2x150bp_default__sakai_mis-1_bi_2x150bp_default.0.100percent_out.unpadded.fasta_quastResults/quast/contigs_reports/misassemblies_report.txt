All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.100percent_out.unpadded
#Mis_misassemblies               57                                                                                                  
#Mis_relocations                 57                                                                                                  
#Mis_translocations              0                                                                                                   
#Mis_inversions                  0                                                                                                   
#Mis_misassembled contigs        45                                                                                                  
Mis_Misassembled contigs length  225161                                                                                              
#Mis_local misassemblies         9                                                                                                   
#mismatches                      2760                                                                                                
#indels                          124                                                                                                 
#Mis_short indels (<= 5 bp)      121                                                                                                 
#Mis_long indels (> 5 bp)        3                                                                                                   
Indels length                    187                                                                                                 
