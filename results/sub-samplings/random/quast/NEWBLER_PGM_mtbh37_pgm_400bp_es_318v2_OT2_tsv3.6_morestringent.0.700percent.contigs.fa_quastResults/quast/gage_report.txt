All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.700percent.contigs
GAGE_Contigs #                   223                                                                                
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  157994                                                                             
GAGE_N50                         55869 COUNT: 24                                                                    
GAGE_Genome size                 4411532                                                                            
GAGE_Assembly size               4294571                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     68190(1.55%)                                                                       
GAGE_Missing assembly bases      1094(0.03%)                                                                        
GAGE_Missing assembly contigs    2(0.90%)                                                                           
GAGE_Duplicated reference bases  953                                                                                
GAGE_Compressed reference bases  50613                                                                              
GAGE_Bad trim                    15                                                                                 
GAGE_Avg idy                     99.97                                                                              
GAGE_SNPs                        109                                                                                
GAGE_Indels < 5bp                872                                                                                
GAGE_Indels >= 5                 11                                                                                 
GAGE_Inversions                  0                                                                                  
GAGE_Relocation                  8                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          237                                                                                
GAGE_Corrected assembly size     4295618                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          158020                                                                             
GAGE_Corrected N50               46261 COUNT: 29                                                                    
