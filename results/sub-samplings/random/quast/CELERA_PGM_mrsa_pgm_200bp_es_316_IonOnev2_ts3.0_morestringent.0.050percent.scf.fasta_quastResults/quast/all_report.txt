All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.050percent.scf
#Contigs                                   888                                                                           
#Contigs (>= 0 bp)                         888                                                                           
#Contigs (>= 1000 bp)                      888                                                                           
Largest contig                             15351                                                                         
Total length                               2401988                                                                       
Total length (>= 0 bp)                     2401988                                                                       
Total length (>= 1000 bp)                  2401988                                                                       
Reference length                           2813862                                                                       
N50                                        3280                                                                          
NG50                                       2769                                                                          
N75                                        2032                                                                          
NG75                                       1492                                                                          
L50                                        241                                                                           
LG50                                       310                                                                           
L75                                        475                                                                           
LG75                                       651                                                                           
#local misassemblies                       5                                                                             
#misassemblies                             28                                                                            
#misassembled contigs                      28                                                                            
Misassembled contigs length                80731                                                                         
Misassemblies inter-contig overlap         1929                                                                          
#unaligned contigs                         0 + 5 part                                                                    
Unaligned contigs length                   301                                                                           
#ambiguously mapped contigs                0                                                                             
Extra bases in ambiguously mapped contigs  0                                                                             
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        84.560                                                                        
Duplication ratio                          1.010                                                                         
#genes                                     1628 + 811 part                                                               
#predicted genes (unique)                  3101                                                                          
#predicted genes (>= 0 bp)                 3101                                                                          
#predicted genes (>= 300 bp)               2289                                                                          
#predicted genes (>= 1500 bp)              153                                                                           
#predicted genes (>= 3000 bp)              8                                                                             
#mismatches                                198                                                                           
#indels                                    1610                                                                          
Indels length                              1658                                                                          
#mismatches per 100 kbp                    8.32                                                                          
#indels per 100 kbp                        67.66                                                                         
GC (%)                                     32.72                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.569                                                                        
Largest alignment                          15351                                                                         
NA50                                       3243                                                                          
NGA50                                      2759                                                                          
NA75                                       2015                                                                          
NGA75                                      1472                                                                          
LA50                                       242                                                                           
LGA50                                      311                                                                           
LA75                                       478                                                                           
LGA75                                      655                                                                           
#Mis_misassemblies                         28                                                                            
#Mis_relocations                           14                                                                            
#Mis_translocations                        2                                                                             
#Mis_inversions                            12                                                                            
#Mis_misassembled contigs                  28                                                                            
Mis_Misassembled contigs length            80731                                                                         
#Mis_local misassemblies                   5                                                                             
#Mis_short indels (<= 5 bp)                1610                                                                          
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           5                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             301                                                                           
GAGE_Contigs #                             888                                                                           
GAGE_Min contig                            1000                                                                          
GAGE_Max contig                            15351                                                                         
GAGE_N50                                   2769 COUNT: 310                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2401988                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               421870(14.99%)                                                                
GAGE_Missing assembly bases                738(0.03%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            1430                                                                          
GAGE_Compressed reference bases            17014                                                                         
GAGE_Bad trim                              738                                                                           
GAGE_Avg idy                               99.91                                                                         
GAGE_SNPs                                  122                                                                           
GAGE_Indels < 5bp                          1457                                                                          
GAGE_Indels >= 5                           3                                                                             
GAGE_Inversions                            9                                                                             
GAGE_Relocation                            6                                                                             
GAGE_Translocation                         2                                                                             
GAGE_Corrected contig #                    898                                                                           
GAGE_Corrected assembly size               2401108                                                                       
GAGE_Min correct contig                    260                                                                           
GAGE_Max correct contig                    15357                                                                         
GAGE_Corrected N50                         2758 COUNT: 311                                                               
