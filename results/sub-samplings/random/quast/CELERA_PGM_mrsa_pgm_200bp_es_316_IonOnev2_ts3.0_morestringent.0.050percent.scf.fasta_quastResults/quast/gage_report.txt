All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.050percent.scf
GAGE_Contigs #                   888                                                                           
GAGE_Min contig                  1000                                                                          
GAGE_Max contig                  15351                                                                         
GAGE_N50                         2769 COUNT: 310                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2401988                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     421870(14.99%)                                                                
GAGE_Missing assembly bases      738(0.03%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  1430                                                                          
GAGE_Compressed reference bases  17014                                                                         
GAGE_Bad trim                    738                                                                           
GAGE_Avg idy                     99.91                                                                         
GAGE_SNPs                        122                                                                           
GAGE_Indels < 5bp                1457                                                                          
GAGE_Indels >= 5                 3                                                                             
GAGE_Inversions                  9                                                                             
GAGE_Relocation                  6                                                                             
GAGE_Translocation               2                                                                             
GAGE_Corrected contig #          898                                                                           
GAGE_Corrected assembly size     2401108                                                                       
GAGE_Min correct contig          260                                                                           
GAGE_Max correct contig          15357                                                                         
GAGE_Corrected N50               2758 COUNT: 311                                                               
