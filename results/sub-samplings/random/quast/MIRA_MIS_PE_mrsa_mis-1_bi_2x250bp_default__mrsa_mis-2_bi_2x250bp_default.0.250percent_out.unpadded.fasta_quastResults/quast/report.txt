All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.250percent_out.unpadded
#Contigs (>= 0 bp)             161                                                                                               
#Contigs (>= 1000 bp)          44                                                                                                
Total length (>= 0 bp)         2897337                                                                                           
Total length (>= 1000 bp)      2838235                                                                                           
#Contigs                       160                                                                                               
Largest contig                 499156                                                                                            
Total length                   2897272                                                                                           
Reference length               2813862                                                                                           
GC (%)                         32.93                                                                                             
Reference GC (%)               32.81                                                                                             
N50                            249561                                                                                            
NG50                           261582                                                                                            
N75                            121518                                                                                            
NG75                           210903                                                                                            
#misassemblies                 18                                                                                                
#local misassemblies           2                                                                                                 
#unaligned contigs             2 + 0 part                                                                                        
Unaligned contigs length       6114                                                                                              
Genome fraction (%)            99.949                                                                                            
Duplication ratio              1.032                                                                                             
#N's per 100 kbp               1.10                                                                                              
#mismatches per 100 kbp        6.58                                                                                              
#indels per 100 kbp            3.09                                                                                              
#genes                         2707 + 19 part                                                                                    
#predicted genes (unique)      2769                                                                                              
#predicted genes (>= 0 bp)     2790                                                                                              
#predicted genes (>= 300 bp)   2407                                                                                              
#predicted genes (>= 1500 bp)  304                                                                                               
#predicted genes (>= 3000 bp)  28                                                                                                
Largest alignment              499156                                                                                            
NA50                           210903                                                                                            
NGA50                          210903                                                                                            
NA75                           70899                                                                                             
NGA75                          80361                                                                                             
