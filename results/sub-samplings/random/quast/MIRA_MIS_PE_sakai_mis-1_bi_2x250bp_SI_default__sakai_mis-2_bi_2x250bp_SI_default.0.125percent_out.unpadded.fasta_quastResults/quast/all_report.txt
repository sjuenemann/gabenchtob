All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.125percent_out.unpadded
#Contigs                                   499                                                                                                       
#Contigs (>= 0 bp)                         499                                                                                                       
#Contigs (>= 1000 bp)                      326                                                                                                       
Largest contig                             107796                                                                                                    
Total length                               5602781                                                                                                   
Total length (>= 0 bp)                     5602781                                                                                                   
Total length (>= 1000 bp)                  5508132                                                                                                   
Reference length                           5594470                                                                                                   
N50                                        34277                                                                                                     
NG50                                       34277                                                                                                     
N75                                        17379                                                                                                     
NG75                                       17379                                                                                                     
L50                                        47                                                                                                        
LG50                                       47                                                                                                        
L75                                        102                                                                                                       
LG75                                       102                                                                                                       
#local misassemblies                       10                                                                                                        
#misassemblies                             41                                                                                                        
#misassembled contigs                      32                                                                                                        
Misassembled contigs length                719384                                                                                                    
Misassemblies inter-contig overlap         41691                                                                                                     
#unaligned contigs                         4 + 1 part                                                                                                
Unaligned contigs length                   7050                                                                                                      
#ambiguously mapped contigs                69                                                                                                        
Extra bases in ambiguously mapped contigs  -58498                                                                                                    
#N's                                       119                                                                                                       
#N's per 100 kbp                           2.12                                                                                                      
Genome fraction (%)                        97.878                                                                                                    
Duplication ratio                          1.019                                                                                                     
#genes                                     5093 + 280 part                                                                                           
#predicted genes (unique)                  5674                                                                                                      
#predicted genes (>= 0 bp)                 5713                                                                                                      
#predicted genes (>= 300 bp)               4733                                                                                                      
#predicted genes (>= 1500 bp)              671                                                                                                       
#predicted genes (>= 3000 bp)              70                                                                                                        
#mismatches                                2649                                                                                                      
#indels                                    141                                                                                                       
Indels length                              179                                                                                                       
#mismatches per 100 kbp                    48.38                                                                                                     
#indels per 100 kbp                        2.57                                                                                                      
GC (%)                                     50.45                                                                                                     
Reference GC (%)                           50.48                                                                                                     
Average %IDY                               98.733                                                                                                    
Largest alignment                          107796                                                                                                    
NA50                                       32426                                                                                                     
NGA50                                      32426                                                                                                     
NA75                                       16194                                                                                                     
NGA75                                      16261                                                                                                     
LA50                                       49                                                                                                        
LGA50                                      49                                                                                                        
LA75                                       109                                                                                                       
LGA75                                      108                                                                                                       
#Mis_misassemblies                         41                                                                                                        
#Mis_relocations                           40                                                                                                        
#Mis_translocations                        0                                                                                                         
#Mis_inversions                            1                                                                                                         
#Mis_misassembled contigs                  32                                                                                                        
Mis_Misassembled contigs length            719384                                                                                                    
#Mis_local misassemblies                   10                                                                                                        
#Mis_short indels (<= 5 bp)                139                                                                                                       
#Mis_long indels (> 5 bp)                  2                                                                                                         
#Una_fully unaligned contigs               4                                                                                                         
Una_Fully unaligned length                 6808                                                                                                      
#Una_partially unaligned contigs           1                                                                                                         
#Una_with misassembly                      0                                                                                                         
#Una_both parts are significant            1                                                                                                         
Una_Partially unaligned length             242                                                                                                       
GAGE_Contigs #                             499                                                                                                       
GAGE_Min contig                            218                                                                                                       
GAGE_Max contig                            107796                                                                                                    
GAGE_N50                                   34277 COUNT: 47                                                                                           
GAGE_Genome size                           5594470                                                                                                   
GAGE_Assembly size                         5602781                                                                                                   
GAGE_Chaff bases                           0                                                                                                         
GAGE_Missing reference bases               11603(0.21%)                                                                                              
GAGE_Missing assembly bases                7235(0.13%)                                                                                               
GAGE_Missing assembly contigs              4(0.80%)                                                                                                  
GAGE_Duplicated reference bases            148490                                                                                                    
GAGE_Compressed reference bases            174971                                                                                                    
GAGE_Bad trim                              409                                                                                                       
GAGE_Avg idy                               99.95                                                                                                     
GAGE_SNPs                                  610                                                                                                       
GAGE_Indels < 5bp                          33                                                                                                        
GAGE_Indels >= 5                           6                                                                                                         
GAGE_Inversions                            20                                                                                                        
GAGE_Relocation                            8                                                                                                         
GAGE_Translocation                         0                                                                                                         
GAGE_Corrected contig #                    355                                                                                                       
GAGE_Corrected assembly size               5507990                                                                                                   
GAGE_Min correct contig                    392                                                                                                       
GAGE_Max correct contig                    107796                                                                                                    
GAGE_Corrected N50                         32209 COUNT: 51                                                                                           
