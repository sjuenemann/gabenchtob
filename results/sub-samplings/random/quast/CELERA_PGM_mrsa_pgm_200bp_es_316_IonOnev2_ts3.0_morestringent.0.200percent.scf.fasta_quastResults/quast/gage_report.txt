All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.200percent.scf
GAGE_Contigs #                   491                                                                           
GAGE_Min contig                  1001                                                                          
GAGE_Max contig                  46536                                                                         
GAGE_N50                         8493 COUNT: 92                                                                
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2740947                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     124423(4.42%)                                                                 
GAGE_Missing assembly bases      545(0.02%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  0                                                                             
GAGE_Compressed reference bases  14354                                                                         
GAGE_Bad trim                    545                                                                           
GAGE_Avg idy                     99.96                                                                         
GAGE_SNPs                        77                                                                            
GAGE_Indels < 5bp                491                                                                           
GAGE_Indels >= 5                 5                                                                             
GAGE_Inversions                  1                                                                             
GAGE_Relocation                  3                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          500                                                                           
GAGE_Corrected assembly size     2742774                                                                       
GAGE_Min correct contig          225                                                                           
GAGE_Max correct contig          46540                                                                         
GAGE_Corrected N50               8472 COUNT: 92                                                                
