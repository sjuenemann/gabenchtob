All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.975percent.contigs
#Contigs                                   353                                                                
#Contigs (>= 0 bp)                         1309                                                               
#Contigs (>= 1000 bp)                      150                                                                
Largest contig                             278188                                                             
Total length                               5355875                                                            
Total length (>= 0 bp)                     5488345                                                            
Total length (>= 1000 bp)                  5272120                                                            
Reference length                           5594470                                                            
N50                                        111015                                                             
NG50                                       105969                                                             
N75                                        46443                                                              
NG75                                       43147                                                              
L50                                        17                                                                 
LG50                                       18                                                                 
L75                                        35                                                                 
LG75                                       39                                                                 
#local misassemblies                       2                                                                  
#misassemblies                             22                                                                 
#misassembled contigs                      22                                                                 
Misassembled contigs length                315906                                                             
Misassemblies inter-contig overlap         53                                                                 
#unaligned contigs                         0 + 3 part                                                         
Unaligned contigs length                   173                                                                
#ambiguously mapped contigs                111                                                                
Extra bases in ambiguously mapped contigs  -87551                                                             
#N's                                       0                                                                  
#N's per 100 kbp                           0.00                                                               
Genome fraction (%)                        93.979                                                             
Duplication ratio                          1.002                                                              
#genes                                     4953 + 145 part                                                    
#predicted genes (unique)                  5365                                                               
#predicted genes (>= 0 bp)                 5367                                                               
#predicted genes (>= 300 bp)               4469                                                               
#predicted genes (>= 1500 bp)              663                                                                
#predicted genes (>= 3000 bp)              64                                                                 
#mismatches                                46                                                                 
#indels                                    143                                                                
Indels length                              150                                                                
#mismatches per 100 kbp                    0.87                                                               
#indels per 100 kbp                        2.72                                                               
GC (%)                                     50.29                                                              
Reference GC (%)                           50.48                                                              
Average %IDY                               98.876                                                             
Largest alignment                          224039                                                             
NA50                                       111015                                                             
NGA50                                      105969                                                             
NA75                                       46443                                                              
NGA75                                      43147                                                              
LA50                                       18                                                                 
LGA50                                      19                                                                 
LA75                                       36                                                                 
LGA75                                      40                                                                 
#Mis_misassemblies                         22                                                                 
#Mis_relocations                           21                                                                 
#Mis_translocations                        1                                                                  
#Mis_inversions                            0                                                                  
#Mis_misassembled contigs                  22                                                                 
Mis_Misassembled contigs length            315906                                                             
#Mis_local misassemblies                   2                                                                  
#Mis_short indels (<= 5 bp)                143                                                                
#Mis_long indels (> 5 bp)                  0                                                                  
#Una_fully unaligned contigs               0                                                                  
Una_Fully unaligned length                 0                                                                  
#Una_partially unaligned contigs           3                                                                  
#Una_with misassembly                      0                                                                  
#Una_both parts are significant            0                                                                  
Una_Partially unaligned length             173                                                                
GAGE_Contigs #                             353                                                                
GAGE_Min contig                            200                                                                
GAGE_Max contig                            278188                                                             
GAGE_N50                                   105969 COUNT: 18                                                   
GAGE_Genome size                           5594470                                                            
GAGE_Assembly size                         5355875                                                            
GAGE_Chaff bases                           0                                                                  
GAGE_Missing reference bases               36115(0.65%)                                                       
GAGE_Missing assembly bases                98(0.00%)                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                           
GAGE_Duplicated reference bases            12907                                                              
GAGE_Compressed reference bases            233896                                                             
GAGE_Bad trim                              98                                                                 
GAGE_Avg idy                               99.99                                                              
GAGE_SNPs                                  81                                                                 
GAGE_Indels < 5bp                          163                                                                
GAGE_Indels >= 5                           1                                                                  
GAGE_Inversions                            0                                                                  
GAGE_Relocation                            3                                                                  
GAGE_Translocation                         0                                                                  
GAGE_Corrected contig #                    304                                                                
GAGE_Corrected assembly size               5342670                                                            
GAGE_Min correct contig                    200                                                                
GAGE_Max correct contig                    224043                                                             
GAGE_Corrected N50                         105972 COUNT: 19                                                   
