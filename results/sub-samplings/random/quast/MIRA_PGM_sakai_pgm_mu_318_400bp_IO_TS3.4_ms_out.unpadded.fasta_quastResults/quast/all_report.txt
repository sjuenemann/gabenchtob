All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_out.unpadded
#Contigs                                   3439                                                    
#Contigs (>= 0 bp)                         3743                                                    
#Contigs (>= 1000 bp)                      156                                                     
Largest contig                             376546                                                  
Total length                               7275218                                                 
Total length (>= 0 bp)                     7320257                                                 
Total length (>= 1000 bp)                  5697886                                                 
Reference length                           5594470                                                 
N50                                        106642                                                  
NG50                                       158319                                                  
N75                                        10770                                                   
NG75                                       99953                                                   
L50                                        19                                                      
LG50                                       12                                                      
L75                                        58                                                      
LG75                                       24                                                      
#local misassemblies                       29                                                      
#misassemblies                             193                                                     
#misassembled contigs                      158                                                     
Misassembled contigs length                3624521                                                 
Misassemblies inter-contig overlap         67685                                                   
#unaligned contigs                         11 + 31 part                                            
Unaligned contigs length                   6524                                                    
#ambiguously mapped contigs                153                                                     
Extra bases in ambiguously mapped contigs  -105512                                                 
#N's                                       475                                                     
#N's per 100 kbp                           6.53                                                    
Genome fraction (%)                        98.952                                                  
Duplication ratio                          1.306                                                   
#genes                                     5346 + 66 part                                          
#predicted genes (unique)                  10044                                                   
#predicted genes (>= 0 bp)                 10213                                                   
#predicted genes (>= 300 bp)               5903                                                    
#predicted genes (>= 1500 bp)              665                                                     
#predicted genes (>= 3000 bp)              66                                                      
#mismatches                                2451                                                    
#indels                                    1117                                                    
Indels length                              1183                                                    
#mismatches per 100 kbp                    44.28                                                   
#indels per 100 kbp                        20.18                                                   
GC (%)                                     50.00                                                   
Reference GC (%)                           50.48                                                   
Average %IDY                               98.907                                                  
Largest alignment                          346273                                                  
NA50                                       78391                                                   
NGA50                                      125506                                                  
NA75                                       6498                                                    
NGA75                                      56833                                                   
LA50                                       24                                                      
LGA50                                      15                                                      
LA75                                       90                                                      
LGA75                                      32                                                      
#Mis_misassemblies                         193                                                     
#Mis_relocations                           180                                                     
#Mis_translocations                        13                                                      
#Mis_inversions                            0                                                       
#Mis_misassembled contigs                  158                                                     
Mis_Misassembled contigs length            3624521                                                 
#Mis_local misassemblies                   29                                                      
#Mis_short indels (<= 5 bp)                1116                                                    
#Mis_long indels (> 5 bp)                  1                                                       
#Una_fully unaligned contigs               11                                                      
Una_Fully unaligned length                 4301                                                    
#Una_partially unaligned contigs           31                                                      
#Una_with misassembly                      0                                                       
#Una_both parts are significant            1                                                       
Una_Partially unaligned length             2223                                                    
GAGE_Contigs #                             3439                                                    
GAGE_Min contig                            201                                                     
GAGE_Max contig                            376546                                                  
GAGE_N50                                   158319 COUNT: 12                                        
GAGE_Genome size                           5594470                                                 
GAGE_Assembly size                         7275218                                                 
GAGE_Chaff bases                           0                                                       
GAGE_Missing reference bases               106(0.00%)                                              
GAGE_Missing assembly bases                4145(0.06%)                                             
GAGE_Missing assembly contigs              1(0.03%)                                                
GAGE_Duplicated reference bases            1761670                                                 
GAGE_Compressed reference bases            109721                                                  
GAGE_Bad trim                              3874                                                    
GAGE_Avg idy                               99.94                                                   
GAGE_SNPs                                  710                                                     
GAGE_Indels < 5bp                          955                                                     
GAGE_Indels >= 5                           6                                                       
GAGE_Inversions                            28                                                      
GAGE_Relocation                            12                                                      
GAGE_Translocation                         2                                                       
GAGE_Corrected contig #                    157                                                     
GAGE_Corrected assembly size               5618075                                                 
GAGE_Min correct contig                    232                                                     
GAGE_Max correct contig                    256079                                                  
GAGE_Corrected N50                         104382 COUNT: 18                                        
