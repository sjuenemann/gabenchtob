All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_out.unpadded
GAGE_Contigs #                   3439                                                    
GAGE_Min contig                  201                                                     
GAGE_Max contig                  376546                                                  
GAGE_N50                         158319 COUNT: 12                                        
GAGE_Genome size                 5594470                                                 
GAGE_Assembly size               7275218                                                 
GAGE_Chaff bases                 0                                                       
GAGE_Missing reference bases     106(0.00%)                                              
GAGE_Missing assembly bases      4145(0.06%)                                             
GAGE_Missing assembly contigs    1(0.03%)                                                
GAGE_Duplicated reference bases  1761670                                                 
GAGE_Compressed reference bases  109721                                                  
GAGE_Bad trim                    3874                                                    
GAGE_Avg idy                     99.94                                                   
GAGE_SNPs                        710                                                     
GAGE_Indels < 5bp                955                                                     
GAGE_Indels >= 5                 6                                                       
GAGE_Inversions                  28                                                      
GAGE_Relocation                  12                                                      
GAGE_Translocation               2                                                       
GAGE_Corrected contig #          157                                                     
GAGE_Corrected assembly size     5618075                                                 
GAGE_Min correct contig          232                                                     
GAGE_Max correct contig          256079                                                  
GAGE_Corrected N50               104382 COUNT: 18                                        
