All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                  #Mis_misassemblies  #Mis_relocations  #Mis_translocations  #Mis_inversions  #Mis_misassembled contigs  Mis_Misassembled contigs length  #Mis_local misassemblies  #mismatches  #indels  #Mis_short indels (<= 5 bp)  #Mis_long indels (> 5 bp)  Indels length
MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_out.unpadded  193                 180               13                   0                158                        3624521                          29                        2451         1117     1116                         1                          1183         
