All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.9percent.scf
GAGE_Contigs #                   823                                                                    
GAGE_Min contig                  1002                                                                   
GAGE_Max contig                  43197                                                                  
GAGE_N50                         12152 COUNT: 139                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5225457                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     390278(6.98%)                                                          
GAGE_Missing assembly bases      1284(0.02%)                                                            
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  92                                                                     
GAGE_Compressed reference bases  110225                                                                 
GAGE_Bad trim                    1284                                                                   
GAGE_Avg idy                     99.98                                                                  
GAGE_SNPs                        144                                                                    
GAGE_Indels < 5bp                740                                                                    
GAGE_Indels >= 5                 6                                                                      
GAGE_Inversions                  3                                                                      
GAGE_Relocation                  8                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          838                                                                    
GAGE_Corrected assembly size     5227546                                                                
GAGE_Min correct contig          248                                                                    
GAGE_Max correct contig          43202                                                                  
GAGE_Corrected N50               11978 COUNT: 140                                                       
