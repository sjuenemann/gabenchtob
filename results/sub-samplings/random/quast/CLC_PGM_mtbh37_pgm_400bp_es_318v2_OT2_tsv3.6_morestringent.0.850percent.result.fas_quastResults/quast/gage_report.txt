All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.850percent.result
GAGE_Contigs #                   888                                                                           
GAGE_Min contig                  201                                                                           
GAGE_Max contig                  69176                                                                         
GAGE_N50                         21401 COUNT: 67                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4332377                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     118723(2.69%)                                                                 
GAGE_Missing assembly bases      3536(0.08%)                                                                   
GAGE_Missing assembly contigs    6(0.68%)                                                                      
GAGE_Duplicated reference bases  73302                                                                         
GAGE_Compressed reference bases  50260                                                                         
GAGE_Bad trim                    1519                                                                          
GAGE_Avg idy                     99.97                                                                         
GAGE_SNPs                        166                                                                           
GAGE_Indels < 5bp                1150                                                                          
GAGE_Indels >= 5                 10                                                                            
GAGE_Inversions                  10                                                                            
GAGE_Relocation                  3                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          658                                                                           
GAGE_Corrected assembly size     4256170                                                                       
GAGE_Min correct contig          202                                                                           
GAGE_Max correct contig          69189                                                                         
GAGE_Corrected N50               21092 COUNT: 69                                                               
