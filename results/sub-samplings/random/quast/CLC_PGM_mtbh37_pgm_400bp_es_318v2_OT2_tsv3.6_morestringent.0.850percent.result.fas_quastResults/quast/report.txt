All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.850percent.result
#Contigs (>= 0 bp)             888                                                                           
#Contigs (>= 1000 bp)          316                                                                           
Total length (>= 0 bp)         4332377                                                                       
Total length (>= 1000 bp)      4134691                                                                       
#Contigs                       888                                                                           
Largest contig                 69176                                                                         
Total length                   4332377                                                                       
Reference length               4411532                                                                       
GC (%)                         65.35                                                                         
Reference GC (%)               65.61                                                                         
N50                            21712                                                                         
NG50                           21401                                                                         
N75                            10589                                                                         
NG75                           10100                                                                         
#misassemblies                 17                                                                            
#local misassemblies           10                                                                            
#unaligned contigs             5 + 21 part                                                                   
Unaligned contigs length       2783                                                                          
Genome fraction (%)            96.037                                                                        
Duplication ratio              1.024                                                                         
#N's per 100 kbp               19.13                                                                         
#mismatches per 100 kbp        5.10                                                                          
#indels per 100 kbp            27.95                                                                         
#genes                         3697 + 350 part                                                               
#predicted genes (unique)      4971                                                                          
#predicted genes (>= 0 bp)     4979                                                                          
#predicted genes (>= 300 bp)   3827                                                                          
#predicted genes (>= 1500 bp)  443                                                                           
#predicted genes (>= 3000 bp)  45                                                                            
Largest alignment              69176                                                                         
NA50                           21712                                                                         
NGA50                          21385                                                                         
NA75                           10482                                                                         
NGA75                          10073                                                                         
