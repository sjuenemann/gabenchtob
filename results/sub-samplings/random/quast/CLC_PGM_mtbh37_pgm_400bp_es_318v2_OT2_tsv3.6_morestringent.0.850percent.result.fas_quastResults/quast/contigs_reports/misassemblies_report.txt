All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.850percent.result
#Mis_misassemblies               17                                                                            
#Mis_relocations                 17                                                                            
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        16                                                                            
Mis_Misassembled contigs length  79277                                                                         
#Mis_local misassemblies         10                                                                            
#mismatches                      216                                                                           
#indels                          1184                                                                          
#Mis_short indels (<= 5 bp)      1180                                                                          
#Mis_long indels (> 5 bp)        4                                                                             
Indels length                    1373                                                                          
