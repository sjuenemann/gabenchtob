All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.700percent_out.unpadded
#Mis_misassemblies               179                                                                             
#Mis_relocations                 168                                                                             
#Mis_translocations              6                                                                               
#Mis_inversions                  5                                                                               
#Mis_misassembled contigs        176                                                                             
Mis_Misassembled contigs length  417568                                                                          
#Mis_local misassemblies         19                                                                              
#mismatches                      110                                                                             
#indels                          176                                                                             
#Mis_short indels (<= 5 bp)      175                                                                             
#Mis_long indels (> 5 bp)        1                                                                               
Indels length                    196                                                                             
