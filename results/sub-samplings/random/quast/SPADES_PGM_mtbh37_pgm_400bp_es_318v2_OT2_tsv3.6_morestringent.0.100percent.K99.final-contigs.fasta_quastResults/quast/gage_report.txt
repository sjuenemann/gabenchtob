All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.100percent.K99.final-contigs
GAGE_Contigs #                   750                                                                                         
GAGE_Min contig                  220                                                                                         
GAGE_Max contig                  44688                                                                                       
GAGE_N50                         8777 COUNT: 139                                                                             
GAGE_Genome size                 4411532                                                                                     
GAGE_Assembly size               4176687                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     201834(4.58%)                                                                               
GAGE_Missing assembly bases      2065(0.05%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  12820                                                                                       
GAGE_Compressed reference bases  54280                                                                                       
GAGE_Bad trim                    1222                                                                                        
GAGE_Avg idy                     99.84                                                                                       
GAGE_SNPs                        542                                                                                         
GAGE_Indels < 5bp                5670                                                                                        
GAGE_Indels >= 5                 23                                                                                          
GAGE_Inversions                  8                                                                                           
GAGE_Relocation                  19                                                                                          
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          799                                                                                         
GAGE_Corrected assembly size     4169586                                                                                     
GAGE_Min correct contig          220                                                                                         
GAGE_Max correct contig          44712                                                                                       
GAGE_Corrected N50               7986 COUNT: 154                                                                             
