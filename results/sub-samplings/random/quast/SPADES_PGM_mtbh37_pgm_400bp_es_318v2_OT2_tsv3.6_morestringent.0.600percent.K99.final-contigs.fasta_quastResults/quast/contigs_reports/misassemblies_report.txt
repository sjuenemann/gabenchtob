All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.600percent.K99.final-contigs
#Mis_misassemblies               8                                                                                           
#Mis_relocations                 8                                                                                           
#Mis_translocations              0                                                                                           
#Mis_inversions                  0                                                                                           
#Mis_misassembled contigs        7                                                                                           
Mis_Misassembled contigs length  256605                                                                                      
#Mis_local misassemblies         31                                                                                          
#mismatches                      433                                                                                         
#indels                          1687                                                                                        
#Mis_short indels (<= 5 bp)      1679                                                                                        
#Mis_long indels (> 5 bp)        8                                                                                           
Indels length                    2148                                                                                        
