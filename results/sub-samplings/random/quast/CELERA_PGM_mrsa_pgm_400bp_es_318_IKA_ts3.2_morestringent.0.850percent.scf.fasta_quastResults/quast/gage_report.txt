All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.850percent.scf
GAGE_Contigs #                   51                                                                       
GAGE_Min contig                  1002                                                                     
GAGE_Max contig                  134545                                                                   
GAGE_N50                         0 COUNT: 0                                                               
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               396320                                                                   
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     2402570(85.38%)                                                          
GAGE_Missing assembly bases      1577(0.40%)                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                                 
GAGE_Duplicated reference bases  2061                                                                     
GAGE_Compressed reference bases  30614                                                                    
GAGE_Bad trim                    1577                                                                     
GAGE_Avg idy                     99.96                                                                    
GAGE_SNPs                        20                                                                       
GAGE_Indels < 5bp                90                                                                       
GAGE_Indels >= 5                 0                                                                        
GAGE_Inversions                  0                                                                        
GAGE_Relocation                  1                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          49                                                                       
GAGE_Corrected assembly size     392607                                                                   
GAGE_Min correct contig          1002                                                                     
GAGE_Max correct contig          134548                                                                   
GAGE_Corrected N50               0 COUNT: 0                                                               
