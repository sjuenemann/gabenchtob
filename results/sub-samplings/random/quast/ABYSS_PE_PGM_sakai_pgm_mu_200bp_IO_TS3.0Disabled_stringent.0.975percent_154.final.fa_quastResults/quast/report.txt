All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.975percent_154.final
#Contigs (>= 0 bp)             4721                                                                             
#Contigs (>= 1000 bp)          953                                                                              
Total length (>= 0 bp)         6183922                                                                          
Total length (>= 1000 bp)      5066864                                                                          
#Contigs                       3978                                                                             
Largest contig                 39329                                                                            
Total length                   6053488                                                                          
Reference length               5594470                                                                          
GC (%)                         50.62                                                                            
Reference GC (%)               50.48                                                                            
N50                            6141                                                                             
NG50                           6919                                                                             
N75                            2533                                                                             
NG75                           3496                                                                             
#misassemblies                 5                                                                                
#local misassemblies           0                                                                                
#unaligned contigs             0 + 1 part                                                                       
Unaligned contigs length       47                                                                               
Genome fraction (%)            94.806                                                                           
Duplication ratio              1.097                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        1.28                                                                             
#indels per 100 kbp            31.32                                                                            
#genes                         4233 + 976 part                                                                  
#predicted genes (unique)      9332                                                                             
#predicted genes (>= 0 bp)     9499                                                                             
#predicted genes (>= 300 bp)   5318                                                                             
#predicted genes (>= 1500 bp)  450                                                                              
#predicted genes (>= 3000 bp)  21                                                                               
Largest alignment              39329                                                                            
NA50                           6141                                                                             
NGA50                          6919                                                                             
NA75                           2494                                                                             
NGA75                          3466                                                                             
