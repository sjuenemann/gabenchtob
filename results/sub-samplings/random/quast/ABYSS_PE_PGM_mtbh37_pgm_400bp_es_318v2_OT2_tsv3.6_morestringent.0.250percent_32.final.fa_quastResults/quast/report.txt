All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.250percent_32.final
#Contigs (>= 0 bp)             4532                                                                                 
#Contigs (>= 1000 bp)          1186                                                                                 
Total length (>= 0 bp)         4256107                                                                              
Total length (>= 1000 bp)      3633145                                                                              
#Contigs                       2036                                                                                 
Largest contig                 16117                                                                                
Total length                   4082037                                                                              
Reference length               4411532                                                                              
GC (%)                         65.08                                                                                
Reference GC (%)               65.61                                                                                
N50                            3432                                                                                 
NG50                           3142                                                                                 
N75                            1826                                                                                 
NG75                           1493                                                                                 
#misassemblies                 0                                                                                    
#local misassemblies           8                                                                                    
#unaligned contigs             1 + 3 part                                                                           
Unaligned contigs length       1017                                                                                 
Genome fraction (%)            92.078                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        3.72                                                                                 
#indels per 100 kbp            25.09                                                                                
#genes                         2596 + 1406 part                                                                     
#predicted genes (unique)      5591                                                                                 
#predicted genes (>= 0 bp)     5591                                                                                 
#predicted genes (>= 300 bp)   4092                                                                                 
#predicted genes (>= 1500 bp)  297                                                                                  
#predicted genes (>= 3000 bp)  17                                                                                   
Largest alignment              16117                                                                                
NA50                           3432                                                                                 
NGA50                          3142                                                                                 
NA75                           1826                                                                                 
NGA75                          1493                                                                                 
