All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.700percent.result
#Contigs                                   1084                                                                     
#Contigs (>= 0 bp)                         1084                                                                     
#Contigs (>= 1000 bp)                      175                                                                      
Largest contig                             80892                                                                    
Total length                               2969100                                                                  
Total length (>= 0 bp)                     2969100                                                                  
Total length (>= 1000 bp)                  2685190                                                                  
Reference length                           2813862                                                                  
N50                                        22819                                                                    
NG50                                       25492                                                                    
N75                                        11711                                                                    
NG75                                       13029                                                                    
L50                                        43                                                                       
LG50                                       40                                                                       
L75                                        87                                                                       
LG75                                       78                                                                       
#local misassemblies                       4                                                                        
#misassemblies                             6                                                                        
#misassembled contigs                      6                                                                        
Misassembled contigs length                61503                                                                    
Misassemblies inter-contig overlap         391                                                                      
#unaligned contigs                         26 + 19 part                                                             
Unaligned contigs length                   8891                                                                     
#ambiguously mapped contigs                22                                                                       
Extra bases in ambiguously mapped contigs  -10899                                                                   
#N's                                       2363                                                                     
#N's per 100 kbp                           79.59                                                                    
Genome fraction (%)                        97.315                                                                   
Duplication ratio                          1.077                                                                    
#genes                                     2526 + 152 part                                                          
#predicted genes (unique)                  3415                                                                     
#predicted genes (>= 0 bp)                 3450                                                                     
#predicted genes (>= 300 bp)               2372                                                                     
#predicted genes (>= 1500 bp)              272                                                                      
#predicted genes (>= 3000 bp)              27                                                                       
#mismatches                                80                                                                       
#indels                                    424                                                                      
Indels length                              440                                                                      
#mismatches per 100 kbp                    2.92                                                                     
#indels per 100 kbp                        15.48                                                                    
GC (%)                                     32.56                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               99.203                                                                   
Largest alignment                          80892                                                                    
NA50                                       22539                                                                    
NGA50                                      24126                                                                    
NA75                                       11164                                                                    
NGA75                                      12847                                                                    
LA50                                       43                                                                       
LGA50                                      40                                                                       
LA75                                       88                                                                       
LGA75                                      79                                                                       
#Mis_misassemblies                         6                                                                        
#Mis_relocations                           6                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  6                                                                        
Mis_Misassembled contigs length            61503                                                                    
#Mis_local misassemblies                   4                                                                        
#Mis_short indels (<= 5 bp)                424                                                                      
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               26                                                                       
Una_Fully unaligned length                 7224                                                                     
#Una_partially unaligned contigs           19                                                                       
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            1                                                                        
Una_Partially unaligned length             1667                                                                     
GAGE_Contigs #                             1084                                                                     
GAGE_Min contig                            200                                                                      
GAGE_Max contig                            80892                                                                    
GAGE_N50                                   25492 COUNT: 40                                                          
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         2969100                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               34471(1.23%)                                                             
GAGE_Missing assembly bases                9457(0.32%)                                                              
GAGE_Missing assembly contigs              26(2.40%)                                                                
GAGE_Duplicated reference bases            205576                                                                   
GAGE_Compressed reference bases            40753                                                                    
GAGE_Bad trim                              2132                                                                     
GAGE_Avg idy                               99.98                                                                    
GAGE_SNPs                                  62                                                                       
GAGE_Indels < 5bp                          289                                                                      
GAGE_Indels >= 5                           1                                                                        
GAGE_Inversions                            1                                                                        
GAGE_Relocation                            2                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    374                                                                      
GAGE_Corrected assembly size               2754760                                                                  
GAGE_Min correct contig                    201                                                                      
GAGE_Max correct contig                    80900                                                                    
GAGE_Corrected N50                         22821 COUNT: 40                                                          
