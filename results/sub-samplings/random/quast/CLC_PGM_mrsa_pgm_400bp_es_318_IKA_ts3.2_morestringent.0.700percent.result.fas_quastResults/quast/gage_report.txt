All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.700percent.result
GAGE_Contigs #                   1084                                                                     
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  80892                                                                    
GAGE_N50                         25492 COUNT: 40                                                          
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2969100                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     34471(1.23%)                                                             
GAGE_Missing assembly bases      9457(0.32%)                                                              
GAGE_Missing assembly contigs    26(2.40%)                                                                
GAGE_Duplicated reference bases  205576                                                                   
GAGE_Compressed reference bases  40753                                                                    
GAGE_Bad trim                    2132                                                                     
GAGE_Avg idy                     99.98                                                                    
GAGE_SNPs                        62                                                                       
GAGE_Indels < 5bp                289                                                                      
GAGE_Indels >= 5                 1                                                                        
GAGE_Inversions                  1                                                                        
GAGE_Relocation                  2                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          374                                                                      
GAGE_Corrected assembly size     2754760                                                                  
GAGE_Min correct contig          201                                                                      
GAGE_Max correct contig          80900                                                                    
GAGE_Corrected N50               22821 COUNT: 40                                                          
