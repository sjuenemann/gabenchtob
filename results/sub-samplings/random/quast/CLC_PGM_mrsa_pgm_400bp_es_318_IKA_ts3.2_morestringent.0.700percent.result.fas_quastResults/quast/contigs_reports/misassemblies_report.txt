All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.700percent.result
#Mis_misassemblies               6                                                                        
#Mis_relocations                 6                                                                        
#Mis_translocations              0                                                                        
#Mis_inversions                  0                                                                        
#Mis_misassembled contigs        6                                                                        
Mis_Misassembled contigs length  61503                                                                    
#Mis_local misassemblies         4                                                                        
#mismatches                      80                                                                       
#indels                          424                                                                      
#Mis_short indels (<= 5 bp)      424                                                                      
#Mis_long indels (> 5 bp)        0                                                                        
Indels length                    440                                                                      
