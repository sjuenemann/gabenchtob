All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.550percent_38.final
#Contigs                                   1646                                                                                 
#Contigs (>= 0 bp)                         5623                                                                                 
#Contigs (>= 1000 bp)                      1092                                                                                 
Largest contig                             25016                                                                                
Total length                               4184544                                                                              
Total length (>= 0 bp)                     4465083                                                                              
Total length (>= 1000 bp)                  3895223                                                                              
Reference length                           4411532                                                                              
N50                                        4299                                                                                 
NG50                                       4125                                                                                 
N75                                        2401                                                                                 
NG75                                       2099                                                                                 
L50                                        296                                                                                  
LG50                                       323                                                                                  
L75                                        620                                                                                  
LG75                                       695                                                                                  
#local misassemblies                       5                                                                                    
#misassemblies                             0                                                                                    
#misassembled contigs                      0                                                                                    
Misassembled contigs length                0                                                                                    
Misassemblies inter-contig overlap         684                                                                                  
#unaligned contigs                         1 + 0 part                                                                           
Unaligned contigs length                   786                                                                                  
#ambiguously mapped contigs                25                                                                                   
Extra bases in ambiguously mapped contigs  -12799                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.438                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     2885 + 1134 part                                                                     
#predicted genes (unique)                  5264                                                                                 
#predicted genes (>= 0 bp)                 5264                                                                                 
#predicted genes (>= 300 bp)               4051                                                                                 
#predicted genes (>= 1500 bp)              345                                                                                  
#predicted genes (>= 3000 bp)              24                                                                                   
#mismatches                                124                                                                                  
#indels                                    725                                                                                  
Indels length                              760                                                                                  
#mismatches per 100 kbp                    2.98                                                                                 
#indels per 100 kbp                        17.40                                                                                
GC (%)                                     65.25                                                                                
Reference GC (%)                           65.61                                                                                
Average %IDY                               99.958                                                                               
Largest alignment                          25016                                                                                
NA50                                       4299                                                                                 
NGA50                                      4125                                                                                 
NA75                                       2401                                                                                 
NGA75                                      2099                                                                                 
LA50                                       296                                                                                  
LGA50                                      323                                                                                  
LA75                                       620                                                                                  
LGA75                                      695                                                                                  
#Mis_misassemblies                         0                                                                                    
#Mis_relocations                           0                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  0                                                                                    
Mis_Misassembled contigs length            0                                                                                    
#Mis_local misassemblies                   5                                                                                    
#Mis_short indels (<= 5 bp)                724                                                                                  
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               1                                                                                    
Una_Fully unaligned length                 786                                                                                  
#Una_partially unaligned contigs           0                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             0                                                                                    
GAGE_Contigs #                             1646                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            25016                                                                                
GAGE_N50                                   4125 COUNT: 323                                                                      
GAGE_Genome size                           4411532                                                                              
GAGE_Assembly size                         4184544                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               212205(4.81%)                                                                        
GAGE_Missing assembly bases                838(0.02%)                                                                           
GAGE_Missing assembly contigs              1(0.06%)                                                                             
GAGE_Duplicated reference bases            0                                                                                    
GAGE_Compressed reference bases            22286                                                                                
GAGE_Bad trim                              52                                                                                   
GAGE_Avg idy                               99.98                                                                                
GAGE_SNPs                                  84                                                                                   
GAGE_Indels < 5bp                          745                                                                                  
GAGE_Indels >= 5                           5                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            2                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1651                                                                                 
GAGE_Corrected assembly size               4184916                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    25016                                                                                
GAGE_Corrected N50                         4103 COUNT: 324                                                                      
