All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.025percent.result
GAGE_Contigs #                   774                                                                      
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  41669                                                                    
GAGE_N50                         10532 COUNT: 78                                                          
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2804107                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     54614(1.94%)                                                             
GAGE_Missing assembly bases      1453(0.05%)                                                              
GAGE_Missing assembly contigs    1(0.13%)                                                                 
GAGE_Duplicated reference bases  51965                                                                    
GAGE_Compressed reference bases  43244                                                                    
GAGE_Bad trim                    709                                                                      
GAGE_Avg idy                     99.90                                                                    
GAGE_SNPs                        166                                                                      
GAGE_Indels < 5bp                1860                                                                     
GAGE_Indels >= 5                 5                                                                        
GAGE_Inversions                  2                                                                        
GAGE_Relocation                  4                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          631                                                                      
GAGE_Corrected assembly size     2753378                                                                  
GAGE_Min correct contig          200                                                                      
GAGE_Max correct contig          41671                                                                    
GAGE_Corrected N50               10210 COUNT: 84                                                          
