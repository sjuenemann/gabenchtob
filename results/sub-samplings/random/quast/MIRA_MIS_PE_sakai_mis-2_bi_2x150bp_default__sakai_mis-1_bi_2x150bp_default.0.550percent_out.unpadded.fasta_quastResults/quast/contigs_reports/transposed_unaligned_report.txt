All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                                              #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.550percent_out.unpadded  4                             6574                        5                                 0                      0                                278                             31  
