All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.200percent.contigs
#Contigs                                   116                                                                                
#Contigs (>= 0 bp)                         162                                                                                
#Contigs (>= 1000 bp)                      91                                                                                 
Largest contig                             182162                                                                             
Total length                               2764697                                                                            
Total length (>= 0 bp)                     2770891                                                                            
Total length (>= 1000 bp)                  2754374                                                                            
Reference length                           2813862                                                                            
N50                                        46699                                                                              
NG50                                       46109                                                                              
N75                                        32605                                                                              
NG75                                       31967                                                                              
L50                                        18                                                                                 
LG50                                       19                                                                                 
L75                                        36                                                                                 
LG75                                       37                                                                                 
#local misassemblies                       5                                                                                  
#misassemblies                             2                                                                                  
#misassembled contigs                      2                                                                                  
Misassembled contigs length                46918                                                                              
Misassemblies inter-contig overlap         20734                                                                              
#unaligned contigs                         0 + 1 part                                                                         
Unaligned contigs length                   60                                                                                 
#ambiguously mapped contigs                12                                                                                 
Extra bases in ambiguously mapped contigs  -9179                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.904                                                                             
Duplication ratio                          1.008                                                                              
#genes                                     2635 + 52 part                                                                     
#predicted genes (unique)                  2713                                                                               
#predicted genes (>= 0 bp)                 2713                                                                               
#predicted genes (>= 300 bp)               2322                                                                               
#predicted genes (>= 1500 bp)              285                                                                                
#predicted genes (>= 3000 bp)              27                                                                                 
#mismatches                                62                                                                                 
#indels                                    241                                                                                
Indels length                              364                                                                                
#mismatches per 100 kbp                    2.25                                                                               
#indels per 100 kbp                        8.75                                                                               
GC (%)                                     32.64                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               99.143                                                                             
Largest alignment                          182161                                                                             
NA50                                       46109                                                                              
NGA50                                      45831                                                                              
NA75                                       32605                                                                              
NGA75                                      31967                                                                              
LA50                                       18                                                                                 
LGA50                                      19                                                                                 
LA75                                       36                                                                                 
LGA75                                      37                                                                                 
#Mis_misassemblies                         2                                                                                  
#Mis_relocations                           1                                                                                  
#Mis_translocations                        1                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  2                                                                                  
Mis_Misassembled contigs length            46918                                                                              
#Mis_local misassemblies                   5                                                                                  
#Mis_short indels (<= 5 bp)                239                                                                                
#Mis_long indels (> 5 bp)                  2                                                                                  
#Una_fully unaligned contigs               0                                                                                  
Una_Fully unaligned length                 0                                                                                  
#Una_partially unaligned contigs           1                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             60                                                                                 
GAGE_Contigs #                             116                                                                                
GAGE_Min contig                            211                                                                                
GAGE_Max contig                            182162                                                                             
GAGE_N50                                   46109 COUNT: 19                                                                    
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2764697                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               14011(0.50%)                                                                       
GAGE_Missing assembly bases                110(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            220                                                                                
GAGE_Compressed reference bases            38105                                                                              
GAGE_Bad trim                              110                                                                                
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  48                                                                                 
GAGE_Indels < 5bp                          267                                                                                
GAGE_Indels >= 5                           5                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            2                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    122                                                                                
GAGE_Corrected assembly size               2765379                                                                            
GAGE_Min correct contig                    211                                                                                
GAGE_Max correct contig                    182173                                                                             
GAGE_Corrected N50                         43047 COUNT: 20                                                                    
