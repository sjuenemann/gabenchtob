All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.175percent_out.unpadded
#Mis_misassemblies               43                                                                              
#Mis_relocations                 30                                                                              
#Mis_translocations              1                                                                               
#Mis_inversions                  12                                                                              
#Mis_misassembled contigs        36                                                                              
Mis_Misassembled contigs length  1319504                                                                         
#Mis_local misassemblies         12                                                                              
#mismatches                      1514                                                                            
#indels                          783                                                                             
#Mis_short indels (<= 5 bp)      778                                                                             
#Mis_long indels (> 5 bp)        5                                                                               
Indels length                    856                                                                             
