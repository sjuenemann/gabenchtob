All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.350percent.result
#Contigs (>= 0 bp)             5017                                                                       
#Contigs (>= 1000 bp)          805                                                                        
Total length (>= 0 bp)         3225877                                                                    
Total length (>= 1000 bp)      1281160                                                                    
#Contigs                       5017                                                                       
Largest contig                 5092                                                                       
Total length                   3225877                                                                    
Reference length               4411532                                                                    
GC (%)                         64.27                                                                      
Reference GC (%)               65.61                                                                      
N50                            784                                                                        
NG50                           529                                                                        
N75                            460                                                                        
NG75                           None                                                                       
#misassemblies                 1090                                                                       
#local misassemblies           52                                                                         
#unaligned contigs             12 + 645 part                                                              
Unaligned contigs length       36991                                                                      
Genome fraction (%)            67.796                                                                     
Duplication ratio              1.068                                                                      
#N's per 100 kbp               77.41                                                                      
#mismatches per 100 kbp        68.01                                                                      
#indels per 100 kbp            203.65                                                                     
#genes                         770 + 2941 part                                                            
#predicted genes (unique)      7437                                                                       
#predicted genes (>= 0 bp)     7442                                                                       
#predicted genes (>= 300 bp)   3371                                                                       
#predicted genes (>= 1500 bp)  10                                                                         
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              5046                                                                       
NA50                           676                                                                        
NGA50                          444                                                                        
NA75                           386                                                                        
NGA75                          None                                                                       
