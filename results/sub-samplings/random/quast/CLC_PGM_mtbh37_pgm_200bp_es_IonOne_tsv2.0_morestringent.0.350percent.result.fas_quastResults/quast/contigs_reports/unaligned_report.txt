All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.350percent.result
#Una_fully unaligned contigs      12                                                                         
Una_Fully unaligned length        6660                                                                       
#Una_partially unaligned contigs  645                                                                        
#Una_with misassembly             0                                                                          
#Una_both parts are significant   4                                                                          
Una_Partially unaligned length    30331                                                                      
#N's                              2497                                                                       
