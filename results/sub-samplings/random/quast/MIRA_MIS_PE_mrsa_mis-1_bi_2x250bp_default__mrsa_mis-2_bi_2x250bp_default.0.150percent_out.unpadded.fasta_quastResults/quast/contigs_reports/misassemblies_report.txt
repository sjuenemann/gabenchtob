All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.150percent_out.unpadded
#Mis_misassemblies               7                                                                                                 
#Mis_relocations                 7                                                                                                 
#Mis_translocations              0                                                                                                 
#Mis_inversions                  0                                                                                                 
#Mis_misassembled contigs        6                                                                                                 
Mis_Misassembled contigs length  507201                                                                                            
#Mis_local misassemblies         4                                                                                                 
#mismatches                      222                                                                                               
#indels                          38                                                                                                
#Mis_short indels (<= 5 bp)      33                                                                                                
#Mis_long indels (> 5 bp)        5                                                                                                 
Indels length                    117                                                                                               
