All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CELERA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.300percent.scf_broken  CELERA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.300percent.scf
#Una_fully unaligned contigs      1                                                                                                     1                                                                                            
Una_Fully unaligned length        5639                                                                                                  5639                                                                                         
#Una_partially unaligned contigs  2                                                                                                     2                                                                                            
#Una_with misassembly             0                                                                                                     0                                                                                            
#Una_both parts are significant   1                                                                                                     1                                                                                            
Una_Partially unaligned length    354                                                                                                   354                                                                                          
#N's                              0                                                                                                     472                                                                                          
