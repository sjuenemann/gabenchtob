All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.500percent_out.unpadded
#Contigs (>= 0 bp)             238                                                                                               
#Contigs (>= 1000 bp)          46                                                                                                
Total length (>= 0 bp)         2911484                                                                                           
Total length (>= 1000 bp)      2838148                                                                                           
#Contigs                       216                                                                                               
Largest contig                 565346                                                                                            
Total length                   2907800                                                                                           
Reference length               2813862                                                                                           
GC (%)                         32.96                                                                                             
Reference GC (%)               32.81                                                                                             
N50                            157433                                                                                            
NG50                           157433                                                                                            
N75                            88884                                                                                             
NG75                           93994                                                                                             
#misassemblies                 17                                                                                                
#local misassemblies           4                                                                                                 
#unaligned contigs             2 + 1 part                                                                                        
Unaligned contigs length       6277                                                                                              
Genome fraction (%)            99.904                                                                                            
Duplication ratio              1.037                                                                                             
#N's per 100 kbp               0.45                                                                                              
#mismatches per 100 kbp        6.40                                                                                              
#indels per 100 kbp            1.14                                                                                              
#genes                         2709 + 17 part                                                                                    
#predicted genes (unique)      2823                                                                                              
#predicted genes (>= 0 bp)     2851                                                                                              
#predicted genes (>= 300 bp)   2399                                                                                              
#predicted genes (>= 1500 bp)  304                                                                                               
#predicted genes (>= 3000 bp)  30                                                                                                
Largest alignment              337014                                                                                            
NA50                           145386                                                                                            
NGA50                          145386                                                                                            
NA75                           87600                                                                                             
NGA75                          88525                                                                                             
