All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.300percent.scf
#Contigs (>= 0 bp)             546                                                                           
#Contigs (>= 1000 bp)          546                                                                           
Total length (>= 0 bp)         2720994                                                                       
Total length (>= 1000 bp)      2720994                                                                       
#Contigs                       546                                                                           
Largest contig                 38119                                                                         
Total length                   2720994                                                                       
Reference length               2813862                                                                       
GC (%)                         32.60                                                                         
Reference GC (%)               32.81                                                                         
N50                            8417                                                                          
NG50                           8087                                                                          
N75                            3724                                                                          
NG75                           3393                                                                          
#misassemblies                 6                                                                             
#local misassemblies           6                                                                             
#unaligned contigs             0 + 4 part                                                                    
Unaligned contigs length       157                                                                           
Genome fraction (%)            94.308                                                                        
Duplication ratio              1.026                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        7.73                                                                          
#indels per 100 kbp            22.87                                                                         
#genes                         2230 + 383 part                                                               
#predicted genes (unique)      2995                                                                          
#predicted genes (>= 0 bp)     2995                                                                          
#predicted genes (>= 300 bp)   2416                                                                          
#predicted genes (>= 1500 bp)  230                                                                           
#predicted genes (>= 3000 bp)  19                                                                            
Largest alignment              38112                                                                         
NA50                           8416                                                                          
NGA50                          7973                                                                          
NA75                           3665                                                                          
NGA75                          3382                                                                          
