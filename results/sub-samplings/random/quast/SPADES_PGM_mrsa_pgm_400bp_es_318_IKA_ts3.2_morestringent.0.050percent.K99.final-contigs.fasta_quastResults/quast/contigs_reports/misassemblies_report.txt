All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.050percent.K99.final-contigs
#Mis_misassemblies               5                                                                                      
#Mis_relocations                 5                                                                                      
#Mis_translocations              0                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        4                                                                                      
Mis_Misassembled contigs length  278661                                                                                 
#Mis_local misassemblies         14                                                                                     
#mismatches                      391                                                                                    
#indels                          749                                                                                    
#Mis_short indels (<= 5 bp)      745                                                                                    
#Mis_long indels (> 5 bp)        4                                                                                      
Indels length                    836                                                                                    
