All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.100percent.result
#Contigs                                   3121                                                                       
#Contigs (>= 0 bp)                         3121                                                                       
#Contigs (>= 1000 bp)                      63                                                                         
Largest contig                             1832                                                                       
Total length                               1449986                                                                    
Total length (>= 0 bp)                     1449986                                                                    
Total length (>= 1000 bp)                  76466                                                                      
Reference length                           4411532                                                                    
N50                                        482                                                                        
NG50                                       None                                                                       
N75                                        379                                                                        
NG75                                       None                                                                       
L50                                        1099                                                                       
LG50                                       None                                                                       
L75                                        1944                                                                       
LG75                                       None                                                                       
#local misassemblies                       53                                                                         
#misassemblies                             1030                                                                       
#misassembled contigs                      905                                                                        
Misassembled contigs length                488582                                                                     
Misassemblies inter-contig overlap         8986                                                                       
#unaligned contigs                         15 + 495 part                                                              
Unaligned contigs length                   36616                                                                      
#ambiguously mapped contigs                7                                                                          
Extra bases in ambiguously mapped contigs  -3245                                                                      
#N's                                       1122                                                                       
#N's per 100 kbp                           77.38                                                                      
Genome fraction (%)                        29.473                                                                     
Duplication ratio                          1.091                                                                      
#genes                                     106 + 2415 part                                                            
#predicted genes (unique)                  3929                                                                       
#predicted genes (>= 0 bp)                 3932                                                                       
#predicted genes (>= 300 bp)               1286                                                                       
#predicted genes (>= 1500 bp)              0                                                                          
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                3115                                                                       
#indels                                    5030                                                                       
Indels length                              5253                                                                       
#mismatches per 100 kbp                    239.58                                                                     
#indels per 100 kbp                        386.87                                                                     
GC (%)                                     63.26                                                                      
Reference GC (%)                           65.61                                                                      
Average %IDY                               99.345                                                                     
Largest alignment                          1811                                                                       
NA50                                       392                                                                        
NGA50                                      None                                                                       
NA75                                       273                                                                        
NGA75                                      None                                                                       
LA50                                       1312                                                                       
LGA50                                      None                                                                       
LA75                                       2413                                                                       
LGA75                                      None                                                                       
#Mis_misassemblies                         1030                                                                       
#Mis_relocations                           361                                                                        
#Mis_translocations                        0                                                                          
#Mis_inversions                            669                                                                        
#Mis_misassembled contigs                  905                                                                        
Mis_Misassembled contigs length            488582                                                                     
#Mis_local misassemblies                   53                                                                         
#Mis_short indels (<= 5 bp)                5027                                                                       
#Mis_long indels (> 5 bp)                  3                                                                          
#Una_fully unaligned contigs               15                                                                         
Una_Fully unaligned length                 6422                                                                       
#Una_partially unaligned contigs           495                                                                        
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            8                                                                          
Una_Partially unaligned length             30194                                                                      
GAGE_Contigs #                             3121                                                                       
GAGE_Min contig                            201                                                                        
GAGE_Max contig                            1832                                                                       
GAGE_N50                                   0 COUNT: 0                                                                 
GAGE_Genome size                           4411532                                                                    
GAGE_Assembly size                         1449986                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               3062255(69.41%)                                                            
GAGE_Missing assembly bases                43228(2.98%)                                                               
GAGE_Missing assembly contigs              12(0.38%)                                                                  
GAGE_Duplicated reference bases            63998                                                                      
GAGE_Compressed reference bases            43774                                                                      
GAGE_Bad trim                              35369                                                                      
GAGE_Avg idy                               99.36                                                                      
GAGE_SNPs                                  2707                                                                       
GAGE_Indels < 5bp                          4970                                                                       
GAGE_Indels >= 5                           26                                                                         
GAGE_Inversions                            559                                                                        
GAGE_Relocation                            98                                                                         
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    3139                                                                       
GAGE_Corrected assembly size               1261799                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    1817                                                                       
GAGE_Corrected N50                         0 COUNT: 0                                                                 
