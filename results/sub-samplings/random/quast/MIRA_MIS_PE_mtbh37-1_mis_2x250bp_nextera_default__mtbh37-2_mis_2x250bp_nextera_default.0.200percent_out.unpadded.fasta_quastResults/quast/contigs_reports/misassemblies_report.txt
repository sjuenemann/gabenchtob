All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.200percent_out.unpadded
#Mis_misassemblies               32                                                                                                              
#Mis_relocations                 32                                                                                                              
#Mis_translocations              0                                                                                                               
#Mis_inversions                  0                                                                                                               
#Mis_misassembled contigs        27                                                                                                              
Mis_Misassembled contigs length  428752                                                                                                          
#Mis_local misassemblies         7                                                                                                               
#mismatches                      585                                                                                                             
#indels                          81                                                                                                              
#Mis_short indels (<= 5 bp)      76                                                                                                              
#Mis_long indels (> 5 bp)        5                                                                                                               
Indels length                    160                                                                                                             
