All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.55percent.result
#Mis_misassemblies               8                                                            
#Mis_relocations                 8                                                            
#Mis_translocations              0                                                            
#Mis_inversions                  0                                                            
#Mis_misassembled contigs        8                                                            
Mis_Misassembled contigs length  82490                                                        
#Mis_local misassemblies         6                                                            
#mismatches                      169                                                          
#indels                          376                                                          
#Mis_short indels (<= 5 bp)      375                                                          
#Mis_long indels (> 5 bp)        1                                                            
Indels length                    393                                                          
