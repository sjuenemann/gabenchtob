All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.9percent.contigs
#Contigs                                   330                                                              
#Contigs (>= 0 bp)                         1106                                                             
#Contigs (>= 1000 bp)                      150                                                              
Largest contig                             313765                                                           
Total length                               5354291                                                          
Total length (>= 0 bp)                     5462376                                                          
Total length (>= 1000 bp)                  5279520                                                          
Reference length                           5594470                                                          
N50                                        124147                                                           
NG50                                       105970                                                           
N75                                        50850                                                            
NG75                                       44833                                                            
L50                                        15                                                               
LG50                                       16                                                               
L75                                        32                                                               
LG75                                       36                                                               
#local misassemblies                       5                                                                
#misassemblies                             18                                                               
#misassembled contigs                      18                                                               
Misassembled contigs length                245449                                                           
Misassemblies inter-contig overlap         688                                                              
#unaligned contigs                         0 + 2 part                                                       
Unaligned contigs length                   67                                                               
#ambiguously mapped contigs                108                                                              
Extra bases in ambiguously mapped contigs  -85651                                                           
#N's                                       0                                                                
#N's per 100 kbp                           0.00                                                             
Genome fraction (%)                        94.016                                                           
Duplication ratio                          1.002                                                            
#genes                                     4955 + 147 part                                                  
#predicted genes (unique)                  5347                                                             
#predicted genes (>= 0 bp)                 5350                                                             
#predicted genes (>= 300 bp)               4462                                                             
#predicted genes (>= 1500 bp)              667                                                              
#predicted genes (>= 3000 bp)              65                                                               
#mismatches                                77                                                               
#indels                                    154                                                              
Indels length                              164                                                              
#mismatches per 100 kbp                    1.46                                                             
#indels per 100 kbp                        2.93                                                             
GC (%)                                     50.29                                                            
Reference GC (%)                           50.48                                                            
Average %IDY                               98.806                                                           
Largest alignment                          313765                                                           
NA50                                       105970                                                           
NGA50                                      103694                                                           
NA75                                       50850                                                            
NGA75                                      44833                                                            
LA50                                       16                                                               
LGA50                                      17                                                               
LA75                                       33                                                               
LGA75                                      37                                                               
#Mis_misassemblies                         18                                                               
#Mis_relocations                           17                                                               
#Mis_translocations                        1                                                                
#Mis_inversions                            0                                                                
#Mis_misassembled contigs                  18                                                               
Mis_Misassembled contigs length            245449                                                           
#Mis_local misassemblies                   5                                                                
#Mis_short indels (<= 5 bp)                154                                                              
#Mis_long indels (> 5 bp)                  0                                                                
#Una_fully unaligned contigs               0                                                                
Una_Fully unaligned length                 0                                                                
#Una_partially unaligned contigs           2                                                                
#Una_with misassembly                      0                                                                
#Una_both parts are significant            0                                                                
Una_Partially unaligned length             67                                                               
GAGE_Contigs #                             330                                                              
GAGE_Min contig                            200                                                              
GAGE_Max contig                            313765                                                           
GAGE_N50                                   105970 COUNT: 16                                                 
GAGE_Genome size                           5594470                                                          
GAGE_Assembly size                         5354291                                                          
GAGE_Chaff bases                           0                                                                
GAGE_Missing reference bases               41620(0.74%)                                                     
GAGE_Missing assembly bases                73(0.00%)                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                         
GAGE_Duplicated reference bases            11232                                                            
GAGE_Compressed reference bases            230567                                                           
GAGE_Bad trim                              73                                                               
GAGE_Avg idy                               99.99                                                            
GAGE_SNPs                                  67                                                               
GAGE_Indels < 5bp                          165                                                              
GAGE_Indels >= 5                           4                                                                
GAGE_Inversions                            0                                                                
GAGE_Relocation                            3                                                                
GAGE_Translocation                         0                                                                
GAGE_Corrected contig #                    291                                                              
GAGE_Corrected assembly size               5343885                                                          
GAGE_Min correct contig                    203                                                              
GAGE_Max correct contig                    313770                                                           
GAGE_Corrected N50                         99571 COUNT: 18                                                  
