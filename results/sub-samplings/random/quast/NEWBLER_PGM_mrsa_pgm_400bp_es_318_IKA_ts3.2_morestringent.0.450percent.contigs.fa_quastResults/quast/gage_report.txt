All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.450percent.contigs
GAGE_Contigs #                   97                                                                            
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  214702                                                                        
GAGE_N50                         77987 COUNT: 11                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2771361                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     11387(0.40%)                                                                  
GAGE_Missing assembly bases      222(0.01%)                                                                    
GAGE_Missing assembly contigs    1(1.03%)                                                                      
GAGE_Duplicated reference bases  1763                                                                          
GAGE_Compressed reference bases  36135                                                                         
GAGE_Bad trim                    9                                                                             
GAGE_Avg idy                     99.99                                                                         
GAGE_SNPs                        38                                                                            
GAGE_Indels < 5bp                220                                                                           
GAGE_Indels >= 5                 1                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  1                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          92                                                                            
GAGE_Corrected assembly size     2769734                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          214715                                                                        
GAGE_Corrected N50               77990 COUNT: 11                                                               
