All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.600percent.result
GAGE_Contigs #                   4635                                                                       
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  8601                                                                       
GAGE_N50                         968 COUNT: 1166                                                            
GAGE_Genome size                 4411532                                                                    
GAGE_Assembly size               3747122                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     834792(18.92%)                                                             
GAGE_Missing assembly bases      39511(1.05%)                                                               
GAGE_Missing assembly contigs    4(0.09%)                                                                   
GAGE_Duplicated reference bases  83365                                                                      
GAGE_Compressed reference bases  45857                                                                      
GAGE_Bad trim                    34616                                                                      
GAGE_Avg idy                     99.79                                                                      
GAGE_SNPs                        1332                                                                       
GAGE_Indels < 5bp                4570                                                                       
GAGE_Indels >= 5                 15                                                                         
GAGE_Inversions                  209                                                                        
GAGE_Relocation                  32                                                                         
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          4596                                                                       
GAGE_Corrected assembly size     3607758                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          8595                                                                       
GAGE_Corrected N50               862 COUNT: 1262                                                            
