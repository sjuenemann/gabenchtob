All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.4percent.K99.final-contigs
#Mis_misassemblies               4                                                                                    
#Mis_relocations                 4                                                                                    
#Mis_translocations              0                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        4                                                                                    
Mis_Misassembled contigs length  422602                                                                               
#Mis_local misassemblies         12                                                                                   
#mismatches                      298                                                                                  
#indels                          1612                                                                                 
#Mis_short indels (<= 5 bp)      1611                                                                                 
#Mis_long indels (> 5 bp)        1                                                                                    
Indels length                    1688                                                                                 
