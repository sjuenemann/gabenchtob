All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.9percent.contigs
#Contigs                                   425                                                                         
#Contigs (>= 0 bp)                         593                                                                         
#Contigs (>= 1000 bp)                      233                                                                         
Largest contig                             200254                                                                      
Total length                               5306344                                                                     
Total length (>= 0 bp)                     5329378                                                                     
Total length (>= 1000 bp)                  5226059                                                                     
Reference length                           5594470                                                                     
N50                                        55553                                                                       
NG50                                       50989                                                                       
N75                                        27502                                                                       
NG75                                       22387                                                                       
L50                                        27                                                                          
LG50                                       30                                                                          
L75                                        63                                                                          
LG75                                       72                                                                          
#local misassemblies                       6                                                                           
#misassemblies                             10                                                                          
#misassembled contigs                      10                                                                          
Misassembled contigs length                114451                                                                      
Misassemblies inter-contig overlap         1641                                                                        
#unaligned contigs                         0 + 1 part                                                                  
Unaligned contigs length                   25                                                                          
#ambiguously mapped contigs                125                                                                         
Extra bases in ambiguously mapped contigs  -84825                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        93.334                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     4846 + 222 part                                                             
#predicted genes (unique)                  5695                                                                        
#predicted genes (>= 0 bp)                 5695                                                                        
#predicted genes (>= 300 bp)               4645                                                                        
#predicted genes (>= 1500 bp)              564                                                                         
#predicted genes (>= 3000 bp)              49                                                                          
#mismatches                                114                                                                         
#indels                                    819                                                                         
Indels length                              858                                                                         
#mismatches per 100 kbp                    2.18                                                                        
#indels per 100 kbp                        15.69                                                                       
GC (%)                                     50.27                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.156                                                                      
Largest alignment                          200254                                                                      
NA50                                       54900                                                                       
NGA50                                      50535                                                                       
NA75                                       26068                                                                       
NGA75                                      22153                                                                       
LA50                                       27                                                                          
LGA50                                      30                                                                          
LA75                                       64                                                                          
LGA75                                      73                                                                          
#Mis_misassemblies                         10                                                                          
#Mis_relocations                           6                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            4                                                                           
#Mis_misassembled contigs                  10                                                                          
Mis_Misassembled contigs length            114451                                                                      
#Mis_local misassemblies                   6                                                                           
#Mis_short indels (<= 5 bp)                818                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           1                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             25                                                                          
GAGE_Contigs #                             425                                                                         
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            200254                                                                      
GAGE_N50                                   50989 COUNT: 30                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         5306344                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               100454(1.80%)                                                               
GAGE_Missing assembly bases                68(0.00%)                                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            1690                                                                        
GAGE_Compressed reference bases            194079                                                                      
GAGE_Bad trim                              68                                                                          
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  113                                                                         
GAGE_Indels < 5bp                          875                                                                         
GAGE_Indels >= 5                           7                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    427                                                                         
GAGE_Corrected assembly size               5307308                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    184486                                                                      
GAGE_Corrected N50                         47030 COUNT: 32                                                             
