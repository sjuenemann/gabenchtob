All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.300percent_out.unpadded
#Contigs (>= 0 bp)             1809                                                                            
#Contigs (>= 1000 bp)          131                                                                             
Total length (>= 0 bp)         3847971                                                                         
Total length (>= 1000 bp)      2972733                                                                         
#Contigs                       1754                                                                            
Largest contig                 505026                                                                          
Total length                   3839889                                                                         
Reference length               2813862                                                                         
GC (%)                         32.78                                                                           
Reference GC (%)               32.81                                                                           
N50                            85309                                                                           
NG50                           155626                                                                          
N75                            1769                                                                            
NG75                           78460                                                                           
#misassemblies                 53                                                                              
#local misassemblies           11                                                                              
#unaligned contigs             8 + 20 part                                                                     
Unaligned contigs length       5833                                                                            
Genome fraction (%)            99.971                                                                          
Duplication ratio              1.359                                                                           
#N's per 100 kbp               13.80                                                                           
#mismatches per 100 kbp        6.11                                                                            
#indels per 100 kbp            6.08                                                                            
#genes                         2707 + 19 part                                                                  
#predicted genes (unique)      5053                                                                            
#predicted genes (>= 0 bp)     5102                                                                            
#predicted genes (>= 300 bp)   2847                                                                            
#predicted genes (>= 1500 bp)  298                                                                             
#predicted genes (>= 3000 bp)  29                                                                              
Largest alignment              505008                                                                          
NA50                           85309                                                                           
NGA50                          153185                                                                          
NA75                           1777                                                                            
NGA75                          68878                                                                           
