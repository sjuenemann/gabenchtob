All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.900percent_out.unpadded
GAGE_Contigs #                   474                                                                                               
GAGE_Min contig                  202                                                                                               
GAGE_Max contig                  803414                                                                                            
GAGE_N50                         632933 COUNT: 2                                                                                   
GAGE_Genome size                 2813862                                                                                           
GAGE_Assembly size               3025603                                                                                           
GAGE_Chaff bases                 0                                                                                                 
GAGE_Missing reference bases     5(0.00%)                                                                                          
GAGE_Missing assembly bases      7786(0.26%)                                                                                       
GAGE_Missing assembly contigs    3(0.63%)                                                                                          
GAGE_Duplicated reference bases  220213                                                                                            
GAGE_Compressed reference bases  5336                                                                                              
GAGE_Bad trim                    1134                                                                                              
GAGE_Avg idy                     99.99                                                                                             
GAGE_SNPs                        35                                                                                                
GAGE_Indels < 5bp                9                                                                                                 
GAGE_Indels >= 5                 2                                                                                                 
GAGE_Inversions                  6                                                                                                 
GAGE_Relocation                  4                                                                                                 
GAGE_Translocation               0                                                                                                 
GAGE_Corrected contig #          45                                                                                                
GAGE_Corrected assembly size     2830193                                                                                           
GAGE_Min correct contig          202                                                                                               
GAGE_Max correct contig          632932                                                                                            
GAGE_Corrected N50               328564 COUNT: 3                                                                                   
