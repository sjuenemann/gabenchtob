All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.2percent.result
GAGE_Contigs #                   950                                                                    
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  99601                                                                  
GAGE_N50                         25459 COUNT: 69                                                        
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5338469                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     79868(1.43%)                                                           
GAGE_Missing assembly bases      1745(0.03%)                                                            
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  39656                                                                  
GAGE_Compressed reference bases  253450                                                                 
GAGE_Bad trim                    1745                                                                   
GAGE_Avg idy                     99.96                                                                  
GAGE_SNPs                        149                                                                    
GAGE_Indels < 5bp                1891                                                                   
GAGE_Indels >= 5                 6                                                                      
GAGE_Inversions                  1                                                                      
GAGE_Relocation                  3                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          807                                                                    
GAGE_Corrected assembly size     5299302                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          99634                                                                  
GAGE_Corrected N50               24279 COUNT: 71                                                        
