All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.65percent_132.final
#Contigs (>= 0 bp)             4700                                                                            
#Contigs (>= 1000 bp)          871                                                                             
Total length (>= 0 bp)         6075559                                                                         
Total length (>= 1000 bp)      5083081                                                                         
#Contigs                       3562                                                                            
Largest contig                 30666                                                                           
Total length                   5891972                                                                         
Reference length               5594470                                                                         
GC (%)                         50.52                                                                           
Reference GC (%)               50.48                                                                           
N50                            7528                                                                            
NG50                           8088                                                                            
N75                            3146                                                                            
NG75                           3834                                                                            
#misassemblies                 5                                                                               
#local misassemblies           1                                                                               
#unaligned contigs             1 + 1 part                                                                      
Unaligned contigs length       1440                                                                            
Genome fraction (%)            94.679                                                                          
Duplication ratio              1.075                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        0.98                                                                            
#indels per 100 kbp            32.36                                                                           
#genes                         4282 + 901 part                                                                 
#predicted genes (unique)      8754                                                                            
#predicted genes (>= 0 bp)     8845                                                                            
#predicted genes (>= 300 bp)   5021                                                                            
#predicted genes (>= 1500 bp)  459                                                                             
#predicted genes (>= 3000 bp)  28                                                                              
Largest alignment              30666                                                                           
NA50                           7528                                                                            
NGA50                          8088                                                                            
NA75                           3139                                                                            
NGA75                          3833                                                                            
