All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.075percent_out.unpadded
#Contigs                                   484                                                                  
#Contigs (>= 0 bp)                         493                                                                  
#Contigs (>= 1000 bp)                      264                                                                  
Largest contig                             217830                                                               
Total length                               5594940                                                              
Total length (>= 0 bp)                     5596471                                                              
Total length (>= 1000 bp)                  5481921                                                              
Reference length                           5594470                                                              
N50                                        64097                                                                
NG50                                       64097                                                                
N75                                        28056                                                                
NG75                                       28056                                                                
L50                                        26                                                                   
LG50                                       26                                                                   
L75                                        57                                                                   
LG75                                       57                                                                   
#local misassemblies                       22                                                                   
#misassemblies                             29                                                                   
#misassembled contigs                      26                                                                   
Misassembled contigs length                1037069                                                              
Misassemblies inter-contig overlap         38736                                                                
#unaligned contigs                         0 + 3 part                                                           
Unaligned contigs length                   328                                                                  
#ambiguously mapped contigs                69                                                                   
Extra bases in ambiguously mapped contigs  -75348                                                               
#N's                                       34                                                                   
#N's per 100 kbp                           0.61                                                                 
Genome fraction (%)                        97.280                                                               
Duplication ratio                          1.021                                                                
#genes                                     5131 + 217 part                                                      
#predicted genes (unique)                  6111                                                                 
#predicted genes (>= 0 bp)                 6126                                                                 
#predicted genes (>= 300 bp)               4910                                                                 
#predicted genes (>= 1500 bp)              575                                                                  
#predicted genes (>= 3000 bp)              53                                                                   
#mismatches                                1317                                                                 
#indels                                    2146                                                                 
Indels length                              2263                                                                 
#mismatches per 100 kbp                    24.20                                                                
#indels per 100 kbp                        39.43                                                                
GC (%)                                     50.41                                                                
Reference GC (%)                           50.48                                                                
Average %IDY                               98.533                                                               
Largest alignment                          217830                                                               
NA50                                       58346                                                                
NGA50                                      58346                                                                
NA75                                       26201                                                                
NGA75                                      26201                                                                
LA50                                       28                                                                   
LGA50                                      28                                                                   
LA75                                       62                                                                   
LGA75                                      62                                                                   
#Mis_misassemblies                         29                                                                   
#Mis_relocations                           28                                                                   
#Mis_translocations                        1                                                                    
#Mis_inversions                            0                                                                    
#Mis_misassembled contigs                  26                                                                   
Mis_Misassembled contigs length            1037069                                                              
#Mis_local misassemblies                   22                                                                   
#Mis_short indels (<= 5 bp)                2142                                                                 
#Mis_long indels (> 5 bp)                  4                                                                    
#Una_fully unaligned contigs               0                                                                    
Una_Fully unaligned length                 0                                                                    
#Una_partially unaligned contigs           3                                                                    
#Una_with misassembly                      0                                                                    
#Una_both parts are significant            0                                                                    
Una_Partially unaligned length             328                                                                  
GAGE_Contigs #                             484                                                                  
GAGE_Min contig                            201                                                                  
GAGE_Max contig                            217830                                                               
GAGE_N50                                   64097 COUNT: 26                                                      
GAGE_Genome size                           5594470                                                              
GAGE_Assembly size                         5594940                                                              
GAGE_Chaff bases                           0                                                                    
GAGE_Missing reference bases               2537(0.05%)                                                          
GAGE_Missing assembly bases                133(0.00%)                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                             
GAGE_Duplicated reference bases            117597                                                               
GAGE_Compressed reference bases            186420                                                               
GAGE_Bad trim                              120                                                                  
GAGE_Avg idy                               99.93                                                                
GAGE_SNPs                                  283                                                                  
GAGE_Indels < 5bp                          2063                                                                 
GAGE_Indels >= 5                           15                                                                   
GAGE_Inversions                            8                                                                    
GAGE_Relocation                            16                                                                   
GAGE_Translocation                         0                                                                    
GAGE_Corrected contig #                    346                                                                  
GAGE_Corrected assembly size               5524425                                                              
GAGE_Min correct contig                    246                                                                  
GAGE_Max correct contig                    217770                                                               
GAGE_Corrected N50                         47364 COUNT: 32                                                      
