All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.925percent_152.final
#Mis_misassemblies               5                                                                                
#Mis_relocations                 5                                                                                
#Mis_translocations              0                                                                                
#Mis_inversions                  0                                                                                
#Mis_misassembled contigs        5                                                                                
Mis_Misassembled contigs length  3915                                                                             
#Mis_local misassemblies         0                                                                                
#mismatches                      68                                                                               
#indels                          1639                                                                             
#Mis_short indels (<= 5 bp)      1638                                                                             
#Mis_long indels (> 5 bp)        1                                                                                
Indels length                    1724                                                                             
