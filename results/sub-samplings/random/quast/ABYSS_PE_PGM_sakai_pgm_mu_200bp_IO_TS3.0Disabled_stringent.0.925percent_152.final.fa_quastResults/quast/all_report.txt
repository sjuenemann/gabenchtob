All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.925percent_152.final
#Contigs                                   3827                                                                             
#Contigs (>= 0 bp)                         4558                                                                             
#Contigs (>= 1000 bp)                      937                                                                              
Largest contig                             39324                                                                            
Total length                               6012917                                                                          
Total length (>= 0 bp)                     6140753                                                                          
Total length (>= 1000 bp)                  5070894                                                                          
Reference length                           5594470                                                                          
N50                                        6312                                                                             
NG50                                       7017                                                                             
N75                                        2673                                                                             
NG75                                       3554                                                                             
L50                                        266                                                                              
LG50                                       235                                                                              
L75                                        613                                                                              
LG75                                       513                                                                              
#local misassemblies                       0                                                                                
#misassemblies                             5                                                                                
#misassembled contigs                      5                                                                                
Misassembled contigs length                3915                                                                             
Misassemblies inter-contig overlap         0                                                                                
#unaligned contigs                         0 + 1 part                                                                       
Unaligned contigs length                   47                                                                               
#ambiguously mapped contigs                704                                                                              
Extra bases in ambiguously mapped contigs  -224389                                                                          
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        94.800                                                                           
Duplication ratio                          1.091                                                                            
#genes                                     4247 + 962 part                                                                  
#predicted genes (unique)                  9195                                                                             
#predicted genes (>= 0 bp)                 9344                                                                             
#predicted genes (>= 300 bp)               5083                                                                             
#predicted genes (>= 1500 bp)              455                                                                              
#predicted genes (>= 3000 bp)              21                                                                               
#mismatches                                68                                                                               
#indels                                    1639                                                                             
Indels length                              1724                                                                             
#mismatches per 100 kbp                    1.28                                                                             
#indels per 100 kbp                        30.90                                                                            
GC (%)                                     50.59                                                                            
Reference GC (%)                           50.48                                                                            
Average %IDY                               99.203                                                                           
Largest alignment                          39324                                                                            
NA50                                       6312                                                                             
NGA50                                      7017                                                                             
NA75                                       2667                                                                             
NGA75                                      3552                                                                             
LA50                                       266                                                                              
LGA50                                      235                                                                              
LA75                                       614                                                                              
LGA75                                      513                                                                              
#Mis_misassemblies                         5                                                                                
#Mis_relocations                           5                                                                                
#Mis_translocations                        0                                                                                
#Mis_inversions                            0                                                                                
#Mis_misassembled contigs                  5                                                                                
Mis_Misassembled contigs length            3915                                                                             
#Mis_local misassemblies                   0                                                                                
#Mis_short indels (<= 5 bp)                1638                                                                             
#Mis_long indels (> 5 bp)                  1                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           1                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             47                                                                               
GAGE_Contigs #                             3827                                                                             
GAGE_Min contig                            200                                                                              
GAGE_Max contig                            39324                                                                            
GAGE_N50                                   7017 COUNT: 235                                                                  
GAGE_Genome size                           5594470                                                                          
GAGE_Assembly size                         6012917                                                                          
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               24826(0.44%)                                                                     
GAGE_Missing assembly bases                56(0.00%)                                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            466683                                                                           
GAGE_Compressed reference bases            249054                                                                           
GAGE_Bad trim                              56                                                                               
GAGE_Avg idy                               99.97                                                                            
GAGE_SNPs                                  48                                                                               
GAGE_Indels < 5bp                          1551                                                                             
GAGE_Indels >= 5                           1                                                                                
GAGE_Inversions                            0                                                                                
GAGE_Relocation                            2                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    2212                                                                             
GAGE_Corrected assembly size               5546116                                                                          
GAGE_Min correct contig                    200                                                                              
GAGE_Max correct contig                    39332                                                                            
GAGE_Corrected N50                         7019 COUNT: 235                                                                  
