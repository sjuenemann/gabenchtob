All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.700percent.scf
#Contigs                                   148                                                                      
#Contigs (>= 0 bp)                         148                                                                      
#Contigs (>= 1000 bp)                      148                                                                      
Largest contig                             157908                                                                   
Total length                               2772763                                                                  
Total length (>= 0 bp)                     2772763                                                                  
Total length (>= 1000 bp)                  2772763                                                                  
Reference length                           2813862                                                                  
N50                                        43923                                                                    
NG50                                       43454                                                                    
N75                                        22026                                                                    
NG75                                       21458                                                                    
L50                                        19                                                                       
LG50                                       20                                                                       
L75                                        43                                                                       
LG75                                       44                                                                       
#local misassemblies                       0                                                                        
#misassemblies                             2                                                                        
#misassembled contigs                      2                                                                        
Misassembled contigs length                62688                                                                    
Misassemblies inter-contig overlap         1                                                                        
#unaligned contigs                         0 + 1 part                                                               
Unaligned contigs length                   78                                                                       
#ambiguously mapped contigs                0                                                                        
Extra bases in ambiguously mapped contigs  0                                                                        
#N's                                       0                                                                        
#N's per 100 kbp                           0.00                                                                     
Genome fraction (%)                        97.274                                                                   
Duplication ratio                          1.013                                                                    
#genes                                     2561 + 101 part                                                          
#predicted genes (unique)                  2775                                                                     
#predicted genes (>= 0 bp)                 2777                                                                     
#predicted genes (>= 300 bp)               2354                                                                     
#predicted genes (>= 1500 bp)              272                                                                      
#predicted genes (>= 3000 bp)              24                                                                       
#mismatches                                42                                                                       
#indels                                    285                                                                      
Indels length                              290                                                                      
#mismatches per 100 kbp                    1.53                                                                     
#indels per 100 kbp                        10.41                                                                    
GC (%)                                     32.62                                                                    
Reference GC (%)                           32.81                                                                    
Average %IDY                               98.543                                                                   
Largest alignment                          157908                                                                   
NA50                                       43454                                                                    
NGA50                                      38604                                                                    
NA75                                       22021                                                                    
NGA75                                      21458                                                                    
LA50                                       19                                                                       
LGA50                                      20                                                                       
LA75                                       44                                                                       
LGA75                                      45                                                                       
#Mis_misassemblies                         2                                                                        
#Mis_relocations                           2                                                                        
#Mis_translocations                        0                                                                        
#Mis_inversions                            0                                                                        
#Mis_misassembled contigs                  2                                                                        
Mis_Misassembled contigs length            62688                                                                    
#Mis_local misassemblies                   0                                                                        
#Mis_short indels (<= 5 bp)                285                                                                      
#Mis_long indels (> 5 bp)                  0                                                                        
#Una_fully unaligned contigs               0                                                                        
Una_Fully unaligned length                 0                                                                        
#Una_partially unaligned contigs           1                                                                        
#Una_with misassembly                      0                                                                        
#Una_both parts are significant            0                                                                        
Una_Partially unaligned length             78                                                                       
GAGE_Contigs #                             148                                                                      
GAGE_Min contig                            1001                                                                     
GAGE_Max contig                            157908                                                                   
GAGE_N50                                   43454 COUNT: 20                                                          
GAGE_Genome size                           2813862                                                                  
GAGE_Assembly size                         2772763                                                                  
GAGE_Chaff bases                           0                                                                        
GAGE_Missing reference bases               64987(2.31%)                                                             
GAGE_Missing assembly bases                109(0.00%)                                                               
GAGE_Missing assembly contigs              0(0.00%)                                                                 
GAGE_Duplicated reference bases            99                                                                       
GAGE_Compressed reference bases            15798                                                                    
GAGE_Bad trim                              109                                                                      
GAGE_Avg idy                               99.99                                                                    
GAGE_SNPs                                  40                                                                       
GAGE_Indels < 5bp                          266                                                                      
GAGE_Indels >= 5                           0                                                                        
GAGE_Inversions                            0                                                                        
GAGE_Relocation                            1                                                                        
GAGE_Translocation                         0                                                                        
GAGE_Corrected contig #                    149                                                                      
GAGE_Corrected assembly size               2772758                                                                  
GAGE_Min correct contig                    1001                                                                     
GAGE_Max correct contig                    157922                                                                   
GAGE_Corrected N50                         38606 COUNT: 20                                                          
