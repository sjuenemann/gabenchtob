All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.55percent.scf
GAGE_Contigs #                   693                                                                     
GAGE_Min contig                  1001                                                                    
GAGE_Max contig                  64708                                                                   
GAGE_N50                         16761 COUNT: 102                                                        
GAGE_Genome size                 5594470                                                                 
GAGE_Assembly size               5272655                                                                 
GAGE_Chaff bases                 0                                                                       
GAGE_Missing reference bases     353782(6.32%)                                                           
GAGE_Missing assembly bases      1686(0.03%)                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                
GAGE_Duplicated reference bases  2147                                                                    
GAGE_Compressed reference bases  88197                                                                   
GAGE_Bad trim                    1669                                                                    
GAGE_Avg idy                     99.97                                                                   
GAGE_SNPs                        110                                                                     
GAGE_Indels < 5bp                855                                                                     
GAGE_Indels >= 5                 6                                                                       
GAGE_Inversions                  1                                                                       
GAGE_Relocation                  3                                                                       
GAGE_Translocation               0                                                                       
GAGE_Corrected contig #          702                                                                     
GAGE_Corrected assembly size     5271590                                                                 
GAGE_Min correct contig          470                                                                     
GAGE_Max correct contig          64718                                                                   
GAGE_Corrected N50               16522 COUNT: 103                                                        
