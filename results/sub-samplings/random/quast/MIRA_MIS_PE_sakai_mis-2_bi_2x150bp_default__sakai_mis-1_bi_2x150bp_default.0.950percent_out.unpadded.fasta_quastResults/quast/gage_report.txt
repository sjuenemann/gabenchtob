All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.950percent_out.unpadded
GAGE_Contigs #                   232                                                                                                 
GAGE_Min contig                  207                                                                                                 
GAGE_Max contig                  809295                                                                                              
GAGE_N50                         183073 COUNT: 10                                                                                    
GAGE_Genome size                 5594470                                                                                             
GAGE_Assembly size               5588073                                                                                             
GAGE_Chaff bases                 0                                                                                                   
GAGE_Missing reference bases     859(0.02%)                                                                                          
GAGE_Missing assembly bases      6801(0.12%)                                                                                         
GAGE_Missing assembly contigs    3(1.29%)                                                                                            
GAGE_Duplicated reference bases  125231                                                                                              
GAGE_Compressed reference bases  137255                                                                                              
GAGE_Bad trim                    415                                                                                                 
GAGE_Avg idy                     99.94                                                                                               
GAGE_SNPs                        628                                                                                                 
GAGE_Indels < 5bp                58                                                                                                  
GAGE_Indels >= 5                 3                                                                                                   
GAGE_Inversions                  23                                                                                                  
GAGE_Relocation                  18                                                                                                  
GAGE_Translocation               6                                                                                                   
GAGE_Corrected contig #          149                                                                                                 
GAGE_Corrected assembly size     5569694                                                                                             
GAGE_Min correct contig          271                                                                                                 
GAGE_Max correct contig          349510                                                                                              
GAGE_Corrected N50               142364 COUNT: 14                                                                                    
