All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent.contigs
#Mis_misassemblies               130                                                                           
#Mis_relocations                 10                                                                            
#Mis_translocations              0                                                                             
#Mis_inversions                  120                                                                           
#Mis_misassembled contigs        129                                                                           
Mis_Misassembled contigs length  93915                                                                         
#Mis_local misassemblies         7                                                                             
#mismatches                      1224                                                                          
#indels                          4063                                                                          
#Mis_short indels (<= 5 bp)      4054                                                                          
#Mis_long indels (> 5 bp)        9                                                                             
Indels length                    4356                                                                          
