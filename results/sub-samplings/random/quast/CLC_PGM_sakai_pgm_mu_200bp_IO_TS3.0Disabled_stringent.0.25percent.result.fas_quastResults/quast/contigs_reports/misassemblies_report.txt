All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.25percent.result
#Mis_misassemblies               17                                                                      
#Mis_relocations                 4                                                                       
#Mis_translocations              0                                                                       
#Mis_inversions                  13                                                                      
#Mis_misassembled contigs        17                                                                      
Mis_Misassembled contigs length  64936                                                                   
#Mis_local misassemblies         8                                                                       
#mismatches                      169                                                                     
#indels                          1730                                                                    
#Mis_short indels (<= 5 bp)      1730                                                                    
#Mis_long indels (> 5 bp)        0                                                                       
Indels length                    1806                                                                    
