All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.250percent.scf
#Contigs (>= 0 bp)             528                                                                           
#Contigs (>= 1000 bp)          528                                                                           
Total length (>= 0 bp)         2731373                                                                       
Total length (>= 1000 bp)      2731373                                                                       
#Contigs                       528                                                                           
Largest contig                 26971                                                                         
Total length                   2731373                                                                       
Reference length               2813862                                                                       
GC (%)                         32.59                                                                         
Reference GC (%)               32.81                                                                         
N50                            8535                                                                          
NG50                           8214                                                                          
N75                            4252                                                                          
NG75                           3981                                                                          
#misassemblies                 5                                                                             
#local misassemblies           2                                                                             
#unaligned contigs             0 + 6 part                                                                    
Unaligned contigs length       212                                                                           
Genome fraction (%)            94.779                                                                        
Duplication ratio              1.024                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        4.35                                                                          
#indels per 100 kbp            24.18                                                                         
#genes                         2261 + 364 part                                                               
#predicted genes (unique)      3003                                                                          
#predicted genes (>= 0 bp)     3003                                                                          
#predicted genes (>= 300 bp)   2428                                                                          
#predicted genes (>= 1500 bp)  237                                                                           
#predicted genes (>= 3000 bp)  21                                                                            
Largest alignment              26971                                                                         
NA50                           8459                                                                          
NGA50                          8214                                                                          
NA75                           4215                                                                          
NGA75                          3966                                                                          
