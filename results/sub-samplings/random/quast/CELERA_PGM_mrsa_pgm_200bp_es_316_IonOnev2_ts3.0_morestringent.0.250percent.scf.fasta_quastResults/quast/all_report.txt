All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.250percent.scf
#Contigs                                   528                                                                           
#Contigs (>= 0 bp)                         528                                                                           
#Contigs (>= 1000 bp)                      528                                                                           
Largest contig                             26971                                                                         
Total length                               2731373                                                                       
Total length (>= 0 bp)                     2731373                                                                       
Total length (>= 1000 bp)                  2731373                                                                       
Reference length                           2813862                                                                       
N50                                        8535                                                                          
NG50                                       8214                                                                          
N75                                        4252                                                                          
NG75                                       3981                                                                          
L50                                        99                                                                            
LG50                                       104                                                                           
L75                                        213                                                                           
LG75                                       228                                                                           
#local misassemblies                       2                                                                             
#misassemblies                             5                                                                             
#misassembled contigs                      5                                                                             
Misassembled contigs length                35003                                                                         
Misassemblies inter-contig overlap         439                                                                           
#unaligned contigs                         0 + 6 part                                                                    
Unaligned contigs length                   212                                                                           
#ambiguously mapped contigs                0                                                                             
Extra bases in ambiguously mapped contigs  0                                                                             
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        94.779                                                                        
Duplication ratio                          1.024                                                                         
#genes                                     2261 + 364 part                                                               
#predicted genes (unique)                  3003                                                                          
#predicted genes (>= 0 bp)                 3003                                                                          
#predicted genes (>= 300 bp)               2428                                                                          
#predicted genes (>= 1500 bp)              237                                                                           
#predicted genes (>= 3000 bp)              21                                                                            
#mismatches                                116                                                                           
#indels                                    645                                                                           
Indels length                              748                                                                           
#mismatches per 100 kbp                    4.35                                                                          
#indels per 100 kbp                        24.18                                                                         
GC (%)                                     32.59                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.232                                                                        
Largest alignment                          26971                                                                         
NA50                                       8459                                                                          
NGA50                                      8214                                                                          
NA75                                       4215                                                                          
NGA75                                      3966                                                                          
LA50                                       100                                                                           
LGA50                                      104                                                                           
LA75                                       214                                                                           
LGA75                                      229                                                                           
#Mis_misassemblies                         5                                                                             
#Mis_relocations                           4                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            1                                                                             
#Mis_misassembled contigs                  5                                                                             
Mis_Misassembled contigs length            35003                                                                         
#Mis_local misassemblies                   2                                                                             
#Mis_short indels (<= 5 bp)                643                                                                           
#Mis_long indels (> 5 bp)                  2                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           6                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             212                                                                           
GAGE_Contigs #                             528                                                                           
GAGE_Min contig                            1004                                                                          
GAGE_Max contig                            26971                                                                         
GAGE_N50                                   8214 COUNT: 104                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2731373                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               138418(4.92%)                                                                 
GAGE_Missing assembly bases                710(0.03%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            937                                                                           
GAGE_Compressed reference bases            12836                                                                         
GAGE_Bad trim                              710                                                                           
GAGE_Avg idy                               99.96                                                                         
GAGE_SNPs                                  59                                                                            
GAGE_Indels < 5bp                          504                                                                           
GAGE_Indels >= 5                           3                                                                             
GAGE_Inversions                            3                                                                             
GAGE_Relocation                            1                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    536                                                                           
GAGE_Corrected assembly size               2731505                                                                       
GAGE_Min correct contig                    213                                                                           
GAGE_Max correct contig                    26976                                                                         
GAGE_Corrected N50                         8194 COUNT: 105                                                               
