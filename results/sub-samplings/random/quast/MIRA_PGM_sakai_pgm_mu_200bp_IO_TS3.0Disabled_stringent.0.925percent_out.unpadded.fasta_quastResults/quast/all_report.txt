All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.925percent_out.unpadded
#Contigs                                   3358                                                                            
#Contigs (>= 0 bp)                         3420                                                                            
#Contigs (>= 1000 bp)                      219                                                                             
Largest contig                             389819                                                                          
Total length                               6610806                                                                         
Total length (>= 0 bp)                     6620975                                                                         
Total length (>= 1000 bp)                  5510197                                                                         
Reference length                           5594470                                                                         
N50                                        51919                                                                           
NG50                                       61737                                                                           
N75                                        11593                                                                           
NG75                                       33172                                                                           
L50                                        33                                                                              
LG50                                       24                                                                              
L75                                        89                                                                              
LG75                                       54                                                                              
#local misassemblies                       104                                                                             
#misassemblies                             733                                                                             
#misassembled contigs                      655                                                                             
Misassembled contigs length                2291168                                                                         
Misassemblies inter-contig overlap         60283                                                                           
#unaligned contigs                         67 + 1884 part                                                                  
Unaligned contigs length                   143274                                                                          
#ambiguously mapped contigs                81                                                                              
Extra bases in ambiguously mapped contigs  -49980                                                                          
#N's                                       5434                                                                            
#N's per 100 kbp                           82.20                                                                           
Genome fraction (%)                        98.339                                                                          
Duplication ratio                          1.177                                                                           
#genes                                     5212 + 198 part                                                                 
#predicted genes (unique)                  8604                                                                            
#predicted genes (>= 0 bp)                 8645                                                                            
#predicted genes (>= 300 bp)               5168                                                                            
#predicted genes (>= 1500 bp)              662                                                                             
#predicted genes (>= 3000 bp)              65                                                                              
#mismatches                                1733                                                                            
#indels                                    372                                                                             
Indels length                              441                                                                             
#mismatches per 100 kbp                    31.51                                                                           
#indels per 100 kbp                        6.76                                                                            
GC (%)                                     50.45                                                                           
Reference GC (%)                           50.48                                                                           
Average %IDY                               98.816                                                                          
Largest alignment                          279849                                                                          
NA50                                       48914                                                                           
NGA50                                      58131                                                                           
NA75                                       9887                                                                            
NGA75                                      29152                                                                           
LA50                                       38                                                                              
LGA50                                      28                                                                              
LA75                                       103                                                                             
LGA75                                      61                                                                              
#Mis_misassemblies                         733                                                                             
#Mis_relocations                           70                                                                              
#Mis_translocations                        0                                                                               
#Mis_inversions                            663                                                                             
#Mis_misassembled contigs                  655                                                                             
Mis_Misassembled contigs length            2291168                                                                         
#Mis_local misassemblies                   104                                                                             
#Mis_short indels (<= 5 bp)                370                                                                             
#Mis_long indels (> 5 bp)                  2                                                                               
#Una_fully unaligned contigs               67                                                                              
Una_Fully unaligned length                 27883                                                                           
#Una_partially unaligned contigs           1884                                                                            
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            9                                                                               
Una_Partially unaligned length             115391                                                                          
GAGE_Contigs #                             3358                                                                            
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            389819                                                                          
GAGE_N50                                   61737 COUNT: 24                                                                 
GAGE_Genome size                           5594470                                                                         
GAGE_Assembly size                         6610806                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               900(0.02%)                                                                      
GAGE_Missing assembly bases                139080(2.10%)                                                                   
GAGE_Missing assembly contigs              39(1.16%)                                                                       
GAGE_Duplicated reference bases            977617                                                                          
GAGE_Compressed reference bases            156506                                                                          
GAGE_Bad trim                              124909                                                                          
GAGE_Avg idy                               99.95                                                                           
GAGE_SNPs                                  453                                                                             
GAGE_Indels < 5bp                          256                                                                             
GAGE_Indels >= 5                           11                                                                              
GAGE_Inversions                            17                                                                              
GAGE_Relocation                            9                                                                               
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    325                                                                             
GAGE_Corrected assembly size               5559922                                                                         
GAGE_Min correct contig                    205                                                                             
GAGE_Max correct contig                    175662                                                                          
GAGE_Corrected N50                         54865 COUNT: 31                                                                 
