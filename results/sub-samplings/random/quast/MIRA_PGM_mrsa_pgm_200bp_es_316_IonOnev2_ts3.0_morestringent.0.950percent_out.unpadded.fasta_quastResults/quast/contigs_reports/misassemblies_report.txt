All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.950percent_out.unpadded
#Mis_misassemblies               2108                                                                                 
#Mis_relocations                 204                                                                                  
#Mis_translocations              6                                                                                    
#Mis_inversions                  1898                                                                                 
#Mis_misassembled contigs        1871                                                                                 
Mis_Misassembled contigs length  895395                                                                               
#Mis_local misassemblies         178                                                                                  
#mismatches                      240                                                                                  
#indels                          333                                                                                  
#Mis_short indels (<= 5 bp)      321                                                                                  
#Mis_long indels (> 5 bp)        12                                                                                   
Indels length                    614                                                                                  
