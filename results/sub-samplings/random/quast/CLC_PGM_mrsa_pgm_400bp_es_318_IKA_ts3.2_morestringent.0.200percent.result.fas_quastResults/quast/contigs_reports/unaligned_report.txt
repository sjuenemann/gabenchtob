All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.200percent.result
#Una_fully unaligned contigs      8                                                                        
Una_Fully unaligned length        2278                                                                     
#Una_partially unaligned contigs  11                                                                       
#Una_with misassembly             0                                                                        
#Una_both parts are significant   1                                                                        
Una_Partially unaligned length    850                                                                      
#N's                              719                                                                      
