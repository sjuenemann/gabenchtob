All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.250percent.contigs
GAGE_Contigs #                   376                                                                                
GAGE_Min contig                  204                                                                                
GAGE_Max contig                  117772                                                                             
GAGE_N50                         24824 COUNT: 53                                                                    
GAGE_Genome size                 4411532                                                                            
GAGE_Assembly size               4258828                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     100715(2.28%)                                                                      
GAGE_Missing assembly bases      1036(0.02%)                                                                        
GAGE_Missing assembly contigs    1(0.27%)                                                                           
GAGE_Duplicated reference bases  0                                                                                  
GAGE_Compressed reference bases  52978                                                                              
GAGE_Bad trim                    185                                                                                
GAGE_Avg idy                     99.96                                                                              
GAGE_SNPs                        110                                                                                
GAGE_Indels < 5bp                1349                                                                               
GAGE_Indels >= 5                 11                                                                                 
GAGE_Inversions                  1                                                                                  
GAGE_Relocation                  12                                                                                 
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          399                                                                                
GAGE_Corrected assembly size     4261370                                                                            
GAGE_Min correct contig          204                                                                                
GAGE_Max correct contig          117810                                                                             
GAGE_Corrected N50               22645 COUNT: 57                                                                    
