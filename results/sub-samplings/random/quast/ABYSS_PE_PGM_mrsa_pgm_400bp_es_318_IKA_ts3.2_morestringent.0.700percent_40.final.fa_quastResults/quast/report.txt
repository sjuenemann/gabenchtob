All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.700percent_40.final
#Contigs (>= 0 bp)             41099                                                                           
#Contigs (>= 1000 bp)          664                                                                             
Total length (>= 0 bp)         4841727                                                                         
Total length (>= 1000 bp)      1060909                                                                         
#Contigs                       3496                                                                            
Largest contig                 4781                                                                            
Total length                   2409686                                                                         
Reference length               2813862                                                                         
GC (%)                         32.61                                                                           
Reference GC (%)               32.81                                                                           
N50                            885                                                                             
NG50                           742                                                                             
N75                            520                                                                             
NG75                           364                                                                             
#misassemblies                 1                                                                               
#local misassemblies           0                                                                               
#unaligned contigs             0 + 0 part                                                                      
Unaligned contigs length       0                                                                               
Genome fraction (%)            85.512                                                                          
Duplication ratio              1.001                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        1.16                                                                            
#indels per 100 kbp            1.70                                                                            
#genes                         700 + 1821 part                                                                 
#predicted genes (unique)      4608                                                                            
#predicted genes (>= 0 bp)     4608                                                                            
#predicted genes (>= 300 bp)   2752                                                                            
#predicted genes (>= 1500 bp)  55                                                                              
#predicted genes (>= 3000 bp)  2                                                                               
Largest alignment              4781                                                                            
NA50                           885                                                                             
NGA50                          742                                                                             
NA75                           519                                                                             
NGA75                          364                                                                             
