All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.075percent.result
GAGE_Contigs #                   835                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  34452                                                                         
GAGE_N50                         8019 COUNT: 97                                                                
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2737882                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     76345(2.71%)                                                                  
GAGE_Missing assembly bases      3020(0.11%)                                                                   
GAGE_Missing assembly contigs    1(0.12%)                                                                      
GAGE_Duplicated reference bases  15845                                                                         
GAGE_Compressed reference bases  38992                                                                         
GAGE_Bad trim                    2343                                                                          
GAGE_Avg idy                     99.94                                                                         
GAGE_SNPs                        114                                                                           
GAGE_Indels < 5bp                1072                                                                          
GAGE_Indels >= 5                 2                                                                             
GAGE_Inversions                  2                                                                             
GAGE_Relocation                  2                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          780                                                                           
GAGE_Corrected assembly size     2719835                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          34451                                                                         
GAGE_Corrected N50               7958 COUNT: 98                                                                
