All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.075percent.result
#Mis_misassemblies               16                                                                            
#Mis_relocations                 5                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  11                                                                            
#Mis_misassembled contigs        16                                                                            
Mis_Misassembled contigs length  48215                                                                         
#Mis_local misassemblies         2                                                                             
#mismatches                      173                                                                           
#indels                          1141                                                                          
#Mis_short indels (<= 5 bp)      1141                                                                          
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    1215                                                                          
