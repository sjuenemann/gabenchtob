All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.650percent.contigs
GAGE_Contigs #                   213                                                                                
GAGE_Min contig                  203                                                                                
GAGE_Max contig                  182923                                                                             
GAGE_N50                         58056 COUNT: 22                                                                    
GAGE_Genome size                 4411532                                                                            
GAGE_Assembly size               4289355                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     72853(1.65%)                                                                       
GAGE_Missing assembly bases      875(0.02%)                                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  187                                                                                
GAGE_Compressed reference bases  49416                                                                              
GAGE_Bad trim                    875                                                                                
GAGE_Avg idy                     99.98                                                                              
GAGE_SNPs                        102                                                                                
GAGE_Indels < 5bp                873                                                                                
GAGE_Indels >= 5                 11                                                                                 
GAGE_Inversions                  0                                                                                  
GAGE_Relocation                  9                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          232                                                                                
GAGE_Corrected assembly size     4291274                                                                            
GAGE_Min correct contig          204                                                                                
GAGE_Max correct contig          182947                                                                             
GAGE_Corrected N50               47414 COUNT: 28                                                                    
