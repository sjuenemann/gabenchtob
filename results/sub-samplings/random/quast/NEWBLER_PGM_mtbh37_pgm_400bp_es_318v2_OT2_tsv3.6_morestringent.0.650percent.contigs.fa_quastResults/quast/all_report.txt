All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.650percent.contigs
#Contigs                                   213                                                                                
#Contigs (>= 0 bp)                         244                                                                                
#Contigs (>= 1000 bp)                      151                                                                                
Largest contig                             182923                                                                             
Total length                               4289355                                                                            
Total length (>= 0 bp)                     4293849                                                                            
Total length (>= 1000 bp)                  4258268                                                                            
Reference length                           4411532                                                                            
N50                                        61074                                                                              
NG50                                       58056                                                                              
N75                                        31361                                                                              
NG75                                       27316                                                                              
L50                                        21                                                                                 
LG50                                       22                                                                                 
L75                                        47                                                                                 
LG75                                       50                                                                                 
#local misassemblies                       16                                                                                 
#misassemblies                             0                                                                                  
#misassembled contigs                      0                                                                                  
Misassembled contigs length                0                                                                                  
Misassemblies inter-contig overlap         1969                                                                               
#unaligned contigs                         0 + 2 part                                                                         
Unaligned contigs length                   1613                                                                               
#ambiguously mapped contigs                28                                                                                 
Extra bases in ambiguously mapped contigs  -19937                                                                             
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        96.783                                                                             
Duplication ratio                          1.000                                                                              
#genes                                     3929 + 120 part                                                                    
#predicted genes (unique)                  4362                                                                               
#predicted genes (>= 0 bp)                 4362                                                                               
#predicted genes (>= 300 bp)               3693                                                                               
#predicted genes (>= 1500 bp)              493                                                                                
#predicted genes (>= 3000 bp)              61                                                                                 
#mismatches                                136                                                                                
#indels                                    835                                                                                
Indels length                              947                                                                                
#mismatches per 100 kbp                    3.19                                                                               
#indels per 100 kbp                        19.56                                                                              
GC (%)                                     65.38                                                                              
Reference GC (%)                           65.61                                                                              
Average %IDY                               99.601                                                                             
Largest alignment                          182923                                                                             
NA50                                       61071                                                                              
NGA50                                      58056                                                                              
NA75                                       31361                                                                              
NGA75                                      27316                                                                              
LA50                                       21                                                                                 
LGA50                                      22                                                                                 
LA75                                       47                                                                                 
LGA75                                      50                                                                                 
#Mis_misassemblies                         0                                                                                  
#Mis_relocations                           0                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  0                                                                                  
Mis_Misassembled contigs length            0                                                                                  
#Mis_local misassemblies                   16                                                                                 
#Mis_short indels (<= 5 bp)                832                                                                                
#Mis_long indels (> 5 bp)                  3                                                                                  
#Una_fully unaligned contigs               0                                                                                  
Una_Fully unaligned length                 0                                                                                  
#Una_partially unaligned contigs           2                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            1                                                                                  
Una_Partially unaligned length             1613                                                                               
GAGE_Contigs #                             213                                                                                
GAGE_Min contig                            203                                                                                
GAGE_Max contig                            182923                                                                             
GAGE_N50                                   58056 COUNT: 22                                                                    
GAGE_Genome size                           4411532                                                                            
GAGE_Assembly size                         4289355                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               72853(1.65%)                                                                       
GAGE_Missing assembly bases                875(0.02%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            187                                                                                
GAGE_Compressed reference bases            49416                                                                              
GAGE_Bad trim                              875                                                                                
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  102                                                                                
GAGE_Indels < 5bp                          873                                                                                
GAGE_Indels >= 5                           11                                                                                 
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            9                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    232                                                                                
GAGE_Corrected assembly size               4291274                                                                            
GAGE_Min correct contig                    204                                                                                
GAGE_Max correct contig                    182947                                                                             
GAGE_Corrected N50                         47414 COUNT: 28                                                                    
