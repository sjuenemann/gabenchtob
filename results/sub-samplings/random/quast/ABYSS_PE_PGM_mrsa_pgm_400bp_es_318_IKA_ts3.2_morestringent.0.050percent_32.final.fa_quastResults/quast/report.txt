All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.050percent_32.final
#Contigs (>= 0 bp)             5260                                                                            
#Contigs (>= 1000 bp)          495                                                                             
Total length (>= 0 bp)         2942846                                                                         
Total length (>= 1000 bp)      2584877                                                                         
#Contigs                       766                                                                             
Largest contig                 46227                                                                           
Total length                   2714104                                                                         
Reference length               2813862                                                                         
GC (%)                         32.59                                                                           
Reference GC (%)               32.81                                                                           
N50                            7511                                                                            
NG50                           7059                                                                            
N75                            3897                                                                            
NG75                           3519                                                                            
#misassemblies                 1                                                                               
#local misassemblies           2                                                                               
#unaligned contigs             0 + 0 part                                                                      
Unaligned contigs length       0                                                                               
Genome fraction (%)            96.107                                                                          
Duplication ratio              1.002                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        1.81                                                                            
#indels per 100 kbp            9.76                                                                            
#genes                         2222 + 392 part                                                                 
#predicted genes (unique)      3013                                                                            
#predicted genes (>= 0 bp)     3013                                                                            
#predicted genes (>= 300 bp)   2378                                                                            
#predicted genes (>= 1500 bp)  243                                                                             
#predicted genes (>= 3000 bp)  19                                                                              
Largest alignment              46227                                                                           
NA50                           7511                                                                            
NGA50                          7059                                                                            
NA75                           3897                                                                            
NGA75                          3519                                                                            
