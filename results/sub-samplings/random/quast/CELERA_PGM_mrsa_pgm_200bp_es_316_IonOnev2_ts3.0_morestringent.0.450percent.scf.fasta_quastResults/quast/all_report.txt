All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.450percent.scf
#Contigs                                   531                                                                           
#Contigs (>= 0 bp)                         531                                                                           
#Contigs (>= 1000 bp)                      531                                                                           
Largest contig                             29589                                                                         
Total length                               2676381                                                                       
Total length (>= 0 bp)                     2676381                                                                       
Total length (>= 1000 bp)                  2676381                                                                       
Reference length                           2813862                                                                       
N50                                        9394                                                                          
NG50                                       8907                                                                          
N75                                        4135                                                                          
NG75                                       3517                                                                          
L50                                        98                                                                            
LG50                                       105                                                                           
L75                                        206                                                                           
LG75                                       233                                                                           
#local misassemblies                       4                                                                             
#misassemblies                             3                                                                             
#misassembled contigs                      3                                                                             
Misassembled contigs length                21395                                                                         
Misassemblies inter-contig overlap         1166                                                                          
#unaligned contigs                         0 + 7 part                                                                    
Unaligned contigs length                   317                                                                           
#ambiguously mapped contigs                1                                                                             
Extra bases in ambiguously mapped contigs  -1159                                                                         
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        93.222                                                                        
Duplication ratio                          1.020                                                                         
#genes                                     2192 + 435 part                                                               
#predicted genes (unique)                  2926                                                                          
#predicted genes (>= 0 bp)                 2927                                                                          
#predicted genes (>= 300 bp)               2383                                                                          
#predicted genes (>= 1500 bp)              228                                                                           
#predicted genes (>= 3000 bp)              21                                                                            
#mismatches                                201                                                                           
#indels                                    582                                                                           
Indels length                              687                                                                           
#mismatches per 100 kbp                    7.66                                                                          
#indels per 100 kbp                        22.19                                                                         
GC (%)                                     32.63                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.438                                                                        
Largest alignment                          29589                                                                         
NA50                                       9394                                                                          
NGA50                                      8850                                                                          
NA75                                       4117                                                                          
NGA75                                      3511                                                                          
LA50                                       98                                                                            
LGA50                                      106                                                                           
LA75                                       207                                                                           
LGA75                                      234                                                                           
#Mis_misassemblies                         3                                                                             
#Mis_relocations                           1                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            2                                                                             
#Mis_misassembled contigs                  3                                                                             
Mis_Misassembled contigs length            21395                                                                         
#Mis_local misassemblies                   4                                                                             
#Mis_short indels (<= 5 bp)                580                                                                           
#Mis_long indels (> 5 bp)                  2                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           7                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             317                                                                           
GAGE_Contigs #                             531                                                                           
GAGE_Min contig                            1002                                                                          
GAGE_Max contig                            29589                                                                         
GAGE_N50                                   8907 COUNT: 105                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2676381                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               171756(6.10%)                                                                 
GAGE_Missing assembly bases                952(0.04%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            383                                                                           
GAGE_Compressed reference bases            21882                                                                         
GAGE_Bad trim                              952                                                                           
GAGE_Avg idy                               99.97                                                                         
GAGE_SNPs                                  99                                                                            
GAGE_Indels < 5bp                          480                                                                           
GAGE_Indels >= 5                           5                                                                             
GAGE_Inversions                            1                                                                             
GAGE_Relocation                            2                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    540                                                                           
GAGE_Corrected assembly size               2676487                                                                       
GAGE_Min correct contig                    251                                                                           
GAGE_Max correct contig                    29592                                                                         
GAGE_Corrected N50                         8628 COUNT: 106                                                               
