All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.350percent.result
#Contigs (>= 0 bp)             1039                                                                          
#Contigs (>= 1000 bp)          439                                                                           
Total length (>= 0 bp)         4273326                                                                       
Total length (>= 1000 bp)      4073114                                                                       
#Contigs                       1039                                                                          
Largest contig                 82462                                                                         
Total length                   4273326                                                                       
Reference length               4411532                                                                       
GC (%)                         65.31                                                                         
Reference GC (%)               65.61                                                                         
N50                            14477                                                                         
NG50                           13926                                                                         
N75                            7376                                                                          
NG75                           6797                                                                          
#misassemblies                 12                                                                            
#local misassemblies           3                                                                             
#unaligned contigs             4 + 6 part                                                                    
Unaligned contigs length       2637                                                                          
Genome fraction (%)            94.860                                                                        
Duplication ratio              1.019                                                                         
#N's per 100 kbp               8.03                                                                          
#mismatches per 100 kbp        4.92                                                                          
#indels per 100 kbp            41.99                                                                         
#genes                         3554 + 468 part                                                               
#predicted genes (unique)      5160                                                                          
#predicted genes (>= 0 bp)     5161                                                                          
#predicted genes (>= 300 bp)   3869                                                                          
#predicted genes (>= 1500 bp)  404                                                                           
#predicted genes (>= 3000 bp)  32                                                                            
Largest alignment              82462                                                                         
NA50                           14447                                                                         
NGA50                          13520                                                                         
NA75                           7282                                                                          
NGA75                          6659                                                                          
