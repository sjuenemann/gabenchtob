All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent_190.final
#Contigs                                   3                                                                                
#Contigs (>= 0 bp)                         3                                                                                
#Contigs (>= 1000 bp)                      1                                                                                
Largest contig                             1004                                                                             
Total length                               1915                                                                             
Total length (>= 0 bp)                     1915                                                                             
Total length (>= 1000 bp)                  1004                                                                             
Reference length                           5594470                                                                          
N50                                        1004                                                                             
NG50                                       None                                                                             
N75                                        500                                                                              
NG75                                       None                                                                             
L50                                        1                                                                                
LG50                                       None                                                                             
L75                                        2                                                                                
LG75                                       None                                                                             
#local misassemblies                       0                                                                                
#misassemblies                             0                                                                                
#misassembled contigs                      0                                                                                
Misassembled contigs length                0                                                                                
Misassemblies inter-contig overlap         0                                                                                
#unaligned contigs                         0 + 0 part                                                                       
Unaligned contigs length                   0                                                                                
#ambiguously mapped contigs                2                                                                                
Extra bases in ambiguously mapped contigs  -911                                                                             
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        0.018                                                                            
Duplication ratio                          1.000                                                                            
#genes                                     1 + 1 part                                                                       
#predicted genes (unique)                  4                                                                                
#predicted genes (>= 0 bp)                 4                                                                                
#predicted genes (>= 300 bp)               4                                                                                
#predicted genes (>= 1500 bp)              0                                                                                
#predicted genes (>= 3000 bp)              0                                                                                
#mismatches                                0                                                                                
#indels                                    0                                                                                
Indels length                              0                                                                                
#mismatches per 100 kbp                    0.00                                                                             
#indels per 100 kbp                        0.00                                                                             
GC (%)                                     53.84                                                                            
Reference GC (%)                           50.48                                                                            
Average %IDY                               99.165                                                                           
Largest alignment                          1004                                                                             
NA50                                       1004                                                                             
NGA50                                      None                                                                             
NA75                                       None                                                                             
NGA75                                      None                                                                             
LA50                                       1                                                                                
LGA50                                      None                                                                             
LA75                                       None                                                                             
LGA75                                      None                                                                             
#Mis_misassemblies                         0                                                                                
#Mis_relocations                           0                                                                                
#Mis_translocations                        0                                                                                
#Mis_inversions                            0                                                                                
#Mis_misassembled contigs                  0                                                                                
Mis_Misassembled contigs length            0                                                                                
#Mis_local misassemblies                   0                                                                                
#Mis_short indels (<= 5 bp)                0                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           0                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             0                                                                                
GAGE_Contigs #                             3                                                                                
GAGE_Min contig                            411                                                                              
GAGE_Max contig                            1004                                                                             
GAGE_N50                                   0 COUNT: 0                                                                       
GAGE_Genome size                           5594470                                                                          
GAGE_Assembly size                         1915                                                                             
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               5566178(99.49%)                                                                  
GAGE_Missing assembly bases                0(0.00%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            0                                                                                
GAGE_Compressed reference bases            28263                                                                            
GAGE_Bad trim                              0                                                                                
GAGE_Avg idy                               99.95                                                                            
GAGE_SNPs                                  0                                                                                
GAGE_Indels < 5bp                          0                                                                                
GAGE_Indels >= 5                           0                                                                                
GAGE_Inversions                            0                                                                                
GAGE_Relocation                            0                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    3                                                                                
GAGE_Corrected assembly size               1914                                                                             
GAGE_Min correct contig                    411                                                                              
GAGE_Max correct contig                    1004                                                                             
GAGE_Corrected N50                         0 COUNT: 0                                                                       
