All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent_out.unpadded
#Contigs                                   371                                                                  
#Contigs (>= 0 bp)                         386                                                                  
#Contigs (>= 1000 bp)                      184                                                                  
Largest contig                             411241                                                               
Total length                               5643845                                                              
Total length (>= 0 bp)                     5646057                                                              
Total length (>= 1000 bp)                  5544313                                                              
Reference length                           5594470                                                              
N50                                        134887                                                               
NG50                                       134887                                                               
N75                                        67192                                                                
NG75                                       69848                                                                
L50                                        12                                                                   
LG50                                       12                                                                   
L75                                        28                                                                   
LG75                                       27                                                                   
#local misassemblies                       19                                                                   
#misassemblies                             40                                                                   
#misassembled contigs                      30                                                                   
Misassembled contigs length                2467977                                                              
Misassemblies inter-contig overlap         45684                                                                
#unaligned contigs                         1 + 5 part                                                           
Unaligned contigs length                   903                                                                  
#ambiguously mapped contigs                73                                                                   
Extra bases in ambiguously mapped contigs  -83022                                                               
#N's                                       22                                                                   
#N's per 100 kbp                           0.39                                                                 
Genome fraction (%)                        97.827                                                               
Duplication ratio                          1.024                                                                
#genes                                     5262 + 112 part                                                      
#predicted genes (unique)                  5725                                                                 
#predicted genes (>= 0 bp)                 5782                                                                 
#predicted genes (>= 300 bp)               4764                                                                 
#predicted genes (>= 1500 bp)              660                                                                  
#predicted genes (>= 3000 bp)              63                                                                   
#mismatches                                1421                                                                 
#indels                                    596                                                                  
Indels length                              631                                                                  
#mismatches per 100 kbp                    25.96                                                                
#indels per 100 kbp                        10.89                                                                
GC (%)                                     50.42                                                                
Reference GC (%)                           50.48                                                                
Average %IDY                               98.443                                                               
Largest alignment                          379595                                                               
NA50                                       125741                                                               
NGA50                                      125741                                                               
NA75                                       59658                                                                
NGA75                                      67085                                                                
LA50                                       14                                                                   
LGA50                                      14                                                                   
LA75                                       31                                                                   
LGA75                                      30                                                                   
#Mis_misassemblies                         40                                                                   
#Mis_relocations                           38                                                                   
#Mis_translocations                        2                                                                    
#Mis_inversions                            0                                                                    
#Mis_misassembled contigs                  30                                                                   
Mis_Misassembled contigs length            2467977                                                              
#Mis_local misassemblies                   19                                                                   
#Mis_short indels (<= 5 bp)                594                                                                  
#Mis_long indels (> 5 bp)                  2                                                                    
#Una_fully unaligned contigs               1                                                                    
Una_Fully unaligned length                 642                                                                  
#Una_partially unaligned contigs           5                                                                    
#Una_with misassembly                      0                                                                    
#Una_both parts are significant            0                                                                    
Una_Partially unaligned length             261                                                                  
GAGE_Contigs #                             371                                                                  
GAGE_Min contig                            204                                                                  
GAGE_Max contig                            411241                                                               
GAGE_N50                                   134887 COUNT: 12                                                     
GAGE_Genome size                           5594470                                                              
GAGE_Assembly size                         5643845                                                              
GAGE_Chaff bases                           0                                                                    
GAGE_Missing reference bases               451(0.01%)                                                           
GAGE_Missing assembly bases                264(0.00%)                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                             
GAGE_Duplicated reference bases            150238                                                               
GAGE_Compressed reference bases            160003                                                               
GAGE_Bad trim                              255                                                                  
GAGE_Avg idy                               99.97                                                                
GAGE_SNPs                                  272                                                                  
GAGE_Indels < 5bp                          545                                                                  
GAGE_Indels >= 5                           10                                                                   
GAGE_Inversions                            10                                                                   
GAGE_Relocation                            7                                                                    
GAGE_Translocation                         0                                                                    
GAGE_Corrected contig #                    231                                                                  
GAGE_Corrected assembly size               5542157                                                              
GAGE_Min correct contig                    204                                                                  
GAGE_Max correct contig                    315641                                                               
GAGE_Corrected N50                         101170 COUNT: 18                                                     
