All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent_out.unpadded
GAGE_Contigs #                   371                                                                  
GAGE_Min contig                  204                                                                  
GAGE_Max contig                  411241                                                               
GAGE_N50                         134887 COUNT: 12                                                     
GAGE_Genome size                 5594470                                                              
GAGE_Assembly size               5643845                                                              
GAGE_Chaff bases                 0                                                                    
GAGE_Missing reference bases     451(0.01%)                                                           
GAGE_Missing assembly bases      264(0.00%)                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                             
GAGE_Duplicated reference bases  150238                                                               
GAGE_Compressed reference bases  160003                                                               
GAGE_Bad trim                    255                                                                  
GAGE_Avg idy                     99.97                                                                
GAGE_SNPs                        272                                                                  
GAGE_Indels < 5bp                545                                                                  
GAGE_Indels >= 5                 10                                                                   
GAGE_Inversions                  10                                                                   
GAGE_Relocation                  7                                                                    
GAGE_Translocation               0                                                                    
GAGE_Corrected contig #          231                                                                  
GAGE_Corrected assembly size     5542157                                                              
GAGE_Min correct contig          204                                                                  
GAGE_Max correct contig          315641                                                               
GAGE_Corrected N50               101170 COUNT: 18                                                     
