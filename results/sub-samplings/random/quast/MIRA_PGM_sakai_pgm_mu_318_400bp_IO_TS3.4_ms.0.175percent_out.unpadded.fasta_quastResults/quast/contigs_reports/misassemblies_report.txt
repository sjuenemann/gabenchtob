All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.175percent_out.unpadded
#Mis_misassemblies               40                                                                   
#Mis_relocations                 38                                                                   
#Mis_translocations              2                                                                    
#Mis_inversions                  0                                                                    
#Mis_misassembled contigs        30                                                                   
Mis_Misassembled contigs length  2467977                                                              
#Mis_local misassemblies         19                                                                   
#mismatches                      1421                                                                 
#indels                          596                                                                  
#Mis_short indels (<= 5 bp)      594                                                                  
#Mis_long indels (> 5 bp)        2                                                                    
Indels length                    631                                                                  
