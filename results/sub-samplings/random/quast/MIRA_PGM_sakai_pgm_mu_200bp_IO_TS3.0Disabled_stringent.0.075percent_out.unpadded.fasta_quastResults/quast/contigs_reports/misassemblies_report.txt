All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.075percent_out.unpadded
#Mis_misassemblies               91                                                                              
#Mis_relocations                 46                                                                              
#Mis_translocations              0                                                                               
#Mis_inversions                  45                                                                              
#Mis_misassembled contigs        79                                                                              
Mis_Misassembled contigs length  468060                                                                          
#Mis_local misassemblies         21                                                                              
#mismatches                      2353                                                                            
#indels                          2347                                                                            
#Mis_short indels (<= 5 bp)      2338                                                                            
#Mis_long indels (> 5 bp)        9                                                                               
Indels length                    2555                                                                            
