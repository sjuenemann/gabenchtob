All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.925percent_out.unpadded
#Contigs (>= 0 bp)             513                                                                                               
#Contigs (>= 1000 bp)          34                                                                                                
Total length (>= 0 bp)         3044392                                                                                           
Total length (>= 1000 bp)      2840552                                                                                           
#Contigs                       505                                                                                               
Largest contig                 475768                                                                                            
Total length                   3043346                                                                                           
Reference length               2813862                                                                                           
GC (%)                         32.87                                                                                             
Reference GC (%)               32.81                                                                                             
N50                            363308                                                                                            
NG50                           363308                                                                                            
N75                            118522                                                                                            
NG75                           126740                                                                                            
#misassemblies                 43                                                                                                
#local misassemblies           7                                                                                                 
#unaligned contigs             5 + 17 part                                                                                       
Unaligned contigs length       9456                                                                                              
Genome fraction (%)            99.950                                                                                            
Duplication ratio              1.081                                                                                             
#N's per 100 kbp               6.47                                                                                              
#mismatches per 100 kbp        4.80                                                                                              
#indels per 100 kbp            3.16                                                                                              
#genes                         2712 + 14 part                                                                                    
#predicted genes (unique)      3092                                                                                              
#predicted genes (>= 0 bp)     3117                                                                                              
#predicted genes (>= 300 bp)   2561                                                                                              
#predicted genes (>= 1500 bp)  306                                                                                               
#predicted genes (>= 3000 bp)  30                                                                                                
Largest alignment              471100                                                                                            
NA50                           337014                                                                                            
NGA50                          337014                                                                                            
NA75                           94061                                                                                             
NGA75                          107045                                                                                            
