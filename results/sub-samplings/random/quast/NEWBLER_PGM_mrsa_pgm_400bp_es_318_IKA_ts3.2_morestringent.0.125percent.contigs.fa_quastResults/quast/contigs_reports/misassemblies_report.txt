All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.125percent.contigs
#Mis_misassemblies               2                                                                             
#Mis_relocations                 2                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        2                                                                             
Mis_Misassembled contigs length  159626                                                                        
#Mis_local misassemblies         9                                                                             
#mismatches                      63                                                                            
#indels                          179                                                                           
#Mis_short indels (<= 5 bp)      178                                                                           
#Mis_long indels (> 5 bp)        1                                                                             
Indels length                    191                                                                           
