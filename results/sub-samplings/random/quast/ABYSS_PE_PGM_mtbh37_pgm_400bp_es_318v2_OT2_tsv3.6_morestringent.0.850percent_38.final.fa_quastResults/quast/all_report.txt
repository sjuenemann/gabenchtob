All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.850percent_38.final
#Contigs                                   1969                                                                                 
#Contigs (>= 0 bp)                         8924                                                                                 
#Contigs (>= 1000 bp)                      1231                                                                                 
Largest contig                             15195                                                                                
Total length                               4180287                                                                              
Total length (>= 0 bp)                     4658124                                                                              
Total length (>= 1000 bp)                  3794782                                                                              
Reference length                           4411532                                                                              
N50                                        3547                                                                                 
NG50                                       3400                                                                                 
N75                                        1911                                                                                 
NG75                                       1686                                                                                 
L50                                        364                                                                                  
LG50                                       397                                                                                  
L75                                        764                                                                                  
LG75                                       861                                                                                  
#local misassemblies                       5                                                                                    
#misassemblies                             0                                                                                    
#misassembled contigs                      0                                                                                    
Misassembled contigs length                0                                                                                    
Misassemblies inter-contig overlap         672                                                                                  
#unaligned contigs                         1 + 2 part                                                                           
Unaligned contigs length                   875                                                                                  
#ambiguously mapped contigs                29                                                                                   
Extra bases in ambiguously mapped contigs  -12980                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.368                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     2685 + 1329 part                                                                     
#predicted genes (unique)                  5432                                                                                 
#predicted genes (>= 0 bp)                 5432                                                                                 
#predicted genes (>= 300 bp)               4090                                                                                 
#predicted genes (>= 1500 bp)              333                                                                                  
#predicted genes (>= 3000 bp)              24                                                                                   
#mismatches                                123                                                                                  
#indels                                    642                                                                                  
Indels length                              676                                                                                  
#mismatches per 100 kbp                    2.95                                                                                 
#indels per 100 kbp                        15.42                                                                                
GC (%)                                     65.31                                                                                
Reference GC (%)                           65.61                                                                                
Average %IDY                               99.966                                                                               
Largest alignment                          15195                                                                                
NA50                                       3547                                                                                 
NGA50                                      3400                                                                                 
NA75                                       1911                                                                                 
NGA75                                      1686                                                                                 
LA50                                       364                                                                                  
LGA50                                      397                                                                                  
LA75                                       764                                                                                  
LGA75                                      861                                                                                  
#Mis_misassemblies                         0                                                                                    
#Mis_relocations                           0                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  0                                                                                    
Mis_Misassembled contigs length            0                                                                                    
#Mis_local misassemblies                   5                                                                                    
#Mis_short indels (<= 5 bp)                641                                                                                  
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               1                                                                                    
Una_Fully unaligned length                 819                                                                                  
#Una_partially unaligned contigs           2                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             56                                                                                   
GAGE_Contigs #                             1969                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            15195                                                                                
GAGE_N50                                   3400 COUNT: 397                                                                      
GAGE_Genome size                           4411532                                                                              
GAGE_Assembly size                         4180287                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               217096(4.92%)                                                                        
GAGE_Missing assembly bases                887(0.02%)                                                                           
GAGE_Missing assembly contigs              1(0.05%)                                                                             
GAGE_Duplicated reference bases            0                                                                                    
GAGE_Compressed reference bases            19705                                                                                
GAGE_Bad trim                              68                                                                                   
GAGE_Avg idy                               99.98                                                                                
GAGE_SNPs                                  81                                                                                   
GAGE_Indels < 5bp                          659                                                                                  
GAGE_Indels >= 5                           5                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            2                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1972                                                                                 
GAGE_Corrected assembly size               4180222                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    15199                                                                                
GAGE_Corrected N50                         3398 COUNT: 398                                                                      
