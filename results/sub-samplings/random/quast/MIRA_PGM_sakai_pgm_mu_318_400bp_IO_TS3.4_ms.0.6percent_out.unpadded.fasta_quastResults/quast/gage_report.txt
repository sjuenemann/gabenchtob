All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.6percent_out.unpadded
GAGE_Contigs #                   1425                                                               
GAGE_Min contig                  200                                                                
GAGE_Max contig                  429768                                                             
GAGE_N50                         184154 COUNT: 11                                                   
GAGE_Genome size                 5594470                                                            
GAGE_Assembly size               6247081                                                            
GAGE_Chaff bases                 0                                                                  
GAGE_Missing reference bases     474(0.01%)                                                         
GAGE_Missing assembly bases      1059(0.02%)                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                           
GAGE_Duplicated reference bases  720866                                                             
GAGE_Compressed reference bases  101804                                                             
GAGE_Bad trim                    1059                                                               
GAGE_Avg idy                     99.97                                                              
GAGE_SNPs                        361                                                                
GAGE_Indels < 5bp                420                                                                
GAGE_Indels >= 5                 6                                                                  
GAGE_Inversions                  16                                                                 
GAGE_Relocation                  14                                                                 
GAGE_Translocation               2                                                                  
GAGE_Corrected contig #          159                                                                
GAGE_Corrected assembly size     5610089                                                            
GAGE_Min correct contig          204                                                                
GAGE_Max correct contig          349757                                                             
GAGE_Corrected N50               130673 COUNT: 15                                                   
