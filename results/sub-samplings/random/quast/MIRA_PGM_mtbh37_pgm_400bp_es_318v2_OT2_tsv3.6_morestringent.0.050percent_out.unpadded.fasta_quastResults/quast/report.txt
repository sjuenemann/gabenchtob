All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.050percent_out.unpadded
#Contigs (>= 0 bp)             3825                                                                                 
#Contigs (>= 1000 bp)          1021                                                                                 
Total length (>= 0 bp)         3264534                                                                              
Total length (>= 1000 bp)      1717147                                                                              
#Contigs                       3791                                                                                 
Largest contig                 6966                                                                                 
Total length                   3259002                                                                              
Reference length               4411532                                                                              
GC (%)                         64.34                                                                                
Reference GC (%)               65.61                                                                                
N50                            1056                                                                                 
NG50                           756                                                                                  
N75                            643                                                                                  
NG75                           None                                                                                 
#misassemblies                 14                                                                                   
#local misassemblies           6                                                                                    
#unaligned contigs             5 + 14 part                                                                          
Unaligned contigs length       6530                                                                                 
Genome fraction (%)            71.852                                                                               
Duplication ratio              1.026                                                                                
#N's per 100 kbp               11.63                                                                                
#mismatches per 100 kbp        15.55                                                                                
#indels per 100 kbp            120.73                                                                               
#genes                         1073 + 2723 part                                                                     
#predicted genes (unique)      6352                                                                                 
#predicted genes (>= 0 bp)     6356                                                                                 
#predicted genes (>= 300 bp)   3809                                                                                 
#predicted genes (>= 1500 bp)  46                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                    
Largest alignment              6966                                                                                 
NA50                           1049                                                                                 
NGA50                          749                                                                                  
NA75                           639                                                                                  
NGA75                          None                                                                                 
