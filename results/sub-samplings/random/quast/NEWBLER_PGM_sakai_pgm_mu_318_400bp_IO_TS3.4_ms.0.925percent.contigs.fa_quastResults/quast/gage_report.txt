All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent.contigs
GAGE_Contigs #                   325                                                                
GAGE_Min contig                  200                                                                
GAGE_Max contig                  316782                                                             
GAGE_N50                         111027 COUNT: 16                                                   
GAGE_Genome size                 5594470                                                            
GAGE_Assembly size               5354763                                                            
GAGE_Chaff bases                 0                                                                  
GAGE_Missing reference bases     40904(0.73%)                                                       
GAGE_Missing assembly bases      40(0.00%)                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                           
GAGE_Duplicated reference bases  10135                                                              
GAGE_Compressed reference bases  219938                                                             
GAGE_Bad trim                    40                                                                 
GAGE_Avg idy                     99.99                                                              
GAGE_SNPs                        63                                                                 
GAGE_Indels < 5bp                166                                                                
GAGE_Indels >= 5                 4                                                                  
GAGE_Inversions                  0                                                                  
GAGE_Relocation                  3                                                                  
GAGE_Translocation               0                                                                  
GAGE_Corrected contig #          293                                                                
GAGE_Corrected assembly size     5345819                                                            
GAGE_Min correct contig          200                                                                
GAGE_Max correct contig          313854                                                             
GAGE_Corrected N50               111031 COUNT: 17                                                   
