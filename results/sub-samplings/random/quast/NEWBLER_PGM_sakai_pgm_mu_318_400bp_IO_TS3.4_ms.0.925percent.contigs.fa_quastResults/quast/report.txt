All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent.contigs
#Contigs (>= 0 bp)             1165                                                               
#Contigs (>= 1000 bp)          150                                                                
Total length (>= 0 bp)         5472081                                                            
Total length (>= 1000 bp)      5281718                                                            
#Contigs                       325                                                                
Largest contig                 316782                                                             
Total length                   5354763                                                            
Reference length               5594470                                                            
GC (%)                         50.29                                                              
Reference GC (%)               50.48                                                              
N50                            123947                                                             
NG50                           111027                                                             
N75                            53074                                                              
NG75                           44514                                                              
#misassemblies                 16                                                                 
#local misassemblies           5                                                                  
#unaligned contigs             0 + 1 part                                                         
Unaligned contigs length       22                                                                 
Genome fraction (%)            94.063                                                             
Duplication ratio              1.002                                                              
#N's per 100 kbp               0.00                                                               
#mismatches per 100 kbp        1.60                                                               
#indels per 100 kbp            2.91                                                               
#genes                         4962 + 147 part                                                    
#predicted genes (unique)      5349                                                               
#predicted genes (>= 0 bp)     5352                                                               
#predicted genes (>= 300 bp)   4472                                                               
#predicted genes (>= 1500 bp)  667                                                                
#predicted genes (>= 3000 bp)  64                                                                 
Largest alignment              313849                                                             
NA50                           123947                                                             
NGA50                          111027                                                             
NA75                           53074                                                              
NGA75                          44514                                                              
