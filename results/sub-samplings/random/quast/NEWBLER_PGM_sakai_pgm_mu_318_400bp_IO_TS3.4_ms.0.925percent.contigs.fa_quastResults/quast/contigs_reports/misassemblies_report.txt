All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.925percent.contigs
#Mis_misassemblies               16                                                                 
#Mis_relocations                 15                                                                 
#Mis_translocations              1                                                                  
#Mis_inversions                  0                                                                  
#Mis_misassembled contigs        16                                                                 
Mis_Misassembled contigs length  329398                                                             
#Mis_local misassemblies         5                                                                  
#mismatches                      84                                                                 
#indels                          153                                                                
#Mis_short indels (<= 5 bp)      152                                                                
#Mis_long indels (> 5 bp)        1                                                                  
Indels length                    171                                                                
