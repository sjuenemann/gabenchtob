All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.100percent_out.unpadded
#Contigs                                   258                                                                                  
#Contigs (>= 0 bp)                         273                                                                                  
#Contigs (>= 1000 bp)                      140                                                                                  
Largest contig                             90838                                                                                
Total length                               2847073                                                                              
Total length (>= 0 bp)                     2849449                                                                              
Total length (>= 1000 bp)                  2790042                                                                              
Reference length                           2813862                                                                              
N50                                        36956                                                                                
NG50                                       36956                                                                                
N75                                        24625                                                                                
NG75                                       25753                                                                                
L50                                        26                                                                                   
LG50                                       26                                                                                   
L75                                        48                                                                                   
LG75                                       47                                                                                   
#local misassemblies                       12                                                                                   
#misassemblies                             41                                                                                   
#misassembled contigs                      37                                                                                   
Misassembled contigs length                889687                                                                               
Misassemblies inter-contig overlap         9492                                                                                 
#unaligned contigs                         3 + 30 part                                                                          
Unaligned contigs length                   2242                                                                                 
#ambiguously mapped contigs                18                                                                                   
Extra bases in ambiguously mapped contigs  -9283                                                                                
#N's                                       225                                                                                  
#N's per 100 kbp                           7.90                                                                                 
Genome fraction (%)                        99.255                                                                               
Duplication ratio                          1.019                                                                                
#genes                                     2610 + 112 part                                                                      
#predicted genes (unique)                  2913                                                                                 
#predicted genes (>= 0 bp)                 2917                                                                                 
#predicted genes (>= 300 bp)               2391                                                                                 
#predicted genes (>= 1500 bp)              273                                                                                  
#predicted genes (>= 3000 bp)              26                                                                                   
#mismatches                                384                                                                                  
#indels                                    464                                                                                  
Indels length                              782                                                                                  
#mismatches per 100 kbp                    13.75                                                                                
#indels per 100 kbp                        16.61                                                                                
GC (%)                                     32.72                                                                                
Reference GC (%)                           32.81                                                                                
Average %IDY                               98.899                                                                               
Largest alignment                          82186                                                                                
NA50                                       34962                                                                                
NGA50                                      34962                                                                                
NA75                                       19485                                                                                
NGA75                                      20693                                                                                
LA50                                       28                                                                                   
LGA50                                      28                                                                                   
LA75                                       54                                                                                   
LGA75                                      52                                                                                   
#Mis_misassemblies                         41                                                                                   
#Mis_relocations                           24                                                                                   
#Mis_translocations                        0                                                                                    
#Mis_inversions                            17                                                                                   
#Mis_misassembled contigs                  37                                                                                   
Mis_Misassembled contigs length            889687                                                                               
#Mis_local misassemblies                   12                                                                                   
#Mis_short indels (<= 5 bp)                454                                                                                  
#Mis_long indels (> 5 bp)                  10                                                                                   
#Una_fully unaligned contigs               3                                                                                    
Una_Fully unaligned length                 808                                                                                  
#Una_partially unaligned contigs           30                                                                                   
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             1434                                                                                 
GAGE_Contigs #                             258                                                                                  
GAGE_Min contig                            210                                                                                  
GAGE_Max contig                            90838                                                                                
GAGE_N50                                   36956 COUNT: 26                                                                      
GAGE_Genome size                           2813862                                                                              
GAGE_Assembly size                         2847073                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               2229(0.08%)                                                                          
GAGE_Missing assembly bases                3934(0.14%)                                                                          
GAGE_Missing assembly contigs              3(1.16%)                                                                             
GAGE_Duplicated reference bases            32455                                                                                
GAGE_Compressed reference bases            25648                                                                                
GAGE_Bad trim                              3028                                                                                 
GAGE_Avg idy                               99.96                                                                                
GAGE_SNPs                                  107                                                                                  
GAGE_Indels < 5bp                          380                                                                                  
GAGE_Indels >= 5                           9                                                                                    
GAGE_Inversions                            14                                                                                   
GAGE_Relocation                            7                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    221                                                                                  
GAGE_Corrected assembly size               2820269                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    82203                                                                                
GAGE_Corrected N50                         33221 COUNT: 30                                                                      
