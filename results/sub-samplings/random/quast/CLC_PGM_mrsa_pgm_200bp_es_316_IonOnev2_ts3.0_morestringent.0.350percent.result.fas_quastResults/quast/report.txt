All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.350percent.result
#Contigs (>= 0 bp)             317                                                                           
#Contigs (>= 1000 bp)          152                                                                           
Total length (>= 0 bp)         2783310                                                                       
Total length (>= 1000 bp)      2735528                                                                       
#Contigs                       317                                                                           
Largest contig                 116954                                                                        
Total length                   2783310                                                                       
Reference length               2813862                                                                       
GC (%)                         32.63                                                                         
Reference GC (%)               32.81                                                                         
N50                            31343                                                                         
NG50                           31343                                                                         
N75                            15535                                                                         
NG75                           15461                                                                         
#misassemblies                 18                                                                            
#local misassemblies           3                                                                             
#unaligned contigs             7 + 19 part                                                                   
Unaligned contigs length       2493                                                                          
Genome fraction (%)            97.619                                                                        
Duplication ratio              1.011                                                                         
#N's per 100 kbp               4.89                                                                          
#mismatches per 100 kbp        1.53                                                                          
#indels per 100 kbp            15.25                                                                         
#genes                         2586 + 91 part                                                                
#predicted genes (unique)      2899                                                                          
#predicted genes (>= 0 bp)     2901                                                                          
#predicted genes (>= 300 bp)   2327                                                                          
#predicted genes (>= 1500 bp)  273                                                                           
#predicted genes (>= 3000 bp)  25                                                                            
Largest alignment              116835                                                                        
NA50                           31325                                                                         
NGA50                          31325                                                                         
NA75                           15461                                                                         
NGA75                          15046                                                                         
