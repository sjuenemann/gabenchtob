All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.350percent.result
#Mis_misassemblies               18                                                                            
#Mis_relocations                 3                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  15                                                                            
#Mis_misassembled contigs        18                                                                            
Mis_Misassembled contigs length  31713                                                                         
#Mis_local misassemblies         3                                                                             
#mismatches                      42                                                                            
#indels                          419                                                                           
#Mis_short indels (<= 5 bp)      419                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    439                                                                           
