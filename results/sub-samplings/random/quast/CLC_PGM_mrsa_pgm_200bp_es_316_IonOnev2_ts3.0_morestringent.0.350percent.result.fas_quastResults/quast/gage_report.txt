All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.350percent.result
GAGE_Contigs #                   317                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  116954                                                                        
GAGE_N50                         31343 COUNT: 28                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2783310                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     27955(0.99%)                                                                  
GAGE_Missing assembly bases      2336(0.08%)                                                                   
GAGE_Missing assembly contigs    6(1.89%)                                                                      
GAGE_Duplicated reference bases  28485                                                                         
GAGE_Compressed reference bases  38327                                                                         
GAGE_Bad trim                    794                                                                           
GAGE_Avg idy                     99.98                                                                         
GAGE_SNPs                        41                                                                            
GAGE_Indels < 5bp                427                                                                           
GAGE_Indels >= 5                 4                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  2                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          202                                                                           
GAGE_Corrected assembly size     2753509                                                                       
GAGE_Min correct contig          201                                                                           
GAGE_Max correct contig          116847                                                                        
GAGE_Corrected N50               29526 COUNT: 29                                                               
