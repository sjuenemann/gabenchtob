All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.K99.final-contigs
GAGE_Contigs #                   454                                                                       
GAGE_Min contig                  200                                                                       
GAGE_Max contig                  374870                                                                    
GAGE_N50                         127682 COUNT: 15                                                          
GAGE_Genome size                 5594470                                                                   
GAGE_Assembly size               5384579                                                                   
GAGE_Chaff bases                 0                                                                         
GAGE_Missing reference bases     8851(0.16%)                                                               
GAGE_Missing assembly bases      47(0.00%)                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                  
GAGE_Duplicated reference bases  1457                                                                      
GAGE_Compressed reference bases  270261                                                                    
GAGE_Bad trim                    47                                                                        
GAGE_Avg idy                     99.97                                                                     
GAGE_SNPs                        133                                                                       
GAGE_Indels < 5bp                1319                                                                      
GAGE_Indels >= 5                 8                                                                         
GAGE_Inversions                  0                                                                         
GAGE_Relocation                  5                                                                         
GAGE_Translocation               0                                                                         
GAGE_Corrected contig #          462                                                                       
GAGE_Corrected assembly size     5386522                                                                   
GAGE_Min correct contig          200                                                                       
GAGE_Max correct contig          226975                                                                    
GAGE_Corrected N50               124348 COUNT: 17                                                          
