All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.850percent.K99.final-contigs
#Contigs (>= 0 bp)             223                                                                                         
#Contigs (>= 1000 bp)          172                                                                                         
Total length (>= 0 bp)         4300261                                                                                     
Total length (>= 1000 bp)      4282113                                                                                     
#Contigs                       197                                                                                         
Largest contig                 158673                                                                                      
Total length                   4297196                                                                                     
Reference length               4411532                                                                                     
GC (%)                         65.40                                                                                       
Reference GC (%)               65.61                                                                                       
N50                            52622                                                                                       
NG50                           47767                                                                                       
N75                            24368                                                                                       
NG75                           23308                                                                                       
#misassemblies                 4                                                                                           
#local misassemblies           26                                                                                          
#unaligned contigs             1 + 0 part                                                                                  
Unaligned contigs length       203                                                                                         
Genome fraction (%)            97.001                                                                                      
Duplication ratio              1.006                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        9.88                                                                                        
#indels per 100 kbp            36.78                                                                                       
#genes                         3908 + 138 part                                                                             
#predicted genes (unique)      4521                                                                                        
#predicted genes (>= 0 bp)     4526                                                                                        
#predicted genes (>= 300 bp)   3806                                                                                        
#predicted genes (>= 1500 bp)  452                                                                                         
#predicted genes (>= 3000 bp)  44                                                                                          
Largest alignment              158673                                                                                      
NA50                           47767                                                                                       
NGA50                          46571                                                                                       
NA75                           23632                                                                                       
NGA75                          22922                                                                                       
