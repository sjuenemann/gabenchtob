All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.990percent.result
#Una_fully unaligned contigs      3                                                                             
Una_Fully unaligned length        1316                                                                          
#Una_partially unaligned contigs  15                                                                            
#Una_with misassembly             0                                                                             
#Una_both parts are significant   1                                                                             
Una_Partially unaligned length    1418                                                                          
#N's                              588                                                                           
