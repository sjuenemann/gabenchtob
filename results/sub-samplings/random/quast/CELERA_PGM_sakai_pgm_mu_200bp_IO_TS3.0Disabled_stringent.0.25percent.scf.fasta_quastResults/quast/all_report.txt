All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.25percent.scf
#Contigs                                   513                                                                     
#Contigs (>= 0 bp)                         513                                                                     
#Contigs (>= 1000 bp)                      513                                                                     
Largest contig                             103225                                                                  
Total length                               5274970                                                                 
Total length (>= 0 bp)                     5274970                                                                 
Total length (>= 1000 bp)                  5274970                                                                 
Reference length                           5594470                                                                 
N50                                        24056                                                                   
NG50                                       20676                                                                   
N75                                        9194                                                                    
NG75                                       7857                                                                    
L50                                        62                                                                      
LG50                                       69                                                                      
L75                                        152                                                                     
LG75                                       180                                                                     
#local misassemblies                       7                                                                       
#misassemblies                             5                                                                       
#misassembled contigs                      5                                                                       
Misassembled contigs length                43520                                                                   
Misassemblies inter-contig overlap         2074                                                                    
#unaligned contigs                         0 + 5 part                                                              
Unaligned contigs length                   194                                                                     
#ambiguously mapped contigs                2                                                                       
Extra bases in ambiguously mapped contigs  -2586                                                                   
#N's                                       0                                                                       
#N's per 100 kbp                           0.00                                                                    
Genome fraction (%)                        93.084                                                                  
Duplication ratio                          1.013                                                                   
#genes                                     4683 + 398 part                                                         
#predicted genes (unique)                  5855                                                                    
#predicted genes (>= 0 bp)                 5857                                                                    
#predicted genes (>= 300 bp)               4741                                                                    
#predicted genes (>= 1500 bp)              527                                                                     
#predicted genes (>= 3000 bp)              34                                                                      
#mismatches                                125                                                                     
#indels                                    1317                                                                    
Indels length                              1398                                                                    
#mismatches per 100 kbp                    2.40                                                                    
#indels per 100 kbp                        25.29                                                                   
GC (%)                                     50.21                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               99.034                                                                  
Largest alignment                          103225                                                                  
NA50                                       24007                                                                   
NGA50                                      20567                                                                   
NA75                                       9194                                                                    
NGA75                                      7810                                                                    
LA50                                       62                                                                      
LGA50                                      69                                                                      
LA75                                       153                                                                     
LGA75                                      182                                                                     
#Mis_misassemblies                         5                                                                       
#Mis_relocations                           4                                                                       
#Mis_translocations                        0                                                                       
#Mis_inversions                            1                                                                       
#Mis_misassembled contigs                  5                                                                       
Mis_Misassembled contigs length            43520                                                                   
#Mis_local misassemblies                   7                                                                       
#Mis_short indels (<= 5 bp)                1313                                                                    
#Mis_long indels (> 5 bp)                  4                                                                       
#Una_fully unaligned contigs               0                                                                       
Una_Fully unaligned length                 0                                                                       
#Una_partially unaligned contigs           5                                                                       
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             194                                                                     
GAGE_Contigs #                             513                                                                     
GAGE_Min contig                            1004                                                                    
GAGE_Max contig                            103225                                                                  
GAGE_N50                                   20676 COUNT: 69                                                         
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5274970                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               331436(5.92%)                                                           
GAGE_Missing assembly bases                713(0.01%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                
GAGE_Duplicated reference bases            1586                                                                    
GAGE_Compressed reference bases            80175                                                                   
GAGE_Bad trim                              693                                                                     
GAGE_Avg idy                               99.97                                                                   
GAGE_SNPs                                  82                                                                      
GAGE_Indels < 5bp                          1218                                                                    
GAGE_Indels >= 5                           10                                                                      
GAGE_Inversions                            3                                                                       
GAGE_Relocation                            2                                                                       
GAGE_Translocation                         0                                                                       
GAGE_Corrected contig #                    527                                                                     
GAGE_Corrected assembly size               5276988                                                                 
GAGE_Min correct contig                    408                                                                     
GAGE_Max correct contig                    103245                                                                  
GAGE_Corrected N50                         20488 COUNT: 71                                                         
