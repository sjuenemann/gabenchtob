All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.400percent.K99.final-contigs
GAGE_Contigs #                   188                                                                                         
GAGE_Min contig                  203                                                                                         
GAGE_Max contig                  181881                                                                                      
GAGE_N50                         40791 COUNT: 29                                                                             
GAGE_Genome size                 4411532                                                                                     
GAGE_Assembly size               4278120                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     89727(2.03%)                                                                                
GAGE_Missing assembly bases      659(0.02%)                                                                                  
GAGE_Missing assembly contigs    1(0.53%)                                                                                    
GAGE_Duplicated reference bases  1116                                                                                        
GAGE_Compressed reference bases  56646                                                                                       
GAGE_Bad trim                    224                                                                                         
GAGE_Avg idy                     99.93                                                                                       
GAGE_SNPs                        258                                                                                         
GAGE_Indels < 5bp                1977                                                                                        
GAGE_Indels >= 5                 19                                                                                          
GAGE_Inversions                  6                                                                                           
GAGE_Relocation                  18                                                                                          
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          232                                                                                         
GAGE_Corrected assembly size     4284511                                                                                     
GAGE_Min correct contig          203                                                                                         
GAGE_Max correct contig          123818                                                                                      
GAGE_Corrected N50               34035 COUNT: 37                                                                             
