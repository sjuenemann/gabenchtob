All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.result
#Contigs                                   880                                                         
#Contigs (>= 0 bp)                         880                                                         
#Contigs (>= 1000 bp)                      341                                                         
Largest contig                             108419                                                      
Total length                               5395200                                                     
Total length (>= 0 bp)                     5395200                                                     
Total length (>= 1000 bp)                  5214911                                                     
Reference length                           5594470                                                     
N50                                        31626                                                       
NG50                                       30778                                                       
N75                                        15828                                                       
NG75                                       14186                                                       
L50                                        55                                                          
LG50                                       58                                                          
L75                                        114                                                         
LG75                                       124                                                         
#local misassemblies                       5                                                           
#misassemblies                             12                                                          
#misassembled contigs                      12                                                          
Misassembled contigs length                124012                                                      
Misassemblies inter-contig overlap         1283                                                        
#unaligned contigs                         3 + 20 part                                                 
Unaligned contigs length                   1439                                                        
#ambiguously mapped contigs                153                                                         
Extra bases in ambiguously mapped contigs  -84233                                                      
#N's                                       275                                                         
#N's per 100 kbp                           5.10                                                        
Genome fraction (%)                        94.036                                                      
Duplication ratio                          1.009                                                       
#genes                                     4786 + 339 part                                             
#predicted genes (unique)                  6139                                                        
#predicted genes (>= 0 bp)                 6142                                                        
#predicted genes (>= 300 bp)               4717                                                        
#predicted genes (>= 1500 bp)              547                                                         
#predicted genes (>= 3000 bp)              42                                                          
#mismatches                                112                                                         
#indels                                    1093                                                        
Indels length                              1251                                                        
#mismatches per 100 kbp                    2.13                                                        
#indels per 100 kbp                        20.78                                                       
GC (%)                                     50.29                                                       
Reference GC (%)                           50.48                                                       
Average %IDY                               98.947                                                      
Largest alignment                          108419                                                      
NA50                                       31626                                                       
NGA50                                      30778                                                       
NA75                                       15179                                                       
NGA75                                      13888                                                       
LA50                                       55                                                          
LGA50                                      58                                                          
LA75                                       116                                                         
LGA75                                      126                                                         
#Mis_misassemblies                         12                                                          
#Mis_relocations                           3                                                           
#Mis_translocations                        0                                                           
#Mis_inversions                            9                                                           
#Mis_misassembled contigs                  12                                                          
Mis_Misassembled contigs length            124012                                                      
#Mis_local misassemblies                   5                                                           
#Mis_short indels (<= 5 bp)                1089                                                        
#Mis_long indels (> 5 bp)                  4                                                           
#Una_fully unaligned contigs               3                                                           
Una_Fully unaligned length                 760                                                         
#Una_partially unaligned contigs           20                                                          
#Una_with misassembly                      0                                                           
#Una_both parts are significant            0                                                           
Una_Partially unaligned length             679                                                         
GAGE_Contigs #                             880                                                         
GAGE_Min contig                            200                                                         
GAGE_Max contig                            108419                                                      
GAGE_N50                                   30778 COUNT: 58                                             
GAGE_Genome size                           5594470                                                     
GAGE_Assembly size                         5395200                                                     
GAGE_Chaff bases                           0                                                           
GAGE_Missing reference bases               44438(0.79%)                                                
GAGE_Missing assembly bases                1510(0.03%)                                                 
GAGE_Missing assembly contigs              3(0.34%)                                                    
GAGE_Duplicated reference bases            60297                                                       
GAGE_Compressed reference bases            252230                                                      
GAGE_Bad trim                              750                                                         
GAGE_Avg idy                               99.98                                                       
GAGE_SNPs                                  68                                                          
GAGE_Indels < 5bp                          1103                                                        
GAGE_Indels >= 5                           6                                                           
GAGE_Inversions                            0                                                           
GAGE_Relocation                            4                                                           
GAGE_Translocation                         0                                                           
GAGE_Corrected contig #                    655                                                         
GAGE_Corrected assembly size               5335747                                                     
GAGE_Min correct contig                    200                                                         
GAGE_Max correct contig                    108437                                                      
GAGE_Corrected N50                         29305 COUNT: 59                                             
