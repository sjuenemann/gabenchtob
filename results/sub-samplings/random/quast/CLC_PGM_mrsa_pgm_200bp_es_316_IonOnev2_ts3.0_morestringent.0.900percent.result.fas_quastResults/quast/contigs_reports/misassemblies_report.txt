All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.900percent.result
#Mis_misassemblies               7                                                                             
#Mis_relocations                 4                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  3                                                                             
#Mis_misassembled contigs        7                                                                             
Mis_Misassembled contigs length  63776                                                                         
#Mis_local misassemblies         1                                                                             
#mismatches                      93                                                                            
#indels                          277                                                                           
#Mis_short indels (<= 5 bp)      277                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    301                                                                           
