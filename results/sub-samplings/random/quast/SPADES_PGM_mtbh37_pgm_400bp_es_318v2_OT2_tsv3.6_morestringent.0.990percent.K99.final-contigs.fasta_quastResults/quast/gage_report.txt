All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.990percent.K99.final-contigs
GAGE_Contigs #                   170                                                                                         
GAGE_Min contig                  202                                                                                         
GAGE_Max contig                  158677                                                                                      
GAGE_N50                         66035 COUNT: 22                                                                             
GAGE_Genome size                 4411532                                                                                     
GAGE_Assembly size               4300413                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     70076(1.59%)                                                                                
GAGE_Missing assembly bases      308(0.01%)                                                                                  
GAGE_Missing assembly contigs    1(0.59%)                                                                                    
GAGE_Duplicated reference bases  475                                                                                         
GAGE_Compressed reference bases  53542                                                                                       
GAGE_Bad trim                    105                                                                                         
GAGE_Avg idy                     99.96                                                                                       
GAGE_SNPs                        213                                                                                         
GAGE_Indels < 5bp                1401                                                                                        
GAGE_Indels >= 5                 19                                                                                          
GAGE_Inversions                  0                                                                                           
GAGE_Relocation                  13                                                                                          
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          202                                                                                         
GAGE_Corrected assembly size     4305012                                                                                     
GAGE_Min correct contig          202                                                                                         
GAGE_Max correct contig          156929                                                                                      
GAGE_Corrected N50               55569 COUNT: 27                                                                             
