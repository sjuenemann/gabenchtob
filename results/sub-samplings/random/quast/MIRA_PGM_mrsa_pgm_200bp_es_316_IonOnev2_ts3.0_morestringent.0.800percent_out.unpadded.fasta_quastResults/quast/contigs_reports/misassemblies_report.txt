All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.800percent_out.unpadded
#Mis_misassemblies               1257                                                                                 
#Mis_relocations                 128                                                                                  
#Mis_translocations              1                                                                                    
#Mis_inversions                  1128                                                                                 
#Mis_misassembled contigs        1128                                                                                 
Mis_Misassembled contigs length  885857                                                                               
#Mis_local misassemblies         99                                                                                   
#mismatches                      139                                                                                  
#indels                          232                                                                                  
#Mis_short indels (<= 5 bp)      230                                                                                  
#Mis_long indels (> 5 bp)        2                                                                                    
Indels length                    310                                                                                  
