All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.650percent.result
#Contigs                                   756                                                                           
#Contigs (>= 0 bp)                         756                                                                           
#Contigs (>= 1000 bp)                      305                                                                           
Largest contig                             83080                                                                         
Total length                               4312768                                                                       
Total length (>= 0 bp)                     4312768                                                                       
Total length (>= 1000 bp)                  4159005                                                                       
Reference length                           4411532                                                                       
N50                                        20617                                                                         
NG50                                       20203                                                                         
N75                                        11476                                                                         
NG75                                       10593                                                                         
L50                                        66                                                                            
LG50                                       68                                                                            
L75                                        135                                                                           
LG75                                       142                                                                           
#local misassemblies                       4                                                                             
#misassemblies                             13                                                                            
#misassembled contigs                      12                                                                            
Misassembled contigs length                59836                                                                         
Misassemblies inter-contig overlap         923                                                                           
#unaligned contigs                         7 + 10 part                                                                   
Unaligned contigs length                   3381                                                                          
#ambiguously mapped contigs                35                                                                            
Extra bases in ambiguously mapped contigs  -18178                                                                        
#N's                                       533                                                                           
#N's per 100 kbp                           12.36                                                                         
Genome fraction (%)                        95.969                                                                        
Duplication ratio                          1.014                                                                         
#genes                                     3734 + 315 part                                                               
#predicted genes (unique)                  4857                                                                          
#predicted genes (>= 0 bp)                 4865                                                                          
#predicted genes (>= 300 bp)               3797                                                                          
#predicted genes (>= 1500 bp)              434                                                                           
#predicted genes (>= 3000 bp)              46                                                                            
#mismatches                                233                                                                           
#indels                                    1254                                                                          
Indels length                              1525                                                                          
#mismatches per 100 kbp                    5.50                                                                          
#indels per 100 kbp                        29.62                                                                         
GC (%)                                     65.33                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.628                                                                        
Largest alignment                          83080                                                                         
NA50                                       20206                                                                         
NGA50                                      19834                                                                         
NA75                                       11343                                                                         
NGA75                                      10497                                                                         
LA50                                       66                                                                            
LGA50                                      69                                                                            
LA75                                       137                                                                           
LGA75                                      144                                                                           
#Mis_misassemblies                         13                                                                            
#Mis_relocations                           13                                                                            
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  12                                                                            
Mis_Misassembled contigs length            59836                                                                         
#Mis_local misassemblies                   4                                                                             
#Mis_short indels (<= 5 bp)                1249                                                                          
#Mis_long indels (> 5 bp)                  5                                                                             
#Una_fully unaligned contigs               7                                                                             
Una_Fully unaligned length                 2394                                                                          
#Una_partially unaligned contigs           10                                                                            
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             987                                                                           
GAGE_Contigs #                             756                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            83080                                                                         
GAGE_N50                                   20203 COUNT: 68                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         4312768                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               119143(2.70%)                                                                 
GAGE_Missing assembly bases                3533(0.08%)                                                                   
GAGE_Missing assembly contigs              7(0.93%)                                                                      
GAGE_Duplicated reference bases            56480                                                                         
GAGE_Compressed reference bases            48218                                                                         
GAGE_Bad trim                              1137                                                                          
GAGE_Avg idy                               99.96                                                                         
GAGE_SNPs                                  132                                                                           
GAGE_Indels < 5bp                          1226                                                                          
GAGE_Indels >= 5                           7                                                                             
GAGE_Inversions                            6                                                                             
GAGE_Relocation                            7                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    574                                                                           
GAGE_Corrected assembly size               4254720                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    60689                                                                         
GAGE_Corrected N50                         19503 COUNT: 72                                                               
