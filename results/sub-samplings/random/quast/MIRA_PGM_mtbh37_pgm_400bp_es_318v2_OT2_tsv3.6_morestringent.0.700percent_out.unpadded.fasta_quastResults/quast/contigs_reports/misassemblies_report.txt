All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.700percent_out.unpadded
#Mis_misassemblies               30                                                                                   
#Mis_relocations                 30                                                                                   
#Mis_translocations              0                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        27                                                                                   
Mis_Misassembled contigs length  1180050                                                                              
#Mis_local misassemblies         24                                                                                   
#mismatches                      483                                                                                  
#indels                          608                                                                                  
#Mis_short indels (<= 5 bp)      597                                                                                  
#Mis_long indels (> 5 bp)        11                                                                                   
Indels length                    931                                                                                  
