reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.800percent.contigs_broken  | 97.9873213399       | 1.00125161938     | 28          | 2669      | 23        | None      | None      |
  NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.800percent.contigs  | 98.2790911566       | 1.00317996643     | 31          | 2670      | 24        | None      | None      |
