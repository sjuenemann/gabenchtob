All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.500percent_out.unpadded
#Contigs (>= 0 bp)             217                                                                                                             
#Contigs (>= 1000 bp)          131                                                                                                             
Total length (>= 0 bp)         4417155                                                                                                         
Total length (>= 1000 bp)      4370183                                                                                                         
#Contigs                       207                                                                                                             
Largest contig                 201189                                                                                                          
Total length                   4415726                                                                                                         
Reference length               4411532                                                                                                         
GC (%)                         65.57                                                                                                           
Reference GC (%)               65.61                                                                                                           
N50                            78138                                                                                                           
NG50                           78138                                                                                                           
N75                            39582                                                                                                           
NG75                           39582                                                                                                           
#misassemblies                 40                                                                                                              
#local misassemblies           16                                                                                                              
#unaligned contigs             0 + 9 part                                                                                                      
Unaligned contigs length       660                                                                                                             
Genome fraction (%)            99.482                                                                                                          
Duplication ratio              1.016                                                                                                           
#N's per 100 kbp               2.92                                                                                                            
#mismatches per 100 kbp        10.66                                                                                                           
#indels per 100 kbp            1.50                                                                                                            
#genes                         3987 + 123 part                                                                                                 
#predicted genes (unique)      4152                                                                                                            
#predicted genes (>= 0 bp)     4178                                                                                                            
#predicted genes (>= 300 bp)   3652                                                                                                            
#predicted genes (>= 1500 bp)  564                                                                                                             
#predicted genes (>= 3000 bp)  78                                                                                                              
Largest alignment              151382                                                                                                          
NA50                           59058                                                                                                           
NGA50                          59058                                                                                                           
NA75                           28571                                                                                                           
NGA75                          28571                                                                                                           
