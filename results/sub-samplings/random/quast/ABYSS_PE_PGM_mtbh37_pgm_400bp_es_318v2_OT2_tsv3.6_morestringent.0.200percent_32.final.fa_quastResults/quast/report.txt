All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.200percent_32.final
#Contigs (>= 0 bp)             4393                                                                                 
#Contigs (>= 1000 bp)          1277                                                                                 
Total length (>= 0 bp)         4177937                                                                              
Total length (>= 1000 bp)      3420803                                                                              
#Contigs                       2428                                                                                 
Largest contig                 15337                                                                                
Total length                   4022280                                                                              
Reference length               4411532                                                                              
GC (%)                         65.01                                                                                
Reference GC (%)               65.61                                                                                
N50                            2780                                                                                 
NG50                           2480                                                                                 
N75                            1470                                                                                 
NG75                           1125                                                                                 
#misassemblies                 0                                                                                    
#local misassemblies           9                                                                                    
#unaligned contigs             1 + 0 part                                                                           
Unaligned contigs length       779                                                                                  
Genome fraction (%)            90.722                                                                               
Duplication ratio              1.002                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        4.72                                                                                 
#indels per 100 kbp            27.23                                                                                
#genes                         2338 + 1650 part                                                                     
#predicted genes (unique)      5831                                                                                 
#predicted genes (>= 0 bp)     5831                                                                                 
#predicted genes (>= 300 bp)   4190                                                                                 
#predicted genes (>= 1500 bp)  247                                                                                  
#predicted genes (>= 3000 bp)  9                                                                                    
Largest alignment              15337                                                                                
NA50                           2780                                                                                 
NGA50                          2480                                                                                 
NA75                           1470                                                                                 
NGA75                          1125                                                                                 
