All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent_out.unpadded
#Contigs (>= 0 bp)             8185                                                               
#Contigs (>= 1000 bp)          1385                                                               
Total length (>= 0 bp)         9481123                                                            
Total length (>= 1000 bp)      5672259                                                            
#Contigs                       8010                                                               
Largest contig                 239102                                                             
Total length                   9454476                                                            
Reference length               2813862                                                            
GC (%)                         32.79                                                              
Reference GC (%)               32.81                                                              
N50                            1794                                                               
NG50                           43167                                                              
N75                            676                                                                
NG75                           18053                                                              
#misassemblies                 533                                                                
#local misassemblies           30                                                                 
#unaligned contigs             22 + 234 part                                                      
Unaligned contigs length       24717                                                              
Genome fraction (%)            99.806                                                             
Duplication ratio              3.282                                                              
#N's per 100 kbp               27.30                                                              
#mismatches per 100 kbp        5.88                                                               
#indels per 100 kbp            13.82                                                              
#genes                         2674 + 52 part                                                     
#predicted genes (unique)      15399                                                              
#predicted genes (>= 0 bp)     16636                                                              
#predicted genes (>= 300 bp)   7650                                                               
#predicted genes (>= 1500 bp)  359                                                                
#predicted genes (>= 3000 bp)  29                                                                 
Largest alignment              239102                                                             
NA50                           1744                                                               
NGA50                          40359                                                              
NA75                           642                                                                
NGA75                          17074                                                              
