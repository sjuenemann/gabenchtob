All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.650percent_out.unpadded
#Contigs                                   4806                                                                            
#Contigs (>= 0 bp)                         4931                                                                            
#Contigs (>= 1000 bp)                      695                                                                             
Largest contig                             305953                                                                          
Total length                               6268096                                                                         
Total length (>= 0 bp)                     6287324                                                                         
Total length (>= 1000 bp)                  3965112                                                                         
Reference length                           2813862                                                                         
N50                                        2677                                                                            
NG50                                       125952                                                                          
N75                                        698                                                                             
NG75                                       62875                                                                           
L50                                        122                                                                             
LG50                                       8                                                                               
L75                                        1601                                                                            
LG75                                       16                                                                              
#local misassemblies                       27                                                                              
#misassemblies                             167                                                                             
#misassembled contigs                      165                                                                             
Misassembled contigs length                676828                                                                          
Misassemblies inter-contig overlap         22018                                                                           
#unaligned contigs                         12 + 80 part                                                                    
Unaligned contigs length                   9239                                                                            
#ambiguously mapped contigs                114                                                                             
Extra bases in ambiguously mapped contigs  -68889                                                                          
#N's                                       1368                                                                            
#N's per 100 kbp                           21.82                                                                           
Genome fraction (%)                        99.984                                                                          
Duplication ratio                          2.208                                                                           
#genes                                     2703 + 23 part                                                                  
#predicted genes (unique)                  10541                                                                           
#predicted genes (>= 0 bp)                 10856                                                                           
#predicted genes (>= 300 bp)               4868                                                                            
#predicted genes (>= 1500 bp)              306                                                                             
#predicted genes (>= 3000 bp)              31                                                                              
#mismatches                                172                                                                             
#indels                                    156                                                                             
Indels length                              179                                                                             
#mismatches per 100 kbp                    6.11                                                                            
#indels per 100 kbp                        5.54                                                                            
GC (%)                                     32.77                                                                           
Reference GC (%)                           32.81                                                                           
Average %IDY                               98.903                                                                          
Largest alignment                          305950                                                                          
NA50                                       2580                                                                            
NGA50                                      121394                                                                          
NA75                                       683                                                                             
NGA75                                      60434                                                                           
LA50                                       133                                                                             
LGA50                                      8                                                                               
LA75                                       1644                                                                            
LGA75                                      16                                                                              
#Mis_misassemblies                         167                                                                             
#Mis_relocations                           158                                                                             
#Mis_translocations                        4                                                                               
#Mis_inversions                            5                                                                               
#Mis_misassembled contigs                  165                                                                             
Mis_Misassembled contigs length            676828                                                                          
#Mis_local misassemblies                   27                                                                              
#Mis_short indels (<= 5 bp)                155                                                                             
#Mis_long indels (> 5 bp)                  1                                                                               
#Una_fully unaligned contigs               12                                                                              
Una_Fully unaligned length                 4204                                                                            
#Una_partially unaligned contigs           80                                                                              
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            0                                                                               
Una_Partially unaligned length             5035                                                                            
GAGE_Contigs #                             4806                                                                            
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            305953                                                                          
GAGE_N50                                   125952 COUNT: 8                                                                 
GAGE_Genome size                           2813862                                                                         
GAGE_Assembly size                         6268096                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               88(0.00%)                                                                       
GAGE_Missing assembly bases                14946(0.24%)                                                                    
GAGE_Missing assembly contigs              8(0.17%)                                                                        
GAGE_Duplicated reference bases            3419894                                                                         
GAGE_Compressed reference bases            5928                                                                            
GAGE_Bad trim                              11823                                                                           
GAGE_Avg idy                               99.98                                                                           
GAGE_SNPs                                  50                                                                              
GAGE_Indels < 5bp                          63                                                                              
GAGE_Indels >= 5                           4                                                                               
GAGE_Inversions                            2                                                                               
GAGE_Relocation                            2                                                                               
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    74                                                                              
GAGE_Corrected assembly size               2855232                                                                         
GAGE_Min correct contig                    234                                                                             
GAGE_Max correct contig                    305951                                                                          
GAGE_Corrected N50                         123143 COUNT: 8                                                                 
