All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.100percent.result
#Contigs (>= 0 bp)             603                                                                      
#Contigs (>= 1000 bp)          219                                                                      
Total length (>= 0 bp)         2840329                                                                  
Total length (>= 1000 bp)      2715308                                                                  
#Contigs                       603                                                                      
Largest contig                 64514                                                                    
Total length                   2840329                                                                  
Reference length               2813862                                                                  
GC (%)                         32.62                                                                    
Reference GC (%)               32.81                                                                    
N50                            20490                                                                    
NG50                           20490                                                                    
N75                            10071                                                                    
NG75                           10106                                                                    
#misassemblies                 4                                                                        
#local misassemblies           1                                                                        
#unaligned contigs             2 + 13 part                                                              
Unaligned contigs length       1306                                                                     
Genome fraction (%)            97.631                                                                   
Duplication ratio              1.030                                                                    
#N's per 100 kbp               20.10                                                                    
#mismatches per 100 kbp        2.11                                                                     
#indels per 100 kbp            20.82                                                                    
#genes                         2522 + 171 part                                                          
#predicted genes (unique)      3155                                                                     
#predicted genes (>= 0 bp)     3158                                                                     
#predicted genes (>= 300 bp)   2380                                                                     
#predicted genes (>= 1500 bp)  270                                                                      
#predicted genes (>= 3000 bp)  21                                                                       
Largest alignment              64514                                                                    
NA50                           20490                                                                    
NGA50                          20490                                                                    
NA75                           10071                                                                    
NGA75                          10106                                                                    
