All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.400percent_36.final
#Contigs (>= 0 bp)             4913                                                                                 
#Contigs (>= 1000 bp)          1132                                                                                 
Total length (>= 0 bp)         4381334                                                                              
Total length (>= 1000 bp)      3854015                                                                              
#Contigs                       1723                                                                                 
Largest contig                 22807                                                                                
Total length                   4158981                                                                              
Reference length               4411532                                                                              
GC (%)                         65.20                                                                                
Reference GC (%)               65.61                                                                                
N50                            4004                                                                                 
NG50                           3810                                                                                 
N75                            2267                                                                                 
NG75                           1951                                                                                 
#misassemblies                 0                                                                                    
#local misassemblies           6                                                                                    
#unaligned contigs             1 + 0 part                                                                           
Unaligned contigs length       829                                                                                  
Genome fraction (%)            93.836                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        2.56                                                                                 
#indels per 100 kbp            20.80                                                                                
#genes                         2835 + 1178 part                                                                     
#predicted genes (unique)      5382                                                                                 
#predicted genes (>= 0 bp)     5382                                                                                 
#predicted genes (>= 300 bp)   4081                                                                                 
#predicted genes (>= 1500 bp)  326                                                                                  
#predicted genes (>= 3000 bp)  20                                                                                   
Largest alignment              22807                                                                                
NA50                           4004                                                                                 
NGA50                          3810                                                                                 
NA75                           2267                                                                                 
NGA75                          1951                                                                                 
