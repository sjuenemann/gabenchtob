All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.950percent.result
#Mis_misassemblies               4                                                                             
#Mis_relocations                 2                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  2                                                                             
#Mis_misassembled contigs        4                                                                             
Mis_Misassembled contigs length  54203                                                                         
#Mis_local misassemblies         1                                                                             
#mismatches                      42                                                                            
#indels                          290                                                                           
#Mis_short indels (<= 5 bp)      290                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    305                                                                           
