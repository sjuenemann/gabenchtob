All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.8percent_out.unpadded
GAGE_Contigs #                   2248                                                                          
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  215257                                                                        
GAGE_N50                         77774 COUNT: 26                                                               
GAGE_Genome size                 5594470                                                                       
GAGE_Assembly size               6221383                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     573(0.01%)                                                                    
GAGE_Missing assembly bases      84199(1.35%)                                                                  
GAGE_Missing assembly contigs    33(1.47%)                                                                     
GAGE_Duplicated reference bases  650446                                                                        
GAGE_Compressed reference bases  159927                                                                        
GAGE_Bad trim                    71972                                                                         
GAGE_Avg idy                     99.96                                                                         
GAGE_SNPs                        385                                                                           
GAGE_Indels < 5bp                240                                                                           
GAGE_Indels >= 5                 11                                                                            
GAGE_Inversions                  16                                                                            
GAGE_Relocation                  16                                                                            
GAGE_Translocation               5                                                                             
GAGE_Corrected contig #          295                                                                           
GAGE_Corrected assembly size     5552651                                                                       
GAGE_Min correct contig          229                                                                           
GAGE_Max correct contig          215260                                                                        
GAGE_Corrected N50               57458 COUNT: 31                                                               
