All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.175percent_out.unpadded
#Contigs                                   580                                                                                                 
#Contigs (>= 0 bp)                         586                                                                                                 
#Contigs (>= 1000 bp)                      410                                                                                                 
Largest contig                             64426                                                                                               
Total length                               5503753                                                                                             
Total length (>= 0 bp)                     5504495                                                                                             
Total length (>= 1000 bp)                  5419485                                                                                             
Reference length                           5594470                                                                                             
N50                                        23212                                                                                               
NG50                                       22791                                                                                               
N75                                        13041                                                                                               
NG75                                       12585                                                                                               
L50                                        77                                                                                                  
LG50                                       79                                                                                                  
L75                                        155                                                                                                 
LG75                                       160                                                                                                 
#local misassemblies                       10                                                                                                  
#misassemblies                             53                                                                                                  
#misassembled contigs                      45                                                                                                  
Misassembled contigs length                719929                                                                                              
Misassemblies inter-contig overlap         70642                                                                                               
#unaligned contigs                         1 + 2 part                                                                                          
Unaligned contigs length                   5614                                                                                                
#ambiguously mapped contigs                80                                                                                                  
Extra bases in ambiguously mapped contigs  -57702                                                                                              
#N's                                       47                                                                                                  
#N's per 100 kbp                           0.85                                                                                                
Genome fraction (%)                        97.445                                                                                              
Duplication ratio                          1.011                                                                                               
#genes                                     4978 + 355 part                                                                                     
#predicted genes (unique)                  5635                                                                                                
#predicted genes (>= 0 bp)                 5647                                                                                                
#predicted genes (>= 300 bp)               4665                                                                                                
#predicted genes (>= 1500 bp)              659                                                                                                 
#predicted genes (>= 3000 bp)              68                                                                                                  
#mismatches                                2722                                                                                                
#indels                                    153                                                                                                 
Indels length                              297                                                                                                 
#mismatches per 100 kbp                    49.93                                                                                               
#indels per 100 kbp                        2.81                                                                                                
GC (%)                                     50.40                                                                                               
Reference GC (%)                           50.48                                                                                               
Average %IDY                               98.723                                                                                              
Largest alignment                          64426                                                                                               
NA50                                       22574                                                                                               
NGA50                                      22334                                                                                               
NA75                                       12291                                                                                               
NGA75                                      12017                                                                                               
LA50                                       80                                                                                                  
LGA50                                      82                                                                                                  
LA75                                       162                                                                                                 
LGA75                                      168                                                                                                 
#Mis_misassemblies                         53                                                                                                  
#Mis_relocations                           51                                                                                                  
#Mis_translocations                        2                                                                                                   
#Mis_inversions                            0                                                                                                   
#Mis_misassembled contigs                  45                                                                                                  
Mis_Misassembled contigs length            719929                                                                                              
#Mis_local misassemblies                   10                                                                                                  
#Mis_short indels (<= 5 bp)                146                                                                                                 
#Mis_long indels (> 5 bp)                  7                                                                                                   
#Una_fully unaligned contigs               1                                                                                                   
Una_Fully unaligned length                 5464                                                                                                
#Una_partially unaligned contigs           2                                                                                                   
#Una_with misassembly                      0                                                                                                   
#Una_both parts are significant            0                                                                                                   
Una_Partially unaligned length             150                                                                                                 
GAGE_Contigs #                             580                                                                                                 
GAGE_Min contig                            205                                                                                                 
GAGE_Max contig                            64426                                                                                               
GAGE_N50                                   22791 COUNT: 79                                                                                     
GAGE_Genome size                           5594470                                                                                             
GAGE_Assembly size                         5503753                                                                                             
GAGE_Chaff bases                           0                                                                                                   
GAGE_Missing reference bases               13603(0.24%)                                                                                        
GAGE_Missing assembly bases                6461(0.12%)                                                                                         
GAGE_Missing assembly contigs              1(0.17%)                                                                                            
GAGE_Duplicated reference bases            87269                                                                                               
GAGE_Compressed reference bases            200056                                                                                              
GAGE_Bad trim                              994                                                                                                 
GAGE_Avg idy                               99.95                                                                                               
GAGE_SNPs                                  804                                                                                                 
GAGE_Indels < 5bp                          47                                                                                                  
GAGE_Indels >= 5                           5                                                                                                   
GAGE_Inversions                            16                                                                                                  
GAGE_Relocation                            16                                                                                                  
GAGE_Translocation                         2                                                                                                   
GAGE_Corrected contig #                    513                                                                                                 
GAGE_Corrected assembly size               5480759                                                                                             
GAGE_Min correct contig                    205                                                                                                 
GAGE_Max correct contig                    64426                                                                                               
GAGE_Corrected N50                         21973 COUNT: 86                                                                                     
