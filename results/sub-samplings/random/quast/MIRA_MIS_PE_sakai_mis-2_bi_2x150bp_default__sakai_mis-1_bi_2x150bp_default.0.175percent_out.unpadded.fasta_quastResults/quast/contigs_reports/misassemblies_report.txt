All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.175percent_out.unpadded
#Mis_misassemblies               53                                                                                                  
#Mis_relocations                 51                                                                                                  
#Mis_translocations              2                                                                                                   
#Mis_inversions                  0                                                                                                   
#Mis_misassembled contigs        45                                                                                                  
Mis_Misassembled contigs length  719929                                                                                              
#Mis_local misassemblies         10                                                                                                  
#mismatches                      2722                                                                                                
#indels                          153                                                                                                 
#Mis_short indels (<= 5 bp)      146                                                                                                 
#Mis_long indels (> 5 bp)        7                                                                                                   
Indels length                    297                                                                                                 
