All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.05percent.K99.final-contigs
#Contigs                                   325                                                                        
#Contigs (>= 0 bp)                         366                                                                        
#Contigs (>= 1000 bp)                      298                                                                        
Largest contig                             196627                                                                     
Total length                               5323880                                                                    
Total length (>= 0 bp)                     5328791                                                                    
Total length (>= 1000 bp)                  5311611                                                                    
Reference length                           5594470                                                                    
N50                                        38130                                                                      
NG50                                       36437                                                                      
N75                                        19642                                                                      
NG75                                       17161                                                                      
L50                                        40                                                                         
LG50                                       44                                                                         
L75                                        87                                                                         
LG75                                       98                                                                         
#local misassemblies                       29                                                                         
#misassemblies                             27                                                                         
#misassembled contigs                      22                                                                         
Misassembled contigs length                309080                                                                     
Misassemblies inter-contig overlap         12665                                                                      
#unaligned contigs                         0 + 0 part                                                                 
Unaligned contigs length                   0                                                                          
#ambiguously mapped contigs                38                                                                         
Extra bases in ambiguously mapped contigs  -42750                                                                     
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        94.008                                                                     
Duplication ratio                          1.007                                                                      
#genes                                     4864 + 220 part                                                            
#predicted genes (unique)                  5944                                                                       
#predicted genes (>= 0 bp)                 5978                                                                       
#predicted genes (>= 300 bp)               4764                                                                       
#predicted genes (>= 1500 bp)              505                                                                        
#predicted genes (>= 3000 bp)              34                                                                         
#mismatches                                1548                                                                       
#indels                                    3501                                                                       
Indels length                              3627                                                                       
#mismatches per 100 kbp                    29.43                                                                      
#indels per 100 kbp                        66.57                                                                      
GC (%)                                     50.30                                                                      
Reference GC (%)                           50.48                                                                      
Average %IDY                               98.965                                                                     
Largest alignment                          196497                                                                     
NA50                                       36437                                                                      
NGA50                                      35286                                                                      
NA75                                       19438                                                                      
NGA75                                      17062                                                                      
LA50                                       42                                                                         
LGA50                                      45                                                                         
LA75                                       90                                                                         
LGA75                                      101                                                                        
#Mis_misassemblies                         27                                                                         
#Mis_relocations                           26                                                                         
#Mis_translocations                        0                                                                          
#Mis_inversions                            1                                                                          
#Mis_misassembled contigs                  22                                                                         
Mis_Misassembled contigs length            309080                                                                     
#Mis_local misassemblies                   29                                                                         
#Mis_short indels (<= 5 bp)                3501                                                                       
#Mis_long indels (> 5 bp)                  0                                                                          
#Una_fully unaligned contigs               0                                                                          
Una_Fully unaligned length                 0                                                                          
#Una_partially unaligned contigs           0                                                                          
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             0                                                                          
GAGE_Contigs #                             325                                                                        
GAGE_Min contig                            201                                                                        
GAGE_Max contig                            196627                                                                     
GAGE_N50                                   36437 COUNT: 44                                                            
GAGE_Genome size                           5594470                                                                    
GAGE_Assembly size                         5323880                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               37869(0.68%)                                                               
GAGE_Missing assembly bases                186(0.00%)                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                   
GAGE_Duplicated reference bases            29166                                                                      
GAGE_Compressed reference bases            282873                                                                     
GAGE_Bad trim                              177                                                                        
GAGE_Avg idy                               99.91                                                                      
GAGE_SNPs                                  667                                                                        
GAGE_Indels < 5bp                          3391                                                                       
GAGE_Indels >= 5                           24                                                                         
GAGE_Inversions                            10                                                                         
GAGE_Relocation                            14                                                                         
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    378                                                                        
GAGE_Corrected assembly size               5318382                                                                    
GAGE_Min correct contig                    201                                                                        
GAGE_Max correct contig                    196498                                                                     
GAGE_Corrected N50                         30431 COUNT: 51                                                            
