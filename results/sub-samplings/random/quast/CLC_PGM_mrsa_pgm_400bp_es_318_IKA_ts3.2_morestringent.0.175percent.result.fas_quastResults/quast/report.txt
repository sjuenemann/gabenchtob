All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent.result
#Contigs (>= 0 bp)             691                                                                      
#Contigs (>= 1000 bp)          184                                                                      
Total length (>= 0 bp)         2853080                                                                  
Total length (>= 1000 bp)      2692393                                                                  
#Contigs                       691                                                                      
Largest contig                 82579                                                                    
Total length                   2853080                                                                  
Reference length               2813862                                                                  
GC (%)                         32.62                                                                    
Reference GC (%)               32.81                                                                    
N50                            25036                                                                    
NG50                           25036                                                                    
N75                            11370                                                                    
NG75                           12145                                                                    
#misassemblies                 9                                                                        
#local misassemblies           1                                                                        
#unaligned contigs             10 + 8 part                                                              
Unaligned contigs length       3892                                                                     
Genome fraction (%)            97.483                                                                   
Duplication ratio              1.035                                                                    
#N's per 100 kbp               34.45                                                                    
#mismatches per 100 kbp        2.88                                                                     
#indels per 100 kbp            18.67                                                                    
#genes                         2510 + 168 part                                                          
#predicted genes (unique)      3165                                                                     
#predicted genes (>= 0 bp)     3173                                                                     
#predicted genes (>= 300 bp)   2376                                                                     
#predicted genes (>= 1500 bp)  269                                                                      
#predicted genes (>= 3000 bp)  22                                                                       
Largest alignment              82579                                                                    
NA50                           24142                                                                    
NGA50                          24157                                                                    
NA75                           11370                                                                    
NGA75                          12145                                                                    
