All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent.result
#Una_fully unaligned contigs      10                                                                       
Una_Fully unaligned length        3174                                                                     
#Una_partially unaligned contigs  8                                                                        
#Una_with misassembly             0                                                                        
#Una_both parts are significant   0                                                                        
Una_Partially unaligned length    718                                                                      
#N's                              983                                                                      
