All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.550percent_54.final
GAGE_Contigs #                   2875                                                                            
GAGE_Min contig                  200                                                                             
GAGE_Max contig                  6272                                                                            
GAGE_N50                         1052 COUNT: 761                                                                 
GAGE_Genome size                 2813862                                                                         
GAGE_Assembly size               2490571                                                                         
GAGE_Chaff bases                 0                                                                               
GAGE_Missing reference bases     320814(11.40%)                                                                  
GAGE_Missing assembly bases      0(0.00%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                        
GAGE_Duplicated reference bases  0                                                                               
GAGE_Compressed reference bases  6165                                                                            
GAGE_Bad trim                    0                                                                               
GAGE_Avg idy                     100.00                                                                          
GAGE_SNPs                        27                                                                              
GAGE_Indels < 5bp                68                                                                              
GAGE_Indels >= 5                 1                                                                               
GAGE_Inversions                  0                                                                               
GAGE_Relocation                  2                                                                               
GAGE_Translocation               0                                                                               
GAGE_Corrected contig #          2877                                                                            
GAGE_Corrected assembly size     2490802                                                                         
GAGE_Min correct contig          200                                                                             
GAGE_Max correct contig          6272                                                                            
GAGE_Corrected N50               1052 COUNT: 761                                                                 
