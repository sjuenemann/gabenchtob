All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.500percent.result
#Contigs                                   4868                                                                       
#Contigs (>= 0 bp)                         4868                                                                       
#Contigs (>= 1000 bp)                      1105                                                                       
Largest contig                             7067                                                                       
Total length                               3732968                                                                    
Total length (>= 0 bp)                     3732968                                                                    
Total length (>= 1000 bp)                  1953797                                                                    
Reference length                           4411532                                                                    
N50                                        1062                                                                       
NG50                                       841                                                                        
N75                                        549                                                                        
NG75                                       371                                                                        
L50                                        1021                                                                       
LG50                                       1381                                                                       
L75                                        2253                                                                       
LG75                                       3384                                                                       
#local misassemblies                       40                                                                         
#misassemblies                             790                                                                        
#misassembled contigs                      732                                                                        
Misassembled contigs length                774470                                                                     
Misassemblies inter-contig overlap         10414                                                                      
#unaligned contigs                         10 + 677 part                                                              
Unaligned contigs length                   32457                                                                      
#ambiguously mapped contigs                12                                                                         
Extra bases in ambiguously mapped contigs  -6327                                                                      
#N's                                       3119                                                                       
#N's per 100 kbp                           83.55                                                                      
Genome fraction (%)                        79.162                                                                     
Duplication ratio                          1.061                                                                      
#genes                                     1137 + 2762 part                                                           
#predicted genes (unique)                  7817                                                                       
#predicted genes (>= 0 bp)                 7824                                                                       
#predicted genes (>= 300 bp)               3950                                                                       
#predicted genes (>= 1500 bp)              36                                                                         
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                2072                                                                       
#indels                                    5623                                                                       
Indels length                              6054                                                                       
#mismatches per 100 kbp                    59.33                                                                      
#indels per 100 kbp                        161.01                                                                     
GC (%)                                     64.67                                                                      
Reference GC (%)                           65.61                                                                      
Average %IDY                               99.673                                                                     
Largest alignment                          7067                                                                       
NA50                                       964                                                                        
NGA50                                      763                                                                        
NA75                                       484                                                                        
NGA75                                      309                                                                        
LA50                                       1107                                                                       
LGA50                                      1504                                                                       
LA75                                       2486                                                                       
LGA75                                      3796                                                                       
#Mis_misassemblies                         790                                                                        
#Mis_relocations                           191                                                                        
#Mis_translocations                        0                                                                          
#Mis_inversions                            599                                                                        
#Mis_misassembled contigs                  732                                                                        
Mis_Misassembled contigs length            774470                                                                     
#Mis_local misassemblies                   40                                                                         
#Mis_short indels (<= 5 bp)                5613                                                                       
#Mis_long indels (> 5 bp)                  10                                                                         
#Una_fully unaligned contigs               10                                                                         
Una_Fully unaligned length                 3456                                                                       
#Una_partially unaligned contigs           677                                                                        
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            2                                                                          
Una_Partially unaligned length             29001                                                                      
GAGE_Contigs #                             4868                                                                       
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            7067                                                                       
GAGE_N50                                   841 COUNT: 1381                                                            
GAGE_Genome size                           4411532                                                                    
GAGE_Assembly size                         3732968                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               874588(19.83%)                                                             
GAGE_Missing assembly bases                39638(1.06%)                                                               
GAGE_Missing assembly contigs              8(0.16%)                                                                   
GAGE_Duplicated reference bases            89556                                                                      
GAGE_Compressed reference bases            42394                                                                      
GAGE_Bad trim                              35672                                                                      
GAGE_Avg idy                               99.76                                                                      
GAGE_SNPs                                  1529                                                                       
GAGE_Indels < 5bp                          5154                                                                       
GAGE_Indels >= 5                           17                                                                         
GAGE_Inversions                            278                                                                        
GAGE_Relocation                            47                                                                         
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    4859                                                                       
GAGE_Corrected assembly size               3580667                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    7075                                                                       
GAGE_Corrected N50                         764 COUNT: 1508                                                            
