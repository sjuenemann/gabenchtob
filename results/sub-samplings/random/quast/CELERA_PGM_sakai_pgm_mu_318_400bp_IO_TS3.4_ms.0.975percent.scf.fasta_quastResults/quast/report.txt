All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.975percent.scf
#Contigs (>= 0 bp)             333                                                           
#Contigs (>= 1000 bp)          333                                                           
Total length (>= 0 bp)         5294605                                                       
Total length (>= 1000 bp)      5294605                                                       
#Contigs                       333                                                           
Largest contig                 254038                                                        
Total length                   5294605                                                       
Reference length               5594470                                                       
GC (%)                         50.32                                                         
Reference GC (%)               50.48                                                         
N50                            41594                                                         
NG50                           34782                                                         
N75                            19615                                                         
NG75                           16974                                                         
#misassemblies                 3                                                             
#local misassemblies           8                                                             
#unaligned contigs             0 + 0 part                                                    
Unaligned contigs length       0                                                             
Genome fraction (%)            92.870                                                        
Duplication ratio              1.013                                                         
#N's per 100 kbp               0.00                                                          
#mismatches per 100 kbp        2.21                                                          
#indels per 100 kbp            7.10                                                          
#genes                         4854 + 232 part                                               
#predicted genes (unique)      5388                                                          
#predicted genes (>= 0 bp)     5393                                                          
#predicted genes (>= 300 bp)   4526                                                          
#predicted genes (>= 1500 bp)  629                                                           
#predicted genes (>= 3000 bp)  55                                                            
Largest alignment              254038                                                        
NA50                           40410                                                         
NGA50                          34782                                                         
NA75                           19615                                                         
NGA75                          16974                                                         
