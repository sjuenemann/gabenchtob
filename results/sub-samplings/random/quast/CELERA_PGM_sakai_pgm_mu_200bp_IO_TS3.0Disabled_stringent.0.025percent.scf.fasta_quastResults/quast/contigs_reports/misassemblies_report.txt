All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent.scf
#Mis_misassemblies               6                                                                        
#Mis_relocations                 1                                                                        
#Mis_translocations              0                                                                        
#Mis_inversions                  5                                                                        
#Mis_misassembled contigs        6                                                                        
Mis_Misassembled contigs length  8373                                                                     
#Mis_local misassemblies         1                                                                        
#mismatches                      217                                                                      
#indels                          522                                                                      
#Mis_short indels (<= 5 bp)      522                                                                      
#Mis_long indels (> 5 bp)        0                                                                        
Indels length                    533                                                                      
