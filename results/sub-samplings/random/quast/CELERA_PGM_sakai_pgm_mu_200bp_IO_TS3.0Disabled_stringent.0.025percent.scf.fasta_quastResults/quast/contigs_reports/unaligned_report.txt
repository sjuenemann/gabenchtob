All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent.scf
#Una_fully unaligned contigs      0                                                                        
Una_Fully unaligned length        0                                                                        
#Una_partially unaligned contigs  4                                                                        
#Una_with misassembly             0                                                                        
#Una_both parts are significant   1                                                                        
Una_Partially unaligned length    426                                                                      
#N's                              0                                                                        
