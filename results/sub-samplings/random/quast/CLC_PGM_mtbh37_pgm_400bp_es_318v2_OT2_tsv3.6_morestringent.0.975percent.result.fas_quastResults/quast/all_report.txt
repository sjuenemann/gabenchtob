All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.975percent.result
#Contigs                                   913                                                                           
#Contigs (>= 0 bp)                         913                                                                           
#Contigs (>= 1000 bp)                      339                                                                           
Largest contig                             67205                                                                         
Total length                               4349466                                                                       
Total length (>= 0 bp)                     4349466                                                                       
Total length (>= 1000 bp)                  4158375                                                                       
Reference length                           4411532                                                                       
N50                                        18200                                                                         
NG50                                       18020                                                                         
N75                                        10045                                                                         
NG75                                       9647                                                                          
L50                                        74                                                                            
LG50                                       76                                                                            
L75                                        151                                                                           
LG75                                       156                                                                           
#local misassemblies                       9                                                                             
#misassemblies                             17                                                                            
#misassembled contigs                      15                                                                            
Misassembled contigs length                109295                                                                        
Misassemblies inter-contig overlap         21399                                                                         
#unaligned contigs                         2 + 14 part                                                                   
Unaligned contigs length                   2374                                                                          
#ambiguously mapped contigs                30                                                                            
Extra bases in ambiguously mapped contigs  -17871                                                                        
#N's                                       403                                                                           
#N's per 100 kbp                           9.27                                                                          
Genome fraction (%)                        96.123                                                                        
Duplication ratio                          1.026                                                                         
#genes                                     3712 + 337 part                                                               
#predicted genes (unique)                  4970                                                                          
#predicted genes (>= 0 bp)                 4978                                                                          
#predicted genes (>= 300 bp)               3823                                                                          
#predicted genes (>= 1500 bp)              457                                                                           
#predicted genes (>= 3000 bp)              44                                                                            
#mismatches                                177                                                                           
#indels                                    1156                                                                          
Indels length                              1374                                                                          
#mismatches per 100 kbp                    4.17                                                                          
#indels per 100 kbp                        27.26                                                                         
GC (%)                                     65.35                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.654                                                                        
Largest alignment                          67186                                                                         
NA50                                       18053                                                                         
NGA50                                      17884                                                                         
NA75                                       10045                                                                         
NGA75                                      9647                                                                          
LA50                                       74                                                                            
LGA50                                      76                                                                            
LA75                                       152                                                                           
LGA75                                      157                                                                           
#Mis_misassemblies                         17                                                                            
#Mis_relocations                           17                                                                            
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  15                                                                            
Mis_Misassembled contigs length            109295                                                                        
#Mis_local misassemblies                   9                                                                             
#Mis_short indels (<= 5 bp)                1152                                                                          
#Mis_long indels (> 5 bp)                  4                                                                             
#Una_fully unaligned contigs               2                                                                             
Una_Fully unaligned length                 1352                                                                          
#Una_partially unaligned contigs           14                                                                            
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             1022                                                                          
GAGE_Contigs #                             913                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            67205                                                                         
GAGE_N50                                   18020 COUNT: 76                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         4349466                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               110382(2.50%)                                                                 
GAGE_Missing assembly bases                2531(0.06%)                                                                   
GAGE_Missing assembly contigs              3(0.33%)                                                                      
GAGE_Duplicated reference bases            80858                                                                         
GAGE_Compressed reference bases            50403                                                                         
GAGE_Bad trim                              909                                                                           
GAGE_Avg idy                               99.97                                                                         
GAGE_SNPs                                  104                                                                           
GAGE_Indels < 5bp                          1100                                                                          
GAGE_Indels >= 5                           8                                                                             
GAGE_Inversions                            9                                                                             
GAGE_Relocation                            6                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    660                                                                           
GAGE_Corrected assembly size               4268040                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    62802                                                                         
GAGE_Corrected N50                         17629 COUNT: 78                                                               
