All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.8percent_144.final
GAGE_Contigs #                   3786                                                                           
GAGE_Min contig                  200                                                                            
GAGE_Max contig                  34705                                                                          
GAGE_N50                         7469 COUNT: 222                                                                
GAGE_Genome size                 5594470                                                                        
GAGE_Assembly size               5975981                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     21253(0.38%)                                                                   
GAGE_Missing assembly bases      66(0.00%)                                                                      
GAGE_Missing assembly contigs    0(0.00%)                                                                       
GAGE_Duplicated reference bases  422959                                                                         
GAGE_Compressed reference bases  249436                                                                         
GAGE_Bad trim                    66                                                                             
GAGE_Avg idy                     99.97                                                                          
GAGE_SNPs                        48                                                                             
GAGE_Indels < 5bp                1582                                                                           
GAGE_Indels >= 5                 1                                                                              
GAGE_Inversions                  0                                                                              
GAGE_Relocation                  2                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          2260                                                                           
GAGE_Corrected assembly size     5554139                                                                        
GAGE_Min correct contig          200                                                                            
GAGE_Max correct contig          34713                                                                          
GAGE_Corrected N50               7472 COUNT: 222                                                                
