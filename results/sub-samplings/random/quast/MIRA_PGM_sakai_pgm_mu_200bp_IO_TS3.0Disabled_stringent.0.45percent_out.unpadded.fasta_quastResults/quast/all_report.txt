All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.45percent_out.unpadded
#Contigs                                   698                                                                            
#Contigs (>= 0 bp)                         719                                                                            
#Contigs (>= 1000 bp)                      173                                                                            
Largest contig                             399564                                                                         
Total length                               5687471                                                                        
Total length (>= 0 bp)                     5691097                                                                        
Total length (>= 1000 bp)                  5481039                                                                        
Reference length                           5594470                                                                        
N50                                        122856                                                                         
NG50                                       122856                                                                         
N75                                        47670                                                                          
NG75                                       48341                                                                          
L50                                        15                                                                             
LG50                                       15                                                                             
L75                                        34                                                                             
LG75                                       33                                                                             
#local misassemblies                       19                                                                             
#misassemblies                             113                                                                            
#misassembled contigs                      96                                                                             
Misassembled contigs length                2023625                                                                        
Misassemblies inter-contig overlap         40607                                                                          
#unaligned contigs                         3 + 230 part                                                                   
Unaligned contigs length                   11831                                                                          
#ambiguously mapped contigs                81                                                                             
Extra bases in ambiguously mapped contigs  -56356                                                                         
#N's                                       463                                                                            
#N's per 100 kbp                           8.14                                                                           
Genome fraction (%)                        97.857                                                                         
Duplication ratio                          1.034                                                                          
#genes                                     5230 + 156 part                                                                
#predicted genes (unique)                  5993                                                                           
#predicted genes (>= 0 bp)                 6027                                                                           
#predicted genes (>= 300 bp)               4746                                                                           
#predicted genes (>= 1500 bp)              669                                                                            
#predicted genes (>= 3000 bp)              64                                                                             
#mismatches                                1510                                                                           
#indels                                    385                                                                            
Indels length                              419                                                                            
#mismatches per 100 kbp                    27.57                                                                          
#indels per 100 kbp                        7.03                                                                           
GC (%)                                     50.42                                                                          
Reference GC (%)                           50.48                                                                          
Average %IDY                               98.618                                                                         
Largest alignment                          249730                                                                         
NA50                                       103061                                                                         
NGA50                                      103061                                                                         
NA75                                       41193                                                                          
NGA75                                      45108                                                                          
LA50                                       18                                                                             
LGA50                                      18                                                                             
LA75                                       39                                                                             
LGA75                                      37                                                                             
#Mis_misassemblies                         113                                                                            
#Mis_relocations                           44                                                                             
#Mis_translocations                        1                                                                              
#Mis_inversions                            68                                                                             
#Mis_misassembled contigs                  96                                                                             
Mis_Misassembled contigs length            2023625                                                                        
#Mis_local misassemblies                   19                                                                             
#Mis_short indels (<= 5 bp)                384                                                                            
#Mis_long indels (> 5 bp)                  1                                                                              
#Una_fully unaligned contigs               3                                                                              
Una_Fully unaligned length                 1110                                                                           
#Una_partially unaligned contigs           230                                                                            
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             10721                                                                          
GAGE_Contigs #                             698                                                                            
GAGE_Min contig                            203                                                                            
GAGE_Max contig                            399564                                                                         
GAGE_N50                                   122856 COUNT: 15                                                               
GAGE_Genome size                           5594470                                                                        
GAGE_Assembly size                         5687471                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               360(0.01%)                                                                     
GAGE_Missing assembly bases                14011(0.25%)                                                                   
GAGE_Missing assembly contigs              3(0.43%)                                                                       
GAGE_Duplicated reference bases            205611                                                                         
GAGE_Compressed reference bases            192752                                                                         
GAGE_Bad trim                              12901                                                                          
GAGE_Avg idy                               99.96                                                                          
GAGE_SNPs                                  355                                                                            
GAGE_Indels < 5bp                          313                                                                            
GAGE_Indels >= 5                           7                                                                              
GAGE_Inversions                            11                                                                             
GAGE_Relocation                            8                                                                              
GAGE_Translocation                         1                                                                              
GAGE_Corrected contig #                    248                                                                            
GAGE_Corrected assembly size               5506309                                                                        
GAGE_Min correct contig                    214                                                                            
GAGE_Max correct contig                    249733                                                                         
GAGE_Corrected N50                         101958 COUNT: 19                                                               
