All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.150percent.scf
GAGE_Contigs #                   63                                                                       
GAGE_Min contig                  1031                                                                     
GAGE_Max contig                  301692                                                                   
GAGE_N50                         102354 COUNT: 10                                                         
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2744374                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     75643(2.69%)                                                             
GAGE_Missing assembly bases      316(0.01%)                                                               
GAGE_Missing assembly contigs    0(0.00%)                                                                 
GAGE_Duplicated reference bases  0                                                                        
GAGE_Compressed reference bases  14359                                                                    
GAGE_Bad trim                    316                                                                      
GAGE_Avg idy                     99.98                                                                    
GAGE_SNPs                        55                                                                       
GAGE_Indels < 5bp                305                                                                      
GAGE_Indels >= 5                 6                                                                        
GAGE_Inversions                  0                                                                        
GAGE_Relocation                  1                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          70                                                                       
GAGE_Corrected assembly size     2745619                                                                  
GAGE_Min correct contig          229                                                                      
GAGE_Max correct contig          202518                                                                   
GAGE_Corrected N50               80959 COUNT: 11                                                          
