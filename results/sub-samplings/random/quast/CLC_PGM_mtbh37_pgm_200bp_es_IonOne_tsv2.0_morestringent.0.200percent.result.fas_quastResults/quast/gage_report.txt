All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.200percent.result
GAGE_Contigs #                   4901                                                                       
GAGE_Min contig                  200                                                                        
GAGE_Max contig                  4015                                                                       
GAGE_N50                         401 COUNT: 3121                                                            
GAGE_Genome size                 4411532                                                                    
GAGE_Assembly size               2770869                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     1822324(41.31%)                                                            
GAGE_Missing assembly bases      50767(1.83%)                                                               
GAGE_Missing assembly contigs    14(0.29%)                                                                  
GAGE_Duplicated reference bases  97736                                                                      
GAGE_Compressed reference bases  42139                                                                      
GAGE_Bad trim                    42333                                                                      
GAGE_Avg idy                     99.52                                                                      
GAGE_SNPs                        3326                                                                       
GAGE_Indels < 5bp                7372                                                                       
GAGE_Indels >= 5                 31                                                                         
GAGE_Inversions                  628                                                                        
GAGE_Relocation                  87                                                                         
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          5018                                                                       
GAGE_Corrected assembly size     2556248                                                                    
GAGE_Min correct contig          200                                                                        
GAGE_Max correct contig          3003                                                                       
GAGE_Corrected N50               316 COUNT: 3686                                                            
