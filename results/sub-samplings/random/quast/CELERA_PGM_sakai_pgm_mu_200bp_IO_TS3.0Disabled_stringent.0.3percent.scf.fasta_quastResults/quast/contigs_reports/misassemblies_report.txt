All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.3percent.scf
#Mis_misassemblies               5                                                                      
#Mis_relocations                 4                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  1                                                                      
#Mis_misassembled contigs        5                                                                      
Mis_Misassembled contigs length  71842                                                                  
#Mis_local misassemblies         6                                                                      
#mismatches                      129                                                                    
#indels                          1127                                                                   
#Mis_short indels (<= 5 bp)      1124                                                                   
#Mis_long indels (> 5 bp)        3                                                                      
Indels length                    1188                                                                   
