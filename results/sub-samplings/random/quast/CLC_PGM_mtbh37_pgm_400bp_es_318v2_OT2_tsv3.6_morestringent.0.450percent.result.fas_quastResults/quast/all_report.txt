All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.450percent.result
#Contigs                                   976                                                                           
#Contigs (>= 0 bp)                         976                                                                           
#Contigs (>= 1000 bp)                      422                                                                           
Largest contig                             53644                                                                         
Total length                               4320171                                                                       
Total length (>= 0 bp)                     4320171                                                                       
Total length (>= 1000 bp)                  4136008                                                                       
Reference length                           4411532                                                                       
N50                                        14030                                                                         
NG50                                       13768                                                                         
N75                                        7992                                                                          
NG75                                       7488                                                                          
L50                                        96                                                                            
LG50                                       99                                                                            
L75                                        197                                                                           
LG75                                       206                                                                           
#local misassemblies                       6                                                                             
#misassemblies                             19                                                                            
#misassembled contigs                      18                                                                            
Misassembled contigs length                177622                                                                        
Misassemblies inter-contig overlap         2211                                                                          
#unaligned contigs                         6 + 11 part                                                                   
Unaligned contigs length                   2893                                                                          
#ambiguously mapped contigs                25                                                                            
Extra bases in ambiguously mapped contigs  -15130                                                                        
#N's                                       742                                                                           
#N's per 100 kbp                           17.18                                                                         
Genome fraction (%)                        95.525                                                                        
Duplication ratio                          1.021                                                                         
#genes                                     3611 + 416 part                                                               
#predicted genes (unique)                  5095                                                                          
#predicted genes (>= 0 bp)                 5098                                                                          
#predicted genes (>= 300 bp)               3848                                                                          
#predicted genes (>= 1500 bp)              413                                                                           
#predicted genes (>= 3000 bp)              30                                                                            
#mismatches                                288                                                                           
#indels                                    1596                                                                          
Indels length                              1743                                                                          
#mismatches per 100 kbp                    6.83                                                                          
#indels per 100 kbp                        37.87                                                                         
GC (%)                                     65.33                                                                         
Reference GC (%)                           65.61                                                                         
Average %IDY                               99.646                                                                        
Largest alignment                          53644                                                                         
NA50                                       13768                                                                         
NGA50                                      13681                                                                         
NA75                                       7805                                                                          
NGA75                                      7424                                                                          
LA50                                       97                                                                            
LGA50                                      100                                                                           
LA75                                       200                                                                           
LGA75                                      209                                                                           
#Mis_misassemblies                         19                                                                            
#Mis_relocations                           19                                                                            
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  18                                                                            
Mis_Misassembled contigs length            177622                                                                        
#Mis_local misassemblies                   6                                                                             
#Mis_short indels (<= 5 bp)                1593                                                                          
#Mis_long indels (> 5 bp)                  3                                                                             
#Una_fully unaligned contigs               6                                                                             
Una_Fully unaligned length                 2127                                                                          
#Una_partially unaligned contigs           11                                                                            
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             766                                                                           
GAGE_Contigs #                             976                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            53644                                                                         
GAGE_N50                                   13768 COUNT: 99                                                               
GAGE_Genome size                           4411532                                                                       
GAGE_Assembly size                         4320171                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               139194(3.16%)                                                                 
GAGE_Missing assembly bases                3313(0.08%)                                                                   
GAGE_Missing assembly contigs              7(0.72%)                                                                      
GAGE_Duplicated reference bases            74115                                                                         
GAGE_Compressed reference bases            49109                                                                         
GAGE_Bad trim                              900                                                                           
GAGE_Avg idy                               99.95                                                                         
GAGE_SNPs                                  174                                                                           
GAGE_Indels < 5bp                          1503                                                                          
GAGE_Indels >= 5                           9                                                                             
GAGE_Inversions                            9                                                                             
GAGE_Relocation                            3                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    736                                                                           
GAGE_Corrected assembly size               4245640                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    53662                                                                         
GAGE_Corrected N50                         13497 COUNT: 103                                                              
