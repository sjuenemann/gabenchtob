All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.175percent.scf
GAGE_Contigs #                   531                                                                           
GAGE_Min contig                  1013                                                                          
GAGE_Max contig                  48001                                                                         
GAGE_N50                         11454 COUNT: 118                                                              
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4125461                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     274139(6.21%)                                                                 
GAGE_Missing assembly bases      936(0.02%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  901                                                                           
GAGE_Compressed reference bases  33948                                                                         
GAGE_Bad trim                    936                                                                           
GAGE_Avg idy                     99.96                                                                         
GAGE_SNPs                        89                                                                            
GAGE_Indels < 5bp                1521                                                                          
GAGE_Indels >= 5                 11                                                                            
GAGE_Inversions                  1                                                                             
GAGE_Relocation                  9                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          552                                                                           
GAGE_Corrected assembly size     4127281                                                                       
GAGE_Min correct contig          365                                                                           
GAGE_Max correct contig          48017                                                                         
GAGE_Corrected N50               11119 COUNT: 123                                                              
