All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.result
#Contigs                                   1227                                             
#Contigs (>= 0 bp)                         1227                                             
#Contigs (>= 1000 bp)                      295                                              
Largest contig                             133820                                           
Total length                               5493444                                          
Total length (>= 0 bp)                     5493444                                          
Total length (>= 1000 bp)                  5177242                                          
Reference length                           5594470                                          
N50                                        36019                                            
NG50                                       35823                                            
N75                                        17529                                            
NG75                                       17342                                            
L50                                        47                                               
LG50                                       49                                               
L75                                        103                                              
LG75                                       107                                              
#local misassemblies                       10                                               
#misassemblies                             7                                                
#misassembled contigs                      7                                                
Misassembled contigs length                108023                                           
Misassemblies inter-contig overlap         2994                                             
#unaligned contigs                         20 + 8 part                                      
Unaligned contigs length                   5814                                             
#ambiguously mapped contigs                160                                              
Extra bases in ambiguously mapped contigs  -87412                                           
#N's                                       2396                                             
#N's per 100 kbp                           43.62                                            
Genome fraction (%)                        94.125                                           
Duplication ratio                          1.026                                            
#genes                                     4788 + 348 part                                  
#predicted genes (unique)                  6068                                             
#predicted genes (>= 0 bp)                 6092                                             
#predicted genes (>= 300 bp)               4622                                             
#predicted genes (>= 1500 bp)              645                                              
#predicted genes (>= 3000 bp)              60                                               
#mismatches                                243                                              
#indels                                    496                                              
Indels length                              514                                              
#mismatches per 100 kbp                    4.61                                             
#indels per 100 kbp                        9.42                                             
GC (%)                                     50.29                                            
Reference GC (%)                           50.48                                            
Average %IDY                               98.982                                           
Largest alignment                          133820                                           
NA50                                       36016                                            
NGA50                                      35823                                            
NA75                                       17529                                            
NGA75                                      17342                                            
LA50                                       48                                               
LGA50                                      49                                               
LA75                                       103                                              
LGA75                                      107                                              
#Mis_misassemblies                         7                                                
#Mis_relocations                           7                                                
#Mis_translocations                        0                                                
#Mis_inversions                            0                                                
#Mis_misassembled contigs                  7                                                
Mis_Misassembled contigs length            108023                                           
#Mis_local misassemblies                   10                                               
#Mis_short indels (<= 5 bp)                495                                              
#Mis_long indels (> 5 bp)                  1                                                
#Una_fully unaligned contigs               20                                               
Una_Fully unaligned length                 5278                                             
#Una_partially unaligned contigs           8                                                
#Una_with misassembly                      0                                                
#Una_both parts are significant            0                                                
Una_Partially unaligned length             536                                              
GAGE_Contigs #                             1227                                             
GAGE_Min contig                            200                                              
GAGE_Max contig                            133820                                           
GAGE_N50                                   35823 COUNT: 49                                  
GAGE_Genome size                           5594470                                          
GAGE_Assembly size                         5493444                                          
GAGE_Chaff bases                           0                                                
GAGE_Missing reference bases               63967(1.14%)                                     
GAGE_Missing assembly bases                6101(0.11%)                                      
GAGE_Missing assembly contigs              20(1.63%)                                        
GAGE_Duplicated reference bases            158589                                           
GAGE_Compressed reference bases            243837                                           
GAGE_Bad trim                              731                                              
GAGE_Avg idy                               99.98                                            
GAGE_SNPs                                  90                                               
GAGE_Indels < 5bp                          439                                              
GAGE_Indels >= 5                           5                                                
GAGE_Inversions                            1                                                
GAGE_Relocation                            6                                                
GAGE_Translocation                         0                                                
GAGE_Corrected contig #                    709                                              
GAGE_Corrected assembly size               5331880                                          
GAGE_Min correct contig                    200                                              
GAGE_Max correct contig                    133825                                           
GAGE_Corrected N50                         35823 COUNT: 49                                  
