All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.450percent_out.unpadded
#Contigs                                   3337                                                                            
#Contigs (>= 0 bp)                         3445                                                                            
#Contigs (>= 1000 bp)                      292                                                                             
Largest contig                             676340                                                                          
Total length                               4874996                                                                         
Total length (>= 0 bp)                     4890281                                                                         
Total length (>= 1000 bp)                  3222858                                                                         
Reference length                           2813862                                                                         
N50                                        45177                                                                           
NG50                                       155807                                                                          
N75                                        704                                                                             
NG75                                       69197                                                                           
L50                                        17                                                                              
LG50                                       4                                                                               
L75                                        829                                                                             
LG75                                       11                                                                              
#local misassemblies                       22                                                                              
#misassemblies                             116                                                                             
#misassembled contigs                      113                                                                             
Misassembled contigs length                492418                                                                          
Misassemblies inter-contig overlap         22815                                                                           
#unaligned contigs                         12 + 59 part                                                                    
Unaligned contigs length                   8362                                                                            
#ambiguously mapped contigs                54                                                                              
Extra bases in ambiguously mapped contigs  -31841                                                                          
#N's                                       1020                                                                            
#N's per 100 kbp                           20.92                                                                           
Genome fraction (%)                        99.974                                                                          
Duplication ratio                          1.727                                                                           
#genes                                     2706 + 20 part                                                                  
#predicted genes (unique)                  7658                                                                            
#predicted genes (>= 0 bp)                 7792                                                                            
#predicted genes (>= 300 bp)               3563                                                                            
#predicted genes (>= 1500 bp)              301                                                                             
#predicted genes (>= 3000 bp)              31                                                                              
#mismatches                                188                                                                             
#indels                                    171                                                                             
Indels length                              224                                                                             
#mismatches per 100 kbp                    6.68                                                                            
#indels per 100 kbp                        6.08                                                                            
GC (%)                                     32.73                                                                           
Reference GC (%)                           32.81                                                                           
Average %IDY                               98.827                                                                          
Largest alignment                          676337                                                                          
NA50                                       45074                                                                           
NGA50                                      144781                                                                          
NA75                                       695                                                                             
NGA75                                      65139                                                                           
LA50                                       18                                                                              
LGA50                                      4                                                                               
LA75                                       847                                                                             
LGA75                                      12                                                                              
#Mis_misassemblies                         116                                                                             
#Mis_relocations                           110                                                                             
#Mis_translocations                        3                                                                               
#Mis_inversions                            3                                                                               
#Mis_misassembled contigs                  113                                                                             
Mis_Misassembled contigs length            492418                                                                          
#Mis_local misassemblies                   22                                                                              
#Mis_short indels (<= 5 bp)                169                                                                             
#Mis_long indels (> 5 bp)                  2                                                                               
#Una_fully unaligned contigs               12                                                                              
Una_Fully unaligned length                 5085                                                                            
#Una_partially unaligned contigs           59                                                                              
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            1                                                                               
Una_Partially unaligned length             3277                                                                            
GAGE_Contigs #                             3337                                                                            
GAGE_Min contig                            202                                                                             
GAGE_Max contig                            676340                                                                          
GAGE_N50                                   155807 COUNT: 4                                                                 
GAGE_Genome size                           2813862                                                                         
GAGE_Assembly size                         4874996                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               287(0.01%)                                                                      
GAGE_Missing assembly bases                10007(0.21%)                                                                    
GAGE_Missing assembly contigs              4(0.12%)                                                                        
GAGE_Duplicated reference bases            2054861                                                                         
GAGE_Compressed reference bases            11710                                                                           
GAGE_Bad trim                              8262                                                                            
GAGE_Avg idy                               99.99                                                                           
GAGE_SNPs                                  55                                                                              
GAGE_Indels < 5bp                          76                                                                              
GAGE_Indels >= 5                           4                                                                               
GAGE_Inversions                            2                                                                               
GAGE_Relocation                            1                                                                               
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    59                                                                              
GAGE_Corrected assembly size               2832269                                                                         
GAGE_Min correct contig                    233                                                                             
GAGE_Max correct contig                    623467                                                                          
GAGE_Corrected N50                         112909 COUNT: 6                                                                 
