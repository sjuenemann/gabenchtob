All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.55percent.result
#Contigs (>= 0 bp)             833                                                                     
#Contigs (>= 1000 bp)          352                                                                     
Total length (>= 0 bp)         5386587                                                                 
Total length (>= 1000 bp)      5226072                                                                 
#Contigs                       833                                                                     
Largest contig                 113408                                                                  
Total length                   5386587                                                                 
Reference length               5594470                                                                 
GC (%)                         50.29                                                                   
Reference GC (%)               50.48                                                                   
N50                            29801                                                                   
NG50                           28682                                                                   
N75                            15427                                                                   
NG75                           12819                                                                   
#misassemblies                 11                                                                      
#local misassemblies           2                                                                       
#unaligned contigs             0 + 19 part                                                             
Unaligned contigs length       640                                                                     
Genome fraction (%)            93.983                                                                  
Duplication ratio              1.008                                                                   
#N's per 100 kbp               0.78                                                                    
#mismatches per 100 kbp        2.00                                                                    
#indels per 100 kbp            25.37                                                                   
#genes                         4773 + 344 part                                                         
#predicted genes (unique)      6190                                                                    
#predicted genes (>= 0 bp)     6193                                                                    
#predicted genes (>= 300 bp)   4793                                                                    
#predicted genes (>= 1500 bp)  538                                                                     
#predicted genes (>= 3000 bp)  38                                                                      
Largest alignment              113408                                                                  
NA50                           29801                                                                   
NGA50                          28682                                                                   
NA75                           14837                                                                   
NGA75                          12425                                                                   
