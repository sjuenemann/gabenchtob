All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.125percent.contigs
#Contigs                                   168                                                                                
#Contigs (>= 0 bp)                         214                                                                                
#Contigs (>= 1000 bp)                      134                                                                                
Largest contig                             86809                                                                              
Total length                               2764872                                                                            
Total length (>= 0 bp)                     2771227                                                                            
Total length (>= 1000 bp)                  2750069                                                                            
Reference length                           2813862                                                                            
N50                                        32881                                                                              
NG50                                       32264                                                                              
N75                                        20481                                                                              
NG75                                       19998                                                                              
L50                                        28                                                                                 
LG50                                       29                                                                                 
L75                                        55                                                                                 
LG75                                       57                                                                                 
#local misassemblies                       8                                                                                  
#misassemblies                             3                                                                                  
#misassembled contigs                      3                                                                                  
Misassembled contigs length                64125                                                                              
Misassemblies inter-contig overlap         1111                                                                               
#unaligned contigs                         1 + 4 part                                                                         
Unaligned contigs length                   394                                                                                
#ambiguously mapped contigs                10                                                                                 
Extra bases in ambiguously mapped contigs  -9174                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        97.780                                                                             
Duplication ratio                          1.002                                                                              
#genes                                     2595 + 87 part                                                                     
#predicted genes (unique)                  2770                                                                               
#predicted genes (>= 0 bp)                 2770                                                                               
#predicted genes (>= 300 bp)               2331                                                                               
#predicted genes (>= 1500 bp)              284                                                                                
#predicted genes (>= 3000 bp)              26                                                                                 
#mismatches                                79                                                                                 
#indels                                    324                                                                                
Indels length                              438                                                                                
#mismatches per 100 kbp                    2.87                                                                               
#indels per 100 kbp                        11.78                                                                              
GC (%)                                     32.64                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               99.371                                                                             
Largest alignment                          86809                                                                              
NA50                                       32881                                                                              
NGA50                                      32264                                                                              
NA75                                       20110                                                                              
NGA75                                      19998                                                                              
LA50                                       28                                                                                 
LGA50                                      29                                                                                 
LA75                                       56                                                                                 
LGA75                                      57                                                                                 
#Mis_misassemblies                         3                                                                                  
#Mis_relocations                           1                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            2                                                                                  
#Mis_misassembled contigs                  3                                                                                  
Mis_Misassembled contigs length            64125                                                                              
#Mis_local misassemblies                   8                                                                                  
#Mis_short indels (<= 5 bp)                321                                                                                
#Mis_long indels (> 5 bp)                  3                                                                                  
#Una_fully unaligned contigs               1                                                                                  
Una_Fully unaligned length                 248                                                                                
#Una_partially unaligned contigs           4                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             146                                                                                
GAGE_Contigs #                             168                                                                                
GAGE_Min contig                            203                                                                                
GAGE_Max contig                            86809                                                                              
GAGE_N50                                   32264 COUNT: 29                                                                    
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2764872                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               16718(0.59%)                                                                       
GAGE_Missing assembly bases                431(0.02%)                                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            967                                                                                
GAGE_Compressed reference bases            38728                                                                              
GAGE_Bad trim                              412                                                                                
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  48                                                                                 
GAGE_Indels < 5bp                          350                                                                                
GAGE_Indels >= 5                           7                                                                                  
GAGE_Inversions                            1                                                                                  
GAGE_Relocation                            2                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    173                                                                                
GAGE_Corrected assembly size               2764901                                                                            
GAGE_Min correct contig                    203                                                                                
GAGE_Max correct contig                    86819                                                                              
GAGE_Corrected N50                         29525 COUNT: 30                                                                    
