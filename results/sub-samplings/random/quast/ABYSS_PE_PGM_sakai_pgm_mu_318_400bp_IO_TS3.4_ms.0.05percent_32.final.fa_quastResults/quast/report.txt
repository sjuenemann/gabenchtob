All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.05percent_32.final
#Contigs (>= 0 bp)             7730                                                                
#Contigs (>= 1000 bp)          1541                                                                
Total length (>= 0 bp)         5284818                                                             
Total length (>= 1000 bp)      4116110                                                             
#Contigs                       3235                                                                
Largest contig                 20083                                                               
Total length                   4954199                                                             
Reference length               5594470                                                             
GC (%)                         50.61                                                               
Reference GC (%)               50.48                                                               
N50                            2664                                                                
NG50                           2279                                                                
N75                            1382                                                                
NG75                           922                                                                 
#misassemblies                 3                                                                   
#local misassemblies           6                                                                   
#unaligned contigs             0 + 1 part                                                          
Unaligned contigs length       206                                                                 
Genome fraction (%)            87.109                                                              
Duplication ratio              1.002                                                               
#N's per 100 kbp               0.00                                                                
#mismatches per 100 kbp        3.20                                                                
#indels per 100 kbp            17.13                                                               
#genes                         2884 + 1939 part                                                    
#predicted genes (unique)      6840                                                                
#predicted genes (>= 0 bp)     6840                                                                
#predicted genes (>= 300 bp)   4817                                                                
#predicted genes (>= 1500 bp)  378                                                                 
#predicted genes (>= 3000 bp)  24                                                                  
Largest alignment              20083                                                               
NA50                           2656                                                                
NGA50                          2266                                                                
NA75                           1370                                                                
NGA75                          883                                                                 
