All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.300percent.K99.final-contigs
#Mis_misassemblies               7                                                                                           
#Mis_relocations                 7                                                                                           
#Mis_translocations              0                                                                                           
#Mis_inversions                  0                                                                                           
#Mis_misassembled contigs        5                                                                                           
Mis_Misassembled contigs length  287447                                                                                      
#Mis_local misassemblies         16                                                                                          
#mismatches                      191                                                                                         
#indels                          512                                                                                         
#Mis_short indels (<= 5 bp)      510                                                                                         
#Mis_long indels (> 5 bp)        2                                                                                           
Indels length                    598                                                                                         
