All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.700percent.K99.final-contigs
#Contigs (>= 0 bp)             386                                                                                         
#Contigs (>= 1000 bp)          59                                                                                          
Total length (>= 0 bp)         2820078                                                                                     
Total length (>= 1000 bp)      2760135                                                                                     
#Contigs                       112                                                                                         
Largest contig                 207875                                                                                      
Total length                   2782579                                                                                     
Reference length               2813862                                                                                     
GC (%)                         32.67                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            88649                                                                                       
NG50                           88649                                                                                       
N75                            48244                                                                                       
NG75                           48244                                                                                       
#misassemblies                 2                                                                                           
#local misassemblies           10                                                                                          
#unaligned contigs             0 + 1 part                                                                                  
Unaligned contigs length       84                                                                                          
Genome fraction (%)            98.438                                                                                      
Duplication ratio              1.002                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        3.94                                                                                        
#indels per 100 kbp            13.75                                                                                       
#genes                         2658 + 45 part                                                                              
#predicted genes (unique)      2737                                                                                        
#predicted genes (>= 0 bp)     2741                                                                                        
#predicted genes (>= 300 bp)   2319                                                                                        
#predicted genes (>= 1500 bp)  285                                                                                         
#predicted genes (>= 3000 bp)  25                                                                                          
Largest alignment              207875                                                                                      
NA50                           88649                                                                                       
NGA50                          88649                                                                                       
NA75                           48243                                                                                       
NGA75                          46836                                                                                       
