All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.5percent.scf
#Mis_misassemblies               2                                                           
#Mis_relocations                 2                                                           
#Mis_translocations              0                                                           
#Mis_inversions                  0                                                           
#Mis_misassembled contigs        2                                                           
Mis_Misassembled contigs length  198880                                                      
#Mis_local misassemblies         7                                                           
#mismatches                      81                                                          
#indels                          244                                                         
#Mis_short indels (<= 5 bp)      244                                                         
#Mis_long indels (> 5 bp)        0                                                           
Indels length                    248                                                         
