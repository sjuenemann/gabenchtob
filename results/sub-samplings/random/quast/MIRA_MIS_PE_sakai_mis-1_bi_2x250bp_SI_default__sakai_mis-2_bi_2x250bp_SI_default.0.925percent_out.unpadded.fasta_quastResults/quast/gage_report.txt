All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.0.925percent_out.unpadded
GAGE_Contigs #                   3576                                                                                                      
GAGE_Min contig                  200                                                                                                       
GAGE_Max contig                  508499                                                                                                    
GAGE_N50                         202918 COUNT: 8                                                                                           
GAGE_Genome size                 5594470                                                                                                   
GAGE_Assembly size               6818918                                                                                                   
GAGE_Chaff bases                 0                                                                                                         
GAGE_Missing reference bases     292(0.01%)                                                                                                
GAGE_Missing assembly bases      11815(0.17%)                                                                                              
GAGE_Missing assembly contigs    6(0.17%)                                                                                                  
GAGE_Duplicated reference bases  1318783                                                                                                   
GAGE_Compressed reference bases  127451                                                                                                    
GAGE_Bad trim                    2613                                                                                                      
GAGE_Avg idy                     99.95                                                                                                     
GAGE_SNPs                        687                                                                                                       
GAGE_Indels < 5bp                53                                                                                                        
GAGE_Indels >= 5                 2                                                                                                         
GAGE_Inversions                  23                                                                                                        
GAGE_Relocation                  11                                                                                                        
GAGE_Translocation               2                                                                                                         
GAGE_Corrected contig #          137                                                                                                       
GAGE_Corrected assembly size     5574362                                                                                                   
GAGE_Min correct contig          248                                                                                                       
GAGE_Max correct contig          464289                                                                                                    
GAGE_Corrected N50               148529 COUNT: 11                                                                                          
