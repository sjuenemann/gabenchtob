All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.175percent_36.final
#Contigs                                   2419                                                                                 
#Contigs (>= 0 bp)                         4554                                                                                 
#Contigs (>= 1000 bp)                      1290                                                                                 
Largest contig                             16023                                                                                
Total length                               4003072                                                                              
Total length (>= 0 bp)                     4168898                                                                              
Total length (>= 1000 bp)                  3404923                                                                              
Reference length                           4411532                                                                              
N50                                        2652                                                                                 
NG50                                       2404                                                                                 
N75                                        1462                                                                                 
NG75                                       1121                                                                                 
L50                                        459                                                                                  
LG50                                       540                                                                                  
L75                                        959                                                                                  
LG75                                       1200                                                                                 
#local misassemblies                       11                                                                                   
#misassemblies                             0                                                                                    
#misassembled contigs                      0                                                                                    
Misassembled contigs length                0                                                                                    
Misassemblies inter-contig overlap         980                                                                                  
#unaligned contigs                         2 + 3 part                                                                           
Unaligned contigs length                   864                                                                                  
#ambiguously mapped contigs                30                                                                                   
Extra bases in ambiguously mapped contigs  -14090                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        90.325                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     2339 + 1651 part                                                                     
#predicted genes (unique)                  5940                                                                                 
#predicted genes (>= 0 bp)                 5940                                                                                 
#predicted genes (>= 300 bp)               4214                                                                                 
#predicted genes (>= 1500 bp)              226                                                                                  
#predicted genes (>= 3000 bp)              10                                                                                   
#mismatches                                127                                                                                  
#indels                                    1254                                                                                 
Indels length                              1396                                                                                 
#mismatches per 100 kbp                    3.19                                                                                 
#indels per 100 kbp                        31.47                                                                                
GC (%)                                     64.98                                                                                
Reference GC (%)                           65.61                                                                                
Average %IDY                               99.949                                                                               
Largest alignment                          16023                                                                                
NA50                                       2652                                                                                 
NGA50                                      2404                                                                                 
NA75                                       1462                                                                                 
NGA75                                      1121                                                                                 
LA50                                       459                                                                                  
LGA50                                      540                                                                                  
LA75                                       959                                                                                  
LGA75                                      1200                                                                                 
#Mis_misassemblies                         0                                                                                    
#Mis_relocations                           0                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  0                                                                                    
Mis_Misassembled contigs length            0                                                                                    
#Mis_local misassemblies                   11                                                                                   
#Mis_short indels (<= 5 bp)                1251                                                                                 
#Mis_long indels (> 5 bp)                  3                                                                                    
#Una_fully unaligned contigs               2                                                                                    
Una_Fully unaligned length                 763                                                                                  
#Una_partially unaligned contigs           3                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             101                                                                                  
GAGE_Contigs #                             2419                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            16023                                                                                
GAGE_N50                                   2404 COUNT: 540                                                                      
GAGE_Genome size                           4411532                                                                              
GAGE_Assembly size                         4003072                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               386770(8.77%)                                                                        
GAGE_Missing assembly bases                890(0.02%)                                                                           
GAGE_Missing assembly contigs              2(0.08%)                                                                             
GAGE_Duplicated reference bases            0                                                                                    
GAGE_Compressed reference bases            28021                                                                                
GAGE_Bad trim                              127                                                                                  
GAGE_Avg idy                               99.96                                                                                
GAGE_SNPs                                  109                                                                                  
GAGE_Indels < 5bp                          1282                                                                                 
GAGE_Indels >= 5                           9                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            5                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    2428                                                                                 
GAGE_Corrected assembly size               4003802                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    16027                                                                                
GAGE_Corrected N50                         2396 COUNT: 544                                                                      
