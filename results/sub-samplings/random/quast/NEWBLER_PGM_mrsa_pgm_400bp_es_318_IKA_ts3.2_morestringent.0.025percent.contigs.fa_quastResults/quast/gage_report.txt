All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.025percent.contigs
GAGE_Contigs #                   185                                                                           
GAGE_Min contig                  205                                                                           
GAGE_Max contig                  90969                                                                         
GAGE_N50                         32089 COUNT: 32                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2763605                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     12701(0.45%)                                                                  
GAGE_Missing assembly bases      17(0.00%)                                                                     
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  425                                                                           
GAGE_Compressed reference bases  39488                                                                         
GAGE_Bad trim                    17                                                                            
GAGE_Avg idy                     99.96                                                                         
GAGE_SNPs                        66                                                                            
GAGE_Indels < 5bp                815                                                                           
GAGE_Indels >= 5                 7                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  4                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          192                                                                           
GAGE_Corrected assembly size     2765287                                                                       
GAGE_Min correct contig          204                                                                           
GAGE_Max correct contig          76611                                                                         
GAGE_Corrected N50               29129 COUNT: 33                                                               
