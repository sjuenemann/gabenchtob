All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.800percent_40.final
GAGE_Contigs #                   1847                                                                                 
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  22456                                                                                
GAGE_N50                         3698 COUNT: 373                                                                      
GAGE_Genome size                 4411532                                                                              
GAGE_Assembly size               4186046                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     212857(4.83%)                                                                        
GAGE_Missing assembly bases      867(0.02%)                                                                           
GAGE_Missing assembly contigs    2(0.11%)                                                                             
GAGE_Duplicated reference bases  0                                                                                    
GAGE_Compressed reference bases  18453                                                                                
GAGE_Bad trim                    53                                                                                   
GAGE_Avg idy                     99.98                                                                                
GAGE_SNPs                        89                                                                                   
GAGE_Indels < 5bp                697                                                                                  
GAGE_Indels >= 5                 6                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  2                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          1852                                                                                 
GAGE_Corrected assembly size     4186442                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          22459                                                                                
GAGE_Corrected N50               3698 COUNT: 373                                                                      
