All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.175percent.contigs
#Contigs (>= 0 bp)             112                                                                           
#Contigs (>= 1000 bp)          37                                                                            
Total length (>= 0 bp)         2784643                                                                       
Total length (>= 1000 bp)      2767487                                                                       
#Contigs                       66                                                                            
Largest contig                 597065                                                                        
Total length                   2777705                                                                       
Reference length               2813862                                                                       
GC (%)                         32.66                                                                         
Reference GC (%)               32.81                                                                         
N50                            170007                                                                        
NG50                           170007                                                                        
N75                            97599                                                                         
NG75                           97599                                                                         
#misassemblies                 3                                                                             
#local misassemblies           7                                                                             
#unaligned contigs             1 + 0 part                                                                    
Unaligned contigs length       258                                                                           
Genome fraction (%)            98.277                                                                        
Duplication ratio              1.001                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        2.86                                                                          
#indels per 100 kbp            8.06                                                                          
#genes                         2673 + 25 part                                                                
#predicted genes (unique)      2692                                                                          
#predicted genes (>= 0 bp)     2694                                                                          
#predicted genes (>= 300 bp)   2325                                                                          
#predicted genes (>= 1500 bp)  287                                                                           
#predicted genes (>= 3000 bp)  28                                                                            
Largest alignment              597065                                                                        
NA50                           170007                                                                        
NGA50                          170007                                                                        
NA75                           81303                                                                         
NGA75                          81303                                                                         
