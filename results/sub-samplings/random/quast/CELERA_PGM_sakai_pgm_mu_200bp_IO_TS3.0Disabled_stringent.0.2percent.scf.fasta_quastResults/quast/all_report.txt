All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.2percent.scf
#Contigs                                   466                                                                    
#Contigs (>= 0 bp)                         466                                                                    
#Contigs (>= 1000 bp)                      466                                                                    
Largest contig                             92931                                                                  
Total length                               5257113                                                                
Total length (>= 0 bp)                     5257113                                                                
Total length (>= 1000 bp)                  5257113                                                                
Reference length                           5594470                                                                
N50                                        25412                                                                  
NG50                                       22918                                                                  
N75                                        11712                                                                  
NG75                                       8997                                                                   
L50                                        60                                                                     
LG50                                       68                                                                     
L75                                        138                                                                    
LG75                                       162                                                                    
#local misassemblies                       7                                                                      
#misassemblies                             5                                                                      
#misassembled contigs                      5                                                                      
Misassembled contigs length                47574                                                                  
Misassemblies inter-contig overlap         3563                                                                   
#unaligned contigs                         0 + 2 part                                                             
Unaligned contigs length                   82                                                                     
#ambiguously mapped contigs                0                                                                      
Extra bases in ambiguously mapped contigs  0                                                                      
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        93.044                                                                 
Duplication ratio                          1.011                                                                  
#genes                                     4693 + 390 part                                                        
#predicted genes (unique)                  5816                                                                   
#predicted genes (>= 0 bp)                 5817                                                                   
#predicted genes (>= 300 bp)               4705                                                                   
#predicted genes (>= 1500 bp)              523                                                                    
#predicted genes (>= 3000 bp)              37                                                                     
#mismatches                                264                                                                    
#indels                                    1383                                                                   
Indels length                              1442                                                                   
#mismatches per 100 kbp                    5.07                                                                   
#indels per 100 kbp                        26.57                                                                  
GC (%)                                     50.21                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.075                                                                 
Largest alignment                          92931                                                                  
NA50                                       25057                                                                  
NGA50                                      22764                                                                  
NA75                                       11712                                                                  
NGA75                                      8887                                                                   
LA50                                       61                                                                     
LGA50                                      68                                                                     
LA75                                       139                                                                    
LGA75                                      164                                                                    
#Mis_misassemblies                         5                                                                      
#Mis_relocations                           5                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  5                                                                      
Mis_Misassembled contigs length            47574                                                                  
#Mis_local misassemblies                   7                                                                      
#Mis_short indels (<= 5 bp)                1380                                                                   
#Mis_long indels (> 5 bp)                  3                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           2                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             82                                                                     
GAGE_Contigs #                             466                                                                    
GAGE_Min contig                            1001                                                                   
GAGE_Max contig                            92931                                                                  
GAGE_N50                                   22918 COUNT: 68                                                        
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5257113                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               330116(5.90%)                                                          
GAGE_Missing assembly bases                526(0.01%)                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            139                                                                    
GAGE_Compressed reference bases            83505                                                                  
GAGE_Bad trim                              524                                                                    
GAGE_Avg idy                               99.96                                                                  
GAGE_SNPs                                  124                                                                    
GAGE_Indels < 5bp                          1291                                                                   
GAGE_Indels >= 5                           10                                                                     
GAGE_Inversions                            2                                                                      
GAGE_Relocation                            4                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    482                                                                    
GAGE_Corrected assembly size               5261877                                                                
GAGE_Min correct contig                    210                                                                    
GAGE_Max correct contig                    92960                                                                  
GAGE_Corrected N50                         21918 COUNT: 69                                                        
