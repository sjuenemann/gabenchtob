All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.650percent_58.final
#Contigs (>= 0 bp)             5945                                                                                 
#Contigs (>= 1000 bp)          1160                                                                                 
Total length (>= 0 bp)         4614804                                                                              
Total length (>= 1000 bp)      3847531                                                                              
#Contigs                       1790                                                                                 
Largest contig                 19594                                                                                
Total length                   4184543                                                                              
Reference length               4411532                                                                              
GC (%)                         65.27                                                                                
Reference GC (%)               65.61                                                                                
N50                            3946                                                                                 
NG50                           3776                                                                                 
N75                            2178                                                                                 
NG75                           1919                                                                                 
#misassemblies                 1                                                                                    
#local misassemblies           13                                                                                   
#unaligned contigs             1 + 0 part                                                                           
Unaligned contigs length       839                                                                                  
Genome fraction (%)            94.448                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        3.02                                                                                 
#indels per 100 kbp            18.91                                                                                
#genes                         2758 + 1259 part                                                                     
#predicted genes (unique)      5425                                                                                 
#predicted genes (>= 0 bp)     5427                                                                                 
#predicted genes (>= 300 bp)   4071                                                                                 
#predicted genes (>= 1500 bp)  332                                                                                  
#predicted genes (>= 3000 bp)  16                                                                                   
Largest alignment              19594                                                                                
NA50                           3946                                                                                 
NGA50                          3776                                                                                 
NA75                           2178                                                                                 
NGA75                          1919                                                                                 
