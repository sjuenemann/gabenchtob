All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.650percent_out.unpadded
GAGE_Contigs #                   271                                                                                               
GAGE_Min contig                  200                                                                                               
GAGE_Max contig                  354572                                                                                            
GAGE_N50                         184369 COUNT: 6                                                                                   
GAGE_Genome size                 2813862                                                                                           
GAGE_Assembly size               2923279                                                                                           
GAGE_Chaff bases                 0                                                                                                 
GAGE_Missing reference bases     888(0.03%)                                                                                        
GAGE_Missing assembly bases      6326(0.22%)                                                                                       
GAGE_Missing assembly contigs    3(1.11%)                                                                                          
GAGE_Duplicated reference bases  117371                                                                                            
GAGE_Compressed reference bases  5507                                                                                              
GAGE_Bad trim                    163                                                                                               
GAGE_Avg idy                     99.99                                                                                             
GAGE_SNPs                        45                                                                                                
GAGE_Indels < 5bp                9                                                                                                 
GAGE_Indels >= 5                 2                                                                                                 
GAGE_Inversions                  7                                                                                                 
GAGE_Relocation                  3                                                                                                 
GAGE_Translocation               0                                                                                                 
GAGE_Corrected contig #          62                                                                                                
GAGE_Corrected assembly size     2832200                                                                                           
GAGE_Min correct contig          219                                                                                               
GAGE_Max correct contig          337020                                                                                            
GAGE_Corrected N50               150970 COUNT: 7                                                                                   
