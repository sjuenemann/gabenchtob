All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.5percent.contigs
#Contigs                                   348                                                                         
#Contigs (>= 0 bp)                         447                                                                         
#Contigs (>= 1000 bp)                      160                                                                         
Largest contig                             361499                                                                      
Total length                               5318828                                                                     
Total length (>= 0 bp)                     5333083                                                                     
Total length (>= 1000 bp)                  5235374                                                                     
Reference length                           5594470                                                                     
N50                                        128871                                                                      
NG50                                       124136                                                                      
N75                                        46442                                                                       
NG75                                       40860                                                                       
L50                                        16                                                                          
LG50                                       17                                                                          
L75                                        32                                                                          
LG75                                       37                                                                          
#local misassemblies                       5                                                                           
#misassemblies                             6                                                                           
#misassembled contigs                      6                                                                           
Misassembled contigs length                196115                                                                      
Misassemblies inter-contig overlap         979                                                                         
#unaligned contigs                         0 + 2 part                                                                  
Unaligned contigs length                   133                                                                         
#ambiguously mapped contigs                116                                                                         
Extra bases in ambiguously mapped contigs  -86097                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        93.545                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     4915 + 174 part                                                             
#predicted genes (unique)                  5716                                                                        
#predicted genes (>= 0 bp)                 5716                                                                        
#predicted genes (>= 300 bp)               4660                                                                        
#predicted genes (>= 1500 bp)              571                                                                         
#predicted genes (>= 3000 bp)              51                                                                          
#mismatches                                88                                                                          
#indels                                    1025                                                                        
Indels length                              1081                                                                        
#mismatches per 100 kbp                    1.68                                                                        
#indels per 100 kbp                        19.59                                                                       
GC (%)                                     50.27                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               98.954                                                                      
Largest alignment                          361499                                                                      
NA50                                       124136                                                                      
NGA50                                      113368                                                                      
NA75                                       46442                                                                       
NGA75                                      40860                                                                       
LA50                                       16                                                                          
LGA50                                      17                                                                          
LA75                                       33                                                                          
LGA75                                      38                                                                          
#Mis_misassemblies                         6                                                                           
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            3                                                                           
#Mis_misassembled contigs                  6                                                                           
Mis_Misassembled contigs length            196115                                                                      
#Mis_local misassemblies                   5                                                                           
#Mis_short indels (<= 5 bp)                1025                                                                        
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           2                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             133                                                                         
GAGE_Contigs #                             348                                                                         
GAGE_Min contig                            204                                                                         
GAGE_Max contig                            361499                                                                      
GAGE_N50                                   124136 COUNT: 17                                                            
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         5318828                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               64594(1.15%)                                                                
GAGE_Missing assembly bases                59(0.00%)                                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            773                                                                         
GAGE_Compressed reference bases            223073                                                                      
GAGE_Bad trim                              59                                                                          
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  97                                                                          
GAGE_Indels < 5bp                          1103                                                                        
GAGE_Indels >= 5                           5                                                                           
GAGE_Inversions                            1                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    349                                                                         
GAGE_Corrected assembly size               5319180                                                                     
GAGE_Min correct contig                    206                                                                         
GAGE_Max correct contig                    217342                                                                      
GAGE_Corrected N50                         99570 COUNT: 18                                                             
