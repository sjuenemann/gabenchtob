All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.5percent.contigs
#Contigs (>= 0 bp)             447                                                                         
#Contigs (>= 1000 bp)          160                                                                         
Total length (>= 0 bp)         5333083                                                                     
Total length (>= 1000 bp)      5235374                                                                     
#Contigs                       348                                                                         
Largest contig                 361499                                                                      
Total length                   5318828                                                                     
Reference length               5594470                                                                     
GC (%)                         50.27                                                                       
Reference GC (%)               50.48                                                                       
N50                            128871                                                                      
NG50                           124136                                                                      
N75                            46442                                                                       
NG75                           40860                                                                       
#misassemblies                 6                                                                           
#local misassemblies           5                                                                           
#unaligned contigs             0 + 2 part                                                                  
Unaligned contigs length       133                                                                         
Genome fraction (%)            93.545                                                                      
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        1.68                                                                        
#indels per 100 kbp            19.59                                                                       
#genes                         4915 + 174 part                                                             
#predicted genes (unique)      5716                                                                        
#predicted genes (>= 0 bp)     5716                                                                        
#predicted genes (>= 300 bp)   4660                                                                        
#predicted genes (>= 1500 bp)  571                                                                         
#predicted genes (>= 3000 bp)  51                                                                          
Largest alignment              361499                                                                      
NA50                           124136                                                                      
NGA50                          113368                                                                      
NA75                           46442                                                                       
NGA75                          40860                                                                       
