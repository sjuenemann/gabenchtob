All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.175percent_out.unpadded
#Mis_misassemblies               16                                                                                   
#Mis_relocations                 16                                                                                   
#Mis_translocations              0                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        15                                                                                   
Mis_Misassembled contigs length  160728                                                                               
#Mis_local misassemblies         20                                                                                   
#mismatches                      710                                                                                  
#indels                          1684                                                                                 
#Mis_short indels (<= 5 bp)      1674                                                                                 
#Mis_long indels (> 5 bp)        10                                                                                   
Indels length                    1997                                                                                 
