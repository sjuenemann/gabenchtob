All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.15percent.K99.final-contigs
#Mis_misassemblies               14                                                                                    
#Mis_relocations                 13                                                                                    
#Mis_translocations              0                                                                                     
#Mis_inversions                  1                                                                                     
#Mis_misassembled contigs        10                                                                                    
Mis_Misassembled contigs length  321812                                                                                
#Mis_local misassemblies         12                                                                                    
#mismatches                      1339                                                                                  
#indels                          2674                                                                                  
#Mis_short indels (<= 5 bp)      2671                                                                                  
#Mis_long indels (> 5 bp)        3                                                                                     
Indels length                    2827                                                                                  
