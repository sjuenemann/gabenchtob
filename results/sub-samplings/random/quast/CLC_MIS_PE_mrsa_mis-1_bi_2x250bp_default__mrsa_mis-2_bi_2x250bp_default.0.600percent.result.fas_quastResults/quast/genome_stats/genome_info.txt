reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  CLC_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.600percent.result_broken  | 98.4628954796       | 1.00041362732     | 27          | 2684      | 18        | None      | None      |
  CLC_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.600percent.result  | 98.4677642329       | 1.00052801645     | 26          | 2685      | 17        | None      | None      |
