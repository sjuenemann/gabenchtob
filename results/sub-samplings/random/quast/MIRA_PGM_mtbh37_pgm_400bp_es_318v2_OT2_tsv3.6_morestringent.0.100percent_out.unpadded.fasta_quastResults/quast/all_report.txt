All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.100percent_out.unpadded
#Contigs                                   2289                                                                                 
#Contigs (>= 0 bp)                         2312                                                                                 
#Contigs (>= 1000 bp)                      1170                                                                                 
Largest contig                             20030                                                                                
Total length                               4324434                                                                              
Total length (>= 0 bp)                     4328377                                                                              
Total length (>= 1000 bp)                  3763323                                                                              
Reference length                           4411532                                                                              
N50                                        3462                                                                                 
NG50                                       3404                                                                                 
N75                                        1802                                                                                 
NG75                                       1709                                                                                 
L50                                        370                                                                                  
LG50                                       382                                                                                  
L75                                        793                                                                                  
LG75                                       830                                                                                  
#local misassemblies                       12                                                                                   
#misassemblies                             29                                                                                   
#misassembled contigs                      28                                                                                   
Misassembled contigs length                124498                                                                               
Misassemblies inter-contig overlap         16160                                                                                
#unaligned contigs                         3 + 5 part                                                                           
Unaligned contigs length                   1380                                                                                 
#ambiguously mapped contigs                10                                                                                   
Extra bases in ambiguously mapped contigs  -3444                                                                                
#N's                                       204                                                                                  
#N's per 100 kbp                           4.72                                                                                 
Genome fraction (%)                        92.679                                                                               
Duplication ratio                          1.060                                                                                
#genes                                     2663 + 1414 part                                                                     
#predicted genes (unique)                  6280                                                                                 
#predicted genes (>= 0 bp)                 6291                                                                                 
#predicted genes (>= 300 bp)               4362                                                                                 
#predicted genes (>= 1500 bp)              258                                                                                  
#predicted genes (>= 3000 bp)              14                                                                                   
#mismatches                                803                                                                                  
#indels                                    2814                                                                                 
Indels length                              3086                                                                                 
#mismatches per 100 kbp                    19.64                                                                                
#indels per 100 kbp                        68.83                                                                                
GC (%)                                     64.87                                                                                
Reference GC (%)                           65.61                                                                                
Average %IDY                               99.816                                                                               
Largest alignment                          20030                                                                                
NA50                                       3413                                                                                 
NGA50                                      3338                                                                                 
NA75                                       1783                                                                                 
NGA75                                      1689                                                                                 
LA50                                       375                                                                                  
LGA50                                      388                                                                                  
LA75                                       806                                                                                  
LGA75                                      844                                                                                  
#Mis_misassemblies                         29                                                                                   
#Mis_relocations                           29                                                                                   
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  28                                                                                   
Mis_Misassembled contigs length            124498                                                                               
#Mis_local misassemblies                   12                                                                                   
#Mis_short indels (<= 5 bp)                2808                                                                                 
#Mis_long indels (> 5 bp)                  6                                                                                    
#Una_fully unaligned contigs               3                                                                                    
Una_Fully unaligned length                 1000                                                                                 
#Una_partially unaligned contigs           5                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             380                                                                                  
GAGE_Contigs #                             2289                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            20030                                                                                
GAGE_N50                                   3404 COUNT: 382                                                                      
GAGE_Genome size                           4411532                                                                              
GAGE_Assembly size                         4324434                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               301454(6.83%)                                                                        
GAGE_Missing assembly bases                2237(0.05%)                                                                          
GAGE_Missing assembly contigs              2(0.09%)                                                                             
GAGE_Duplicated reference bases            202640                                                                               
GAGE_Compressed reference bases            41996                                                                                
GAGE_Bad trim                              1609                                                                                 
GAGE_Avg idy                               99.90                                                                                
GAGE_SNPs                                  283                                                                                  
GAGE_Indels < 5bp                          2771                                                                                 
GAGE_Indels >= 5                           7                                                                                    
GAGE_Inversions                            11                                                                                   
GAGE_Relocation                            10                                                                                   
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    1822                                                                                 
GAGE_Corrected assembly size               4132191                                                                              
GAGE_Min correct contig                    201                                                                                  
GAGE_Max correct contig                    20030                                                                                
GAGE_Corrected N50                         3329 COUNT: 389                                                                      
