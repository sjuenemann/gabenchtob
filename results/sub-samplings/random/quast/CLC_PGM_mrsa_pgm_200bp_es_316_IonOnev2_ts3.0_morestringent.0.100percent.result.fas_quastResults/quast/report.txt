All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.100percent.result
#Contigs (>= 0 bp)             604                                                                           
#Contigs (>= 1000 bp)          299                                                                           
Total length (>= 0 bp)         2732572                                                                       
Total length (>= 1000 bp)      2631590                                                                       
#Contigs                       604                                                                           
Largest contig                 43466                                                                         
Total length                   2732572                                                                       
Reference length               2813862                                                                       
GC (%)                         32.65                                                                         
Reference GC (%)               32.81                                                                         
N50                            13875                                                                         
NG50                           13599                                                                         
N75                            7328                                                                          
NG75                           6388                                                                          
#misassemblies                 22                                                                            
#local misassemblies           1                                                                             
#unaligned contigs             1 + 21 part                                                                   
Unaligned contigs length       1371                                                                          
Genome fraction (%)            95.771                                                                        
Duplication ratio              1.011                                                                         
#N's per 100 kbp               13.87                                                                         
#mismatches per 100 kbp        3.45                                                                          
#indels per 100 kbp            31.32                                                                         
#genes                         2378 + 265 part                                                               
#predicted genes (unique)      3171                                                                          
#predicted genes (>= 0 bp)     3172                                                                          
#predicted genes (>= 300 bp)   2404                                                                          
#predicted genes (>= 1500 bp)  239                                                                           
#predicted genes (>= 3000 bp)  17                                                                            
Largest alignment              43466                                                                         
NA50                           13875                                                                         
NGA50                          13599                                                                         
NA75                           7071                                                                          
NGA75                          6302                                                                          
