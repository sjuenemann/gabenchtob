All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.125percent.result
#Contigs                                   4011                                                                       
#Contigs (>= 0 bp)                         4011                                                                       
#Contigs (>= 1000 bp)                      129                                                                        
Largest contig                             2017                                                                       
Total length                               1987277                                                                    
Total length (>= 0 bp)                     1987277                                                                    
Total length (>= 1000 bp)                  159874                                                                     
Reference length                           4411532                                                                    
N50                                        518                                                                        
NG50                                       None                                                                       
N75                                        400                                                                        
NG75                                       None                                                                       
L50                                        1373                                                                       
LG50                                       None                                                                       
L75                                        2470                                                                       
LG75                                       None                                                                       
#local misassemblies                       67                                                                         
#misassemblies                             1106                                                                       
#misassembled contigs                      1006                                                                       
Misassembled contigs length                592452                                                                     
Misassemblies inter-contig overlap         11085                                                                      
#unaligned contigs                         17 + 577 part                                                              
Unaligned contigs length                   40122                                                                      
#ambiguously mapped contigs                9                                                                          
Extra bases in ambiguously mapped contigs  -2716                                                                      
#N's                                       336                                                                        
#N's per 100 kbp                           16.91                                                                      
Genome fraction (%)                        41.010                                                                     
Duplication ratio                          1.081                                                                      
#genes                                     202 + 2799 part                                                            
#predicted genes (unique)                  5279                                                                       
#predicted genes (>= 0 bp)                 5284                                                                       
#predicted genes (>= 300 bp)               1899                                                                       
#predicted genes (>= 1500 bp)              0                                                                          
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                2065                                                                       
#indels                                    6622                                                                       
Indels length                              6955                                                                       
#mismatches per 100 kbp                    114.14                                                                     
#indels per 100 kbp                        366.03                                                                     
GC (%)                                     63.57                                                                      
Reference GC (%)                           65.61                                                                      
Average %IDY                               99.469                                                                     
Largest alignment                          2017                                                                       
NA50                                       436                                                                        
NGA50                                      None                                                                       
NA75                                       316                                                                        
NGA75                                      None                                                                       
LA50                                       1591                                                                       
LGA50                                      None                                                                       
LA75                                       2919                                                                       
LGA75                                      None                                                                       
#Mis_misassemblies                         1106                                                                       
#Mis_relocations                           263                                                                        
#Mis_translocations                        0                                                                          
#Mis_inversions                            843                                                                        
#Mis_misassembled contigs                  1006                                                                       
Mis_Misassembled contigs length            592452                                                                     
#Mis_local misassemblies                   67                                                                         
#Mis_short indels (<= 5 bp)                6612                                                                       
#Mis_long indels (> 5 bp)                  10                                                                         
#Una_fully unaligned contigs               17                                                                         
Una_Fully unaligned length                 8202                                                                       
#Una_partially unaligned contigs           577                                                                        
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            8                                                                          
Una_Partially unaligned length             31920                                                                      
GAGE_Contigs #                             4011                                                                       
GAGE_Min contig                            202                                                                        
GAGE_Max contig                            2017                                                                       
GAGE_N50                                   0 COUNT: 0                                                                 
GAGE_Genome size                           4411532                                                                    
GAGE_Assembly size                         1987277                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               2553520(57.88%)                                                            
GAGE_Missing assembly bases                43803(2.20%)                                                               
GAGE_Missing assembly contigs              10(0.25%)                                                                  
GAGE_Duplicated reference bases            79478                                                                      
GAGE_Compressed reference bases            39621                                                                      
GAGE_Bad trim                              36267                                                                      
GAGE_Avg idy                               99.50                                                                      
GAGE_SNPs                                  1599                                                                       
GAGE_Indels < 5bp                          6494                                                                       
GAGE_Indels >= 5                           28                                                                         
GAGE_Inversions                            601                                                                        
GAGE_Relocation                            67                                                                         
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    4074                                                                       
GAGE_Corrected assembly size               1794377                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    2021                                                                       
GAGE_Corrected N50                         0 COUNT: 0                                                                 
