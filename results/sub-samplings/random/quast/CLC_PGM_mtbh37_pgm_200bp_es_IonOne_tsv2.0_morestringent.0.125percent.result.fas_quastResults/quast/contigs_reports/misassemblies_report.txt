All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.125percent.result
#Mis_misassemblies               1106                                                                       
#Mis_relocations                 263                                                                        
#Mis_translocations              0                                                                          
#Mis_inversions                  843                                                                        
#Mis_misassembled contigs        1006                                                                       
Mis_Misassembled contigs length  592452                                                                     
#Mis_local misassemblies         67                                                                         
#mismatches                      2065                                                                       
#indels                          6622                                                                       
#Mis_short indels (<= 5 bp)      6612                                                                       
#Mis_long indels (> 5 bp)        10                                                                         
Indels length                    6955                                                                       
