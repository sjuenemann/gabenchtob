All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.55percent.contigs
#Contigs (>= 0 bp)             565                                                               
#Contigs (>= 1000 bp)          143                                                               
Total length (>= 0 bp)         5390919                                                           
Total length (>= 1000 bp)      5285024                                                           
#Contigs                       287                                                               
Largest contig                 317780                                                            
Total length                   5351546                                                           
Reference length               5594470                                                           
GC (%)                         50.29                                                             
Reference GC (%)               50.48                                                             
N50                            134937                                                            
NG50                           134937                                                            
N75                            56461                                                             
NG75                           49547                                                             
#misassemblies                 7                                                                 
#local misassemblies           8                                                                 
#unaligned contigs             0 + 1 part                                                        
Unaligned contigs length       30                                                                
Genome fraction (%)            94.077                                                            
Duplication ratio              1.001                                                             
#N's per 100 kbp               0.00                                                              
#mismatches per 100 kbp        2.01                                                              
#indels per 100 kbp            3.33                                                              
#genes                         4981 + 142 part                                                   
#predicted genes (unique)      5324                                                              
#predicted genes (>= 0 bp)     5327                                                              
#predicted genes (>= 300 bp)   4481                                                              
#predicted genes (>= 1500 bp)  660                                                               
#predicted genes (>= 3000 bp)  65                                                                
Largest alignment              317780                                                            
NA50                           134033                                                            
NGA50                          124167                                                            
NA75                           56461                                                             
NGA75                          49547                                                             
