All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.850percent.contigs
#Contigs                                   149                                                                           
#Contigs (>= 0 bp)                         900                                                                           
#Contigs (>= 1000 bp)                      73                                                                            
Largest contig                             186577                                                                        
Total length                               2775562                                                                       
Total length (>= 0 bp)                     2885739                                                                       
Total length (>= 1000 bp)                  2750560                                                                       
Reference length                           2813862                                                                       
N50                                        73305                                                                         
NG50                                       73275                                                                         
N75                                        35334                                                                         
NG75                                       35334                                                                         
L50                                        13                                                                            
LG50                                       14                                                                            
L75                                        27                                                                            
LG75                                       27                                                                            
#local misassemblies                       1                                                                             
#misassemblies                             11                                                                            
#misassembled contigs                      11                                                                            
Misassembled contigs length                36680                                                                         
Misassemblies inter-contig overlap         195                                                                           
#unaligned contigs                         0 + 1 part                                                                    
Unaligned contigs length                   22                                                                            
#ambiguously mapped contigs                13                                                                            
Extra bases in ambiguously mapped contigs  -8905                                                                         
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        98.018                                                                        
Duplication ratio                          1.003                                                                         
#genes                                     2643 + 53 part                                                                
#predicted genes (unique)                  2706                                                                          
#predicted genes (>= 0 bp)                 2708                                                                          
#predicted genes (>= 300 bp)               2300                                                                          
#predicted genes (>= 1500 bp)              292                                                                           
#predicted genes (>= 3000 bp)              28                                                                            
#mismatches                                52                                                                            
#indels                                    126                                                                           
Indels length                              184                                                                           
#mismatches per 100 kbp                    1.89                                                                          
#indels per 100 kbp                        4.57                                                                          
GC (%)                                     32.65                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               98.933                                                                        
Largest alignment                          186577                                                                        
NA50                                       73305                                                                         
NGA50                                      73275                                                                         
NA75                                       35334                                                                         
NGA75                                      35334                                                                         
LA50                                       13                                                                            
LGA50                                      14                                                                            
LA75                                       27                                                                            
LGA75                                      27                                                                            
#Mis_misassemblies                         11                                                                            
#Mis_relocations                           11                                                                            
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  11                                                                            
Mis_Misassembled contigs length            36680                                                                         
#Mis_local misassemblies                   1                                                                             
#Mis_short indels (<= 5 bp)                125                                                                           
#Mis_long indels (> 5 bp)                  1                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           1                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             22                                                                            
GAGE_Contigs #                             149                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            186577                                                                        
GAGE_N50                                   73275 COUNT: 14                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2775562                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               10347(0.37%)                                                                  
GAGE_Missing assembly bases                27(0.00%)                                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            8715                                                                          
GAGE_Compressed reference bases            40722                                                                         
GAGE_Bad trim                              27                                                                            
GAGE_Avg idy                               99.99                                                                         
GAGE_SNPs                                  44                                                                            
GAGE_Indels < 5bp                          132                                                                           
GAGE_Indels >= 5                           2                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            2                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    117                                                                           
GAGE_Corrected assembly size               2766938                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    149983                                                                        
GAGE_Corrected N50                         73278 COUNT: 15                                                               
