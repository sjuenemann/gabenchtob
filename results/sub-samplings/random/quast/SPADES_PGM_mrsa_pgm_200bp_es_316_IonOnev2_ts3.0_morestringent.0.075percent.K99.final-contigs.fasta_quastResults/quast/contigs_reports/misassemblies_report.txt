All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.075percent.K99.final-contigs
#Mis_misassemblies               26                                                                                          
#Mis_relocations                 21                                                                                          
#Mis_translocations              0                                                                                           
#Mis_inversions                  5                                                                                           
#Mis_misassembled contigs        20                                                                                          
Mis_Misassembled contigs length  506690                                                                                      
#Mis_local misassemblies         15                                                                                          
#mismatches                      602                                                                                         
#indels                          1610                                                                                        
#Mis_short indels (<= 5 bp)      1609                                                                                        
#Mis_long indels (> 5 bp)        1                                                                                           
Indels length                    1669                                                                                        
