All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent_32.final
#Contigs (>= 0 bp)             67293                                                              
#Contigs (>= 1000 bp)          280                                                                
Total length (>= 0 bp)         5505875                                                            
Total length (>= 1000 bp)      378276                                                             
#Contigs                       4419                                                               
Largest contig                 3706                                                               
Total length                   2087843                                                            
Reference length               2813862                                                            
GC (%)                         32.69                                                              
Reference GC (%)               32.81                                                              
N50                            537                                                                
NG50                           398                                                                
N75                            346                                                                
NG75                           None                                                               
#misassemblies                 1                                                                  
#local misassemblies           0                                                                  
#unaligned contigs             0 + 0 part                                                         
Unaligned contigs length       0                                                                  
Genome fraction (%)            74.084                                                             
Duplication ratio              1.001                                                              
#N's per 100 kbp               0.00                                                               
#mismatches per 100 kbp        1.15                                                               
#indels per 100 kbp            1.15                                                               
#genes                         337 + 2085 part                                                    
#predicted genes (unique)      4975                                                               
#predicted genes (>= 0 bp)     4975                                                               
#predicted genes (>= 300 bp)   2433                                                               
#predicted genes (>= 1500 bp)  13                                                                 
#predicted genes (>= 3000 bp)  1                                                                  
Largest alignment              3706                                                               
NA50                           537                                                                
NGA50                          398                                                                
NA75                           346                                                                
NGA75                          None                                                               
