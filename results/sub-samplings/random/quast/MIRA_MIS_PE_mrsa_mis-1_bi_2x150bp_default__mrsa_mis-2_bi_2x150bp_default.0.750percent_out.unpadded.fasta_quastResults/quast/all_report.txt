All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.750percent_out.unpadded
#Contigs                                   310                                                                                               
#Contigs (>= 0 bp)                         374                                                                                               
#Contigs (>= 1000 bp)                      40                                                                                                
Largest contig                             534849                                                                                            
Total length                               2940484                                                                                           
Total length (>= 0 bp)                     2950860                                                                                           
Total length (>= 1000 bp)                  2845901                                                                                           
Reference length                           2813862                                                                                           
N50                                        181908                                                                                            
NG50                                       181908                                                                                            
N75                                        87609                                                                                             
NG75                                       93994                                                                                             
L50                                        5                                                                                                 
LG50                                       5                                                                                                 
L75                                        11                                                                                                
LG75                                       10                                                                                                
#local misassemblies                       11                                                                                                
#misassemblies                             28                                                                                                
#misassembled contigs                      19                                                                                                
Misassembled contigs length                1144665                                                                                           
Misassemblies inter-contig overlap         47960                                                                                             
#unaligned contigs                         3 + 4 part                                                                                        
Unaligned contigs length                   6567                                                                                              
#ambiguously mapped contigs                17                                                                                                
Extra bases in ambiguously mapped contigs  -6755                                                                                             
#N's                                       30                                                                                                
#N's per 100 kbp                           1.02                                                                                              
Genome fraction (%)                        99.960                                                                                            
Duplication ratio                          1.058                                                                                             
#genes                                     2707 + 19 part                                                                                    
#predicted genes (unique)                  2906                                                                                              
#predicted genes (>= 0 bp)                 2937                                                                                              
#predicted genes (>= 300 bp)               2392                                                                                              
#predicted genes (>= 1500 bp)              304                                                                                               
#predicted genes (>= 3000 bp)              31                                                                                                
#mismatches                                181                                                                                               
#indels                                    53                                                                                                
Indels length                              187                                                                                               
#mismatches per 100 kbp                    6.44                                                                                              
#indels per 100 kbp                        1.88                                                                                              
GC (%)                                     33.02                                                                                             
Reference GC (%)                           32.81                                                                                             
Average %IDY                               98.927                                                                                            
Largest alignment                          534849                                                                                            
NA50                                       134698                                                                                            
NGA50                                      134698                                                                                            
NA75                                       75447                                                                                             
NGA75                                      87609                                                                                             
LA50                                       6                                                                                                 
LGA50                                      6                                                                                                 
LA75                                       14                                                                                                
LGA75                                      12                                                                                                
#Mis_misassemblies                         28                                                                                                
#Mis_relocations                           28                                                                                                
#Mis_translocations                        0                                                                                                 
#Mis_inversions                            0                                                                                                 
#Mis_misassembled contigs                  19                                                                                                
Mis_Misassembled contigs length            1144665                                                                                           
#Mis_local misassemblies                   11                                                                                                
#Mis_short indels (<= 5 bp)                45                                                                                                
#Mis_long indels (> 5 bp)                  8                                                                                                 
#Una_fully unaligned contigs               3                                                                                                 
Una_Fully unaligned length                 6357                                                                                              
#Una_partially unaligned contigs           4                                                                                                 
#Una_with misassembly                      0                                                                                                 
#Una_both parts are significant            0                                                                                                 
Una_Partially unaligned length             210                                                                                               
GAGE_Contigs #                             310                                                                                               
GAGE_Min contig                            201                                                                                               
GAGE_Max contig                            534849                                                                                            
GAGE_N50                                   181908 COUNT: 5                                                                                   
GAGE_Genome size                           2813862                                                                                           
GAGE_Assembly size                         2940484                                                                                           
GAGE_Chaff bases                           0                                                                                                 
GAGE_Missing reference bases               74(0.00%)                                                                                         
GAGE_Missing assembly bases                6673(0.23%)                                                                                       
GAGE_Missing assembly contigs              3(0.97%)                                                                                          
GAGE_Duplicated reference bases            124195                                                                                            
GAGE_Compressed reference bases            3602                                                                                              
GAGE_Bad trim                              316                                                                                               
GAGE_Avg idy                               99.98                                                                                             
GAGE_SNPs                                  43                                                                                                
GAGE_Indels < 5bp                          9                                                                                                 
GAGE_Indels >= 5                           4                                                                                                 
GAGE_Inversions                            3                                                                                                 
GAGE_Relocation                            3                                                                                                 
GAGE_Translocation                         0                                                                                                 
GAGE_Corrected contig #                    68                                                                                                
GAGE_Corrected assembly size               2845851                                                                                           
GAGE_Min correct contig                    202                                                                                               
GAGE_Max correct contig                    534848                                                                                            
GAGE_Corrected N50                         134699 COUNT: 6                                                                                   
