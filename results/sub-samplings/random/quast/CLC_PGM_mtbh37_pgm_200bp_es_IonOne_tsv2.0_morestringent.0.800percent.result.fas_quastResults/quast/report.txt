All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.800percent.result
#Contigs (>= 0 bp)             4266                                                                       
#Contigs (>= 1000 bp)          1113                                                                       
Total length (>= 0 bp)         3739841                                                                    
Total length (>= 1000 bp)      2411498                                                                    
#Contigs                       4266                                                                       
Largest contig                 10768                                                                      
Total length                   3739841                                                                    
Reference length               4411532                                                                    
GC (%)                         64.80                                                                      
Reference GC (%)               65.61                                                                      
N50                            1478                                                                       
NG50                           1163                                                                       
N75                            648                                                                        
NG75                           362                                                                        
#misassemblies                 567                                                                        
#local misassemblies           27                                                                         
#unaligned contigs             4 + 599 part                                                               
Unaligned contigs length       27319                                                                      
Genome fraction (%)            80.468                                                                     
Duplication ratio              1.045                                                                      
#N's per 100 kbp               35.59                                                                      
#mismatches per 100 kbp        32.48                                                                      
#indels per 100 kbp            107.89                                                                     
#genes                         1494 + 2399 part                                                           
#predicted genes (unique)      7315                                                                       
#predicted genes (>= 0 bp)     7317                                                                       
#predicted genes (>= 300 bp)   3898                                                                       
#predicted genes (>= 1500 bp)  91                                                                         
#predicted genes (>= 3000 bp)  1                                                                          
Largest alignment              10768                                                                      
NA50                           1398                                                                       
NGA50                          1072                                                                       
NA75                           563                                                                        
NGA75                          313                                                                        
