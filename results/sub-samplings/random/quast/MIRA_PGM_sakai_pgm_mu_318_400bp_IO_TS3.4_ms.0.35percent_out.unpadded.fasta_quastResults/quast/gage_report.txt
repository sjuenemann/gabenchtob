All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.35percent_out.unpadded
GAGE_Contigs #                   623                                                                 
GAGE_Min contig                  207                                                                 
GAGE_Max contig                  398215                                                              
GAGE_N50                         317368 COUNT: 8                                                     
GAGE_Genome size                 5594470                                                             
GAGE_Assembly size               5817341                                                             
GAGE_Chaff bases                 0                                                                   
GAGE_Missing reference bases     120(0.00%)                                                          
GAGE_Missing assembly bases      203(0.00%)                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                            
GAGE_Duplicated reference bases  297677                                                              
GAGE_Compressed reference bases  117518                                                              
GAGE_Bad trim                    193                                                                 
GAGE_Avg idy                     99.95                                                               
GAGE_SNPs                        484                                                                 
GAGE_Indels < 5bp                288                                                                 
GAGE_Indels >= 5                 11                                                                  
GAGE_Inversions                  16                                                                  
GAGE_Relocation                  15                                                                  
GAGE_Translocation               1                                                                   
GAGE_Corrected contig #          181                                                                 
GAGE_Corrected assembly size     5588242                                                             
GAGE_Min correct contig          208                                                                 
GAGE_Max correct contig          317177                                                              
GAGE_Corrected N50               114722 COUNT: 17                                                    
