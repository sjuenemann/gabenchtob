All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.150percent.scf
GAGE_Contigs #                   656                                                                           
GAGE_Min contig                  1000                                                                          
GAGE_Max contig                  41701                                                                         
GAGE_N50                         8298 COUNT: 167                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4000822                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     396129(8.98%)                                                                 
GAGE_Missing assembly bases      1001(0.03%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  282                                                                           
GAGE_Compressed reference bases  32406                                                                         
GAGE_Bad trim                    157                                                                           
GAGE_Avg idy                     99.95                                                                         
GAGE_SNPs                        103                                                                           
GAGE_Indels < 5bp                1649                                                                          
GAGE_Indels >= 5                 11                                                                            
GAGE_Inversions                  1                                                                             
GAGE_Relocation                  10                                                                            
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          676                                                                           
GAGE_Corrected assembly size     4002754                                                                       
GAGE_Min correct contig          201                                                                           
GAGE_Max correct contig          41710                                                                         
GAGE_Corrected N50               8130 COUNT: 172                                                               
