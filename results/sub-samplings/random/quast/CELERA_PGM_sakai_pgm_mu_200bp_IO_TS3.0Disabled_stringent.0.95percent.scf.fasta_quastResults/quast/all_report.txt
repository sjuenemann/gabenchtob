All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.95percent.scf
#Contigs                                   847                                                                     
#Contigs (>= 0 bp)                         847                                                                     
#Contigs (>= 1000 bp)                      847                                                                     
Largest contig                             44426                                                                   
Total length                               5226702                                                                 
Total length (>= 0 bp)                     5226702                                                                 
Total length (>= 1000 bp)                  5226702                                                                 
Reference length                           5594470                                                                 
N50                                        12184                                                                   
NG50                                       10865                                                                   
N75                                        5812                                                                    
NG75                                       4437                                                                    
L50                                        135                                                                     
LG50                                       151                                                                     
L75                                        288                                                                     
LG75                                       343                                                                     
#local misassemblies                       4                                                                       
#misassemblies                             8                                                                       
#misassembled contigs                      8                                                                       
Misassembled contigs length                59561                                                                   
Misassemblies inter-contig overlap         1575                                                                    
#unaligned contigs                         0 + 12 part                                                             
Unaligned contigs length                   471                                                                     
#ambiguously mapped contigs                3                                                                       
Extra bases in ambiguously mapped contigs  -3515                                                                   
#N's                                       0                                                                       
#N's per 100 kbp                           0.00                                                                    
Genome fraction (%)                        91.576                                                                  
Duplication ratio                          1.020                                                                   
#genes                                     4342 + 692 part                                                         
#predicted genes (unique)                  5854                                                                    
#predicted genes (>= 0 bp)                 5855                                                                    
#predicted genes (>= 300 bp)               4691                                                                    
#predicted genes (>= 1500 bp)              516                                                                     
#predicted genes (>= 3000 bp)              41                                                                      
#mismatches                                168                                                                     
#indels                                    800                                                                     
Indels length                              820                                                                     
#mismatches per 100 kbp                    3.28                                                                    
#indels per 100 kbp                        15.62                                                                   
GC (%)                                     50.22                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               99.251                                                                  
Largest alignment                          44426                                                                   
NA50                                       12152                                                                   
NGA50                                      10774                                                                   
NA75                                       5797                                                                    
NGA75                                      4390                                                                    
LA50                                       136                                                                     
LGA50                                      152                                                                     
LA75                                       290                                                                     
LGA75                                      345                                                                     
#Mis_misassemblies                         8                                                                       
#Mis_relocations                           6                                                                       
#Mis_translocations                        1                                                                       
#Mis_inversions                            1                                                                       
#Mis_misassembled contigs                  8                                                                       
Mis_Misassembled contigs length            59561                                                                   
#Mis_local misassemblies                   4                                                                       
#Mis_short indels (<= 5 bp)                799                                                                     
#Mis_long indels (> 5 bp)                  1                                                                       
#Una_fully unaligned contigs               0                                                                       
Una_Fully unaligned length                 0                                                                       
#Una_partially unaligned contigs           12                                                                      
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             471                                                                     
GAGE_Contigs #                             847                                                                     
GAGE_Min contig                            1000                                                                    
GAGE_Max contig                            44426                                                                   
GAGE_N50                                   10865 COUNT: 151                                                        
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5226702                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               402170(7.19%)                                                           
GAGE_Missing assembly bases                1252(0.02%)                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                                
GAGE_Duplicated reference bases            2731                                                                    
GAGE_Compressed reference bases            96671                                                                   
GAGE_Bad trim                              1252                                                                    
GAGE_Avg idy                               99.98                                                                   
GAGE_SNPs                                  144                                                                     
GAGE_Indels < 5bp                          747                                                                     
GAGE_Indels >= 5                           6                                                                       
GAGE_Inversions                            3                                                                       
GAGE_Relocation                            3                                                                       
GAGE_Translocation                         1                                                                       
GAGE_Corrected contig #                    857                                                                     
GAGE_Corrected assembly size               5225060                                                                 
GAGE_Min correct contig                    313                                                                     
GAGE_Max correct contig                    44429                                                                   
GAGE_Corrected N50                         10775 COUNT: 152                                                        
