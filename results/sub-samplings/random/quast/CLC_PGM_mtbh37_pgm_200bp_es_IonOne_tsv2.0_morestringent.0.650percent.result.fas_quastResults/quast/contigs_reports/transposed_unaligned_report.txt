All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                     #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.650percent.result  5                             1396                        654                               0                      4                                27713                           2104
