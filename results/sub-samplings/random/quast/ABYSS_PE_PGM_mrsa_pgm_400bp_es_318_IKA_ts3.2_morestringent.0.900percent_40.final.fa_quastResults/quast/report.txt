All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.900percent_40.final
#Contigs (>= 0 bp)             55259                                                                           
#Contigs (>= 1000 bp)          404                                                                             
Total length (>= 0 bp)         5584310                                                                         
Total length (>= 1000 bp)      585360                                                                          
#Contigs                       4131                                                                            
Largest contig                 3985                                                                            
Total length                   2211195                                                                         
Reference length               2813862                                                                         
GC (%)                         32.62                                                                           
Reference GC (%)               32.81                                                                           
N50                            627                                                                             
NG50                           495                                                                             
N75                            388                                                                             
NG75                           236                                                                             
#misassemblies                 1                                                                               
#local misassemblies           0                                                                               
#unaligned contigs             0 + 0 part                                                                      
Unaligned contigs length       0                                                                               
Genome fraction (%)            78.458                                                                          
Duplication ratio              1.001                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        1.22                                                                            
#indels per 100 kbp            1.36                                                                            
#genes                         440 + 2026 part                                                                 
#predicted genes (unique)      4884                                                                            
#predicted genes (>= 0 bp)     4884                                                                            
#predicted genes (>= 300 bp)   2561                                                                            
#predicted genes (>= 1500 bp)  30                                                                              
#predicted genes (>= 3000 bp)  1                                                                               
Largest alignment              3985                                                                            
NA50                           627                                                                             
NGA50                          495                                                                             
NA75                           387                                                                             
NGA75                          236                                                                             
