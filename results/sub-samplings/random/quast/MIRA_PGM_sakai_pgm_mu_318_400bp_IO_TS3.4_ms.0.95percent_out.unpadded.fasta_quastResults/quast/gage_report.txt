All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.95percent_out.unpadded
GAGE_Contigs #                   3155                                                                
GAGE_Min contig                  200                                                                 
GAGE_Max contig                  357399                                                              
GAGE_N50                         178802 COUNT: 11                                                    
GAGE_Genome size                 5594470                                                             
GAGE_Assembly size               7123105                                                             
GAGE_Chaff bases                 0                                                                   
GAGE_Missing reference bases     155(0.00%)                                                          
GAGE_Missing assembly bases      2889(0.04%)                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                            
GAGE_Duplicated reference bases  1611189                                                             
GAGE_Compressed reference bases  104252                                                              
GAGE_Bad trim                    2888                                                                
GAGE_Avg idy                     99.95                                                               
GAGE_SNPs                        531                                                                 
GAGE_Indels < 5bp                891                                                                 
GAGE_Indels >= 5                 7                                                                   
GAGE_Inversions                  17                                                                  
GAGE_Relocation                  12                                                                  
GAGE_Translocation               4                                                                   
GAGE_Corrected contig #          151                                                                 
GAGE_Corrected assembly size     5597763                                                             
GAGE_Min correct contig          200                                                                 
GAGE_Max correct contig          330946                                                              
GAGE_Corrected N50               99855 COUNT: 18                                                     
