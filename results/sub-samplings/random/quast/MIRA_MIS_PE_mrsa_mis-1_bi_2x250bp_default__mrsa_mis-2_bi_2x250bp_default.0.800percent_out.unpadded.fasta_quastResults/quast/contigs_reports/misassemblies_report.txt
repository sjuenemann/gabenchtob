All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.800percent_out.unpadded
#Mis_misassemblies               42                                                                                                
#Mis_relocations                 40                                                                                                
#Mis_translocations              0                                                                                                 
#Mis_inversions                  2                                                                                                 
#Mis_misassembled contigs        36                                                                                                
Mis_Misassembled contigs length  1584416                                                                                           
#Mis_local misassemblies         5                                                                                                 
#mismatches                      186                                                                                               
#indels                          27                                                                                                
#Mis_short indels (<= 5 bp)      21                                                                                                
#Mis_long indels (> 5 bp)        6                                                                                                 
Indels length                    164                                                                                               
