All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.050percent.K99.final-contigs
#Contigs (>= 0 bp)             279                                                                                         
#Contigs (>= 1000 bp)          240                                                                                         
Total length (>= 0 bp)         2744254                                                                                     
Total length (>= 1000 bp)      2722392                                                                                     
#Contigs                       275                                                                                         
Largest contig                 94382                                                                                       
Total length                   2743695                                                                                     
Reference length               2813862                                                                                     
GC (%)                         32.63                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            18671                                                                                       
NG50                           17603                                                                                       
N75                            9710                                                                                        
NG75                           9044                                                                                        
#misassemblies                 64                                                                                          
#local misassemblies           18                                                                                          
#unaligned contigs             0 + 21 part                                                                                 
Unaligned contigs length       1340                                                                                        
Genome fraction (%)            96.960                                                                                      
Duplication ratio              1.006                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        32.44                                                                                       
#indels per 100 kbp            131.22                                                                                      
#genes                         2426 + 222 part                                                                             
#predicted genes (unique)      3549                                                                                        
#predicted genes (>= 0 bp)     3551                                                                                        
#predicted genes (>= 300 bp)   2594                                                                                        
#predicted genes (>= 1500 bp)  151                                                                                         
#predicted genes (>= 3000 bp)  12                                                                                          
Largest alignment              74368                                                                                       
NA50                           16385                                                                                       
NGA50                          15992                                                                                       
NA75                           9011                                                                                        
NGA75                          8722                                                                                        
