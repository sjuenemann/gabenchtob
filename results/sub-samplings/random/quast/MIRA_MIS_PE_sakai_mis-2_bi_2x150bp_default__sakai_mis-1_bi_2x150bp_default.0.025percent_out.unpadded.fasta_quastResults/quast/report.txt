All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_sakai_mis-2_bi_2x150bp_default__sakai_mis-1_bi_2x150bp_default.0.025percent_out.unpadded
#Contigs (>= 0 bp)             1795                                                                                                
#Contigs (>= 1000 bp)          31                                                                                                  
Total length (>= 0 bp)         779225                                                                                              
Total length (>= 1000 bp)      59887                                                                                               
#Contigs                       1757                                                                                                
Largest contig                 5757                                                                                                
Total length                   772537                                                                                              
Reference length               5594470                                                                                             
GC (%)                         51.32                                                                                               
Reference GC (%)               50.48                                                                                               
N50                            443                                                                                                 
NG50                           None                                                                                                
N75                            351                                                                                                 
NG75                           None                                                                                                
#misassemblies                 25                                                                                                  
#local misassemblies           2                                                                                                   
#unaligned contigs             2 + 3 part                                                                                          
Unaligned contigs length       920                                                                                                 
Genome fraction (%)            13.095                                                                                              
Duplication ratio              1.002                                                                                               
#N's per 100 kbp               37.67                                                                                               
#mismatches per 100 kbp        82.86                                                                                               
#indels per 100 kbp            6.01                                                                                                
#genes                         129 + 1516 part                                                                                     
#predicted genes (unique)      2060                                                                                                
#predicted genes (>= 0 bp)     2060                                                                                                
#predicted genes (>= 300 bp)   1062                                                                                                
#predicted genes (>= 1500 bp)  5                                                                                                   
#predicted genes (>= 3000 bp)  0                                                                                                   
Largest alignment              5669                                                                                                
NA50                           419                                                                                                 
NGA50                          None                                                                                                
NA75                           326                                                                                                 
NGA75                          None                                                                                                
