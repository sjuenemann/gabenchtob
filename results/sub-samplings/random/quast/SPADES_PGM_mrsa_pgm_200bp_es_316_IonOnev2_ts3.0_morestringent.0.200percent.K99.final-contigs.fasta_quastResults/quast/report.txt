All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.200percent.K99.final-contigs
#Contigs (>= 0 bp)             104                                                                                         
#Contigs (>= 1000 bp)          77                                                                                          
Total length (>= 0 bp)         2770807                                                                                     
Total length (>= 1000 bp)      2765298                                                                                     
#Contigs                       84                                                                                          
Largest contig                 161661                                                                                      
Total length                   2768339                                                                                     
Reference length               2813862                                                                                     
GC (%)                         32.64                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            68661                                                                                       
NG50                           68661                                                                                       
N75                            37300                                                                                       
NG75                           35836                                                                                       
#misassemblies                 11                                                                                          
#local misassemblies           11                                                                                          
#unaligned contigs             0 + 4 part                                                                                  
Unaligned contigs length       140                                                                                         
Genome fraction (%)            98.160                                                                                      
Duplication ratio              1.003                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        10.28                                                                                       
#indels per 100 kbp            24.58                                                                                       
#genes                         2622 + 44 part                                                                              
#predicted genes (unique)      2754                                                                                        
#predicted genes (>= 0 bp)     2755                                                                                        
#predicted genes (>= 300 bp)   2340                                                                                        
#predicted genes (>= 1500 bp)  283                                                                                         
#predicted genes (>= 3000 bp)  25                                                                                          
Largest alignment              148171                                                                                      
NA50                           61519                                                                                       
NGA50                          59919                                                                                       
NA75                           35836                                                                                       
NGA75                          32994                                                                                       
