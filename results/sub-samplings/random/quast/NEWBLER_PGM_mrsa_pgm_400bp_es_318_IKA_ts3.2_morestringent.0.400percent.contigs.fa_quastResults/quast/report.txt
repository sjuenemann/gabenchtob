All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.400percent.contigs
#Contigs (>= 0 bp)             298                                                                           
#Contigs (>= 1000 bp)          56                                                                            
Total length (>= 0 bp)         2807005                                                                       
Total length (>= 1000 bp)      2764393                                                                       
#Contigs                       95                                                                            
Largest contig                 293635                                                                        
Total length                   2777068                                                                       
Reference length               2813862                                                                       
GC (%)                         32.64                                                                         
Reference GC (%)               32.81                                                                         
N50                            101149                                                                        
NG50                           101149                                                                        
N75                            51575                                                                         
NG75                           51575                                                                         
#misassemblies                 3                                                                             
#local misassemblies           1                                                                             
#unaligned contigs             0 + 0 part                                                                    
Unaligned contigs length       0                                                                             
Genome fraction (%)            98.193                                                                        
Duplication ratio              1.002                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        1.34                                                                          
#indels per 100 kbp            66.67                                                                         
#genes                         2662 + 33 part                                                                
#predicted genes (unique)      3295                                                                          
#predicted genes (>= 0 bp)     3297                                                                          
#predicted genes (>= 300 bp)   2555                                                                          
#predicted genes (>= 1500 bp)  188                                                                           
#predicted genes (>= 3000 bp)  18                                                                            
Largest alignment              293635                                                                        
NA50                           84715                                                                         
NGA50                          84715                                                                         
NA75                           51281                                                                         
NGA75                          51281                                                                         
