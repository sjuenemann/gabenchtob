All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.35percent_60.final
#Contigs                                   1232                                                                           
#Contigs (>= 0 bp)                         5839                                                                           
#Contigs (>= 1000 bp)                      726                                                                            
Largest contig                             45879                                                                          
Total length                               5256045                                                                        
Total length (>= 0 bp)                     5738123                                                                        
Total length (>= 1000 bp)                  5045054                                                                        
Reference length                           5594470                                                                        
N50                                        10644                                                                          
NG50                                       10072                                                                          
N75                                        5231                                                                           
NG75                                       4322                                                                           
L50                                        148                                                                            
LG50                                       164                                                                            
L75                                        322                                                                            
LG75                                       375                                                                            
#local misassemblies                       5                                                                              
#misassemblies                             2                                                                              
#misassembled contigs                      2                                                                              
Misassembled contigs length                20382                                                                          
Misassemblies inter-contig overlap         470                                                                            
#unaligned contigs                         0 + 1 part                                                                     
Unaligned contigs length                   47                                                                             
#ambiguously mapped contigs                127                                                                            
Extra bases in ambiguously mapped contigs  -59750                                                                         
#N's                                       0                                                                              
#N's per 100 kbp                           0.00                                                                           
Genome fraction (%)                        92.767                                                                         
Duplication ratio                          1.001                                                                          
#genes                                     4306 + 712 part                                                                
#predicted genes (unique)                  6295                                                                           
#predicted genes (>= 0 bp)                 6295                                                                           
#predicted genes (>= 300 bp)               4810                                                                           
#predicted genes (>= 1500 bp)              501                                                                            
#predicted genes (>= 3000 bp)              32                                                                             
#mismatches                                85                                                                             
#indels                                    1250                                                                           
Indels length                              1298                                                                           
#mismatches per 100 kbp                    1.64                                                                           
#indels per 100 kbp                        24.09                                                                          
GC (%)                                     50.22                                                                          
Reference GC (%)                           50.48                                                                          
Average %IDY                               99.905                                                                         
Largest alignment                          45879                                                                          
NA50                                       10578                                                                          
NGA50                                      10020                                                                          
NA75                                       5231                                                                           
NGA75                                      4322                                                                           
LA50                                       149                                                                            
LGA50                                      165                                                                            
LA75                                       323                                                                            
LGA75                                      376                                                                            
#Mis_misassemblies                         2                                                                              
#Mis_relocations                           2                                                                              
#Mis_translocations                        0                                                                              
#Mis_inversions                            0                                                                              
#Mis_misassembled contigs                  2                                                                              
Mis_Misassembled contigs length            20382                                                                          
#Mis_local misassemblies                   5                                                                              
#Mis_short indels (<= 5 bp)                1250                                                                           
#Mis_long indels (> 5 bp)                  0                                                                              
#Una_fully unaligned contigs               0                                                                              
Una_Fully unaligned length                 0                                                                              
#Una_partially unaligned contigs           1                                                                              
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             47                                                                             
GAGE_Contigs #                             1232                                                                           
GAGE_Min contig                            200                                                                            
GAGE_Max contig                            45879                                                                          
GAGE_N50                                   10072 COUNT: 164                                                               
GAGE_Genome size                           5594470                                                                        
GAGE_Assembly size                         5256045                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               231326(4.13%)                                                                  
GAGE_Missing assembly bases                102(0.00%)                                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                                       
GAGE_Duplicated reference bases            1726                                                                           
GAGE_Compressed reference bases            128548                                                                         
GAGE_Bad trim                              102                                                                            
GAGE_Avg idy                               99.97                                                                          
GAGE_SNPs                                  105                                                                            
GAGE_Indels < 5bp                          1302                                                                           
GAGE_Indels >= 5                           4                                                                              
GAGE_Inversions                            0                                                                              
GAGE_Relocation                            3                                                                              
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    1231                                                                           
GAGE_Corrected assembly size               5255815                                                                        
GAGE_Min correct contig                    200                                                                            
GAGE_Max correct contig                    45893                                                                          
GAGE_Corrected N50                         9914 COUNT: 166                                                                
