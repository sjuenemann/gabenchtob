All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.900percent.result
GAGE_Contigs #                   1130                                                                     
GAGE_Min contig                  200                                                                      
GAGE_Max contig                  85352                                                                    
GAGE_N50                         25696 COUNT: 34                                                          
GAGE_Genome size                 2813862                                                                  
GAGE_Assembly size               2990775                                                                  
GAGE_Chaff bases                 0                                                                        
GAGE_Missing reference bases     30268(1.08%)                                                             
GAGE_Missing assembly bases      8150(0.27%)                                                              
GAGE_Missing assembly contigs    23(2.04%)                                                                
GAGE_Duplicated reference bases  223743                                                                   
GAGE_Compressed reference bases  41342                                                                    
GAGE_Bad trim                    1532                                                                     
GAGE_Avg idy                     99.98                                                                    
GAGE_SNPs                        39                                                                       
GAGE_Indels < 5bp                217                                                                      
GAGE_Indels >= 5                 1                                                                        
GAGE_Inversions                  1                                                                        
GAGE_Relocation                  2                                                                        
GAGE_Translocation               0                                                                        
GAGE_Corrected contig #          370                                                                      
GAGE_Corrected assembly size     2761604                                                                  
GAGE_Min correct contig          201                                                                      
GAGE_Max correct contig          85356                                                                    
GAGE_Corrected N50               24909 COUNT: 35                                                          
