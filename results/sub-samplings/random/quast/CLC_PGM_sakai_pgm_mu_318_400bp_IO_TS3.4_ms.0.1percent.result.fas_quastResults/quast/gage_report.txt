All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.1percent.result
GAGE_Contigs #                   715                                                         
GAGE_Min contig                  201                                                         
GAGE_Max contig                  109141                                                      
GAGE_N50                         38581 COUNT: 46                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5393631                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     38644(0.69%)                                                
GAGE_Missing assembly bases      443(0.01%)                                                  
GAGE_Missing assembly contigs    1(0.14%)                                                    
GAGE_Duplicated reference bases  41609                                                       
GAGE_Compressed reference bases  243297                                                      
GAGE_Bad trim                    193                                                         
GAGE_Avg idy                     99.98                                                       
GAGE_SNPs                        118                                                         
GAGE_Indels < 5bp                939                                                         
GAGE_Indels >= 5                 9                                                           
GAGE_Inversions                  0                                                           
GAGE_Relocation                  7                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          586                                                         
GAGE_Corrected assembly size     5353559                                                     
GAGE_Min correct contig          201                                                         
GAGE_Max correct contig          109130                                                      
GAGE_Corrected N50               37202 COUNT: 48                                             
