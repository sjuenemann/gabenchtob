All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.scf
GAGE_Contigs #                   333                                              
GAGE_Min contig                  1003                                             
GAGE_Max contig                  139018                                           
GAGE_N50                         33565 COUNT: 45                                  
GAGE_Genome size                 5594470                                          
GAGE_Assembly size               5289907                                          
GAGE_Chaff bases                 0                                                
GAGE_Missing reference bases     275709(4.93%)                                    
GAGE_Missing assembly bases      74(0.00%)                                        
GAGE_Missing assembly contigs    0(0.00%)                                         
GAGE_Duplicated reference bases  5071                                             
GAGE_Compressed reference bases  151733                                           
GAGE_Bad trim                    74                                               
GAGE_Avg idy                     99.99                                            
GAGE_SNPs                        63                                               
GAGE_Indels < 5bp                393                                              
GAGE_Indels >= 5                 6                                                
GAGE_Inversions                  1                                                
GAGE_Relocation                  5                                                
GAGE_Translocation               0                                                
GAGE_Corrected contig #          340                                              
GAGE_Corrected assembly size     5288399                                          
GAGE_Min correct contig          482                                              
GAGE_Max correct contig          139019                                           
GAGE_Corrected N50               30964 COUNT: 47                                  
