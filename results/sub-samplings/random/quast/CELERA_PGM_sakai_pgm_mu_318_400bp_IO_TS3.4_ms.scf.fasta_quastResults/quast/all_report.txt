All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.scf
#Contigs                                   333                                              
#Contigs (>= 0 bp)                         333                                              
#Contigs (>= 1000 bp)                      333                                              
Largest contig                             139018                                           
Total length                               5289907                                          
Total length (>= 0 bp)                     5289907                                          
Total length (>= 1000 bp)                  5289907                                          
Reference length                           5594470                                          
N50                                        39548                                            
NG50                                       33565                                            
N75                                        18823                                            
NG75                                       16583                                            
L50                                        41                                               
LG50                                       45                                               
L75                                        94                                               
LG75                                       106                                              
#local misassemblies                       7                                                
#misassemblies                             3                                                
#misassembled contigs                      3                                                
Misassembled contigs length                107260                                           
Misassemblies inter-contig overlap         1097                                             
#unaligned contigs                         0 + 0 part                                       
Unaligned contigs length                   0                                                
#ambiguously mapped contigs                22                                               
Extra bases in ambiguously mapped contigs  -32260                                           
#N's                                       0                                                
#N's per 100 kbp                           0.00                                             
Genome fraction (%)                        92.816                                           
Duplication ratio                          1.013                                            
#genes                                     4841 + 236 part                                  
#predicted genes (unique)                  5398                                             
#predicted genes (>= 0 bp)                 5404                                             
#predicted genes (>= 300 bp)               4543                                             
#predicted genes (>= 1500 bp)              617                                              
#predicted genes (>= 3000 bp)              57                                               
#mismatches                                90                                               
#indels                                    418                                              
Indels length                              420                                              
#mismatches per 100 kbp                    1.73                                             
#indels per 100 kbp                        8.05                                             
GC (%)                                     50.33                                            
Reference GC (%)                           50.48                                            
Average %IDY                               98.690                                           
Largest alignment                          139018                                           
NA50                                       37290                                            
NGA50                                      33565                                            
NA75                                       18823                                            
NGA75                                      16583                                            
LA50                                       42                                               
LGA50                                      46                                               
LA75                                       95                                               
LGA75                                      107                                              
#Mis_misassemblies                         3                                                
#Mis_relocations                           3                                                
#Mis_translocations                        0                                                
#Mis_inversions                            0                                                
#Mis_misassembled contigs                  3                                                
Mis_Misassembled contigs length            107260                                           
#Mis_local misassemblies                   7                                                
#Mis_short indels (<= 5 bp)                418                                              
#Mis_long indels (> 5 bp)                  0                                                
#Una_fully unaligned contigs               0                                                
Una_Fully unaligned length                 0                                                
#Una_partially unaligned contigs           0                                                
#Una_with misassembly                      0                                                
#Una_both parts are significant            0                                                
Una_Partially unaligned length             0                                                
GAGE_Contigs #                             333                                              
GAGE_Min contig                            1003                                             
GAGE_Max contig                            139018                                           
GAGE_N50                                   33565 COUNT: 45                                  
GAGE_Genome size                           5594470                                          
GAGE_Assembly size                         5289907                                          
GAGE_Chaff bases                           0                                                
GAGE_Missing reference bases               275709(4.93%)                                    
GAGE_Missing assembly bases                74(0.00%)                                        
GAGE_Missing assembly contigs              0(0.00%)                                         
GAGE_Duplicated reference bases            5071                                             
GAGE_Compressed reference bases            151733                                           
GAGE_Bad trim                              74                                               
GAGE_Avg idy                               99.99                                            
GAGE_SNPs                                  63                                               
GAGE_Indels < 5bp                          393                                              
GAGE_Indels >= 5                           6                                                
GAGE_Inversions                            1                                                
GAGE_Relocation                            5                                                
GAGE_Translocation                         0                                                
GAGE_Corrected contig #                    340                                              
GAGE_Corrected assembly size               5288399                                          
GAGE_Min correct contig                    482                                              
GAGE_Max correct contig                    139019                                           
GAGE_Corrected N50                         30964 COUNT: 47                                  
