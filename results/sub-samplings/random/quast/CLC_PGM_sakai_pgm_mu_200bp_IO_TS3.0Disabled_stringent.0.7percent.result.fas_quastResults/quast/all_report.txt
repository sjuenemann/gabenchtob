All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.7percent.result
#Contigs                                   842                                                                    
#Contigs (>= 0 bp)                         842                                                                    
#Contigs (>= 1000 bp)                      347                                                                    
Largest contig                             123683                                                                 
Total length                               5384166                                                                
Total length (>= 0 bp)                     5384166                                                                
Total length (>= 1000 bp)                  5225372                                                                
Reference length                           5594470                                                                
N50                                        28685                                                                  
NG50                                       28009                                                                  
N75                                        14387                                                                  
NG75                                       13197                                                                  
L50                                        56                                                                     
LG50                                       59                                                                     
L75                                        123                                                                    
LG75                                       134                                                                    
#local misassemblies                       2                                                                      
#misassemblies                             16                                                                     
#misassembled contigs                      15                                                                     
Misassembled contigs length                72041                                                                  
Misassemblies inter-contig overlap         318                                                                    
#unaligned contigs                         3 + 26 part                                                            
Unaligned contigs length                   1826                                                                   
#ambiguously mapped contigs                141                                                                    
Extra bases in ambiguously mapped contigs  -77169                                                                 
#N's                                       201                                                                    
#N's per 100 kbp                           3.73                                                                   
Genome fraction (%)                        94.015                                                                 
Duplication ratio                          1.009                                                                  
#genes                                     4773 + 350 part                                                        
#predicted genes (unique)                  6137                                                                   
#predicted genes (>= 0 bp)                 6141                                                                   
#predicted genes (>= 300 bp)               4756                                                                   
#predicted genes (>= 1500 bp)              552                                                                    
#predicted genes (>= 3000 bp)              36                                                                     
#mismatches                                62                                                                     
#indels                                    1202                                                                   
Indels length                              1256                                                                   
#mismatches per 100 kbp                    1.18                                                                   
#indels per 100 kbp                        22.85                                                                  
GC (%)                                     50.28                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               98.931                                                                 
Largest alignment                          123683                                                                 
NA50                                       28302                                                                  
NGA50                                      27697                                                                  
NA75                                       14199                                                                  
NGA75                                      12541                                                                  
LA50                                       56                                                                     
LGA50                                      60                                                                     
LA75                                       124                                                                    
LGA75                                      136                                                                    
#Mis_misassemblies                         16                                                                     
#Mis_relocations                           4                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            12                                                                     
#Mis_misassembled contigs                  15                                                                     
Mis_Misassembled contigs length            72041                                                                  
#Mis_local misassemblies                   2                                                                      
#Mis_short indels (<= 5 bp)                1201                                                                   
#Mis_long indels (> 5 bp)                  1                                                                      
#Una_fully unaligned contigs               3                                                                      
Una_Fully unaligned length                 766                                                                    
#Una_partially unaligned contigs           26                                                                     
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             1060                                                                   
GAGE_Contigs #                             842                                                                    
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            123683                                                                 
GAGE_N50                                   28009 COUNT: 59                                                        
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5384166                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               61672(1.10%)                                                           
GAGE_Missing assembly bases                1906(0.04%)                                                            
GAGE_Missing assembly contigs              3(0.36%)                                                               
GAGE_Duplicated reference bases            54758                                                                  
GAGE_Compressed reference bases            237584                                                                 
GAGE_Bad trim                              1140                                                                   
GAGE_Avg idy                               99.97                                                                  
GAGE_SNPs                                  61                                                                     
GAGE_Indels < 5bp                          1212                                                                   
GAGE_Indels >= 5                           3                                                                      
GAGE_Inversions                            1                                                                      
GAGE_Relocation                            3                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    634                                                                    
GAGE_Corrected assembly size               5328979                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    123708                                                                 
GAGE_Corrected N50                         26725 COUNT: 61                                                        
