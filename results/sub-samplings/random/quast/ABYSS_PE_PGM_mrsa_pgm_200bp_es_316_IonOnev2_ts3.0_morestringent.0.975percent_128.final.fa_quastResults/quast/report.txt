All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.975percent_128.final
#Contigs (>= 0 bp)             2769                                                                                  
#Contigs (>= 1000 bp)          248                                                                                   
Total length (>= 0 bp)         3224120                                                                               
Total length (>= 1000 bp)      2723777                                                                               
#Contigs                       1380                                                                                  
Largest contig                 82555                                                                                 
Total length                   3020462                                                                               
Reference length               2813862                                                                               
GC (%)                         32.65                                                                                 
Reference GC (%)               32.81                                                                                 
N50                            17445                                                                                 
NG50                           18260                                                                                 
N75                            7320                                                                                  
NG75                           9963                                                                                  
#misassemblies                 17                                                                                    
#local misassemblies           3                                                                                     
#unaligned contigs             0 + 13 part                                                                           
Unaligned contigs length       434                                                                                   
Genome fraction (%)            98.445                                                                                
Duplication ratio              1.080                                                                                 
#N's per 100 kbp               0.00                                                                                  
#mismatches per 100 kbp        1.30                                                                                  
#indels per 100 kbp            12.74                                                                                 
#genes                         2513 + 186 part                                                                       
#predicted genes (unique)      3857                                                                                  
#predicted genes (>= 0 bp)     3933                                                                                  
#predicted genes (>= 300 bp)   2363                                                                                  
#predicted genes (>= 1500 bp)  273                                                                                   
#predicted genes (>= 3000 bp)  21                                                                                    
Largest alignment              82555                                                                                 
NA50                           17445                                                                                 
NGA50                          18260                                                                                 
NA75                           7320                                                                                  
NGA75                          9539                                                                                  
