reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  NEWBLER_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.600percent.contigs_broken  | 97.6730759292       | 1.00492355436     | 82          | 3933      | 97        | None      | None      |
  NEWBLER_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default.0.600percent.contigs  | 97.7815643182       | 1.01455096768     | 78          | 3935      | 97        | None      | None      |
