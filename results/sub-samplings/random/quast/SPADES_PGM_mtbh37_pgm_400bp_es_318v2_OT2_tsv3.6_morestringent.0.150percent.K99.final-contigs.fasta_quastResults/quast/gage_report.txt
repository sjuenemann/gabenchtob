All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.150percent.K99.final-contigs
GAGE_Contigs #                   403                                                                                         
GAGE_Min contig                  223                                                                                         
GAGE_Max contig                  66866                                                                                       
GAGE_N50                         18519 COUNT: 75                                                                             
GAGE_Genome size                 4411532                                                                                     
GAGE_Assembly size               4251635                                                                                     
GAGE_Chaff bases                 0                                                                                           
GAGE_Missing reference bases     129568(2.94%)                                                                               
GAGE_Missing assembly bases      1909(0.04%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                    
GAGE_Duplicated reference bases  12689                                                                                       
GAGE_Compressed reference bases  51884                                                                                       
GAGE_Bad trim                    1401                                                                                        
GAGE_Avg idy                     99.88                                                                                       
GAGE_SNPs                        362                                                                                         
GAGE_Indels < 5bp                4049                                                                                        
GAGE_Indels >= 5                 23                                                                                          
GAGE_Inversions                  5                                                                                           
GAGE_Relocation                  16                                                                                          
GAGE_Translocation               0                                                                                           
GAGE_Corrected contig #          447                                                                                         
GAGE_Corrected assembly size     4244239                                                                                     
GAGE_Min correct contig          223                                                                                         
GAGE_Max correct contig          66878                                                                                       
GAGE_Corrected N50               17321 COUNT: 81                                                                             
