All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mtbh37_pgm_200bp_es_IonOne_tsv2.0_morestringent.0.950percent.result
#Contigs                                   4036                                                                       
#Contigs (>= 0 bp)                         4036                                                                       
#Contigs (>= 1000 bp)                      1119                                                                       
Largest contig                             10632                                                                      
Total length                               3738849                                                                    
Total length (>= 0 bp)                     3738849                                                                    
Total length (>= 1000 bp)                  2557057                                                                    
Reference length                           4411532                                                                    
N50                                        1741                                                                       
NG50                                       1346                                                                       
N75                                        731                                                                        
NG75                                       362                                                                        
L50                                        599                                                                        
LG50                                       819                                                                        
L75                                        1409                                                                       
LG75                                       2436                                                                       
#local misassemblies                       21                                                                         
#misassemblies                             525                                                                        
#misassembled contigs                      495                                                                        
Misassembled contigs length                613412                                                                     
Misassemblies inter-contig overlap         6718                                                                       
#unaligned contigs                         6 + 610 part                                                               
Unaligned contigs length                   29244                                                                      
#ambiguously mapped contigs                21                                                                         
Extra bases in ambiguously mapped contigs  -10649                                                                     
#N's                                       1051                                                                       
#N's per 100 kbp                           28.11                                                                      
Genome fraction (%)                        80.353                                                                     
Duplication ratio                          1.045                                                                      
#genes                                     1600 + 2269 part                                                           
#predicted genes (unique)                  7105                                                                       
#predicted genes (>= 0 bp)                 7105                                                                       
#predicted genes (>= 300 bp)               3874                                                                       
#predicted genes (>= 1500 bp)              105                                                                        
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                1155                                                                       
#indels                                    3450                                                                       
Indels length                              3613                                                                       
#mismatches per 100 kbp                    32.58                                                                      
#indels per 100 kbp                        97.33                                                                      
GC (%)                                     64.80                                                                      
Reference GC (%)                           65.61                                                                      
Average %IDY                               99.757                                                                     
Largest alignment                          10290                                                                      
NA50                                       1622                                                                       
NGA50                                      1236                                                                       
NA75                                       630                                                                        
NGA75                                      315                                                                        
LA50                                       634                                                                        
LGA50                                      871                                                                        
LA75                                       1525                                                                       
LGA75                                      2700                                                                       
#Mis_misassemblies                         525                                                                        
#Mis_relocations                           137                                                                        
#Mis_translocations                        0                                                                          
#Mis_inversions                            388                                                                        
#Mis_misassembled contigs                  495                                                                        
Mis_Misassembled contigs length            613412                                                                     
#Mis_local misassemblies                   21                                                                         
#Mis_short indels (<= 5 bp)                3445                                                                       
#Mis_long indels (> 5 bp)                  5                                                                          
#Una_fully unaligned contigs               6                                                                          
Una_Fully unaligned length                 3845                                                                       
#Una_partially unaligned contigs           610                                                                        
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             25399                                                                      
GAGE_Contigs #                             4036                                                                       
GAGE_Min contig                            200                                                                        
GAGE_Max contig                            10632                                                                      
GAGE_N50                                   1346 COUNT: 819                                                            
GAGE_Genome size                           4411532                                                                    
GAGE_Assembly size                         3738849                                                                    
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               811452(18.39%)                                                             
GAGE_Missing assembly bases                32484(0.87%)                                                               
GAGE_Missing assembly contigs              6(0.15%)                                                                   
GAGE_Duplicated reference bases            97933                                                                      
GAGE_Compressed reference bases            46799                                                                      
GAGE_Bad trim                              28065                                                                      
GAGE_Avg idy                               99.86                                                                      
GAGE_SNPs                                  826                                                                        
GAGE_Indels < 5bp                          3205                                                                       
GAGE_Indels >= 5                           11                                                                         
GAGE_Inversions                            139                                                                        
GAGE_Relocation                            31                                                                         
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    3829                                                                       
GAGE_Corrected assembly size               3598386                                                                    
GAGE_Min correct contig                    200                                                                        
GAGE_Max correct contig                    10293                                                                      
GAGE_Corrected N50                         1238 COUNT: 876                                                            
