All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.700percent_32.final
#Contigs (>= 0 bp)             7415                                                                                 
#Contigs (>= 1000 bp)          1101                                                                                 
Total length (>= 0 bp)         4518070                                                                              
Total length (>= 1000 bp)      3817665                                                                              
#Contigs                       1783                                                                                 
Largest contig                 27390                                                                                
Total length                   4180515                                                                              
Reference length               4411532                                                                              
GC (%)                         65.29                                                                                
Reference GC (%)               65.61                                                                                
N50                            4067                                                                                 
NG50                           3903                                                                                 
N75                            2220                                                                                 
NG75                           1930                                                                                 
#misassemblies                 0                                                                                    
#local misassemblies           2                                                                                    
#unaligned contigs             1 + 2 part                                                                           
Unaligned contigs length       955                                                                                  
Genome fraction (%)            94.320                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        2.67                                                                                 
#indels per 100 kbp            15.62                                                                                
#genes                         2824 + 1184 part                                                                     
#predicted genes (unique)      5302                                                                                 
#predicted genes (>= 0 bp)     5302                                                                                 
#predicted genes (>= 300 bp)   4075                                                                                 
#predicted genes (>= 1500 bp)  349                                                                                  
#predicted genes (>= 3000 bp)  28                                                                                   
Largest alignment              27390                                                                                
NA50                           4067                                                                                 
NGA50                          3903                                                                                 
NA75                           2220                                                                                 
NGA75                          1930                                                                                 
