All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.125percent.scf
#Mis_misassemblies               1                                                                        
#Mis_relocations                 1                                                                        
#Mis_translocations              0                                                                        
#Mis_inversions                  0                                                                        
#Mis_misassembled contigs        1                                                                        
Mis_Misassembled contigs length  155460                                                                   
#Mis_local misassemblies         3                                                                        
#mismatches                      83                                                                       
#indels                          312                                                                      
#Mis_short indels (<= 5 bp)      308                                                                      
#Mis_long indels (> 5 bp)        4                                                                        
Indels length                    340                                                                      
