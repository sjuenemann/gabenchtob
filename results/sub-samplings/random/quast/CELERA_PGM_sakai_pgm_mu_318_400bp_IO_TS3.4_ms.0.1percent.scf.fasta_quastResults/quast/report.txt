All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.1percent.scf
#Contigs (>= 0 bp)             202                                                         
#Contigs (>= 1000 bp)          202                                                         
Total length (>= 0 bp)         5200466                                                     
Total length (>= 1000 bp)      5200466                                                     
#Contigs                       202                                                         
Largest contig                 196797                                                      
Total length                   5200466                                                     
Reference length               5594470                                                     
GC (%)                         50.30                                                       
Reference GC (%)               50.48                                                       
N50                            67325                                                       
NG50                           64128                                                       
N75                            36069                                                       
NG75                           30016                                                       
#misassemblies                 1                                                           
#local misassemblies           10                                                          
#unaligned contigs             0 + 0 part                                                  
Unaligned contigs length       0                                                           
Genome fraction (%)            92.649                                                      
Duplication ratio              1.003                                                       
#N's per 100 kbp               0.00                                                        
#mismatches per 100 kbp        1.87                                                        
#indels per 100 kbp            14.70                                                       
#genes                         4880 + 191 part                                             
#predicted genes (unique)      5303                                                        
#predicted genes (>= 0 bp)     5306                                                        
#predicted genes (>= 300 bp)   4454                                                        
#predicted genes (>= 1500 bp)  597                                                         
#predicted genes (>= 3000 bp)  53                                                          
Largest alignment              196797                                                      
NA50                           67325                                                       
NGA50                          64128                                                       
NA75                           35780                                                       
NGA75                          29687                                                       
