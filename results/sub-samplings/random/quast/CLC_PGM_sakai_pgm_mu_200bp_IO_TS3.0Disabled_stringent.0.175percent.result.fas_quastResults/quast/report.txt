All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.175percent.result
#Contigs (>= 0 bp)             962                                                                      
#Contigs (>= 1000 bp)          420                                                                      
Total length (>= 0 bp)         5342756                                                                  
Total length (>= 1000 bp)      5148306                                                                  
#Contigs                       962                                                                      
Largest contig                 96598                                                                    
Total length                   5342756                                                                  
Reference length               5594470                                                                  
GC (%)                         50.30                                                                    
Reference GC (%)               50.48                                                                    
N50                            22662                                                                    
NG50                           21173                                                                    
N75                            11280                                                                    
NG75                           9410                                                                     
#misassemblies                 26                                                                       
#local misassemblies           6                                                                        
#unaligned contigs             2 + 53 part                                                              
Unaligned contigs length       2354                                                                     
Genome fraction (%)            93.177                                                                   
Duplication ratio              1.008                                                                    
#N's per 100 kbp               3.80                                                                     
#mismatches per 100 kbp        4.30                                                                     
#indels per 100 kbp            38.10                                                                    
#genes                         4636 + 438 part                                                          
#predicted genes (unique)      6515                                                                     
#predicted genes (>= 0 bp)     6516                                                                     
#predicted genes (>= 300 bp)   4877                                                                     
#predicted genes (>= 1500 bp)  458                                                                      
#predicted genes (>= 3000 bp)  29                                                                       
Largest alignment              96598                                                                    
NA50                           21602                                                                    
NGA50                          21017                                                                    
NA75                           11082                                                                    
NGA75                          9362                                                                     
