All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.8percent.K99.final-contigs
#Contigs (>= 0 bp)             687                                                                       
#Contigs (>= 1000 bp)          169                                                                       
Total length (>= 0 bp)         5414568                                                                   
Total length (>= 1000 bp)      5261400                                                                   
#Contigs                       428                                                                       
Largest contig                 316884                                                                    
Total length                   5378759                                                                   
Reference length               5594470                                                                   
GC (%)                         50.31                                                                     
Reference GC (%)               50.48                                                                     
N50                            124343                                                                    
NG50                           113475                                                                    
N75                            50281                                                                     
NG75                           42995                                                                     
#misassemblies                 3                                                                         
#local misassemblies           10                                                                        
#unaligned contigs             0 + 0 part                                                                
Unaligned contigs length       0                                                                         
Genome fraction (%)            94.417                                                                    
Duplication ratio              1.002                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        5.02                                                                      
#indels per 100 kbp            5.28                                                                      
#genes                         4927 + 199 part                                                           
#predicted genes (unique)      5467                                                                      
#predicted genes (>= 0 bp)     5485                                                                      
#predicted genes (>= 300 bp)   4523                                                                      
#predicted genes (>= 1500 bp)  657                                                                       
#predicted genes (>= 3000 bp)  62                                                                        
Largest alignment              238170                                                                    
NA50                           124343                                                                    
NGA50                          113475                                                                    
NA75                           50281                                                                     
NGA75                          41259                                                                     
