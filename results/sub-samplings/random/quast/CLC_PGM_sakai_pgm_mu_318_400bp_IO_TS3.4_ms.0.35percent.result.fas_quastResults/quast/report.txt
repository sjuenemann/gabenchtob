All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.35percent.result
#Contigs (>= 0 bp)             820                                                          
#Contigs (>= 1000 bp)          269                                                          
Total length (>= 0 bp)         5426858                                                      
Total length (>= 1000 bp)      5230402                                                      
#Contigs                       820                                                          
Largest contig                 133730                                                       
Total length                   5426858                                                      
Reference length               5594470                                                      
GC (%)                         50.32                                                        
Reference GC (%)               50.48                                                        
N50                            43767                                                        
NG50                           42441                                                        
N75                            20882                                                        
NG75                           18350                                                        
#misassemblies                 7                                                            
#local misassemblies           9                                                            
#unaligned contigs             2 + 5 part                                                   
Unaligned contigs length       850                                                          
Genome fraction (%)            94.433                                                       
Duplication ratio              1.012                                                        
#N's per 100 kbp               5.95                                                         
#mismatches per 100 kbp        4.30                                                         
#indels per 100 kbp            8.33                                                         
#genes                         4861 + 284 part                                              
#predicted genes (unique)      5783                                                         
#predicted genes (>= 0 bp)     5792                                                         
#predicted genes (>= 300 bp)   4586                                                         
#predicted genes (>= 1500 bp)  624                                                          
#predicted genes (>= 3000 bp)  64                                                           
Largest alignment              133730                                                       
NA50                           43767                                                        
NGA50                          42353                                                        
NA75                           20473                                                        
NGA75                          17394                                                        
