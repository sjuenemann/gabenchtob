All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.900percent.contigs
GAGE_Contigs #                   147                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  297234                                                                        
GAGE_N50                         68432 COUNT: 12                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2772195                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     14384(0.51%)                                                                  
GAGE_Missing assembly bases      29(0.00%)                                                                     
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  6897                                                                          
GAGE_Compressed reference bases  39053                                                                         
GAGE_Bad trim                    29                                                                            
GAGE_Avg idy                     99.99                                                                         
GAGE_SNPs                        33                                                                            
GAGE_Indels < 5bp                126                                                                           
GAGE_Indels >= 5                 3                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  1                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          124                                                                           
GAGE_Corrected assembly size     2765592                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          297243                                                                        
GAGE_Corrected N50               57637 COUNT: 13                                                               
