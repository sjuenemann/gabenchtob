All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.900percent.contigs
#Mis_misassemblies               12                                                                            
#Mis_relocations                 11                                                                            
#Mis_translocations              1                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        12                                                                            
Mis_Misassembled contigs length  118426                                                                        
#Mis_local misassemblies         4                                                                             
#mismatches                      47                                                                            
#indels                          132                                                                           
#Mis_short indels (<= 5 bp)      132                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    134                                                                           
