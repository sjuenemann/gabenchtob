All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.925percent.scf
GAGE_Contigs #                   205                                                                           
GAGE_Min contig                  1002                                                                          
GAGE_Max contig                  157536                                                                        
GAGE_N50                         39568 COUNT: 33                                                               
GAGE_Genome size                 4411532                                                                       
GAGE_Assembly size               4312132                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     105824(2.40%)                                                                 
GAGE_Missing assembly bases      912(0.02%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  1500                                                                          
GAGE_Compressed reference bases  33849                                                                         
GAGE_Bad trim                    912                                                                           
GAGE_Avg idy                     99.98                                                                         
GAGE_SNPs                        81                                                                            
GAGE_Indels < 5bp                604                                                                           
GAGE_Indels >= 5                 10                                                                            
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  7                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          223                                                                           
GAGE_Corrected assembly size     4313972                                                                       
GAGE_Min correct contig          349                                                                           
GAGE_Max correct contig          157548                                                                        
GAGE_Corrected N50               34511 COUNT: 38                                                               
