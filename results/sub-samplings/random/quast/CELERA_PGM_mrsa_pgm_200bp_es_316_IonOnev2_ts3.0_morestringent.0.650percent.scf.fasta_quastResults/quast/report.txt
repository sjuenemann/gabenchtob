All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.650percent.scf
#Contigs (>= 0 bp)             553                                                                           
#Contigs (>= 1000 bp)          553                                                                           
Total length (>= 0 bp)         2588622                                                                       
Total length (>= 1000 bp)      2588622                                                                       
#Contigs                       553                                                                           
Largest contig                 29499                                                                         
Total length                   2588622                                                                       
Reference length               2813862                                                                       
GC (%)                         32.61                                                                         
Reference GC (%)               32.81                                                                         
N50                            7032                                                                          
NG50                           6476                                                                          
N75                            3927                                                                          
NG75                           3233                                                                          
#misassemblies                 6                                                                             
#local misassemblies           0                                                                             
#unaligned contigs             0 + 6 part                                                                    
Unaligned contigs length       898                                                                           
Genome fraction (%)            90.363                                                                        
Duplication ratio              1.018                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        7.04                                                                          
#indels per 100 kbp            21.24                                                                         
#genes                         2084 + 447 part                                                               
#predicted genes (unique)      2849                                                                          
#predicted genes (>= 0 bp)     2849                                                                          
#predicted genes (>= 300 bp)   2301                                                                          
#predicted genes (>= 1500 bp)  243                                                                           
#predicted genes (>= 3000 bp)  15                                                                            
Largest alignment              29499                                                                         
NA50                           7026                                                                          
NGA50                          6476                                                                          
NA75                           3894                                                                          
NGA75                          3208                                                                          
