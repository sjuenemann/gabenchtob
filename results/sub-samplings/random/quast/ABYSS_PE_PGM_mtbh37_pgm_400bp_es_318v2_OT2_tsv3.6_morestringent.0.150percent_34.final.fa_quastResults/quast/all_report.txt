All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.150percent_34.final
#Contigs                                   2720                                                                                 
#Contigs (>= 0 bp)                         4755                                                                                 
#Contigs (>= 1000 bp)                      1326                                                                                 
Largest contig                             12159                                                                                
Total length                               3917611                                                                              
Total length (>= 0 bp)                     4088925                                                                              
Total length (>= 1000 bp)                  3176654                                                                              
Reference length                           4411532                                                                              
N50                                        2258                                                                                 
NG50                                       1976                                                                                 
N75                                        1239                                                                                 
NG75                                       866                                                                                  
L50                                        526                                                                                  
LG50                                       643                                                                                  
L75                                        1112                                                                                 
LG75                                       1468                                                                                 
#local misassemblies                       4                                                                                    
#misassemblies                             1                                                                                    
#misassembled contigs                      1                                                                                    
Misassembled contigs length                308                                                                                  
Misassemblies inter-contig overlap         257                                                                                  
#unaligned contigs                         1 + 4 part                                                                           
Unaligned contigs length                   900                                                                                  
#ambiguously mapped contigs                25                                                                                   
Extra bases in ambiguously mapped contigs  -13094                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        88.367                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     2076 + 1886 part                                                                     
#predicted genes (unique)                  6044                                                                                 
#predicted genes (>= 0 bp)                 6044                                                                                 
#predicted genes (>= 300 bp)               4152                                                                                 
#predicted genes (>= 1500 bp)              193                                                                                  
#predicted genes (>= 3000 bp)              6                                                                                    
#mismatches                                162                                                                                  
#indels                                    1325                                                                                 
Indels length                              1398                                                                                 
#mismatches per 100 kbp                    4.16                                                                                 
#indels per 100 kbp                        33.99                                                                                
GC (%)                                     64.91                                                                                
Reference GC (%)                           65.61                                                                                
Average %IDY                               99.944                                                                               
Largest alignment                          12159                                                                                
NA50                                       2258                                                                                 
NGA50                                      1976                                                                                 
NA75                                       1239                                                                                 
NGA75                                      862                                                                                  
LA50                                       526                                                                                  
LGA50                                      643                                                                                  
LA75                                       1112                                                                                 
LGA75                                      1469                                                                                 
#Mis_misassemblies                         1                                                                                    
#Mis_relocations                           1                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  1                                                                                    
Mis_Misassembled contigs length            308                                                                                  
#Mis_local misassemblies                   4                                                                                    
#Mis_short indels (<= 5 bp)                1323                                                                                 
#Mis_long indels (> 5 bp)                  2                                                                                    
#Una_fully unaligned contigs               1                                                                                    
Una_Fully unaligned length                 781                                                                                  
#Una_partially unaligned contigs           4                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             119                                                                                  
GAGE_Contigs #                             2720                                                                                 
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            12159                                                                                
GAGE_N50                                   1976 COUNT: 643                                                                      
GAGE_Genome size                           4411532                                                                              
GAGE_Assembly size                         3917611                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               472791(10.72%)                                                                       
GAGE_Missing assembly bases                941(0.02%)                                                                           
GAGE_Missing assembly contigs              1(0.04%)                                                                             
GAGE_Duplicated reference bases            0                                                                                    
GAGE_Compressed reference bases            28745                                                                                
GAGE_Bad trim                              160                                                                                  
GAGE_Avg idy                               99.96                                                                                
GAGE_SNPs                                  158                                                                                  
GAGE_Indels < 5bp                          1353                                                                                 
GAGE_Indels >= 5                           6                                                                                    
GAGE_Inversions                            1                                                                                    
GAGE_Relocation                            1                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    2724                                                                                 
GAGE_Corrected assembly size               3917605                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    12162                                                                                
GAGE_Corrected N50                         1975 COUNT: 646                                                                      
