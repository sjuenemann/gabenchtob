All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.300percent.result
#Contigs (>= 0 bp)             1027                                                                     
#Contigs (>= 1000 bp)          236                                                                      
Total length (>= 0 bp)         2935056                                                                  
Total length (>= 1000 bp)      2682866                                                                  
#Contigs                       1027                                                                     
Largest contig                 51906                                                                    
Total length                   2935056                                                                  
Reference length               2813862                                                                  
GC (%)                         32.59                                                                    
Reference GC (%)               32.81                                                                    
N50                            17313                                                                    
NG50                           18966                                                                    
N75                            8309                                                                     
NG75                           9412                                                                     
#misassemblies                 3                                                                        
#local misassemblies           5                                                                        
#unaligned contigs             21 + 16 part                                                             
Unaligned contigs length       7446                                                                     
Genome fraction (%)            97.726                                                                   
Duplication ratio              1.061                                                                    
#N's per 100 kbp               65.28                                                                    
#mismatches per 100 kbp        2.98                                                                     
#indels per 100 kbp            24.33                                                                    
#genes                         2464 + 230 part                                                          
#predicted genes (unique)      3468                                                                     
#predicted genes (>= 0 bp)     3490                                                                     
#predicted genes (>= 300 bp)   2432                                                                     
#predicted genes (>= 1500 bp)  252                                                                      
#predicted genes (>= 3000 bp)  21                                                                       
Largest alignment              51906                                                                    
NA50                           17313                                                                    
NGA50                          18966                                                                    
NA75                           8309                                                                     
NGA75                          9242                                                                     
