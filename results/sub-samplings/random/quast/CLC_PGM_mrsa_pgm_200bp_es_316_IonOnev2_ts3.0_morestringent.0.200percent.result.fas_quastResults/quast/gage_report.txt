All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.200percent.result
GAGE_Contigs #                   355                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  70238                                                                         
GAGE_N50                         26710 COUNT: 35                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2778135                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     31825(1.13%)                                                                  
GAGE_Missing assembly bases      1429(0.05%)                                                                   
GAGE_Missing assembly contigs    2(0.56%)                                                                      
GAGE_Duplicated reference bases  26816                                                                         
GAGE_Compressed reference bases  40589                                                                         
GAGE_Bad trim                    992                                                                           
GAGE_Avg idy                     99.98                                                                         
GAGE_SNPs                        54                                                                            
GAGE_Indels < 5bp                535                                                                           
GAGE_Indels >= 5                 2                                                                             
GAGE_Inversions                  1                                                                             
GAGE_Relocation                  2                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          252                                                                           
GAGE_Corrected assembly size     2750907                                                                       
GAGE_Min correct contig          201                                                                           
GAGE_Max correct contig          70247                                                                         
GAGE_Corrected N50               26151 COUNT: 36                                                               
