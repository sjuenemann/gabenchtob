All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.1percent.contigs
GAGE_Contigs #                   567                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  118895                                                                      
GAGE_N50                         28126 COUNT: 58                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               5287200                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     66840(1.19%)                                                                
GAGE_Missing assembly bases      288(0.01%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  736                                                                         
GAGE_Compressed reference bases  248152                                                                      
GAGE_Bad trim                    280                                                                         
GAGE_Avg idy                     99.95                                                                       
GAGE_SNPs                        275                                                                         
GAGE_Indels < 5bp                2120                                                                        
GAGE_Indels >= 5                 12                                                                          
GAGE_Inversions                  2                                                                           
GAGE_Relocation                  6                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          583                                                                         
GAGE_Corrected assembly size     5290906                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          118945                                                                      
GAGE_Corrected N50               27321 COUNT: 61                                                             
