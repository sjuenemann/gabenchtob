All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.0.900percent.contigs
#Contigs (>= 0 bp)             254                                                                                
#Contigs (>= 1000 bp)          144                                                                                
Total length (>= 0 bp)         4303464                                                                            
Total length (>= 1000 bp)      4264389                                                                            
#Contigs                       214                                                                                
Largest contig                 182926                                                                             
Total length                   4297675                                                                            
Reference length               4411532                                                                            
GC (%)                         65.41                                                                              
Reference GC (%)               65.61                                                                              
N50                            63166                                                                              
NG50                           62664                                                                              
N75                            34938                                                                              
NG75                           33590                                                                              
#misassemblies                 0                                                                                  
#local misassemblies           14                                                                                 
#unaligned contigs             1 + 1 part                                                                         
Unaligned contigs length       1616                                                                               
Genome fraction (%)            96.940                                                                             
Duplication ratio              1.005                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        3.72                                                                               
#indels per 100 kbp            17.23                                                                              
#genes                         3930 + 118 part                                                                    
#predicted genes (unique)      4339                                                                               
#predicted genes (>= 0 bp)     4339                                                                               
#predicted genes (>= 300 bp)   3691                                                                               
#predicted genes (>= 1500 bp)  506                                                                                
#predicted genes (>= 3000 bp)  56                                                                                 
Largest alignment              182926                                                                             
NA50                           63166                                                                              
NGA50                          62664                                                                              
NA75                           34938                                                                              
NGA75                          33590                                                                              
