All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.0.4percent.K99.final-contigs
GAGE_Contigs #                   448                                                                       
GAGE_Min contig                  201                                                                       
GAGE_Max contig                  316883                                                                    
GAGE_N50                         124345 COUNT: 15                                                          
GAGE_Genome size                 5594470                                                                   
GAGE_Assembly size               5382952                                                                   
GAGE_Chaff bases                 0                                                                         
GAGE_Missing reference bases     6370(0.11%)                                                               
GAGE_Missing assembly bases      113(0.00%)                                                                
GAGE_Missing assembly contigs    0(0.00%)                                                                  
GAGE_Duplicated reference bases  1804                                                                      
GAGE_Compressed reference bases  273811                                                                    
GAGE_Bad trim                    111                                                                       
GAGE_Avg idy                     99.99                                                                     
GAGE_SNPs                        162                                                                       
GAGE_Indels < 5bp                341                                                                       
GAGE_Indels >= 5                 12                                                                        
GAGE_Inversions                  1                                                                         
GAGE_Relocation                  9                                                                         
GAGE_Translocation               0                                                                         
GAGE_Corrected contig #          463                                                                       
GAGE_Corrected assembly size     5384208                                                                   
GAGE_Min correct contig          201                                                                       
GAGE_Max correct contig          224217                                                                    
GAGE_Corrected N50               95651 COUNT: 19                                                           
