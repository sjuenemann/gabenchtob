All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.175percent.K99.final-contigs
GAGE_Contigs #                   232                                                                                    
GAGE_Min contig                  201                                                                                    
GAGE_Max contig                  259133                                                                                 
GAGE_N50                         81269 COUNT: 21                                                                        
GAGE_Genome size                 5594470                                                                                
GAGE_Assembly size               5317051                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     27259(0.49%)                                                                           
GAGE_Missing assembly bases      874(0.02%)                                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                               
GAGE_Duplicated reference bases  474                                                                                    
GAGE_Compressed reference bases  282418                                                                                 
GAGE_Bad trim                    155                                                                                    
GAGE_Avg idy                     99.94                                                                                  
GAGE_SNPs                        421                                                                                    
GAGE_Indels < 5bp                2308                                                                                   
GAGE_Indels >= 5                 11                                                                                     
GAGE_Inversions                  6                                                                                      
GAGE_Relocation                  14                                                                                     
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          266                                                                                    
GAGE_Corrected assembly size     5332611                                                                                
GAGE_Min correct contig          201                                                                                    
GAGE_Max correct contig          190553                                                                                 
GAGE_Corrected N50               66004 COUNT: 25                                                                        
