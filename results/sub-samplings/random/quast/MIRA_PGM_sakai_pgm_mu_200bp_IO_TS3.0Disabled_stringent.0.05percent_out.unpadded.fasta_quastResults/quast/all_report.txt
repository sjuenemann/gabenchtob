All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.05percent_out.unpadded
#Contigs                                   3824                                                                           
#Contigs (>= 0 bp)                         3865                                                                           
#Contigs (>= 1000 bp)                      1829                                                                           
Largest contig                             15903                                                                          
Total length                               5432461                                                                        
Total length (>= 0 bp)                     5439816                                                                        
Total length (>= 1000 bp)                  4383781                                                                        
Reference length                           5594470                                                                        
N50                                        2279                                                                           
NG50                                       2190                                                                           
N75                                        1231                                                                           
NG75                                       1143                                                                           
L50                                        739                                                                            
LG50                                       775                                                                            
L75                                        1552                                                                           
LG75                                       1654                                                                           
#local misassemblies                       19                                                                             
#misassemblies                             122                                                                            
#misassembled contigs                      117                                                                            
Misassembled contigs length                277603                                                                         
Misassemblies inter-contig overlap         31042                                                                          
#unaligned contigs                         4 + 321 part                                                                   
Unaligned contigs length                   18462                                                                          
#ambiguously mapped contigs                86                                                                             
Extra bases in ambiguously mapped contigs  -52381                                                                         
#N's                                       833                                                                            
#N's per 100 kbp                           15.33                                                                          
Genome fraction (%)                        92.814                                                                         
Duplication ratio                          1.039                                                                          
#genes                                     2961 + 2284 part                                                               
#predicted genes (unique)                  8812                                                                           
#predicted genes (>= 0 bp)                 8813                                                                           
#predicted genes (>= 300 bp)               5660                                                                           
#predicted genes (>= 1500 bp)              248                                                                            
#predicted genes (>= 3000 bp)              7                                                                              
#mismatches                                2462                                                                           
#indels                                    3990                                                                           
Indels length                              4338                                                                           
#mismatches per 100 kbp                    47.42                                                                          
#indels per 100 kbp                        76.84                                                                          
GC (%)                                     50.42                                                                          
Reference GC (%)                           50.48                                                                          
Average %IDY                               99.344                                                                         
Largest alignment                          13576                                                                          
NA50                                       2217                                                                           
NGA50                                      2131                                                                           
NA75                                       1179                                                                           
NGA75                                      1096                                                                           
LA50                                       753                                                                            
LGA50                                      791                                                                            
LA75                                       1589                                                                           
LGA75                                      1695                                                                           
#Mis_misassemblies                         122                                                                            
#Mis_relocations                           47                                                                             
#Mis_translocations                        0                                                                              
#Mis_inversions                            75                                                                             
#Mis_misassembled contigs                  117                                                                            
Mis_Misassembled contigs length            277603                                                                         
#Mis_local misassemblies                   19                                                                             
#Mis_short indels (<= 5 bp)                3971                                                                           
#Mis_long indels (> 5 bp)                  19                                                                             
#Una_fully unaligned contigs               4                                                                              
Una_Fully unaligned length                 4680                                                                           
#Una_partially unaligned contigs           321                                                                            
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            2                                                                              
Una_Partially unaligned length             13782                                                                          
GAGE_Contigs #                             3824                                                                           
GAGE_Min contig                            201                                                                            
GAGE_Max contig                            15903                                                                          
GAGE_N50                                   2190 COUNT: 775                                                                
GAGE_Genome size                           5594470                                                                        
GAGE_Assembly size                         5432461                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               189257(3.38%)                                                                  
GAGE_Missing assembly bases                18907(0.35%)                                                                   
GAGE_Missing assembly contigs              1(0.03%)                                                                       
GAGE_Duplicated reference bases            169727                                                                         
GAGE_Compressed reference bases            262323                                                                         
GAGE_Bad trim                              17382                                                                          
GAGE_Avg idy                               99.86                                                                          
GAGE_SNPs                                  1197                                                                           
GAGE_Indels < 5bp                          3777                                                                           
GAGE_Indels >= 5                           12                                                                             
GAGE_Inversions                            34                                                                             
GAGE_Relocation                            20                                                                             
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    3426                                                                           
GAGE_Corrected assembly size               5274290                                                                        
GAGE_Min correct contig                    201                                                                            
GAGE_Max correct contig                    13051                                                                          
GAGE_Corrected N50                         2141 COUNT: 791                                                                
