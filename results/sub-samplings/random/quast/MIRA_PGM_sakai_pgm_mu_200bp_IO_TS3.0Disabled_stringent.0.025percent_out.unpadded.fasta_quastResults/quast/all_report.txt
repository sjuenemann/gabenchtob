All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.025percent_out.unpadded
#Contigs                                   6603                                                                            
#Contigs (>= 0 bp)                         6693                                                                            
#Contigs (>= 1000 bp)                      493                                                                             
Largest contig                             9436                                                                            
Total length                               3550712                                                                         
Total length (>= 0 bp)                     3566799                                                                         
Total length (>= 1000 bp)                  682980                                                                          
Reference length                           5594470                                                                         
N50                                        600                                                                             
NG50                                       385                                                                             
N75                                        410                                                                             
NG75                                       None                                                                            
L50                                        1957                                                                            
LG50                                       4101                                                                            
L75                                        3763                                                                            
LG75                                       None                                                                            
#local misassemblies                       8                                                                               
#misassemblies                             91                                                                              
#misassembled contigs                      82                                                                              
Misassembled contigs length                76098                                                                           
Misassemblies inter-contig overlap         16598                                                                           
#unaligned contigs                         3 + 376 part                                                                    
Unaligned contigs length                   20161                                                                           
#ambiguously mapped contigs                76                                                                              
Extra bases in ambiguously mapped contigs  -46893                                                                          
#N's                                       1179                                                                            
#N's per 100 kbp                           33.20                                                                           
Genome fraction (%)                        61.799                                                                          
Duplication ratio                          1.012                                                                           
#genes                                     682 + 3928 part                                                                 
#predicted genes (unique)                  8714                                                                            
#predicted genes (>= 0 bp)                 8716                                                                            
#predicted genes (>= 300 bp)               4145                                                                            
#predicted genes (>= 1500 bp)              10                                                                              
#predicted genes (>= 3000 bp)              0                                                                               
#mismatches                                1929                                                                            
#indels                                    4605                                                                            
Indels length                              4777                                                                            
#mismatches per 100 kbp                    55.79                                                                           
#indels per 100 kbp                        133.19                                                                          
GC (%)                                     50.67                                                                           
Reference GC (%)                           50.48                                                                           
Average %IDY                               99.567                                                                          
Largest alignment                          9420                                                                            
NA50                                       584                                                                             
NGA50                                      371                                                                             
NA75                                       396                                                                             
NGA75                                      None                                                                            
LA50                                       2008                                                                            
LGA50                                      4216                                                                            
LA75                                       3866                                                                            
LGA75                                      None                                                                            
#Mis_misassemblies                         91                                                                              
#Mis_relocations                           37                                                                              
#Mis_translocations                        0                                                                               
#Mis_inversions                            54                                                                              
#Mis_misassembled contigs                  82                                                                              
Mis_Misassembled contigs length            76098                                                                           
#Mis_local misassemblies                   8                                                                               
#Mis_short indels (<= 5 bp)                4602                                                                            
#Mis_long indels (> 5 bp)                  3                                                                               
#Una_fully unaligned contigs               3                                                                               
Una_Fully unaligned length                 3280                                                                            
#Una_partially unaligned contigs           376                                                                             
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            7                                                                               
Una_Partially unaligned length             16881                                                                           
GAGE_Contigs #                             6603                                                                            
GAGE_Min contig                            200                                                                             
GAGE_Max contig                            9436                                                                            
GAGE_N50                                   385 COUNT: 4101                                                                 
GAGE_Genome size                           5594470                                                                         
GAGE_Assembly size                         3550712                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               1878137(33.57%)                                                                 
GAGE_Missing assembly bases                20747(0.58%)                                                                    
GAGE_Missing assembly contigs              3(0.05%)                                                                        
GAGE_Duplicated reference bases            62441                                                                           
GAGE_Compressed reference bases            298538                                                                          
GAGE_Bad trim                              17435                                                                           
GAGE_Avg idy                               99.78                                                                           
GAGE_SNPs                                  1280                                                                            
GAGE_Indels < 5bp                          4544                                                                            
GAGE_Indels >= 5                           9                                                                               
GAGE_Inversions                            36                                                                              
GAGE_Relocation                            15                                                                              
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    6483                                                                            
GAGE_Corrected assembly size               3482926                                                                         
GAGE_Min correct contig                    200                                                                             
GAGE_Max correct contig                    7942                                                                            
GAGE_Corrected N50                         375 COUNT: 4167                                                                 
