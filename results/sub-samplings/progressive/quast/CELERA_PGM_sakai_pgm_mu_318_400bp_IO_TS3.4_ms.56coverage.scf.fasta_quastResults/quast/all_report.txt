All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.56coverage.scf
#Contigs                                   214                                                         
#Contigs (>= 0 bp)                         214                                                         
#Contigs (>= 1000 bp)                      214                                                         
Largest contig                             221500                                                      
Total length                               5238744                                                     
Total length (>= 0 bp)                     5238744                                                     
Total length (>= 1000 bp)                  5238744                                                     
Reference length                           5594470                                                     
N50                                        61204                                                       
NG50                                       57346                                                       
N75                                        38231                                                       
NG75                                       33311                                                       
L50                                        25                                                          
LG50                                       28                                                          
L75                                        53                                                          
LG75                                       60                                                          
#local misassemblies                       6                                                           
#misassemblies                             3                                                           
#misassembled contigs                      3                                                           
Misassembled contigs length                238748                                                      
Misassemblies inter-contig overlap         1822                                                        
#unaligned contigs                         0 + 0 part                                                  
Unaligned contigs length                   0                                                           
#ambiguously mapped contigs                12                                                          
Extra bases in ambiguously mapped contigs  -16124                                                      
#N's                                       0                                                           
#N's per 100 kbp                           0.00                                                        
Genome fraction (%)                        92.947                                                      
Duplication ratio                          1.005                                                       
#genes                                     4900 + 190 part                                             
#predicted genes (unique)                  5255                                                        
#predicted genes (>= 0 bp)                 5256                                                        
#predicted genes (>= 300 bp)               4454                                                        
#predicted genes (>= 1500 bp)              627                                                         
#predicted genes (>= 3000 bp)              59                                                          
#mismatches                                113                                                         
#indels                                    467                                                         
Indels length                              471                                                         
#mismatches per 100 kbp                    2.17                                                        
#indels per 100 kbp                        8.98                                                        
GC (%)                                     50.31                                                       
Reference GC (%)                           50.48                                                       
Average %IDY                               98.519                                                      
Largest alignment                          221500                                                      
NA50                                       61204                                                       
NGA50                                      57346                                                       
NA75                                       38231                                                       
NGA75                                      33311                                                       
LA50                                       26                                                          
LGA50                                      29                                                          
LA75                                       54                                                          
LGA75                                      61                                                          
#Mis_misassemblies                         3                                                           
#Mis_relocations                           3                                                           
#Mis_translocations                        0                                                           
#Mis_inversions                            0                                                           
#Mis_misassembled contigs                  3                                                           
Mis_Misassembled contigs length            238748                                                      
#Mis_local misassemblies                   6                                                           
#Mis_short indels (<= 5 bp)                467                                                         
#Mis_long indels (> 5 bp)                  0                                                           
#Una_fully unaligned contigs               0                                                           
Una_Fully unaligned length                 0                                                           
#Una_partially unaligned contigs           0                                                           
#Una_with misassembly                      0                                                           
#Una_both parts are significant            0                                                           
Una_Partially unaligned length             0                                                           
GAGE_Contigs #                             214                                                         
GAGE_Min contig                            1006                                                        
GAGE_Max contig                            221500                                                      
GAGE_N50                                   57346 COUNT: 28                                             
GAGE_Genome size                           5594470                                                     
GAGE_Assembly size                         5238744                                                     
GAGE_Chaff bases                           0                                                           
GAGE_Missing reference bases               307908(5.50%)                                               
GAGE_Missing assembly bases                25(0.00%)                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                    
GAGE_Duplicated reference bases            6996                                                        
GAGE_Compressed reference bases            123563                                                      
GAGE_Bad trim                              25                                                          
GAGE_Avg idy                               99.98                                                       
GAGE_SNPs                                  108                                                         
GAGE_Indels < 5bp                          441                                                         
GAGE_Indels >= 5                           4                                                           
GAGE_Inversions                            3                                                           
GAGE_Relocation                            4                                                           
GAGE_Translocation                         0                                                           
GAGE_Corrected contig #                    219                                                         
GAGE_Corrected assembly size               5235046                                                     
GAGE_Min correct contig                    240                                                         
GAGE_Max correct contig                    155524                                                      
GAGE_Corrected N50                         54849 COUNT: 31                                             
