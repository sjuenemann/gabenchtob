All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.96coverage.contigs
#Contigs (>= 0 bp)             402                                                                                                   
#Contigs (>= 1000 bp)          158                                                                                                   
Total length (>= 0 bp)         5356568                                                                                               
Total length (>= 1000 bp)      5272941                                                                                               
#Contigs                       327                                                                                                   
Largest contig                 374702                                                                                                
Total length                   5345987                                                                                               
Reference length               5594470                                                                                               
GC (%)                         50.28                                                                                                 
Reference GC (%)               50.48                                                                                                 
N50                            123777                                                                                                
NG50                           116968                                                                                                
N75                            55527                                                                                                 
NG75                           44605                                                                                                 
#misassemblies                 3                                                                                                     
#local misassemblies           2                                                                                                     
#unaligned contigs             6 + 0 part                                                                                            
Unaligned contigs length       7345                                                                                                  
Genome fraction (%)            93.900                                                                                                
Duplication ratio              1.000                                                                                                 
#N's per 100 kbp               0.00                                                                                                  
#mismatches per 100 kbp        1.31                                                                                                  
#indels per 100 kbp            0.29                                                                                                  
#genes                         4942 + 167 part                                                                                       
#predicted genes (unique)      5286                                                                                                  
#predicted genes (>= 0 bp)     5287                                                                                                  
#predicted genes (>= 300 bp)   4425                                                                                                  
#predicted genes (>= 1500 bp)  664                                                                                                   
#predicted genes (>= 3000 bp)  67                                                                                                    
Largest alignment              254324                                                                                                
NA50                           123777                                                                                                
NGA50                          116968                                                                                                
NA75                           55527                                                                                                 
NGA75                          44605                                                                                                 
