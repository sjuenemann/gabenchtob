All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.16coverage.contigs
#Contigs                                   483                                                                                                   
#Contigs (>= 0 bp)                         544                                                                                                   
#Contigs (>= 1000 bp)                      301                                                                                                   
Largest contig                             131598                                                                                                
Total length                               5299108                                                                                               
Total length (>= 0 bp)                     5308288                                                                                               
Total length (>= 1000 bp)                  5222672                                                                                               
Reference length                           5594470                                                                                               
N50                                        39570                                                                                                 
NG50                                       37873                                                                                                 
N75                                        19773                                                                                                 
NG75                                       16929                                                                                                 
L50                                        43                                                                                                    
LG50                                       47                                                                                                    
L75                                        91                                                                                                    
LG75                                       103                                                                                                   
#local misassemblies                       6                                                                                                     
#misassemblies                             3                                                                                                     
#misassembled contigs                      3                                                                                                     
Misassembled contigs length                100507                                                                                                
Misassemblies inter-contig overlap         1201                                                                                                  
#unaligned contigs                         2 + 0 part                                                                                            
Unaligned contigs length                   5599                                                                                                  
#ambiguously mapped contigs                101                                                                                                   
Extra bases in ambiguously mapped contigs  -80859                                                                                                
#N's                                       1                                                                                                     
#N's per 100 kbp                           0.02                                                                                                  
Genome fraction (%)                        93.166                                                                                                
Duplication ratio                          1.000                                                                                                 
#genes                                     4775 + 283 part                                                                                       
#predicted genes (unique)                  5328                                                                                                  
#predicted genes (>= 0 bp)                 5328                                                                                                  
#predicted genes (>= 300 bp)               4416                                                                                                  
#predicted genes (>= 1500 bp)              655                                                                                                   
#predicted genes (>= 3000 bp)              67                                                                                                    
#mismatches                                229                                                                                                   
#indels                                    30                                                                                                    
Indels length                              32                                                                                                    
#mismatches per 100 kbp                    4.39                                                                                                  
#indels per 100 kbp                        0.58                                                                                                  
GC (%)                                     50.28                                                                                                 
Reference GC (%)                           50.48                                                                                                 
Average %IDY                               99.193                                                                                                
Largest alignment                          131598                                                                                                
NA50                                       38743                                                                                                 
NGA50                                      37540                                                                                                 
NA75                                       19674                                                                                                 
NGA75                                      16911                                                                                                 
LA50                                       43                                                                                                    
LGA50                                      47                                                                                                    
LA75                                       92                                                                                                    
LGA75                                      104                                                                                                   
#Mis_misassemblies                         3                                                                                                     
#Mis_relocations                           3                                                                                                     
#Mis_translocations                        0                                                                                                     
#Mis_inversions                            0                                                                                                     
#Mis_misassembled contigs                  3                                                                                                     
Mis_Misassembled contigs length            100507                                                                                                
#Mis_local misassemblies                   6                                                                                                     
#Mis_short indels (<= 5 bp)                30                                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                                     
#Una_fully unaligned contigs               2                                                                                                     
Una_Fully unaligned length                 5599                                                                                                  
#Una_partially unaligned contigs           0                                                                                                     
#Una_with misassembly                      0                                                                                                     
#Una_both parts are significant            0                                                                                                     
Una_Partially unaligned length             0                                                                                                     
GAGE_Contigs #                             483                                                                                                   
GAGE_Min contig                            201                                                                                                   
GAGE_Max contig                            131598                                                                                                
GAGE_N50                                   37873 COUNT: 47                                                                                       
GAGE_Genome size                           5594470                                                                                               
GAGE_Assembly size                         5299108                                                                                               
GAGE_Chaff bases                           0                                                                                                     
GAGE_Missing reference bases               50438(0.90%)                                                                                          
GAGE_Missing assembly bases                5392(0.10%)                                                                                           
GAGE_Missing assembly contigs              1(0.21%)                                                                                              
GAGE_Duplicated reference bases            992                                                                                                   
GAGE_Compressed reference bases            261562                                                                                                
GAGE_Bad trim                              6                                                                                                     
GAGE_Avg idy                               99.99                                                                                                 
GAGE_SNPs                                  307                                                                                                   
GAGE_Indels < 5bp                          29                                                                                                    
GAGE_Indels >= 5                           6                                                                                                     
GAGE_Inversions                            1                                                                                                     
GAGE_Relocation                            3                                                                                                     
GAGE_Translocation                         0                                                                                                     
GAGE_Corrected contig #                    489                                                                                                   
GAGE_Corrected assembly size               5295053                                                                                               
GAGE_Min correct contig                    201                                                                                                   
GAGE_Max correct contig                    123517                                                                                                
GAGE_Corrected N50                         35431 COUNT: 48                                                                                       
