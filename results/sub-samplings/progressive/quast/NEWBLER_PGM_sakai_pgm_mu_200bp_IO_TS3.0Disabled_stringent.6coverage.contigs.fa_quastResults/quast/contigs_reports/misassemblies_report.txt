All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.6coverage.contigs
#Mis_misassemblies               94                                                                         
#Mis_relocations                 6                                                                          
#Mis_translocations              0                                                                          
#Mis_inversions                  88                                                                         
#Mis_misassembled contigs        93                                                                         
Mis_Misassembled contigs length  141158                                                                     
#Mis_local misassemblies         9                                                                          
#mismatches                      748                                                                        
#indels                          4142                                                                       
#Mis_short indels (<= 5 bp)      4126                                                                       
#Mis_long indels (> 5 bp)        16                                                                         
Indels length                    4597                                                                       
