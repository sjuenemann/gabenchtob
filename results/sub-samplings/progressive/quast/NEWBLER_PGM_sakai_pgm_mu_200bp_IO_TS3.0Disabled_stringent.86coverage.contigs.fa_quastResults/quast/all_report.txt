All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.86coverage.contigs
#Contigs                                   373                                                                         
#Contigs (>= 0 bp)                         497                                                                         
#Contigs (>= 1000 bp)                      178                                                                         
Largest contig                             296001                                                                      
Total length                               5311767                                                                     
Total length (>= 0 bp)                     5328965                                                                     
Total length (>= 1000 bp)                  5231720                                                                     
Reference length                           5594470                                                                     
N50                                        83003                                                                       
NG50                                       82955                                                                       
N75                                        40859                                                                       
NG75                                       38538                                                                       
L50                                        18                                                                          
LG50                                       20                                                                          
L75                                        39                                                                          
LG75                                       44                                                                          
#local misassemblies                       4                                                                           
#misassemblies                             8                                                                           
#misassembled contigs                      8                                                                           
Misassembled contigs length                340463                                                                      
Misassemblies inter-contig overlap         773                                                                         
#unaligned contigs                         0 + 2 part                                                                  
Unaligned contigs length                   89                                                                          
#ambiguously mapped contigs                127                                                                         
Extra bases in ambiguously mapped contigs  -87573                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        93.393                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     4889 + 196 part                                                             
#predicted genes (unique)                  5680                                                                        
#predicted genes (>= 0 bp)                 5680                                                                        
#predicted genes (>= 300 bp)               4637                                                                        
#predicted genes (>= 1500 bp)              572                                                                         
#predicted genes (>= 3000 bp)              50                                                                          
#mismatches                                63                                                                          
#indels                                    887                                                                         
Indels length                              927                                                                         
#mismatches per 100 kbp                    1.21                                                                        
#indels per 100 kbp                        16.98                                                                       
GC (%)                                     50.27                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.072                                                                      
Largest alignment                          259338                                                                      
NA50                                       82985                                                                       
NGA50                                      78651                                                                       
NA75                                       40859                                                                       
NGA75                                      32964                                                                       
LA50                                       19                                                                          
LGA50                                      21                                                                          
LA75                                       40                                                                          
LGA75                                      45                                                                          
#Mis_misassemblies                         8                                                                           
#Mis_relocations                           4                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            4                                                                           
#Mis_misassembled contigs                  8                                                                           
Mis_Misassembled contigs length            340463                                                                      
#Mis_local misassemblies                   4                                                                           
#Mis_short indels (<= 5 bp)                886                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           2                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             89                                                                          
GAGE_Contigs #                             373                                                                         
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            296001                                                                      
GAGE_N50                                   82955 COUNT: 20                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         5311767                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               82788(1.48%)                                                                
GAGE_Missing assembly bases                242(0.00%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            1828                                                                        
GAGE_Compressed reference bases            210895                                                                      
GAGE_Bad trim                              242                                                                         
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  101                                                                         
GAGE_Indels < 5bp                          945                                                                         
GAGE_Indels >= 5                           5                                                                           
GAGE_Inversions                            1                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    372                                                                         
GAGE_Corrected assembly size               5311151                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    238202                                                                      
GAGE_Corrected N50                         76122 COUNT: 22                                                             
