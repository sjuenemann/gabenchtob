All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.41coverage.contigs
#Contigs                                   355                                                                         
#Contigs (>= 0 bp)                         442                                                                         
#Contigs (>= 1000 bp)                      163                                                                         
Largest contig                             259601                                                                      
Total length                               5321540                                                                     
Total length (>= 0 bp)                     5334080                                                                     
Total length (>= 1000 bp)                  5241781                                                                     
Reference length                           5594470                                                                     
N50                                        103702                                                                      
NG50                                       100259                                                                      
N75                                        44539                                                                       
NG75                                       39846                                                                       
L50                                        16                                                                          
LG50                                       17                                                                          
L75                                        35                                                                          
LG75                                       39                                                                          
#local misassemblies                       5                                                                           
#misassemblies                             4                                                                           
#misassembled contigs                      4                                                                           
Misassembled contigs length                223232                                                                      
Misassemblies inter-contig overlap         897                                                                         
#unaligned contigs                         0 + 0 part                                                                  
Unaligned contigs length                   0                                                                           
#ambiguously mapped contigs                123                                                                         
Extra bases in ambiguously mapped contigs  -87993                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        93.571                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     4909 + 183 part                                                             
#predicted genes (unique)                  5793                                                                        
#predicted genes (>= 0 bp)                 5793                                                                        
#predicted genes (>= 300 bp)               4692                                                                        
#predicted genes (>= 1500 bp)              548                                                                         
#predicted genes (>= 3000 bp)              41                                                                          
#mismatches                                73                                                                          
#indels                                    1126                                                                        
Indels length                              1177                                                                        
#mismatches per 100 kbp                    1.39                                                                        
#indels per 100 kbp                        21.51                                                                       
GC (%)                                     50.27                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               98.995                                                                      
Largest alignment                          259601                                                                      
NA50                                       103702                                                                      
NGA50                                      100259                                                                      
NA75                                       44539                                                                       
NGA75                                      38550                                                                       
LA50                                       16                                                                          
LGA50                                      17                                                                          
LA75                                       35                                                                          
LGA75                                      40                                                                          
#Mis_misassemblies                         4                                                                           
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            1                                                                           
#Mis_misassembled contigs                  4                                                                           
Mis_Misassembled contigs length            223232                                                                      
#Mis_local misassemblies                   5                                                                           
#Mis_short indels (<= 5 bp)                1125                                                                        
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           0                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             0                                                                           
GAGE_Contigs #                             355                                                                         
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            259601                                                                      
GAGE_N50                                   100259 COUNT: 17                                                            
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         5321540                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               59850(1.07%)                                                                
GAGE_Missing assembly bases                1(0.00%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            469                                                                         
GAGE_Compressed reference bases            220622                                                                      
GAGE_Bad trim                              1                                                                           
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  95                                                                          
GAGE_Indels < 5bp                          1192                                                                        
GAGE_Indels >= 5                           6                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    362                                                                         
GAGE_Corrected assembly size               5323151                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    238193                                                                      
GAGE_Corrected N50                         95575 COUNT: 19                                                             
