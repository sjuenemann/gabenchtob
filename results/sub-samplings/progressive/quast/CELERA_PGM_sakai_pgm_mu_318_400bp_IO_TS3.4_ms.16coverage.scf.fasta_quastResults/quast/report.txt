All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.16coverage.scf
#Contigs (>= 0 bp)             249                                                         
#Contigs (>= 1000 bp)          249                                                         
Total length (>= 0 bp)         5187481                                                     
Total length (>= 1000 bp)      5187481                                                     
#Contigs                       249                                                         
Largest contig                 196987                                                      
Total length                   5187481                                                     
Reference length               5594470                                                     
GC (%)                         50.30                                                       
Reference GC (%)               50.48                                                       
N50                            61309                                                       
NG50                           54470                                                       
N75                            26682                                                       
NG75                           20802                                                       
#misassemblies                 2                                                           
#local misassemblies           8                                                           
#unaligned contigs             0 + 0 part                                                  
Unaligned contigs length       0                                                           
Genome fraction (%)            92.464                                                      
Duplication ratio              1.002                                                       
#N's per 100 kbp               0.00                                                        
#mismatches per 100 kbp        1.66                                                        
#indels per 100 kbp            21.86                                                       
#genes                         4839 + 223 part                                             
#predicted genes (unique)      5399                                                        
#predicted genes (>= 0 bp)     5400                                                        
#predicted genes (>= 300 bp)   4478                                                        
#predicted genes (>= 1500 bp)  591                                                         
#predicted genes (>= 3000 bp)  52                                                          
Largest alignment              196983                                                      
NA50                           61309                                                       
NGA50                          45951                                                       
NA75                           26668                                                       
NGA75                          20713                                                       
