All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.81coverage_out.unpadded
#Contigs                                   209                                                                                             
#Contigs (>= 0 bp)                         240                                                                                             
#Contigs (>= 1000 bp)                      37                                                                                              
Largest contig                             368965                                                                                          
Total length                               2889872                                                                                         
Total length (>= 0 bp)                     2894986                                                                                         
Total length (>= 1000 bp)                  2822722                                                                                         
Reference length                           2813862                                                                                         
N50                                        172103                                                                                          
NG50                                       172103                                                                                          
N75                                        85674                                                                                           
NG75                                       87611                                                                                           
L50                                        6                                                                                               
LG50                                       6                                                                                               
L75                                        12                                                                                              
LG75                                       11                                                                                              
#local misassemblies                       4                                                                                               
#misassemblies                             15                                                                                              
#misassembled contigs                      12                                                                                              
Misassembled contigs length                1041100                                                                                         
Misassemblies inter-contig overlap         14284                                                                                           
#unaligned contigs                         2 + 0 part                                                                                      
Unaligned contigs length                   6196                                                                                            
#ambiguously mapped contigs                14                                                                                              
Extra bases in ambiguously mapped contigs  -6976                                                                                           
#N's                                       12                                                                                              
#N's per 100 kbp                           0.42                                                                                            
Genome fraction (%)                        99.809                                                                                          
Duplication ratio                          1.029                                                                                           
#genes                                     2709 + 15 part                                                                                  
#predicted genes (unique)                  2814                                                                                            
#predicted genes (>= 0 bp)                 2834                                                                                            
#predicted genes (>= 300 bp)               2383                                                                                            
#predicted genes (>= 1500 bp)              303                                                                                             
#predicted genes (>= 3000 bp)              30                                                                                              
#mismatches                                153                                                                                             
#indels                                    85                                                                                              
Indels length                              181                                                                                             
#mismatches per 100 kbp                    5.45                                                                                            
#indels per 100 kbp                        3.03                                                                                            
GC (%)                                     32.92                                                                                           
Reference GC (%)                           32.81                                                                                           
Average %IDY                               98.940                                                                                          
Largest alignment                          337357                                                                                          
NA50                                       112608                                                                                          
NGA50                                      114859                                                                                          
NA75                                       75917                                                                                           
NGA75                                      80357                                                                                           
LA50                                       7                                                                                               
LGA50                                      6                                                                                               
LA75                                       15                                                                                              
LGA75                                      14                                                                                              
#Mis_misassemblies                         15                                                                                              
#Mis_relocations                           15                                                                                              
#Mis_translocations                        0                                                                                               
#Mis_inversions                            0                                                                                               
#Mis_misassembled contigs                  12                                                                                              
Mis_Misassembled contigs length            1041100                                                                                         
#Mis_local misassemblies                   4                                                                                               
#Mis_short indels (<= 5 bp)                81                                                                                              
#Mis_long indels (> 5 bp)                  4                                                                                               
#Una_fully unaligned contigs               2                                                                                               
Una_Fully unaligned length                 6196                                                                                            
#Una_partially unaligned contigs           0                                                                                               
#Una_with misassembly                      0                                                                                               
#Una_both parts are significant            0                                                                                               
Una_Partially unaligned length             0                                                                                               
GAGE_Contigs #                             209                                                                                             
GAGE_Min contig                            200                                                                                             
GAGE_Max contig                            368965                                                                                          
GAGE_N50                                   172103 COUNT: 6                                                                                 
GAGE_Genome size                           2813862                                                                                         
GAGE_Assembly size                         2889872                                                                                         
GAGE_Chaff bases                           0                                                                                               
GAGE_Missing reference bases               1027(0.04%)                                                                                     
GAGE_Missing assembly bases                6215(0.22%)                                                                                     
GAGE_Missing assembly contigs              2(0.96%)                                                                                        
GAGE_Duplicated reference bases            80020                                                                                           
GAGE_Compressed reference bases            14154                                                                                           
GAGE_Bad trim                              19                                                                                              
GAGE_Avg idy                               99.99                                                                                           
GAGE_SNPs                                  51                                                                                              
GAGE_Indels < 5bp                          9                                                                                               
GAGE_Indels >= 5                           2                                                                                               
GAGE_Inversions                            6                                                                                               
GAGE_Relocation                            3                                                                                               
GAGE_Translocation                         0                                                                                               
GAGE_Corrected contig #                    64                                                                                              
GAGE_Corrected assembly size               2824706                                                                                         
GAGE_Min correct contig                    203                                                                                             
GAGE_Max correct contig                    336637                                                                                          
GAGE_Corrected N50                         115569 COUNT: 6                                                                                 
