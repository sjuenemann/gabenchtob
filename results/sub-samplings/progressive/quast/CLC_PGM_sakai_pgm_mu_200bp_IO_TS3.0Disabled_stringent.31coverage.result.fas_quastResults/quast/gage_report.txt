All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.31coverage.result
GAGE_Contigs #                   803                                                                    
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  106236                                                                 
GAGE_N50                         25228 COUNT: 65                                                        
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5365867                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     60203(1.08%)                                                           
GAGE_Missing assembly bases      1315(0.02%)                                                            
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  40957                                                                  
GAGE_Compressed reference bases  239370                                                                 
GAGE_Bad trim                    1315                                                                   
GAGE_Avg idy                     99.96                                                                  
GAGE_SNPs                        105                                                                    
GAGE_Indels < 5bp                1686                                                                   
GAGE_Indels >= 5                 7                                                                      
GAGE_Inversions                  1                                                                      
GAGE_Relocation                  4                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          658                                                                    
GAGE_Corrected assembly size     5327797                                                                
GAGE_Min correct contig          201                                                                    
GAGE_Max correct contig          106275                                                                 
GAGE_Corrected N50               24831 COUNT: 66                                                        
