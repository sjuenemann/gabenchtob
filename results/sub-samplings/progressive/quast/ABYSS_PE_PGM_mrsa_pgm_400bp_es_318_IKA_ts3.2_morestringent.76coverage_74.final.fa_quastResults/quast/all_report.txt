All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.76coverage_74.final
#Contigs                                   437                                                                           
#Contigs (>= 0 bp)                         3988                                                                          
#Contigs (>= 1000 bp)                      312                                                                           
Largest contig                             58599                                                                         
Total length                               2751735                                                                       
Total length (>= 0 bp)                     3140221                                                                       
Total length (>= 1000 bp)                  2694082                                                                       
Reference length                           2813862                                                                       
N50                                        14162                                                                         
NG50                                       13797                                                                         
N75                                        7238                                                                          
NG75                                       6888                                                                          
L50                                        60                                                                            
LG50                                       62                                                                            
L75                                        127                                                                           
LG75                                       134                                                                           
#local misassemblies                       3                                                                             
#misassemblies                             2                                                                             
#misassembled contigs                      2                                                                             
Misassembled contigs length                12161                                                                         
Misassemblies inter-contig overlap         717                                                                           
#unaligned contigs                         0 + 0 part                                                                    
Unaligned contigs length                   0                                                                             
#ambiguously mapped contigs                9                                                                             
Extra bases in ambiguously mapped contigs  -3235                                                                         
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        97.521                                                                        
Duplication ratio                          1.002                                                                         
#genes                                     2421 + 237 part                                                               
#predicted genes (unique)                  2883                                                                          
#predicted genes (>= 0 bp)                 2886                                                                          
#predicted genes (>= 300 bp)               2363                                                                          
#predicted genes (>= 1500 bp)              268                                                                           
#predicted genes (>= 3000 bp)              23                                                                            
#mismatches                                69                                                                            
#indels                                    126                                                                           
Indels length                              135                                                                           
#mismatches per 100 kbp                    2.51                                                                          
#indels per 100 kbp                        4.59                                                                          
GC (%)                                     32.60                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               99.339                                                                        
Largest alignment                          58599                                                                         
NA50                                       14162                                                                         
NGA50                                      13797                                                                         
NA75                                       7238                                                                          
NGA75                                      6888                                                                          
LA50                                       60                                                                            
LGA50                                      62                                                                            
LA75                                       127                                                                           
LGA75                                      134                                                                           
#Mis_misassemblies                         2                                                                             
#Mis_relocations                           2                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  2                                                                             
Mis_Misassembled contigs length            12161                                                                         
#Mis_local misassemblies                   3                                                                             
#Mis_short indels (<= 5 bp)                125                                                                           
#Mis_long indels (> 5 bp)                  1                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           0                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             0                                                                             
GAGE_Contigs #                             437                                                                           
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            58599                                                                         
GAGE_N50                                   13797 COUNT: 62                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         2751735                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               53521(1.90%)                                                                  
GAGE_Missing assembly bases                5(0.00%)                                                                      
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            1125                                                                          
GAGE_Compressed reference bases            20724                                                                         
GAGE_Bad trim                              5                                                                             
GAGE_Avg idy                               99.99                                                                         
GAGE_SNPs                                  45                                                                            
GAGE_Indels < 5bp                          120                                                                           
GAGE_Indels >= 5                           3                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            2                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    438                                                                           
GAGE_Corrected assembly size               2751421                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    58603                                                                         
GAGE_Corrected N50                         13586 COUNT: 63                                                               
