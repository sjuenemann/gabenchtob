All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.61coverage.contigs
#Contigs (>= 0 bp)             437                                                                                             
#Contigs (>= 1000 bp)          160                                                                                             
Total length (>= 0 bp)         5323481                                                                                         
Total length (>= 1000 bp)      5228689                                                                                         
#Contigs                       359                                                                                             
Largest contig                 224048                                                                                          
Total length                   5312071                                                                                         
Reference length               5594470                                                                                         
GC (%)                         50.27                                                                                           
Reference GC (%)               50.48                                                                                           
N50                            95677                                                                                           
NG50                           93089                                                                                           
N75                            44605                                                                                           
NG75                           41204                                                                                           
#misassemblies                 4                                                                                               
#local misassemblies           2                                                                                               
#unaligned contigs             1 + 0 part                                                                                      
Unaligned contigs length       5386                                                                                            
Genome fraction (%)            93.246                                                                                          
Duplication ratio              1.000                                                                                           
#N's per 100 kbp               0.00                                                                                            
#mismatches per 100 kbp        2.20                                                                                            
#indels per 100 kbp            0.23                                                                                            
#genes                         4886 + 169 part                                                                                 
#predicted genes (unique)      5263                                                                                            
#predicted genes (>= 0 bp)     5263                                                                                            
#predicted genes (>= 300 bp)   4396                                                                                            
#predicted genes (>= 1500 bp)  666                                                                                             
#predicted genes (>= 3000 bp)  68                                                                                              
Largest alignment              224048                                                                                          
NA50                           95677                                                                                           
NGA50                          92039                                                                                           
NA75                           44549                                                                                           
NGA75                          40537                                                                                           
