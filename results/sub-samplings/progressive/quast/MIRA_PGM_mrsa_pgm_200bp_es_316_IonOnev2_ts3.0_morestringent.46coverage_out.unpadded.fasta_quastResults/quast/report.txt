All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.46coverage_out.unpadded
#Contigs (>= 0 bp)             344                                                                                
#Contigs (>= 1000 bp)          75                                                                                 
Total length (>= 0 bp)         2908761                                                                            
Total length (>= 1000 bp)      2807380                                                                            
#Contigs                       325                                                                                
Largest contig                 326626                                                                             
Total length                   2905989                                                                            
Reference length               2813862                                                                            
GC (%)                         32.75                                                                              
Reference GC (%)               32.81                                                                              
N50                            129669                                                                             
NG50                           129669                                                                             
N75                            54452                                                                              
NG75                           55553                                                                              
#misassemblies                 46                                                                                 
#local misassemblies           11                                                                                 
#unaligned contigs             7 + 100 part                                                                       
Unaligned contigs length       7369                                                                               
Genome fraction (%)            99.582                                                                             
Duplication ratio              1.034                                                                              
#N's per 100 kbp               8.22                                                                               
#mismatches per 100 kbp        6.07                                                                               
#indels per 100 kbp            7.99                                                                               
#genes                         2671 + 52 part                                                                     
#predicted genes (unique)      2906                                                                               
#predicted genes (>= 0 bp)     2917                                                                               
#predicted genes (>= 300 bp)   2350                                                                               
#predicted genes (>= 1500 bp)  287                                                                                
#predicted genes (>= 3000 bp)  29                                                                                 
Largest alignment              326626                                                                             
NA50                           112477                                                                             
NGA50                          129669                                                                             
NA75                           44978                                                                              
NGA75                          52804                                                                              
