All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.71coverage_66.final
#Contigs                                   1114                                                               
#Contigs (>= 0 bp)                         5601                                                               
#Contigs (>= 1000 bp)                      601                                                                
Largest contig                             76643                                                              
Total length                               5273775                                                            
Total length (>= 0 bp)                     5767035                                                            
Total length (>= 1000 bp)                  5051196                                                            
Reference length                           5594470                                                            
N50                                        14326                                                              
NG50                                       12744                                                              
N75                                        6998                                                               
NG75                                       5581                                                               
L50                                        111                                                                
LG50                                       123                                                                
L75                                        243                                                                
LG75                                       282                                                                
#local misassemblies                       7                                                                  
#misassemblies                             2                                                                  
#misassembled contigs                      2                                                                  
Misassembled contigs length                9948                                                               
Misassemblies inter-contig overlap         651                                                                
#unaligned contigs                         0 + 0 part                                                         
Unaligned contigs length                   0                                                                  
#ambiguously mapped contigs                129                                                                
Extra bases in ambiguously mapped contigs  -64288                                                             
#N's                                       0                                                                  
#N's per 100 kbp                           0.00                                                               
Genome fraction (%)                        92.953                                                             
Duplication ratio                          1.002                                                              
#genes                                     4419 + 619 part                                                    
#predicted genes (unique)                  5809                                                               
#predicted genes (>= 0 bp)                 5810                                                               
#predicted genes (>= 300 bp)               4560                                                               
#predicted genes (>= 1500 bp)              589                                                                
#predicted genes (>= 3000 bp)              49                                                                 
#mismatches                                89                                                                 
#indels                                    240                                                                
Indels length                              245                                                                
#mismatches per 100 kbp                    1.71                                                               
#indels per 100 kbp                        4.62                                                               
GC (%)                                     50.25                                                              
Reference GC (%)                           50.48                                                              
Average %IDY                               99.332                                                             
Largest alignment                          76643                                                              
NA50                                       14326                                                              
NGA50                                      12744                                                              
NA75                                       6989                                                               
NGA75                                      5581                                                               
LA50                                       111                                                                
LGA50                                      123                                                                
LA75                                       244                                                                
LGA75                                      282                                                                
#Mis_misassemblies                         2                                                                  
#Mis_relocations                           2                                                                  
#Mis_translocations                        0                                                                  
#Mis_inversions                            0                                                                  
#Mis_misassembled contigs                  2                                                                  
Mis_Misassembled contigs length            9948                                                               
#Mis_local misassemblies                   7                                                                  
#Mis_short indels (<= 5 bp)                240                                                                
#Mis_long indels (> 5 bp)                  0                                                                  
#Una_fully unaligned contigs               0                                                                  
Una_Fully unaligned length                 0                                                                  
#Una_partially unaligned contigs           0                                                                  
#Una_with misassembly                      0                                                                  
#Una_both parts are significant            0                                                                  
Una_Partially unaligned length             0                                                                  
GAGE_Contigs #                             1114                                                               
GAGE_Min contig                            200                                                                
GAGE_Max contig                            76643                                                              
GAGE_N50                                   12744 COUNT: 123                                                   
GAGE_Genome size                           5594470                                                            
GAGE_Assembly size                         5273775                                                            
GAGE_Chaff bases                           0                                                                  
GAGE_Missing reference bases               188841(3.38%)                                                      
GAGE_Missing assembly bases                12(0.00%)                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                           
GAGE_Duplicated reference bases            2778                                                               
GAGE_Compressed reference bases            164784                                                             
GAGE_Bad trim                              12                                                                 
GAGE_Avg idy                               99.99                                                              
GAGE_SNPs                                  101                                                                
GAGE_Indels < 5bp                          241                                                                
GAGE_Indels >= 5                           3                                                                  
GAGE_Inversions                            0                                                                  
GAGE_Relocation                            6                                                                  
GAGE_Translocation                         0                                                                  
GAGE_Corrected contig #                    1110                                                               
GAGE_Corrected assembly size               5271487                                                            
GAGE_Min correct contig                    200                                                                
GAGE_Max correct contig                    76643                                                              
GAGE_Corrected N50                         12744 COUNT: 123                                                   
