All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.71coverage.contigs
GAGE_Contigs #                   282                                                              
GAGE_Min contig                  201                                                              
GAGE_Max contig                  313763                                                           
GAGE_N50                         99570 COUNT: 17                                                  
GAGE_Genome size                 5594470                                                          
GAGE_Assembly size               5355738                                                          
GAGE_Chaff bases                 0                                                                
GAGE_Missing reference bases     32170(0.58%)                                                     
GAGE_Missing assembly bases      2(0.00%)                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                         
GAGE_Duplicated reference bases  5520                                                             
GAGE_Compressed reference bases  228317                                                           
GAGE_Bad trim                    2                                                                
GAGE_Avg idy                     99.99                                                            
GAGE_SNPs                        86                                                               
GAGE_Indels < 5bp                207                                                              
GAGE_Indels >= 5                 4                                                                
GAGE_Inversions                  0                                                                
GAGE_Relocation                  6                                                                
GAGE_Translocation               0                                                                
GAGE_Corrected contig #          273                                                              
GAGE_Corrected assembly size     5351234                                                          
GAGE_Min correct contig          205                                                              
GAGE_Max correct contig          313770                                                           
GAGE_Corrected N50               99570 COUNT: 19                                                  
