All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.91coverage.K99.final-contigs
#Contigs                                   427                                                                       
#Contigs (>= 0 bp)                         696                                                                       
#Contigs (>= 1000 bp)                      171                                                                       
Largest contig                             316882                                                                    
Total length                               5376367                                                                   
Total length (>= 0 bp)                     5414211                                                                   
Total length (>= 1000 bp)                  5260810                                                                   
Reference length                           5594470                                                                   
N50                                        111452                                                                    
NG50                                       108086                                                                    
N75                                        52191                                                                     
NG75                                       43342                                                                     
L50                                        15                                                                        
LG50                                       16                                                                        
L75                                        31                                                                        
LG75                                       34                                                                        
#local misassemblies                       11                                                                        
#misassemblies                             3                                                                         
#misassembled contigs                      3                                                                         
Misassembled contigs length                348616                                                                    
Misassemblies inter-contig overlap         1939                                                                      
#unaligned contigs                         0 + 0 part                                                                
Unaligned contigs length                   0                                                                         
#ambiguously mapped contigs                144                                                                       
Extra bases in ambiguously mapped contigs  -86396                                                                    
#N's                                       0                                                                         
#N's per 100 kbp                           0.00                                                                      
Genome fraction (%)                        94.415                                                                    
Duplication ratio                          1.002                                                                     
#genes                                     4937 + 201 part                                                           
#predicted genes (unique)                  5501                                                                      
#predicted genes (>= 0 bp)                 5521                                                                      
#predicted genes (>= 300 bp)               4542                                                                      
#predicted genes (>= 1500 bp)              640                                                                       
#predicted genes (>= 3000 bp)              61                                                                        
#mismatches                                239                                                                       
#indels                                    370                                                                       
Indels length                              380                                                                       
#mismatches per 100 kbp                    4.52                                                                      
#indels per 100 kbp                        7.00                                                                      
GC (%)                                     50.31                                                                     
Reference GC (%)                           50.48                                                                     
Average %IDY                               98.858                                                                    
Largest alignment                          311125                                                                    
NA50                                       111188                                                                    
NGA50                                      108086                                                                    
NA75                                       52191                                                                     
NGA75                                      43342                                                                     
LA50                                       16                                                                        
LGA50                                      17                                                                        
LA75                                       32                                                                        
LGA75                                      35                                                                        
#Mis_misassemblies                         3                                                                         
#Mis_relocations                           3                                                                         
#Mis_translocations                        0                                                                         
#Mis_inversions                            0                                                                         
#Mis_misassembled contigs                  3                                                                         
Mis_Misassembled contigs length            348616                                                                    
#Mis_local misassemblies                   11                                                                        
#Mis_short indels (<= 5 bp)                370                                                                       
#Mis_long indels (> 5 bp)                  0                                                                         
#Una_fully unaligned contigs               0                                                                         
Una_Fully unaligned length                 0                                                                         
#Una_partially unaligned contigs           0                                                                         
#Una_with misassembly                      0                                                                         
#Una_both parts are significant            0                                                                         
Una_Partially unaligned length             0                                                                         
GAGE_Contigs #                             427                                                                       
GAGE_Min contig                            200                                                                       
GAGE_Max contig                            316882                                                                    
GAGE_N50                                   108086 COUNT: 16                                                          
GAGE_Genome size                           5594470                                                                   
GAGE_Assembly size                         5376367                                                                   
GAGE_Chaff bases                           0                                                                         
GAGE_Missing reference bases               8155(0.15%)                                                               
GAGE_Missing assembly bases                99(0.00%)                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                  
GAGE_Duplicated reference bases            2411                                                                      
GAGE_Compressed reference bases            281005                                                                    
GAGE_Bad trim                              99                                                                        
GAGE_Avg idy                               99.98                                                                     
GAGE_SNPs                                  199                                                                       
GAGE_Indels < 5bp                          380                                                                       
GAGE_Indels >= 5                           8                                                                         
GAGE_Inversions                            0                                                                         
GAGE_Relocation                            9                                                                         
GAGE_Translocation                         0                                                                         
GAGE_Corrected contig #                    435                                                                       
GAGE_Corrected assembly size               5376354                                                                   
GAGE_Min correct contig                    200                                                                       
GAGE_Max correct contig                    224217                                                                    
GAGE_Corrected N50                         99753 COUNT: 19                                                           
