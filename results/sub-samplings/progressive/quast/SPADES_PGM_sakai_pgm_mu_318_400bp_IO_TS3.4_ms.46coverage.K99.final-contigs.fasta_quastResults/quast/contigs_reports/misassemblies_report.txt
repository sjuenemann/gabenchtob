All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.46coverage.K99.final-contigs
#Mis_misassemblies               5                                                                         
#Mis_relocations                 5                                                                         
#Mis_translocations              0                                                                         
#Mis_inversions                  0                                                                         
#Mis_misassembled contigs        5                                                                         
Mis_Misassembled contigs length  393388                                                                    
#Mis_local misassemblies         16                                                                        
#mismatches                      248                                                                       
#indels                          586                                                                       
#Mis_short indels (<= 5 bp)      580                                                                       
#Mis_long indels (> 5 bp)        6                                                                         
Indels length                    687                                                                       
