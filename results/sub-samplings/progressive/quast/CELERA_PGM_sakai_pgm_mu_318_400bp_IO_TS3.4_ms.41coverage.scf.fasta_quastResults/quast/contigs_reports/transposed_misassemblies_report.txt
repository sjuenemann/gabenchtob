All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                      #Mis_misassemblies  #Mis_relocations  #Mis_translocations  #Mis_inversions  #Mis_misassembled contigs  Mis_Misassembled contigs length  #Mis_local misassemblies  #mismatches  #indels  #Mis_short indels (<= 5 bp)  #Mis_long indels (> 5 bp)  Indels length
CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.41coverage.scf  3                   3                 0                    0                3                          237445                           7                         100          544      543                          1                          557          
