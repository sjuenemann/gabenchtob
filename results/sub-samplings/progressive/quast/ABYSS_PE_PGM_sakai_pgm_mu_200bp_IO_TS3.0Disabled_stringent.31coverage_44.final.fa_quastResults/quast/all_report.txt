All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.31coverage_44.final
#Contigs                                   1144                                                                          
#Contigs (>= 0 bp)                         5609                                                                          
#Contigs (>= 1000 bp)                      710                                                                           
Largest contig                             47590                                                                         
Total length                               5219561                                                                       
Total length (>= 0 bp)                     5585103                                                                       
Total length (>= 1000 bp)                  5030251                                                                       
Reference length                           5594470                                                                       
N50                                        11203                                                                         
NG50                                       10421                                                                         
N75                                        5821                                                                          
NG75                                       4718                                                                          
L50                                        144                                                                           
LG50                                       162                                                                           
L75                                        305                                                                           
LG75                                       359                                                                           
#local misassemblies                       4                                                                             
#misassemblies                             2                                                                             
#misassembled contigs                      2                                                                             
Misassembled contigs length                10650                                                                         
Misassemblies inter-contig overlap         271                                                                           
#unaligned contigs                         0 + 4 part                                                                    
Unaligned contigs length                   151                                                                           
#ambiguously mapped contigs                120                                                                           
Extra bases in ambiguously mapped contigs  -60445                                                                        
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        92.108                                                                        
Duplication ratio                          1.001                                                                         
#genes                                     4314 + 642 part                                                               
#predicted genes (unique)                  6217                                                                          
#predicted genes (>= 0 bp)                 6218                                                                          
#predicted genes (>= 300 bp)               4786                                                                          
#predicted genes (>= 1500 bp)              484                                                                           
#predicted genes (>= 3000 bp)              34                                                                            
#mismatches                                102                                                                           
#indels                                    1282                                                                          
Indels length                              1322                                                                          
#mismatches per 100 kbp                    1.98                                                                          
#indels per 100 kbp                        24.88                                                                         
GC (%)                                     50.21                                                                         
Reference GC (%)                           50.48                                                                         
Average %IDY                               99.873                                                                        
Largest alignment                          47590                                                                         
NA50                                       11203                                                                         
NGA50                                      10421                                                                         
NA75                                       5821                                                                          
NGA75                                      4701                                                                          
LA50                                       144                                                                           
LGA50                                      162                                                                           
LA75                                       305                                                                           
LGA75                                      359                                                                           
#Mis_misassemblies                         2                                                                             
#Mis_relocations                           2                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  2                                                                             
Mis_Misassembled contigs length            10650                                                                         
#Mis_local misassemblies                   4                                                                             
#Mis_short indels (<= 5 bp)                1282                                                                          
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           4                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             151                                                                           
GAGE_Contigs #                             1144                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            47590                                                                         
GAGE_N50                                   10421 COUNT: 162                                                              
GAGE_Genome size                           5594470                                                                       
GAGE_Assembly size                         5219561                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               264390(4.73%)                                                                 
GAGE_Missing assembly bases                156(0.00%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            0                                                                             
GAGE_Compressed reference bases            121961                                                                        
GAGE_Bad trim                              156                                                                           
GAGE_Avg idy                               99.97                                                                         
GAGE_SNPs                                  133                                                                           
GAGE_Indels < 5bp                          1326                                                                          
GAGE_Indels >= 5                           4                                                                             
GAGE_Inversions                            0                                                                             
GAGE_Relocation                            2                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    1148                                                                          
GAGE_Corrected assembly size               5220645                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    47598                                                                         
GAGE_Corrected N50                         10424 COUNT: 162                                                              
