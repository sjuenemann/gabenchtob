All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.21coverage.contigs
#Contigs                                   68                                                                          
#Contigs (>= 0 bp)                         92                                                                          
#Contigs (>= 1000 bp)                      55                                                                          
Largest contig                             223244                                                                      
Total length                               2771954                                                                     
Total length (>= 0 bp)                     2775536                                                                     
Total length (>= 1000 bp)                  2766810                                                                     
Reference length                           2813862                                                                     
N50                                        135440                                                                      
NG50                                       135440                                                                      
N75                                        46490                                                                       
NG75                                       46350                                                                       
L50                                        9                                                                           
LG50                                       9                                                                           
L75                                        18                                                                          
LG75                                       19                                                                          
#local misassemblies                       6                                                                           
#misassemblies                             2                                                                           
#misassembled contigs                      2                                                                           
Misassembled contigs length                139880                                                                      
Misassemblies inter-contig overlap         1045                                                                        
#unaligned contigs                         0 + 0 part                                                                  
Unaligned contigs length                   0                                                                           
#ambiguously mapped contigs                12                                                                          
Extra bases in ambiguously mapped contigs  -9635                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        98.180                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     2667 + 33 part                                                              
#predicted genes (unique)                  2717                                                                        
#predicted genes (>= 0 bp)                 2718                                                                        
#predicted genes (>= 300 bp)               2334                                                                        
#predicted genes (>= 1500 bp)              277                                                                         
#predicted genes (>= 3000 bp)              25                                                                          
#mismatches                                101                                                                         
#indels                                    291                                                                         
Indels length                              363                                                                         
#mismatches per 100 kbp                    3.66                                                                        
#indels per 100 kbp                        10.53                                                                       
GC (%)                                     32.65                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               98.607                                                                      
Largest alignment                          223244                                                                      
NA50                                       97554                                                                       
NGA50                                      97554                                                                       
NA75                                       46156                                                                       
NGA75                                      43856                                                                       
LA50                                       9                                                                           
LGA50                                      9                                                                           
LA75                                       19                                                                          
LGA75                                      20                                                                          
#Mis_misassemblies                         2                                                                           
#Mis_relocations                           2                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  2                                                                           
Mis_Misassembled contigs length            139880                                                                      
#Mis_local misassemblies                   6                                                                           
#Mis_short indels (<= 5 bp)                289                                                                         
#Mis_long indels (> 5 bp)                  2                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           0                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             0                                                                           
GAGE_Contigs #                             68                                                                          
GAGE_Min contig                            206                                                                         
GAGE_Max contig                            223244                                                                      
GAGE_N50                                   135440 COUNT: 9                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2771954                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               6219(0.22%)                                                                 
GAGE_Missing assembly bases                14(0.00%)                                                                   
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            206                                                                         
GAGE_Compressed reference bases            37973                                                                       
GAGE_Bad trim                              14                                                                          
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  61                                                                          
GAGE_Indels < 5bp                          300                                                                         
GAGE_Indels >= 5                           8                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            2                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    77                                                                          
GAGE_Corrected assembly size               2773312                                                                     
GAGE_Min correct contig                    223                                                                         
GAGE_Max correct contig                    189606                                                                      
GAGE_Corrected N50                         80521 COUNT: 11                                                             
