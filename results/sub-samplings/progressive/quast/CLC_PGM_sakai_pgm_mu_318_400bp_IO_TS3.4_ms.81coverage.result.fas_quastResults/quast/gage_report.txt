All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.81coverage.result
GAGE_Contigs #                   1016                                                        
GAGE_Min contig                  200                                                         
GAGE_Max contig                  110421                                                      
GAGE_N50                         31306 COUNT: 57                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5468886                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     52600(0.94%)                                                
GAGE_Missing assembly bases      1192(0.02%)                                                 
GAGE_Missing assembly contigs    1(0.10%)                                                    
GAGE_Duplicated reference bases  112170                                                      
GAGE_Compressed reference bases  231170                                                      
GAGE_Bad trim                    984                                                         
GAGE_Avg idy                     99.99                                                       
GAGE_SNPs                        93                                                          
GAGE_Indels < 5bp                523                                                         
GAGE_Indels >= 5                 4                                                           
GAGE_Inversions                  1                                                           
GAGE_Relocation                  5                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          664                                                         
GAGE_Corrected assembly size     5355973                                                     
GAGE_Min correct contig          200                                                         
GAGE_Max correct contig          110421                                                      
GAGE_Corrected N50               31307 COUNT: 57                                             
