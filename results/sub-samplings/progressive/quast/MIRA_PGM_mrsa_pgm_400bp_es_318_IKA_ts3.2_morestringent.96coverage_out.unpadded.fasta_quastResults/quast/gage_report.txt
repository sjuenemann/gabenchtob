All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.96coverage_out.unpadded
GAGE_Contigs #                   1135                                                                          
GAGE_Min contig                  202                                                                           
GAGE_Max contig                  520916                                                                        
GAGE_N50                         212338 COUNT: 5                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               3483811                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     628(0.02%)                                                                    
GAGE_Missing assembly bases      3807(0.11%)                                                                   
GAGE_Missing assembly contigs    3(0.26%)                                                                      
GAGE_Duplicated reference bases  676985                                                                        
GAGE_Compressed reference bases  12523                                                                         
GAGE_Bad trim                    2723                                                                          
GAGE_Avg idy                     99.98                                                                         
GAGE_SNPs                        43                                                                            
GAGE_Indels < 5bp                55                                                                            
GAGE_Indels >= 5                 7                                                                             
GAGE_Inversions                  4                                                                             
GAGE_Relocation                  6                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          56                                                                            
GAGE_Corrected assembly size     2834931                                                                       
GAGE_Min correct contig          308                                                                           
GAGE_Max correct contig          300059                                                                        
GAGE_Corrected N50               126835 COUNT: 7                                                               
