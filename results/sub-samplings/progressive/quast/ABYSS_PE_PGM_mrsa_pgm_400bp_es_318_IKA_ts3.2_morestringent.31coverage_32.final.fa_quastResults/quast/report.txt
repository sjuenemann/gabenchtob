All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.31coverage_32.final
#Contigs (>= 0 bp)             4994                                                                          
#Contigs (>= 1000 bp)          415                                                                           
Total length (>= 0 bp)         2940265                                                                       
Total length (>= 1000 bp)      2623840                                                                       
#Contigs                       611                                                                           
Largest contig                 44784                                                                         
Total length                   2716785                                                                       
Reference length               2813862                                                                       
GC (%)                         32.59                                                                         
Reference GC (%)               32.81                                                                         
N50                            9225                                                                          
NG50                           9017                                                                          
N75                            4974                                                                          
NG75                           4496                                                                          
#misassemblies                 1                                                                             
#local misassemblies           2                                                                             
#unaligned contigs             0 + 0 part                                                                    
Unaligned contigs length       0                                                                             
Genome fraction (%)            96.262                                                                        
Duplication ratio              1.002                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        1.55                                                                          
#indels per 100 kbp            6.90                                                                          
#genes                         2315 + 301 part                                                               
#predicted genes (unique)      2925                                                                          
#predicted genes (>= 0 bp)     2925                                                                          
#predicted genes (>= 300 bp)   2362                                                                          
#predicted genes (>= 1500 bp)  256                                                                           
#predicted genes (>= 3000 bp)  22                                                                            
Largest alignment              44784                                                                         
NA50                           9225                                                                          
NGA50                          9017                                                                          
NA75                           4974                                                                          
NGA75                          4496                                                                          
