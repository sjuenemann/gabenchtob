All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.46coverage.result
#Contigs                                   285                                                                         
#Contigs (>= 0 bp)                         285                                                                         
#Contigs (>= 1000 bp)                      161                                                                         
Largest contig                             75381                                                                       
Total length                               2777906                                                                     
Total length (>= 0 bp)                     2777906                                                                     
Total length (>= 1000 bp)                  2741492                                                                     
Reference length                           2813862                                                                     
N50                                        30286                                                                       
NG50                                       29635                                                                       
N75                                        16633                                                                       
NG75                                       16324                                                                       
L50                                        32                                                                          
LG50                                       33                                                                          
L75                                        62                                                                          
LG75                                       64                                                                          
#local misassemblies                       3                                                                           
#misassemblies                             10                                                                          
#misassembled contigs                      10                                                                          
Misassembled contigs length                50569                                                                       
Misassemblies inter-contig overlap         887                                                                         
#unaligned contigs                         3 + 16 part                                                                 
Unaligned contigs length                   1681                                                                        
#ambiguously mapped contigs                11                                                                          
Extra bases in ambiguously mapped contigs  -8750                                                                       
#N's                                       127                                                                         
#N's per 100 kbp                           4.57                                                                        
Genome fraction (%)                        97.588                                                                      
Duplication ratio                          1.008                                                                       
#genes                                     2589 + 91 part                                                              
#predicted genes (unique)                  2866                                                                        
#predicted genes (>= 0 bp)                 2867                                                                        
#predicted genes (>= 300 bp)               2347                                                                        
#predicted genes (>= 1500 bp)              263                                                                         
#predicted genes (>= 3000 bp)              24                                                                          
#mismatches                                95                                                                          
#indels                                    436                                                                         
Indels length                              461                                                                         
#mismatches per 100 kbp                    3.46                                                                        
#indels per 100 kbp                        15.88                                                                       
GC (%)                                     32.62                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.323                                                                      
Largest alignment                          75381                                                                       
NA50                                       30286                                                                       
NGA50                                      29635                                                                       
NA75                                       16333                                                                       
NGA75                                      16324                                                                       
LA50                                       32                                                                          
LGA50                                      33                                                                          
LA75                                       63                                                                          
LGA75                                      64                                                                          
#Mis_misassemblies                         10                                                                          
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            7                                                                           
#Mis_misassembled contigs                  10                                                                          
Mis_Misassembled contigs length            50569                                                                       
#Mis_local misassemblies                   3                                                                           
#Mis_short indels (<= 5 bp)                436                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               3                                                                           
Una_Fully unaligned length                 909                                                                         
#Una_partially unaligned contigs           16                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             772                                                                         
GAGE_Contigs #                             285                                                                         
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            75381                                                                       
GAGE_N50                                   29635 COUNT: 33                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2777906                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               25015(0.89%)                                                                
GAGE_Missing assembly bases                1526(0.05%)                                                                 
GAGE_Missing assembly contigs              2(0.70%)                                                                    
GAGE_Duplicated reference bases            20369                                                                       
GAGE_Compressed reference bases            39375                                                                       
GAGE_Bad trim                              896                                                                         
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  46                                                                          
GAGE_Indels < 5bp                          417                                                                         
GAGE_Indels >= 5                           4                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            2                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    205                                                                         
GAGE_Corrected assembly size               2757078                                                                     
GAGE_Min correct contig                    202                                                                         
GAGE_Max correct contig                    75394                                                                       
GAGE_Corrected N50                         29032 COUNT: 33                                                             
