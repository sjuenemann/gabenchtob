All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.16coverage_out.unpadded
#Contigs (>= 0 bp)             330                                                                                
#Contigs (>= 1000 bp)          202                                                                                
Total length (>= 0 bp)         2849484                                                                            
Total length (>= 1000 bp)      2798306                                                                            
#Contigs                       314                                                                                
Largest contig                 69921                                                                              
Total length                   2846943                                                                            
Reference length               2813862                                                                            
GC (%)                         32.68                                                                              
Reference GC (%)               32.81                                                                              
N50                            27754                                                                              
NG50                           27754                                                                              
N75                            13190                                                                              
NG75                           13543                                                                              
#misassemblies                 63                                                                                 
#local misassemblies           13                                                                                 
#unaligned contigs             0 + 22 part                                                                        
Unaligned contigs length       1087                                                                               
Genome fraction (%)            98.924                                                                             
Duplication ratio              1.022                                                                              
#N's per 100 kbp               6.95                                                                               
#mismatches per 100 kbp        12.00                                                                              
#indels per 100 kbp            21.48                                                                              
#genes                         2570 + 150 part                                                                    
#predicted genes (unique)      2991                                                                               
#predicted genes (>= 0 bp)     2991                                                                               
#predicted genes (>= 300 bp)   2429                                                                               
#predicted genes (>= 1500 bp)  268                                                                                
#predicted genes (>= 3000 bp)  23                                                                                 
Largest alignment              69903                                                                              
NA50                           25594                                                                              
NGA50                          25686                                                                              
NA75                           11987                                                                              
NGA75                          12803                                                                              
