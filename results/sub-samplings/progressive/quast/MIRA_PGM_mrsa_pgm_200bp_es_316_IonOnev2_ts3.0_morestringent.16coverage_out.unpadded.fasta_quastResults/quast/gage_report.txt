All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.16coverage_out.unpadded
GAGE_Contigs #                   314                                                                                
GAGE_Min contig                  210                                                                                
GAGE_Max contig                  69921                                                                              
GAGE_N50                         27754 COUNT: 34                                                                    
GAGE_Genome size                 2813862                                                                            
GAGE_Assembly size               2846943                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     2884(0.10%)                                                                        
GAGE_Missing assembly bases      2966(0.10%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  36968                                                                              
GAGE_Compressed reference bases  30907                                                                              
GAGE_Bad trim                    2934                                                                               
GAGE_Avg idy                     99.95                                                                              
GAGE_SNPs                        140                                                                                
GAGE_Indels < 5bp                481                                                                                
GAGE_Indels >= 5                 7                                                                                  
GAGE_Inversions                  14                                                                                 
GAGE_Relocation                  12                                                                                 
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          278                                                                                
GAGE_Corrected assembly size     2818719                                                                            
GAGE_Min correct contig          201                                                                                
GAGE_Max correct contig          69908                                                                              
GAGE_Corrected N50               24020 COUNT: 38                                                                    
