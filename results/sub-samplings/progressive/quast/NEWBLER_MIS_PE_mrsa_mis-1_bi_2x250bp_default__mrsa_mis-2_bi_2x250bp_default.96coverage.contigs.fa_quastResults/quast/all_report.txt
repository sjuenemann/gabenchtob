All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.96coverage.contigs
#Contigs                                   53                                                                                            
#Contigs (>= 0 bp)                         66                                                                                            
#Contigs (>= 1000 bp)                      34                                                                                            
Largest contig                             713087                                                                                        
Total length                               2782410                                                                                       
Total length (>= 0 bp)                     2784098                                                                                       
Total length (>= 1000 bp)                  2774212                                                                                       
Reference length                           2813862                                                                                       
N50                                        237750                                                                                        
NG50                                       237750                                                                                        
N75                                        119112                                                                                        
NG75                                       119112                                                                                        
L50                                        4                                                                                             
LG50                                       4                                                                                             
L75                                        8                                                                                             
LG75                                       8                                                                                             
#local misassemblies                       0                                                                                             
#misassemblies                             2                                                                                             
#misassembled contigs                      2                                                                                             
Misassembled contigs length                150491                                                                                        
Misassemblies inter-contig overlap         0                                                                                             
#unaligned contigs                         4 + 0 part                                                                                    
Unaligned contigs length                   6282                                                                                          
#ambiguously mapped contigs                11                                                                                            
Extra bases in ambiguously mapped contigs  -9214                                                                                         
#N's                                       0                                                                                             
#N's per 100 kbp                           0.00                                                                                          
Genome fraction (%)                        98.329                                                                                        
Duplication ratio                          1.000                                                                                         
#genes                                     2681 + 18 part                                                                                
#predicted genes (unique)                  2633                                                                                          
#predicted genes (>= 0 bp)                 2634                                                                                          
#predicted genes (>= 300 bp)               2287                                                                                          
#predicted genes (>= 1500 bp)              300                                                                                           
#predicted genes (>= 3000 bp)              29                                                                                            
#mismatches                                30                                                                                            
#indels                                    10                                                                                            
Indels length                              10                                                                                            
#mismatches per 100 kbp                    1.08                                                                                          
#indels per 100 kbp                        0.36                                                                                          
GC (%)                                     32.69                                                                                         
Reference GC (%)                           32.81                                                                                         
Average %IDY                               98.579                                                                                        
Largest alignment                          713087                                                                                        
NA50                                       237750                                                                                        
NGA50                                      237750                                                                                        
NA75                                       87827                                                                                         
NGA75                                      87827                                                                                         
LA50                                       4                                                                                             
LGA50                                      4                                                                                             
LA75                                       9                                                                                             
LGA75                                      9                                                                                             
#Mis_misassemblies                         2                                                                                             
#Mis_relocations                           2                                                                                             
#Mis_translocations                        0                                                                                             
#Mis_inversions                            0                                                                                             
#Mis_misassembled contigs                  2                                                                                             
Mis_Misassembled contigs length            150491                                                                                        
#Mis_local misassemblies                   0                                                                                             
#Mis_short indels (<= 5 bp)                10                                                                                            
#Mis_long indels (> 5 bp)                  0                                                                                             
#Una_fully unaligned contigs               4                                                                                             
Una_Fully unaligned length                 6282                                                                                          
#Una_partially unaligned contigs           0                                                                                             
#Una_with misassembly                      0                                                                                             
#Una_both parts are significant            0                                                                                             
Una_Partially unaligned length             0                                                                                             
GAGE_Contigs #                             53                                                                                            
GAGE_Min contig                            207                                                                                           
GAGE_Max contig                            713087                                                                                        
GAGE_N50                                   237750 COUNT: 4                                                                               
GAGE_Genome size                           2813862                                                                                       
GAGE_Assembly size                         2782410                                                                                       
GAGE_Chaff bases                           0                                                                                             
GAGE_Missing reference bases               2230(0.08%)                                                                                   
GAGE_Missing assembly bases                6284(0.23%)                                                                                   
GAGE_Missing assembly contigs              4(7.55%)                                                                                      
GAGE_Duplicated reference bases            0                                                                                             
GAGE_Compressed reference bases            37754                                                                                         
GAGE_Bad trim                              2                                                                                             
GAGE_Avg idy                               99.99                                                                                         
GAGE_SNPs                                  31                                                                                            
GAGE_Indels < 5bp                          10                                                                                            
GAGE_Indels >= 5                           0                                                                                             
GAGE_Inversions                            0                                                                                             
GAGE_Relocation                            2                                                                                             
GAGE_Translocation                         0                                                                                             
GAGE_Corrected contig #                    51                                                                                            
GAGE_Corrected assembly size               2776132                                                                                       
GAGE_Min correct contig                    207                                                                                           
GAGE_Max correct contig                    713086                                                                                        
GAGE_Corrected N50                         237753 COUNT: 4                                                                               
