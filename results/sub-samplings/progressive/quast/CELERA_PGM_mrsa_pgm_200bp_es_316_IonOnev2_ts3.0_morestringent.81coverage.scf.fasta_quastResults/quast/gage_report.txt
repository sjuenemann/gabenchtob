All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.81coverage.scf
GAGE_Contigs #                   456                                                                         
GAGE_Min contig                  1001                                                                        
GAGE_Max contig                  44017                                                                       
GAGE_N50                         10585 COUNT: 84                                                             
GAGE_Genome size                 2813862                                                                     
GAGE_Assembly size               2694673                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     153798(5.47%)                                                               
GAGE_Missing assembly bases      657(0.02%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  1245                                                                        
GAGE_Compressed reference bases  19682                                                                       
GAGE_Bad trim                    657                                                                         
GAGE_Avg idy                     99.97                                                                       
GAGE_SNPs                        88                                                                          
GAGE_Indels < 5bp                415                                                                         
GAGE_Indels >= 5                 4                                                                           
GAGE_Inversions                  1                                                                           
GAGE_Relocation                  1                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          463                                                                         
GAGE_Corrected assembly size     2694803                                                                     
GAGE_Min correct contig          465                                                                         
GAGE_Max correct contig          40655                                                                       
GAGE_Corrected N50               10586 COUNT: 84                                                             
