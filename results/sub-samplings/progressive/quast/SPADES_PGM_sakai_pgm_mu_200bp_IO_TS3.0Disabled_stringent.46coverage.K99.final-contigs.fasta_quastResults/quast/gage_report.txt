All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.46coverage.K99.final-contigs
GAGE_Contigs #                   386                                                                                  
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  374847                                                                               
GAGE_N50                         140496 COUNT: 14                                                                     
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5364348                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     9806(0.18%)                                                                          
GAGE_Missing assembly bases      779(0.01%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  941                                                                                  
GAGE_Compressed reference bases  284951                                                                               
GAGE_Bad trim                    779                                                                                  
GAGE_Avg idy                     99.96                                                                                
GAGE_SNPs                        257                                                                                  
GAGE_Indels < 5bp                1722                                                                                 
GAGE_Indels >= 5                 8                                                                                    
GAGE_Inversions                  3                                                                                    
GAGE_Relocation                  7                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          402                                                                                  
GAGE_Corrected assembly size     5366883                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          226975                                                                               
GAGE_Corrected N50               98055 COUNT: 18                                                                      
