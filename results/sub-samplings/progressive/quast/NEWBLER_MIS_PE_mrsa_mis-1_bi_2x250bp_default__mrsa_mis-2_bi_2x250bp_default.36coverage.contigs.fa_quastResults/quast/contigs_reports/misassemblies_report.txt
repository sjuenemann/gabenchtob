All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.36coverage.contigs
#Mis_misassemblies               2                                                                                             
#Mis_relocations                 2                                                                                             
#Mis_translocations              0                                                                                             
#Mis_inversions                  0                                                                                             
#Mis_misassembled contigs        2                                                                                             
Mis_Misassembled contigs length  150366                                                                                        
#Mis_local misassemblies         0                                                                                             
#mismatches                      61                                                                                            
#indels                          9                                                                                             
#Mis_short indels (<= 5 bp)      9                                                                                             
#Mis_long indels (> 5 bp)        0                                                                                             
Indels length                    9                                                                                             
