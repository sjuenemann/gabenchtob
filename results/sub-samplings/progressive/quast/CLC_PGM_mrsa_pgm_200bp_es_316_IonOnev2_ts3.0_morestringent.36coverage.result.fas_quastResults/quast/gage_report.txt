All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.36coverage.result
GAGE_Contigs #                   384                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  90722                                                                       
GAGE_N50                         25989 COUNT: 34                                                             
GAGE_Genome size                 2813862                                                                     
GAGE_Assembly size               2766821                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     40246(1.43%)                                                                
GAGE_Missing assembly bases      1007(0.04%)                                                                 
GAGE_Missing assembly contigs    2(0.52%)                                                                    
GAGE_Duplicated reference bases  23436                                                                       
GAGE_Compressed reference bases  39122                                                                       
GAGE_Bad trim                    594                                                                         
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        60                                                                          
GAGE_Indels < 5bp                474                                                                         
GAGE_Indels >= 5                 1                                                                           
GAGE_Inversions                  2                                                                           
GAGE_Relocation                  5                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          298                                                                         
GAGE_Corrected assembly size     2744229                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          90733                                                                       
GAGE_Corrected N50               25923 COUNT: 35                                                             
