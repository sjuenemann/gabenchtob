All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.36coverage_48.final
GAGE_Contigs #                   1154                                                               
GAGE_Min contig                  202                                                                
GAGE_Max contig                  76104                                                              
GAGE_N50                         12734 COUNT: 118                                                   
GAGE_Genome size                 5594470                                                            
GAGE_Assembly size               5239816                                                            
GAGE_Chaff bases                 0                                                                  
GAGE_Missing reference bases     226774(4.05%)                                                      
GAGE_Missing assembly bases      51(0.00%)                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                           
GAGE_Duplicated reference bases  679                                                                
GAGE_Compressed reference bases  145471                                                             
GAGE_Bad trim                    51                                                                 
GAGE_Avg idy                     99.99                                                              
GAGE_SNPs                        129                                                                
GAGE_Indels < 5bp                394                                                                
GAGE_Indels >= 5                 5                                                                  
GAGE_Inversions                  0                                                                  
GAGE_Relocation                  5                                                                  
GAGE_Translocation               0                                                                  
GAGE_Corrected contig #          1158                                                               
GAGE_Corrected assembly size     5239781                                                            
GAGE_Min correct contig          202                                                                
GAGE_Max correct contig          76109                                                              
GAGE_Corrected N50               12357 COUNT: 121                                                   
