All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.86coverage.contigs
GAGE_Contigs #                   167                                                                              
GAGE_Min contig                  201                                                                              
GAGE_Max contig                  96211                                                                            
GAGE_N50                         31783 COUNT: 29                                                                  
GAGE_Genome size                 2813862                                                                          
GAGE_Assembly size               2759021                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     23097(0.82%)                                                                     
GAGE_Missing assembly bases      153(0.01%)                                                                       
GAGE_Missing assembly contigs    0(0.00%)                                                                         
GAGE_Duplicated reference bases  1212                                                                             
GAGE_Compressed reference bases  34995                                                                            
GAGE_Bad trim                    153                                                                              
GAGE_Avg idy                     99.99                                                                            
GAGE_SNPs                        34                                                                               
GAGE_Indels < 5bp                254                                                                              
GAGE_Indels >= 5                 6                                                                                
GAGE_Inversions                  1                                                                                
GAGE_Relocation                  1                                                                                
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          167                                                                              
GAGE_Corrected assembly size     2758766                                                                          
GAGE_Min correct contig          201                                                                              
GAGE_Max correct contig          75952                                                                            
GAGE_Corrected N50               28759 COUNT: 31                                                                  
