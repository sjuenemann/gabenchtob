All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.86coverage.contigs
#Contigs                                   167                                                                              
#Contigs (>= 0 bp)                         296                                                                              
#Contigs (>= 1000 bp)                      133                                                                              
Largest contig                             96211                                                                            
Total length                               2759021                                                                          
Total length (>= 0 bp)                     2775548                                                                          
Total length (>= 1000 bp)                  2747480                                                                          
Reference length                           2813862                                                                          
N50                                        31783                                                                            
NG50                                       31783                                                                            
N75                                        18358                                                                            
NG75                                       16971                                                                            
L50                                        29                                                                               
LG50                                       29                                                                               
L75                                        57                                                                               
LG75                                       60                                                                               
#local misassemblies                       5                                                                                
#misassemblies                             4                                                                                
#misassembled contigs                      4                                                                                
Misassembled contigs length                44137                                                                            
Misassemblies inter-contig overlap         1108                                                                             
#unaligned contigs                         0 + 2 part                                                                       
Unaligned contigs length                   118                                                                              
#ambiguously mapped contigs                14                                                                               
Extra bases in ambiguously mapped contigs  -7900                                                                            
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        97.741                                                                           
Duplication ratio                          1.001                                                                            
#genes                                     2598 + 97 part                                                                   
#predicted genes (unique)                  2738                                                                             
#predicted genes (>= 0 bp)                 2738                                                                             
#predicted genes (>= 300 bp)               2325                                                                             
#predicted genes (>= 1500 bp)              285                                                                              
#predicted genes (>= 3000 bp)              24                                                                               
#mismatches                                55                                                                               
#indels                                    233                                                                              
Indels length                              323                                                                              
#mismatches per 100 kbp                    2.00                                                                             
#indels per 100 kbp                        8.47                                                                             
GC (%)                                     32.64                                                                            
Reference GC (%)                           32.81                                                                            
Average %IDY                               99.357                                                                           
Largest alignment                          96211                                                                            
NA50                                       31783                                                                            
NGA50                                      31783                                                                            
NA75                                       17368                                                                            
NGA75                                      16971                                                                            
LA50                                       29                                                                               
LGA50                                      29                                                                               
LA75                                       58                                                                               
LGA75                                      60                                                                               
#Mis_misassemblies                         4                                                                                
#Mis_relocations                           1                                                                                
#Mis_translocations                        1                                                                                
#Mis_inversions                            2                                                                                
#Mis_misassembled contigs                  4                                                                                
Mis_Misassembled contigs length            44137                                                                            
#Mis_local misassemblies                   5                                                                                
#Mis_short indels (<= 5 bp)                232                                                                              
#Mis_long indels (> 5 bp)                  1                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           2                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             118                                                                              
GAGE_Contigs #                             167                                                                              
GAGE_Min contig                            201                                                                              
GAGE_Max contig                            96211                                                                            
GAGE_N50                                   31783 COUNT: 29                                                                  
GAGE_Genome size                           2813862                                                                          
GAGE_Assembly size                         2759021                                                                          
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               23097(0.82%)                                                                     
GAGE_Missing assembly bases                153(0.01%)                                                                       
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            1212                                                                             
GAGE_Compressed reference bases            34995                                                                            
GAGE_Bad trim                              153                                                                              
GAGE_Avg idy                               99.99                                                                            
GAGE_SNPs                                  34                                                                               
GAGE_Indels < 5bp                          254                                                                              
GAGE_Indels >= 5                           6                                                                                
GAGE_Inversions                            1                                                                                
GAGE_Relocation                            1                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    167                                                                              
GAGE_Corrected assembly size               2758766                                                                          
GAGE_Min correct contig                    201                                                                              
GAGE_Max correct contig                    75952                                                                            
GAGE_Corrected N50                         28759 COUNT: 31                                                                  
