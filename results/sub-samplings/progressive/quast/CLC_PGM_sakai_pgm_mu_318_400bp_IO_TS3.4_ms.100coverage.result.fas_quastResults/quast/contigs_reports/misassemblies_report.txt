All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.100coverage.result
#Mis_misassemblies               7                                                            
#Mis_relocations                 7                                                            
#Mis_translocations              0                                                            
#Mis_inversions                  0                                                            
#Mis_misassembled contigs        7                                                            
Mis_Misassembled contigs length  29032                                                        
#Mis_local misassemblies         8                                                            
#mismatches                      304                                                          
#indels                          597                                                          
#Mis_short indels (<= 5 bp)      592                                                          
#Mis_long indels (> 5 bp)        5                                                            
Indels length                    685                                                          
