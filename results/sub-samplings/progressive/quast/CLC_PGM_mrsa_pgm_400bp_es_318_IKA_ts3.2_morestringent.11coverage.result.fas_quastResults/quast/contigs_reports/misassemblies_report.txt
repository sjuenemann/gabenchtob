All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.11coverage.result
#Mis_misassemblies               23                                                                     
#Mis_relocations                 23                                                                     
#Mis_translocations              0                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        22                                                                     
Mis_Misassembled contigs length  207385                                                                 
#Mis_local misassemblies         8                                                                      
#mismatches                      462                                                                    
#indels                          2578                                                                   
#Mis_short indels (<= 5 bp)      2573                                                                   
#Mis_long indels (> 5 bp)        5                                                                      
Indels length                    2830                                                                   
