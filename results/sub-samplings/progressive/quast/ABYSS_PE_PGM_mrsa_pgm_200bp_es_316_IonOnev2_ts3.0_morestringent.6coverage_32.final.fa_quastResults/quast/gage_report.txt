All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.6coverage_32.final
GAGE_Contigs #                   3996                                                                              
GAGE_Min contig                  200                                                                               
GAGE_Max contig                  7122                                                                              
GAGE_N50                         480 COUNT: 1716                                                                   
GAGE_Genome size                 2813862                                                                           
GAGE_Assembly size               2133397                                                                           
GAGE_Chaff bases                 0                                                                                 
GAGE_Missing reference bases     665436(23.65%)                                                                    
GAGE_Missing assembly bases      2180(0.10%)                                                                       
GAGE_Missing assembly contigs    0(0.00%)                                                                          
GAGE_Duplicated reference bases  0                                                                                 
GAGE_Compressed reference bases  24227                                                                             
GAGE_Bad trim                    2173                                                                              
GAGE_Avg idy                     99.91                                                                             
GAGE_SNPs                        205                                                                               
GAGE_Indels < 5bp                1479                                                                              
GAGE_Indels >= 5                 3                                                                                 
GAGE_Inversions                  17                                                                                
GAGE_Relocation                  8                                                                                 
GAGE_Translocation               0                                                                                 
GAGE_Corrected contig #          3992                                                                              
GAGE_Corrected assembly size     2129741                                                                           
GAGE_Min correct contig          200                                                                               
GAGE_Max correct contig          7122                                                                              
GAGE_Corrected N50               479 COUNT: 1721                                                                   
