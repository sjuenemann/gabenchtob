All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.21coverage.contigs
GAGE_Contigs #                   370                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  259577                                                                      
GAGE_N50                         87694 COUNT: 21                                                             
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               5308755                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     47095(0.84%)                                                                
GAGE_Missing assembly bases      55(0.00%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  655                                                                         
GAGE_Compressed reference bases  246726                                                                      
GAGE_Bad trim                    55                                                                          
GAGE_Avg idy                     99.97                                                                       
GAGE_SNPs                        150                                                                         
GAGE_Indels < 5bp                1609                                                                        
GAGE_Indels >= 5                 8                                                                           
GAGE_Inversions                  0                                                                           
GAGE_Relocation                  5                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          380                                                                         
GAGE_Corrected assembly size     5311923                                                                     
GAGE_Min correct contig          201                                                                         
GAGE_Max correct contig          224231                                                                      
GAGE_Corrected N50               78455 COUNT: 22                                                             
