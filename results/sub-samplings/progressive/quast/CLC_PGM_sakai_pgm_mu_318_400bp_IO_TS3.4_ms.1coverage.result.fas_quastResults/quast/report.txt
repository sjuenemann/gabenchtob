All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.1coverage.result
#Contigs (>= 0 bp)             2914                                                       
#Contigs (>= 1000 bp)          250                                                        
Total length (>= 0 bp)         1783047                                                    
Total length (>= 1000 bp)      340454                                                     
#Contigs                       2914                                                       
Largest contig                 3321                                                       
Total length                   1783047                                                    
Reference length               5594470                                                    
GC (%)                         51.32                                                      
Reference GC (%)               50.48                                                      
N50                            648                                                        
NG50                           None                                                       
N75                            484                                                        
NG75                           None                                                       
#misassemblies                 240                                                        
#local misassemblies           9                                                          
#unaligned contigs             15 + 41 part                                               
Unaligned contigs length       17832                                                      
Genome fraction (%)            30.352                                                     
Duplication ratio              1.018                                                      
#N's per 100 kbp               23.16                                                      
#mismatches per 100 kbp        166.49                                                     
#indels per 100 kbp            375.02                                                     
#genes                         330 + 2519 part                                            
#predicted genes (unique)      4432                                                       
#predicted genes (>= 0 bp)     4438                                                       
#predicted genes (>= 300 bp)   1926                                                       
#predicted genes (>= 1500 bp)  0                                                          
#predicted genes (>= 3000 bp)  0                                                          
Largest alignment              2779                                                       
NA50                           604                                                        
NGA50                          None                                                       
NA75                           435                                                        
NGA75                          None                                                       
