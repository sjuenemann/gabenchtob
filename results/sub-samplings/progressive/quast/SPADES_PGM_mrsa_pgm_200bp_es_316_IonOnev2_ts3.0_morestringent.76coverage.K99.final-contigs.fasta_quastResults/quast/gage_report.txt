All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.76coverage.K99.final-contigs
GAGE_Contigs #                   114                                                                                       
GAGE_Min contig                  203                                                                                       
GAGE_Max contig                  236814                                                                                    
GAGE_N50                         86754 COUNT: 10                                                                           
GAGE_Genome size                 2813862                                                                                   
GAGE_Assembly size               2781887                                                                                   
GAGE_Chaff bases                 0                                                                                         
GAGE_Missing reference bases     4127(0.15%)                                                                               
GAGE_Missing assembly bases      164(0.01%)                                                                                
GAGE_Missing assembly contigs    0(0.00%)                                                                                  
GAGE_Duplicated reference bases  423                                                                                       
GAGE_Compressed reference bases  38522                                                                                     
GAGE_Bad trim                    164                                                                                       
GAGE_Avg idy                     99.98                                                                                     
GAGE_SNPs                        40                                                                                        
GAGE_Indels < 5bp                406                                                                                       
GAGE_Indels >= 5                 11                                                                                        
GAGE_Inversions                  0                                                                                         
GAGE_Relocation                  3                                                                                         
GAGE_Translocation               0                                                                                         
GAGE_Corrected contig #          128                                                                                       
GAGE_Corrected assembly size     2784231                                                                                   
GAGE_Min correct contig          203                                                                                       
GAGE_Max correct contig          207893                                                                                    
GAGE_Corrected N50               67118 COUNT: 14                                                                           
