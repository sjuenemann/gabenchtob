All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.56coverage.scf
#Contigs (>= 0 bp)             446                                                                         
#Contigs (>= 1000 bp)          446                                                                         
Total length (>= 0 bp)         2716514                                                                     
Total length (>= 1000 bp)      2716514                                                                     
#Contigs                       446                                                                         
Largest contig                 42585                                                                       
Total length                   2716514                                                                     
Reference length               2813862                                                                     
GC (%)                         32.61                                                                       
Reference GC (%)               32.81                                                                       
N50                            13298                                                                       
NG50                           12362                                                                       
N75                            5426                                                                        
NG75                           4430                                                                        
#misassemblies                 5                                                                           
#local misassemblies           5                                                                           
#unaligned contigs             0 + 3 part                                                                  
Unaligned contigs length       80                                                                          
Genome fraction (%)            94.580                                                                      
Duplication ratio              1.021                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        5.11                                                                        
#indels per 100 kbp            18.15                                                                       
#genes                         2297 + 324 part                                                             
#predicted genes (unique)      2919                                                                        
#predicted genes (>= 0 bp)     2921                                                                        
#predicted genes (>= 300 bp)   2365                                                                        
#predicted genes (>= 1500 bp)  252                                                                         
#predicted genes (>= 3000 bp)  16                                                                          
Largest alignment              42580                                                                       
NA50                           13119                                                                       
NGA50                          11905                                                                       
NA75                           5354                                                                        
NGA75                          4430                                                                        
