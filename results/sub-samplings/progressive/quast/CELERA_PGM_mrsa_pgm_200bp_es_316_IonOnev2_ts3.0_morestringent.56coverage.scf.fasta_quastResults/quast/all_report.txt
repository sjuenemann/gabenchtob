All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.56coverage.scf
#Contigs                                   446                                                                         
#Contigs (>= 0 bp)                         446                                                                         
#Contigs (>= 1000 bp)                      446                                                                         
Largest contig                             42585                                                                       
Total length                               2716514                                                                     
Total length (>= 0 bp)                     2716514                                                                     
Total length (>= 1000 bp)                  2716514                                                                     
Reference length                           2813862                                                                     
N50                                        13298                                                                       
NG50                                       12362                                                                       
N75                                        5426                                                                        
NG75                                       4430                                                                        
L50                                        69                                                                          
LG50                                       73                                                                          
L75                                        150                                                                         
LG75                                       166                                                                         
#local misassemblies                       5                                                                           
#misassemblies                             5                                                                           
#misassembled contigs                      3                                                                           
Misassembled contigs length                23176                                                                       
Misassemblies inter-contig overlap         1601                                                                        
#unaligned contigs                         0 + 3 part                                                                  
Unaligned contigs length                   80                                                                          
#ambiguously mapped contigs                0                                                                           
Extra bases in ambiguously mapped contigs  0                                                                           
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        94.580                                                                      
Duplication ratio                          1.021                                                                       
#genes                                     2297 + 324 part                                                             
#predicted genes (unique)                  2919                                                                        
#predicted genes (>= 0 bp)                 2921                                                                        
#predicted genes (>= 300 bp)               2365                                                                        
#predicted genes (>= 1500 bp)              252                                                                         
#predicted genes (>= 3000 bp)              16                                                                          
#mismatches                                136                                                                         
#indels                                    483                                                                         
Indels length                              507                                                                         
#mismatches per 100 kbp                    5.11                                                                        
#indels per 100 kbp                        18.15                                                                       
GC (%)                                     32.61                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.196                                                                      
Largest alignment                          42580                                                                       
NA50                                       13119                                                                       
NGA50                                      11905                                                                       
NA75                                       5354                                                                        
NGA75                                      4430                                                                        
LA50                                       69                                                                          
LGA50                                      73                                                                          
LA75                                       151                                                                         
LGA75                                      167                                                                         
#Mis_misassemblies                         5                                                                           
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            2                                                                           
#Mis_misassembled contigs                  3                                                                           
Mis_Misassembled contigs length            23176                                                                       
#Mis_local misassemblies                   5                                                                           
#Mis_short indels (<= 5 bp)                483                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           3                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             80                                                                          
GAGE_Contigs #                             446                                                                         
GAGE_Min contig                            1000                                                                        
GAGE_Max contig                            42585                                                                       
GAGE_N50                                   12362 COUNT: 73                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2716514                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               141514(5.03%)                                                               
GAGE_Missing assembly bases                548(0.02%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            275                                                                         
GAGE_Compressed reference bases            14831                                                                       
GAGE_Bad trim                              548                                                                         
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  73                                                                          
GAGE_Indels < 5bp                          402                                                                         
GAGE_Indels >= 5                           6                                                                           
GAGE_Inversions                            3                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    454                                                                         
GAGE_Corrected assembly size               2717380                                                                     
GAGE_Min correct contig                    204                                                                         
GAGE_Max correct contig                    42580                                                                       
GAGE_Corrected N50                         11905 COUNT: 74                                                             
