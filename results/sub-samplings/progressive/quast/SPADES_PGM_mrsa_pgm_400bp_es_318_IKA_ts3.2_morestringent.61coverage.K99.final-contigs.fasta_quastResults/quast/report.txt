All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.61coverage.K99.final-contigs
#Contigs (>= 0 bp)             419                                                                                  
#Contigs (>= 1000 bp)          59                                                                                   
Total length (>= 0 bp)         2822255                                                                              
Total length (>= 1000 bp)      2757600                                                                              
#Contigs                       111                                                                                  
Largest contig                 207872                                                                               
Total length                   2780123                                                                              
Reference length               2813862                                                                              
GC (%)                         32.67                                                                                
Reference GC (%)               32.81                                                                                
N50                            112517                                                                               
NG50                           107395                                                                               
N75                            46833                                                                                
NG75                           46833                                                                                
#misassemblies                 2                                                                                    
#local misassemblies           7                                                                                    
#unaligned contigs             1 + 0 part                                                                           
Unaligned contigs length       928                                                                                  
Genome fraction (%)            98.348                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        4.08                                                                                 
#indels per 100 kbp            9.50                                                                                 
#genes                         2658 + 45 part                                                                       
#predicted genes (unique)      2714                                                                                 
#predicted genes (>= 0 bp)     2718                                                                                 
#predicted genes (>= 300 bp)   2319                                                                                 
#predicted genes (>= 1500 bp)  286                                                                                  
#predicted genes (>= 3000 bp)  30                                                                                   
Largest alignment              207872                                                                               
NA50                           112517                                                                               
NGA50                          107395                                                                               
NA75                           46833                                                                                
NGA75                          46833                                                                                
