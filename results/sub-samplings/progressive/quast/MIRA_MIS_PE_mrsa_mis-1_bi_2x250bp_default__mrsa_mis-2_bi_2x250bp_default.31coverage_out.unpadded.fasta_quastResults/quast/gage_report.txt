All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.31coverage_out.unpadded
GAGE_Contigs #                   168                                                                                             
GAGE_Min contig                  251                                                                                             
GAGE_Max contig                  334342                                                                                          
GAGE_N50                         87244 COUNT: 10                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2863450                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     1425(0.05%)                                                                                     
GAGE_Missing assembly bases      5633(0.20%)                                                                                     
GAGE_Missing assembly contigs    1(0.60%)                                                                                        
GAGE_Duplicated reference bases  56325                                                                                           
GAGE_Compressed reference bases  16677                                                                                           
GAGE_Bad trim                    12                                                                                              
GAGE_Avg idy                     99.98                                                                                           
GAGE_SNPs                        64                                                                                              
GAGE_Indels < 5bp                9                                                                                               
GAGE_Indels >= 5                 3                                                                                               
GAGE_Inversions                  2                                                                                               
GAGE_Relocation                  5                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          89                                                                                              
GAGE_Corrected assembly size     2814605                                                                                         
GAGE_Min correct contig          251                                                                                             
GAGE_Max correct contig          296612                                                                                          
GAGE_Corrected N50               75157 COUNT: 12                                                                                 
