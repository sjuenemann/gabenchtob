All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.31coverage.contigs
#Contigs (>= 0 bp)             439                                                                         
#Contigs (>= 1000 bp)          167                                                                         
Total length (>= 0 bp)         5330384                                                                     
Total length (>= 1000 bp)      5240383                                                                     
#Contigs                       354                                                                         
Largest contig                 361327                                                                      
Total length                   5318349                                                                     
Reference length               5594470                                                                     
GC (%)                         50.27                                                                       
Reference GC (%)               50.48                                                                       
N50                            99560                                                                       
NG50                           95882                                                                       
N75                            53932                                                                       
NG75                           40989                                                                       
#misassemblies                 4                                                                           
#local misassemblies           6                                                                           
#unaligned contigs             0 + 0 part                                                                  
Unaligned contigs length       0                                                                           
Genome fraction (%)            93.425                                                                      
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        1.66                                                                        
#indels per 100 kbp            24.22                                                                       
#genes                         4900 + 171 part                                                             
#predicted genes (unique)      5847                                                                        
#predicted genes (>= 0 bp)     5847                                                                        
#predicted genes (>= 300 bp)   4738                                                                        
#predicted genes (>= 1500 bp)  535                                                                         
#predicted genes (>= 3000 bp)  40                                                                          
Largest alignment              361327                                                                      
NA50                           99560                                                                       
NGA50                          94398                                                                       
NA75                           53444                                                                       
NGA75                          40989                                                                       
