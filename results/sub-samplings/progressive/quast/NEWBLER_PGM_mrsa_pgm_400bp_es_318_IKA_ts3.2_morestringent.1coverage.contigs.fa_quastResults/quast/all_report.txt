All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.1coverage.contigs
#Contigs                                   118                                                                        
#Contigs (>= 0 bp)                         127                                                                        
#Contigs (>= 1000 bp)                      8                                                                          
Largest contig                             4439                                                                       
Total length                               79189                                                                      
Total length (>= 0 bp)                     80575                                                                      
Total length (>= 1000 bp)                  15309                                                                      
Reference length                           2813862                                                                    
N50                                        703                                                                        
NG50                                       None                                                                       
N75                                        531                                                                        
NG75                                       None                                                                       
L50                                        38                                                                         
LG50                                       None                                                                       
L75                                        70                                                                         
LG75                                       None                                                                       
#local misassemblies                       0                                                                          
#misassemblies                             1                                                                          
#misassembled contigs                      1                                                                          
Misassembled contigs length                4439                                                                       
Misassemblies inter-contig overlap         0                                                                          
#unaligned contigs                         0 + 2 part                                                                 
Unaligned contigs length                   82                                                                         
#ambiguously mapped contigs                6                                                                          
Extra bases in ambiguously mapped contigs  -6943                                                                      
#N's                                       0                                                                          
#N's per 100 kbp                           0.00                                                                       
Genome fraction (%)                        2.567                                                                      
Duplication ratio                          0.999                                                                      
#genes                                     15 + 123 part                                                              
#predicted genes (unique)                  174                                                                        
#predicted genes (>= 0 bp)                 174                                                                        
#predicted genes (>= 300 bp)               79                                                                         
#predicted genes (>= 1500 bp)              0                                                                          
#predicted genes (>= 3000 bp)              0                                                                          
#mismatches                                20                                                                         
#indels                                    174                                                                        
Indels length                              177                                                                        
#mismatches per 100 kbp                    27.69                                                                      
#indels per 100 kbp                        240.89                                                                     
GC (%)                                     34.58                                                                      
Reference GC (%)                           32.81                                                                      
Average %IDY                               99.317                                                                     
Largest alignment                          4342                                                                       
NA50                                       665                                                                        
NGA50                                      None                                                                       
NA75                                       481                                                                        
NGA75                                      None                                                                       
LA50                                       43                                                                         
LGA50                                      None                                                                       
LA75                                       78                                                                         
LGA75                                      None                                                                       
#Mis_misassemblies                         1                                                                          
#Mis_relocations                           1                                                                          
#Mis_translocations                        0                                                                          
#Mis_inversions                            0                                                                          
#Mis_misassembled contigs                  1                                                                          
Mis_Misassembled contigs length            4439                                                                       
#Mis_local misassemblies                   0                                                                          
#Mis_short indels (<= 5 bp)                174                                                                        
#Mis_long indels (> 5 bp)                  0                                                                          
#Una_fully unaligned contigs               0                                                                          
Una_Fully unaligned length                 0                                                                          
#Una_partially unaligned contigs           2                                                                          
#Una_with misassembly                      0                                                                          
#Una_both parts are significant            0                                                                          
Una_Partially unaligned length             82                                                                         
GAGE_Contigs #                             118                                                                        
GAGE_Min contig                            205                                                                        
GAGE_Max contig                            4439                                                                       
GAGE_N50                                   0 COUNT: 0                                                                 
GAGE_Genome size                           2813862                                                                    
GAGE_Assembly size                         79189                                                                      
GAGE_Chaff bases                           0                                                                          
GAGE_Missing reference bases               2698204(95.89%)                                                            
GAGE_Missing assembly bases                77(0.10%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                   
GAGE_Duplicated reference bases            119                                                                        
GAGE_Compressed reference bases            37766                                                                      
GAGE_Bad trim                              77                                                                         
GAGE_Avg idy                               99.71                                                                      
GAGE_SNPs                                  31                                                                         
GAGE_Indels < 5bp                          181                                                                        
GAGE_Indels >= 5                           0                                                                          
GAGE_Inversions                            0                                                                          
GAGE_Relocation                            4                                                                          
GAGE_Translocation                         0                                                                          
GAGE_Corrected contig #                    119                                                                        
GAGE_Corrected assembly size               79404                                                                      
GAGE_Min correct contig                    207                                                                        
GAGE_Max correct contig                    4343                                                                       
GAGE_Corrected N50                         0 COUNT: 0                                                                 
