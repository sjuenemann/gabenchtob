All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.96coverage.scf
#Mis_misassemblies               2                                                                      
#Mis_relocations                 2                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        2                                                                      
Mis_Misassembled contigs length  68006                                                                  
#Mis_local misassemblies         6                                                                      
#mismatches                      152                                                                    
#indels                          222                                                                    
#Mis_short indels (<= 5 bp)      221                                                                    
#Mis_long indels (> 5 bp)        1                                                                      
Indels length                    232                                                                    
