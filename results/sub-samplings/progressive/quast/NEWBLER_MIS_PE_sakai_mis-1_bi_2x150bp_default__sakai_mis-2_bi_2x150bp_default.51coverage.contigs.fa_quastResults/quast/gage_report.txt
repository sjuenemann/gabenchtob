All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.51coverage.contigs
GAGE_Contigs #                   356                                                                                             
GAGE_Min contig                  200                                                                                             
GAGE_Max contig                  244684                                                                                          
GAGE_N50                         93156 COUNT: 17                                                                                 
GAGE_Genome size                 5594470                                                                                         
GAGE_Assembly size               5310187                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     69058(1.23%)                                                                                    
GAGE_Missing assembly bases      5394(0.10%)                                                                                     
GAGE_Missing assembly contigs    1(0.28%)                                                                                        
GAGE_Duplicated reference bases  417                                                                                             
GAGE_Compressed reference bases  224354                                                                                          
GAGE_Bad trim                    8                                                                                               
GAGE_Avg idy                     99.99                                                                                           
GAGE_SNPs                        124                                                                                             
GAGE_Indels < 5bp                12                                                                                              
GAGE_Indels >= 5                 4                                                                                               
GAGE_Inversions                  1                                                                                               
GAGE_Relocation                  3                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          362                                                                                             
GAGE_Corrected assembly size     5307115                                                                                         
GAGE_Min correct contig          200                                                                                             
GAGE_Max correct contig          238183                                                                                          
GAGE_Corrected N50               92039 COUNT: 18                                                                                 
