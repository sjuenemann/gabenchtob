All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.96coverage.contigs
#Mis_misassemblies               8                                                                           
#Mis_relocations                 3                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  5                                                                           
#Mis_misassembled contigs        8                                                                           
Mis_Misassembled contigs length  199902                                                                      
#Mis_local misassemblies         6                                                                           
#mismatches                      72                                                                          
#indels                          878                                                                         
#Mis_short indels (<= 5 bp)      877                                                                         
#Mis_long indels (> 5 bp)        1                                                                           
Indels length                    918                                                                         
