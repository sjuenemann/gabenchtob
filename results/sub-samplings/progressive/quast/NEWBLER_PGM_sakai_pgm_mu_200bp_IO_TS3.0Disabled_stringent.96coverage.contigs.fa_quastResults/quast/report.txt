All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.96coverage.contigs
#Contigs (>= 0 bp)             525                                                                         
#Contigs (>= 1000 bp)          193                                                                         
Total length (>= 0 bp)         5332103                                                                     
Total length (>= 1000 bp)      5234302                                                                     
#Contigs                       379                                                                         
Largest contig                 226262                                                                      
Total length                   5312081                                                                     
Reference length               5594470                                                                     
GC (%)                         50.27                                                                       
Reference GC (%)               50.48                                                                       
N50                            83012                                                                       
NG50                           81061                                                                       
N75                            37980                                                                       
NG75                           32594                                                                       
#misassemblies                 8                                                                           
#local misassemblies           6                                                                           
#unaligned contigs             0 + 3 part                                                                  
Unaligned contigs length       291                                                                         
Genome fraction (%)            93.404                                                                      
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        1.38                                                                        
#indels per 100 kbp            16.80                                                                       
#genes                         4880 + 192 part                                                             
#predicted genes (unique)      5673                                                                        
#predicted genes (>= 0 bp)     5673                                                                        
#predicted genes (>= 300 bp)   4648                                                                        
#predicted genes (>= 1500 bp)  569                                                                         
#predicted genes (>= 3000 bp)  49                                                                          
Largest alignment              226262                                                                      
NA50                           81579                                                                       
NGA50                          81061                                                                       
NA75                           37392                                                                       
NGA75                          32594                                                                       
