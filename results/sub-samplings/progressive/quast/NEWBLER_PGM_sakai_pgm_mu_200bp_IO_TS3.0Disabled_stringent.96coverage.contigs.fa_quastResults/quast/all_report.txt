All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.96coverage.contigs
#Contigs                                   379                                                                         
#Contigs (>= 0 bp)                         525                                                                         
#Contigs (>= 1000 bp)                      193                                                                         
Largest contig                             226262                                                                      
Total length                               5312081                                                                     
Total length (>= 0 bp)                     5332103                                                                     
Total length (>= 1000 bp)                  5234302                                                                     
Reference length                           5594470                                                                     
N50                                        83012                                                                       
NG50                                       81061                                                                       
N75                                        37980                                                                       
NG75                                       32594                                                                       
L50                                        20                                                                          
LG50                                       22                                                                          
L75                                        43                                                                          
LG75                                       49                                                                          
#local misassemblies                       6                                                                           
#misassemblies                             8                                                                           
#misassembled contigs                      8                                                                           
Misassembled contigs length                199902                                                                      
Misassemblies inter-contig overlap         1202                                                                        
#unaligned contigs                         0 + 3 part                                                                  
Unaligned contigs length                   291                                                                         
#ambiguously mapped contigs                128                                                                         
Extra bases in ambiguously mapped contigs  -87683                                                                      
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        93.404                                                                      
Duplication ratio                          1.000                                                                       
#genes                                     4880 + 192 part                                                             
#predicted genes (unique)                  5673                                                                        
#predicted genes (>= 0 bp)                 5673                                                                        
#predicted genes (>= 300 bp)               4648                                                                        
#predicted genes (>= 1500 bp)              569                                                                         
#predicted genes (>= 3000 bp)              49                                                                          
#mismatches                                72                                                                          
#indels                                    878                                                                         
Indels length                              918                                                                         
#mismatches per 100 kbp                    1.38                                                                        
#indels per 100 kbp                        16.80                                                                       
GC (%)                                     50.27                                                                       
Reference GC (%)                           50.48                                                                       
Average %IDY                               99.140                                                                      
Largest alignment                          226262                                                                      
NA50                                       81579                                                                       
NGA50                                      81061                                                                       
NA75                                       37392                                                                       
NGA75                                      32594                                                                       
LA50                                       21                                                                          
LGA50                                      22                                                                          
LA75                                       44                                                                          
LGA75                                      50                                                                          
#Mis_misassemblies                         8                                                                           
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            5                                                                           
#Mis_misassembled contigs                  8                                                                           
Mis_Misassembled contigs length            199902                                                                      
#Mis_local misassemblies                   6                                                                           
#Mis_short indels (<= 5 bp)                877                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           3                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             291                                                                         
GAGE_Contigs #                             379                                                                         
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            226262                                                                      
GAGE_N50                                   81061 COUNT: 22                                                             
GAGE_Genome size                           5594470                                                                     
GAGE_Assembly size                         5312081                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               76002(1.36%)                                                                
GAGE_Missing assembly bases                186(0.00%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            1323                                                                        
GAGE_Compressed reference bases            220991                                                                      
GAGE_Bad trim                              186                                                                         
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  104                                                                         
GAGE_Indels < 5bp                          945                                                                         
GAGE_Indels >= 5                           6                                                                           
GAGE_Inversions                            2                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    378                                                                         
GAGE_Corrected assembly size               5311664                                                                     
GAGE_Min correct contig                    200                                                                         
GAGE_Max correct contig                    198588                                                                      
GAGE_Corrected N50                         73773 COUNT: 24                                                             
