All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.31coverage.K99.final-contigs
#Mis_misassemblies               4                                                                                    
#Mis_relocations                 4                                                                                    
#Mis_translocations              0                                                                                    
#Mis_inversions                  0                                                                                    
#Mis_misassembled contigs        3                                                                                    
Mis_Misassembled contigs length  184516                                                                               
#Mis_local misassemblies         14                                                                                   
#mismatches                      169                                                                                  
#indels                          513                                                                                  
#Mis_short indels (<= 5 bp)      510                                                                                  
#Mis_long indels (> 5 bp)        3                                                                                    
Indels length                    587                                                                                  
