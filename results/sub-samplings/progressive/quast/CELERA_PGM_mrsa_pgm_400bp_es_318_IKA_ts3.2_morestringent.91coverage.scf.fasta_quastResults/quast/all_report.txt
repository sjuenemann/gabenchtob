All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.91coverage.scf
#Contigs                                   68                                                                     
#Contigs (>= 0 bp)                         68                                                                     
#Contigs (>= 1000 bp)                      68                                                                     
Largest contig                             212010                                                                 
Total length                               2746982                                                                
Total length (>= 0 bp)                     2746982                                                                
Total length (>= 1000 bp)                  2746982                                                                
Reference length                           2813862                                                                
N50                                        71169                                                                  
NG50                                       71169                                                                  
N75                                        44411                                                                  
NG75                                       42180                                                                  
L50                                        12                                                                     
LG50                                       12                                                                     
L75                                        24                                                                     
LG75                                       25                                                                     
#local misassemblies                       4                                                                      
#misassemblies                             1                                                                      
#misassembled contigs                      1                                                                      
Misassembled contigs length                63743                                                                  
Misassemblies inter-contig overlap         1534                                                                   
#unaligned contigs                         0 + 0 part                                                             
Unaligned contigs length                   0                                                                      
#ambiguously mapped contigs                1                                                                      
Extra bases in ambiguously mapped contigs  -1248                                                                  
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        97.051                                                                 
Duplication ratio                          1.006                                                                  
#genes                                     2624 + 34 part                                                         
#predicted genes (unique)                  2660                                                                   
#predicted genes (>= 0 bp)                 2664                                                                   
#predicted genes (>= 300 bp)               2287                                                                   
#predicted genes (>= 1500 bp)              285                                                                    
#predicted genes (>= 3000 bp)              28                                                                     
#mismatches                                122                                                                    
#indels                                    199                                                                    
Indels length                              221                                                                    
#mismatches per 100 kbp                    4.47                                                                   
#indels per 100 kbp                        7.29                                                                   
GC (%)                                     32.63                                                                  
Reference GC (%)                           32.81                                                                  
Average %IDY                               98.305                                                                 
Largest alignment                          212010                                                                 
NA50                                       71169                                                                  
NGA50                                      71169                                                                  
NA75                                       44410                                                                  
NGA75                                      42180                                                                  
LA50                                       12                                                                     
LGA50                                      12                                                                     
LA75                                       24                                                                     
LGA75                                      25                                                                     
#Mis_misassemblies                         1                                                                      
#Mis_relocations                           1                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  1                                                                      
Mis_Misassembled contigs length            63743                                                                  
#Mis_local misassemblies                   4                                                                      
#Mis_short indels (<= 5 bp)                198                                                                    
#Mis_long indels (> 5 bp)                  1                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           0                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             0                                                                      
GAGE_Contigs #                             68                                                                     
GAGE_Min contig                            1056                                                                   
GAGE_Max contig                            212010                                                                 
GAGE_N50                                   71169 COUNT: 12                                                        
GAGE_Genome size                           2813862                                                                
GAGE_Assembly size                         2746982                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               74486(2.65%)                                                           
GAGE_Missing assembly bases                32(0.00%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            0                                                                      
GAGE_Compressed reference bases            12215                                                                  
GAGE_Bad trim                              32                                                                     
GAGE_Avg idy                               99.98                                                                  
GAGE_SNPs                                  36                                                                     
GAGE_Indels < 5bp                          165                                                                    
GAGE_Indels >= 5                           5                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            1                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    74                                                                     
GAGE_Corrected assembly size               2748626                                                                
GAGE_Min correct contig                    868                                                                    
GAGE_Max correct contig                    184239                                                                 
GAGE_Corrected N50                         70419 COUNT: 13                                                        
