All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.56coverage_out.unpadded
#Una_fully unaligned contigs      1                                                                  
Una_Fully unaligned length        214                                                                
#Una_partially unaligned contigs  1                                                                  
#Una_with misassembly             0                                                                  
#Una_both parts are significant   0                                                                  
Una_Partially unaligned length    180                                                                
#N's                              26                                                                 
