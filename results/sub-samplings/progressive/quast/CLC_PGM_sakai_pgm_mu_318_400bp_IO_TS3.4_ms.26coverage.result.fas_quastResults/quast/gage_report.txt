All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.26coverage.result
GAGE_Contigs #                   711                                                         
GAGE_Min contig                  200                                                         
GAGE_Max contig                  169938                                                      
GAGE_N50                         37603 COUNT: 40                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5407031                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     36413(0.65%)                                                
GAGE_Missing assembly bases      187(0.00%)                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                    
GAGE_Duplicated reference bases  56222                                                       
GAGE_Compressed reference bases  238313                                                      
GAGE_Bad trim                    187                                                         
GAGE_Avg idy                     99.98                                                       
GAGE_SNPs                        166                                                         
GAGE_Indels < 5bp                886                                                         
GAGE_Indels >= 5                 6                                                           
GAGE_Inversions                  0                                                           
GAGE_Relocation                  7                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          544                                                         
GAGE_Corrected assembly size     5354978                                                     
GAGE_Min correct contig          200                                                         
GAGE_Max correct contig          169939                                                      
GAGE_Corrected N50               37602 COUNT: 41                                             
