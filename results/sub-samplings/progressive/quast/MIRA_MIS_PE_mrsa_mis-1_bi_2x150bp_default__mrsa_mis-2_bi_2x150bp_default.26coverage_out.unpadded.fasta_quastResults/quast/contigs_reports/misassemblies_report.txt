All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.26coverage_out.unpadded
#Mis_misassemblies               13                                                                                              
#Mis_relocations                 13                                                                                              
#Mis_translocations              0                                                                                               
#Mis_inversions                  0                                                                                               
#Mis_misassembled contigs        10                                                                                              
Mis_Misassembled contigs length  338165                                                                                          
#Mis_local misassemblies         7                                                                                               
#mismatches                      197                                                                                             
#indels                          121                                                                                             
#Mis_short indels (<= 5 bp)      120                                                                                             
#Mis_long indels (> 5 bp)        1                                                                                               
Indels length                    156                                                                                             
