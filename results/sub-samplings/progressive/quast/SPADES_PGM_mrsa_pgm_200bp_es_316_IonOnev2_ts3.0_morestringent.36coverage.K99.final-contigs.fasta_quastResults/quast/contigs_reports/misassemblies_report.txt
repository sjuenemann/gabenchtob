All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.36coverage.K99.final-contigs
#Mis_misassemblies               7                                                                                         
#Mis_relocations                 7                                                                                         
#Mis_translocations              0                                                                                         
#Mis_inversions                  0                                                                                         
#Mis_misassembled contigs        6                                                                                         
Mis_Misassembled contigs length  325694                                                                                    
#Mis_local misassemblies         8                                                                                         
#mismatches                      163                                                                                       
#indels                          514                                                                                       
#Mis_short indels (<= 5 bp)      512                                                                                       
#Mis_long indels (> 5 bp)        2                                                                                         
Indels length                    634                                                                                       
