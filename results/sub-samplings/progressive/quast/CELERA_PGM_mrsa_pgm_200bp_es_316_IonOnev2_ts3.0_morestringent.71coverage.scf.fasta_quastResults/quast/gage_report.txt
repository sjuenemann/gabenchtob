All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.71coverage.scf
GAGE_Contigs #                   434                                                                         
GAGE_Min contig                  1007                                                                        
GAGE_Max contig                  49374                                                                       
GAGE_N50                         11093 COUNT: 77                                                             
GAGE_Genome size                 2813862                                                                     
GAGE_Assembly size               2691237                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     150921(5.36%)                                                               
GAGE_Missing assembly bases      446(0.02%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  142                                                                         
GAGE_Compressed reference bases  22466                                                                       
GAGE_Bad trim                    446                                                                         
GAGE_Avg idy                     99.97                                                                       
GAGE_SNPs                        57                                                                          
GAGE_Indels < 5bp                413                                                                         
GAGE_Indels >= 5                 4                                                                           
GAGE_Inversions                  1                                                                           
GAGE_Relocation                  3                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          441                                                                         
GAGE_Corrected assembly size     2692039                                                                     
GAGE_Min correct contig          207                                                                         
GAGE_Max correct contig          39455                                                                       
GAGE_Corrected N50               10923 COUNT: 78                                                             
