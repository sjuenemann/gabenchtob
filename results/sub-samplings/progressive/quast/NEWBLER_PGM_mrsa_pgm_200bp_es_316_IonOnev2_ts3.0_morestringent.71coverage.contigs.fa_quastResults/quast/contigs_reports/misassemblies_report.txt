All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.71coverage.contigs
#Mis_misassemblies               2                                                                                
#Mis_relocations                 1                                                                                
#Mis_translocations              0                                                                                
#Mis_inversions                  1                                                                                
#Mis_misassembled contigs        2                                                                                
Mis_Misassembled contigs length  4588                                                                             
#Mis_local misassemblies         5                                                                                
#mismatches                      46                                                                               
#indels                          218                                                                              
#Mis_short indels (<= 5 bp)      217                                                                              
#Mis_long indels (> 5 bp)        1                                                                                
Indels length                    313                                                                              
