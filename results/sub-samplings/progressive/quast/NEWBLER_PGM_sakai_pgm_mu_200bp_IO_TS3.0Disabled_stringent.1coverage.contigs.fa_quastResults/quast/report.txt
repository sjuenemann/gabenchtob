All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.1coverage.contigs
#Contigs (>= 0 bp)             385                                                                        
#Contigs (>= 1000 bp)          13                                                                         
Total length (>= 0 bp)         165992                                                                     
Total length (>= 1000 bp)      21132                                                                      
#Contigs                       348                                                                        
Largest contig                 3192                                                                       
Total length                   160453                                                                     
Reference length               5594470                                                                    
GC (%)                         52.09                                                                      
Reference GC (%)               50.48                                                                      
N50                            446                                                                        
NG50                           None                                                                       
N75                            359                                                                        
NG75                           None                                                                       
#misassemblies                 18                                                                         
#local misassemblies           1                                                                          
#unaligned contigs             1 + 4 part                                                                 
Unaligned contigs length       669                                                                        
Genome fraction (%)            2.302                                                                      
Duplication ratio              1.005                                                                      
#N's per 100 kbp               0.00                                                                       
#mismatches per 100 kbp        243.00                                                                     
#indels per 100 kbp            145.95                                                                     
#genes                         25 + 294 part                                                              
#predicted genes (unique)      436                                                                        
#predicted genes (>= 0 bp)     436                                                                        
#predicted genes (>= 300 bp)   186                                                                        
#predicted genes (>= 1500 bp)  0                                                                          
#predicted genes (>= 3000 bp)  0                                                                          
Largest alignment              3192                                                                       
NA50                           372                                                                        
NGA50                          None                                                                       
NA75                           236                                                                        
NGA75                          None                                                                       
