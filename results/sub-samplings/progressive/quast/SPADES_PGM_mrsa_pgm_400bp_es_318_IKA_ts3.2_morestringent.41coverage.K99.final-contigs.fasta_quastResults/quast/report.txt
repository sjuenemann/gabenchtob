All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.41coverage.K99.final-contigs
#Contigs (>= 0 bp)             371                                                                                  
#Contigs (>= 1000 bp)          57                                                                                   
Total length (>= 0 bp)         2812645                                                                              
Total length (>= 1000 bp)      2756109                                                                              
#Contigs                       99                                                                                   
Largest contig                 207865                                                                               
Total length                   2775174                                                                              
Reference length               2813862                                                                              
GC (%)                         32.67                                                                                
Reference GC (%)               32.81                                                                                
N50                            112397                                                                               
NG50                           88411                                                                                
N75                            46833                                                                                
NG75                           46757                                                                                
#misassemblies                 3                                                                                    
#local misassemblies           12                                                                                   
#unaligned contigs             0 + 1 part                                                                           
Unaligned contigs length       60                                                                                   
Genome fraction (%)            98.268                                                                               
Duplication ratio              1.001                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        5.89                                                                                 
#indels per 100 kbp            13.78                                                                                
#genes                         2653 + 47 part                                                                       
#predicted genes (unique)      2739                                                                                 
#predicted genes (>= 0 bp)     2741                                                                                 
#predicted genes (>= 300 bp)   2334                                                                                 
#predicted genes (>= 1500 bp)  274                                                                                  
#predicted genes (>= 3000 bp)  31                                                                                   
Largest alignment              207865                                                                               
NA50                           112397                                                                               
NGA50                          88411                                                                                
NA75                           46833                                                                                
NGA75                          46757                                                                                
