All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.91coverage.K99.final-contigs
GAGE_Contigs #                   437                                                                                  
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  374866                                                                               
GAGE_N50                         124324 COUNT: 15                                                                     
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5380043                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     9254(0.17%)                                                                          
GAGE_Missing assembly bases      0(0.00%)                                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  1468                                                                                 
GAGE_Compressed reference bases  272328                                                                               
GAGE_Bad trim                    0                                                                                    
GAGE_Avg idy                     99.97                                                                                
GAGE_SNPs                        152                                                                                  
GAGE_Indels < 5bp                1434                                                                                 
GAGE_Indels >= 5                 9                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  6                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          447                                                                                  
GAGE_Corrected assembly size     5382031                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          226975                                                                               
GAGE_Corrected N50               116683 COUNT: 18                                                                     
