All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.36coverage_out.unpadded
#Contigs (>= 0 bp)             143                                                                           
#Contigs (>= 1000 bp)          51                                                                            
Total length (>= 0 bp)         2888130                                                                       
Total length (>= 1000 bp)      2839283                                                                       
#Contigs                       138                                                                           
Largest contig                 415525                                                                        
Total length                   2887468                                                                       
Reference length               2813862                                                                       
GC (%)                         32.84                                                                         
Reference GC (%)               32.81                                                                         
N50                            239166                                                                        
NG50                           302773                                                                        
N75                            122315                                                                        
NG75                           122315                                                                        
#misassemblies                 18                                                                            
#local misassemblies           16                                                                            
#unaligned contigs             1 + 1 part                                                                    
Unaligned contigs length       609                                                                           
Genome fraction (%)            99.889                                                                        
Duplication ratio              1.031                                                                         
#N's per 100 kbp               0.73                                                                          
#mismatches per 100 kbp        7.61                                                                          
#indels per 100 kbp            6.12                                                                          
#genes                         2695 + 31 part                                                                
#predicted genes (unique)      2778                                                                          
#predicted genes (>= 0 bp)     2793                                                                          
#predicted genes (>= 300 bp)   2364                                                                          
#predicted genes (>= 1500 bp)  297                                                                           
#predicted genes (>= 3000 bp)  26                                                                            
Largest alignment              397510                                                                        
NA50                           200170                                                                        
NGA50                          200170                                                                        
NA75                           64374                                                                         
NGA75                          69645                                                                         
