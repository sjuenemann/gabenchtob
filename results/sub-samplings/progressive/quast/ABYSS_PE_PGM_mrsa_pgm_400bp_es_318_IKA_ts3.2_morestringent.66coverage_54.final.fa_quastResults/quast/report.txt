All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.66coverage_54.final
#Contigs (>= 0 bp)             4707                                                                          
#Contigs (>= 1000 bp)          302                                                                           
Total length (>= 0 bp)         3084729                                                                       
Total length (>= 1000 bp)      2670857                                                                       
#Contigs                       455                                                                           
Largest contig                 58974                                                                         
Total length                   2739654                                                                       
Reference length               2813862                                                                       
GC (%)                         32.59                                                                         
Reference GC (%)               32.81                                                                         
N50                            13784                                                                         
NG50                           13511                                                                         
N75                            7149                                                                          
NG75                           6774                                                                          
#misassemblies                 2                                                                             
#local misassemblies           4                                                                             
#unaligned contigs             0 + 0 part                                                                    
Unaligned contigs length       0                                                                             
Genome fraction (%)            97.150                                                                        
Duplication ratio              1.002                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        3.99                                                                          
#indels per 100 kbp            4.57                                                                          
#genes                         2384 + 255 part                                                               
#predicted genes (unique)      2862                                                                          
#predicted genes (>= 0 bp)     2863                                                                          
#predicted genes (>= 300 bp)   2346                                                                          
#predicted genes (>= 1500 bp)  267                                                                           
#predicted genes (>= 3000 bp)  22                                                                            
Largest alignment              58974                                                                         
NA50                           13784                                                                         
NGA50                          13511                                                                         
NA75                           7149                                                                          
NGA75                          6774                                                                          
