All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.41coverage.contigs
#Contigs (>= 0 bp)             99                                                                          
#Contigs (>= 1000 bp)          44                                                                          
Total length (>= 0 bp)         2782149                                                                     
Total length (>= 1000 bp)      2770126                                                                     
#Contigs                       61                                                                          
Largest contig                 304608                                                                      
Total length                   2776866                                                                     
Reference length               2813862                                                                     
GC (%)                         32.65                                                                       
Reference GC (%)               32.81                                                                       
N50                            179280                                                                      
NG50                           179280                                                                      
N75                            75468                                                                       
NG75                           74212                                                                       
#misassemblies                 2                                                                           
#local misassemblies           5                                                                           
#unaligned contigs             0 + 0 part                                                                  
Unaligned contigs length       0                                                                           
Genome fraction (%)            98.335                                                                      
Duplication ratio              1.001                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        1.99                                                                        
#indels per 100 kbp            5.02                                                                        
#genes                         2674 + 27 part                                                              
#predicted genes (unique)      2680                                                                        
#predicted genes (>= 0 bp)     2681                                                                        
#predicted genes (>= 300 bp)   2318                                                                        
#predicted genes (>= 1500 bp)  287                                                                         
#predicted genes (>= 3000 bp)  27                                                                          
Largest alignment              304608                                                                      
NA50                           179280                                                                      
NGA50                          179280                                                                      
NA75                           74212                                                                       
NGA75                          74212                                                                       
