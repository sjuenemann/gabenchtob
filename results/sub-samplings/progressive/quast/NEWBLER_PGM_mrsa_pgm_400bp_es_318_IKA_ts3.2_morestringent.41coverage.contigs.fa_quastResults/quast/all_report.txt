All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.41coverage.contigs
#Contigs                                   61                                                                          
#Contigs (>= 0 bp)                         99                                                                          
#Contigs (>= 1000 bp)                      44                                                                          
Largest contig                             304608                                                                      
Total length                               2776866                                                                     
Total length (>= 0 bp)                     2782149                                                                     
Total length (>= 1000 bp)                  2770126                                                                     
Reference length                           2813862                                                                     
N50                                        179280                                                                      
NG50                                       179280                                                                      
N75                                        75468                                                                       
NG75                                       74212                                                                       
L50                                        6                                                                           
LG50                                       6                                                                           
L75                                        12                                                                          
LG75                                       13                                                                          
#local misassemblies                       5                                                                           
#misassemblies                             2                                                                           
#misassembled contigs                      2                                                                           
Misassembled contigs length                140067                                                                      
Misassemblies inter-contig overlap         719                                                                         
#unaligned contigs                         0 + 0 part                                                                  
Unaligned contigs length                   0                                                                           
#ambiguously mapped contigs                11                                                                          
Extra bases in ambiguously mapped contigs  -9058                                                                       
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        98.335                                                                      
Duplication ratio                          1.001                                                                       
#genes                                     2674 + 27 part                                                              
#predicted genes (unique)                  2680                                                                        
#predicted genes (>= 0 bp)                 2681                                                                        
#predicted genes (>= 300 bp)               2318                                                                        
#predicted genes (>= 1500 bp)              287                                                                         
#predicted genes (>= 3000 bp)              27                                                                          
#mismatches                                55                                                                          
#indels                                    139                                                                         
Indels length                              200                                                                         
#mismatches per 100 kbp                    1.99                                                                        
#indels per 100 kbp                        5.02                                                                        
GC (%)                                     32.65                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               98.527                                                                      
Largest alignment                          304608                                                                      
NA50                                       179280                                                                      
NGA50                                      179280                                                                      
NA75                                       74212                                                                       
NGA75                                      74212                                                                       
LA50                                       6                                                                           
LGA50                                      6                                                                           
LA75                                       13                                                                          
LGA75                                      13                                                                          
#Mis_misassemblies                         2                                                                           
#Mis_relocations                           2                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            0                                                                           
#Mis_misassembled contigs                  2                                                                           
Mis_Misassembled contigs length            140067                                                                      
#Mis_local misassemblies                   5                                                                           
#Mis_short indels (<= 5 bp)                138                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           0                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             0                                                                           
GAGE_Contigs #                             61                                                                          
GAGE_Min contig                            201                                                                         
GAGE_Max contig                            304608                                                                      
GAGE_N50                                   179280 COUNT: 6                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2776866                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               5103(0.18%)                                                                 
GAGE_Missing assembly bases                6(0.00%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            985                                                                         
GAGE_Compressed reference bases            35385                                                                       
GAGE_Bad trim                              6                                                                           
GAGE_Avg idy                               99.99                                                                       
GAGE_SNPs                                  57                                                                          
GAGE_Indels < 5bp                          143                                                                         
GAGE_Indels >= 5                           4                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            3                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    64                                                                          
GAGE_Corrected assembly size               2776828                                                                     
GAGE_Min correct contig                    236                                                                         
GAGE_Max correct contig                    297950                                                                      
GAGE_Corrected N50                         106661 COUNT: 8                                                             
