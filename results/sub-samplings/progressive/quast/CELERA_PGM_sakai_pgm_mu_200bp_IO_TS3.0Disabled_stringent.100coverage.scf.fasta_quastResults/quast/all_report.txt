All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.100coverage.scf
#Contigs                                   757                                                                     
#Contigs (>= 0 bp)                         757                                                                     
#Contigs (>= 1000 bp)                      757                                                                     
Largest contig                             60767                                                                   
Total length                               5224950                                                                 
Total length (>= 0 bp)                     5224950                                                                 
Total length (>= 1000 bp)                  5224950                                                                 
Reference length                           5594470                                                                 
N50                                        15015                                                                   
NG50                                       14058                                                                   
N75                                        6606                                                                    
NG75                                       4804                                                                    
L50                                        115                                                                     
LG50                                       128                                                                     
L75                                        242                                                                     
LG75                                       292                                                                     
#local misassemblies                       5                                                                       
#misassemblies                             7                                                                       
#misassembled contigs                      7                                                                       
Misassembled contigs length                78781                                                                   
Misassemblies inter-contig overlap         1802                                                                    
#unaligned contigs                         0 + 5 part                                                              
Unaligned contigs length                   165                                                                     
#ambiguously mapped contigs                1                                                                       
Extra bases in ambiguously mapped contigs  -1307                                                                   
#N's                                       0                                                                       
#N's per 100 kbp                           0.00                                                                    
Genome fraction (%)                        91.747                                                                  
Duplication ratio                          1.018                                                                   
#genes                                     4406 + 625 part                                                         
#predicted genes (unique)                  5823                                                                    
#predicted genes (>= 0 bp)                 5823                                                                    
#predicted genes (>= 300 bp)               4697                                                                    
#predicted genes (>= 1500 bp)              530                                                                     
#predicted genes (>= 3000 bp)              36                                                                      
#mismatches                                190                                                                     
#indels                                    849                                                                     
Indels length                              866                                                                     
#mismatches per 100 kbp                    3.70                                                                    
#indels per 100 kbp                        16.54                                                                   
GC (%)                                     50.23                                                                   
Reference GC (%)                           50.48                                                                   
Average %IDY                               99.230                                                                  
Largest alignment                          60766                                                                   
NA50                                       15007                                                                   
NGA50                                      13960                                                                   
NA75                                       6545                                                                    
NGA75                                      4757                                                                    
LA50                                       117                                                                     
LGA50                                      130                                                                     
LA75                                       245                                                                     
LGA75                                      294                                                                     
#Mis_misassemblies                         7                                                                       
#Mis_relocations                           6                                                                       
#Mis_translocations                        1                                                                       
#Mis_inversions                            0                                                                       
#Mis_misassembled contigs                  7                                                                       
Mis_Misassembled contigs length            78781                                                                   
#Mis_local misassemblies                   5                                                                       
#Mis_short indels (<= 5 bp)                848                                                                     
#Mis_long indels (> 5 bp)                  1                                                                       
#Una_fully unaligned contigs               0                                                                       
Una_Fully unaligned length                 0                                                                       
#Una_partially unaligned contigs           5                                                                       
#Una_with misassembly                      0                                                                       
#Una_both parts are significant            0                                                                       
Una_Partially unaligned length             165                                                                     
GAGE_Contigs #                             757                                                                     
GAGE_Min contig                            1003                                                                    
GAGE_Max contig                            60767                                                                   
GAGE_N50                                   14058 COUNT: 128                                                        
GAGE_Genome size                           5594470                                                                 
GAGE_Assembly size                         5224950                                                                 
GAGE_Chaff bases                           0                                                                       
GAGE_Missing reference bases               398481(7.12%)                                                           
GAGE_Missing assembly bases                901(0.02%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                                
GAGE_Duplicated reference bases            136                                                                     
GAGE_Compressed reference bases            84368                                                                   
GAGE_Bad trim                              840                                                                     
GAGE_Avg idy                               99.98                                                                   
GAGE_SNPs                                  117                                                                     
GAGE_Indels < 5bp                          764                                                                     
GAGE_Indels >= 5                           8                                                                       
GAGE_Inversions                            2                                                                       
GAGE_Relocation                            4                                                                       
GAGE_Translocation                         1                                                                       
GAGE_Corrected contig #                    769                                                                     
GAGE_Corrected assembly size               5225863                                                                 
GAGE_Min correct contig                    537                                                                     
GAGE_Max correct contig                    50034                                                                   
GAGE_Corrected N50                         13385 COUNT: 131                                                        
