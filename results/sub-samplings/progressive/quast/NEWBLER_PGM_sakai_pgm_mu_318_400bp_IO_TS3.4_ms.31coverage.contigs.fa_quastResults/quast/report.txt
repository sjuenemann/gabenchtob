All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.31coverage.contigs
#Contigs (>= 0 bp)             380                                                              
#Contigs (>= 1000 bp)          161                                                              
Total length (>= 0 bp)         5360235                                                          
Total length (>= 1000 bp)      5279925                                                          
#Contigs                       306                                                              
Largest contig                 326734                                                           
Total length                   5349685                                                          
Reference length               5594470                                                          
GC (%)                         50.30                                                            
Reference GC (%)               50.48                                                            
N50                            103789                                                           
NG50                           99574                                                            
N75                            53930                                                            
NG75                           43143                                                            
#misassemblies                 3                                                                
#local misassemblies           12                                                               
#unaligned contigs             0 + 0 part                                                       
Unaligned contigs length       0                                                                
Genome fraction (%)            94.028                                                           
Duplication ratio              1.000                                                            
#N's per 100 kbp               0.00                                                             
#mismatches per 100 kbp        2.17                                                             
#indels per 100 kbp            7.13                                                             
#genes                         4954 + 148 part                                                  
#predicted genes (unique)      5395                                                             
#predicted genes (>= 0 bp)     5397                                                             
#predicted genes (>= 300 bp)   4533                                                             
#predicted genes (>= 1500 bp)  638                                                              
#predicted genes (>= 3000 bp)  62                                                               
Largest alignment              326734                                                           
NA50                           103789                                                           
NGA50                          99574                                                            
NA75                           53930                                                            
NGA75                          43143                                                            
