All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.51coverage.K99.final-contigs
#Contigs (>= 0 bp)             254                                                                                       
#Contigs (>= 1000 bp)          62                                                                                        
Total length (>= 0 bp)         2796598                                                                                   
Total length (>= 1000 bp)      2759200                                                                                   
#Contigs                       101                                                                                       
Largest contig                 236814                                                                                    
Total length                   2775714                                                                                   
Reference length               2813862                                                                                   
GC (%)                         32.66                                                                                     
Reference GC (%)               32.81                                                                                     
N50                            74623                                                                                     
NG50                           74623                                                                                     
N75                            39826                                                                                     
NG75                           39344                                                                                     
#misassemblies                 4                                                                                         
#local misassemblies           11                                                                                        
#unaligned contigs             0 + 2 part                                                                                
Unaligned contigs length       33                                                                                        
Genome fraction (%)            98.235                                                                                    
Duplication ratio              1.002                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        7.27                                                                                      
#indels per 100 kbp            16.42                                                                                     
#genes                         2645 + 44 part                                                                            
#predicted genes (unique)      2733                                                                                      
#predicted genes (>= 0 bp)     2735                                                                                      
#predicted genes (>= 300 bp)   2317                                                                                      
#predicted genes (>= 1500 bp)  284                                                                                       
#predicted genes (>= 3000 bp)  23                                                                                        
Largest alignment              236814                                                                                    
NA50                           74524                                                                                     
NGA50                          74524                                                                                     
NA75                           39344                                                                                     
NGA75                          38333                                                                                     
