All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.51coverage.K99.final-contigs
#Mis_misassemblies               4                                                                                         
#Mis_relocations                 3                                                                                         
#Mis_translocations              0                                                                                         
#Mis_inversions                  1                                                                                         
#Mis_misassembled contigs        4                                                                                         
Mis_Misassembled contigs length  138508                                                                                    
#Mis_local misassemblies         11                                                                                        
#mismatches                      201                                                                                       
#indels                          454                                                                                       
#Mis_short indels (<= 5 bp)      453                                                                                       
#Mis_long indels (> 5 bp)        1                                                                                         
Indels length                    502                                                                                       
