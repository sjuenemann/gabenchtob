All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.1coverage_out.unpadded
#Contigs (>= 0 bp)             239                                                                          
#Contigs (>= 1000 bp)          7                                                                            
Total length (>= 0 bp)         134878                                                                       
Total length (>= 1000 bp)      15664                                                                        
#Contigs                       238                                                                          
Largest contig                 4735                                                                         
Total length                   134680                                                                       
Reference length               2813862                                                                      
GC (%)                         34.44                                                                        
Reference GC (%)               32.81                                                                        
N50                            577                                                                          
NG50                           None                                                                         
N75                            452                                                                          
NG75                           None                                                                         
#misassemblies                 6                                                                            
#local misassemblies           1                                                                            
#unaligned contigs             0 + 0 part                                                                   
Unaligned contigs length       0                                                                            
Genome fraction (%)            4.664                                                                        
Duplication ratio              1.005                                                                        
#N's per 100 kbp               54.95                                                                        
#mismatches per 100 kbp        58.67                                                                        
#indels per 100 kbp            333.71                                                                       
#genes                         29 + 239 part                                                                
#predicted genes (unique)      333                                                                          
#predicted genes (>= 0 bp)     333                                                                          
#predicted genes (>= 300 bp)   136                                                                          
#predicted genes (>= 1500 bp)  0                                                                            
#predicted genes (>= 3000 bp)  0                                                                            
Largest alignment              3777                                                                         
NA50                           560                                                                          
NGA50                          None                                                                         
NA75                           438                                                                          
NGA75                          None                                                                         
