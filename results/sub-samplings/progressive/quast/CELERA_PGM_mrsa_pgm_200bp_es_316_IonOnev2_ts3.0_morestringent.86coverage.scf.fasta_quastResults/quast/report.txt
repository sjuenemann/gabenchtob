All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.86coverage.scf
#Contigs (>= 0 bp)             455                                                                         
#Contigs (>= 1000 bp)          455                                                                         
Total length (>= 0 bp)         2681620                                                                     
Total length (>= 1000 bp)      2681620                                                                     
#Contigs                       455                                                                         
Largest contig                 49509                                                                       
Total length                   2681620                                                                     
Reference length               2813862                                                                     
GC (%)                         32.65                                                                       
Reference GC (%)               32.81                                                                       
N50                            10429                                                                       
NG50                           9893                                                                        
N75                            5658                                                                        
NG75                           4810                                                                        
#misassemblies                 12                                                                          
#local misassemblies           6                                                                           
#unaligned contigs             0 + 7 part                                                                  
Unaligned contigs length       538                                                                         
Genome fraction (%)            93.665                                                                      
Duplication ratio              1.019                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        9.07                                                                        
#indels per 100 kbp            19.08                                                                       
#genes                         2244 + 361 part                                                             
#predicted genes (unique)      2846                                                                        
#predicted genes (>= 0 bp)     2846                                                                        
#predicted genes (>= 300 bp)   2342                                                                        
#predicted genes (>= 1500 bp)  251                                                                         
#predicted genes (>= 3000 bp)  23                                                                          
Largest alignment              49483                                                                       
NA50                           10307                                                                       
NGA50                          9889                                                                        
NA75                           5656                                                                        
NGA75                          4670                                                                        
