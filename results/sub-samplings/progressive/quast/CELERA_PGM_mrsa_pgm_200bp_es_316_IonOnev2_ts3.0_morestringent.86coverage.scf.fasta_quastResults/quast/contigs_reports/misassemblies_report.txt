All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.86coverage.scf
#Mis_misassemblies               12                                                                          
#Mis_relocations                 8                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  4                                                                           
#Mis_misassembled contigs        12                                                                          
Mis_Misassembled contigs length  51671                                                                       
#Mis_local misassemblies         6                                                                           
#mismatches                      239                                                                         
#indels                          503                                                                         
#Mis_short indels (<= 5 bp)      500                                                                         
#Mis_long indels (> 5 bp)        3                                                                           
Indels length                    582                                                                         
