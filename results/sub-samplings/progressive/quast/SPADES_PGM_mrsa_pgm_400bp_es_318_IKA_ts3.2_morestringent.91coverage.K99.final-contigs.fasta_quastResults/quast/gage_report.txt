All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.91coverage.K99.final-contigs
GAGE_Contigs #                   108                                                                                  
GAGE_Min contig                  202                                                                                  
GAGE_Max contig                  236828                                                                               
GAGE_N50                         112508 COUNT: 9                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               2780208                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     5447(0.19%)                                                                          
GAGE_Missing assembly bases      956(0.03%)                                                                           
GAGE_Missing assembly contigs    1(0.93%)                                                                             
GAGE_Duplicated reference bases  446                                                                                  
GAGE_Compressed reference bases  38513                                                                                
GAGE_Bad trim                    28                                                                                   
GAGE_Avg idy                     99.99                                                                                
GAGE_SNPs                        41                                                                                   
GAGE_Indels < 5bp                177                                                                                  
GAGE_Indels >= 5                 9                                                                                    
GAGE_Inversions                  1                                                                                    
GAGE_Relocation                  2                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          117                                                                                  
GAGE_Corrected assembly size     2781027                                                                              
GAGE_Min correct contig          202                                                                                  
GAGE_Max correct contig          194448                                                                               
GAGE_Corrected N50               75657 COUNT: 11                                                                      
