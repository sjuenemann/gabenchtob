All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.36coverage.contigs
#Contigs (>= 0 bp)             95                                                                          
#Contigs (>= 1000 bp)          41                                                                          
Total length (>= 0 bp)         2781101                                                                     
Total length (>= 1000 bp)      2769115                                                                     
#Contigs                       59                                                                          
Largest contig                 297663                                                                      
Total length                   2776217                                                                     
Reference length               2813862                                                                     
GC (%)                         32.65                                                                       
Reference GC (%)               32.81                                                                       
N50                            179310                                                                      
NG50                           179310                                                                      
N75                            81194                                                                       
NG75                           74216                                                                       
#misassemblies                 2                                                                           
#local misassemblies           4                                                                           
#unaligned contigs             0 + 0 part                                                                  
Unaligned contigs length       0                                                                           
Genome fraction (%)            98.329                                                                      
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        1.95                                                                        
#indels per 100 kbp            5.60                                                                        
#genes                         2673 + 25 part                                                              
#predicted genes (unique)      2676                                                                        
#predicted genes (>= 0 bp)     2677                                                                        
#predicted genes (>= 300 bp)   2315                                                                        
#predicted genes (>= 1500 bp)  288                                                                         
#predicted genes (>= 3000 bp)  28                                                                          
Largest alignment              297663                                                                      
NA50                           179310                                                                      
NGA50                          179310                                                                      
NA75                           74216                                                                       
NGA75                          74216                                                                       
