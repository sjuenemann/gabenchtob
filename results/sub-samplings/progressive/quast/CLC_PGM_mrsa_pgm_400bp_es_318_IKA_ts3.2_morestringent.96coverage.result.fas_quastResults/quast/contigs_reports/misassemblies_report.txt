All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.96coverage.result
#Mis_misassemblies               3                                                                      
#Mis_relocations                 3                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        3                                                                      
Mis_Misassembled contigs length  90653                                                                  
#Mis_local misassemblies         3                                                                      
#mismatches                      77                                                                     
#indels                          294                                                                    
#Mis_short indels (<= 5 bp)      294                                                                    
#Mis_long indels (> 5 bp)        0                                                                      
Indels length                    306                                                                    
