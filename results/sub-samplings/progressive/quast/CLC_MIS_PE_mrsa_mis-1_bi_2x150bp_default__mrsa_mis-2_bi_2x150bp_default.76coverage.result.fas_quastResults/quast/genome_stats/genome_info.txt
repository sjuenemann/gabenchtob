reference chromosomes:
	gi_77102894_ref_NC_006629.2__Staphylococcus_aureus_subsp._aureus_COL_plasmid_pT181__complete_sequence (4440 bp)
	gi_57650036_ref_NC_002951.2__Staphylococcus_aureus_subsp._aureus_COL_chromosome__complete_genome (2809422 bp)

total genome size: 2813862

gap min size: 50
partial gene/operon min size: 100

genes loaded: 2726


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  CLC_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.76coverage.result_broken  | 98.3088367518       | 1.00022882757     | 37          | 2675      | 24        | None      | None      |
  CLC_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.76coverage.result  | 98.4857821741       | 1.0003283712      | 38          | 2678      | 24        | None      | None      |
