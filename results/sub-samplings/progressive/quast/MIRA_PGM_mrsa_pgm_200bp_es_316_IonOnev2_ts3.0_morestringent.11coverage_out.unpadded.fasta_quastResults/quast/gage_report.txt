All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.11coverage_out.unpadded
GAGE_Contigs #                   798                                                                                
GAGE_Min contig                  201                                                                                
GAGE_Max contig                  29676                                                                              
GAGE_N50                         7123 COUNT: 117                                                                    
GAGE_Genome size                 2813862                                                                            
GAGE_Assembly size               2863930                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     9755(0.35%)                                                                        
GAGE_Missing assembly bases      5972(0.21%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  46170                                                                              
GAGE_Compressed reference bases  34352                                                                              
GAGE_Bad trim                    5949                                                                               
GAGE_Avg idy                     99.93                                                                              
GAGE_SNPs                        158                                                                                
GAGE_Indels < 5bp                1163                                                                               
GAGE_Indels >= 5                 9                                                                                  
GAGE_Inversions                  26                                                                                 
GAGE_Relocation                  6                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          716                                                                                
GAGE_Corrected assembly size     2818904                                                                            
GAGE_Min correct contig          204                                                                                
GAGE_Max correct contig          29670                                                                              
GAGE_Corrected N50               6580 COUNT: 126                                                                    
