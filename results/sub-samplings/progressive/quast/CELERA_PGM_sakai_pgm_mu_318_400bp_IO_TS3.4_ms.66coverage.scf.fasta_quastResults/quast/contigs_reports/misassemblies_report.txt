All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.66coverage.scf
#Mis_misassemblies               4                                                           
#Mis_relocations                 4                                                           
#Mis_translocations              0                                                           
#Mis_inversions                  0                                                           
#Mis_misassembled contigs        4                                                           
Mis_Misassembled contigs length  362708                                                      
#Mis_local misassemblies         5                                                           
#mismatches                      201                                                         
#indels                          434                                                         
#Mis_short indels (<= 5 bp)      434                                                         
#Mis_long indels (> 5 bp)        0                                                           
Indels length                    438                                                         
