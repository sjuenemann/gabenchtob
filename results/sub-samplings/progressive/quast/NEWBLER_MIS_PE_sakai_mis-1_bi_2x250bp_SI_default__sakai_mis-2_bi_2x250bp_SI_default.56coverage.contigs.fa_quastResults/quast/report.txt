All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.56coverage.contigs
#Contigs (>= 0 bp)             396                                                                                                   
#Contigs (>= 1000 bp)          147                                                                                                   
Total length (>= 0 bp)         5349204                                                                                               
Total length (>= 1000 bp)      5257815                                                                                               
#Contigs                       326                                                                                                   
Largest contig                 260205                                                                                                
Total length                   5339271                                                                                               
Reference length               5594470                                                                                               
GC (%)                         50.27                                                                                                 
Reference GC (%)               50.48                                                                                                 
N50                            124152                                                                                                
NG50                           107802                                                                                                
N75                            59926                                                                                                 
NG75                           44859                                                                                                 
#misassemblies                 4                                                                                                     
#local misassemblies           3                                                                                                     
#unaligned contigs             4 + 0 part                                                                                            
Unaligned contigs length       6469                                                                                                  
Genome fraction (%)            93.743                                                                                                
Duplication ratio              1.000                                                                                                 
#N's per 100 kbp               0.00                                                                                                  
#mismatches per 100 kbp        2.00                                                                                                  
#indels per 100 kbp            0.42                                                                                                  
#genes                         4939 + 162 part                                                                                       
#predicted genes (unique)      5271                                                                                                  
#predicted genes (>= 0 bp)     5274                                                                                                  
#predicted genes (>= 300 bp)   4435                                                                                                  
#predicted genes (>= 1500 bp)  662                                                                                                   
#predicted genes (>= 3000 bp)  68                                                                                                    
Largest alignment              260205                                                                                                
NA50                           124152                                                                                                
NGA50                          107802                                                                                                
NA75                           59926                                                                                                 
NGA75                          44859                                                                                                 
