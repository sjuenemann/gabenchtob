All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.91coverage.result
#Mis_misassemblies               13                                                                          
#Mis_relocations                 3                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  10                                                                          
#Mis_misassembled contigs        13                                                                          
Mis_Misassembled contigs length  23717                                                                       
#Mis_local misassemblies         3                                                                           
#mismatches                      78                                                                          
#indels                          332                                                                         
#Mis_short indels (<= 5 bp)      332                                                                         
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    347                                                                         
