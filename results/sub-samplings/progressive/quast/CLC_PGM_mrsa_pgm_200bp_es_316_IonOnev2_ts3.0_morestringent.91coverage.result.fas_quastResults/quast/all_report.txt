All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.91coverage.result
#Contigs                                   276                                                                         
#Contigs (>= 0 bp)                         276                                                                         
#Contigs (>= 1000 bp)                      137                                                                         
Largest contig                             87193                                                                       
Total length                               2780817                                                                     
Total length (>= 0 bp)                     2780817                                                                     
Total length (>= 1000 bp)                  2741995                                                                     
Reference length                           2813862                                                                     
N50                                        33100                                                                       
NG50                                       32513                                                                       
N75                                        19195                                                                       
NG75                                       18726                                                                       
L50                                        27                                                                          
LG50                                       28                                                                          
L75                                        53                                                                          
LG75                                       55                                                                          
#local misassemblies                       3                                                                           
#misassemblies                             13                                                                          
#misassembled contigs                      13                                                                          
Misassembled contigs length                23717                                                                       
Misassemblies inter-contig overlap         741                                                                         
#unaligned contigs                         4 + 13 part                                                                 
Unaligned contigs length                   1856                                                                        
#ambiguously mapped contigs                8                                                                           
Extra bases in ambiguously mapped contigs  -6992                                                                       
#N's                                       316                                                                         
#N's per 100 kbp                           11.36                                                                       
Genome fraction (%)                        97.747                                                                      
Duplication ratio                          1.008                                                                       
#genes                                     2604 + 84 part                                                              
#predicted genes (unique)                  2847                                                                        
#predicted genes (>= 0 bp)                 2848                                                                        
#predicted genes (>= 300 bp)               2340                                                                        
#predicted genes (>= 1500 bp)              279                                                                         
#predicted genes (>= 3000 bp)              25                                                                          
#mismatches                                78                                                                          
#indels                                    332                                                                         
Indels length                              347                                                                         
#mismatches per 100 kbp                    2.84                                                                        
#indels per 100 kbp                        12.07                                                                       
GC (%)                                     32.62                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.238                                                                      
Largest alignment                          87193                                                                       
NA50                                       33100                                                                       
NGA50                                      32513                                                                       
NA75                                       19195                                                                       
NGA75                                      18726                                                                       
LA50                                       27                                                                          
LGA50                                      28                                                                          
LA75                                       53                                                                          
LGA75                                      55                                                                          
#Mis_misassemblies                         13                                                                          
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            10                                                                          
#Mis_misassembled contigs                  13                                                                          
Mis_Misassembled contigs length            23717                                                                       
#Mis_local misassemblies                   3                                                                           
#Mis_short indels (<= 5 bp)                332                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               4                                                                           
Una_Fully unaligned length                 1142                                                                        
#Una_partially unaligned contigs           13                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             714                                                                         
GAGE_Contigs #                             276                                                                         
GAGE_Min contig                            200                                                                         
GAGE_Max contig                            87193                                                                       
GAGE_N50                                   32513 COUNT: 28                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2780817                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               25154(0.89%)                                                                
GAGE_Missing assembly bases                2220(0.08%)                                                                 
GAGE_Missing assembly contigs              5(1.81%)                                                                    
GAGE_Duplicated reference bases            19917                                                                       
GAGE_Compressed reference bases            35489                                                                       
GAGE_Bad trim                              796                                                                         
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  36                                                                          
GAGE_Indels < 5bp                          336                                                                         
GAGE_Indels >= 5                           5                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            2                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    192                                                                         
GAGE_Corrected assembly size               2759646                                                                     
GAGE_Min correct contig                    202                                                                         
GAGE_Max correct contig                    87196                                                                       
GAGE_Corrected N50                         32519 COUNT: 28                                                             
