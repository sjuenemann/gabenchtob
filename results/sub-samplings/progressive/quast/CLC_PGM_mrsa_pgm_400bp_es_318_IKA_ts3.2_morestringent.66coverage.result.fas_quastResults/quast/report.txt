All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.66coverage.result
#Contigs (>= 0 bp)             503                                                                    
#Contigs (>= 1000 bp)          165                                                                    
Total length (>= 0 bp)         2832529                                                                
Total length (>= 1000 bp)      2721708                                                                
#Contigs                       503                                                                    
Largest contig                 85822                                                                  
Total length                   2832529                                                                
Reference length               2813862                                                                
GC (%)                         32.60                                                                  
Reference GC (%)               32.81                                                                  
N50                            26025                                                                  
NG50                           26896                                                                  
N75                            15545                                                                  
NG75                           15704                                                                  
#misassemblies                 2                                                                      
#local misassemblies           1                                                                      
#unaligned contigs             9 + 5 part                                                             
Unaligned contigs length       2829                                                                   
Genome fraction (%)            97.594                                                                 
Duplication ratio              1.027                                                                  
#N's per 100 kbp               7.63                                                                   
#mismatches per 100 kbp        2.37                                                                   
#indels per 100 kbp            12.64                                                                  
#genes                         2563 + 121 part                                                        
#predicted genes (unique)      3019                                                                   
#predicted genes (>= 0 bp)     3023                                                                   
#predicted genes (>= 300 bp)   2358                                                                   
#predicted genes (>= 1500 bp)  277                                                                    
#predicted genes (>= 3000 bp)  24                                                                     
Largest alignment              85822                                                                  
NA50                           26025                                                                  
NGA50                          26896                                                                  
NA75                           15545                                                                  
NGA75                          15704                                                                  
