All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.51coverage_out.unpadded
#Contigs                                   355                                                                                
#Contigs (>= 0 bp)                         374                                                                                
#Contigs (>= 1000 bp)                      77                                                                                 
Largest contig                             214029                                                                             
Total length                               2920212                                                                            
Total length (>= 0 bp)                     2922964                                                                            
Total length (>= 1000 bp)                  2815099                                                                            
Reference length                           2813862                                                                            
N50                                        94764                                                                              
NG50                                       95446                                                                              
N75                                        46377                                                                              
NG75                                       55872                                                                              
L50                                        10                                                                                 
LG50                                       9                                                                                  
L75                                        21                                                                                 
LG75                                       19                                                                                 
#local misassemblies                       6                                                                                  
#misassemblies                             68                                                                                 
#misassembled contigs                      58                                                                                 
Misassembled contigs length                426946                                                                             
Misassemblies inter-contig overlap         4933                                                                               
#unaligned contigs                         3 + 115 part                                                                       
Unaligned contigs length                   7080                                                                               
#ambiguously mapped contigs                17                                                                                 
Extra bases in ambiguously mapped contigs  -8227                                                                              
#N's                                       264                                                                                
#N's per 100 kbp                           9.04                                                                               
Genome fraction (%)                        99.660                                                                             
Duplication ratio                          1.038                                                                              
#genes                                     2686 + 39 part                                                                     
#predicted genes (unique)                  2941                                                                               
#predicted genes (>= 0 bp)                 2948                                                                               
#predicted genes (>= 300 bp)               2353                                                                               
#predicted genes (>= 1500 bp)              291                                                                                
#predicted genes (>= 3000 bp)              29                                                                                 
#mismatches                                111                                                                                
#indels                                    180                                                                                
Indels length                              246                                                                                
#mismatches per 100 kbp                    3.96                                                                               
#indels per 100 kbp                        6.42                                                                               
GC (%)                                     32.73                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               98.587                                                                             
Largest alignment                          214029                                                                             
NA50                                       80269                                                                              
NGA50                                      95439                                                                              
NA75                                       43487                                                                              
NGA75                                      50873                                                                              
LA50                                       10                                                                                 
LGA50                                      9                                                                                  
LA75                                       22                                                                                 
LGA75                                      20                                                                                 
#Mis_misassemblies                         68                                                                                 
#Mis_relocations                           24                                                                                 
#Mis_translocations                        2                                                                                  
#Mis_inversions                            42                                                                                 
#Mis_misassembled contigs                  58                                                                                 
Mis_Misassembled contigs length            426946                                                                             
#Mis_local misassemblies                   6                                                                                  
#Mis_short indels (<= 5 bp)                178                                                                                
#Mis_long indels (> 5 bp)                  2                                                                                  
#Una_fully unaligned contigs               3                                                                                  
Una_Fully unaligned length                 1229                                                                               
#Una_partially unaligned contigs           115                                                                                
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             5851                                                                               
GAGE_Contigs #                             355                                                                                
GAGE_Min contig                            200                                                                                
GAGE_Max contig                            214029                                                                             
GAGE_N50                                   95446 COUNT: 9                                                                     
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2920212                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               631(0.02%)                                                                         
GAGE_Missing assembly bases                8289(0.28%)                                                                        
GAGE_Missing assembly contigs              2(0.56%)                                                                           
GAGE_Duplicated reference bases            103129                                                                             
GAGE_Compressed reference bases            17674                                                                              
GAGE_Bad trim                              7622                                                                               
GAGE_Avg idy                               99.98                                                                              
GAGE_SNPs                                  71                                                                                 
GAGE_Indels < 5bp                          154                                                                                
GAGE_Indels >= 5                           3                                                                                  
GAGE_Inversions                            3                                                                                  
GAGE_Relocation                            2                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    102                                                                                
GAGE_Corrected assembly size               2817728                                                                            
GAGE_Min correct contig                    200                                                                                
GAGE_Max correct contig                    200795                                                                             
GAGE_Corrected N50                         75785 COUNT: 11                                                                    
