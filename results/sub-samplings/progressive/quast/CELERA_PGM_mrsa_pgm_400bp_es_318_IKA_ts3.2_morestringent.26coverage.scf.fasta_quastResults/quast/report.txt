All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.26coverage.scf
#Contigs (>= 0 bp)             68                                                                     
#Contigs (>= 1000 bp)          68                                                                     
Total length (>= 0 bp)         2739385                                                                
Total length (>= 1000 bp)      2739385                                                                
#Contigs                       68                                                                     
Largest contig                 297833                                                                 
Total length                   2739385                                                                
Reference length               2813862                                                                
GC (%)                         32.62                                                                  
Reference GC (%)               32.81                                                                  
N50                            73688                                                                  
NG50                           70018                                                                  
N75                            37232                                                                  
NG75                           32568                                                                  
#misassemblies                 1                                                                      
#local misassemblies           3                                                                      
#unaligned contigs             0 + 1 part                                                             
Unaligned contigs length       56                                                                     
Genome fraction (%)            97.043                                                                 
Duplication ratio              1.003                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        3.11                                                                   
#indels per 100 kbp            15.93                                                                  
#genes                         2615 + 44 part                                                         
#predicted genes (unique)      2723                                                                   
#predicted genes (>= 0 bp)     2725                                                                   
#predicted genes (>= 300 bp)   2319                                                                   
#predicted genes (>= 1500 bp)  280                                                                    
#predicted genes (>= 3000 bp)  26                                                                     
Largest alignment              297829                                                                 
NA50                           73688                                                                  
NGA50                          70018                                                                  
NA75                           37232                                                                  
NGA75                          32564                                                                  
