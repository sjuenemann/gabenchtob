All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.81coverage_out.unpadded
#Contigs                                   319                                                                                               
#Contigs (>= 0 bp)                         335                                                                                               
#Contigs (>= 1000 bp)                      101                                                                                               
Largest contig                             407868                                                                                            
Total length                               5632905                                                                                           
Total length (>= 0 bp)                     5635462                                                                                           
Total length (>= 1000 bp)                  5534765                                                                                           
Reference length                           5594470                                                                                           
N50                                        166610                                                                                            
NG50                                       166610                                                                                            
N75                                        91840                                                                                             
NG75                                       97389                                                                                             
L50                                        10                                                                                                
LG50                                       10                                                                                                
L75                                        21                                                                                                
LG75                                       20                                                                                                
#local misassemblies                       7                                                                                                 
#misassemblies                             72                                                                                                
#misassembled contigs                      38                                                                                                
Misassembled contigs length                3613031                                                                                           
Misassemblies inter-contig overlap         81034                                                                                             
#unaligned contigs                         3 + 5 part                                                                                        
Unaligned contigs length                   6401                                                                                              
#ambiguously mapped contigs                105                                                                                               
Extra bases in ambiguously mapped contigs  -64156                                                                                            
#N's                                       125                                                                                               
#N's per 100 kbp                           2.22                                                                                              
Genome fraction (%)                        98.718                                                                                            
Duplication ratio                          1.022                                                                                             
#genes                                     5316 + 84 part                                                                                    
#predicted genes (unique)                  5530                                                                                              
#predicted genes (>= 0 bp)                 5585                                                                                              
#predicted genes (>= 300 bp)               4695                                                                                              
#predicted genes (>= 1500 bp)              695                                                                                               
#predicted genes (>= 3000 bp)              77                                                                                                
#mismatches                                2577                                                                                              
#indels                                    132                                                                                               
Indels length                              166                                                                                               
#mismatches per 100 kbp                    46.66                                                                                             
#indels per 100 kbp                        2.39                                                                                              
GC (%)                                     50.48                                                                                             
Reference GC (%)                           50.48                                                                                             
Average %IDY                               98.520                                                                                            
Largest alignment                          346647                                                                                            
NA50                                       141043                                                                                            
NGA50                                      141043                                                                                            
NA75                                       65791                                                                                             
NGA75                                      65791                                                                                             
LA50                                       14                                                                                                
LGA50                                      14                                                                                                
LA75                                       28                                                                                                
LGA75                                      28                                                                                                
#Mis_misassemblies                         72                                                                                                
#Mis_relocations                           68                                                                                                
#Mis_translocations                        4                                                                                                 
#Mis_inversions                            0                                                                                                 
#Mis_misassembled contigs                  38                                                                                                
Mis_Misassembled contigs length            3613031                                                                                           
#Mis_local misassemblies                   7                                                                                                 
#Mis_short indels (<= 5 bp)                130                                                                                               
#Mis_long indels (> 5 bp)                  2                                                                                                 
#Una_fully unaligned contigs               3                                                                                                 
Una_Fully unaligned length                 6133                                                                                              
#Una_partially unaligned contigs           5                                                                                                 
#Una_with misassembly                      0                                                                                                 
#Una_both parts are significant            0                                                                                                 
Una_Partially unaligned length             268                                                                                               
GAGE_Contigs #                             319                                                                                               
GAGE_Min contig                            211                                                                                               
GAGE_Max contig                            407868                                                                                            
GAGE_N50                                   166610 COUNT: 10                                                                                  
GAGE_Genome size                           5594470                                                                                           
GAGE_Assembly size                         5632905                                                                                           
GAGE_Chaff bases                           0                                                                                                 
GAGE_Missing reference bases               998(0.02%)                                                                                        
GAGE_Missing assembly bases                6791(0.12%)                                                                                       
GAGE_Missing assembly contigs              3(0.94%)                                                                                          
GAGE_Duplicated reference bases            153801                                                                                            
GAGE_Compressed reference bases            140553                                                                                            
GAGE_Bad trim                              649                                                                                               
GAGE_Avg idy                               99.94                                                                                             
GAGE_SNPs                                  628                                                                                               
GAGE_Indels < 5bp                          55                                                                                                
GAGE_Indels >= 5                           4                                                                                                 
GAGE_Inversions                            22                                                                                                
GAGE_Relocation                            12                                                                                                
GAGE_Translocation                         5                                                                                                 
GAGE_Corrected contig #                    163                                                                                               
GAGE_Corrected assembly size               5573248                                                                                           
GAGE_Min correct contig                    231                                                                                               
GAGE_Max correct contig                    346647                                                                                            
GAGE_Corrected N50                         141044 COUNT: 14                                                                                  
