All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.11coverage_out.unpadded
#Mis_misassemblies               22                                                                                              
#Mis_relocations                 22                                                                                              
#Mis_translocations              0                                                                                               
#Mis_inversions                  0                                                                                               
#Mis_misassembled contigs        20                                                                                              
Mis_Misassembled contigs length  63247                                                                                           
#Mis_local misassemblies         4                                                                                               
#mismatches                      446                                                                                             
#indels                          27                                                                                              
#Mis_short indels (<= 5 bp)      24                                                                                              
#Mis_long indels (> 5 bp)        3                                                                                               
Indels length                    64                                                                                              
