All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.91coverage.contigs
#Mis_misassemblies               3                                                                
#Mis_relocations                 3                                                                
#Mis_translocations              0                                                                
#Mis_inversions                  0                                                                
#Mis_misassembled contigs        3                                                                
Mis_Misassembled contigs length  312730                                                           
#Mis_local misassemblies         8                                                                
#mismatches                      118                                                              
#indels                          180                                                              
#Mis_short indels (<= 5 bp)      180                                                              
#Mis_long indels (> 5 bp)        0                                                                
Indels length                    188                                                              
