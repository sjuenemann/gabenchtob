All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.96coverage.K99.final-contigs
#Contigs                                   446                                                                                  
#Contigs (>= 0 bp)                         759                                                                                  
#Contigs (>= 1000 bp)                      176                                                                                  
Largest contig                             374860                                                                               
Total length                               5382228                                                                              
Total length (>= 0 bp)                     5426143                                                                              
Total length (>= 1000 bp)                  5259228                                                                              
Reference length                           5594470                                                                              
N50                                        124322                                                                               
NG50                                       121781                                                                               
N75                                        44314                                                                                
NG75                                       41178                                                                                
L50                                        15                                                                                   
LG50                                       16                                                                                   
L75                                        32                                                                                   
LG75                                       35                                                                                   
#local misassemblies                       6                                                                                    
#misassemblies                             3                                                                                    
#misassembled contigs                      3                                                                                    
Misassembled contigs length                418314                                                                               
Misassemblies inter-contig overlap         1157                                                                                 
#unaligned contigs                         0 + 0 part                                                                           
Unaligned contigs length                   0                                                                                    
#ambiguously mapped contigs                149                                                                                  
Extra bases in ambiguously mapped contigs  -93980                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.417                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     4939 + 203 part                                                                      
#predicted genes (unique)                  5966                                                                                 
#predicted genes (>= 0 bp)                 5982                                                                                 
#predicted genes (>= 300 bp)               4796                                                                                 
#predicted genes (>= 1500 bp)              546                                                                                  
#predicted genes (>= 3000 bp)              40                                                                                   
#mismatches                                121                                                                                  
#indels                                    1366                                                                                 
Indels length                              1419                                                                                 
#mismatches per 100 kbp                    2.29                                                                                 
#indels per 100 kbp                        25.86                                                                                
GC (%)                                     50.30                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               98.847                                                                               
Largest alignment                          247867                                                                               
NA50                                       124322                                                                               
NGA50                                      121781                                                                               
NA75                                       44314                                                                                
NGA75                                      41178                                                                                
LA50                                       16                                                                                   
LGA50                                      17                                                                                   
LA75                                       33                                                                                   
LGA75                                      36                                                                                   
#Mis_misassemblies                         3                                                                                    
#Mis_relocations                           3                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  3                                                                                    
Mis_Misassembled contigs length            418314                                                                               
#Mis_local misassemblies                   6                                                                                    
#Mis_short indels (<= 5 bp)                1365                                                                                 
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           0                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             0                                                                                    
GAGE_Contigs #                             446                                                                                  
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            374860                                                                               
GAGE_N50                                   121781 COUNT: 16                                                                     
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5382228                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               9675(0.17%)                                                                          
GAGE_Missing assembly bases                2(0.00%)                                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            1231                                                                                 
GAGE_Compressed reference bases            271423                                                                               
GAGE_Bad trim                              2                                                                                    
GAGE_Avg idy                               99.97                                                                                
GAGE_SNPs                                  157                                                                                  
GAGE_Indels < 5bp                          1424                                                                                 
GAGE_Indels >= 5                           8                                                                                    
GAGE_Inversions                            0                                                                                    
GAGE_Relocation                            5                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    455                                                                                  
GAGE_Corrected assembly size               5384192                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    226975                                                                               
GAGE_Corrected N50                         116683 COUNT: 18                                                                     
