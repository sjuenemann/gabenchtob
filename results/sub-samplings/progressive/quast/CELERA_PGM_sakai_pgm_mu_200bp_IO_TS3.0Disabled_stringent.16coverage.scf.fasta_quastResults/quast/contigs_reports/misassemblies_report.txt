All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.16coverage.scf
#Mis_misassemblies               4                                                                      
#Mis_relocations                 3                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  1                                                                      
#Mis_misassembled contigs        4                                                                      
Mis_Misassembled contigs length  85625                                                                  
#Mis_local misassemblies         4                                                                      
#mismatches                      124                                                                    
#indels                          1771                                                                   
#Mis_short indels (<= 5 bp)      1771                                                                   
#Mis_long indels (> 5 bp)        0                                                                      
Indels length                    1814                                                                   
