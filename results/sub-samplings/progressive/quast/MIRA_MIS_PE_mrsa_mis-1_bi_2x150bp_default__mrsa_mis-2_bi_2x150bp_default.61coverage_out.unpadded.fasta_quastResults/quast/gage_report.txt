All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.61coverage_out.unpadded
GAGE_Contigs #                   185                                                                                             
GAGE_Min contig                  201                                                                                             
GAGE_Max contig                  534843                                                                                          
GAGE_N50                         155442 COUNT: 6                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2881009                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     1211(0.04%)                                                                                     
GAGE_Missing assembly bases      6231(0.22%)                                                                                     
GAGE_Missing assembly contigs    3(1.62%)                                                                                        
GAGE_Duplicated reference bases  67299                                                                                           
GAGE_Compressed reference bases  8002                                                                                            
GAGE_Bad trim                    57                                                                                              
GAGE_Avg idy                     99.99                                                                                           
GAGE_SNPs                        58                                                                                              
GAGE_Indels < 5bp                9                                                                                               
GAGE_Indels >= 5                 3                                                                                               
GAGE_Inversions                  3                                                                                               
GAGE_Relocation                  4                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          61                                                                                              
GAGE_Corrected assembly size     2823403                                                                                         
GAGE_Min correct contig          208                                                                                             
GAGE_Max correct contig          534842                                                                                          
GAGE_Corrected N50               151010 COUNT: 6                                                                                 
