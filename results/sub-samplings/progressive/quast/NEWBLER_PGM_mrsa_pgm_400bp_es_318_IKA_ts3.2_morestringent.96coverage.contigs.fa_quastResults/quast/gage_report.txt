All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.96coverage.contigs
GAGE_Contigs #                   60                                                                          
GAGE_Min contig                  201                                                                         
GAGE_Max contig                  369030                                                                      
GAGE_N50                         156773 COUNT: 6                                                             
GAGE_Genome size                 2813862                                                                     
GAGE_Assembly size               2776584                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     5158(0.18%)                                                                 
GAGE_Missing assembly bases      2(0.00%)                                                                    
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  1176                                                                        
GAGE_Compressed reference bases  34720                                                                       
GAGE_Bad trim                    2                                                                           
GAGE_Avg idy                     99.99                                                                       
GAGE_SNPs                        42                                                                          
GAGE_Indels < 5bp                88                                                                          
GAGE_Indels >= 5                 4                                                                           
GAGE_Inversions                  0                                                                           
GAGE_Relocation                  1                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          61                                                                          
GAGE_Corrected assembly size     2776304                                                                     
GAGE_Min correct contig          203                                                                         
GAGE_Max correct contig          369039                                                                      
GAGE_Corrected N50               110544 COUNT: 6                                                             
