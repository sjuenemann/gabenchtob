All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.61coverage.scf
#Mis_misassemblies               6                                                                           
#Mis_relocations                 4                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  2                                                                           
#Mis_misassembled contigs        4                                                                           
Mis_Misassembled contigs length  15713                                                                       
#Mis_local misassemblies         7                                                                           
#mismatches                      129                                                                         
#indels                          510                                                                         
#Mis_short indels (<= 5 bp)      510                                                                         
#Mis_long indels (> 5 bp)        0                                                                           
Indels length                    540                                                                         
