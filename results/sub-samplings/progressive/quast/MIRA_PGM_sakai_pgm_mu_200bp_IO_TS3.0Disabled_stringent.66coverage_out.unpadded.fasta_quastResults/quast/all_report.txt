All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.66coverage_out.unpadded
#Contigs                                   880                                                                           
#Contigs (>= 0 bp)                         901                                                                           
#Contigs (>= 1000 bp)                      196                                                                           
Largest contig                             220479                                                                        
Total length                               5749736                                                                       
Total length (>= 0 bp)                     5753383                                                                       
Total length (>= 1000 bp)                  5491584                                                                       
Reference length                           5594470                                                                       
N50                                        82016                                                                         
NG50                                       83483                                                                         
N75                                        40082                                                                         
NG75                                       41559                                                                         
L50                                        24                                                                            
LG50                                       23                                                                            
L75                                        49                                                                            
LG75                                       46                                                                            
#local misassemblies                       23                                                                            
#misassemblies                             147                                                                           
#misassembled contigs                      121                                                                           
Misassembled contigs length                1295346                                                                       
Misassemblies inter-contig overlap         41059                                                                         
#unaligned contigs                         12 + 360 part                                                                 
Unaligned contigs length                   22991                                                                         
#ambiguously mapped contigs                83                                                                            
Extra bases in ambiguously mapped contigs  -66873                                                                        
#N's                                       781                                                                           
#N's per 100 kbp                           13.58                                                                         
Genome fraction (%)                        97.686                                                                        
Duplication ratio                          1.043                                                                         
#genes                                     5210 + 156 part                                                               
#predicted genes (unique)                  6146                                                                          
#predicted genes (>= 0 bp)                 6185                                                                          
#predicted genes (>= 300 bp)               4794                                                                          
#predicted genes (>= 1500 bp)              664                                                                           
#predicted genes (>= 3000 bp)              66                                                                            
#mismatches                                1772                                                                          
#indels                                    425                                                                           
Indels length                              469                                                                           
#mismatches per 100 kbp                    32.43                                                                         
#indels per 100 kbp                        7.78                                                                          
GC (%)                                     50.42                                                                         
Reference GC (%)                           50.48                                                                         
Average %IDY                               98.633                                                                        
Largest alignment                          209657                                                                        
NA50                                       75736                                                                         
NGA50                                      79591                                                                         
NA75                                       32727                                                                         
NGA75                                      36721                                                                         
LA50                                       26                                                                            
LGA50                                      25                                                                            
LA75                                       55                                                                            
LGA75                                      51                                                                            
#Mis_misassemblies                         147                                                                           
#Mis_relocations                           43                                                                            
#Mis_translocations                        1                                                                             
#Mis_inversions                            103                                                                           
#Mis_misassembled contigs                  121                                                                           
Mis_Misassembled contigs length            1295346                                                                       
#Mis_local misassemblies                   23                                                                            
#Mis_short indels (<= 5 bp)                423                                                                           
#Mis_long indels (> 5 bp)                  2                                                                             
#Una_fully unaligned contigs               12                                                                            
Una_Fully unaligned length                 5134                                                                          
#Una_partially unaligned contigs           360                                                                           
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            3                                                                             
Una_Partially unaligned length             17857                                                                         
GAGE_Contigs #                             880                                                                           
GAGE_Min contig                            204                                                                           
GAGE_Max contig                            220479                                                                        
GAGE_N50                                   83483 COUNT: 23                                                               
GAGE_Genome size                           5594470                                                                       
GAGE_Assembly size                         5749736                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               367(0.01%)                                                                    
GAGE_Missing assembly bases                23500(0.41%)                                                                  
GAGE_Missing assembly contigs              6(0.68%)                                                                      
GAGE_Duplicated reference bases            244331                                                                        
GAGE_Compressed reference bases            168185                                                                        
GAGE_Bad trim                              21121                                                                         
GAGE_Avg idy                               99.95                                                                         
GAGE_SNPs                                  464                                                                           
GAGE_Indels < 5bp                          331                                                                           
GAGE_Indels >= 5                           6                                                                             
GAGE_Inversions                            10                                                                            
GAGE_Relocation                            9                                                                             
GAGE_Translocation                         1                                                                             
GAGE_Corrected contig #                    268                                                                           
GAGE_Corrected assembly size               5531878                                                                       
GAGE_Min correct contig                    204                                                                           
GAGE_Max correct contig                    209658                                                                        
GAGE_Corrected N50                         71653 COUNT: 26                                                               
