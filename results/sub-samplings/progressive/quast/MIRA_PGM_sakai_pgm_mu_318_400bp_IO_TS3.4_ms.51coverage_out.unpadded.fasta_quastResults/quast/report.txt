All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.51coverage_out.unpadded
#Contigs (>= 0 bp)             498                                                                
#Contigs (>= 1000 bp)          170                                                                
Total length (>= 0 bp)         5709633                                                            
Total length (>= 1000 bp)      5555596                                                            
#Contigs                       475                                                                
Largest contig                 482344                                                             
Total length                   5705906                                                            
Reference length               5594470                                                            
GC (%)                         50.39                                                              
Reference GC (%)               50.48                                                              
N50                            162725                                                             
NG50                           171256                                                             
N75                            81835                                                              
NG75                           84586                                                              
#misassemblies                 39                                                                 
#local misassemblies           18                                                                 
#unaligned contigs             1 + 2 part                                                         
Unaligned contigs length       377                                                                
Genome fraction (%)            97.926                                                             
Duplication ratio              1.038                                                              
#N's per 100 kbp               0.23                                                               
#mismatches per 100 kbp        22.93                                                              
#indels per 100 kbp            21.47                                                              
#genes                         5262 + 116 part                                                    
#predicted genes (unique)      5984                                                               
#predicted genes (>= 0 bp)     6039                                                               
#predicted genes (>= 300 bp)   4877                                                               
#predicted genes (>= 1500 bp)  638                                                                
#predicted genes (>= 3000 bp)  57                                                                 
Largest alignment              379459                                                             
NA50                           138290                                                             
NGA50                          148168                                                             
NA75                           74523                                                              
NGA75                          76836                                                              
