All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.16coverage.result
#Contigs (>= 0 bp)             1119                                                                   
#Contigs (>= 1000 bp)          498                                                                    
Total length (>= 0 bp)         5328463                                                                
Total length (>= 1000 bp)      5089620                                                                
#Contigs                       1119                                                                   
Largest contig                 68702                                                                  
Total length                   5328463                                                                
Reference length               5594470                                                                
GC (%)                         50.26                                                                  
Reference GC (%)               50.48                                                                  
N50                            17133                                                                  
NG50                           15762                                                                  
N75                            8406                                                                   
NG75                           7114                                                                   
#misassemblies                 29                                                                     
#local misassemblies           7                                                                      
#unaligned contigs             2 + 33 part                                                            
Unaligned contigs length       3400                                                                   
Genome fraction (%)            92.897                                                                 
Duplication ratio              1.008                                                                  
#N's per 100 kbp               4.34                                                                   
#mismatches per 100 kbp        4.14                                                                   
#indels per 100 kbp            44.29                                                                  
#genes                         4540 + 540 part                                                        
#predicted genes (unique)      6732                                                                   
#predicted genes (>= 0 bp)     6734                                                                   
#predicted genes (>= 300 bp)   4980                                                                   
#predicted genes (>= 1500 bp)  434                                                                    
#predicted genes (>= 3000 bp)  23                                                                     
Largest alignment              68702                                                                  
NA50                           16989                                                                  
NGA50                          15662                                                                  
NA75                           8306                                                                   
NGA75                          7056                                                                   
