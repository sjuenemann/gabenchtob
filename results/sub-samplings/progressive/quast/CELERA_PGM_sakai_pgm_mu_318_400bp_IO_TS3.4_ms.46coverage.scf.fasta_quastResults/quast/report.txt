All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.46coverage.scf
#Contigs (>= 0 bp)             218                                                         
#Contigs (>= 1000 bp)          218                                                         
Total length (>= 0 bp)         5235138                                                     
Total length (>= 1000 bp)      5235138                                                     
#Contigs                       218                                                         
Largest contig                 238422                                                      
Total length                   5235138                                                     
Reference length               5594470                                                     
GC (%)                         50.31                                                       
Reference GC (%)               50.48                                                       
N50                            59244                                                       
NG50                           58140                                                       
N75                            36376                                                       
NG75                           30747                                                       
#misassemblies                 4                                                           
#local misassemblies           7                                                           
#unaligned contigs             0 + 0 part                                                  
Unaligned contigs length       0                                                           
Genome fraction (%)            92.888                                                      
Duplication ratio              1.005                                                       
#N's per 100 kbp               0.00                                                        
#mismatches per 100 kbp        2.39                                                        
#indels per 100 kbp            9.58                                                        
#genes                         4888 + 189 part                                             
#predicted genes (unique)      5255                                                        
#predicted genes (>= 0 bp)     5256                                                        
#predicted genes (>= 300 bp)   4460                                                        
#predicted genes (>= 1500 bp)  628                                                         
#predicted genes (>= 3000 bp)  61                                                          
Largest alignment              238422                                                      
NA50                           59244                                                       
NGA50                          58140                                                       
NA75                           36376                                                       
NGA75                          30633                                                       
