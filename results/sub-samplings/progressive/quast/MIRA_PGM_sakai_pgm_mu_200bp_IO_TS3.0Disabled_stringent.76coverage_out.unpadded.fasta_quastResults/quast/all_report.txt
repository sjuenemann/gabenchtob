All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.76coverage_out.unpadded
#Contigs                                   1146                                                                          
#Contigs (>= 0 bp)                         1174                                                                          
#Contigs (>= 1000 bp)                      195                                                                           
Largest contig                             346866                                                                        
Total length                               5843566                                                                       
Total length (>= 0 bp)                     5848356                                                                       
Total length (>= 1000 bp)                  5496033                                                                       
Reference length                           5594470                                                                       
N50                                        68055                                                                         
NG50                                       74477                                                                         
N75                                        32698                                                                         
NG75                                       41874                                                                         
L50                                        23                                                                            
LG50                                       21                                                                            
L75                                        52                                                                            
LG75                                       47                                                                            
#local misassemblies                       34                                                                            
#misassemblies                             180                                                                           
#misassembled contigs                      159                                                                           
Misassembled contigs length                1633442                                                                       
Misassemblies inter-contig overlap         43275                                                                         
#unaligned contigs                         22 + 532 part                                                                 
Unaligned contigs length                   39645                                                                         
#ambiguously mapped contigs                82                                                                            
Extra bases in ambiguously mapped contigs  -59637                                                                        
#N's                                       1199                                                                          
#N's per 100 kbp                           20.52                                                                         
Genome fraction (%)                        97.666                                                                        
Duplication ratio                          1.059                                                                         
#genes                                     5208 + 158 part                                                               
#predicted genes (unique)                  6365                                                                          
#predicted genes (>= 0 bp)                 6401                                                                          
#predicted genes (>= 300 bp)               4826                                                                          
#predicted genes (>= 1500 bp)              675                                                                           
#predicted genes (>= 3000 bp)              66                                                                            
#mismatches                                1556                                                                          
#indels                                    393                                                                           
Indels length                              510                                                                           
#mismatches per 100 kbp                    28.47                                                                         
#indels per 100 kbp                        7.19                                                                          
GC (%)                                     50.41                                                                         
Reference GC (%)                           50.48                                                                         
Average %IDY                               98.660                                                                        
Largest alignment                          337759                                                                        
NA50                                       64588                                                                         
NGA50                                      66993                                                                         
NA75                                       28552                                                                         
NGA75                                      32698                                                                         
LA50                                       25                                                                            
LGA50                                      23                                                                            
LA75                                       58                                                                            
LGA75                                      52                                                                            
#Mis_misassemblies                         180                                                                           
#Mis_relocations                           44                                                                            
#Mis_translocations                        1                                                                             
#Mis_inversions                            135                                                                           
#Mis_misassembled contigs                  159                                                                           
Mis_Misassembled contigs length            1633442                                                                       
#Mis_local misassemblies                   34                                                                            
#Mis_short indels (<= 5 bp)                389                                                                           
#Mis_long indels (> 5 bp)                  4                                                                             
#Una_fully unaligned contigs               22                                                                            
Una_Fully unaligned length                 9956                                                                          
#Una_partially unaligned contigs           532                                                                           
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            3                                                                             
Una_Partially unaligned length             29689                                                                         
GAGE_Contigs #                             1146                                                                          
GAGE_Min contig                            201                                                                           
GAGE_Max contig                            346866                                                                        
GAGE_N50                                   74477 COUNT: 21                                                               
GAGE_Genome size                           5594470                                                                       
GAGE_Assembly size                         5843566                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               767(0.01%)                                                                    
GAGE_Missing assembly bases                35425(0.61%)                                                                  
GAGE_Missing assembly contigs              8(0.70%)                                                                      
GAGE_Duplicated reference bases            333769                                                                        
GAGE_Compressed reference bases            179340                                                                        
GAGE_Bad trim                              32332                                                                         
GAGE_Avg idy                               99.96                                                                         
GAGE_SNPs                                  612                                                                           
GAGE_Indels < 5bp                          306                                                                           
GAGE_Indels >= 5                           9                                                                             
GAGE_Inversions                            14                                                                            
GAGE_Relocation                            14                                                                            
GAGE_Translocation                         1                                                                             
GAGE_Corrected contig #                    271                                                                           
GAGE_Corrected assembly size               5530905                                                                       
GAGE_Min correct contig                    213                                                                           
GAGE_Max correct contig                    336796                                                                        
GAGE_Corrected N50                         64589 COUNT: 24                                                               
