All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.76coverage_out.unpadded
#Mis_misassemblies               84                                                                                                
#Mis_relocations                 79                                                                                                
#Mis_translocations              5                                                                                                 
#Mis_inversions                  0                                                                                                 
#Mis_misassembled contigs        41                                                                                                
Mis_Misassembled contigs length  3311281                                                                                           
#Mis_local misassemblies         12                                                                                                
#mismatches                      2378                                                                                              
#indels                          147                                                                                               
#Mis_short indels (<= 5 bp)      145                                                                                               
#Mis_long indels (> 5 bp)        2                                                                                                 
Indels length                    208                                                                                               
