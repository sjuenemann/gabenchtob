All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.36coverage_out.unpadded
#Mis_misassemblies               14                                                                                              
#Mis_relocations                 14                                                                                              
#Mis_translocations              0                                                                                               
#Mis_inversions                  0                                                                                               
#Mis_misassembled contigs        12                                                                                              
Mis_Misassembled contigs length  666177                                                                                          
#Mis_local misassemblies         6                                                                                               
#mismatches                      250                                                                                             
#indels                          69                                                                                              
#Mis_short indels (<= 5 bp)      57                                                                                              
#Mis_long indels (> 5 bp)        12                                                                                              
Indels length                    274                                                                                             
