All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.61coverage_out.unpadded
#Contigs (>= 0 bp)             598                                                                
#Contigs (>= 1000 bp)          150                                                                
Total length (>= 0 bp)         5768457                                                            
Total length (>= 1000 bp)      5553675                                                            
#Contigs                       567                                                                
Largest contig                 460319                                                             
Total length                   5764243                                                            
Reference length               5594470                                                            
GC (%)                         50.38                                                              
Reference GC (%)               50.48                                                              
N50                            172974                                                             
NG50                           172974                                                             
N75                            87798                                                              
NG75                           92134                                                              
#misassemblies                 46                                                                 
#local misassemblies           25                                                                 
#unaligned contigs             0 + 1 part                                                         
Unaligned contigs length       29                                                                 
Genome fraction (%)            98.163                                                             
Duplication ratio              1.046                                                              
#N's per 100 kbp               0.52                                                               
#mismatches per 100 kbp        25.68                                                              
#indels per 100 kbp            20.74                                                              
#genes                         5279 + 100 part                                                    
#predicted genes (unique)      6077                                                               
#predicted genes (>= 0 bp)     6140                                                               
#predicted genes (>= 300 bp)   4876                                                               
#predicted genes (>= 1500 bp)  651                                                                
#predicted genes (>= 3000 bp)  61                                                                 
Largest alignment              346273                                                             
NA50                           146675                                                             
NGA50                          147896                                                             
NA75                           61842                                                              
NGA75                          79129                                                              
