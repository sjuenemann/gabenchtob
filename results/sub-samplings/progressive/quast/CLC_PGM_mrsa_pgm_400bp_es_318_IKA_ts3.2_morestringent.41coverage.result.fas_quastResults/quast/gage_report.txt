All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.41coverage.result
GAGE_Contigs #                   594                                                                    
GAGE_Min contig                  200                                                                    
GAGE_Max contig                  60165                                                                  
GAGE_N50                         19664 COUNT: 44                                                        
GAGE_Genome size                 2813862                                                                
GAGE_Assembly size               2837419                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     23582(0.84%)                                                           
GAGE_Missing assembly bases      1671(0.06%)                                                            
GAGE_Missing assembly contigs    3(0.51%)                                                               
GAGE_Duplicated reference bases  71960                                                                  
GAGE_Compressed reference bases  41811                                                                  
GAGE_Bad trim                    402                                                                    
GAGE_Avg idy                     99.98                                                                  
GAGE_SNPs                        56                                                                     
GAGE_Indels < 5bp                457                                                                    
GAGE_Indels >= 5                 0                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  2                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          367                                                                    
GAGE_Corrected assembly size     2764187                                                                
GAGE_Min correct contig          200                                                                    
GAGE_Max correct contig          60171                                                                  
GAGE_Corrected N50               19614 COUNT: 45                                                        
