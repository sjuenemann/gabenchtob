All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.86coverage.result
#Mis_misassemblies               5                                                           
#Mis_relocations                 5                                                           
#Mis_translocations              0                                                           
#Mis_inversions                  0                                                           
#Mis_misassembled contigs        5                                                           
Mis_Misassembled contigs length  103069                                                      
#Mis_local misassemblies         5                                                           
#mismatches                      143                                                         
#indels                          542                                                         
#Mis_short indels (<= 5 bp)      542                                                         
#Mis_long indels (> 5 bp)        0                                                           
Indels length                    550                                                         
