All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.96coverage_out.unpadded
GAGE_Contigs #                   256                                                                                             
GAGE_Min contig                  201                                                                                             
GAGE_Max contig                  685969                                                                                          
GAGE_N50                         166439 COUNT: 4                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2921779                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     464(0.02%)                                                                                      
GAGE_Missing assembly bases      6494(0.22%)                                                                                     
GAGE_Missing assembly contigs    2(0.78%)                                                                                        
GAGE_Duplicated reference bases  112799                                                                                          
GAGE_Compressed reference bases  7426                                                                                            
GAGE_Bad trim                    55                                                                                              
GAGE_Avg idy                     99.99                                                                                           
GAGE_SNPs                        47                                                                                              
GAGE_Indels < 5bp                9                                                                                               
GAGE_Indels >= 5                 3                                                                                               
GAGE_Inversions                  8                                                                                               
GAGE_Relocation                  3                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          60                                                                                              
GAGE_Corrected assembly size     2830435                                                                                         
GAGE_Min correct contig          214                                                                                             
GAGE_Max correct contig          359866                                                                                          
GAGE_Corrected N50               131485 COUNT: 6                                                                                 
