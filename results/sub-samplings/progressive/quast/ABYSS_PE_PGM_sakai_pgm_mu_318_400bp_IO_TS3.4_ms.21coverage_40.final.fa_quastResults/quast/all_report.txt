All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.21coverage_40.final
#Contigs                                   1604                                                               
#Contigs (>= 0 bp)                         5504                                                               
#Contigs (>= 1000 bp)                      883                                                                
Largest contig                             52286                                                              
Total length                               5188848                                                            
Total length (>= 0 bp)                     5485591                                                            
Total length (>= 1000 bp)                  4857570                                                            
Reference length                           5594470                                                            
N50                                        8141                                                               
NG50                                       7478                                                               
N75                                        3747                                                               
NG75                                       2806                                                               
L50                                        185                                                                
LG50                                       211                                                                
L75                                        412                                                                
LG75                                       506                                                                
#local misassemblies                       7                                                                  
#misassemblies                             2                                                                  
#misassembled contigs                      2                                                                  
Misassembled contigs length                22110                                                              
Misassemblies inter-contig overlap         781                                                                
#unaligned contigs                         0 + 3 part                                                         
Unaligned contigs length                   128                                                                
#ambiguously mapped contigs                123                                                                
Extra bases in ambiguously mapped contigs  -69215                                                             
#N's                                       0                                                                  
#N's per 100 kbp                           0.00                                                               
Genome fraction (%)                        91.318                                                             
Duplication ratio                          1.002                                                              
#genes                                     4113 + 810 part                                                    
#predicted genes (unique)                  5981                                                               
#predicted genes (>= 0 bp)                 5981                                                               
#predicted genes (>= 300 bp)               4612                                                               
#predicted genes (>= 1500 bp)              542                                                                
#predicted genes (>= 3000 bp)              44                                                                 
#mismatches                                96                                                                 
#indels                                    585                                                                
Indels length                              589                                                                
#mismatches per 100 kbp                    1.88                                                               
#indels per 100 kbp                        11.45                                                              
GC (%)                                     50.33                                                              
Reference GC (%)                           50.48                                                              
Average %IDY                               99.879                                                             
Largest alignment                          52286                                                              
NA50                                       8122                                                               
NGA50                                      7477                                                               
NA75                                       3695                                                               
NGA75                                      2781                                                               
LA50                                       186                                                                
LGA50                                      212                                                                
LA75                                       414                                                                
LGA75                                      510                                                                
#Mis_misassemblies                         2                                                                  
#Mis_relocations                           2                                                                  
#Mis_translocations                        0                                                                  
#Mis_inversions                            0                                                                  
#Mis_misassembled contigs                  2                                                                  
Mis_Misassembled contigs length            22110                                                              
#Mis_local misassemblies                   7                                                                  
#Mis_short indels (<= 5 bp)                585                                                                
#Mis_long indels (> 5 bp)                  0                                                                  
#Una_fully unaligned contigs               0                                                                  
Una_Fully unaligned length                 0                                                                  
#Una_partially unaligned contigs           3                                                                  
#Una_with misassembly                      0                                                                  
#Una_both parts are significant            0                                                                  
Una_Partially unaligned length             128                                                                
GAGE_Contigs #                             1604                                                               
GAGE_Min contig                            200                                                                
GAGE_Max contig                            52286                                                              
GAGE_N50                                   7478 COUNT: 211                                                    
GAGE_Genome size                           5594470                                                            
GAGE_Assembly size                         5188848                                                            
GAGE_Chaff bases                           0                                                                  
GAGE_Missing reference bases               277947(4.97%)                                                      
GAGE_Missing assembly bases                149(0.00%)                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                           
GAGE_Duplicated reference bases            0                                                                  
GAGE_Compressed reference bases            143528                                                             
GAGE_Bad trim                              149                                                                
GAGE_Avg idy                               99.98                                                              
GAGE_SNPs                                  157                                                                
GAGE_Indels < 5bp                          587                                                                
GAGE_Indels >= 5                           5                                                                  
GAGE_Inversions                            0                                                                  
GAGE_Relocation                            4                                                                  
GAGE_Translocation                         0                                                                  
GAGE_Corrected contig #                    1611                                                               
GAGE_Corrected assembly size               5189106                                                            
GAGE_Min correct contig                    200                                                                
GAGE_Max correct contig                    52288                                                              
GAGE_Corrected N50                         7436 COUNT: 213                                                    
