All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.86coverage_out.unpadded
#Contigs (>= 0 bp)             1473                                                                          
#Contigs (>= 1000 bp)          194                                                                           
Total length (>= 0 bp)         5949494                                                                       
Total length (>= 1000 bp)      5498169                                                                       
#Contigs                       1446                                                                          
Largest contig                 201483                                                                        
Total length                   5944819                                                                       
Reference length               5594470                                                                       
GC (%)                         50.42                                                                         
Reference GC (%)               50.48                                                                         
N50                            73371                                                                         
NG50                           75615                                                                         
N75                            32299                                                                         
NG75                           40817                                                                         
#misassemblies                 243                                                                           
#local misassemblies           32                                                                            
#unaligned contigs             23 + 707 part                                                                 
Unaligned contigs length       49511                                                                         
Genome fraction (%)            98.039                                                                        
Duplication ratio              1.072                                                                         
#N's per 100 kbp               28.73                                                                         
#mismatches per 100 kbp        27.35                                                                         
#indels per 100 kbp            5.93                                                                          
#genes                         5204 + 172 part                                                               
#predicted genes (unique)      6682                                                                          
#predicted genes (>= 0 bp)     6715                                                                          
#predicted genes (>= 300 bp)   4850                                                                          
#predicted genes (>= 1500 bp)  677                                                                           
#predicted genes (>= 3000 bp)  66                                                                            
Largest alignment              172626                                                                        
NA50                           65398                                                                         
NGA50                          67666                                                                         
NA75                           29298                                                                         
NGA75                          33358                                                                         
