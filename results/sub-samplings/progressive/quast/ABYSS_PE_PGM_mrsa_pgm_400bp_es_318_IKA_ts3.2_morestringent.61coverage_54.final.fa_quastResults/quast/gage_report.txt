All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.61coverage_54.final
GAGE_Contigs #                   447                                                                           
GAGE_Min contig                  200                                                                           
GAGE_Max contig                  53615                                                                         
GAGE_N50                         13790 COUNT: 62                                                               
GAGE_Genome size                 2813862                                                                       
GAGE_Assembly size               2740884                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     68427(2.43%)                                                                  
GAGE_Missing assembly bases      22(0.00%)                                                                     
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  0                                                                             
GAGE_Compressed reference bases  11427                                                                         
GAGE_Bad trim                    22                                                                            
GAGE_Avg idy                     99.99                                                                         
GAGE_SNPs                        40                                                                            
GAGE_Indels < 5bp                124                                                                           
GAGE_Indels >= 5                 3                                                                             
GAGE_Inversions                  0                                                                             
GAGE_Relocation                  2                                                                             
GAGE_Translocation               0                                                                             
GAGE_Corrected contig #          451                                                                           
GAGE_Corrected assembly size     2741621                                                                       
GAGE_Min correct contig          200                                                                           
GAGE_Max correct contig          52152                                                                         
GAGE_Corrected N50               13784 COUNT: 63                                                               
