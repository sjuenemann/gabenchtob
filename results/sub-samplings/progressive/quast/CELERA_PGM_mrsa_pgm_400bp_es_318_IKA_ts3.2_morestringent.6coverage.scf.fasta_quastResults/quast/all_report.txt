All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.6coverage.scf
#Contigs                                   856                                                                   
#Contigs (>= 0 bp)                         856                                                                   
#Contigs (>= 1000 bp)                      856                                                                   
Largest contig                             13254                                                                 
Total length                               1989726                                                               
Total length (>= 0 bp)                     1989726                                                               
Total length (>= 1000 bp)                  1989726                                                               
Reference length                           2813862                                                               
N50                                        2614                                                                  
NG50                                       1784                                                                  
N75                                        1643                                                                  
NG75                                       None                                                                  
L50                                        233                                                                   
LG50                                       424                                                                   
L75                                        474                                                                   
LG75                                       None                                                                  
#local misassemblies                       2                                                                     
#misassemblies                             9                                                                     
#misassembled contigs                      9                                                                     
Misassembled contigs length                20706                                                                 
Misassemblies inter-contig overlap         816                                                                   
#unaligned contigs                         0 + 2 part                                                            
Unaligned contigs length                   277                                                                   
#ambiguously mapped contigs                0                                                                     
Extra bases in ambiguously mapped contigs  0                                                                     
#N's                                       0                                                                     
#N's per 100 kbp                           0.00                                                                  
Genome fraction (%)                        70.609                                                                
Duplication ratio                          1.002                                                                 
#genes                                     1246 + 888 part                                                       
#predicted genes (unique)                  2821                                                                  
#predicted genes (>= 0 bp)                 2821                                                                  
#predicted genes (>= 300 bp)               1952                                                                  
#predicted genes (>= 1500 bp)              93                                                                    
#predicted genes (>= 3000 bp)              4                                                                     
#mismatches                                120                                                                   
#indels                                    2153                                                                  
Indels length                              2200                                                                  
#mismatches per 100 kbp                    6.04                                                                  
#indels per 100 kbp                        108.36                                                                
GC (%)                                     33.00                                                                 
Reference GC (%)                           32.81                                                                 
Average %IDY                               99.524                                                                
Largest alignment                          13253                                                                 
NA50                                       2603                                                                  
NGA50                                      1774                                                                  
NA75                                       1639                                                                  
NGA75                                      None                                                                  
LA50                                       234                                                                   
LGA50                                      426                                                                   
LA75                                       476                                                                   
LGA75                                      None                                                                  
#Mis_misassemblies                         9                                                                     
#Mis_relocations                           8                                                                     
#Mis_translocations                        1                                                                     
#Mis_inversions                            0                                                                     
#Mis_misassembled contigs                  9                                                                     
Mis_Misassembled contigs length            20706                                                                 
#Mis_local misassemblies                   2                                                                     
#Mis_short indels (<= 5 bp)                2153                                                                  
#Mis_long indels (> 5 bp)                  0                                                                     
#Una_fully unaligned contigs               0                                                                     
Una_Fully unaligned length                 0                                                                     
#Una_partially unaligned contigs           2                                                                     
#Una_with misassembly                      0                                                                     
#Una_both parts are significant            1                                                                     
Una_Partially unaligned length             277                                                                   
GAGE_Contigs #                             856                                                                   
GAGE_Min contig                            1003                                                                  
GAGE_Max contig                            13254                                                                 
GAGE_N50                                   1784 COUNT: 424                                                       
GAGE_Genome size                           2813862                                                               
GAGE_Assembly size                         1989726                                                               
GAGE_Chaff bases                           0                                                                     
GAGE_Missing reference bases               812475(28.87%)                                                        
GAGE_Missing assembly bases                379(0.02%)                                                            
GAGE_Missing assembly contigs              0(0.00%)                                                              
GAGE_Duplicated reference bases            376                                                                   
GAGE_Compressed reference bases            19763                                                                 
GAGE_Bad trim                              379                                                                   
GAGE_Avg idy                               99.88                                                                 
GAGE_SNPs                                  108                                                                   
GAGE_Indels < 5bp                          2121                                                                  
GAGE_Indels >= 5                           2                                                                     
GAGE_Inversions                            2                                                                     
GAGE_Relocation                            3                                                                     
GAGE_Translocation                         1                                                                     
GAGE_Corrected contig #                    862                                                                   
GAGE_Corrected assembly size               1990121                                                               
GAGE_Min correct contig                    279                                                                   
GAGE_Max correct contig                    13259                                                                 
GAGE_Corrected N50                         1775 COUNT: 427                                                       
