All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.86coverage.contigs
#Contigs (>= 0 bp)             116                                                                                           
#Contigs (>= 1000 bp)          64                                                                                            
Total length (>= 0 bp)         2774571                                                                                       
Total length (>= 1000 bp)      2762470                                                                                       
#Contigs                       79                                                                                            
Largest contig                 207928                                                                                        
Total length                   2769065                                                                                       
Reference length               2813862                                                                                       
GC (%)                         32.68                                                                                         
Reference GC (%)               32.81                                                                                         
N50                            87104                                                                                         
NG50                           87104                                                                                         
N75                            43575                                                                                         
NG75                           43476                                                                                         
#misassemblies                 2                                                                                             
#local misassemblies           2                                                                                             
#unaligned contigs             1 + 0 part                                                                                    
Unaligned contigs length       5386                                                                                          
Genome fraction (%)            97.921                                                                                        
Duplication ratio              1.000                                                                                         
#N's per 100 kbp               0.00                                                                                          
#mismatches per 100 kbp        1.13                                                                                          
#indels per 100 kbp            0.62                                                                                          
#genes                         2651 + 34 part                                                                                
#predicted genes (unique)      2626                                                                                          
#predicted genes (>= 0 bp)     2626                                                                                          
#predicted genes (>= 300 bp)   2280                                                                                          
#predicted genes (>= 1500 bp)  299                                                                                           
#predicted genes (>= 3000 bp)  29                                                                                            
Largest alignment              207928                                                                                        
NA50                           87104                                                                                         
NGA50                          87104                                                                                         
NA75                           43575                                                                                         
NGA75                          43476                                                                                         
