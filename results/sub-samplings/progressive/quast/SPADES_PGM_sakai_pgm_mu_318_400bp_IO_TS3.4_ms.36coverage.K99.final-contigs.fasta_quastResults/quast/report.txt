All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.36coverage.K99.final-contigs
#Contigs (>= 0 bp)             593                                                                       
#Contigs (>= 1000 bp)          170                                                                       
Total length (>= 0 bp)         5394474                                                                   
Total length (>= 1000 bp)      5267166                                                                   
#Contigs                       393                                                                       
Largest contig                 352173                                                                    
Total length                   5366737                                                                   
Reference length               5594470                                                                   
GC (%)                         50.31                                                                     
Reference GC (%)               50.48                                                                     
N50                            142119                                                                    
NG50                           140545                                                                    
N75                            56372                                                                     
NG75                           44315                                                                     
#misassemblies                 6                                                                         
#local misassemblies           19                                                                        
#unaligned contigs             0 + 0 part                                                                
Unaligned contigs length       0                                                                         
Genome fraction (%)            94.359                                                                    
Duplication ratio              1.002                                                                     
#N's per 100 kbp               0.00                                                                      
#mismatches per 100 kbp        7.62                                                                      
#indels per 100 kbp            14.15                                                                     
#genes                         4932 + 194 part                                                           
#predicted genes (unique)      5556                                                                      
#predicted genes (>= 0 bp)     5574                                                                      
#predicted genes (>= 300 bp)   4585                                                                      
#predicted genes (>= 1500 bp)  618                                                                       
#predicted genes (>= 3000 bp)  58                                                                        
Largest alignment              352173                                                                    
NA50                           142119                                                                    
NGA50                          140545                                                                    
NA75                           56372                                                                     
NGA75                          44315                                                                     
