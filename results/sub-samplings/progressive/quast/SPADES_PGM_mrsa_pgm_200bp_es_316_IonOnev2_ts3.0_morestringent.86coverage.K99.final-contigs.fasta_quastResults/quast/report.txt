All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.86coverage.K99.final-contigs
#Contigs (>= 0 bp)             377                                                                                       
#Contigs (>= 1000 bp)          56                                                                                        
Total length (>= 0 bp)         2818540                                                                                   
Total length (>= 1000 bp)      2758437                                                                                   
#Contigs                       113                                                                                       
Largest contig                 236815                                                                                    
Total length                   2782664                                                                                   
Reference length               2813862                                                                                   
GC (%)                         32.66                                                                                     
Reference GC (%)               32.81                                                                                     
N50                            108601                                                                                    
NG50                           108601                                                                                    
N75                            46836                                                                                     
NG75                           46836                                                                                     
#misassemblies                 5                                                                                         
#local misassemblies           13                                                                                        
#unaligned contigs             0 + 2 part                                                                                
Unaligned contigs length       98                                                                                        
Genome fraction (%)            98.438                                                                                    
Duplication ratio              1.002                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        5.49                                                                                      
#indels per 100 kbp            13.57                                                                                     
#genes                         2656 + 46 part                                                                            
#predicted genes (unique)      2735                                                                                      
#predicted genes (>= 0 bp)     2739                                                                                      
#predicted genes (>= 300 bp)   2322                                                                                      
#predicted genes (>= 1500 bp)  290                                                                                       
#predicted genes (>= 3000 bp)  27                                                                                        
Largest alignment              236815                                                                                    
NA50                           90285                                                                                     
NGA50                          90285                                                                                     
NA75                           45912                                                                                     
NGA75                          39829                                                                                     
