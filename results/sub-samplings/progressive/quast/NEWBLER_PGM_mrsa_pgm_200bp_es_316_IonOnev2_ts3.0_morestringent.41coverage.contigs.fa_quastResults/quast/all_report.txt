All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.41coverage.contigs
#Contigs                                   113                                                                              
#Contigs (>= 0 bp)                         152                                                                              
#Contigs (>= 1000 bp)                      85                                                                               
Largest contig                             149620                                                                           
Total length                               2766911                                                                          
Total length (>= 0 bp)                     2772082                                                                          
Total length (>= 1000 bp)                  2755870                                                                          
Reference length                           2813862                                                                          
N50                                        56616                                                                            
NG50                                       56373                                                                            
N75                                        35434                                                                            
NG75                                       35020                                                                            
L50                                        17                                                                               
LG50                                       18                                                                               
L75                                        32                                                                               
LG75                                       33                                                                               
#local misassemblies                       7                                                                                
#misassemblies                             1                                                                                
#misassembled contigs                      1                                                                                
Misassembled contigs length                46577                                                                            
Misassemblies inter-contig overlap         1270                                                                             
#unaligned contigs                         0 + 3 part                                                                       
Unaligned contigs length                   158                                                                              
#ambiguously mapped contigs                13                                                                               
Extra bases in ambiguously mapped contigs  -8800                                                                            
#N's                                       0                                                                                
#N's per 100 kbp                           0.00                                                                             
Genome fraction (%)                        98.008                                                                           
Duplication ratio                          1.001                                                                            
#genes                                     2650 + 45 part                                                                   
#predicted genes (unique)                  2708                                                                             
#predicted genes (>= 0 bp)                 2708                                                                             
#predicted genes (>= 300 bp)               2326                                                                             
#predicted genes (>= 1500 bp)              285                                                                              
#predicted genes (>= 3000 bp)              26                                                                               
#mismatches                                90                                                                               
#indels                                    238                                                                              
Indels length                              274                                                                              
#mismatches per 100 kbp                    3.26                                                                             
#indels per 100 kbp                        8.63                                                                             
GC (%)                                     32.64                                                                            
Reference GC (%)                           32.81                                                                            
Average %IDY                               99.168                                                                           
Largest alignment                          149620                                                                           
NA50                                       56616                                                                            
NGA50                                      56373                                                                            
NA75                                       35020                                                                            
NGA75                                      32330                                                                            
LA50                                       17                                                                               
LGA50                                      18                                                                               
LA75                                       33                                                                               
LGA75                                      34                                                                               
#Mis_misassemblies                         1                                                                                
#Mis_relocations                           1                                                                                
#Mis_translocations                        0                                                                                
#Mis_inversions                            0                                                                                
#Mis_misassembled contigs                  1                                                                                
Mis_Misassembled contigs length            46577                                                                            
#Mis_local misassemblies                   7                                                                                
#Mis_short indels (<= 5 bp)                238                                                                              
#Mis_long indels (> 5 bp)                  0                                                                                
#Una_fully unaligned contigs               0                                                                                
Una_Fully unaligned length                 0                                                                                
#Una_partially unaligned contigs           3                                                                                
#Una_with misassembly                      0                                                                                
#Una_both parts are significant            0                                                                                
Una_Partially unaligned length             158                                                                              
GAGE_Contigs #                             113                                                                              
GAGE_Min contig                            201                                                                              
GAGE_Max contig                            149620                                                                           
GAGE_N50                                   56373 COUNT: 18                                                                  
GAGE_Genome size                           2813862                                                                          
GAGE_Assembly size                         2766911                                                                          
GAGE_Chaff bases                           0                                                                                
GAGE_Missing reference bases               9445(0.34%)                                                                      
GAGE_Missing assembly bases                62(0.00%)                                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                                         
GAGE_Duplicated reference bases            295                                                                              
GAGE_Compressed reference bases            41611                                                                            
GAGE_Bad trim                              62                                                                               
GAGE_Avg idy                               99.98                                                                            
GAGE_SNPs                                  42                                                                               
GAGE_Indels < 5bp                          259                                                                              
GAGE_Indels >= 5                           6                                                                                
GAGE_Inversions                            0                                                                                
GAGE_Relocation                            2                                                                                
GAGE_Translocation                         0                                                                                
GAGE_Corrected contig #                    119                                                                              
GAGE_Corrected assembly size               2768129                                                                          
GAGE_Min correct contig                    201                                                                              
GAGE_Max correct contig                    129421                                                                           
GAGE_Corrected N50                         43601 COUNT: 19                                                                  
