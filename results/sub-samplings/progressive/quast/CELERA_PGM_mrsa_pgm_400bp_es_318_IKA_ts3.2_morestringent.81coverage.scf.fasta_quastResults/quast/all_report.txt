All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.81coverage.scf
#Contigs                                   62                                                                     
#Contigs (>= 0 bp)                         62                                                                     
#Contigs (>= 1000 bp)                      62                                                                     
Largest contig                             302059                                                                 
Total length                               2744487                                                                
Total length (>= 0 bp)                     2744487                                                                
Total length (>= 1000 bp)                  2744487                                                                
Reference length                           2813862                                                                
N50                                        130404                                                                 
NG50                                       122037                                                                 
N75                                        43505                                                                  
NG75                                       40290                                                                  
L50                                        8                                                                      
LG50                                       9                                                                      
L75                                        18                                                                     
LG75                                       20                                                                     
#local misassemblies                       2                                                                      
#misassemblies                             2                                                                      
#misassembled contigs                      2                                                                      
Misassembled contigs length                207889                                                                 
Misassemblies inter-contig overlap         508                                                                    
#unaligned contigs                         0 + 0 part                                                             
Unaligned contigs length                   0                                                                      
#ambiguously mapped contigs                0                                                                      
Extra bases in ambiguously mapped contigs  0                                                                      
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        97.133                                                                 
Duplication ratio                          1.004                                                                  
#genes                                     2625 + 37 part                                                         
#predicted genes (unique)                  2660                                                                   
#predicted genes (>= 0 bp)                 2661                                                                   
#predicted genes (>= 300 bp)               2290                                                                   
#predicted genes (>= 1500 bp)              284                                                                    
#predicted genes (>= 3000 bp)              28                                                                     
#mismatches                                89                                                                     
#indels                                    202                                                                    
Indels length                              273                                                                    
#mismatches per 100 kbp                    3.26                                                                   
#indels per 100 kbp                        7.39                                                                   
GC (%)                                     32.63                                                                  
Reference GC (%)                           32.81                                                                  
Average %IDY                               98.379                                                                 
Largest alignment                          302059                                                                 
NA50                                       104307                                                                 
NGA50                                      104307                                                                 
NA75                                       42207                                                                  
NGA75                                      40290                                                                  
LA50                                       9                                                                      
LGA50                                      9                                                                      
LA75                                       20                                                                     
LGA75                                      21                                                                     
#Mis_misassemblies                         2                                                                      
#Mis_relocations                           2                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  2                                                                      
Mis_Misassembled contigs length            207889                                                                 
#Mis_local misassemblies                   2                                                                      
#Mis_short indels (<= 5 bp)                198                                                                    
#Mis_long indels (> 5 bp)                  4                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           0                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             0                                                                      
GAGE_Contigs #                             62                                                                     
GAGE_Min contig                            1011                                                                   
GAGE_Max contig                            302059                                                                 
GAGE_N50                                   122037 COUNT: 9                                                        
GAGE_Genome size                           2813862                                                                
GAGE_Assembly size                         2744487                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               74173(2.64%)                                                           
GAGE_Missing assembly bases                63(0.00%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            0                                                                      
GAGE_Compressed reference bases            14351                                                                  
GAGE_Bad trim                              38                                                                     
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  50                                                                     
GAGE_Indels < 5bp                          181                                                                    
GAGE_Indels >= 5                           5                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            1                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    68                                                                     
GAGE_Corrected assembly size               2745301                                                                
GAGE_Min correct contig                    490                                                                    
GAGE_Max correct contig                    302079                                                                 
GAGE_Corrected N50                         94579 COUNT: 10                                                        
