All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.81coverage.scf
#Mis_misassemblies               2                                                                      
#Mis_relocations                 2                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        2                                                                      
Mis_Misassembled contigs length  207889                                                                 
#Mis_local misassemblies         2                                                                      
#mismatches                      89                                                                     
#indels                          202                                                                    
#Mis_short indels (<= 5 bp)      198                                                                    
#Mis_long indels (> 5 bp)        4                                                                      
Indels length                    273                                                                    
