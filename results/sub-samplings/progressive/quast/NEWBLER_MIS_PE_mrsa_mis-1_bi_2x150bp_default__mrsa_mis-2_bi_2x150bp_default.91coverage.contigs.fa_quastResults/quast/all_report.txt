All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.91coverage.contigs
#Contigs                                   81                                                                                            
#Contigs (>= 0 bp)                         116                                                                                           
#Contigs (>= 1000 bp)                      63                                                                                            
Largest contig                             300744                                                                                        
Total length                               2769465                                                                                       
Total length (>= 0 bp)                     2774555                                                                                       
Total length (>= 1000 bp)                  2762202                                                                                       
Reference length                           2813862                                                                                       
N50                                        112274                                                                                        
NG50                                       112274                                                                                        
N75                                        51793                                                                                         
NG75                                       50346                                                                                         
L50                                        9                                                                                             
LG50                                       9                                                                                             
L75                                        18                                                                                            
LG75                                       19                                                                                            
#local misassemblies                       4                                                                                             
#misassemblies                             2                                                                                             
#misassembled contigs                      2                                                                                             
Misassembled contigs length                70791                                                                                         
Misassemblies inter-contig overlap         602                                                                                           
#unaligned contigs                         1 + 0 part                                                                                    
Unaligned contigs length                   5386                                                                                          
#ambiguously mapped contigs                11                                                                                            
Extra bases in ambiguously mapped contigs  -8852                                                                                         
#N's                                       0                                                                                             
#N's per 100 kbp                           0.00                                                                                          
Genome fraction (%)                        97.929                                                                                        
Duplication ratio                          1.000                                                                                         
#genes                                     2648 + 41 part                                                                                
#predicted genes (unique)                  2635                                                                                          
#predicted genes (>= 0 bp)                 2635                                                                                          
#predicted genes (>= 300 bp)               2282                                                                                          
#predicted genes (>= 1500 bp)              298                                                                                           
#predicted genes (>= 3000 bp)              28                                                                                            
#mismatches                                39                                                                                            
#indels                                    11                                                                                            
Indels length                              65                                                                                            
#mismatches per 100 kbp                    1.42                                                                                          
#indels per 100 kbp                        0.40                                                                                          
GC (%)                                     32.67                                                                                         
Reference GC (%)                           32.81                                                                                         
Average %IDY                               98.934                                                                                        
Largest alignment                          300744                                                                                        
NA50                                       112274                                                                                        
NGA50                                      112274                                                                                        
NA75                                       51793                                                                                         
NGA75                                      50346                                                                                         
LA50                                       9                                                                                             
LGA50                                      9                                                                                             
LA75                                       18                                                                                            
LGA75                                      19                                                                                            
#Mis_misassemblies                         2                                                                                             
#Mis_relocations                           2                                                                                             
#Mis_translocations                        0                                                                                             
#Mis_inversions                            0                                                                                             
#Mis_misassembled contigs                  2                                                                                             
Mis_Misassembled contigs length            70791                                                                                         
#Mis_local misassemblies                   4                                                                                             
#Mis_short indels (<= 5 bp)                10                                                                                            
#Mis_long indels (> 5 bp)                  1                                                                                             
#Una_fully unaligned contigs               1                                                                                             
Una_Fully unaligned length                 5386                                                                                          
#Una_partially unaligned contigs           0                                                                                             
#Una_with misassembly                      0                                                                                             
#Una_both parts are significant            0                                                                                             
Una_Partially unaligned length             0                                                                                             
GAGE_Contigs #                             81                                                                                            
GAGE_Min contig                            200                                                                                           
GAGE_Max contig                            300744                                                                                        
GAGE_N50                                   112274 COUNT: 9                                                                               
GAGE_Genome size                           2813862                                                                                       
GAGE_Assembly size                         2769465                                                                                       
GAGE_Chaff bases                           0                                                                                             
GAGE_Missing reference bases               12784(0.45%)                                                                                  
GAGE_Missing assembly bases                5386(0.19%)                                                                                   
GAGE_Missing assembly contigs              1(1.23%)                                                                                      
GAGE_Duplicated reference bases            0                                                                                             
GAGE_Compressed reference bases            37902                                                                                         
GAGE_Bad trim                              0                                                                                             
GAGE_Avg idy                               99.99                                                                                         
GAGE_SNPs                                  41                                                                                            
GAGE_Indels < 5bp                          10                                                                                            
GAGE_Indels >= 5                           4                                                                                             
GAGE_Inversions                            0                                                                                             
GAGE_Relocation                            3                                                                                             
GAGE_Translocation                         0                                                                                             
GAGE_Corrected contig #                    87                                                                                            
GAGE_Corrected assembly size               2764685                                                                                       
GAGE_Min correct contig                    201                                                                                           
GAGE_Max correct contig                    300744                                                                                        
GAGE_Corrected N50                         75533 COUNT: 11                                                                               
