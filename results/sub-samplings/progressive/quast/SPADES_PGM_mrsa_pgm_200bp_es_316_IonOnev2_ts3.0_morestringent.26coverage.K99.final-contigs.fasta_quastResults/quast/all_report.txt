All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.26coverage.K99.final-contigs
#Contigs                                   84                                                                                        
#Contigs (>= 0 bp)                         99                                                                                        
#Contigs (>= 1000 bp)                      70                                                                                        
Largest contig                             245172                                                                                    
Total length                               2768959                                                                                   
Total length (>= 0 bp)                     2770923                                                                                   
Total length (>= 1000 bp)                  2761789                                                                                   
Reference length                           2813862                                                                                   
N50                                        66223                                                                                     
NG50                                       50635                                                                                     
N75                                        37419                                                                                     
NG75                                       37301                                                                                     
L50                                        13                                                                                        
LG50                                       14                                                                                        
L75                                        28                                                                                        
LG75                                       29                                                                                        
#local misassemblies                       12                                                                                        
#misassemblies                             9                                                                                         
#misassembled contigs                      7                                                                                         
Misassembled contigs length                395960                                                                                    
Misassemblies inter-contig overlap         3628                                                                                      
#unaligned contigs                         0 + 0 part                                                                                
Unaligned contigs length                   0                                                                                         
#ambiguously mapped contigs                4                                                                                         
Extra bases in ambiguously mapped contigs  -1666                                                                                     
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        98.303                                                                                    
Duplication ratio                          1.002                                                                                     
#genes                                     2631 + 47 part                                                                            
#predicted genes (unique)                  2785                                                                                      
#predicted genes (>= 0 bp)                 2787                                                                                      
#predicted genes (>= 300 bp)               2337                                                                                      
#predicted genes (>= 1500 bp)              275                                                                                       
#predicted genes (>= 3000 bp)              23                                                                                        
#mismatches                                370                                                                                       
#indels                                    802                                                                                       
Indels length                              999                                                                                       
#mismatches per 100 kbp                    13.38                                                                                     
#indels per 100 kbp                        28.99                                                                                     
GC (%)                                     32.65                                                                                     
Reference GC (%)                           32.81                                                                                     
Average %IDY                               98.687                                                                                    
Largest alignment                          164205                                                                                    
NA50                                       50433                                                                                     
NGA50                                      50433                                                                                     
NA75                                       37301                                                                                     
NGA75                                      37260                                                                                     
LA50                                       16                                                                                        
LGA50                                      16                                                                                        
LA75                                       32                                                                                        
LGA75                                      33                                                                                        
#Mis_misassemblies                         9                                                                                         
#Mis_relocations                           8                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            1                                                                                         
#Mis_misassembled contigs                  7                                                                                         
Mis_Misassembled contigs length            395960                                                                                    
#Mis_local misassemblies                   12                                                                                        
#Mis_short indels (<= 5 bp)                797                                                                                       
#Mis_long indels (> 5 bp)                  5                                                                                         
#Una_fully unaligned contigs               0                                                                                         
Una_Fully unaligned length                 0                                                                                         
#Una_partially unaligned contigs           0                                                                                         
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             0                                                                                         
GAGE_Contigs #                             84                                                                                        
GAGE_Min contig                            212                                                                                       
GAGE_Max contig                            245172                                                                                    
GAGE_N50                                   50635 COUNT: 14                                                                           
GAGE_Genome size                           2813862                                                                                   
GAGE_Assembly size                         2768959                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               10616(0.38%)                                                                              
GAGE_Missing assembly bases                289(0.01%)                                                                                
GAGE_Missing assembly contigs              0(0.00%)                                                                                  
GAGE_Duplicated reference bases            506                                                                                       
GAGE_Compressed reference bases            41030                                                                                     
GAGE_Bad trim                              286                                                                                       
GAGE_Avg idy                               99.96                                                                                     
GAGE_SNPs                                  84                                                                                        
GAGE_Indels < 5bp                          683                                                                                       
GAGE_Indels >= 5                           10                                                                                        
GAGE_Inversions                            2                                                                                         
GAGE_Relocation                            9                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    104                                                                                       
GAGE_Corrected assembly size               2772225                                                                                   
GAGE_Min correct contig                    232                                                                                       
GAGE_Max correct contig                    164237                                                                                    
GAGE_Corrected N50                         48208 COUNT: 19                                                                           
