All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.66coverage_out.unpadded
GAGE_Contigs #                   169                                                                                             
GAGE_Min contig                  250                                                                                             
GAGE_Max contig                  534609                                                                                          
GAGE_N50                         269622 COUNT: 4                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2886979                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     40(0.00%)                                                                                       
GAGE_Missing assembly bases      6403(0.22%)                                                                                     
GAGE_Missing assembly contigs    2(1.18%)                                                                                        
GAGE_Duplicated reference bases  79372                                                                                           
GAGE_Compressed reference bases  10197                                                                                           
GAGE_Bad trim                    246                                                                                             
GAGE_Avg idy                     99.99                                                                                           
GAGE_SNPs                        50                                                                                              
GAGE_Indels < 5bp                14                                                                                              
GAGE_Indels >= 5                 2                                                                                               
GAGE_Inversions                  3                                                                                               
GAGE_Relocation                  2                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          47                                                                                              
GAGE_Corrected assembly size     2823145                                                                                         
GAGE_Min correct contig          293                                                                                             
GAGE_Max correct contig          336189                                                                                          
GAGE_Corrected N50               126730 COUNT: 7                                                                                 
