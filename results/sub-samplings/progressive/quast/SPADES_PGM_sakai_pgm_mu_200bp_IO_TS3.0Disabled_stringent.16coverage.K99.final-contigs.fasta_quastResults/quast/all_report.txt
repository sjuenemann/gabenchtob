All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.16coverage.K99.final-contigs
#Contigs                                   228                                                                                  
#Contigs (>= 0 bp)                         274                                                                                  
#Contigs (>= 1000 bp)                      187                                                                                  
Largest contig                             283391                                                                               
Total length                               5368991                                                                              
Total length (>= 0 bp)                     5374445                                                                              
Total length (>= 1000 bp)                  5350342                                                                              
Reference length                           5594470                                                                              
N50                                        83165                                                                                
NG50                                       82376                                                                                
N75                                        43373                                                                                
NG75                                       38165                                                                                
L50                                        18                                                                                   
LG50                                       19                                                                                   
L75                                        38                                                                                   
LG75                                       43                                                                                   
#local misassemblies                       19                                                                                   
#misassemblies                             13                                                                                   
#misassembled contigs                      12                                                                                   
Misassembled contigs length                963737                                                                               
Misassemblies inter-contig overlap         9204                                                                                 
#unaligned contigs                         0 + 7 part                                                                           
Unaligned contigs length                   1305                                                                                 
#ambiguously mapped contigs                43                                                                                   
Extra bases in ambiguously mapped contigs  -40678                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.171                                                                               
Duplication ratio                          1.013                                                                                
#genes                                     4932 + 146 part                                                                      
#predicted genes (unique)                  6271                                                                                 
#predicted genes (>= 0 bp)                 6341                                                                                 
#predicted genes (>= 300 bp)               5022                                                                                 
#predicted genes (>= 1500 bp)              449                                                                                  
#predicted genes (>= 3000 bp)              24                                                                                   
#mismatches                                1350                                                                                 
#indels                                    2904                                                                                 
Indels length                              3049                                                                                 
#mismatches per 100 kbp                    25.62                                                                                
#indels per 100 kbp                        55.12                                                                                
GC (%)                                     50.29                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               98.819                                                                               
Largest alignment                          246907                                                                               
NA50                                       82376                                                                                
NGA50                                      76742                                                                                
NA75                                       43245                                                                                
NGA75                                      38165                                                                                
LA50                                       20                                                                                   
LGA50                                      22                                                                                   
LA75                                       42                                                                                   
LGA75                                      46                                                                                   
#Mis_misassemblies                         13                                                                                   
#Mis_relocations                           11                                                                                   
#Mis_translocations                        0                                                                                    
#Mis_inversions                            2                                                                                    
#Mis_misassembled contigs                  12                                                                                   
Mis_Misassembled contigs length            963737                                                                               
#Mis_local misassemblies                   19                                                                                   
#Mis_short indels (<= 5 bp)                2902                                                                                 
#Mis_long indels (> 5 bp)                  2                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           7                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            1                                                                                    
Una_Partially unaligned length             1305                                                                                 
GAGE_Contigs #                             228                                                                                  
GAGE_Min contig                            201                                                                                  
GAGE_Max contig                            283391                                                                               
GAGE_N50                                   82376 COUNT: 19                                                                      
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5368991                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               33234(0.59%)                                                                         
GAGE_Missing assembly bases                1963(0.04%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            62132                                                                                
GAGE_Compressed reference bases            276841                                                                               
GAGE_Bad trim                              1963                                                                                 
GAGE_Avg idy                               99.92                                                                                
GAGE_SNPs                                  646                                                                                  
GAGE_Indels < 5bp                          2919                                                                                 
GAGE_Indels >= 5                           14                                                                                   
GAGE_Inversions                            7                                                                                    
GAGE_Relocation                            11                                                                                   
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    255                                                                                  
GAGE_Corrected assembly size               5326159                                                                              
GAGE_Min correct contig                    201                                                                                  
GAGE_Max correct contig                    224217                                                                               
GAGE_Corrected N50                         66292 COUNT: 24                                                                      
