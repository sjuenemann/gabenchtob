All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.16coverage.K99.final-contigs
#Contigs (>= 0 bp)             274                                                                                  
#Contigs (>= 1000 bp)          187                                                                                  
Total length (>= 0 bp)         5374445                                                                              
Total length (>= 1000 bp)      5350342                                                                              
#Contigs                       228                                                                                  
Largest contig                 283391                                                                               
Total length                   5368991                                                                              
Reference length               5594470                                                                              
GC (%)                         50.29                                                                                
Reference GC (%)               50.48                                                                                
N50                            83165                                                                                
NG50                           82376                                                                                
N75                            43373                                                                                
NG75                           38165                                                                                
#misassemblies                 13                                                                                   
#local misassemblies           19                                                                                   
#unaligned contigs             0 + 7 part                                                                           
Unaligned contigs length       1305                                                                                 
Genome fraction (%)            94.171                                                                               
Duplication ratio              1.013                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        25.62                                                                                
#indels per 100 kbp            55.12                                                                                
#genes                         4932 + 146 part                                                                      
#predicted genes (unique)      6271                                                                                 
#predicted genes (>= 0 bp)     6341                                                                                 
#predicted genes (>= 300 bp)   5022                                                                                 
#predicted genes (>= 1500 bp)  449                                                                                  
#predicted genes (>= 3000 bp)  24                                                                                   
Largest alignment              246907                                                                               
NA50                           82376                                                                                
NGA50                          76742                                                                                
NA75                           43245                                                                                
NGA75                          38165                                                                                
