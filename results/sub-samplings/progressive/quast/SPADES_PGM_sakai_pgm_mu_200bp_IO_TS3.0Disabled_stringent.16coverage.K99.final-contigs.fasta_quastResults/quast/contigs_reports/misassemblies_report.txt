All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.16coverage.K99.final-contigs
#Mis_misassemblies               13                                                                                   
#Mis_relocations                 11                                                                                   
#Mis_translocations              0                                                                                    
#Mis_inversions                  2                                                                                    
#Mis_misassembled contigs        12                                                                                   
Mis_Misassembled contigs length  963737                                                                               
#Mis_local misassemblies         19                                                                                   
#mismatches                      1350                                                                                 
#indels                          2904                                                                                 
#Mis_short indels (<= 5 bp)      2902                                                                                 
#Mis_long indels (> 5 bp)        2                                                                                    
Indels length                    3049                                                                                 
