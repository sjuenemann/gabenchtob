All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.26coverage.K99.final-contigs
#Mis_misassemblies               7                                                                         
#Mis_relocations                 6                                                                         
#Mis_translocations              1                                                                         
#Mis_inversions                  0                                                                         
#Mis_misassembled contigs        7                                                                         
Mis_Misassembled contigs length  397596                                                                    
#Mis_local misassemblies         20                                                                        
#mismatches                      485                                                                       
#indels                          1014                                                                      
#Mis_short indels (<= 5 bp)      1008                                                                      
#Mis_long indels (> 5 bp)        6                                                                         
Indels length                    1111                                                                      
