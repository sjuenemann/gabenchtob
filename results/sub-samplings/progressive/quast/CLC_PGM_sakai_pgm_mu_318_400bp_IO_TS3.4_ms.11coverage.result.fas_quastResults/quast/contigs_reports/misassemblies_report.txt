All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.11coverage.result
#Mis_misassemblies               27                                                          
#Mis_relocations                 26                                                          
#Mis_translocations              1                                                           
#Mis_inversions                  0                                                           
#Mis_misassembled contigs        27                                                          
Mis_Misassembled contigs length  237432                                                      
#Mis_local misassemblies         9                                                           
#mismatches                      660                                                         
#indels                          2632                                                        
#Mis_short indels (<= 5 bp)      2632                                                        
#Mis_long indels (> 5 bp)        0                                                           
Indels length                    2719                                                        
