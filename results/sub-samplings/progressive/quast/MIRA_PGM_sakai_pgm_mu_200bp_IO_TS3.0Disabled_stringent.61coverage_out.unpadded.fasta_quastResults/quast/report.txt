All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.61coverage_out.unpadded
#Contigs (>= 0 bp)             774                                                                           
#Contigs (>= 1000 bp)          186                                                                           
Total length (>= 0 bp)         5699697                                                                       
Total length (>= 1000 bp)      5478848                                                                       
#Contigs                       753                                                                           
Largest contig                 299410                                                                        
Total length                   5696290                                                                       
Reference length               5594470                                                                       
GC (%)                         50.42                                                                         
Reference GC (%)               50.48                                                                         
N50                            93569                                                                         
NG50                           93569                                                                         
N75                            48377                                                                         
NG75                           49808                                                                         
#misassemblies                 127                                                                           
#local misassemblies           21                                                                            
#unaligned contigs             12 + 277 part                                                                 
Unaligned contigs length       18815                                                                         
Genome fraction (%)            97.437                                                                        
Duplication ratio              1.036                                                                         
#N's per 100 kbp               9.95                                                                          
#mismatches per 100 kbp        31.24                                                                         
#indels per 100 kbp            7.08                                                                          
#genes                         5183 + 167 part                                                               
#predicted genes (unique)      6040                                                                          
#predicted genes (>= 0 bp)     6066                                                                          
#predicted genes (>= 300 bp)   4747                                                                          
#predicted genes (>= 1500 bp)  669                                                                           
#predicted genes (>= 3000 bp)  61                                                                            
Largest alignment              299375                                                                        
NA50                           78882                                                                         
NGA50                          79578                                                                         
NA75                           41069                                                                         
NGA75                          41396                                                                         
