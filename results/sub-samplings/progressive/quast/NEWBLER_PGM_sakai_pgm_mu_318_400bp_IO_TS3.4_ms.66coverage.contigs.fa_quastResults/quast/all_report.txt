All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.66coverage.contigs
#Contigs                                   282                                                              
#Contigs (>= 0 bp)                         403                                                              
#Contigs (>= 1000 bp)                      139                                                              
Largest contig                             327105                                                           
Total length                               5354426                                                          
Total length (>= 0 bp)                     5371643                                                          
Total length (>= 1000 bp)                  5287721                                                          
Reference length                           5594470                                                          
N50                                        107974                                                           
NG50                                       105965                                                           
N75                                        50031                                                            
NG75                                       44858                                                            
L50                                        14                                                               
LG50                                       15                                                               
L75                                        31                                                               
LG75                                       35                                                               
#local misassemblies                       7                                                                
#misassemblies                             4                                                                
#misassembled contigs                      4                                                                
Misassembled contigs length                363209                                                           
Misassemblies inter-contig overlap         1441                                                             
#unaligned contigs                         0 + 0 part                                                       
Unaligned contigs length                   0                                                                
#ambiguously mapped contigs                101                                                              
Extra bases in ambiguously mapped contigs  -85684                                                           
#N's                                       0                                                                
#N's per 100 kbp                           0.00                                                             
Genome fraction (%)                        94.152                                                           
Duplication ratio                          1.001                                                            
#genes                                     4987 + 141 part                                                  
#predicted genes (unique)                  5353                                                             
#predicted genes (>= 0 bp)                 5354                                                             
#predicted genes (>= 300 bp)               4496                                                             
#predicted genes (>= 1500 bp)              654                                                              
#predicted genes (>= 3000 bp)              63                                                               
#mismatches                                100                                                              
#indels                                    229                                                              
Indels length                              239                                                              
#mismatches per 100 kbp                    1.90                                                             
#indels per 100 kbp                        4.35                                                             
GC (%)                                     50.29                                                            
Reference GC (%)                           50.48                                                            
Average %IDY                               98.684                                                           
Largest alignment                          327105                                                           
NA50                                       107974                                                           
NGA50                                      105965                                                           
NA75                                       50031                                                            
NGA75                                      44858                                                            
LA50                                       15                                                               
LGA50                                      16                                                               
LA75                                       32                                                               
LGA75                                      36                                                               
#Mis_misassemblies                         4                                                                
#Mis_relocations                           4                                                                
#Mis_translocations                        0                                                                
#Mis_inversions                            0                                                                
#Mis_misassembled contigs                  4                                                                
Mis_Misassembled contigs length            363209                                                           
#Mis_local misassemblies                   7                                                                
#Mis_short indels (<= 5 bp)                229                                                              
#Mis_long indels (> 5 bp)                  0                                                                
#Una_fully unaligned contigs               0                                                                
Una_Fully unaligned length                 0                                                                
#Una_partially unaligned contigs           0                                                                
#Una_with misassembly                      0                                                                
#Una_both parts are significant            0                                                                
Una_Partially unaligned length             0                                                                
GAGE_Contigs #                             282                                                              
GAGE_Min contig                            202                                                              
GAGE_Max contig                            327105                                                           
GAGE_N50                                   105965 COUNT: 15                                                 
GAGE_Genome size                           5594470                                                          
GAGE_Assembly size                         5354426                                                          
GAGE_Chaff bases                           0                                                                
GAGE_Missing reference bases               32177(0.58%)                                                     
GAGE_Missing assembly bases                4(0.00%)                                                         
GAGE_Missing assembly contigs              0(0.00%)                                                         
GAGE_Duplicated reference bases            4482                                                             
GAGE_Compressed reference bases            227037                                                           
GAGE_Bad trim                              4                                                                
GAGE_Avg idy                               99.99                                                            
GAGE_SNPs                                  75                                                               
GAGE_Indels < 5bp                          247                                                              
GAGE_Indels >= 5                           5                                                                
GAGE_Inversions                            0                                                                
GAGE_Relocation                            7                                                                
GAGE_Translocation                         0                                                                
GAGE_Corrected contig #                    278                                                              
GAGE_Corrected assembly size               5352047                                                          
GAGE_Min correct contig                    201                                                              
GAGE_Max correct contig                    313770                                                           
GAGE_Corrected N50                         103073 COUNT: 18                                                 
