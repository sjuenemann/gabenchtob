All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.71coverage_out.unpadded
#Contigs (>= 0 bp)             685                                                                                
#Contigs (>= 1000 bp)          77                                                                                 
Total length (>= 0 bp)         3033992                                                                            
Total length (>= 1000 bp)      2819379                                                                            
#Contigs                       659                                                                                
Largest contig                 219382                                                                             
Total length                   3030069                                                                            
Reference length               2813862                                                                            
GC (%)                         32.68                                                                              
Reference GC (%)               32.81                                                                              
N50                            86401                                                                              
NG50                           88088                                                                              
N75                            33717                                                                              
NG75                           42025                                                                              
#misassemblies                 163                                                                                
#local misassemblies           28                                                                                 
#unaligned contigs             13 + 214 part                                                                      
Unaligned contigs length       18013                                                                              
Genome fraction (%)            99.714                                                                             
Duplication ratio              1.078                                                                              
#N's per 100 kbp               23.70                                                                              
#mismatches per 100 kbp        5.77                                                                               
#indels per 100 kbp            7.66                                                                               
#genes                         2686 + 40 part                                                                     
#predicted genes (unique)      3248                                                                               
#predicted genes (>= 0 bp)     3253                                                                               
#predicted genes (>= 300 bp)   2375                                                                               
#predicted genes (>= 1500 bp)  291                                                                                
#predicted genes (>= 3000 bp)  28                                                                                 
Largest alignment              198448                                                                             
NA50                           83664                                                                              
NGA50                          88088                                                                              
NA75                           30196                                                                              
NGA75                          38719                                                                              
