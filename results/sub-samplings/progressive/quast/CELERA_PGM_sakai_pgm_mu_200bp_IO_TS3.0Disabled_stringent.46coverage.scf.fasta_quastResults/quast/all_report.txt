All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.46coverage.scf
#Contigs                                   596                                                                    
#Contigs (>= 0 bp)                         596                                                                    
#Contigs (>= 1000 bp)                      596                                                                    
Largest contig                             84185                                                                  
Total length                               5290204                                                                
Total length (>= 0 bp)                     5290204                                                                
Total length (>= 1000 bp)                  5290204                                                                
Reference length                           5594470                                                                
N50                                        17992                                                                  
NG50                                       16412                                                                  
N75                                        8074                                                                   
NG75                                       6617                                                                   
L50                                        84                                                                     
LG50                                       93                                                                     
L75                                        193                                                                    
LG75                                       224                                                                    
#local misassemblies                       6                                                                      
#misassemblies                             6                                                                      
#misassembled contigs                      6                                                                      
Misassembled contigs length                84616                                                                  
Misassemblies inter-contig overlap         2476                                                                   
#unaligned contigs                         0 + 4 part                                                             
Unaligned contigs length                   172                                                                    
#ambiguously mapped contigs                3                                                                      
Extra bases in ambiguously mapped contigs  -3471                                                                  
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        93.025                                                                 
Duplication ratio                          1.016                                                                  
#genes                                     4613 + 470 part                                                        
#predicted genes (unique)                  5868                                                                   
#predicted genes (>= 0 bp)                 5874                                                                   
#predicted genes (>= 300 bp)               4731                                                                   
#predicted genes (>= 1500 bp)              532                                                                    
#predicted genes (>= 3000 bp)              38                                                                     
#mismatches                                112                                                                    
#indels                                    1039                                                                   
Indels length                              1072                                                                   
#mismatches per 100 kbp                    2.15                                                                   
#indels per 100 kbp                        19.96                                                                  
GC (%)                                     50.21                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.122                                                                 
Largest alignment                          84185                                                                  
NA50                                       17743                                                                  
NGA50                                      16313                                                                  
NA75                                       8074                                                                   
NGA75                                      6617                                                                   
LA50                                       85                                                                     
LGA50                                      94                                                                     
LA75                                       194                                                                    
LGA75                                      225                                                                    
#Mis_misassemblies                         6                                                                      
#Mis_relocations                           6                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  6                                                                      
Mis_Misassembled contigs length            84616                                                                  
#Mis_local misassemblies                   6                                                                      
#Mis_short indels (<= 5 bp)                1038                                                                   
#Mis_long indels (> 5 bp)                  1                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           4                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             172                                                                    
GAGE_Contigs #                             596                                                                    
GAGE_Min contig                            1000                                                                   
GAGE_Max contig                            84185                                                                  
GAGE_N50                                   16412 COUNT: 93                                                        
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5290204                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               322069(5.76%)                                                          
GAGE_Missing assembly bases                601(0.01%)                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            310                                                                    
GAGE_Compressed reference bases            96017                                                                  
GAGE_Bad trim                              598                                                                    
GAGE_Avg idy                               99.98                                                                  
GAGE_SNPs                                  66                                                                     
GAGE_Indels < 5bp                          964                                                                    
GAGE_Indels >= 5                           5                                                                      
GAGE_Inversions                            3                                                                      
GAGE_Relocation                            3                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    606                                                                    
GAGE_Corrected assembly size               5291938                                                                
GAGE_Min correct contig                    528                                                                    
GAGE_Max correct contig                    82206                                                                  
GAGE_Corrected N50                         16253 COUNT: 96                                                        
