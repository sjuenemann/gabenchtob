All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.66coverage.result
GAGE_Contigs #                   286                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  102531                                                                      
GAGE_N50                         36294 COUNT: 27                                                             
GAGE_Genome size                 2813862                                                                     
GAGE_Assembly size               2787083                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     15767(0.56%)                                                                
GAGE_Missing assembly bases      913(0.03%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  23611                                                                       
GAGE_Compressed reference bases  40541                                                                       
GAGE_Bad trim                    913                                                                         
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        56                                                                          
GAGE_Indels < 5bp                322                                                                         
GAGE_Indels >= 5                 3                                                                           
GAGE_Inversions                  0                                                                           
GAGE_Relocation                  3                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          194                                                                         
GAGE_Corrected assembly size     2763535                                                                     
GAGE_Min correct contig          200                                                                         
GAGE_Max correct contig          102536                                                                      
GAGE_Corrected N50               32722 COUNT: 29                                                             
