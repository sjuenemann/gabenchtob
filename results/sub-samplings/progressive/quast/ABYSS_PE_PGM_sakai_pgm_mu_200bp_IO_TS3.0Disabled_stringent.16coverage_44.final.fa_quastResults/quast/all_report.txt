All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.16coverage_44.final
#Contigs                                   2037                                                                          
#Contigs (>= 0 bp)                         5831                                                                          
#Contigs (>= 1000 bp)                      1208                                                                          
Largest contig                             25813                                                                         
Total length                               5192469                                                                       
Total length (>= 0 bp)                     5511859                                                                       
Total length (>= 1000 bp)                  4786495                                                                       
Reference length                           5594470                                                                       
N50                                        4879                                                                          
NG50                                       4556                                                                          
N75                                        2572                                                                          
NG75                                       2108                                                                          
L50                                        320                                                                           
LG50                                       362                                                                           
L75                                        680                                                                           
LG75                                       809                                                                           
#local misassemblies                       5                                                                             
#misassemblies                             4                                                                             
#misassembled contigs                      4                                                                             
Misassembled contigs length                35926                                                                         
Misassemblies inter-contig overlap         1355                                                                          
#unaligned contigs                         0 + 3 part                                                                    
Unaligned contigs length                   113                                                                           
#ambiguously mapped contigs                124                                                                           
Extra bases in ambiguously mapped contigs  -64113                                                                        
#N's                                       0                                                                             
#N's per 100 kbp                           0.00                                                                          
Genome fraction (%)                        91.507                                                                        
Duplication ratio                          1.002                                                                         
#genes                                     3738 + 1205 part                                                              
#predicted genes (unique)                  6978                                                                          
#predicted genes (>= 0 bp)                 6978                                                                          
#predicted genes (>= 300 bp)               5045                                                                          
#predicted genes (>= 1500 bp)              379                                                                           
#predicted genes (>= 3000 bp)              17                                                                            
#mismatches                                135                                                                           
#indels                                    1802                                                                          
Indels length                              1854                                                                          
#mismatches per 100 kbp                    2.64                                                                          
#indels per 100 kbp                        35.20                                                                         
GC (%)                                     50.26                                                                         
Reference GC (%)                           50.48                                                                         
Average %IDY                               99.887                                                                        
Largest alignment                          25813                                                                         
NA50                                       4858                                                                          
NGA50                                      4532                                                                          
NA75                                       2541                                                                          
NGA75                                      2097                                                                          
LA50                                       321                                                                           
LGA50                                      364                                                                           
LA75                                       684                                                                           
LGA75                                      814                                                                           
#Mis_misassemblies                         4                                                                             
#Mis_relocations                           4                                                                             
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  4                                                                             
Mis_Misassembled contigs length            35926                                                                         
#Mis_local misassemblies                   5                                                                             
#Mis_short indels (<= 5 bp)                1802                                                                          
#Mis_long indels (> 5 bp)                  0                                                                             
#Una_fully unaligned contigs               0                                                                             
Una_Fully unaligned length                 0                                                                             
#Una_partially unaligned contigs           3                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             113                                                                           
GAGE_Contigs #                             2037                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            25813                                                                         
GAGE_N50                                   4556 COUNT: 362                                                               
GAGE_Genome size                           5594470                                                                       
GAGE_Assembly size                         5192469                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               281274(5.03%)                                                                 
GAGE_Missing assembly bases                182(0.00%)                                                                    
GAGE_Missing assembly contigs              0(0.00%)                                                                      
GAGE_Duplicated reference bases            201                                                                           
GAGE_Compressed reference bases            135945                                                                        
GAGE_Bad trim                              182                                                                           
GAGE_Avg idy                               99.96                                                                         
GAGE_SNPs                                  137                                                                           
GAGE_Indels < 5bp                          1859                                                                          
GAGE_Indels >= 5                           4                                                                             
GAGE_Inversions                            1                                                                             
GAGE_Relocation                            3                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    2045                                                                          
GAGE_Corrected assembly size               5195171                                                                       
GAGE_Min correct contig                    200                                                                           
GAGE_Max correct contig                    25818                                                                         
GAGE_Corrected N50                         4531 COUNT: 365                                                               
