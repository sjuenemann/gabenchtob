All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.6coverage.contigs
#Mis_misassemblies               4                                                                          
#Mis_relocations                 3                                                                          
#Mis_translocations              0                                                                          
#Mis_inversions                  1                                                                          
#Mis_misassembled contigs        4                                                                          
Mis_Misassembled contigs length  21591                                                                      
#Mis_local misassemblies         11                                                                         
#mismatches                      359                                                                        
#indels                          2910                                                                       
#Mis_short indels (<= 5 bp)      2907                                                                       
#Mis_long indels (> 5 bp)        3                                                                          
Indels length                    3100                                                                       
