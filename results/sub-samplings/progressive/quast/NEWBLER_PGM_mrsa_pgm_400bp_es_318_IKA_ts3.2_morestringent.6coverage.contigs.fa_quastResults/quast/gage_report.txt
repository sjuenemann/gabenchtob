All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.6coverage.contigs
GAGE_Contigs #                   1317                                                                       
GAGE_Min contig                  205                                                                        
GAGE_Max contig                  22987                                                                      
GAGE_N50                         2384 COUNT: 319                                                            
GAGE_Genome size                 2813862                                                                    
GAGE_Assembly size               2536407                                                                    
GAGE_Chaff bases                 0                                                                          
GAGE_Missing reference bases     231478(8.23%)                                                              
GAGE_Missing assembly bases      269(0.01%)                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                   
GAGE_Duplicated reference bases  380                                                                        
GAGE_Compressed reference bases  44938                                                                      
GAGE_Bad trim                    267                                                                        
GAGE_Avg idy                     99.86                                                                      
GAGE_SNPs                        223                                                                        
GAGE_Indels < 5bp                2960                                                                       
GAGE_Indels >= 5                 11                                                                         
GAGE_Inversions                  2                                                                          
GAGE_Relocation                  3                                                                          
GAGE_Translocation               0                                                                          
GAGE_Corrected contig #          1332                                                                       
GAGE_Corrected assembly size     2541453                                                                    
GAGE_Min correct contig          205                                                                        
GAGE_Max correct contig          22991                                                                      
GAGE_Corrected N50               2355 COUNT: 323                                                            
