All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.46coverage_38.final
GAGE_Contigs #                   315                                                                                
GAGE_Min contig                  206                                                                                
GAGE_Max contig                  100835                                                                             
GAGE_N50                         20378 COUNT: 38                                                                    
GAGE_Genome size                 2813862                                                                            
GAGE_Assembly size               2732236                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     68841(2.45%)                                                                       
GAGE_Missing assembly bases      28(0.00%)                                                                          
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  0                                                                                  
GAGE_Compressed reference bases  15863                                                                              
GAGE_Bad trim                    28                                                                                 
GAGE_Avg idy                     99.99                                                                              
GAGE_SNPs                        64                                                                                 
GAGE_Indels < 5bp                222                                                                                
GAGE_Indels >= 5                 2                                                                                  
GAGE_Inversions                  0                                                                                  
GAGE_Relocation                  1                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          318                                                                                
GAGE_Corrected assembly size     2732716                                                                            
GAGE_Min correct contig          206                                                                                
GAGE_Max correct contig          100840                                                                             
GAGE_Corrected N50               20265 COUNT: 38                                                                    
