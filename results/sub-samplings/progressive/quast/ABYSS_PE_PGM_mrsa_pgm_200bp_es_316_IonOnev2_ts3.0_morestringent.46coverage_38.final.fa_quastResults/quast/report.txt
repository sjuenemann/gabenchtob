All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.46coverage_38.final
#Contigs (>= 0 bp)             4441                                                                               
#Contigs (>= 1000 bp)          225                                                                                
Total length (>= 0 bp)         2968429                                                                            
Total length (>= 1000 bp)      2695310                                                                            
#Contigs                       315                                                                                
Largest contig                 100835                                                                             
Total length                   2732236                                                                            
Reference length               2813862                                                                            
GC (%)                         32.59                                                                              
Reference GC (%)               32.81                                                                              
N50                            21817                                                                              
NG50                           20378                                                                              
N75                            11243                                                                              
NG75                           10220                                                                              
#misassemblies                 1                                                                                  
#local misassemblies           2                                                                                  
#unaligned contigs             0 + 0 part                                                                         
Unaligned contigs length       0                                                                                  
Genome fraction (%)            96.859                                                                             
Duplication ratio              1.001                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        2.27                                                                               
#indels per 100 kbp            8.11                                                                               
#genes                         2497 + 129 part                                                                    
#predicted genes (unique)      2783                                                                               
#predicted genes (>= 0 bp)     2783                                                                               
#predicted genes (>= 300 bp)   2324                                                                               
#predicted genes (>= 1500 bp)  272                                                                                
#predicted genes (>= 3000 bp)  23                                                                                 
Largest alignment              100835                                                                             
NA50                           21817                                                                              
NGA50                          20378                                                                              
NA75                           11243                                                                              
NGA75                          10142                                                                              
