All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.86coverage.scf
GAGE_Contigs #                   71                                                                     
GAGE_Min contig                  1011                                                                   
GAGE_Max contig                  212011                                                                 
GAGE_N50                         87978 COUNT: 11                                                        
GAGE_Genome size                 2813862                                                                
GAGE_Assembly size               2747236                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     72893(2.59%)                                                           
GAGE_Missing assembly bases      62(0.00%)                                                              
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  0                                                                      
GAGE_Compressed reference bases  14377                                                                  
GAGE_Bad trim                    37                                                                     
GAGE_Avg idy                     99.99                                                                  
GAGE_SNPs                        46                                                                     
GAGE_Indels < 5bp                157                                                                    
GAGE_Indels >= 5                 5                                                                      
GAGE_Inversions                  0                                                                      
GAGE_Relocation                  1                                                                      
GAGE_Translocation               0                                                                      
GAGE_Corrected contig #          76                                                                     
GAGE_Corrected assembly size     2748162                                                                
GAGE_Min correct contig          917                                                                    
GAGE_Max correct contig          150497                                                                 
GAGE_Corrected N50               67305 COUNT: 14                                                        
