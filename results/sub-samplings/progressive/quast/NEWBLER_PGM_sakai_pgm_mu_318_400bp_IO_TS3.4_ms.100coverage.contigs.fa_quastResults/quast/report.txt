All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.100coverage.contigs
#Contigs (>= 0 bp)             544                                                               
#Contigs (>= 1000 bp)          142                                                               
Total length (>= 0 bp)         5386115                                                           
Total length (>= 1000 bp)      5287872                                                           
#Contigs                       278                                                               
Largest contig                 316796                                                            
Total length                   5348372                                                           
Reference length               5594470                                                           
GC (%)                         50.29                                                             
Reference GC (%)               50.48                                                             
N50                            142246                                                            
NG50                           123931                                                            
N75                            53934                                                             
NG75                           46458                                                             
#misassemblies                 5                                                                 
#local misassemblies           4                                                                 
#unaligned contigs             0 + 0 part                                                        
Unaligned contigs length       0                                                                 
Genome fraction (%)            94.030                                                            
Duplication ratio              1.001                                                             
#N's per 100 kbp               0.00                                                              
#mismatches per 100 kbp        1.20                                                              
#indels per 100 kbp            3.59                                                              
#genes                         4976 + 139 part                                                   
#predicted genes (unique)      5327                                                              
#predicted genes (>= 0 bp)     5330                                                              
#predicted genes (>= 300 bp)   4482                                                              
#predicted genes (>= 1500 bp)  661                                                               
#predicted genes (>= 3000 bp)  63                                                                
Largest alignment              313761                                                            
NA50                           142246                                                            
NGA50                          123931                                                            
NA75                           53934                                                             
NGA75                          46458                                                             
