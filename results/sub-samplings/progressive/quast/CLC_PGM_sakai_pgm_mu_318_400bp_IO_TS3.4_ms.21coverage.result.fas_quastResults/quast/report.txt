All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.21coverage.result
#Contigs (>= 0 bp)             772                                                         
#Contigs (>= 1000 bp)          301                                                         
Total length (>= 0 bp)         5374604                                                     
Total length (>= 1000 bp)      5196665                                                     
#Contigs                       772                                                         
Largest contig                 184795                                                      
Total length                   5374604                                                     
Reference length               5594470                                                     
GC (%)                         50.31                                                       
Reference GC (%)               50.48                                                       
N50                            37037                                                       
NG50                           34912                                                       
N75                            18312                                                       
NG75                           15713                                                       
#misassemblies                 3                                                           
#local misassemblies           11                                                          
#unaligned contigs             0 + 1 part                                                  
Unaligned contigs length       18                                                          
Genome fraction (%)            93.874                                                      
Duplication ratio              1.008                                                       
#N's per 100 kbp               1.77                                                        
#mismatches per 100 kbp        3.81                                                        
#indels per 100 kbp            21.71                                                       
#genes                         4803 + 305 part                                             
#predicted genes (unique)      5930                                                        
#predicted genes (>= 0 bp)     5935                                                        
#predicted genes (>= 300 bp)   4684                                                        
#predicted genes (>= 1500 bp)  579                                                         
#predicted genes (>= 3000 bp)  53                                                          
Largest alignment              184795                                                      
NA50                           35530                                                       
NGA50                          34490                                                       
NA75                           18312                                                       
NGA75                          15563                                                       
