All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.66coverage.scf
#Contigs                                   69                                                                     
#Contigs (>= 0 bp)                         69                                                                     
#Contigs (>= 1000 bp)                      69                                                                     
Largest contig                             194123                                                                 
Total length                               2747568                                                                
Total length (>= 0 bp)                     2747568                                                                
Total length (>= 1000 bp)                  2747568                                                                
Reference length                           2813862                                                                
N50                                        120795                                                                 
NG50                                       86280                                                                  
N75                                        52929                                                                  
NG75                                       43969                                                                  
L50                                        9                                                                      
LG50                                       10                                                                     
L75                                        19                                                                     
LG75                                       20                                                                     
#local misassemblies                       2                                                                      
#misassemblies                             2                                                                      
#misassembled contigs                      2                                                                      
Misassembled contigs length                203414                                                                 
Misassemblies inter-contig overlap         484                                                                    
#unaligned contigs                         0 + 0 part                                                             
Unaligned contigs length                   0                                                                      
#ambiguously mapped contigs                1                                                                      
Extra bases in ambiguously mapped contigs  -1216                                                                  
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        97.087                                                                 
Duplication ratio                          1.005                                                                  
#genes                                     2622 + 41 part                                                         
#predicted genes (unique)                  2669                                                                   
#predicted genes (>= 0 bp)                 2672                                                                   
#predicted genes (>= 300 bp)               2305                                                                   
#predicted genes (>= 1500 bp)              281                                                                    
#predicted genes (>= 3000 bp)              26                                                                     
#mismatches                                81                                                                     
#indels                                    211                                                                    
Indels length                              277                                                                    
#mismatches per 100 kbp                    2.96                                                                   
#indels per 100 kbp                        7.72                                                                   
GC (%)                                     32.63                                                                  
Reference GC (%)                           32.81                                                                  
Average %IDY                               98.365                                                                 
Largest alignment                          194123                                                                 
NA50                                       86280                                                                  
NGA50                                      86280                                                                  
NA75                                       52058                                                                  
NGA75                                      43969                                                                  
LA50                                       10                                                                     
LGA50                                      10                                                                     
LA75                                       20                                                                     
LGA75                                      21                                                                     
#Mis_misassemblies                         2                                                                      
#Mis_relocations                           2                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  2                                                                      
Mis_Misassembled contigs length            203414                                                                 
#Mis_local misassemblies                   2                                                                      
#Mis_short indels (<= 5 bp)                209                                                                    
#Mis_long indels (> 5 bp)                  2                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           0                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             0                                                                      
GAGE_Contigs #                             69                                                                     
GAGE_Min contig                            1119                                                                   
GAGE_Max contig                            194123                                                                 
GAGE_N50                                   86280 COUNT: 10                                                        
GAGE_Genome size                           2813862                                                                
GAGE_Assembly size                         2747568                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               72689(2.58%)                                                           
GAGE_Missing assembly bases                39(0.00%)                                                              
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            0                                                                      
GAGE_Compressed reference bases            13160                                                                  
GAGE_Bad trim                              39                                                                     
GAGE_Avg idy                               99.99                                                                  
GAGE_SNPs                                  40                                                                     
GAGE_Indels < 5bp                          184                                                                    
GAGE_Indels >= 5                           4                                                                      
GAGE_Inversions                            0                                                                      
GAGE_Relocation                            1                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    75                                                                     
GAGE_Corrected assembly size               2748404                                                                
GAGE_Min correct contig                    868                                                                    
GAGE_Max correct contig                    194134                                                                 
GAGE_Corrected N50                         85657 COUNT: 11                                                        
