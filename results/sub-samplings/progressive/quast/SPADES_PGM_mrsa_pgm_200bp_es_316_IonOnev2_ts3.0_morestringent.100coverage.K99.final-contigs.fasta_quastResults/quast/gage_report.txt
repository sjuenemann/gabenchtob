All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.100coverage.K99.final-contigs
GAGE_Contigs #                   109                                                                                        
GAGE_Min contig                  203                                                                                        
GAGE_Max contig                  236816                                                                                     
GAGE_N50                         114221 COUNT: 9                                                                            
GAGE_Genome size                 2813862                                                                                    
GAGE_Assembly size               2782009                                                                                    
GAGE_Chaff bases                 0                                                                                          
GAGE_Missing reference bases     3701(0.13%)                                                                                
GAGE_Missing assembly bases      200(0.01%)                                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                                   
GAGE_Duplicated reference bases  530                                                                                        
GAGE_Compressed reference bases  38659                                                                                      
GAGE_Bad trim                    200                                                                                        
GAGE_Avg idy                     99.98                                                                                      
GAGE_SNPs                        36                                                                                         
GAGE_Indels < 5bp                382                                                                                        
GAGE_Indels >= 5                 12                                                                                         
GAGE_Inversions                  1                                                                                          
GAGE_Relocation                  3                                                                                          
GAGE_Translocation               0                                                                                          
GAGE_Corrected contig #          124                                                                                        
GAGE_Corrected assembly size     2784421                                                                                    
GAGE_Min correct contig          203                                                                                        
GAGE_Max correct contig          207893                                                                                     
GAGE_Corrected N50               70541 COUNT: 12                                                                            
