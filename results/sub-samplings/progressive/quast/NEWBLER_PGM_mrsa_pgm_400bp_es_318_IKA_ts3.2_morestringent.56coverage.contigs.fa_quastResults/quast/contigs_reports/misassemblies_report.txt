All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.56coverage.contigs
#Mis_misassemblies               2                                                                           
#Mis_relocations                 2                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  0                                                                           
#Mis_misassembled contigs        2                                                                           
Mis_Misassembled contigs length  157701                                                                      
#Mis_local misassemblies         6                                                                           
#mismatches                      61                                                                          
#indels                          121                                                                         
#Mis_short indels (<= 5 bp)      120                                                                         
#Mis_long indels (> 5 bp)        1                                                                           
Indels length                    180                                                                         
