All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.11coverage_52.final
GAGE_Contigs #                   2618                                                                               
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  18446                                                                              
GAGE_N50                         1354 COUNT: 622                                                                    
GAGE_Genome size                 2813862                                                                            
GAGE_Assembly size               2653076                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     162863(5.79%)                                                                      
GAGE_Missing assembly bases      2741(0.10%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                           
GAGE_Duplicated reference bases  1144                                                                               
GAGE_Compressed reference bases  28164                                                                              
GAGE_Bad trim                    2741                                                                               
GAGE_Avg idy                     99.94                                                                              
GAGE_SNPs                        118                                                                                
GAGE_Indels < 5bp                1262                                                                               
GAGE_Indels >= 5                 2                                                                                  
GAGE_Inversions                  5                                                                                  
GAGE_Relocation                  3                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          2607                                                                               
GAGE_Corrected assembly size     2647847                                                                            
GAGE_Min correct contig          200                                                                                
GAGE_Max correct contig          18451                                                                              
GAGE_Corrected N50               1350 COUNT: 623                                                                    
