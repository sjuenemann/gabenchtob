All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.91coverage.scf
#Contigs (>= 0 bp)             464                                                                         
#Contigs (>= 1000 bp)          464                                                                         
Total length (>= 0 bp)         2673885                                                                     
Total length (>= 1000 bp)      2673885                                                                     
#Contigs                       464                                                                         
Largest contig                 64852                                                                       
Total length                   2673885                                                                     
Reference length               2813862                                                                     
GC (%)                         32.64                                                                       
Reference GC (%)               32.81                                                                       
N50                            10680                                                                       
NG50                           10053                                                                       
N75                            4783                                                                        
NG75                           4032                                                                        
#misassemblies                 9                                                                           
#local misassemblies           3                                                                           
#unaligned contigs             0 + 7 part                                                                  
Unaligned contigs length       454                                                                         
Genome fraction (%)            93.375                                                                      
Duplication ratio              1.018                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        6.47                                                                        
#indels per 100 kbp            18.34                                                                       
#genes                         2244 + 363 part                                                             
#predicted genes (unique)      2856                                                                        
#predicted genes (>= 0 bp)     2856                                                                        
#predicted genes (>= 300 bp)   2337                                                                        
#predicted genes (>= 1500 bp)  242                                                                         
#predicted genes (>= 3000 bp)  24                                                                          
Largest alignment              64852                                                                       
NA50                           10345                                                                       
NGA50                          9421                                                                        
NA75                           4627                                                                        
NGA75                          4032                                                                        
