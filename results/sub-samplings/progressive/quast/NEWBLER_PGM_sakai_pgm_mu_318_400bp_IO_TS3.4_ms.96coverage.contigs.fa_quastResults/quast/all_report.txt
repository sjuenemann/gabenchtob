All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.96coverage.contigs
#Contigs                                   300                                                              
#Contigs (>= 0 bp)                         543                                                              
#Contigs (>= 1000 bp)                      144                                                              
Largest contig                             377081                                                           
Total length                               5349568                                                          
Total length (>= 0 bp)                     5384137                                                          
Total length (>= 1000 bp)                  5278015                                                          
Reference length                           5594470                                                          
N50                                        111304                                                           
NG50                                       107993                                                           
N75                                        52670                                                            
NG75                                       44859                                                            
L50                                        14                                                               
LG50                                       15                                                               
L75                                        31                                                               
LG75                                       35                                                               
#local misassemblies                       6                                                                
#misassemblies                             9                                                                
#misassembled contigs                      9                                                                
Misassembled contigs length                314368                                                           
Misassemblies inter-contig overlap         1022                                                             
#unaligned contigs                         0 + 0 part                                                       
Unaligned contigs length                   0                                                                
#ambiguously mapped contigs                106                                                              
Extra bases in ambiguously mapped contigs  -87725                                                           
#N's                                       0                                                                
#N's per 100 kbp                           0.00                                                             
Genome fraction (%)                        93.994                                                           
Duplication ratio                          1.001                                                            
#genes                                     4959 + 146 part                                                  
#predicted genes (unique)                  5337                                                             
#predicted genes (>= 0 bp)                 5337                                                             
#predicted genes (>= 300 bp)               4484                                                             
#predicted genes (>= 1500 bp)              660                                                              
#predicted genes (>= 3000 bp)              64                                                               
#mismatches                                73                                                               
#indels                                    184                                                              
Indels length                              191                                                              
#mismatches per 100 kbp                    1.39                                                             
#indels per 100 kbp                        3.50                                                             
GC (%)                                     50.29                                                            
Reference GC (%)                           50.48                                                            
Average %IDY                               98.761                                                           
Largest alignment                          377081                                                           
NA50                                       111034                                                           
NGA50                                      107993                                                           
NA75                                       52670                                                            
NGA75                                      44859                                                            
LA50                                       15                                                               
LGA50                                      16                                                               
LA75                                       32                                                               
LGA75                                      36                                                               
#Mis_misassemblies                         9                                                                
#Mis_relocations                           7                                                                
#Mis_translocations                        2                                                                
#Mis_inversions                            0                                                                
#Mis_misassembled contigs                  9                                                                
Mis_Misassembled contigs length            314368                                                           
#Mis_local misassemblies                   6                                                                
#Mis_short indels (<= 5 bp)                184                                                              
#Mis_long indels (> 5 bp)                  0                                                                
#Una_fully unaligned contigs               0                                                                
Una_Fully unaligned length                 0                                                                
#Una_partially unaligned contigs           0                                                                
#Una_with misassembly                      0                                                                
#Una_both parts are significant            0                                                                
Una_Partially unaligned length             0                                                                
GAGE_Contigs #                             300                                                              
GAGE_Min contig                            200                                                              
GAGE_Max contig                            377081                                                           
GAGE_N50                                   107993 COUNT: 15                                                 
GAGE_Genome size                           5594470                                                          
GAGE_Assembly size                         5349568                                                          
GAGE_Chaff bases                           0                                                                
GAGE_Missing reference bases               34762(0.62%)                                                     
GAGE_Missing assembly bases                19(0.00%)                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                         
GAGE_Duplicated reference bases            6036                                                             
GAGE_Compressed reference bases            228935                                                           
GAGE_Bad trim                              19                                                               
GAGE_Avg idy                               99.99                                                            
GAGE_SNPs                                  76                                                               
GAGE_Indels < 5bp                          195                                                              
GAGE_Indels >= 5                           4                                                                
GAGE_Inversions                            0                                                                
GAGE_Relocation                            6                                                                
GAGE_Translocation                         0                                                                
GAGE_Corrected contig #                    291                                                              
GAGE_Corrected assembly size               5344683                                                          
GAGE_Min correct contig                    200                                                              
GAGE_Max correct contig                    224231                                                           
GAGE_Corrected N50                         99586 COUNT: 19                                                  
