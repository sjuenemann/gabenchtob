All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.76coverage.contigs
GAGE_Contigs #                   55                                                                                            
GAGE_Min contig                  205                                                                                           
GAGE_Max contig                  437573                                                                                        
GAGE_N50                         178578 COUNT: 5                                                                               
GAGE_Genome size                 2813862                                                                                       
GAGE_Assembly size               2781547                                                                                       
GAGE_Chaff bases                 0                                                                                             
GAGE_Missing reference bases     4373(0.16%)                                                                                   
GAGE_Missing assembly bases      5670(0.20%)                                                                                   
GAGE_Missing assembly contigs    2(3.64%)                                                                                      
GAGE_Duplicated reference bases  601                                                                                           
GAGE_Compressed reference bases  35767                                                                                         
GAGE_Bad trim                    1                                                                                             
GAGE_Avg idy                     99.99                                                                                         
GAGE_SNPs                        34                                                                                            
GAGE_Indels < 5bp                10                                                                                            
GAGE_Indels >= 5                 1                                                                                             
GAGE_Inversions                  0                                                                                             
GAGE_Relocation                  2                                                                                             
GAGE_Translocation               0                                                                                             
GAGE_Corrected contig #          54                                                                                            
GAGE_Corrected assembly size     2775714                                                                                       
GAGE_Min correct contig          205                                                                                           
GAGE_Max correct contig          437572                                                                                        
GAGE_Corrected N50               178577 COUNT: 5                                                                               
