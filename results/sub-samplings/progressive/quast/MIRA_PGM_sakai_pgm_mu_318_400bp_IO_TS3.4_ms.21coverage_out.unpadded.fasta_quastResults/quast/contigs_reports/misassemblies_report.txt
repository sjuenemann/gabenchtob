All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.21coverage_out.unpadded
#Mis_misassemblies               25                                                                 
#Mis_relocations                 23                                                                 
#Mis_translocations              2                                                                  
#Mis_inversions                  0                                                                  
#Mis_misassembled contigs        23                                                                 
Mis_Misassembled contigs length  720746                                                             
#Mis_local misassemblies         18                                                                 
#mismatches                      1169                                                               
#indels                          1975                                                               
#Mis_short indels (<= 5 bp)      1971                                                               
#Mis_long indels (> 5 bp)        4                                                                  
Indels length                    2047                                                               
