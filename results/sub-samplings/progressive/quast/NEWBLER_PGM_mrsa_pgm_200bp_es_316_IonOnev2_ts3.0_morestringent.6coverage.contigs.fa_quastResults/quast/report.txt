All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.6coverage.contigs
#Contigs (>= 0 bp)             3218                                                                            
#Contigs (>= 1000 bp)          770                                                                             
Total length (>= 0 bp)         2461869                                                                         
Total length (>= 1000 bp)      1279171                                                                         
#Contigs                       2944                                                                            
Largest contig                 9727                                                                            
Total length                   2422255                                                                         
Reference length               2813862                                                                         
GC (%)                         32.74                                                                           
Reference GC (%)               32.81                                                                           
N50                            1050                                                                            
NG50                           909                                                                             
N75                            636                                                                             
NG75                           448                                                                             
#misassemblies                 92                                                                              
#local misassemblies           6                                                                               
#unaligned contigs             4 + 82 part                                                                     
Unaligned contigs length       5859                                                                            
Genome fraction (%)            85.204                                                                          
Duplication ratio              1.005                                                                           
#N's per 100 kbp               0.00                                                                            
#mismatches per 100 kbp        21.15                                                                           
#indels per 100 kbp            114.70                                                                          
#genes                         861 + 1716 part                                                                 
#predicted genes (unique)      4746                                                                            
#predicted genes (>= 0 bp)     4746                                                                            
#predicted genes (>= 300 bp)   2640                                                                            
#predicted genes (>= 1500 bp)  41                                                                              
#predicted genes (>= 3000 bp)  0                                                                               
Largest alignment              9727                                                                            
NA50                           1032                                                                            
NGA50                          894                                                                             
NA75                           619                                                                             
NGA75                          431                                                                             
