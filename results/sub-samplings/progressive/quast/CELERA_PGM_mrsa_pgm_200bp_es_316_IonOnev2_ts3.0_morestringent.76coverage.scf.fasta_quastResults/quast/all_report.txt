All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.76coverage.scf
#Contigs                                   448                                                                         
#Contigs (>= 0 bp)                         448                                                                         
#Contigs (>= 1000 bp)                      448                                                                         
Largest contig                             56332                                                                       
Total length                               2707481                                                                     
Total length (>= 0 bp)                     2707481                                                                     
Total length (>= 1000 bp)                  2707481                                                                     
Reference length                           2813862                                                                     
N50                                        11195                                                                       
NG50                                       10797                                                                       
N75                                        5567                                                                        
NG75                                       4730                                                                        
L50                                        76                                                                          
LG50                                       80                                                                          
L75                                        158                                                                         
LG75                                       174                                                                         
#local misassemblies                       3                                                                           
#misassemblies                             8                                                                           
#misassembled contigs                      7                                                                           
Misassembled contigs length                27942                                                                       
Misassemblies inter-contig overlap         1209                                                                        
#unaligned contigs                         0 + 6 part                                                                  
Unaligned contigs length                   216                                                                         
#ambiguously mapped contigs                0                                                                           
Extra bases in ambiguously mapped contigs  0                                                                           
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        94.366                                                                      
Duplication ratio                          1.020                                                                       
#genes                                     2277 + 336 part                                                             
#predicted genes (unique)                  2886                                                                        
#predicted genes (>= 0 bp)                 2887                                                                        
#predicted genes (>= 300 bp)               2357                                                                        
#predicted genes (>= 1500 bp)              251                                                                         
#predicted genes (>= 3000 bp)              20                                                                          
#mismatches                                130                                                                         
#indels                                    449                                                                         
Indels length                              480                                                                         
#mismatches per 100 kbp                    4.90                                                                        
#indels per 100 kbp                        16.91                                                                       
GC (%)                                     32.62                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.387                                                                      
Largest alignment                          56332                                                                       
NA50                                       11195                                                                       
NGA50                                      10797                                                                       
NA75                                       5525                                                                        
NGA75                                      4728                                                                        
LA50                                       76                                                                          
LGA50                                      80                                                                          
LA75                                       159                                                                         
LGA75                                      174                                                                         
#Mis_misassemblies                         8                                                                           
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            5                                                                           
#Mis_misassembled contigs                  7                                                                           
Mis_Misassembled contigs length            27942                                                                       
#Mis_local misassemblies                   3                                                                           
#Mis_short indels (<= 5 bp)                448                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           6                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             216                                                                         
GAGE_Contigs #                             448                                                                         
GAGE_Min contig                            1005                                                                        
GAGE_Max contig                            56332                                                                       
GAGE_N50                                   10797 COUNT: 80                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2707481                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               144389(5.13%)                                                               
GAGE_Missing assembly bases                736(0.03%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            462                                                                         
GAGE_Compressed reference bases            19890                                                                       
GAGE_Bad trim                              731                                                                         
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  67                                                                          
GAGE_Indels < 5bp                          365                                                                         
GAGE_Indels >= 5                           3                                                                           
GAGE_Inversions                            2                                                                           
GAGE_Relocation                            1                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    454                                                                         
GAGE_Corrected assembly size               2707332                                                                     
GAGE_Min correct contig                    534                                                                         
GAGE_Max correct contig                    44785                                                                       
GAGE_Corrected N50                         10797 COUNT: 82                                                             
