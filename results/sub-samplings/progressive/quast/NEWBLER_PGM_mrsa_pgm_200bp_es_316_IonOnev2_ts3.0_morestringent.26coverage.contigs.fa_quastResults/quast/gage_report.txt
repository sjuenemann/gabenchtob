All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.26coverage.contigs
GAGE_Contigs #                   118                                                                              
GAGE_Min contig                  204                                                                              
GAGE_Max contig                  147326                                                                           
GAGE_N50                         53823 COUNT: 19                                                                  
GAGE_Genome size                 2813862                                                                          
GAGE_Assembly size               2764731                                                                          
GAGE_Chaff bases                 0                                                                                
GAGE_Missing reference bases     12197(0.43%)                                                                     
GAGE_Missing assembly bases      72(0.00%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                         
GAGE_Duplicated reference bases  308                                                                              
GAGE_Compressed reference bases  39833                                                                            
GAGE_Bad trim                    72                                                                               
GAGE_Avg idy                     99.98                                                                            
GAGE_SNPs                        37                                                                               
GAGE_Indels < 5bp                306                                                                              
GAGE_Indels >= 5                 9                                                                                
GAGE_Inversions                  2                                                                                
GAGE_Relocation                  4                                                                                
GAGE_Translocation               0                                                                                
GAGE_Corrected contig #          128                                                                              
GAGE_Corrected assembly size     2766273                                                                          
GAGE_Min correct contig          204                                                                              
GAGE_Max correct contig          147339                                                                           
GAGE_Corrected N50               43576 COUNT: 21                                                                  
