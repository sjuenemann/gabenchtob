All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.36coverage.result
GAGE_Contigs #                   709                                                         
GAGE_Min contig                  200                                                         
GAGE_Max contig                  143687                                                      
GAGE_N50                         39881 COUNT: 42                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5417523                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     27327(0.49%)                                                
GAGE_Missing assembly bases      1368(0.03%)                                                 
GAGE_Missing assembly contigs    3(0.42%)                                                    
GAGE_Duplicated reference bases  57788                                                       
GAGE_Compressed reference bases  244774                                                      
GAGE_Bad trim                    699                                                         
GAGE_Avg idy                     99.98                                                       
GAGE_SNPs                        100                                                         
GAGE_Indels < 5bp                741                                                         
GAGE_Indels >= 5                 7                                                           
GAGE_Inversions                  0                                                           
GAGE_Relocation                  5                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          528                                                         
GAGE_Corrected assembly size     5359799                                                     
GAGE_Min correct contig          200                                                         
GAGE_Max correct contig          143690                                                      
GAGE_Corrected N50               36307 COUNT: 46                                             
