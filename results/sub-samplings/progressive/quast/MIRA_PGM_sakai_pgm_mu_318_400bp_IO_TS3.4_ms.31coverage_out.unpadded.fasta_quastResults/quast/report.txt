All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.31coverage_out.unpadded
#Contigs (>= 0 bp)             358                                                                
#Contigs (>= 1000 bp)          191                                                                
Total length (>= 0 bp)         5608130                                                            
Total length (>= 1000 bp)      5519322                                                            
#Contigs                       346                                                                
Largest contig                 381705                                                             
Total length                   5606423                                                            
Reference length               5594470                                                            
GC (%)                         50.40                                                              
Reference GC (%)               50.48                                                              
N50                            141869                                                             
NG50                           141869                                                             
N75                            65769                                                              
NG75                           65769                                                              
#misassemblies                 28                                                                 
#local misassemblies           23                                                                 
#unaligned contigs             0 + 1 part                                                         
Unaligned contigs length       126                                                                
Genome fraction (%)            97.671                                                             
Duplication ratio              1.021                                                              
#N's per 100 kbp               0.37                                                               
#mismatches per 100 kbp        26.19                                                              
#indels per 100 kbp            27.84                                                              
#genes                         5212 + 153 part                                                    
#predicted genes (unique)      5894                                                               
#predicted genes (>= 0 bp)     5927                                                               
#predicted genes (>= 300 bp)   4847                                                               
#predicted genes (>= 1500 bp)  620                                                                
#predicted genes (>= 3000 bp)  51                                                                 
Largest alignment              357100                                                             
NA50                           125521                                                             
NGA50                          125521                                                             
NA75                           61831                                                              
NGA75                          61831                                                              
