All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.76coverage.contigs
#Contigs                                   322                                                                                                   
#Contigs (>= 0 bp)                         397                                                                                                   
#Contigs (>= 1000 bp)                      152                                                                                                   
Largest contig                             374758                                                                                                
Total length                               5343464                                                                                               
Total length (>= 0 bp)                     5353877                                                                                               
Total length (>= 1000 bp)                  5270601                                                                                               
Reference length                           5594470                                                                                               
N50                                        123768                                                                                                
NG50                                       116968                                                                                                
N75                                        50734                                                                                                 
NG75                                       43147                                                                                                 
L50                                        14                                                                                                    
LG50                                       15                                                                                                    
L75                                        31                                                                                                    
LG75                                       35                                                                                                    
#local misassemblies                       2                                                                                                     
#misassemblies                             3                                                                                                     
#misassembled contigs                      3                                                                                                     
Misassembled contigs length                420864                                                                                                
Misassemblies inter-contig overlap         314                                                                                                   
#unaligned contigs                         4 + 0 part                                                                                            
Unaligned contigs length                   6588                                                                                                  
#ambiguously mapped contigs                112                                                                                                   
Extra bases in ambiguously mapped contigs  -83836                                                                                                
#N's                                       0                                                                                                     
#N's per 100 kbp                           0.00                                                                                                  
Genome fraction (%)                        93.894                                                                                                
Duplication ratio                          1.000                                                                                                 
#genes                                     4956 + 158 part                                                                                       
#predicted genes (unique)                  5285                                                                                                  
#predicted genes (>= 0 bp)                 5285                                                                                                  
#predicted genes (>= 300 bp)               4429                                                                                                  
#predicted genes (>= 1500 bp)              664                                                                                                   
#predicted genes (>= 3000 bp)              67                                                                                                    
#mismatches                                64                                                                                                    
#indels                                    16                                                                                                    
Indels length                              16                                                                                                    
#mismatches per 100 kbp                    1.22                                                                                                  
#indels per 100 kbp                        0.30                                                                                                  
GC (%)                                     50.27                                                                                                 
Reference GC (%)                           50.48                                                                                                 
Average %IDY                               98.886                                                                                                
Largest alignment                          245000                                                                                                
NA50                                       123768                                                                                                
NGA50                                      116968                                                                                                
NA75                                       50734                                                                                                 
NGA75                                      43147                                                                                                 
LA50                                       15                                                                                                    
LGA50                                      16                                                                                                    
LA75                                       32                                                                                                    
LGA75                                      36                                                                                                    
#Mis_misassemblies                         3                                                                                                     
#Mis_relocations                           3                                                                                                     
#Mis_translocations                        0                                                                                                     
#Mis_inversions                            0                                                                                                     
#Mis_misassembled contigs                  3                                                                                                     
Mis_Misassembled contigs length            420864                                                                                                
#Mis_local misassemblies                   2                                                                                                     
#Mis_short indels (<= 5 bp)                16                                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                                     
#Una_fully unaligned contigs               4                                                                                                     
Una_Fully unaligned length                 6588                                                                                                  
#Una_partially unaligned contigs           0                                                                                                     
#Una_with misassembly                      0                                                                                                     
#Una_both parts are significant            0                                                                                                     
Una_Partially unaligned length             0                                                                                                     
GAGE_Contigs #                             322                                                                                                   
GAGE_Min contig                            200                                                                                                   
GAGE_Max contig                            374758                                                                                                
GAGE_N50                                   116968 COUNT: 15                                                                                      
GAGE_Genome size                           5594470                                                                                               
GAGE_Assembly size                         5343464                                                                                               
GAGE_Chaff bases                           0                                                                                                     
GAGE_Missing reference bases               48405(0.87%)                                                                                          
GAGE_Missing assembly bases                6596(0.12%)                                                                                           
GAGE_Missing assembly contigs              4(1.24%)                                                                                              
GAGE_Duplicated reference bases            205                                                                                                   
GAGE_Compressed reference bases            226525                                                                                                
GAGE_Bad trim                              8                                                                                                     
GAGE_Avg idy                               99.99                                                                                                 
GAGE_SNPs                                  101                                                                                                   
GAGE_Indels < 5bp                          17                                                                                                    
GAGE_Indels >= 5                           2                                                                                                     
GAGE_Inversions                            0                                                                                                     
GAGE_Relocation                            3                                                                                                     
GAGE_Translocation                         0                                                                                                     
GAGE_Corrected contig #                    322                                                                                                   
GAGE_Corrected assembly size               5336983                                                                                               
GAGE_Min correct contig                    200                                                                                                   
GAGE_Max correct contig                    245000                                                                                                
GAGE_Corrected N50                         116968 COUNT: 16                                                                                      
