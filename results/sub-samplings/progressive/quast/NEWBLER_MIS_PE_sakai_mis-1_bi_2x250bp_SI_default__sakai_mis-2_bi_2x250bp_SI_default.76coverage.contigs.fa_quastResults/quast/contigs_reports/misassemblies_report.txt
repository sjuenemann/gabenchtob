All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.76coverage.contigs
#Mis_misassemblies               3                                                                                                     
#Mis_relocations                 3                                                                                                     
#Mis_translocations              0                                                                                                     
#Mis_inversions                  0                                                                                                     
#Mis_misassembled contigs        3                                                                                                     
Mis_Misassembled contigs length  420864                                                                                                
#Mis_local misassemblies         2                                                                                                     
#mismatches                      64                                                                                                    
#indels                          16                                                                                                    
#Mis_short indels (<= 5 bp)      16                                                                                                    
#Mis_long indels (> 5 bp)        0                                                                                                     
Indels length                    16                                                                                                    
