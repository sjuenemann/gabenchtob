All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.51coverage_out.unpadded
#Contigs (>= 0 bp)             582                                                                           
#Contigs (>= 1000 bp)          182                                                                           
Total length (>= 0 bp)         5629442                                                                       
Total length (>= 1000 bp)      5466599                                                                       
#Contigs                       558                                                                           
Largest contig                 284713                                                                        
Total length                   5625272                                                                       
Reference length               5594470                                                                       
GC (%)                         50.40                                                                         
Reference GC (%)               50.48                                                                         
N50                            95837                                                                         
NG50                           96216                                                                         
N75                            47193                                                                         
NG75                           49164                                                                         
#misassemblies                 71                                                                            
#local misassemblies           10                                                                            
#unaligned contigs             6 + 167 part                                                                  
Unaligned contigs length       10230                                                                         
Genome fraction (%)            97.375                                                                        
Duplication ratio              1.025                                                                         
#N's per 100 kbp               6.12                                                                          
#mismatches per 100 kbp        22.48                                                                         
#indels per 100 kbp            7.67                                                                          
#genes                         5199 + 160 part                                                               
#predicted genes (unique)      5842                                                                          
#predicted genes (>= 0 bp)     5861                                                                          
#predicted genes (>= 300 bp)   4727                                                                          
#predicted genes (>= 1500 bp)  664                                                                           
#predicted genes (>= 3000 bp)  66                                                                            
Largest alignment              284684                                                                        
NA50                           91508                                                                         
NGA50                          93981                                                                         
NA75                           36961                                                                         
NGA75                          42218                                                                         
