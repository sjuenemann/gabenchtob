All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.86coverage_126.final
#Mis_misassemblies               4                                                                              
#Mis_relocations                 4                                                                              
#Mis_translocations              0                                                                              
#Mis_inversions                  0                                                                              
#Mis_misassembled contigs        4                                                                              
Mis_Misassembled contigs length  28812                                                                          
#Mis_local misassemblies         1                                                                              
#mismatches                      56                                                                             
#indels                          1701                                                                           
#Mis_short indels (<= 5 bp)      1700                                                                           
#Mis_long indels (> 5 bp)        1                                                                              
Indels length                    1791                                                                           
