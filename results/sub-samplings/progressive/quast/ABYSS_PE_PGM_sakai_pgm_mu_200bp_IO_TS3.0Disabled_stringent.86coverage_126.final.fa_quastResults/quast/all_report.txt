All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.86coverage_126.final
#Contigs                                   4296                                                                           
#Contigs (>= 0 bp)                         5790                                                                           
#Contigs (>= 1000 bp)                      923                                                                            
Largest contig                             34355                                                                          
Total length                               6012724                                                                        
Total length (>= 0 bp)                     6247204                                                                        
Total length (>= 1000 bp)                  5057977                                                                        
Reference length                           5594470                                                                        
N50                                        6640                                                                           
NG50                                       7076                                                                           
N75                                        2755                                                                           
NG75                                       3686                                                                           
L50                                        277                                                                            
LG50                                       247                                                                            
L75                                        608                                                                            
LG75                                       509                                                                            
#local misassemblies                       1                                                                              
#misassemblies                             4                                                                              
#misassembled contigs                      4                                                                              
Misassembled contigs length                28812                                                                          
Misassemblies inter-contig overlap         368                                                                            
#unaligned contigs                         0 + 3 part                                                                     
Unaligned contigs length                   156                                                                            
#ambiguously mapped contigs                718                                                                            
Extra bases in ambiguously mapped contigs  -207084                                                                        
#N's                                       0                                                                              
#N's per 100 kbp                           0.00                                                                           
Genome fraction (%)                        94.800                                                                         
Duplication ratio                          1.095                                                                          
#genes                                     4175 + 1018 part                                                               
#predicted genes (unique)                  9474                                                                           
#predicted genes (>= 0 bp)                 9574                                                                           
#predicted genes (>= 300 bp)               5028                                                                           
#predicted genes (>= 1500 bp)              438                                                                            
#predicted genes (>= 3000 bp)              28                                                                             
#mismatches                                56                                                                             
#indels                                    1701                                                                           
Indels length                              1791                                                                           
#mismatches per 100 kbp                    1.06                                                                           
#indels per 100 kbp                        32.07                                                                          
GC (%)                                     50.54                                                                          
Reference GC (%)                           50.48                                                                          
Average %IDY                               99.186                                                                         
Largest alignment                          34355                                                                          
NA50                                       6640                                                                           
NGA50                                      7076                                                                           
NA75                                       2755                                                                           
NGA75                                      3673                                                                           
LA50                                       277                                                                            
LGA50                                      247                                                                            
LA75                                       608                                                                            
LGA75                                      510                                                                            
#Mis_misassemblies                         4                                                                              
#Mis_relocations                           4                                                                              
#Mis_translocations                        0                                                                              
#Mis_inversions                            0                                                                              
#Mis_misassembled contigs                  4                                                                              
Mis_Misassembled contigs length            28812                                                                          
#Mis_local misassemblies                   1                                                                              
#Mis_short indels (<= 5 bp)                1700                                                                           
#Mis_long indels (> 5 bp)                  1                                                                              
#Una_fully unaligned contigs               0                                                                              
Una_Fully unaligned length                 0                                                                              
#Una_partially unaligned contigs           3                                                                              
#Una_with misassembly                      0                                                                              
#Una_both parts are significant            0                                                                              
Una_Partially unaligned length             156                                                                            
GAGE_Contigs #                             4296                                                                           
GAGE_Min contig                            200                                                                            
GAGE_Max contig                            34355                                                                          
GAGE_N50                                   7076 COUNT: 247                                                                
GAGE_Genome size                           5594470                                                                        
GAGE_Assembly size                         6012724                                                                        
GAGE_Chaff bases                           0                                                                              
GAGE_Missing reference bases               10652(0.19%)                                                                   
GAGE_Missing assembly bases                182(0.00%)                                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                                       
GAGE_Duplicated reference bases            441571                                                                         
GAGE_Compressed reference bases            262140                                                                         
GAGE_Bad trim                              182                                                                            
GAGE_Avg idy                               99.97                                                                          
GAGE_SNPs                                  53                                                                             
GAGE_Indels < 5bp                          1616                                                                           
GAGE_Indels >= 5                           2                                                                              
GAGE_Inversions                            0                                                                              
GAGE_Relocation                            2                                                                              
GAGE_Translocation                         0                                                                              
GAGE_Corrected contig #                    2511                                                                           
GAGE_Corrected assembly size               5571741                                                                        
GAGE_Min correct contig                    200                                                                            
GAGE_Max correct contig                    34362                                                                          
GAGE_Corrected N50                         7077 COUNT: 247                                                                
