All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CELERA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.26coverage.scf_broken  CELERA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.26coverage.scf
#Una_fully unaligned contigs      1                                                                                                 1                                                                                        
Una_Fully unaligned length        5558                                                                                              5558                                                                                     
#Una_partially unaligned contigs  0                                                                                                 0                                                                                        
#Una_with misassembly             0                                                                                                 0                                                                                        
#Una_both parts are significant   0                                                                                                 0                                                                                        
Una_Partially unaligned length    0                                                                                                 0                                                                                        
#N's                              0                                                                                                 569                                                                                      
