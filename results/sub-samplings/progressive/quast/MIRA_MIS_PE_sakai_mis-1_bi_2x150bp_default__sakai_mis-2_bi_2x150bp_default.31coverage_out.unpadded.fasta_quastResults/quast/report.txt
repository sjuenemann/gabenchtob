All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.31coverage_out.unpadded
#Contigs (>= 0 bp)             292                                                                                               
#Contigs (>= 1000 bp)          153                                                                                               
Total length (>= 0 bp)         5578352                                                                                           
Total length (>= 1000 bp)      5513638                                                                                           
#Contigs                       288                                                                                               
Largest contig                 359878                                                                                            
Total length                   5577741                                                                                           
Reference length               5594470                                                                                           
GC (%)                         50.44                                                                                             
Reference GC (%)               50.48                                                                                             
N50                            99545                                                                                             
NG50                           99545                                                                                             
N75                            43652                                                                                             
NG75                           43652                                                                                             
#misassemblies                 66                                                                                                
#local misassemblies           6                                                                                                 
#unaligned contigs             2 + 2 part                                                                                        
Unaligned contigs length       6241                                                                                              
Genome fraction (%)            98.435                                                                                            
Duplication ratio              1.012                                                                                             
#N's per 100 kbp               0.56                                                                                              
#mismatches per 100 kbp        44.56                                                                                             
#indels per 100 kbp            1.78                                                                                              
#genes                         5267 + 114 part                                                                                   
#predicted genes (unique)      5464                                                                                              
#predicted genes (>= 0 bp)     5508                                                                                              
#predicted genes (>= 300 bp)   4640                                                                                              
#predicted genes (>= 1500 bp)  688                                                                                               
#predicted genes (>= 3000 bp)  72                                                                                                
Largest alignment              353740                                                                                            
NA50                           78682                                                                                             
NGA50                          76793                                                                                             
NA75                           36175                                                                                             
NGA75                          36175                                                                                             
