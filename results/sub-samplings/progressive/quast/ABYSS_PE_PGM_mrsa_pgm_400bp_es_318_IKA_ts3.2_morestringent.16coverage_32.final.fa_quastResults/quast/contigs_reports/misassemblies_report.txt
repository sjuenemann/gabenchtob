All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.16coverage_32.final
#Mis_misassemblies               0                                                                             
#Mis_relocations                 0                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        0                                                                             
Mis_Misassembled contigs length  0                                                                             
#Mis_local misassemblies         2                                                                             
#mismatches                      51                                                                            
#indels                          413                                                                           
#Mis_short indels (<= 5 bp)      413                                                                           
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    415                                                                           
