All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.16coverage_32.final
#Contigs (>= 0 bp)             5399                                                                          
#Contigs (>= 1000 bp)          722                                                                           
Total length (>= 0 bp)         2897588                                                                       
Total length (>= 1000 bp)      2393996                                                                       
#Contigs                       1269                                                                          
Largest contig                 24275                                                                         
Total length                   2669702                                                                       
Reference length               2813862                                                                       
GC (%)                         32.66                                                                         
Reference GC (%)               32.81                                                                         
N50                            3872                                                                          
NG50                           3580                                                                          
N75                            1994                                                                          
NG75                           1701                                                                          
#misassemblies                 0                                                                             
#local misassemblies           2                                                                             
#unaligned contigs             0 + 3 part                                                                    
Unaligned contigs length       98                                                                            
Genome fraction (%)            94.493                                                                        
Duplication ratio              1.003                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        1.92                                                                          
#indels per 100 kbp            15.53                                                                         
#genes                         1867 + 731 part                                                               
#predicted genes (unique)      3347                                                                          
#predicted genes (>= 0 bp)     3347                                                                          
#predicted genes (>= 300 bp)   2450                                                                          
#predicted genes (>= 1500 bp)  200                                                                           
#predicted genes (>= 3000 bp)  18                                                                            
Largest alignment              24275                                                                         
NA50                           3872                                                                          
NGA50                          3580                                                                          
NA75                           1994                                                                          
NGA75                          1701                                                                          
