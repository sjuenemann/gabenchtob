All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.91coverage.K99.final-contigs
#Mis_misassemblies               3                                                                                         
#Mis_relocations                 3                                                                                         
#Mis_translocations              0                                                                                         
#Mis_inversions                  0                                                                                         
#Mis_misassembled contigs        3                                                                                         
Mis_Misassembled contigs length  80397                                                                                     
#Mis_local misassemblies         14                                                                                        
#mismatches                      150                                                                                       
#indels                          376                                                                                       
#Mis_short indels (<= 5 bp)      373                                                                                       
#Mis_long indels (> 5 bp)        3                                                                                         
Indels length                    459                                                                                       
