All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.1coverage.scf
#Contigs (>= 0 bp)             7                                                                     
#Contigs (>= 1000 bp)          7                                                                     
Total length (>= 0 bp)         9905                                                                  
Total length (>= 1000 bp)      9905                                                                  
#Contigs                       7                                                                     
Largest contig                 1744                                                                  
Total length                   9905                                                                  
Reference length               5594470                                                               
GC (%)                         52.62                                                                 
Reference GC (%)               50.48                                                                 
N50                            1391                                                                  
NG50                           None                                                                  
N75                            1344                                                                  
NG75                           None                                                                  
#misassemblies                 0                                                                     
#local misassemblies           0                                                                     
#unaligned contigs             0 + 0 part                                                            
Unaligned contigs length       0                                                                     
Genome fraction (%)            0.027                                                                 
Duplication ratio              1.002                                                                 
#N's per 100 kbp               0.00                                                                  
#mismatches per 100 kbp        0.00                                                                  
#indels per 100 kbp            0.00                                                                  
#genes                         2 + 1 part                                                            
#predicted genes (unique)      14                                                                    
#predicted genes (>= 0 bp)     14                                                                    
#predicted genes (>= 300 bp)   13                                                                    
#predicted genes (>= 1500 bp)  0                                                                     
#predicted genes (>= 3000 bp)  0                                                                     
Largest alignment              1494                                                                  
NA50                           None                                                                  
NGA50                          None                                                                  
NA75                           None                                                                  
NGA75                          None                                                                  
