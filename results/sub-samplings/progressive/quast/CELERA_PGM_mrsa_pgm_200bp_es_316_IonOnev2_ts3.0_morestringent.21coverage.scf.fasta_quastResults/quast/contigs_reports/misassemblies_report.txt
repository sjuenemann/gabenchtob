All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.21coverage.scf
#Mis_misassemblies               4                                                                           
#Mis_relocations                 2                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  2                                                                           
#Mis_misassembled contigs        4                                                                           
Mis_Misassembled contigs length  9936                                                                        
#Mis_local misassemblies         4                                                                           
#mismatches                      127                                                                         
#indels                          659                                                                         
#Mis_short indels (<= 5 bp)      658                                                                         
#Mis_long indels (> 5 bp)        1                                                                           
Indels length                    700                                                                         
