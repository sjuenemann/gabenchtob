All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.21coverage.scf
#Contigs                                   410                                                                         
#Contigs (>= 0 bp)                         410                                                                         
#Contigs (>= 1000 bp)                      410                                                                         
Largest contig                             46961                                                                       
Total length                               2740439                                                                     
Total length (>= 0 bp)                     2740439                                                                     
Total length (>= 1000 bp)                  2740439                                                                     
Reference length                           2813862                                                                     
N50                                        10620                                                                       
NG50                                       10219                                                                       
N75                                        5692                                                                        
NG75                                       5438                                                                        
L50                                        82                                                                          
LG50                                       86                                                                          
L75                                        169                                                                         
LG75                                       179                                                                         
#local misassemblies                       4                                                                           
#misassemblies                             4                                                                           
#misassembled contigs                      4                                                                           
Misassembled contigs length                9936                                                                        
Misassemblies inter-contig overlap         811                                                                         
#unaligned contigs                         0 + 2 part                                                                  
Unaligned contigs length                   80                                                                          
#ambiguously mapped contigs                0                                                                           
Extra bases in ambiguously mapped contigs  0                                                                           
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        95.678                                                                      
Duplication ratio                          1.018                                                                       
#genes                                     2356 + 275 part                                                             
#predicted genes (unique)                  2945                                                                        
#predicted genes (>= 0 bp)                 2945                                                                        
#predicted genes (>= 300 bp)               2387                                                                        
#predicted genes (>= 1500 bp)              253                                                                         
#predicted genes (>= 3000 bp)              19                                                                          
#mismatches                                127                                                                         
#indels                                    659                                                                         
Indels length                              700                                                                         
#mismatches per 100 kbp                    4.72                                                                        
#indels per 100 kbp                        24.48                                                                       
GC (%)                                     32.61                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.271                                                                      
Largest alignment                          46961                                                                       
NA50                                       10620                                                                       
NGA50                                      10219                                                                       
NA75                                       5692                                                                        
NGA75                                      5435                                                                        
LA50                                       82                                                                          
LGA50                                      86                                                                          
LA75                                       169                                                                         
LGA75                                      179                                                                         
#Mis_misassemblies                         4                                                                           
#Mis_relocations                           2                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            2                                                                           
#Mis_misassembled contigs                  4                                                                           
Mis_Misassembled contigs length            9936                                                                        
#Mis_local misassemblies                   4                                                                           
#Mis_short indels (<= 5 bp)                658                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           2                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             80                                                                          
GAGE_Contigs #                             410                                                                         
GAGE_Min contig                            1016                                                                        
GAGE_Max contig                            46961                                                                       
GAGE_N50                                   10219 COUNT: 86                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2740439                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               111782(3.97%)                                                               
GAGE_Missing assembly bases                400(0.01%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            392                                                                         
GAGE_Compressed reference bases            14852                                                                       
GAGE_Bad trim                              400                                                                         
GAGE_Avg idy                               99.96                                                                       
GAGE_SNPs                                  56                                                                          
GAGE_Indels < 5bp                          505                                                                         
GAGE_Indels >= 5                           5                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            1                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    414                                                                         
GAGE_Corrected assembly size               2740796                                                                     
GAGE_Min correct contig                    524                                                                         
GAGE_Max correct contig                    46962                                                                       
GAGE_Corrected N50                         10044 COUNT: 87                                                             
