All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.26coverage.scf
#Contigs (>= 0 bp)             374                                                                         
#Contigs (>= 1000 bp)          374                                                                         
Total length (>= 0 bp)         2740530                                                                     
Total length (>= 1000 bp)      2740530                                                                     
#Contigs                       374                                                                         
Largest contig                 69260                                                                       
Total length                   2740530                                                                     
Reference length               2813862                                                                     
GC (%)                         32.63                                                                       
Reference GC (%)               32.81                                                                       
N50                            11617                                                                       
NG50                           11488                                                                       
N75                            6246                                                                        
NG75                           5684                                                                        
#misassemblies                 5                                                                           
#local misassemblies           7                                                                           
#unaligned contigs             0 + 0 part                                                                  
Unaligned contigs length       0                                                                           
Genome fraction (%)            95.761                                                                      
Duplication ratio              1.018                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        4.86                                                                        
#indels per 100 kbp            21.90                                                                       
#genes                         2387 + 254 part                                                             
#predicted genes (unique)      2897                                                                        
#predicted genes (>= 0 bp)     2897                                                                        
#predicted genes (>= 300 bp)   2377                                                                        
#predicted genes (>= 1500 bp)  249                                                                         
#predicted genes (>= 3000 bp)  19                                                                          
Largest alignment              69260                                                                       
NA50                           11610                                                                       
NGA50                          11333                                                                       
NA75                           6246                                                                        
NGA75                          5684                                                                        
