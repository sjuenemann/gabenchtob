All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.36coverage.scf
#Contigs                                   424                                                                         
#Contigs (>= 0 bp)                         424                                                                         
#Contigs (>= 1000 bp)                      424                                                                         
Largest contig                             48300                                                                       
Total length                               2740092                                                                     
Total length (>= 0 bp)                     2740092                                                                     
Total length (>= 1000 bp)                  2740092                                                                     
Reference length                           2813862                                                                     
N50                                        10897                                                                       
NG50                                       10315                                                                       
N75                                        5270                                                                        
NG75                                       4804                                                                        
L50                                        75                                                                          
LG50                                       78                                                                          
L75                                        162                                                                         
LG75                                       174                                                                         
#local misassemblies                       2                                                                           
#misassemblies                             3                                                                           
#misassembled contigs                      3                                                                           
Misassembled contigs length                13855                                                                       
Misassemblies inter-contig overlap         423                                                                         
#unaligned contigs                         0 + 2 part                                                                  
Unaligned contigs length                   76                                                                          
#ambiguously mapped contigs                0                                                                           
Extra bases in ambiguously mapped contigs  0                                                                           
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        95.409                                                                      
Duplication ratio                          1.021                                                                       
#genes                                     2326 + 304 part                                                             
#predicted genes (unique)                  2932                                                                        
#predicted genes (>= 0 bp)                 2932                                                                        
#predicted genes (>= 300 bp)               2371                                                                        
#predicted genes (>= 1500 bp)              259                                                                         
#predicted genes (>= 3000 bp)              20                                                                          
#mismatches                                119                                                                         
#indels                                    531                                                                         
Indels length                              573                                                                         
#mismatches per 100 kbp                    4.43                                                                        
#indels per 100 kbp                        19.78                                                                       
GC (%)                                     32.60                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.254                                                                      
Largest alignment                          48300                                                                       
NA50                                       10807                                                                       
NGA50                                      10240                                                                       
NA75                                       5195                                                                        
NGA75                                      4804                                                                        
LA50                                       75                                                                          
LGA50                                      78                                                                          
LA75                                       163                                                                         
LGA75                                      174                                                                         
#Mis_misassemblies                         3                                                                           
#Mis_relocations                           2                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            1                                                                           
#Mis_misassembled contigs                  3                                                                           
Mis_Misassembled contigs length            13855                                                                       
#Mis_local misassemblies                   2                                                                           
#Mis_short indels (<= 5 bp)                530                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           2                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             76                                                                          
GAGE_Contigs #                             424                                                                         
GAGE_Min contig                            1003                                                                        
GAGE_Max contig                            48300                                                                       
GAGE_N50                                   10315 COUNT: 78                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2740092                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               118746(4.22%)                                                               
GAGE_Missing assembly bases                695(0.03%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            221                                                                         
GAGE_Compressed reference bases            15005                                                                       
GAGE_Bad trim                              695                                                                         
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  89                                                                          
GAGE_Indels < 5bp                          428                                                                         
GAGE_Indels >= 5                           2                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            1                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    427                                                                         
GAGE_Corrected assembly size               2739907                                                                     
GAGE_Min correct contig                    1004                                                                        
GAGE_Max correct contig                    48316                                                                       
GAGE_Corrected N50                         10241 COUNT: 78                                                             
