All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.66coverage.contigs
GAGE_Contigs #                   327                                                                                                   
GAGE_Min contig                  200                                                                                                   
GAGE_Max contig                  260205                                                                                                
GAGE_N50                         116968 COUNT: 16                                                                                      
GAGE_Genome size                 5594470                                                                                               
GAGE_Assembly size               5342508                                                                                               
GAGE_Chaff bases                 0                                                                                                     
GAGE_Missing reference bases     42463(0.76%)                                                                                          
GAGE_Missing assembly bases      6595(0.12%)                                                                                           
GAGE_Missing assembly contigs    4(1.22%)                                                                                              
GAGE_Duplicated reference bases  215                                                                                                   
GAGE_Compressed reference bases  235452                                                                                                
GAGE_Bad trim                    7                                                                                                     
GAGE_Avg idy                     99.99                                                                                                 
GAGE_SNPs                        136                                                                                                   
GAGE_Indels < 5bp                18                                                                                                    
GAGE_Indels >= 5                 2                                                                                                     
GAGE_Inversions                  0                                                                                                     
GAGE_Relocation                  4                                                                                                     
GAGE_Translocation               0                                                                                                     
GAGE_Corrected contig #          328                                                                                                   
GAGE_Corrected assembly size     5336367                                                                                               
GAGE_Min correct contig          200                                                                                                   
GAGE_Max correct contig          260205                                                                                                
GAGE_Corrected N50               99583 COUNT: 17                                                                                       
