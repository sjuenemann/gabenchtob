All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.76coverage.K99.final-contigs
GAGE_Contigs #                   433                                                                                  
GAGE_Min contig                  200                                                                                  
GAGE_Max contig                  374868                                                                               
GAGE_N50                         124320 COUNT: 15                                                                     
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5379900                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     9311(0.17%)                                                                          
GAGE_Missing assembly bases      1(0.00%)                                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  1321                                                                                 
GAGE_Compressed reference bases  273150                                                                               
GAGE_Bad trim                    1                                                                                    
GAGE_Avg idy                     99.97                                                                                
GAGE_SNPs                        161                                                                                  
GAGE_Indels < 5bp                1463                                                                                 
GAGE_Indels >= 5                 9                                                                                    
GAGE_Inversions                  0                                                                                    
GAGE_Relocation                  6                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          443                                                                                  
GAGE_Corrected assembly size     5382134                                                                              
GAGE_Min correct contig          200                                                                                  
GAGE_Max correct contig          226975                                                                               
GAGE_Corrected N50               116683 COUNT: 18                                                                     
