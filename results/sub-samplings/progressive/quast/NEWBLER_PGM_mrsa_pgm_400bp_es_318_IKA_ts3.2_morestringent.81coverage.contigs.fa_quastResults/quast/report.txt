All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.81coverage.contigs
#Contigs (>= 0 bp)             119                                                                         
#Contigs (>= 1000 bp)          38                                                                          
Total length (>= 0 bp)         2785198                                                                     
Total length (>= 1000 bp)      2766442                                                                     
#Contigs                       59                                                                          
Largest contig                 295346                                                                      
Total length                   2776093                                                                     
Reference length               2813862                                                                     
GC (%)                         32.65                                                                       
Reference GC (%)               32.81                                                                       
N50                            150156                                                                      
NG50                           150156                                                                      
N75                            109109                                                                      
NG75                           109109                                                                      
#misassemblies                 2                                                                           
#local misassemblies           5                                                                           
#unaligned contigs             0 + 0 part                                                                  
Unaligned contigs length       0                                                                           
Genome fraction (%)            98.348                                                                      
Duplication ratio              1.000                                                                       
#N's per 100 kbp               0.00                                                                        
#mismatches per 100 kbp        2.31                                                                        
#indels per 100 kbp            6.03                                                                        
#genes                         2668 + 30 part                                                              
#predicted genes (unique)      2666                                                                        
#predicted genes (>= 0 bp)     2668                                                                        
#predicted genes (>= 300 bp)   2304                                                                        
#predicted genes (>= 1500 bp)  291                                                                         
#predicted genes (>= 3000 bp)  27                                                                          
Largest alignment              295346                                                                      
NA50                           150131                                                                      
NGA50                          150131                                                                      
NA75                           97606                                                                       
NGA75                          81954                                                                       
