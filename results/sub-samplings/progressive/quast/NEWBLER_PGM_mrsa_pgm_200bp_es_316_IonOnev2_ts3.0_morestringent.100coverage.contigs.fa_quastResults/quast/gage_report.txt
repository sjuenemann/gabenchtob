All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.100coverage.contigs
GAGE_Contigs #                   212                                                                               
GAGE_Min contig                  202                                                                               
GAGE_Max contig                  120023                                                                            
GAGE_N50                         26469 COUNT: 35                                                                   
GAGE_Genome size                 2813862                                                                           
GAGE_Assembly size               2756000                                                                           
GAGE_Chaff bases                 0                                                                                 
GAGE_Missing reference bases     24879(0.88%)                                                                      
GAGE_Missing assembly bases      310(0.01%)                                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                                          
GAGE_Duplicated reference bases  658                                                                               
GAGE_Compressed reference bases  34946                                                                             
GAGE_Bad trim                    310                                                                               
GAGE_Avg idy                     99.98                                                                             
GAGE_SNPs                        44                                                                                
GAGE_Indels < 5bp                345                                                                               
GAGE_Indels >= 5                 6                                                                                 
GAGE_Inversions                  1                                                                                 
GAGE_Relocation                  1                                                                                 
GAGE_Translocation               0                                                                                 
GAGE_Corrected contig #          212                                                                               
GAGE_Corrected assembly size     2755923                                                                           
GAGE_Min correct contig          203                                                                               
GAGE_Max correct contig          120030                                                                            
GAGE_Corrected N50               26358 COUNT: 36                                                                   
