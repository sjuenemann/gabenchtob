All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.100coverage.contigs
#Contigs                                   212                                                                               
#Contigs (>= 0 bp)                         387                                                                               
#Contigs (>= 1000 bp)                      165                                                                               
Largest contig                             120023                                                                            
Total length                               2756000                                                                           
Total length (>= 0 bp)                     2778527                                                                           
Total length (>= 1000 bp)                  2737181                                                                           
Reference length                           2813862                                                                           
N50                                        26995                                                                             
NG50                                       26469                                                                             
N75                                        15164                                                                             
NG75                                       14508                                                                             
L50                                        34                                                                                
LG50                                       35                                                                                
L75                                        66                                                                                
LG75                                       69                                                                                
#local misassemblies                       4                                                                                 
#misassemblies                             5                                                                                 
#misassembled contigs                      4                                                                                 
Misassembled contigs length                11579                                                                             
Misassemblies inter-contig overlap         961                                                                               
#unaligned contigs                         0 + 5 part                                                                        
Unaligned contigs length                   397                                                                               
#ambiguously mapped contigs                12                                                                                
Extra bases in ambiguously mapped contigs  -8136                                                                             
#N's                                       0                                                                                 
#N's per 100 kbp                           0.00                                                                              
Genome fraction (%)                        97.645                                                                            
Duplication ratio                          1.000                                                                             
#genes                                     2571 + 125 part                                                                   
#predicted genes (unique)                  2772                                                                              
#predicted genes (>= 0 bp)                 2772                                                                              
#predicted genes (>= 300 bp)               2344                                                                              
#predicted genes (>= 1500 bp)              274                                                                               
#predicted genes (>= 3000 bp)              22                                                                                
#mismatches                                59                                                                                
#indels                                    311                                                                               
Indels length                              468                                                                               
#mismatches per 100 kbp                    2.15                                                                              
#indels per 100 kbp                        11.32                                                                             
GC (%)                                     32.65                                                                             
Reference GC (%)                           32.81                                                                             
Average %IDY                               99.283                                                                            
Largest alignment                          120023                                                                            
NA50                                       26995                                                                             
NGA50                                      26469                                                                             
NA75                                       15164                                                                             
NGA75                                      14508                                                                             
LA50                                       34                                                                                
LGA50                                      35                                                                                
LA75                                       66                                                                                
LGA75                                      69                                                                                
#Mis_misassemblies                         5                                                                                 
#Mis_relocations                           1                                                                                 
#Mis_translocations                        2                                                                                 
#Mis_inversions                            2                                                                                 
#Mis_misassembled contigs                  4                                                                                 
Mis_Misassembled contigs length            11579                                                                             
#Mis_local misassemblies                   4                                                                                 
#Mis_short indels (<= 5 bp)                309                                                                               
#Mis_long indels (> 5 bp)                  2                                                                                 
#Una_fully unaligned contigs               0                                                                                 
Una_Fully unaligned length                 0                                                                                 
#Una_partially unaligned contigs           5                                                                                 
#Una_with misassembly                      0                                                                                 
#Una_both parts are significant            0                                                                                 
Una_Partially unaligned length             397                                                                               
GAGE_Contigs #                             212                                                                               
GAGE_Min contig                            202                                                                               
GAGE_Max contig                            120023                                                                            
GAGE_N50                                   26469 COUNT: 35                                                                   
GAGE_Genome size                           2813862                                                                           
GAGE_Assembly size                         2756000                                                                           
GAGE_Chaff bases                           0                                                                                 
GAGE_Missing reference bases               24879(0.88%)                                                                      
GAGE_Missing assembly bases                310(0.01%)                                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                                          
GAGE_Duplicated reference bases            658                                                                               
GAGE_Compressed reference bases            34946                                                                             
GAGE_Bad trim                              310                                                                               
GAGE_Avg idy                               99.98                                                                             
GAGE_SNPs                                  44                                                                                
GAGE_Indels < 5bp                          345                                                                               
GAGE_Indels >= 5                           6                                                                                 
GAGE_Inversions                            1                                                                                 
GAGE_Relocation                            1                                                                                 
GAGE_Translocation                         0                                                                                 
GAGE_Corrected contig #                    212                                                                               
GAGE_Corrected assembly size               2755923                                                                           
GAGE_Min correct contig                    203                                                                               
GAGE_Max correct contig                    120030                                                                            
GAGE_Corrected N50                         26358 COUNT: 36                                                                   
