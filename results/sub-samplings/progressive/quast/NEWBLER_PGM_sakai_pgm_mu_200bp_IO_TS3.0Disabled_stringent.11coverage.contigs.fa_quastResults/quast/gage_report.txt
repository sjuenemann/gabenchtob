All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.11coverage.contigs
GAGE_Contigs #                   805                                                                         
GAGE_Min contig                  202                                                                         
GAGE_Max contig                  97917                                                                       
GAGE_N50                         15527 COUNT: 101                                                            
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               5272256                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     66513(1.19%)                                                                
GAGE_Missing assembly bases      749(0.01%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  1071                                                                        
GAGE_Compressed reference bases  266826                                                                      
GAGE_Bad trim                    714                                                                         
GAGE_Avg idy                     99.94                                                                       
GAGE_SNPs                        349                                                                         
GAGE_Indels < 5bp                2512                                                                        
GAGE_Indels >= 5                 11                                                                          
GAGE_Inversions                  5                                                                           
GAGE_Relocation                  5                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          820                                                                         
GAGE_Corrected assembly size     5274580                                                                     
GAGE_Min correct contig          202                                                                         
GAGE_Max correct contig          77286                                                                       
GAGE_Corrected N50               14814 COUNT: 105                                                            
