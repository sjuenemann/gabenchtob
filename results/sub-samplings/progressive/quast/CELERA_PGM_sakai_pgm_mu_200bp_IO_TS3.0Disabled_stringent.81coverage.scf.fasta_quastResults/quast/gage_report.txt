All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.81coverage.scf
GAGE_Contigs #                   711                                                                    
GAGE_Min contig                  1004                                                                   
GAGE_Max contig                  80720                                                                  
GAGE_N50                         17669 COUNT: 101                                                       
GAGE_Genome size                 5594470                                                                
GAGE_Assembly size               5268974                                                                
GAGE_Chaff bases                 0                                                                      
GAGE_Missing reference bases     346212(6.19%)                                                          
GAGE_Missing assembly bases      577(0.01%)                                                             
GAGE_Missing assembly contigs    0(0.00%)                                                               
GAGE_Duplicated reference bases  300                                                                    
GAGE_Compressed reference bases  101075                                                                 
GAGE_Bad trim                    577                                                                    
GAGE_Avg idy                     99.98                                                                  
GAGE_SNPs                        126                                                                    
GAGE_Indels < 5bp                855                                                                    
GAGE_Indels >= 5                 7                                                                      
GAGE_Inversions                  2                                                                      
GAGE_Relocation                  3                                                                      
GAGE_Translocation               1                                                                      
GAGE_Corrected contig #          722                                                                    
GAGE_Corrected assembly size     5271105                                                                
GAGE_Min correct contig          497                                                                    
GAGE_Max correct contig          80732                                                                  
GAGE_Corrected N50               17185 COUNT: 103                                                       
