All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.81coverage.scf
#Mis_misassemblies               8                                                                      
#Mis_relocations                 7                                                                      
#Mis_translocations              1                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        8                                                                      
Mis_Misassembled contigs length  83261                                                                  
#Mis_local misassemblies         6                                                                      
#mismatches                      224                                                                    
#indels                          931                                                                    
#Mis_short indels (<= 5 bp)      930                                                                    
#Mis_long indels (> 5 bp)        1                                                                      
Indels length                    956                                                                    
