All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.61coverage_out.unpadded
#Mis_misassemblies               90                                                                                                
#Mis_relocations                 88                                                                                                
#Mis_translocations              2                                                                                                 
#Mis_inversions                  0                                                                                                 
#Mis_misassembled contigs        44                                                                                                
Mis_Misassembled contigs length  4295324                                                                                           
#Mis_local misassemblies         10                                                                                                
#mismatches                      3041                                                                                              
#indels                          160                                                                                               
#Mis_short indels (<= 5 bp)      155                                                                                               
#Mis_long indels (> 5 bp)        5                                                                                                 
Indels length                    248                                                                                               
