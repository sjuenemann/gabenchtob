All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.41coverage.result
#Contigs                                   286                                                                         
#Contigs (>= 0 bp)                         286                                                                         
#Contigs (>= 1000 bp)                      168                                                                         
Largest contig                             116268                                                                      
Total length                               2774511                                                                     
Total length (>= 0 bp)                     2774511                                                                     
Total length (>= 1000 bp)                  2736941                                                                     
Reference length                           2813862                                                                     
N50                                        25418                                                                       
NG50                                       25342                                                                       
N75                                        15740                                                                       
NG75                                       15642                                                                       
L50                                        36                                                                          
LG50                                       37                                                                          
L75                                        70                                                                          
LG75                                       72                                                                          
#local misassemblies                       4                                                                           
#misassemblies                             12                                                                          
#misassembled contigs                      12                                                                          
Misassembled contigs length                175898                                                                      
Misassemblies inter-contig overlap         1446                                                                        
#unaligned contigs                         1 + 8 part                                                                  
Unaligned contigs length                   630                                                                         
#ambiguously mapped contigs                8                                                                           
Extra bases in ambiguously mapped contigs  -7693                                                                       
#N's                                       17                                                                          
#N's per 100 kbp                           0.61                                                                        
Genome fraction (%)                        97.552                                                                      
Duplication ratio                          1.008                                                                       
#genes                                     2573 + 104 part                                                             
#predicted genes (unique)                  2863                                                                        
#predicted genes (>= 0 bp)                 2864                                                                        
#predicted genes (>= 300 bp)               2353                                                                        
#predicted genes (>= 1500 bp)              266                                                                         
#predicted genes (>= 3000 bp)              23                                                                          
#mismatches                                70                                                                          
#indels                                    457                                                                         
Indels length                              540                                                                         
#mismatches per 100 kbp                    2.55                                                                        
#indels per 100 kbp                        16.65                                                                       
GC (%)                                     32.63                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.362                                                                      
Largest alignment                          75095                                                                       
NA50                                       25418                                                                       
NGA50                                      25342                                                                       
NA75                                       15740                                                                       
NGA75                                      15642                                                                       
LA50                                       37                                                                          
LGA50                                      38                                                                          
LA75                                       71                                                                          
LGA75                                      73                                                                          
#Mis_misassemblies                         12                                                                          
#Mis_relocations                           4                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            8                                                                           
#Mis_misassembled contigs                  12                                                                          
Mis_Misassembled contigs length            175898                                                                      
#Mis_local misassemblies                   4                                                                           
#Mis_short indels (<= 5 bp)                456                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               1                                                                           
Una_Fully unaligned length                 281                                                                         
#Una_partially unaligned contigs           8                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             349                                                                         
GAGE_Contigs #                             286                                                                         
GAGE_Min contig                            201                                                                         
GAGE_Max contig                            116268                                                                      
GAGE_N50                                   25342 COUNT: 37                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2774511                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               26596(0.95%)                                                                
GAGE_Missing assembly bases                459(0.02%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            18901                                                                       
GAGE_Compressed reference bases            39745                                                                       
GAGE_Bad trim                              445                                                                         
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  46                                                                          
GAGE_Indels < 5bp                          449                                                                         
GAGE_Indels >= 5                           7                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            2                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    220                                                                         
GAGE_Corrected assembly size               2756778                                                                     
GAGE_Min correct contig                    203                                                                         
GAGE_Max correct contig                    75108                                                                       
GAGE_Corrected N50                         25134 COUNT: 38                                                             
