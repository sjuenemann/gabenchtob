All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.36coverage.contigs
#Contigs (>= 0 bp)             135                                                                              
#Contigs (>= 1000 bp)          71                                                                               
Total length (>= 0 bp)         2774435                                                                          
Total length (>= 1000 bp)      2758175                                                                          
#Contigs                       95                                                                               
Largest contig                 169311                                                                           
Total length                   2769204                                                                          
Reference length               2813862                                                                          
GC (%)                         32.64                                                                            
Reference GC (%)               32.81                                                                            
N50                            71378                                                                            
NG50                           71378                                                                            
N75                            42328                                                                            
NG75                           40109                                                                            
#misassemblies                 3                                                                                
#local misassemblies           9                                                                                
#unaligned contigs             0 + 1 part                                                                       
Unaligned contigs length       57                                                                               
Genome fraction (%)            98.057                                                                           
Duplication ratio              1.001                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        2.97                                                                             
#indels per 100 kbp            8.99                                                                             
#genes                         2647 + 44 part                                                                   
#predicted genes (unique)      2698                                                                             
#predicted genes (>= 0 bp)     2698                                                                             
#predicted genes (>= 300 bp)   2316                                                                             
#predicted genes (>= 1500 bp)  286                                                                              
#predicted genes (>= 3000 bp)  24                                                                               
Largest alignment              169311                                                                           
NA50                           71378                                                                            
NGA50                          71378                                                                            
NA75                           42328                                                                            
NGA75                          40109                                                                            
