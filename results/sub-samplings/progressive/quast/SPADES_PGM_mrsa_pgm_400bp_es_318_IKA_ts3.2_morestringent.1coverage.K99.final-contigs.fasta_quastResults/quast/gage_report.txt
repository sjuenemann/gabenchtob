All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.1coverage.K99.final-contigs
GAGE_Contigs #                   699                                                                                 
GAGE_Min contig                  484                                                                                 
GAGE_Max contig                  5428                                                                                
GAGE_N50                         0 COUNT: 0                                                                          
GAGE_Genome size                 2813862                                                                             
GAGE_Assembly size               620238                                                                              
GAGE_Chaff bases                 0                                                                                   
GAGE_Missing reference bases     2151181(76.45%)                                                                     
GAGE_Missing assembly bases      2075(0.33%)                                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                                            
GAGE_Duplicated reference bases  324                                                                                 
GAGE_Compressed reference bases  44640                                                                               
GAGE_Bad trim                    2050                                                                                
GAGE_Avg idy                     99.16                                                                               
GAGE_SNPs                        473                                                                                 
GAGE_Indels < 5bp                4676                                                                                
GAGE_Indels >= 5                 4                                                                                   
GAGE_Inversions                  10                                                                                  
GAGE_Relocation                  13                                                                                  
GAGE_Translocation               0                                                                                   
GAGE_Corrected contig #          726                                                                                 
GAGE_Corrected assembly size     619026                                                                              
GAGE_Min correct contig          205                                                                                 
GAGE_Max correct contig          3508                                                                                
GAGE_Corrected N50               0 COUNT: 0                                                                          
