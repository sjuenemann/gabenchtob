All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.81coverage_out.unpadded
#Contigs                                   621                                                                           
#Contigs (>= 0 bp)                         639                                                                           
#Contigs (>= 1000 bp)                      50                                                                            
Largest contig                             833844                                                                        
Total length                               3185897                                                                       
Total length (>= 0 bp)                     3188170                                                                       
Total length (>= 1000 bp)                  2893502                                                                       
Reference length                           2813862                                                                       
N50                                        215111                                                                        
NG50                                       215111                                                                        
N75                                        104580                                                                        
NG75                                       119070                                                                        
L50                                        4                                                                             
LG50                                       4                                                                             
L75                                        10                                                                            
LG75                                       8                                                                             
#local misassemblies                       7                                                                             
#misassemblies                             29                                                                            
#misassembled contigs                      26                                                                            
Misassembled contigs length                1481354                                                                       
Misassemblies inter-contig overlap         16098                                                                         
#unaligned contigs                         3 + 1 part                                                                    
Unaligned contigs length                   1313                                                                          
#ambiguously mapped contigs                20                                                                            
Extra bases in ambiguously mapped contigs  -12526                                                                        
#N's                                       145                                                                           
#N's per 100 kbp                           4.55                                                                          
Genome fraction (%)                        99.744                                                                        
Duplication ratio                          1.136                                                                         
#genes                                     2703 + 22 part                                                                
#predicted genes (unique)                  3421                                                                          
#predicted genes (>= 0 bp)                 3480                                                                          
#predicted genes (>= 300 bp)               2497                                                                          
#predicted genes (>= 1500 bp)              301                                                                           
#predicted genes (>= 3000 bp)              32                                                                            
#mismatches                                226                                                                           
#indels                                    130                                                                           
Indels length                              263                                                                           
#mismatches per 100 kbp                    8.05                                                                          
#indels per 100 kbp                        4.63                                                                          
GC (%)                                     32.76                                                                         
Reference GC (%)                           32.81                                                                         
Average %IDY                               98.791                                                                        
Largest alignment                          419979                                                                        
NA50                                       215108                                                                        
NGA50                                      215108                                                                        
NA75                                       75020                                                                         
NGA75                                      104580                                                                        
LA50                                       5                                                                             
LGA50                                      5                                                                             
LA75                                       12                                                                            
LGA75                                      9                                                                             
#Mis_misassemblies                         29                                                                            
#Mis_relocations                           29                                                                            
#Mis_translocations                        0                                                                             
#Mis_inversions                            0                                                                             
#Mis_misassembled contigs                  26                                                                            
Mis_Misassembled contigs length            1481354                                                                       
#Mis_local misassemblies                   7                                                                             
#Mis_short indels (<= 5 bp)                124                                                                           
#Mis_long indels (> 5 bp)                  6                                                                             
#Una_fully unaligned contigs               3                                                                             
Una_Fully unaligned length                 1296                                                                          
#Una_partially unaligned contigs           1                                                                             
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            0                                                                             
Una_Partially unaligned length             17                                                                            
GAGE_Contigs #                             621                                                                           
GAGE_Min contig                            201                                                                           
GAGE_Max contig                            833844                                                                        
GAGE_N50                                   215111 COUNT: 4                                                               
GAGE_Genome size                           2813862                                                                       
GAGE_Assembly size                         3185897                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               546(0.02%)                                                                    
GAGE_Missing assembly bases                2290(0.07%)                                                                   
GAGE_Missing assembly contigs              2(0.32%)                                                                      
GAGE_Duplicated reference bases            379870                                                                        
GAGE_Compressed reference bases            9572                                                                          
GAGE_Bad trim                              1644                                                                          
GAGE_Avg idy                               99.99                                                                         
GAGE_SNPs                                  74                                                                            
GAGE_Indels < 5bp                          46                                                                            
GAGE_Indels >= 5                           6                                                                             
GAGE_Inversions                            4                                                                             
GAGE_Relocation                            4                                                                             
GAGE_Translocation                         0                                                                             
GAGE_Corrected contig #                    47                                                                            
GAGE_Corrected assembly size               2826308                                                                       
GAGE_Min correct contig                    233                                                                           
GAGE_Max correct contig                    417353                                                                        
GAGE_Corrected N50                         201399 COUNT: 5                                                               
