All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.46coverage.contigs
GAGE_Contigs #                   288                                                              
GAGE_Min contig                  204                                                              
GAGE_Max contig                  326743                                                           
GAGE_N50                         99582 COUNT: 17                                                  
GAGE_Genome size                 5594470                                                          
GAGE_Assembly size               5355353                                                          
GAGE_Chaff bases                 0                                                                
GAGE_Missing reference bases     31357(0.56%)                                                     
GAGE_Missing assembly bases      12(0.00%)                                                        
GAGE_Missing assembly contigs    0(0.00%)                                                         
GAGE_Duplicated reference bases  5278                                                             
GAGE_Compressed reference bases  230297                                                           
GAGE_Bad trim                    12                                                               
GAGE_Avg idy                     99.99                                                            
GAGE_SNPs                        87                                                               
GAGE_Indels < 5bp                301                                                              
GAGE_Indels >= 5                 8                                                                
GAGE_Inversions                  0                                                                
GAGE_Relocation                  6                                                                
GAGE_Translocation               0                                                                
GAGE_Corrected contig #          286                                                              
GAGE_Corrected assembly size     5352335                                                          
GAGE_Min correct contig          204                                                              
GAGE_Max correct contig          313854                                                           
GAGE_Corrected N50               95403 COUNT: 19                                                  
