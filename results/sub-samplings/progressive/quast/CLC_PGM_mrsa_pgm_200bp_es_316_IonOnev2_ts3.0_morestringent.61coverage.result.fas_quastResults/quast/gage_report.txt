All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.61coverage.result
GAGE_Contigs #                   277                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  113644                                                                      
GAGE_N50                         42571 COUNT: 24                                                             
GAGE_Genome size                 2813862                                                                     
GAGE_Assembly size               2781841                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     21173(0.75%)                                                                
GAGE_Missing assembly bases      1152(0.04%)                                                                 
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  23618                                                                       
GAGE_Compressed reference bases  38203                                                                       
GAGE_Bad trim                    1152                                                                        
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        50                                                                          
GAGE_Indels < 5bp                360                                                                         
GAGE_Indels >= 5                 5                                                                           
GAGE_Inversions                  1                                                                           
GAGE_Relocation                  2                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          188                                                                         
GAGE_Corrected assembly size     2758814                                                                     
GAGE_Min correct contig          201                                                                         
GAGE_Max correct contig          105603                                                                      
GAGE_Corrected N50               42579 COUNT: 24                                                             
