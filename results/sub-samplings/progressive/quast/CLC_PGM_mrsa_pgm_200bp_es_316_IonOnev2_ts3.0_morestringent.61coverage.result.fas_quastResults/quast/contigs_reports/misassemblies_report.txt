All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.61coverage.result
#Mis_misassemblies               20                                                                          
#Mis_relocations                 4                                                                           
#Mis_translocations              0                                                                           
#Mis_inversions                  16                                                                          
#Mis_misassembled contigs        20                                                                          
Mis_Misassembled contigs length  184813                                                                      
#Mis_local misassemblies         2                                                                           
#mismatches                      64                                                                          
#indels                          356                                                                         
#Mis_short indels (<= 5 bp)      354                                                                         
#Mis_long indels (> 5 bp)        2                                                                           
Indels length                    497                                                                         
