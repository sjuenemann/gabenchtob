All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.61coverage.K99.final-contigs
#Contigs                                   407                                                                                  
#Contigs (>= 0 bp)                         621                                                                                  
#Contigs (>= 1000 bp)                      174                                                                                  
Largest contig                             298922                                                                               
Total length                               5368883                                                                              
Total length (>= 0 bp)                     5398815                                                                              
Total length (>= 1000 bp)                  5265979                                                                              
Reference length                           5594470                                                                              
N50                                        126042                                                                               
NG50                                       121780                                                                               
N75                                        54324                                                                                
NG75                                       41172                                                                                
L50                                        14                                                                                   
LG50                                       15                                                                                   
L75                                        30                                                                                   
LG75                                       34                                                                                   
#local misassemblies                       7                                                                                    
#misassemblies                             5                                                                                    
#misassembled contigs                      5                                                                                    
Misassembled contigs length                818724                                                                               
Misassemblies inter-contig overlap         1546                                                                                 
#unaligned contigs                         0 + 0 part                                                                           
Unaligned contigs length                   0                                                                                    
#ambiguously mapped contigs                135                                                                                  
Extra bases in ambiguously mapped contigs  -82760                                                                               
#N's                                       0                                                                                    
#N's per 100 kbp                           0.00                                                                                 
Genome fraction (%)                        94.393                                                                               
Duplication ratio                          1.001                                                                                
#genes                                     4926 + 192 part                                                                      
#predicted genes (unique)                  6000                                                                                 
#predicted genes (>= 0 bp)                 6018                                                                                 
#predicted genes (>= 300 bp)               4807                                                                                 
#predicted genes (>= 1500 bp)              532                                                                                  
#predicted genes (>= 3000 bp)              41                                                                                   
#mismatches                                200                                                                                  
#indels                                    1522                                                                                 
Indels length                              1576                                                                                 
#mismatches per 100 kbp                    3.79                                                                                 
#indels per 100 kbp                        28.82                                                                                
GC (%)                                     50.29                                                                                
Reference GC (%)                           50.48                                                                                
Average %IDY                               98.848                                                                               
Largest alignment                          247767                                                                               
NA50                                       116608                                                                               
NGA50                                      111271                                                                               
NA75                                       49821                                                                                
NGA75                                      41086                                                                                
LA50                                       16                                                                                   
LGA50                                      17                                                                                   
LA75                                       33                                                                                   
LGA75                                      37                                                                                   
#Mis_misassemblies                         5                                                                                    
#Mis_relocations                           5                                                                                    
#Mis_translocations                        0                                                                                    
#Mis_inversions                            0                                                                                    
#Mis_misassembled contigs                  5                                                                                    
Mis_Misassembled contigs length            818724                                                                               
#Mis_local misassemblies                   7                                                                                    
#Mis_short indels (<= 5 bp)                1521                                                                                 
#Mis_long indels (> 5 bp)                  1                                                                                    
#Una_fully unaligned contigs               0                                                                                    
Una_Fully unaligned length                 0                                                                                    
#Una_partially unaligned contigs           0                                                                                    
#Una_with misassembly                      0                                                                                    
#Una_both parts are significant            0                                                                                    
Una_Partially unaligned length             0                                                                                    
GAGE_Contigs #                             407                                                                                  
GAGE_Min contig                            200                                                                                  
GAGE_Max contig                            298922                                                                               
GAGE_N50                                   121780 COUNT: 15                                                                     
GAGE_Genome size                           5594470                                                                              
GAGE_Assembly size                         5368883                                                                              
GAGE_Chaff bases                           0                                                                                    
GAGE_Missing reference bases               9639(0.17%)                                                                          
GAGE_Missing assembly bases                67(0.00%)                                                                            
GAGE_Missing assembly contigs              0(0.00%)                                                                             
GAGE_Duplicated reference bases            1420                                                                                 
GAGE_Compressed reference bases            282719                                                                               
GAGE_Bad trim                              67                                                                                   
GAGE_Avg idy                               99.97                                                                                
GAGE_SNPs                                  191                                                                                  
GAGE_Indels < 5bp                          1576                                                                                 
GAGE_Indels >= 5                           7                                                                                    
GAGE_Inversions                            2                                                                                    
GAGE_Relocation                            7                                                                                    
GAGE_Translocation                         0                                                                                    
GAGE_Corrected contig #                    419                                                                                  
GAGE_Corrected assembly size               5371207                                                                              
GAGE_Min correct contig                    200                                                                                  
GAGE_Max correct contig                    226975                                                                               
GAGE_Corrected N50                         96225 COUNT: 19                                                                      
