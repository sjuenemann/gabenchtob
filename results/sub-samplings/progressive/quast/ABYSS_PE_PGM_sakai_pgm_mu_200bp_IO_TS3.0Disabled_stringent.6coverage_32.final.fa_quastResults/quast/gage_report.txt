All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.6coverage_32.final
GAGE_Contigs #                   5641                                                                         
GAGE_Min contig                  200                                                                          
GAGE_Max contig                  6732                                                                         
GAGE_N50                         949 COUNT: 1728                                                              
GAGE_Genome size                 5594470                                                                      
GAGE_Assembly size               4720924                                                                      
GAGE_Chaff bases                 0                                                                            
GAGE_Missing reference bases     753064(13.46%)                                                               
GAGE_Missing assembly bases      2043(0.04%)                                                                  
GAGE_Missing assembly contigs    0(0.00%)                                                                     
GAGE_Duplicated reference bases  0                                                                            
GAGE_Compressed reference bases  137896                                                                       
GAGE_Bad trim                    2037                                                                         
GAGE_Avg idy                     99.92                                                                        
GAGE_SNPs                        255                                                                          
GAGE_Indels < 5bp                3278                                                                         
GAGE_Indels >= 5                 5                                                                            
GAGE_Inversions                  4                                                                            
GAGE_Relocation                  3                                                                            
GAGE_Translocation               0                                                                            
GAGE_Corrected contig #          5644                                                                         
GAGE_Corrected assembly size     4720606                                                                      
GAGE_Min correct contig          200                                                                          
GAGE_Max correct contig          6731                                                                         
GAGE_Corrected N50               946 COUNT: 1730                                                              
