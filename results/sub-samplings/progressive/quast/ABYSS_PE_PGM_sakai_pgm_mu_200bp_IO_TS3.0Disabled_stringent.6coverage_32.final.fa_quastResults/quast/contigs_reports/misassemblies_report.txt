All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.6coverage_32.final
#Mis_misassemblies               8                                                                            
#Mis_relocations                 6                                                                            
#Mis_translocations              0                                                                            
#Mis_inversions                  2                                                                            
#Mis_misassembled contigs        8                                                                            
Mis_Misassembled contigs length  7906                                                                         
#Mis_local misassemblies         3                                                                            
#mismatches                      214                                                                          
#indels                          3225                                                                         
#Mis_short indels (<= 5 bp)      3224                                                                         
#Mis_long indels (> 5 bp)        1                                                                            
Indels length                    3313                                                                         
