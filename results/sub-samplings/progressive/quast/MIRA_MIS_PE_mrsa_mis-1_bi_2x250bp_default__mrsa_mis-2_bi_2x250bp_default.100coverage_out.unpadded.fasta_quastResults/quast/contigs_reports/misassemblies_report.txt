All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.100coverage_out.unpadded
#Mis_misassemblies               20                                                                                               
#Mis_relocations                 19                                                                                               
#Mis_translocations              0                                                                                                
#Mis_inversions                  1                                                                                                
#Mis_misassembled contigs        16                                                                                               
Mis_Misassembled contigs length  1048481                                                                                          
#Mis_local misassemblies         4                                                                                                
#mismatches                      150                                                                                              
#indels                          58                                                                                               
#Mis_short indels (<= 5 bp)      53                                                                                               
#Mis_long indels (> 5 bp)        5                                                                                                
Indels length                    131                                                                                              
