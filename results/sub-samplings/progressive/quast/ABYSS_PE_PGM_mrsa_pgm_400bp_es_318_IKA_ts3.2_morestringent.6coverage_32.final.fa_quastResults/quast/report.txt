All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.6coverage_32.final
#Contigs (>= 0 bp)             7691                                                                         
#Contigs (>= 1000 bp)          588                                                                          
Total length (>= 0 bp)         2542980                                                                      
Total length (>= 1000 bp)      938053                                                                       
#Contigs                       3144                                                                         
Largest contig                 7487                                                                         
Total length                   2186510                                                                      
Reference length               2813862                                                                      
GC (%)                         32.95                                                                        
Reference GC (%)               32.81                                                                        
N50                            887                                                                          
NG50                           664                                                                          
N75                            532                                                                          
NG75                           253                                                                          
#misassemblies                 2                                                                            
#local misassemblies           0                                                                            
#unaligned contigs             1 + 6 part                                                                   
Unaligned contigs length       684                                                                          
Genome fraction (%)            77.178                                                                       
Duplication ratio              1.004                                                                        
#N's per 100 kbp               0.00                                                                         
#mismatches per 100 kbp        5.02                                                                         
#indels per 100 kbp            59.03                                                                        
#genes                         628 + 1805 part                                                              
#predicted genes (unique)      4345                                                                         
#predicted genes (>= 0 bp)     4345                                                                         
#predicted genes (>= 300 bp)   2436                                                                         
#predicted genes (>= 1500 bp)  35                                                                           
#predicted genes (>= 3000 bp)  0                                                                            
Largest alignment              7487                                                                         
NA50                           884                                                                          
NGA50                          663                                                                          
NA75                           529                                                                          
NGA75                          249                                                                          
