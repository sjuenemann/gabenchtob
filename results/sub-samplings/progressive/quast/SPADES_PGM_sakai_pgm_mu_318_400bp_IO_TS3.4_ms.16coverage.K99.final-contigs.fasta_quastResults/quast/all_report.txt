All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.16coverage.K99.final-contigs
#Contigs                                   293                                                                       
#Contigs (>= 0 bp)                         382                                                                       
#Contigs (>= 1000 bp)                      193                                                                       
Largest contig                             259761                                                                    
Total length                               5327789                                                                   
Total length (>= 0 bp)                     5339355                                                                   
Total length (>= 1000 bp)                  5281610                                                                   
Reference length                           5594470                                                                   
N50                                        103320                                                                    
NG50                                       99753                                                                     
N75                                        41672                                                                     
NG75                                       33172                                                                     
L50                                        18                                                                        
LG50                                       19                                                                        
L75                                        38                                                                        
LG75                                       44                                                                        
#local misassemblies                       25                                                                        
#misassemblies                             15                                                                        
#misassembled contigs                      14                                                                        
Misassembled contigs length                440669                                                                    
Misassemblies inter-contig overlap         6365                                                                      
#unaligned contigs                         0 + 0 part                                                                
Unaligned contigs length                   0                                                                         
#ambiguously mapped contigs                79                                                                        
Extra bases in ambiguously mapped contigs  -63845                                                                    
#N's                                       0                                                                         
#N's per 100 kbp                           0.00                                                                      
Genome fraction (%)                        94.042                                                                    
Duplication ratio                          1.002                                                                     
#genes                                     4920 + 181 part                                                           
#predicted genes (unique)                  5707                                                                      
#predicted genes (>= 0 bp)                 5718                                                                      
#predicted genes (>= 300 bp)               4667                                                                      
#predicted genes (>= 1500 bp)              573                                                                       
#predicted genes (>= 3000 bp)              43                                                                        
#mismatches                                647                                                                       
#indels                                    1826                                                                      
Indels length                              1950                                                                      
#mismatches per 100 kbp                    12.30                                                                     
#indels per 100 kbp                        34.71                                                                     
GC (%)                                     50.29                                                                     
Reference GC (%)                           50.48                                                                     
Average %IDY                               98.864                                                                    
Largest alignment                          259761                                                                    
NA50                                       103320                                                                    
NGA50                                      99753                                                                     
NA75                                       38744                                                                     
NGA75                                      33007                                                                     
LA50                                       18                                                                        
LGA50                                      19                                                                        
LA75                                       39                                                                        
LGA75                                      44                                                                        
#Mis_misassemblies                         15                                                                        
#Mis_relocations                           14                                                                        
#Mis_translocations                        1                                                                         
#Mis_inversions                            0                                                                         
#Mis_misassembled contigs                  14                                                                        
Mis_Misassembled contigs length            440669                                                                    
#Mis_local misassemblies                   25                                                                        
#Mis_short indels (<= 5 bp)                1820                                                                      
#Mis_long indels (> 5 bp)                  6                                                                         
#Una_fully unaligned contigs               0                                                                         
Una_Fully unaligned length                 0                                                                         
#Una_partially unaligned contigs           0                                                                         
#Una_with misassembly                      0                                                                         
#Una_both parts are significant            0                                                                         
Una_Partially unaligned length             0                                                                         
GAGE_Contigs #                             293                                                                       
GAGE_Min contig                            201                                                                       
GAGE_Max contig                            259761                                                                    
GAGE_N50                                   99753 COUNT: 19                                                           
GAGE_Genome size                           5594470                                                                   
GAGE_Assembly size                         5327789                                                                   
GAGE_Chaff bases                           0                                                                         
GAGE_Missing reference bases               21582(0.39%)                                                              
GAGE_Missing assembly bases                96(0.00%)                                                                 
GAGE_Missing assembly contigs              0(0.00%)                                                                  
GAGE_Duplicated reference bases            2500                                                                      
GAGE_Compressed reference bases            299726                                                                    
GAGE_Bad trim                              93                                                                        
GAGE_Avg idy                               99.95                                                                     
GAGE_SNPs                                  477                                                                       
GAGE_Indels < 5bp                          1805                                                                      
GAGE_Indels >= 5                           19                                                                        
GAGE_Inversions                            5                                                                         
GAGE_Relocation                            10                                                                        
GAGE_Translocation                         0                                                                         
GAGE_Corrected contig #                    325                                                                       
GAGE_Corrected assembly size               5330294                                                                   
GAGE_Min correct contig                    201                                                                       
GAGE_Max correct contig                    224217                                                                    
GAGE_Corrected N50                         66127 COUNT: 24                                                           
