All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.56coverage_out.unpadded
#Mis_misassemblies               86                                                                                 
#Mis_relocations                 26                                                                                 
#Mis_translocations              0                                                                                  
#Mis_inversions                  60                                                                                 
#Mis_misassembled contigs        72                                                                                 
Mis_Misassembled contigs length  886891                                                                             
#Mis_local misassemblies         13                                                                                 
#mismatches                      162                                                                                
#indels                          228                                                                                
#Mis_short indels (<= 5 bp)      223                                                                                
#Mis_long indels (> 5 bp)        5                                                                                  
Indels length                    320                                                                                
