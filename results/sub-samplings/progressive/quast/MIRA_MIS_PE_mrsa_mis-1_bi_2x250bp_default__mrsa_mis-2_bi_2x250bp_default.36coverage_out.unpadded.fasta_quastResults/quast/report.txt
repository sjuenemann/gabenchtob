All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.36coverage_out.unpadded
#Contigs (>= 0 bp)             145                                                                                             
#Contigs (>= 1000 bp)          48                                                                                              
Total length (>= 0 bp)         2872604                                                                                         
Total length (>= 1000 bp)      2826386                                                                                         
#Contigs                       144                                                                                             
Largest contig                 319033                                                                                          
Total length                   2872497                                                                                         
Reference length               2813862                                                                                         
GC (%)                         32.96                                                                                           
Reference GC (%)               32.81                                                                                           
N50                            116746                                                                                          
NG50                           116746                                                                                          
N75                            84737                                                                                           
NG75                           84737                                                                                           
#misassemblies                 12                                                                                              
#local misassemblies           1                                                                                               
#unaligned contigs             2 + 3 part                                                                                      
Unaligned contigs length       6060                                                                                            
Genome fraction (%)            99.805                                                                                          
Duplication ratio              1.022                                                                                           
#N's per 100 kbp               0.52                                                                                            
#mismatches per 100 kbp        4.91                                                                                            
#indels per 100 kbp            3.95                                                                                            
#genes                         2696 + 28 part                                                                                  
#predicted genes (unique)      2748                                                                                            
#predicted genes (>= 0 bp)     2758                                                                                            
#predicted genes (>= 300 bp)   2366                                                                                            
#predicted genes (>= 1500 bp)  301                                                                                             
#predicted genes (>= 3000 bp)  31                                                                                              
Largest alignment              319033                                                                                          
NA50                           108836                                                                                          
NGA50                          108836                                                                                          
NA75                           68231                                                                                           
NGA75                          69476                                                                                           
