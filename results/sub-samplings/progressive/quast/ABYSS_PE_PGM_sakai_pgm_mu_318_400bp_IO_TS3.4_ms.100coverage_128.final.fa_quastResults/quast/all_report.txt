All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.100coverage_128.final
#Contigs                                   3132                                                                 
#Contigs (>= 0 bp)                         4460                                                                 
#Contigs (>= 1000 bp)                      743                                                                  
Largest contig                             47260                                                                
Total length                               5805579                                                              
Total length (>= 0 bp)                     6013153                                                              
Total length (>= 1000 bp)                  5113965                                                              
Reference length                           5594470                                                              
N50                                        10064                                                                
NG50                                       10558                                                                
N75                                        4121                                                                 
NG75                                       4628                                                                 
L50                                        169                                                                  
LG50                                       159                                                                  
L75                                        399                                                                  
LG75                                       363                                                                  
#local misassemblies                       4                                                                    
#misassemblies                             8                                                                    
#misassembled contigs                      8                                                                    
Misassembled contigs length                47909                                                                
Misassemblies inter-contig overlap         686                                                                  
#unaligned contigs                         0 + 4 part                                                           
Unaligned contigs length                   99                                                                   
#ambiguously mapped contigs                632                                                                  
Extra bases in ambiguously mapped contigs  -190874                                                              
#N's                                       0                                                                    
#N's per 100 kbp                           0.00                                                                 
Genome fraction (%)                        94.651                                                               
Duplication ratio                          1.060                                                                
#genes                                     4415 + 762 part                                                      
#predicted genes (unique)                  7781                                                                 
#predicted genes (>= 0 bp)                 7854                                                                 
#predicted genes (>= 300 bp)               4696                                                                 
#predicted genes (>= 1500 bp)              576                                                                  
#predicted genes (>= 3000 bp)              51                                                                   
#mismatches                                67                                                                   
#indels                                    408                                                                  
Indels length                              418                                                                  
#mismatches per 100 kbp                    1.27                                                                 
#indels per 100 kbp                        7.71                                                                 
GC (%)                                     50.52                                                                
Reference GC (%)                           50.48                                                                
Average %IDY                               99.155                                                               
Largest alignment                          47260                                                                
NA50                                       10064                                                                
NGA50                                      10558                                                                
NA75                                       4121                                                                 
NGA75                                      4628                                                                 
LA50                                       170                                                                  
LGA50                                      160                                                                  
LA75                                       400                                                                  
LGA75                                      364                                                                  
#Mis_misassemblies                         8                                                                    
#Mis_relocations                           8                                                                    
#Mis_translocations                        0                                                                    
#Mis_inversions                            0                                                                    
#Mis_misassembled contigs                  8                                                                    
Mis_Misassembled contigs length            47909                                                                
#Mis_local misassemblies                   4                                                                    
#Mis_short indels (<= 5 bp)                408                                                                  
#Mis_long indels (> 5 bp)                  0                                                                    
#Una_fully unaligned contigs               0                                                                    
Una_Fully unaligned length                 0                                                                    
#Una_partially unaligned contigs           4                                                                    
#Una_with misassembly                      0                                                                    
#Una_both parts are significant            0                                                                    
Una_Partially unaligned length             99                                                                   
GAGE_Contigs #                             3132                                                                 
GAGE_Min contig                            200                                                                  
GAGE_Max contig                            47260                                                                
GAGE_N50                                   10558 COUNT: 159                                                     
GAGE_Genome size                           5594470                                                              
GAGE_Assembly size                         5805579                                                              
GAGE_Chaff bases                           0                                                                    
GAGE_Missing reference bases               22082(0.39%)                                                         
GAGE_Missing assembly bases                106(0.00%)                                                           
GAGE_Missing assembly contigs              0(0.00%)                                                             
GAGE_Duplicated reference bases            308202                                                               
GAGE_Compressed reference bases            255446                                                               
GAGE_Bad trim                              105                                                                  
GAGE_Avg idy                               99.99                                                                
GAGE_SNPs                                  61                                                                   
GAGE_Indels < 5bp                          365                                                                  
GAGE_Indels >= 5                           3                                                                    
GAGE_Inversions                            0                                                                    
GAGE_Relocation                            3                                                                    
GAGE_Translocation                         0                                                                    
GAGE_Corrected contig #                    1885                                                                 
GAGE_Corrected assembly size               5495811                                                              
GAGE_Min correct contig                    200                                                                  
GAGE_Max correct contig                    47260                                                                
GAGE_Corrected N50                         10558 COUNT: 160                                                     
