All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.21coverage.K99.final-contigs
#Mis_misassemblies               14                                                                                   
#Mis_relocations                 12                                                                                   
#Mis_translocations              0                                                                                    
#Mis_inversions                  2                                                                                    
#Mis_misassembled contigs        13                                                                                   
Mis_Misassembled contigs length  991512                                                                               
#Mis_local misassemblies         19                                                                                   
#mismatches                      701                                                                                  
#indels                          2419                                                                                 
#Mis_short indels (<= 5 bp)      2416                                                                                 
#Mis_long indels (> 5 bp)        3                                                                                    
Indels length                    2519                                                                                 
