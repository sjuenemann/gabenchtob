All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.21coverage.K99.final-contigs
#Contigs (>= 0 bp)             279                                                                                  
#Contigs (>= 1000 bp)          191                                                                                  
Total length (>= 0 bp)         5383437                                                                              
Total length (>= 1000 bp)      5360553                                                                              
#Contigs                       231                                                                                  
Largest contig                 283405                                                                               
Total length                   5377732                                                                              
Reference length               5594470                                                                              
GC (%)                         50.29                                                                                
Reference GC (%)               50.48                                                                                
N50                            99706                                                                                
NG50                           96195                                                                                
N75                            41753                                                                                
NG75                           40559                                                                                
#misassemblies                 14                                                                                   
#local misassemblies           19                                                                                   
#unaligned contigs             0 + 3 part                                                                           
Unaligned contigs length       188                                                                                  
Genome fraction (%)            94.121                                                                               
Duplication ratio              1.013                                                                                
#N's per 100 kbp               0.00                                                                                 
#mismatches per 100 kbp        13.31                                                                                
#indels per 100 kbp            45.94                                                                                
#genes                         4939 + 148 part                                                                      
#predicted genes (unique)      6169                                                                                 
#predicted genes (>= 0 bp)     6242                                                                                 
#predicted genes (>= 300 bp)   4971                                                                                 
#predicted genes (>= 1500 bp)  481                                                                                  
#predicted genes (>= 3000 bp)  26                                                                                   
Largest alignment              259644                                                                               
NA50                           96195                                                                                
NGA50                          92195                                                                                
NA75                           41753                                                                                
NGA75                          40559                                                                                
