All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.31coverage.scf
#Contigs (>= 0 bp)             69                                                                     
#Contigs (>= 1000 bp)          69                                                                     
Total length (>= 0 bp)         2740511                                                                
Total length (>= 1000 bp)      2740511                                                                
#Contigs                       69                                                                     
Largest contig                 303404                                                                 
Total length                   2740511                                                                
Reference length               2813862                                                                
GC (%)                         32.62                                                                  
Reference GC (%)               32.81                                                                  
N50                            89768                                                                  
NG50                           89768                                                                  
N75                            39632                                                                  
NG75                           36114                                                                  
#misassemblies                 1                                                                      
#local misassemblies           3                                                                      
#unaligned contigs             0 + 1 part                                                             
Unaligned contigs length       56                                                                     
Genome fraction (%)            97.050                                                                 
Duplication ratio              1.004                                                                  
#N's per 100 kbp               0.00                                                                   
#mismatches per 100 kbp        3.08                                                                   
#indels per 100 kbp            12.71                                                                  
#genes                         2619 + 41 part                                                         
#predicted genes (unique)      2721                                                                   
#predicted genes (>= 0 bp)     2724                                                                   
#predicted genes (>= 300 bp)   2314                                                                   
#predicted genes (>= 1500 bp)  275                                                                    
#predicted genes (>= 3000 bp)  26                                                                     
Largest alignment              303404                                                                 
NA50                           89767                                                                  
NGA50                          89767                                                                  
NA75                           37895                                                                  
NGA75                          36114                                                                  
