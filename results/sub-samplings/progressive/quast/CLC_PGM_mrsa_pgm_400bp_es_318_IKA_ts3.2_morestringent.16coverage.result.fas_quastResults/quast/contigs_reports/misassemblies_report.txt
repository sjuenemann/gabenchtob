All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.16coverage.result
#Mis_misassemblies               7                                                                      
#Mis_relocations                 7                                                                      
#Mis_translocations              0                                                                      
#Mis_inversions                  0                                                                      
#Mis_misassembled contigs        7                                                                      
Mis_Misassembled contigs length  55137                                                                  
#Mis_local misassemblies         6                                                                      
#mismatches                      207                                                                    
#indels                          1307                                                                   
#Mis_short indels (<= 5 bp)      1307                                                                   
#Mis_long indels (> 5 bp)        0                                                                      
Indels length                    1355                                                                   
