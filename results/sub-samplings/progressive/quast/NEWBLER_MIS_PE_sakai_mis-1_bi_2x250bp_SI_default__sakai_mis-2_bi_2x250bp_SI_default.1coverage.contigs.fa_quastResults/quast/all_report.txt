All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.1coverage.contigs
#Contigs                                   714                                                                                                  
#Contigs (>= 0 bp)                         744                                                                                                  
#Contigs (>= 1000 bp)                      14                                                                                                   
Largest contig                             2666                                                                                                 
Total length                               339125                                                                                               
Total length (>= 0 bp)                     343768                                                                                               
Total length (>= 1000 bp)                  21103                                                                                                
Reference length                           5594470                                                                                              
N50                                        470                                                                                                  
NG50                                       None                                                                                                 
N75                                        392                                                                                                  
NG75                                       None                                                                                                 
L50                                        260                                                                                                  
LG50                                       None                                                                                                 
L75                                        458                                                                                                  
LG75                                       None                                                                                                 
#local misassemblies                       0                                                                                                    
#misassemblies                             3                                                                                                    
#misassembled contigs                      3                                                                                                    
Misassembled contigs length                3681                                                                                                 
Misassemblies inter-contig overlap         2394                                                                                                 
#unaligned contigs                         1 + 1 part                                                                                           
Unaligned contigs length                   681                                                                                                  
#ambiguously mapped contigs                60                                                                                                   
Extra bases in ambiguously mapped contigs  -40640                                                                                               
#N's                                       0                                                                                                    
#N's per 100 kbp                           0.00                                                                                                 
Genome fraction (%)                        5.365                                                                                                
Duplication ratio                          1.000                                                                                                
#genes                                     38 + 665 part                                                                                        
#predicted genes (unique)                  853                                                                                                  
#predicted genes (>= 0 bp)                 853                                                                                                  
#predicted genes (>= 300 bp)               505                                                                                                  
#predicted genes (>= 1500 bp)              1                                                                                                    
#predicted genes (>= 3000 bp)              0                                                                                                    
#mismatches                                384                                                                                                  
#indels                                    28                                                                                                   
Indels length                              35                                                                                                   
#mismatches per 100 kbp                    127.93                                                                                               
#indels per 100 kbp                        9.33                                                                                                 
GC (%)                                     51.38                                                                                                
Reference GC (%)                           50.48                                                                                                
Average %IDY                               99.458                                                                                               
Largest alignment                          2204                                                                                                 
NA50                                       436                                                                                                  
NGA50                                      None                                                                                                 
NA75                                       351                                                                                                  
NGA75                                      None                                                                                                 
LA50                                       298                                                                                                  
LGA50                                      None                                                                                                 
LA75                                       512                                                                                                  
LGA75                                      None                                                                                                 
#Mis_misassemblies                         3                                                                                                    
#Mis_relocations                           3                                                                                                    
#Mis_translocations                        0                                                                                                    
#Mis_inversions                            0                                                                                                    
#Mis_misassembled contigs                  3                                                                                                    
Mis_Misassembled contigs length            3681                                                                                                 
#Mis_local misassemblies                   0                                                                                                    
#Mis_short indels (<= 5 bp)                28                                                                                                   
#Mis_long indels (> 5 bp)                  0                                                                                                    
#Una_fully unaligned contigs               1                                                                                                    
Una_Fully unaligned length                 620                                                                                                  
#Una_partially unaligned contigs           1                                                                                                    
#Una_with misassembly                      0                                                                                                    
#Una_both parts are significant            0                                                                                                    
Una_Partially unaligned length             61                                                                                                   
GAGE_Contigs #                             714                                                                                                  
GAGE_Min contig                            218                                                                                                  
GAGE_Max contig                            2666                                                                                                 
GAGE_N50                                   0 COUNT: 0                                                                                           
GAGE_Genome size                           5594470                                                                                              
GAGE_Assembly size                         339125                                                                                               
GAGE_Chaff bases                           0                                                                                                    
GAGE_Missing reference bases               5050758(90.28%)                                                                                      
GAGE_Missing assembly bases                701(0.21%)                                                                                           
GAGE_Missing assembly contigs              1(0.14%)                                                                                             
GAGE_Duplicated reference bases            0                                                                                                    
GAGE_Compressed reference bases            202613                                                                                               
GAGE_Bad trim                              81                                                                                                   
GAGE_Avg idy                               99.81                                                                                                
GAGE_SNPs                                  425                                                                                                  
GAGE_Indels < 5bp                          30                                                                                                   
GAGE_Indels >= 5                           0                                                                                                    
GAGE_Inversions                            2                                                                                                    
GAGE_Relocation                            4                                                                                                    
GAGE_Translocation                         0                                                                                                    
GAGE_Corrected contig #                    721                                                                                                  
GAGE_Corrected assembly size               342338                                                                                               
GAGE_Min correct contig                    206                                                                                                  
GAGE_Max correct contig                    2572                                                                                                 
GAGE_Corrected N50                         0 COUNT: 0                                                                                           
