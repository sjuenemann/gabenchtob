All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.96coverage_out.unpadded
#Mis_misassemblies               327                                                                                
#Mis_relocations                 40                                                                                 
#Mis_translocations              2                                                                                  
#Mis_inversions                  285                                                                                
#Mis_misassembled contigs        286                                                                                
Mis_Misassembled contigs length  312873                                                                             
#Mis_local misassemblies         40                                                                                 
#mismatches                      150                                                                                
#indels                          218                                                                                
#Mis_short indels (<= 5 bp)      209                                                                                
#Mis_long indels (> 5 bp)        9                                                                                  
Indels length                    395                                                                                
