All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.96coverage_out.unpadded
GAGE_Contigs #                   1184                                                                               
GAGE_Min contig                  200                                                                                
GAGE_Max contig                  288196                                                                             
GAGE_N50                         92524 COUNT: 9                                                                     
GAGE_Genome size                 2813862                                                                            
GAGE_Assembly size               3216918                                                                            
GAGE_Chaff bases                 0                                                                                  
GAGE_Missing reference bases     159(0.01%)                                                                         
GAGE_Missing assembly bases      37612(1.17%)                                                                       
GAGE_Missing assembly contigs    16(1.35%)                                                                          
GAGE_Duplicated reference bases  366357                                                                             
GAGE_Compressed reference bases  2685                                                                               
GAGE_Bad trim                    31970                                                                              
GAGE_Avg idy                     99.98                                                                              
GAGE_SNPs                        64                                                                                 
GAGE_Indels < 5bp                142                                                                                
GAGE_Indels >= 5                 3                                                                                  
GAGE_Inversions                  4                                                                                  
GAGE_Relocation                  4                                                                                  
GAGE_Translocation               0                                                                                  
GAGE_Corrected contig #          93                                                                                 
GAGE_Corrected assembly size     2834042                                                                            
GAGE_Min correct contig          253                                                                                
GAGE_Max correct contig          280396                                                                             
GAGE_Corrected N50               83340 COUNT: 10                                                                    
