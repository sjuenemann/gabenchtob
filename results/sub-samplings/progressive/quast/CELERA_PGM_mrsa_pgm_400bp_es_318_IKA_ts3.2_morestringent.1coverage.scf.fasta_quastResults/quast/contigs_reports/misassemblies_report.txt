All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.1coverage.scf
#Mis_misassemblies               0                                                                     
#Mis_relocations                 0                                                                     
#Mis_translocations              0                                                                     
#Mis_inversions                  0                                                                     
#Mis_misassembled contigs        0                                                                     
Mis_Misassembled contigs length  0                                                                     
#Mis_local misassemblies         0                                                                     
#mismatches                      0                                                                     
#indels                          5                                                                     
#Mis_short indels (<= 5 bp)      5                                                                     
#Mis_long indels (> 5 bp)        0                                                                     
Indels length                    5                                                                     
