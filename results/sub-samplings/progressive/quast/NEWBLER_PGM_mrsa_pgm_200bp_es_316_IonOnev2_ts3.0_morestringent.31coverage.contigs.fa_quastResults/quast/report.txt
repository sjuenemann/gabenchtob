All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.31coverage.contigs
#Contigs (>= 0 bp)             147                                                                              
#Contigs (>= 1000 bp)          87                                                                               
Total length (>= 0 bp)         2772458                                                                          
Total length (>= 1000 bp)      2758024                                                                          
#Contigs                       111                                                                              
Largest contig                 137187                                                                           
Total length                   2767706                                                                          
Reference length               2813862                                                                          
GC (%)                         32.64                                                                            
Reference GC (%)               32.81                                                                            
N50                            59936                                                                            
NG50                           59936                                                                            
N75                            31862                                                                            
NG75                           30983                                                                            
#misassemblies                 3                                                                                
#local misassemblies           8                                                                                
#unaligned contigs             0 + 1 part                                                                       
Unaligned contigs length       11                                                                               
Genome fraction (%)            97.953                                                                           
Duplication ratio              1.001                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        3.66                                                                             
#indels per 100 kbp            8.82                                                                             
#genes                         2642 + 46 part                                                                   
#predicted genes (unique)      2707                                                                             
#predicted genes (>= 0 bp)     2707                                                                             
#predicted genes (>= 300 bp)   2324                                                                             
#predicted genes (>= 1500 bp)  285                                                                              
#predicted genes (>= 3000 bp)  28                                                                               
Largest alignment              137187                                                                           
NA50                           59880                                                                            
NGA50                          59880                                                                            
NA75                           31862                                                                            
NGA75                          30983                                                                            
