All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.36coverage_44.final
#Mis_misassemblies               2                                                                             
#Mis_relocations                 2                                                                             
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        2                                                                             
Mis_Misassembled contigs length  32011                                                                         
#Mis_local misassemblies         3                                                                             
#mismatches                      76                                                                            
#indels                          1194                                                                          
#Mis_short indels (<= 5 bp)      1194                                                                          
#Mis_long indels (> 5 bp)        0                                                                             
Indels length                    1224                                                                          
