All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.96coverage_128.final
#Contigs (>= 0 bp)             6355                                                                           
#Contigs (>= 1000 bp)          974                                                                            
Total length (>= 0 bp)         6359887                                                                        
Total length (>= 1000 bp)      5019512                                                                        
#Contigs                       4807                                                                           
Largest contig                 29716                                                                          
Total length                   6114979                                                                        
Reference length               5594470                                                                        
GC (%)                         50.57                                                                          
Reference GC (%)               50.48                                                                          
N50                            5911                                                                           
NG50                           6589                                                                           
N75                            2333                                                                           
NG75                           3334                                                                           
#misassemblies                 6                                                                              
#local misassemblies           0                                                                              
#unaligned contigs             0 + 2 part                                                                     
Unaligned contigs length       61                                                                             
Genome fraction (%)            94.834                                                                         
Duplication ratio              1.113                                                                          
#N's per 100 kbp               0.00                                                                           
#mismatches per 100 kbp        0.85                                                                           
#indels per 100 kbp            31.42                                                                          
#genes                         4106 + 1085 part                                                               
#predicted genes (unique)      9891                                                                           
#predicted genes (>= 0 bp)     9989                                                                           
#predicted genes (>= 300 bp)   5032                                                                           
#predicted genes (>= 1500 bp)  445                                                                            
#predicted genes (>= 3000 bp)  22                                                                             
Largest alignment              29716                                                                          
NA50                           5911                                                                           
NGA50                          6589                                                                           
NA75                           2321                                                                           
NGA75                          3334                                                                           
