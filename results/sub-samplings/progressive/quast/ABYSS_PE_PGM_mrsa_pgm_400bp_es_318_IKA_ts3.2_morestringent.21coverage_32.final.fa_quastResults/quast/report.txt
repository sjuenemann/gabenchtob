All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.21coverage_32.final
#Contigs (>= 0 bp)             4703                                                                          
#Contigs (>= 1000 bp)          610                                                                           
Total length (>= 0 bp)         2898449                                                                       
Total length (>= 1000 bp)      2510209                                                                       
#Contigs                       988                                                                           
Largest contig                 28205                                                                         
Total length                   2696586                                                                       
Reference length               2813862                                                                       
GC (%)                         32.62                                                                         
Reference GC (%)               32.81                                                                         
N50                            5299                                                                          
NG50                           5028                                                                          
N75                            2644                                                                          
NG75                           2364                                                                          
#misassemblies                 0                                                                             
#local misassemblies           2                                                                             
#unaligned contigs             0 + 1 part                                                                    
Unaligned contigs length       24                                                                            
Genome fraction (%)            95.397                                                                        
Duplication ratio              1.003                                                                         
#N's per 100 kbp               0.00                                                                          
#mismatches per 100 kbp        1.42                                                                          
#indels per 100 kbp            10.06                                                                         
#genes                         2049 + 557 part                                                               
#predicted genes (unique)      3146                                                                          
#predicted genes (>= 0 bp)     3146                                                                          
#predicted genes (>= 300 bp)   2428                                                                          
#predicted genes (>= 1500 bp)  235                                                                           
#predicted genes (>= 3000 bp)  17                                                                            
Largest alignment              28205                                                                         
NA50                           5299                                                                          
NGA50                          5028                                                                          
NA75                           2644                                                                          
NGA75                          2362                                                                          
