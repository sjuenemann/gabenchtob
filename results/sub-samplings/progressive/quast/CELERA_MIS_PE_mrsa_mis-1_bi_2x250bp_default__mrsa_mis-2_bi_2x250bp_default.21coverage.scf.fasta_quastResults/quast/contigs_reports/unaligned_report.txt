All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CELERA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.21coverage.scf_broken  CELERA_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.21coverage.scf
#Una_fully unaligned contigs      1                                                                                                 1                                                                                        
Una_Fully unaligned length        5621                                                                                              5621                                                                                     
#Una_partially unaligned contigs  1                                                                                                 1                                                                                        
#Una_with misassembly             0                                                                                                 0                                                                                        
#Una_both parts are significant   0                                                                                                 0                                                                                        
Una_Partially unaligned length    45                                                                                                45                                                                                       
#N's                              9                                                                                                 89                                                                                       
