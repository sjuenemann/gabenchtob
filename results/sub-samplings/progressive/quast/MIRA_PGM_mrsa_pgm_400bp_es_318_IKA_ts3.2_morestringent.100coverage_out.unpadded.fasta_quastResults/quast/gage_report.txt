All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.100coverage_out.unpadded
GAGE_Contigs #                   1250                                                                           
GAGE_Min contig                  205                                                                            
GAGE_Max contig                  396723                                                                         
GAGE_N50                         238225 COUNT: 5                                                                
GAGE_Genome size                 2813862                                                                        
GAGE_Assembly size               3486596                                                                        
GAGE_Chaff bases                 0                                                                              
GAGE_Missing reference bases     629(0.02%)                                                                     
GAGE_Missing assembly bases      5584(0.16%)                                                                    
GAGE_Missing assembly contigs    3(0.24%)                                                                       
GAGE_Duplicated reference bases  676157                                                                         
GAGE_Compressed reference bases  7234                                                                           
GAGE_Bad trim                    4214                                                                           
GAGE_Avg idy                     99.98                                                                          
GAGE_SNPs                        47                                                                             
GAGE_Indels < 5bp                48                                                                             
GAGE_Indels >= 5                 4                                                                              
GAGE_Inversions                  4                                                                              
GAGE_Relocation                  3                                                                              
GAGE_Translocation               0                                                                              
GAGE_Corrected contig #          54                                                                             
GAGE_Corrected assembly size     2829096                                                                        
GAGE_Min correct contig          233                                                                            
GAGE_Max correct contig          383165                                                                         
GAGE_Corrected N50               151463 COUNT: 6                                                                
