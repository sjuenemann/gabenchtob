All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.61coverage_out.unpadded
#Contigs                                   248                                                                                                     
#Contigs (>= 0 bp)                         248                                                                                                     
#Contigs (>= 1000 bp)                      108                                                                                                     
Largest contig                             402308                                                                                                  
Total length                               5637833                                                                                                 
Total length (>= 0 bp)                     5637833                                                                                                 
Total length (>= 1000 bp)                  5552808                                                                                                 
Reference length                           5594470                                                                                                 
N50                                        162301                                                                                                  
NG50                                       162301                                                                                                  
N75                                        75871                                                                                                   
NG75                                       81883                                                                                                   
L50                                        12                                                                                                      
LG50                                       12                                                                                                      
L75                                        24                                                                                                      
LG75                                       23                                                                                                      
#local misassemblies                       16                                                                                                      
#misassemblies                             61                                                                                                      
#misassembled contigs                      30                                                                                                      
Misassembled contigs length                2905708                                                                                                 
Misassemblies inter-contig overlap         84969                                                                                                   
#unaligned contigs                         2 + 4 part                                                                                              
Unaligned contigs length                   6849                                                                                                    
#ambiguously mapped contigs                35                                                                                                      
Extra bases in ambiguously mapped contigs  -42293                                                                                                  
#N's                                       100                                                                                                     
#N's per 100 kbp                           1.77                                                                                                    
Genome fraction (%)                        98.548                                                                                                  
Duplication ratio                          1.029                                                                                                   
#genes                                     5320 + 81 part                                                                                          
#predicted genes (unique)                  5528                                                                                                    
#predicted genes (>= 0 bp)                 5570                                                                                                    
#predicted genes (>= 300 bp)               4703                                                                                                    
#predicted genes (>= 1500 bp)              698                                                                                                     
#predicted genes (>= 3000 bp)              76                                                                                                      
#mismatches                                2402                                                                                                    
#indels                                    145                                                                                                     
Indels length                              208                                                                                                     
#mismatches per 100 kbp                    43.57                                                                                                   
#indels per 100 kbp                        2.63                                                                                                    
GC (%)                                     50.49                                                                                                   
Reference GC (%)                           50.48                                                                                                   
Average %IDY                               98.498                                                                                                  
Largest alignment                          305707                                                                                                  
NA50                                       133613                                                                                                  
NGA50                                      133613                                                                                                  
NA75                                       65108                                                                                                   
NGA75                                      65965                                                                                                   
LA50                                       16                                                                                                      
LGA50                                      16                                                                                                      
LA75                                       32                                                                                                      
LGA75                                      31                                                                                                      
#Mis_misassemblies                         61                                                                                                      
#Mis_relocations                           60                                                                                                      
#Mis_translocations                        1                                                                                                       
#Mis_inversions                            0                                                                                                       
#Mis_misassembled contigs                  30                                                                                                      
Mis_Misassembled contigs length            2905708                                                                                                 
#Mis_local misassemblies                   16                                                                                                      
#Mis_short indels (<= 5 bp)                142                                                                                                     
#Mis_long indels (> 5 bp)                  3                                                                                                       
#Una_fully unaligned contigs               2                                                                                                       
Una_Fully unaligned length                 6410                                                                                                    
#Una_partially unaligned contigs           4                                                                                                       
#Una_with misassembly                      0                                                                                                       
#Una_both parts are significant            0                                                                                                       
Una_Partially unaligned length             439                                                                                                     
GAGE_Contigs #                             248                                                                                                     
GAGE_Min contig                            244                                                                                                     
GAGE_Max contig                            402308                                                                                                  
GAGE_N50                                   162301 COUNT: 12                                                                                        
GAGE_Genome size                           5594470                                                                                                 
GAGE_Assembly size                         5637833                                                                                                 
GAGE_Chaff bases                           0                                                                                                       
GAGE_Missing reference bases               556(0.01%)                                                                                              
GAGE_Missing assembly bases                6783(0.12%)                                                                                             
GAGE_Missing assembly contigs              2(0.81%)                                                                                                
GAGE_Duplicated reference bases            152151                                                                                                  
GAGE_Compressed reference bases            145373                                                                                                  
GAGE_Bad trim                              371                                                                                                     
GAGE_Avg idy                               99.95                                                                                                   
GAGE_SNPs                                  343                                                                                                     
GAGE_Indels < 5bp                          42                                                                                                      
GAGE_Indels >= 5                           4                                                                                                       
GAGE_Inversions                            24                                                                                                      
GAGE_Relocation                            9                                                                                                       
GAGE_Translocation                         2                                                                                                       
GAGE_Corrected contig #                    137                                                                                                     
GAGE_Corrected assembly size               5557453                                                                                                 
GAGE_Min correct contig                    244                                                                                                     
GAGE_Max correct contig                    309039                                                                                                  
GAGE_Corrected N50                         107548 COUNT: 17                                                                                        
