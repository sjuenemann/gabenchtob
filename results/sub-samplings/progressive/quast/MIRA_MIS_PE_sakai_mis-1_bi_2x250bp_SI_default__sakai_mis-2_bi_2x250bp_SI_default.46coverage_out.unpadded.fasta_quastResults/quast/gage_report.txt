All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.46coverage_out.unpadded
GAGE_Contigs #                   205                                                                                                     
GAGE_Min contig                  309                                                                                                     
GAGE_Max contig                  466677                                                                                                  
GAGE_N50                         162373 COUNT: 12                                                                                        
GAGE_Genome size                 5594470                                                                                                 
GAGE_Assembly size               5606154                                                                                                 
GAGE_Chaff bases                 0                                                                                                       
GAGE_Missing reference bases     275(0.00%)                                                                                              
GAGE_Missing assembly bases      6665(0.12%)                                                                                             
GAGE_Missing assembly contigs    2(0.98%)                                                                                                
GAGE_Duplicated reference bases  143948                                                                                                  
GAGE_Compressed reference bases  146783                                                                                                  
GAGE_Bad trim                    421                                                                                                     
GAGE_Avg idy                     99.95                                                                                                   
GAGE_SNPs                        478                                                                                                     
GAGE_Indels < 5bp                27                                                                                                      
GAGE_Indels >= 5                 4                                                                                                       
GAGE_Inversions                  20                                                                                                      
GAGE_Relocation                  13                                                                                                      
GAGE_Translocation               0                                                                                                       
GAGE_Corrected contig #          141                                                                                                     
GAGE_Corrected assembly size     5565550                                                                                                 
GAGE_Min correct contig          372                                                                                                     
GAGE_Max correct contig          313106                                                                                                  
GAGE_Corrected N50               123610 COUNT: 16                                                                                        
