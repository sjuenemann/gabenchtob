All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.86coverage_out.unpadded
#Una_fully unaligned contigs      23                                                                                 
Una_Fully unaligned length        8129                                                                               
#Una_partially unaligned contigs  341                                                                                
#Una_with misassembly             0                                                                                  
#Una_both parts are significant   2                                                                                  
Una_Partially unaligned length    21072                                                                              
#N's                              1378                                                                               
