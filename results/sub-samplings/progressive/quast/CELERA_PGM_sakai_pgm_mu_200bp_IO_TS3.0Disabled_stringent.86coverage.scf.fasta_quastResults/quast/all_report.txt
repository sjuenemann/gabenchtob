All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.86coverage.scf
#Contigs                                   730                                                                    
#Contigs (>= 0 bp)                         730                                                                    
#Contigs (>= 1000 bp)                      730                                                                    
Largest contig                             64150                                                                  
Total length                               5256855                                                                
Total length (>= 0 bp)                     5256855                                                                
Total length (>= 1000 bp)                  5256855                                                                
Reference length                           5594470                                                                
N50                                        16690                                                                  
NG50                                       15867                                                                  
N75                                        7011                                                                   
NG75                                       5222                                                                   
L50                                        99                                                                     
LG50                                       109                                                                    
L75                                        220                                                                    
LG75                                       261                                                                    
#local misassemblies                       7                                                                      
#misassemblies                             4                                                                      
#misassembled contigs                      4                                                                      
Misassembled contigs length                50541                                                                  
Misassemblies inter-contig overlap         2607                                                                   
#unaligned contigs                         0 + 5 part                                                             
Unaligned contigs length                   197                                                                    
#ambiguously mapped contigs                1                                                                      
Extra bases in ambiguously mapped contigs  -1231                                                                  
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        92.221                                                                 
Duplication ratio                          1.019                                                                  
#genes                                     4477 + 584 part                                                        
#predicted genes (unique)                  5853                                                                   
#predicted genes (>= 0 bp)                 5854                                                                   
#predicted genes (>= 300 bp)               4707                                                                   
#predicted genes (>= 1500 bp)              537                                                                    
#predicted genes (>= 3000 bp)              38                                                                     
#mismatches                                122                                                                    
#indels                                    900                                                                    
Indels length                              916                                                                    
#mismatches per 100 kbp                    2.36                                                                   
#indels per 100 kbp                        17.44                                                                  
GC (%)                                     50.22                                                                  
Reference GC (%)                           50.48                                                                  
Average %IDY                               99.186                                                                 
Largest alignment                          64150                                                                  
NA50                                       16667                                                                  
NGA50                                      15816                                                                  
NA75                                       7011                                                                   
NGA75                                      5222                                                                   
LA50                                       100                                                                    
LGA50                                      110                                                                    
LA75                                       221                                                                    
LGA75                                      262                                                                    
#Mis_misassemblies                         4                                                                      
#Mis_relocations                           4                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  4                                                                      
Mis_Misassembled contigs length            50541                                                                  
#Mis_local misassemblies                   7                                                                      
#Mis_short indels (<= 5 bp)                899                                                                    
#Mis_long indels (> 5 bp)                  1                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           5                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             197                                                                    
GAGE_Contigs #                             730                                                                    
GAGE_Min contig                            1002                                                                   
GAGE_Max contig                            64150                                                                  
GAGE_N50                                   15867 COUNT: 109                                                       
GAGE_Genome size                           5594470                                                                
GAGE_Assembly size                         5256855                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               364328(6.51%)                                                          
GAGE_Missing assembly bases                698(0.01%)                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            132                                                                    
GAGE_Compressed reference bases            95491                                                                  
GAGE_Bad trim                              666                                                                    
GAGE_Avg idy                               99.98                                                                  
GAGE_SNPs                                  70                                                                     
GAGE_Indels < 5bp                          801                                                                    
GAGE_Indels >= 5                           10                                                                     
GAGE_Inversions                            1                                                                      
GAGE_Relocation                            2                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    742                                                                    
GAGE_Corrected assembly size               5259520                                                                
GAGE_Min correct contig                    503                                                                    
GAGE_Max correct contig                    57496                                                                  
GAGE_Corrected N50                         15510 COUNT: 112                                                       
