All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.31coverage_out.unpadded
#Mis_misassemblies               16                                                                            
#Mis_relocations                 16                                                                            
#Mis_translocations              0                                                                             
#Mis_inversions                  0                                                                             
#Mis_misassembled contigs        15                                                                            
Mis_Misassembled contigs length  1351220                                                                       
#Mis_local misassemblies         14                                                                            
#mismatches                      220                                                                           
#indels                          178                                                                           
#Mis_short indels (<= 5 bp)      174                                                                           
#Mis_long indels (> 5 bp)        4                                                                             
Indels length                    258                                                                           
