All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.26coverage.contigs
#Contigs                                   357                                                                                                   
#Contigs (>= 0 bp)                         427                                                                                                   
#Contigs (>= 1000 bp)                      173                                                                                                   
Largest contig                             259768                                                                                                
Total length                               5318492                                                                                               
Total length (>= 0 bp)                     5328344                                                                                               
Total length (>= 1000 bp)                  5238870                                                                                               
Reference length                           5594470                                                                                               
N50                                        99570                                                                                                 
NG50                                       95700                                                                                                 
N75                                        46281                                                                                                 
NG75                                       37509                                                                                                 
L50                                        16                                                                                                    
LG50                                       17                                                                                                    
L75                                        34                                                                                                    
LG75                                       38                                                                                                    
#local misassemblies                       3                                                                                                     
#misassemblies                             2                                                                                                     
#misassembled contigs                      2                                                                                                     
Misassembled contigs length                206081                                                                                                
Misassemblies inter-contig overlap         258                                                                                                   
#unaligned contigs                         1 + 1 part                                                                                            
Unaligned contigs length                   5429                                                                                                  
#ambiguously mapped contigs                114                                                                                                   
Extra bases in ambiguously mapped contigs  -91921                                                                                                
#N's                                       0                                                                                                     
#N's per 100 kbp                           0.00                                                                                                  
Genome fraction (%)                        93.317                                                                                                
Duplication ratio                          1.000                                                                                                 
#genes                                     4885 + 181 part                                                                                       
#predicted genes (unique)                  5282                                                                                                  
#predicted genes (>= 0 bp)                 5282                                                                                                  
#predicted genes (>= 300 bp)               4433                                                                                                  
#predicted genes (>= 1500 bp)              660                                                                                                   
#predicted genes (>= 3000 bp)              69                                                                                                    
#mismatches                                88                                                                                                    
#indels                                    17                                                                                                    
Indels length                              17                                                                                                    
#mismatches per 100 kbp                    1.69                                                                                                  
#indels per 100 kbp                        0.33                                                                                                  
GC (%)                                     50.28                                                                                                 
Reference GC (%)                           50.48                                                                                                 
Average %IDY                               98.977                                                                                                
Largest alignment                          259768                                                                                                
NA50                                       99570                                                                                                 
NGA50                                      95700                                                                                                 
NA75                                       46281                                                                                                 
NGA75                                      32923                                                                                                 
LA50                                       16                                                                                                    
LGA50                                      17                                                                                                    
LA75                                       34                                                                                                    
LGA75                                      39                                                                                                    
#Mis_misassemblies                         2                                                                                                     
#Mis_relocations                           2                                                                                                     
#Mis_translocations                        0                                                                                                     
#Mis_inversions                            0                                                                                                     
#Mis_misassembled contigs                  2                                                                                                     
Mis_Misassembled contigs length            206081                                                                                                
#Mis_local misassemblies                   3                                                                                                     
#Mis_short indels (<= 5 bp)                17                                                                                                    
#Mis_long indels (> 5 bp)                  0                                                                                                     
#Una_fully unaligned contigs               1                                                                                                     
Una_Fully unaligned length                 5386                                                                                                  
#Una_partially unaligned contigs           1                                                                                                     
#Una_with misassembly                      0                                                                                                     
#Una_both parts are significant            0                                                                                                     
Una_Partially unaligned length             43                                                                                                    
GAGE_Contigs #                             357                                                                                                   
GAGE_Min contig                            200                                                                                                   
GAGE_Max contig                            259768                                                                                                
GAGE_N50                                   95700 COUNT: 17                                                                                       
GAGE_Genome size                           5594470                                                                                               
GAGE_Assembly size                         5318492                                                                                               
GAGE_Chaff bases                           0                                                                                                     
GAGE_Missing reference bases               40011(0.72%)                                                                                          
GAGE_Missing assembly bases                5436(0.10%)                                                                                           
GAGE_Missing assembly contigs              1(0.28%)                                                                                              
GAGE_Duplicated reference bases            281                                                                                                   
GAGE_Compressed reference bases            251777                                                                                                
GAGE_Bad trim                              49                                                                                                    
GAGE_Avg idy                               99.99                                                                                                 
GAGE_SNPs                                  182                                                                                                   
GAGE_Indels < 5bp                          18                                                                                                    
GAGE_Indels >= 5                           4                                                                                                     
GAGE_Inversions                            0                                                                                                     
GAGE_Relocation                            2                                                                                                     
GAGE_Translocation                         0                                                                                                     
GAGE_Corrected contig #                    360                                                                                                   
GAGE_Corrected assembly size               5312935                                                                                               
GAGE_Min correct contig                    200                                                                                                   
GAGE_Max correct contig                    259768                                                                                                
GAGE_Corrected N50                         95700 COUNT: 17                                                                                       
