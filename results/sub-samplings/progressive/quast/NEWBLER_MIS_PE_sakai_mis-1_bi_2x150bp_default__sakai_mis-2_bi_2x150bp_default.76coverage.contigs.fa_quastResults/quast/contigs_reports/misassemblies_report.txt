All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.76coverage.contigs
#Mis_misassemblies               3                                                                                               
#Mis_relocations                 3                                                                                               
#Mis_translocations              0                                                                                               
#Mis_inversions                  0                                                                                               
#Mis_misassembled contigs        3                                                                                               
Mis_Misassembled contigs length  209383                                                                                          
#Mis_local misassemblies         3                                                                                               
#mismatches                      55                                                                                              
#indels                          14                                                                                              
#Mis_short indels (<= 5 bp)      14                                                                                              
#Mis_long indels (> 5 bp)        0                                                                                               
Indels length                    14                                                                                              
