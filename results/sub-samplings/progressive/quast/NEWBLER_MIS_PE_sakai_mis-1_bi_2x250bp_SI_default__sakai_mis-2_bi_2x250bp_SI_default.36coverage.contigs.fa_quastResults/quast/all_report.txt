All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.36coverage.contigs
#Contigs                                   327                                                                                                   
#Contigs (>= 0 bp)                         404                                                                                                   
#Contigs (>= 1000 bp)                      166                                                                                                   
Largest contig                             259768                                                                                                
Total length                               5320347                                                                                               
Total length (>= 0 bp)                     5331233                                                                                               
Total length (>= 1000 bp)                  5247768                                                                                               
Reference length                           5594470                                                                                               
N50                                        92039                                                                                                 
NG50                                       87708                                                                                                 
N75                                        54991                                                                                                 
NG75                                       42802                                                                                                 
L50                                        18                                                                                                    
LG50                                       19                                                                                                    
L75                                        36                                                                                                    
LG75                                       40                                                                                                    
#local misassemblies                       3                                                                                                     
#misassemblies                             3                                                                                                     
#misassembled contigs                      3                                                                                                     
Misassembled contigs length                209391                                                                                                
Misassemblies inter-contig overlap         682                                                                                                   
#unaligned contigs                         3 + 0 part                                                                                            
Unaligned contigs length                   6032                                                                                                  
#ambiguously mapped contigs                103                                                                                                   
Extra bases in ambiguously mapped contigs  -87010                                                                                                
#N's                                       0                                                                                                     
#N's per 100 kbp                           0.00                                                                                                  
Genome fraction (%)                        93.436                                                                                                
Duplication ratio                          1.000                                                                                                 
#genes                                     4895 + 173 part                                                                                       
#predicted genes (unique)                  5253                                                                                                  
#predicted genes (>= 0 bp)                 5253                                                                                                  
#predicted genes (>= 300 bp)               4416                                                                                                  
#predicted genes (>= 1500 bp)              665                                                                                                   
#predicted genes (>= 3000 bp)              68                                                                                                    
#mismatches                                147                                                                                                   
#indels                                    16                                                                                                    
Indels length                              21                                                                                                    
#mismatches per 100 kbp                    2.81                                                                                                  
#indels per 100 kbp                        0.31                                                                                                  
GC (%)                                     50.28                                                                                                 
Reference GC (%)                           50.48                                                                                                 
Average %IDY                               98.945                                                                                                
Largest alignment                          259768                                                                                                
NA50                                       92039                                                                                                 
NGA50                                      87708                                                                                                 
NA75                                       54991                                                                                                 
NGA75                                      40885                                                                                                 
LA50                                       18                                                                                                    
LGA50                                      19                                                                                                    
LA75                                       36                                                                                                    
LGA75                                      41                                                                                                    
#Mis_misassemblies                         3                                                                                                     
#Mis_relocations                           3                                                                                                     
#Mis_translocations                        0                                                                                                     
#Mis_inversions                            0                                                                                                     
#Mis_misassembled contigs                  3                                                                                                     
Mis_Misassembled contigs length            209391                                                                                                
#Mis_local misassemblies                   3                                                                                                     
#Mis_short indels (<= 5 bp)                15                                                                                                    
#Mis_long indels (> 5 bp)                  1                                                                                                     
#Una_fully unaligned contigs               3                                                                                                     
Una_Fully unaligned length                 6032                                                                                                  
#Una_partially unaligned contigs           0                                                                                                     
#Una_with misassembly                      0                                                                                                     
#Una_both parts are significant            0                                                                                                     
Una_Partially unaligned length             0                                                                                                     
GAGE_Contigs #                             327                                                                                                   
GAGE_Min contig                            200                                                                                                   
GAGE_Max contig                            259768                                                                                                
GAGE_N50                                   87708 COUNT: 19                                                                                       
GAGE_Genome size                           5594470                                                                                               
GAGE_Assembly size                         5320347                                                                                               
GAGE_Chaff bases                           0                                                                                                     
GAGE_Missing reference bases               45876(0.82%)                                                                                          
GAGE_Missing assembly bases                6050(0.11%)                                                                                           
GAGE_Missing assembly contigs              3(0.92%)                                                                                              
GAGE_Duplicated reference bases            0                                                                                                     
GAGE_Compressed reference bases            250311                                                                                                
GAGE_Bad trim                              18                                                                                                    
GAGE_Avg idy                               99.99                                                                                                 
GAGE_SNPs                                  216                                                                                                   
GAGE_Indels < 5bp                          18                                                                                                    
GAGE_Indels >= 5                           3                                                                                                     
GAGE_Inversions                            0                                                                                                     
GAGE_Relocation                            3                                                                                                     
GAGE_Translocation                         0                                                                                                     
GAGE_Corrected contig #                    330                                                                                                   
GAGE_Corrected assembly size               5314983                                                                                               
GAGE_Min correct contig                    200                                                                                                   
GAGE_Max correct contig                    259768                                                                                                
GAGE_Corrected N50                         87302 COUNT: 20                                                                                       
