All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.41coverage.K99.final-contigs
#Contigs                                   93                                                                                        
#Contigs (>= 0 bp)                         206                                                                                       
#Contigs (>= 1000 bp)                      71                                                                                        
Largest contig                             283614                                                                                    
Total length                               2769666                                                                                   
Total length (>= 0 bp)                     2785090                                                                                   
Total length (>= 1000 bp)                  2760800                                                                                   
Reference length                           2813862                                                                                   
N50                                        66713                                                                                     
NG50                                       66713                                                                                     
N75                                        39335                                                                                     
NG75                                       39335                                                                                     
L50                                        11                                                                                        
LG50                                       11                                                                                        
L75                                        25                                                                                        
LG75                                       25                                                                                        
#local misassemblies                       7                                                                                         
#misassemblies                             7                                                                                         
#misassembled contigs                      5                                                                                         
Misassembled contigs length                496243                                                                                    
Misassemblies inter-contig overlap         1550                                                                                      
#unaligned contigs                         0 + 2 part                                                                                
Unaligned contigs length                   97                                                                                        
#ambiguously mapped contigs                14                                                                                        
Extra bases in ambiguously mapped contigs  -8647                                                                                     
#N's                                       0                                                                                         
#N's per 100 kbp                           0.00                                                                                      
Genome fraction (%)                        98.021                                                                                    
Duplication ratio                          1.002                                                                                     
#genes                                     2625 + 40 part                                                                            
#predicted genes (unique)                  2739                                                                                      
#predicted genes (>= 0 bp)                 2742                                                                                      
#predicted genes (>= 300 bp)               2322                                                                                      
#predicted genes (>= 1500 bp)              284                                                                                       
#predicted genes (>= 3000 bp)              23                                                                                        
#mismatches                                190                                                                                       
#indels                                    494                                                                                       
Indels length                              604                                                                                       
#mismatches per 100 kbp                    6.89                                                                                      
#indels per 100 kbp                        17.91                                                                                     
GC (%)                                     32.65                                                                                     
Reference GC (%)                           32.81                                                                                     
Average %IDY                               98.779                                                                                    
Largest alignment                          237075                                                                                    
NA50                                       64226                                                                                     
NGA50                                      64226                                                                                     
NA75                                       38333                                                                                     
NGA75                                      37861                                                                                     
LA50                                       13                                                                                        
LGA50                                      13                                                                                        
LA75                                       27                                                                                        
LGA75                                      28                                                                                        
#Mis_misassemblies                         7                                                                                         
#Mis_relocations                           7                                                                                         
#Mis_translocations                        0                                                                                         
#Mis_inversions                            0                                                                                         
#Mis_misassembled contigs                  5                                                                                         
Mis_Misassembled contigs length            496243                                                                                    
#Mis_local misassemblies                   7                                                                                         
#Mis_short indels (<= 5 bp)                492                                                                                       
#Mis_long indels (> 5 bp)                  2                                                                                         
#Una_fully unaligned contigs               0                                                                                         
Una_Fully unaligned length                 0                                                                                         
#Una_partially unaligned contigs           2                                                                                         
#Una_with misassembly                      0                                                                                         
#Una_both parts are significant            0                                                                                         
Una_Partially unaligned length             97                                                                                        
GAGE_Contigs #                             93                                                                                        
GAGE_Min contig                            207                                                                                       
GAGE_Max contig                            283614                                                                                    
GAGE_N50                                   66713 COUNT: 11                                                                           
GAGE_Genome size                           2813862                                                                                   
GAGE_Assembly size                         2769666                                                                                   
GAGE_Chaff bases                           0                                                                                         
GAGE_Missing reference bases               10239(0.36%)                                                                              
GAGE_Missing assembly bases                411(0.01%)                                                                                
GAGE_Missing assembly contigs              0(0.00%)                                                                                  
GAGE_Duplicated reference bases            1227                                                                                      
GAGE_Compressed reference bases            45268                                                                                     
GAGE_Bad trim                              404                                                                                       
GAGE_Avg idy                               99.97                                                                                     
GAGE_SNPs                                  84                                                                                        
GAGE_Indels < 5bp                          491                                                                                       
GAGE_Indels >= 5                           8                                                                                         
GAGE_Inversions                            1                                                                                         
GAGE_Relocation                            4                                                                                         
GAGE_Translocation                         0                                                                                         
GAGE_Corrected contig #                    106                                                                                       
GAGE_Corrected assembly size               2770987                                                                                   
GAGE_Min correct contig                    207                                                                                       
GAGE_Max correct contig                    193770                                                                                    
GAGE_Corrected N50                         59717 COUNT: 16                                                                           
