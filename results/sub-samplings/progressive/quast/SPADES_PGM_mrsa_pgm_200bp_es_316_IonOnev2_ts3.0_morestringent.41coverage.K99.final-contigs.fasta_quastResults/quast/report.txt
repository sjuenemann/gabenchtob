All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.41coverage.K99.final-contigs
#Contigs (>= 0 bp)             206                                                                                       
#Contigs (>= 1000 bp)          71                                                                                        
Total length (>= 0 bp)         2785090                                                                                   
Total length (>= 1000 bp)      2760800                                                                                   
#Contigs                       93                                                                                        
Largest contig                 283614                                                                                    
Total length                   2769666                                                                                   
Reference length               2813862                                                                                   
GC (%)                         32.65                                                                                     
Reference GC (%)               32.81                                                                                     
N50                            66713                                                                                     
NG50                           66713                                                                                     
N75                            39335                                                                                     
NG75                           39335                                                                                     
#misassemblies                 7                                                                                         
#local misassemblies           7                                                                                         
#unaligned contigs             0 + 2 part                                                                                
Unaligned contigs length       97                                                                                        
Genome fraction (%)            98.021                                                                                    
Duplication ratio              1.002                                                                                     
#N's per 100 kbp               0.00                                                                                      
#mismatches per 100 kbp        6.89                                                                                      
#indels per 100 kbp            17.91                                                                                     
#genes                         2625 + 40 part                                                                            
#predicted genes (unique)      2739                                                                                      
#predicted genes (>= 0 bp)     2742                                                                                      
#predicted genes (>= 300 bp)   2322                                                                                      
#predicted genes (>= 1500 bp)  284                                                                                       
#predicted genes (>= 3000 bp)  23                                                                                        
Largest alignment              237075                                                                                    
NA50                           64226                                                                                     
NGA50                          64226                                                                                     
NA75                           38333                                                                                     
NGA75                          37861                                                                                     
