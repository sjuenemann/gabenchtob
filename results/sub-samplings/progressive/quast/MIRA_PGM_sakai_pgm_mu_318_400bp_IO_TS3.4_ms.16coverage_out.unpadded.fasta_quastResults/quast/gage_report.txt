All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.16coverage_out.unpadded
GAGE_Contigs #                   454                                                                
GAGE_Min contig                  200                                                                
GAGE_Max contig                  227331                                                             
GAGE_N50                         76241 COUNT: 22                                                    
GAGE_Genome size                 5594470                                                            
GAGE_Assembly size               5584195                                                            
GAGE_Chaff bases                 0                                                                  
GAGE_Missing reference bases     2464(0.04%)                                                        
GAGE_Missing assembly bases      372(0.01%)                                                         
GAGE_Missing assembly contigs    0(0.00%)                                                           
GAGE_Duplicated reference bases  104654                                                             
GAGE_Compressed reference bases  194846                                                             
GAGE_Bad trim                    371                                                                
GAGE_Avg idy                     99.94                                                              
GAGE_SNPs                        442                                                                
GAGE_Indels < 5bp                2462                                                               
GAGE_Indels >= 5                 13                                                                 
GAGE_Inversions                  7                                                                  
GAGE_Relocation                  9                                                                  
GAGE_Translocation               1                                                                  
GAGE_Corrected contig #          326                                                                
GAGE_Corrected assembly size     5498738                                                            
GAGE_Min correct contig          214                                                                
GAGE_Max correct contig          190366                                                             
GAGE_Corrected N50               54724 COUNT: 29                                                    
