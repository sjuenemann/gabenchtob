All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.96coverage_out.unpadded
#Contigs                                   1906                                                                          
#Contigs (>= 0 bp)                         1955                                                                          
#Contigs (>= 1000 bp)                      196                                                                           
Largest contig                             297401                                                                        
Total length                               6109769                                                                       
Total length (>= 0 bp)                     6117868                                                                       
Total length (>= 1000 bp)                  5505408                                                                       
Reference length                           5594470                                                                       
N50                                        80429                                                                         
NG50                                       94215                                                                         
N75                                        30348                                                                         
NG75                                       42339                                                                         
L50                                        22                                                                            
LG50                                       19                                                                            
L75                                        52                                                                            
LG75                                       42                                                                            
#local misassemblies                       56                                                                            
#misassemblies                             359                                                                           
#misassembled contigs                      320                                                                           
Misassembled contigs length                1981136                                                                       
Misassemblies inter-contig overlap         49467                                                                         
#unaligned contigs                         41 + 963 part                                                                 
Unaligned contigs length                   71021                                                                         
#ambiguously mapped contigs                89                                                                            
Extra bases in ambiguously mapped contigs  -53361                                                                        
#N's                                       2560                                                                          
#N's per 100 kbp                           41.90                                                                         
Genome fraction (%)                        98.012                                                                        
Duplication ratio                          1.101                                                                         
#genes                                     5197 + 184 part                                                               
#predicted genes (unique)                  7132                                                                          
#predicted genes (>= 0 bp)                 7174                                                                          
#predicted genes (>= 300 bp)               4916                                                                          
#predicted genes (>= 1500 bp)              674                                                                           
#predicted genes (>= 3000 bp)              68                                                                            
#mismatches                                1849                                                                          
#indels                                    356                                                                           
Indels length                              429                                                                           
#mismatches per 100 kbp                    33.72                                                                         
#indels per 100 kbp                        6.49                                                                          
GC (%)                                     50.42                                                                         
Reference GC (%)                           50.48                                                                         
Average %IDY                               98.734                                                                        
Largest alignment                          293411                                                                        
NA50                                       68703                                                                         
NGA50                                      78561                                                                         
NA75                                       24585                                                                         
NGA75                                      39143                                                                         
LA50                                       26                                                                            
LGA50                                      22                                                                            
LA75                                       60                                                                            
LGA75                                      48                                                                            
#Mis_misassemblies                         359                                                                           
#Mis_relocations                           55                                                                            
#Mis_translocations                        3                                                                             
#Mis_inversions                            301                                                                           
#Mis_misassembled contigs                  320                                                                           
Mis_Misassembled contigs length            1981136                                                                       
#Mis_local misassemblies                   56                                                                            
#Mis_short indels (<= 5 bp)                354                                                                           
#Mis_long indels (> 5 bp)                  2                                                                             
#Una_fully unaligned contigs               41                                                                            
Una_Fully unaligned length                 16081                                                                         
#Una_partially unaligned contigs           963                                                                           
#Una_with misassembly                      0                                                                             
#Una_both parts are significant            5                                                                             
Una_Partially unaligned length             54940                                                                         
GAGE_Contigs #                             1906                                                                          
GAGE_Min contig                            200                                                                           
GAGE_Max contig                            297401                                                                        
GAGE_N50                                   94215 COUNT: 19                                                               
GAGE_Genome size                           5594470                                                                       
GAGE_Assembly size                         6109769                                                                       
GAGE_Chaff bases                           0                                                                             
GAGE_Missing reference bases               276(0.00%)                                                                    
GAGE_Missing assembly bases                67154(1.10%)                                                                  
GAGE_Missing assembly contigs              21(1.10%)                                                                     
GAGE_Duplicated reference bases            557104                                                                        
GAGE_Compressed reference bases            171125                                                                        
GAGE_Bad trim                              60026                                                                         
GAGE_Avg idy                               99.96                                                                         
GAGE_SNPs                                  374                                                                           
GAGE_Indels < 5bp                          246                                                                           
GAGE_Indels >= 5                           10                                                                            
GAGE_Inversions                            13                                                                            
GAGE_Relocation                            11                                                                            
GAGE_Translocation                         2                                                                             
GAGE_Corrected contig #                    283                                                                           
GAGE_Corrected assembly size               5537965                                                                       
GAGE_Min correct contig                    224                                                                           
GAGE_Max correct contig                    293416                                                                        
GAGE_Corrected N50                         69035 COUNT: 23                                                               
