All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.81coverage.result
#Contigs                                   259                                                                         
#Contigs (>= 0 bp)                         259                                                                         
#Contigs (>= 1000 bp)                      118                                                                         
Largest contig                             192468                                                                      
Total length                               2783064                                                                     
Total length (>= 0 bp)                     2783064                                                                     
Total length (>= 1000 bp)                  2740793                                                                     
Reference length                           2813862                                                                     
N50                                        40141                                                                       
NG50                                       39883                                                                       
N75                                        19664                                                                       
NG75                                       19416                                                                       
L50                                        22                                                                          
LG50                                       23                                                                          
L75                                        45                                                                          
LG75                                       46                                                                          
#local misassemblies                       4                                                                           
#misassemblies                             12                                                                          
#misassembled contigs                      12                                                                          
Misassembled contigs length                46782                                                                       
Misassemblies inter-contig overlap         844                                                                         
#unaligned contigs                         1 + 17 part                                                                 
Unaligned contigs length                   866                                                                         
#ambiguously mapped contigs                11                                                                          
Extra bases in ambiguously mapped contigs  -8538                                                                       
#N's                                       68                                                                          
#N's per 100 kbp                           2.44                                                                        
Genome fraction (%)                        97.792                                                                      
Duplication ratio                          1.008                                                                       
#genes                                     2619 + 69 part                                                              
#predicted genes (unique)                  2821                                                                        
#predicted genes (>= 0 bp)                 2822                                                                        
#predicted genes (>= 300 bp)               2326                                                                        
#predicted genes (>= 1500 bp)              284                                                                         
#predicted genes (>= 3000 bp)              26                                                                          
#mismatches                                68                                                                          
#indels                                    332                                                                         
Indels length                              348                                                                         
#mismatches per 100 kbp                    2.47                                                                        
#indels per 100 kbp                        12.07                                                                       
GC (%)                                     32.63                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.303                                                                      
Largest alignment                          192468                                                                      
NA50                                       40141                                                                       
NGA50                                      39883                                                                       
NA75                                       19664                                                                       
NGA75                                      19416                                                                       
LA50                                       22                                                                          
LGA50                                      23                                                                          
LA75                                       45                                                                          
LGA75                                      46                                                                          
#Mis_misassemblies                         12                                                                          
#Mis_relocations                           3                                                                           
#Mis_translocations                        1                                                                           
#Mis_inversions                            8                                                                           
#Mis_misassembled contigs                  12                                                                          
Mis_Misassembled contigs length            46782                                                                       
#Mis_local misassemblies                   4                                                                           
#Mis_short indels (<= 5 bp)                332                                                                         
#Mis_long indels (> 5 bp)                  0                                                                           
#Una_fully unaligned contigs               1                                                                           
Una_Fully unaligned length                 233                                                                         
#Una_partially unaligned contigs           17                                                                          
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             633                                                                         
GAGE_Contigs #                             259                                                                         
GAGE_Min contig                            202                                                                         
GAGE_Max contig                            192468                                                                      
GAGE_N50                                   39883 COUNT: 23                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2783064                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               20563(0.73%)                                                                
GAGE_Missing assembly bases                948(0.03%)                                                                  
GAGE_Missing assembly contigs              1(0.39%)                                                                    
GAGE_Duplicated reference bases            21482                                                                       
GAGE_Compressed reference bases            37186                                                                       
GAGE_Bad trim                              713                                                                         
GAGE_Avg idy                               99.98                                                                       
GAGE_SNPs                                  47                                                                          
GAGE_Indels < 5bp                          336                                                                         
GAGE_Indels >= 5                           1                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            4                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    175                                                                         
GAGE_Corrected assembly size               2761297                                                                     
GAGE_Min correct contig                    205                                                                         
GAGE_Max correct contig                    192478                                                                      
GAGE_Corrected N50                         39886 COUNT: 23                                                             
