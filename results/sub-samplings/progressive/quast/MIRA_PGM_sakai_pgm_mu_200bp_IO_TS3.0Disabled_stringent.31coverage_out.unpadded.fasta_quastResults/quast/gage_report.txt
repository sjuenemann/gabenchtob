All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.31coverage_out.unpadded
GAGE_Contigs #                   431                                                                           
GAGE_Min contig                  202                                                                           
GAGE_Max contig                  315496                                                                        
GAGE_N50                         102279 COUNT: 18                                                              
GAGE_Genome size                 5594470                                                                       
GAGE_Assembly size               5557112                                                                       
GAGE_Chaff bases                 0                                                                             
GAGE_Missing reference bases     437(0.01%)                                                                    
GAGE_Missing assembly bases      4270(0.08%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                      
GAGE_Duplicated reference bases  95336                                                                         
GAGE_Compressed reference bases  204440                                                                        
GAGE_Bad trim                    4270                                                                          
GAGE_Avg idy                     99.96                                                                         
GAGE_SNPs                        581                                                                           
GAGE_Indels < 5bp                522                                                                           
GAGE_Indels >= 5                 12                                                                            
GAGE_Inversions                  9                                                                             
GAGE_Relocation                  8                                                                             
GAGE_Translocation               1                                                                             
GAGE_Corrected contig #          287                                                                           
GAGE_Corrected assembly size     5489334                                                                       
GAGE_Min correct contig          221                                                                           
GAGE_Max correct contig          315507                                                                        
GAGE_Corrected N50               76772 COUNT: 22                                                               
