All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.91coverage.result
#Contigs (>= 0 bp)             482                                                                    
#Contigs (>= 1000 bp)          137                                                                    
Total length (>= 0 bp)         2838726                                                                
Total length (>= 1000 bp)      2722933                                                                
#Contigs                       482                                                                    
Largest contig                 120775                                                                 
Total length                   2838726                                                                
Reference length               2813862                                                                
GC (%)                         32.61                                                                  
Reference GC (%)               32.81                                                                  
N50                            32479                                                                  
NG50                           33228                                                                  
N75                            16636                                                                  
NG75                           17214                                                                  
#misassemblies                 6                                                                      
#local misassemblies           3                                                                      
#unaligned contigs             11 + 8 part                                                            
Unaligned contigs length       4767                                                                   
Genome fraction (%)            97.789                                                                 
Duplication ratio              1.028                                                                  
#N's per 100 kbp               37.45                                                                  
#mismatches per 100 kbp        10.28                                                                  
#indels per 100 kbp            10.14                                                                  
#genes                         2581 + 110 part                                                        
#predicted genes (unique)      2971                                                                   
#predicted genes (>= 0 bp)     2975                                                                   
#predicted genes (>= 300 bp)   2349                                                                   
#predicted genes (>= 1500 bp)  279                                                                    
#predicted genes (>= 3000 bp)  27                                                                     
Largest alignment              120775                                                                 
NA50                           32479                                                                  
NGA50                          33228                                                                  
NA75                           16636                                                                  
NGA75                          16680                                                                  
