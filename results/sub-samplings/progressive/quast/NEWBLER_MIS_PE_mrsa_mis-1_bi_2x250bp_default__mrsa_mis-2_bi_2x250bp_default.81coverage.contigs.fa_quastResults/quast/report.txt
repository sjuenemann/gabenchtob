All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.81coverage.contigs
#Contigs (>= 0 bp)             78                                                                                            
#Contigs (>= 1000 bp)          42                                                                                            
Total length (>= 0 bp)         2783567                                                                                       
Total length (>= 1000 bp)      2773262                                                                                       
#Contigs                       62                                                                                            
Largest contig                 599882                                                                                        
Total length                   2781444                                                                                       
Reference length               2813862                                                                                       
GC (%)                         32.69                                                                                         
Reference GC (%)               32.81                                                                                         
N50                            159423                                                                                        
NG50                           159423                                                                                        
N75                            74389                                                                                         
NG75                           74389                                                                                         
#misassemblies                 1                                                                                             
#local misassemblies           1                                                                                             
#unaligned contigs             3 + 1 part                                                                                    
Unaligned contigs length       6064                                                                                          
Genome fraction (%)            98.282                                                                                        
Duplication ratio              1.000                                                                                         
#N's per 100 kbp               0.00                                                                                          
#mismatches per 100 kbp        1.55                                                                                          
#indels per 100 kbp            0.36                                                                                          
#genes                         2679 + 22 part                                                                                
#predicted genes (unique)      2628                                                                                          
#predicted genes (>= 0 bp)     2629                                                                                          
#predicted genes (>= 300 bp)   2290                                                                                          
#predicted genes (>= 1500 bp)  298                                                                                           
#predicted genes (>= 3000 bp)  29                                                                                            
Largest alignment              599882                                                                                        
NA50                           159423                                                                                        
NGA50                          159423                                                                                        
NA75                           68605                                                                                         
NGA75                          68605                                                                                         
