All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.96coverage.K99.final-contigs
#Mis_misassemblies               3                                                                         
#Mis_relocations                 3                                                                         
#Mis_translocations              0                                                                         
#Mis_inversions                  0                                                                         
#Mis_misassembled contigs        3                                                                         
Mis_Misassembled contigs length  360242                                                                    
#Mis_local misassemblies         11                                                                        
#mismatches                      225                                                                       
#indels                          361                                                                       
#Mis_short indels (<= 5 bp)      361                                                                       
#Mis_long indels (> 5 bp)        0                                                                         
Indels length                    371                                                                       
