All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.6coverage_out.unpadded
#Contigs                                   2611                                                                                           
#Contigs (>= 0 bp)                         2664                                                                                           
#Contigs (>= 1000 bp)                      361                                                                                            
Largest contig                             5389                                                                                           
Total length                               1638938                                                                                        
Total length (>= 0 bp)                     1647900                                                                                        
Total length (>= 1000 bp)                  520617                                                                                         
Reference length                           2813862                                                                                        
N50                                        721                                                                                            
NG50                                       381                                                                                            
N75                                        469                                                                                            
NG75                                       None                                                                                           
L50                                        717                                                                                            
LG50                                       1849                                                                                           
L75                                        1428                                                                                           
LG75                                       None                                                                                           
#local misassemblies                       4                                                                                              
#misassemblies                             27                                                                                             
#misassembled contigs                      19                                                                                             
Misassembled contigs length                22473                                                                                          
Misassemblies inter-contig overlap         6373                                                                                           
#unaligned contigs                         5 + 6 part                                                                                     
Unaligned contigs length                   4326                                                                                           
#ambiguously mapped contigs                8                                                                                              
Extra bases in ambiguously mapped contigs  -3256                                                                                          
#N's                                       192                                                                                            
#N's per 100 kbp                           11.71                                                                                          
Genome fraction (%)                        57.779                                                                                         
Duplication ratio                          1.007                                                                                          
#genes                                     391 + 1768 part                                                                                
#predicted genes (unique)                  3322                                                                                           
#predicted genes (>= 0 bp)                 3323                                                                                           
#predicted genes (>= 300 bp)               2026                                                                                           
#predicted genes (>= 1500 bp)              14                                                                                             
#predicted genes (>= 3000 bp)              2                                                                                              
#mismatches                                341                                                                                            
#indels                                    42                                                                                             
Indels length                              131                                                                                            
#mismatches per 100 kbp                    20.97                                                                                          
#indels per 100 kbp                        2.58                                                                                           
GC (%)                                     33.71                                                                                          
Reference GC (%)                           32.81                                                                                          
Average %IDY                               99.804                                                                                         
Largest alignment                          5389                                                                                           
NA50                                       713                                                                                            
NGA50                                      375                                                                                            
NA75                                       463                                                                                            
NGA75                                      None                                                                                           
LA50                                       724                                                                                            
LGA50                                      1869                                                                                           
LA75                                       1443                                                                                           
LGA75                                      None                                                                                           
#Mis_misassemblies                         27                                                                                             
#Mis_relocations                           27                                                                                             
#Mis_translocations                        0                                                                                              
#Mis_inversions                            0                                                                                              
#Mis_misassembled contigs                  19                                                                                             
Mis_Misassembled contigs length            22473                                                                                          
#Mis_local misassemblies                   4                                                                                              
#Mis_short indels (<= 5 bp)                39                                                                                             
#Mis_long indels (> 5 bp)                  3                                                                                              
#Una_fully unaligned contigs               5                                                                                              
Una_Fully unaligned length                 3839                                                                                           
#Una_partially unaligned contigs           6                                                                                              
#Una_with misassembly                      0                                                                                              
#Una_both parts are significant            1                                                                                              
Una_Partially unaligned length             487                                                                                            
GAGE_Contigs #                             2611                                                                                           
GAGE_Min contig                            200                                                                                            
GAGE_Max contig                            5389                                                                                           
GAGE_N50                                   381 COUNT: 1849                                                                                
GAGE_Genome size                           2813862                                                                                        
GAGE_Assembly size                         1638938                                                                                        
GAGE_Chaff bases                           0                                                                                              
GAGE_Missing reference bases               1150588(40.89%)                                                                                
GAGE_Missing assembly bases                4414(0.27%)                                                                                    
GAGE_Missing assembly contigs              5(0.19%)                                                                                       
GAGE_Duplicated reference bases            8441                                                                                           
GAGE_Compressed reference bases            46478                                                                                          
GAGE_Bad trim                              547                                                                                            
GAGE_Avg idy                               99.96                                                                                          
GAGE_SNPs                                  165                                                                                            
GAGE_Indels < 5bp                          9                                                                                              
GAGE_Indels >= 5                           4                                                                                              
GAGE_Inversions                            9                                                                                              
GAGE_Relocation                            10                                                                                             
GAGE_Translocation                         0                                                                                              
GAGE_Corrected contig #                    2604                                                                                           
GAGE_Corrected assembly size               1633016                                                                                        
GAGE_Min correct contig                    200                                                                                            
GAGE_Max correct contig                    5389                                                                                           
GAGE_Corrected N50                         379 COUNT: 1858                                                                                
