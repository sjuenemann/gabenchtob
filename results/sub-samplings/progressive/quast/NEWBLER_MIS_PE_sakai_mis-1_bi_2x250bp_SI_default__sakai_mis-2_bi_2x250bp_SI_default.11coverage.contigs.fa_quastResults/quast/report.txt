All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.11coverage.contigs
#Contigs (>= 0 bp)             1127                                                                                                  
#Contigs (>= 1000 bp)          776                                                                                                   
Total length (>= 0 bp)         5265256                                                                                               
Total length (>= 1000 bp)      5097858                                                                                               
#Contigs                       1076                                                                                                  
Largest contig                 55543                                                                                                 
Total length                   5257517                                                                                               
Reference length               5594470                                                                                               
GC (%)                         50.31                                                                                                 
Reference GC (%)               50.48                                                                                                 
N50                            9652                                                                                                  
NG50                           9027                                                                                                  
N75                            5250                                                                                                  
NG75                           4167                                                                                                  
#misassemblies                 3                                                                                                     
#local misassemblies           4                                                                                                     
#unaligned contigs             1 + 0 part                                                                                            
Unaligned contigs length       5386                                                                                                  
Genome fraction (%)            92.405                                                                                                
Duplication ratio              1.001                                                                                                 
#N's per 100 kbp               0.02                                                                                                  
#mismatches per 100 kbp        7.95                                                                                                  
#indels per 100 kbp            0.81                                                                                                  
#genes                         4274 + 777 part                                                                                       
#predicted genes (unique)      5694                                                                                                  
#predicted genes (>= 0 bp)     5694                                                                                                  
#predicted genes (>= 300 bp)   4572                                                                                                  
#predicted genes (>= 1500 bp)  593                                                                                                   
#predicted genes (>= 3000 bp)  46                                                                                                    
Largest alignment              55543                                                                                                 
NA50                           9578                                                                                                  
NGA50                          9011                                                                                                  
NA75                           5087                                                                                                  
NGA75                          4108                                                                                                  
