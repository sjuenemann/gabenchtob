All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          CLC_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.61coverage.result_broken  CLC_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.61coverage.result
#Una_fully unaligned contigs      3                                                                                                   3                                                                                          
Una_Fully unaligned length        5958                                                                                                5958                                                                                       
#Una_partially unaligned contigs  1                                                                                                   2                                                                                          
#Una_with misassembly             0                                                                                                   0                                                                                          
#Una_both parts are significant   0                                                                                                   1                                                                                          
Una_Partially unaligned length    17                                                                                                  379                                                                                        
#N's                              19                                                                                                  3242                                                                                       
