All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.21coverage.result
#Contigs                                   684                                                                    
#Contigs (>= 0 bp)                         684                                                                    
#Contigs (>= 1000 bp)                      254                                                                    
Largest contig                             81386                                                                  
Total length                               2787853                                                                
Total length (>= 0 bp)                     2787853                                                                
Total length (>= 1000 bp)                  2640960                                                                
Reference length                           2813862                                                                
N50                                        15831                                                                  
NG50                                       15794                                                                  
N75                                        7652                                                                   
NG75                                       7518                                                                   
L50                                        52                                                                     
LG50                                       53                                                                     
L75                                        116                                                                    
LG75                                       118                                                                    
#local misassemblies                       2                                                                      
#misassemblies                             6                                                                      
#misassembled contigs                      6                                                                      
Misassembled contigs length                64136                                                                  
Misassemblies inter-contig overlap         1103                                                                   
#unaligned contigs                         1 + 7 part                                                             
Unaligned contigs length                   966                                                                    
#ambiguously mapped contigs                12                                                                     
Extra bases in ambiguously mapped contigs  -9127                                                                  
#N's                                       193                                                                    
#N's per 100 kbp                           6.92                                                                   
Genome fraction (%)                        96.351                                                                 
Duplication ratio                          1.025                                                                  
#genes                                     2427 + 243 part                                                        
#predicted genes (unique)                  3219                                                                   
#predicted genes (>= 0 bp)                 3228                                                                   
#predicted genes (>= 300 bp)               2404                                                                   
#predicted genes (>= 1500 bp)              237                                                                    
#predicted genes (>= 3000 bp)              21                                                                     
#mismatches                                147                                                                    
#indels                                    882                                                                    
Indels length                              911                                                                    
#mismatches per 100 kbp                    5.42                                                                   
#indels per 100 kbp                        32.53                                                                  
GC (%)                                     32.63                                                                  
Reference GC (%)                           32.81                                                                  
Average %IDY                               99.544                                                                 
Largest alignment                          81386                                                                  
NA50                                       15831                                                                  
NGA50                                      15794                                                                  
NA75                                       7593                                                                   
NGA75                                      7517                                                                   
LA50                                       52                                                                     
LGA50                                      53                                                                     
LA75                                       116                                                                    
LGA75                                      119                                                                    
#Mis_misassemblies                         6                                                                      
#Mis_relocations                           6                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  6                                                                      
Mis_Misassembled contigs length            64136                                                                  
#Mis_local misassemblies                   2                                                                      
#Mis_short indels (<= 5 bp)                882                                                                    
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               1                                                                      
Una_Fully unaligned length                 669                                                                    
#Una_partially unaligned contigs           7                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             297                                                                    
GAGE_Contigs #                             684                                                                    
GAGE_Min contig                            200                                                                    
GAGE_Max contig                            81386                                                                  
GAGE_N50                                   15794 COUNT: 53                                                        
GAGE_Genome size                           2813862                                                                
GAGE_Assembly size                         2787853                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               59011(2.10%)                                                           
GAGE_Missing assembly bases                1049(0.04%)                                                            
GAGE_Missing assembly contigs              1(0.15%)                                                               
GAGE_Duplicated reference bases            59554                                                                  
GAGE_Compressed reference bases            38911                                                                  
GAGE_Bad trim                              372                                                                    
GAGE_Avg idy                               99.96                                                                  
GAGE_SNPs                                  58                                                                     
GAGE_Indels < 5bp                          831                                                                    
GAGE_Indels >= 5                           3                                                                      
GAGE_Inversions                            2                                                                      
GAGE_Relocation                            3                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    495                                                                    
GAGE_Corrected assembly size               2728484                                                                
GAGE_Min correct contig                    200                                                                    
GAGE_Max correct contig                    81404                                                                  
GAGE_Corrected N50                         15797 COUNT: 53                                                        
