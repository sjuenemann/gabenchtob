All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.21coverage.result
#Contigs (>= 0 bp)             684                                                                    
#Contigs (>= 1000 bp)          254                                                                    
Total length (>= 0 bp)         2787853                                                                
Total length (>= 1000 bp)      2640960                                                                
#Contigs                       684                                                                    
Largest contig                 81386                                                                  
Total length                   2787853                                                                
Reference length               2813862                                                                
GC (%)                         32.63                                                                  
Reference GC (%)               32.81                                                                  
N50                            15831                                                                  
NG50                           15794                                                                  
N75                            7652                                                                   
NG75                           7518                                                                   
#misassemblies                 6                                                                      
#local misassemblies           2                                                                      
#unaligned contigs             1 + 7 part                                                             
Unaligned contigs length       966                                                                    
Genome fraction (%)            96.351                                                                 
Duplication ratio              1.025                                                                  
#N's per 100 kbp               6.92                                                                   
#mismatches per 100 kbp        5.42                                                                   
#indels per 100 kbp            32.53                                                                  
#genes                         2427 + 243 part                                                        
#predicted genes (unique)      3219                                                                   
#predicted genes (>= 0 bp)     3228                                                                   
#predicted genes (>= 300 bp)   2404                                                                   
#predicted genes (>= 1500 bp)  237                                                                    
#predicted genes (>= 3000 bp)  21                                                                     
Largest alignment              81386                                                                  
NA50                           15831                                                                  
NGA50                          15794                                                                  
NA75                           7593                                                                   
NGA75                          7517                                                                   
