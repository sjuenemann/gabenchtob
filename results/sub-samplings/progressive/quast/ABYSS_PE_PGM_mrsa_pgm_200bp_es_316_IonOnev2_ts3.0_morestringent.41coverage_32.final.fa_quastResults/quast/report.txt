All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.41coverage_32.final
#Contigs (>= 0 bp)             4938                                                                               
#Contigs (>= 1000 bp)          220                                                                                
Total length (>= 0 bp)         2949649                                                                            
Total length (>= 1000 bp)      2673566                                                                            
#Contigs                       351                                                                                
Largest contig                 97829                                                                              
Total length                   2726652                                                                            
Reference length               2813862                                                                            
GC (%)                         32.59                                                                              
Reference GC (%)               32.81                                                                              
N50                            21812                                                                              
NG50                           20482                                                                              
N75                            10474                                                                              
NG75                           10165                                                                              
#misassemblies                 1                                                                                  
#local misassemblies           1                                                                                  
#unaligned contigs             0 + 0 part                                                                         
Unaligned contigs length       0                                                                                  
Genome fraction (%)            96.606                                                                             
Duplication ratio              1.001                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        1.73                                                                               
#indels per 100 kbp            7.87                                                                               
#genes                         2489 + 133 part                                                                    
#predicted genes (unique)      2777                                                                               
#predicted genes (>= 0 bp)     2777                                                                               
#predicted genes (>= 300 bp)   2305                                                                               
#predicted genes (>= 1500 bp)  275                                                                                
#predicted genes (>= 3000 bp)  23                                                                                 
Largest alignment              97829                                                                              
NA50                           21812                                                                              
NGA50                          20482                                                                              
NA75                           10468                                                                              
NGA75                          9940                                                                               
