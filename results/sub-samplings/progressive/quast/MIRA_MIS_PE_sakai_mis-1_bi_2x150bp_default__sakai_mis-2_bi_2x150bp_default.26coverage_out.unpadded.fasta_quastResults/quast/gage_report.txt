All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.26coverage_out.unpadded
GAGE_Contigs #                   311                                                                                               
GAGE_Min contig                  210                                                                                               
GAGE_Max contig                  445503                                                                                            
GAGE_N50                         65116 COUNT: 23                                                                                   
GAGE_Genome size                 5594470                                                                                           
GAGE_Assembly size               5558708                                                                                           
GAGE_Chaff bases                 0                                                                                                 
GAGE_Missing reference bases     3528(0.06%)                                                                                       
GAGE_Missing assembly bases      6006(0.11%)                                                                                       
GAGE_Missing assembly contigs    1(0.32%)                                                                                          
GAGE_Duplicated reference bases  90816                                                                                             
GAGE_Compressed reference bases  158751                                                                                            
GAGE_Bad trim                    133                                                                                               
GAGE_Avg idy                     99.95                                                                                             
GAGE_SNPs                        732                                                                                               
GAGE_Indels < 5bp                59                                                                                                
GAGE_Indels >= 5                 2                                                                                                 
GAGE_Inversions                  17                                                                                                
GAGE_Relocation                  18                                                                                                
GAGE_Translocation               2                                                                                                 
GAGE_Corrected contig #          256                                                                                               
GAGE_Corrected assembly size     5539922                                                                                           
GAGE_Min correct contig          229                                                                                               
GAGE_Max correct contig          353924                                                                                            
GAGE_Corrected N50               54477 COUNT: 28                                                                                   
