All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.11coverage_out.unpadded
#Contigs (>= 0 bp)             2265                                                                                              
#Contigs (>= 1000 bp)          1436                                                                                              
Total length (>= 0 bp)         5363409                                                                                           
Total length (>= 1000 bp)      4935780                                                                                           
#Contigs                       2211                                                                                              
Largest contig                 25916                                                                                             
Total length                   5354650                                                                                           
Reference length               5594470                                                                                           
GC (%)                         50.47                                                                                             
Reference GC (%)               50.48                                                                                             
N50                            4206                                                                                              
NG50                           4011                                                                                              
N75                            2201                                                                                              
NG75                           1948                                                                                              
#misassemblies                 51                                                                                                
#local misassemblies           10                                                                                                
#unaligned contigs             2 + 2 part                                                                                        
Unaligned contigs length       5653                                                                                              
Genome fraction (%)            94.229                                                                                            
Duplication ratio              1.013                                                                                             
#N's per 100 kbp               2.78                                                                                              
#mismatches per 100 kbp        42.07                                                                                             
#indels per 100 kbp            1.71                                                                                              
#genes                         3697 + 1548 part                                                                                  
#predicted genes (unique)      6661                                                                                              
#predicted genes (>= 0 bp)     6673                                                                                              
#predicted genes (>= 300 bp)   5029                                                                                              
#predicted genes (>= 1500 bp)  484                                                                                               
#predicted genes (>= 3000 bp)  36                                                                                                
Largest alignment              25916                                                                                             
NA50                           4126                                                                                              
NGA50                          3903                                                                                              
NA75                           2135                                                                                              
NGA75                          1915                                                                                              
