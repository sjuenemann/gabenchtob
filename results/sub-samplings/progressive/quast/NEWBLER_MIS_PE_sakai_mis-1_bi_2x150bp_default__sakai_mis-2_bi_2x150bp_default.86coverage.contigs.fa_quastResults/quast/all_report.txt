All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   NEWBLER_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.86coverage.contigs
#Contigs                                   347                                                                                             
#Contigs (>= 0 bp)                         422                                                                                             
#Contigs (>= 1000 bp)                      153                                                                                             
Largest contig                             259656                                                                                          
Total length                               5315670                                                                                         
Total length (>= 0 bp)                     5326385                                                                                         
Total length (>= 1000 bp)                  5233736                                                                                         
Reference length                           5594470                                                                                         
N50                                        117527                                                                                          
NG50                                       96027                                                                                           
N75                                        54361                                                                                           
NG75                                       43456                                                                                           
L50                                        16                                                                                              
LG50                                       18                                                                                              
L75                                        34                                                                                              
LG75                                       38                                                                                              
#local misassemblies                       4                                                                                               
#misassemblies                             3                                                                                               
#misassembled contigs                      3                                                                                               
Misassembled contigs length                209433                                                                                          
Misassemblies inter-contig overlap         740                                                                                             
#unaligned contigs                         1 + 0 part                                                                                      
Unaligned contigs length                   5386                                                                                            
#ambiguously mapped contigs                119                                                                                             
Extra bases in ambiguously mapped contigs  -86792                                                                                          
#N's                                       0                                                                                               
#N's per 100 kbp                           0.00                                                                                            
Genome fraction (%)                        93.375                                                                                          
Duplication ratio                          1.000                                                                                           
#genes                                     4901 + 164 part                                                                                 
#predicted genes (unique)                  5258                                                                                            
#predicted genes (>= 0 bp)                 5258                                                                                            
#predicted genes (>= 300 bp)               4397                                                                                            
#predicted genes (>= 1500 bp)              665                                                                                             
#predicted genes (>= 3000 bp)              68                                                                                              
#mismatches                                50                                                                                              
#indels                                    15                                                                                              
Indels length                              15                                                                                              
#mismatches per 100 kbp                    0.96                                                                                            
#indels per 100 kbp                        0.29                                                                                            
GC (%)                                     50.27                                                                                           
Reference GC (%)                           50.48                                                                                           
Average %IDY                               99.076                                                                                          
Largest alignment                          259656                                                                                          
NA50                                       107801                                                                                          
NGA50                                      96027                                                                                           
NA75                                       54361                                                                                           
NGA75                                      43145                                                                                           
LA50                                       17                                                                                              
LGA50                                      18                                                                                              
LA75                                       34                                                                                              
LGA75                                      39                                                                                              
#Mis_misassemblies                         3                                                                                               
#Mis_relocations                           3                                                                                               
#Mis_translocations                        0                                                                                               
#Mis_inversions                            0                                                                                               
#Mis_misassembled contigs                  3                                                                                               
Mis_Misassembled contigs length            209433                                                                                          
#Mis_local misassemblies                   4                                                                                               
#Mis_short indels (<= 5 bp)                15                                                                                              
#Mis_long indels (> 5 bp)                  0                                                                                               
#Una_fully unaligned contigs               1                                                                                               
Una_Fully unaligned length                 5386                                                                                            
#Una_partially unaligned contigs           0                                                                                               
#Una_with misassembly                      0                                                                                               
#Una_both parts are significant            0                                                                                               
Una_Partially unaligned length             0                                                                                               
GAGE_Contigs #                             347                                                                                             
GAGE_Min contig                            205                                                                                             
GAGE_Max contig                            259656                                                                                          
GAGE_N50                                   96027 COUNT: 18                                                                                 
GAGE_Genome size                           5594470                                                                                         
GAGE_Assembly size                         5315670                                                                                         
GAGE_Chaff bases                           0                                                                                               
GAGE_Missing reference bases               60014(1.07%)                                                                                    
GAGE_Missing assembly bases                5399(0.10%)                                                                                     
GAGE_Missing assembly contigs              1(0.29%)                                                                                        
GAGE_Duplicated reference bases            433                                                                                             
GAGE_Compressed reference bases            229631                                                                                          
GAGE_Bad trim                              5                                                                                               
GAGE_Avg idy                               100.00                                                                                          
GAGE_SNPs                                  103                                                                                             
GAGE_Indels < 5bp                          15                                                                                              
GAGE_Indels >= 5                           5                                                                                               
GAGE_Inversions                            0                                                                                               
GAGE_Relocation                            3                                                                                               
GAGE_Translocation                         0                                                                                               
GAGE_Corrected contig #                    352                                                                                             
GAGE_Corrected assembly size               5310582                                                                                         
GAGE_Min correct contig                    205                                                                                             
GAGE_Max correct contig                    224231                                                                                          
GAGE_Corrected N50                         93260 COUNT: 19                                                                                 
