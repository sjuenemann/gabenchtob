All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          MIRA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.21coverage_out.unpadded
#Una_fully unaligned contigs      0                                                                             
Una_Fully unaligned length        0                                                                             
#Una_partially unaligned contigs  1                                                                             
#Una_with misassembly             0                                                                             
#Una_both parts are significant   0                                                                             
Una_Partially unaligned length    57                                                                            
#N's                              24                                                                            
