All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.76coverage_out.unpadded
GAGE_Contigs #                   222                                                                                             
GAGE_Min contig                  200                                                                                             
GAGE_Max contig                  480235                                                                                          
GAGE_N50                         184106 COUNT: 5                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2913740                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     463(0.02%)                                                                                      
GAGE_Missing assembly bases      6287(0.22%)                                                                                     
GAGE_Missing assembly contigs    2(0.90%)                                                                                        
GAGE_Duplicated reference bases  111160                                                                                          
GAGE_Compressed reference bases  11810                                                                                           
GAGE_Bad trim                    3                                                                                               
GAGE_Avg idy                     99.98                                                                                           
GAGE_SNPs                        54                                                                                              
GAGE_Indels < 5bp                15                                                                                              
GAGE_Indels >= 5                 3                                                                                               
GAGE_Inversions                  10                                                                                              
GAGE_Relocation                  3                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          67                                                                                              
GAGE_Corrected assembly size     2832596                                                                                         
GAGE_Min correct contig          214                                                                                             
GAGE_Max correct contig          419807                                                                                          
GAGE_Corrected N50               98074 COUNT: 7                                                                                  
