All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                                                                                  #Una_fully unaligned contigs  Una_Fully unaligned length  #Una_partially unaligned contigs  #Una_with misassembly  #Una_both parts are significant  Una_Partially unaligned length  #N's
MIRA_MIS_PE_sakai_mis-1_bi_2x250bp_SI_default__sakai_mis-2_bi_2x250bp_SI_default.41coverage_out.unpadded  2                             6181                        4                                 0                      0                                126                             43  
