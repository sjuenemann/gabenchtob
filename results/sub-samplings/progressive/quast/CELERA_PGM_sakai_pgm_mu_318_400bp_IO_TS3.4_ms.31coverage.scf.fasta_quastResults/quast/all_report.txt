All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.31coverage.scf
#Contigs                                   200                                                         
#Contigs (>= 0 bp)                         200                                                         
#Contigs (>= 1000 bp)                      200                                                         
Largest contig                             282261                                                      
Total length                               5225722                                                     
Total length (>= 0 bp)                     5225722                                                     
Total length (>= 1000 bp)                  5225722                                                     
Reference length                           5594470                                                     
N50                                        83483                                                       
NG50                                       73299                                                       
N75                                        41009                                                       
NG75                                       35207                                                       
L50                                        19                                                          
LG50                                       22                                                          
L75                                        44                                                          
LG75                                       51                                                          
#local misassemblies                       9                                                           
#misassemblies                             4                                                           
#misassembled contigs                      4                                                           
Misassembled contigs length                316575                                                      
Misassemblies inter-contig overlap         3223                                                        
#unaligned contigs                         0 + 0 part                                                  
Unaligned contigs length                   0                                                           
#ambiguously mapped contigs                8                                                           
Extra bases in ambiguously mapped contigs  -11176                                                      
#N's                                       0                                                           
#N's per 100 kbp                           0.00                                                        
Genome fraction (%)                        92.919                                                      
Duplication ratio                          1.004                                                       
#genes                                     4910 + 188 part                                             
#predicted genes (unique)                  5285                                                        
#predicted genes (>= 0 bp)                 5285                                                        
#predicted genes (>= 300 bp)               4468                                                        
#predicted genes (>= 1500 bp)              612                                                         
#predicted genes (>= 3000 bp)              56                                                          
#mismatches                                148                                                         
#indels                                    660                                                         
Indels length                              679                                                         
#mismatches per 100 kbp                    2.85                                                        
#indels per 100 kbp                        12.70                                                       
GC (%)                                     50.30                                                       
Reference GC (%)                           50.48                                                       
Average %IDY                               98.454                                                      
Largest alignment                          233085                                                      
NA50                                       83483                                                       
NGA50                                      73299                                                       
NA75                                       41009                                                       
NGA75                                      35207                                                       
LA50                                       20                                                          
LGA50                                      23                                                          
LA75                                       45                                                          
LGA75                                      52                                                          
#Mis_misassemblies                         4                                                           
#Mis_relocations                           4                                                           
#Mis_translocations                        0                                                           
#Mis_inversions                            0                                                           
#Mis_misassembled contigs                  4                                                           
Mis_Misassembled contigs length            316575                                                      
#Mis_local misassemblies                   9                                                           
#Mis_short indels (<= 5 bp)                659                                                         
#Mis_long indels (> 5 bp)                  1                                                           
#Una_fully unaligned contigs               0                                                           
Una_Fully unaligned length                 0                                                           
#Una_partially unaligned contigs           0                                                           
#Una_with misassembly                      0                                                           
#Una_both parts are significant            0                                                           
Una_Partially unaligned length             0                                                           
GAGE_Contigs #                             200                                                         
GAGE_Min contig                            1006                                                        
GAGE_Max contig                            282261                                                      
GAGE_N50                                   73299 COUNT: 22                                             
GAGE_Genome size                           5594470                                                     
GAGE_Assembly size                         5225722                                                     
GAGE_Chaff bases                           0                                                           
GAGE_Missing reference bases               308781(5.52%)                                               
GAGE_Missing assembly bases                159(0.00%)                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                    
GAGE_Duplicated reference bases            5143                                                        
GAGE_Compressed reference bases            128703                                                      
GAGE_Bad trim                              159                                                         
GAGE_Avg idy                               99.98                                                       
GAGE_SNPs                                  88                                                          
GAGE_Indels < 5bp                          643                                                         
GAGE_Indels >= 5                           6                                                           
GAGE_Inversions                            4                                                           
GAGE_Relocation                            5                                                           
GAGE_Translocation                         0                                                           
GAGE_Corrected contig #                    212                                                         
GAGE_Corrected assembly size               5225032                                                     
GAGE_Min correct contig                    537                                                         
GAGE_Max correct contig                    197185                                                      
GAGE_Corrected N50                         58959 COUNT: 25                                             
