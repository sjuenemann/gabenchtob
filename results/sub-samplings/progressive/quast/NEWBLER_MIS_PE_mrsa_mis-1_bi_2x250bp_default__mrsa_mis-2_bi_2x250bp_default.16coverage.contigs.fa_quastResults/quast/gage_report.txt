All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.16coverage.contigs
GAGE_Contigs #                   542                                                                                           
GAGE_Min contig                  200                                                                                           
GAGE_Max contig                  61383                                                                                         
GAGE_N50                         10003 COUNT: 76                                                                               
GAGE_Genome size                 2813862                                                                                       
GAGE_Assembly size               2744994                                                                                       
GAGE_Chaff bases                 0                                                                                             
GAGE_Missing reference bases     35676(1.27%)                                                                                  
GAGE_Missing assembly bases      5433(0.20%)                                                                                   
GAGE_Missing assembly contigs    1(0.18%)                                                                                      
GAGE_Duplicated reference bases  0                                                                                             
GAGE_Compressed reference bases  42209                                                                                         
GAGE_Bad trim                    47                                                                                            
GAGE_Avg idy                     99.99                                                                                         
GAGE_SNPs                        99                                                                                            
GAGE_Indels < 5bp                22                                                                                            
GAGE_Indels >= 5                 4                                                                                             
GAGE_Inversions                  1                                                                                             
GAGE_Relocation                  2                                                                                             
GAGE_Translocation               0                                                                                             
GAGE_Corrected contig #          550                                                                                           
GAGE_Corrected assembly size     2741602                                                                                       
GAGE_Min correct contig          200                                                                                           
GAGE_Max correct contig          61383                                                                                         
GAGE_Corrected N50               9995 COUNT: 76                                                                                
