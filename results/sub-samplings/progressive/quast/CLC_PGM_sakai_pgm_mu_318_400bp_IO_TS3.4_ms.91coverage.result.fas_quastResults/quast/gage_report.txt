All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.91coverage.result
GAGE_Contigs #                   977                                                         
GAGE_Min contig                  201                                                         
GAGE_Max contig                  127614                                                      
GAGE_N50                         33085 COUNT: 51                                             
GAGE_Genome size                 5594470                                                     
GAGE_Assembly size               5478063                                                     
GAGE_Chaff bases                 0                                                           
GAGE_Missing reference bases     25234(0.45%)                                                
GAGE_Missing assembly bases      2267(0.04%)                                                 
GAGE_Missing assembly contigs    6(0.61%)                                                    
GAGE_Duplicated reference bases  114008                                                      
GAGE_Compressed reference bases  245990                                                      
GAGE_Bad trim                    429                                                         
GAGE_Avg idy                     99.99                                                       
GAGE_SNPs                        93                                                          
GAGE_Indels < 5bp                488                                                         
GAGE_Indels >= 5                 5                                                           
GAGE_Inversions                  0                                                           
GAGE_Relocation                  6                                                           
GAGE_Translocation               0                                                           
GAGE_Corrected contig #          609                                                         
GAGE_Corrected assembly size     5362439                                                     
GAGE_Min correct contig          201                                                         
GAGE_Max correct contig          127616                                                      
GAGE_Corrected N50               32032 COUNT: 52                                             
