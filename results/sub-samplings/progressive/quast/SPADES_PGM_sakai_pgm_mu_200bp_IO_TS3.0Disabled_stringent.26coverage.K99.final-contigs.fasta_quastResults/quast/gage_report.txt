All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.26coverage.K99.final-contigs
GAGE_Contigs #                   344                                                                                  
GAGE_Min contig                  201                                                                                  
GAGE_Max contig                  271533                                                                               
GAGE_N50                         103971 COUNT: 17                                                                     
GAGE_Genome size                 5594470                                                                              
GAGE_Assembly size               5350914                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     12211(0.22%)                                                                         
GAGE_Missing assembly bases      156(0.00%)                                                                           
GAGE_Missing assembly contigs    0(0.00%)                                                                             
GAGE_Duplicated reference bases  1162                                                                                 
GAGE_Compressed reference bases  288424                                                                               
GAGE_Bad trim                    156                                                                                  
GAGE_Avg idy                     99.95                                                                                
GAGE_SNPs                        308                                                                                  
GAGE_Indels < 5bp                2171                                                                                 
GAGE_Indels >= 5                 12                                                                                   
GAGE_Inversions                  4                                                                                    
GAGE_Relocation                  10                                                                                   
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          370                                                                                  
GAGE_Corrected assembly size     5358600                                                                              
GAGE_Min correct contig          201                                                                                  
GAGE_Max correct contig          217540                                                                               
GAGE_Corrected N50               87150 COUNT: 21                                                                      
