All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.21coverage.contigs
#Contigs (>= 0 bp)             197                                                                              
#Contigs (>= 1000 bp)          119                                                                              
Total length (>= 0 bp)         2766779                                                                          
Total length (>= 1000 bp)      2744856                                                                          
#Contigs                       154                                                                              
Largest contig                 82122                                                                            
Total length                   2760865                                                                          
Reference length               2813862                                                                          
GC (%)                         32.63                                                                            
Reference GC (%)               32.81                                                                            
N50                            35323                                                                            
NG50                           34215                                                                            
N75                            25854                                                                            
NG75                           25607                                                                            
#misassemblies                 2                                                                                
#local misassemblies           8                                                                                
#unaligned contigs             0 + 0 part                                                                       
Unaligned contigs length       0                                                                                
Genome fraction (%)            97.742                                                                           
Duplication ratio              1.001                                                                            
#N's per 100 kbp               0.00                                                                             
#mismatches per 100 kbp        4.14                                                                             
#indels per 100 kbp            11.49                                                                            
#genes                         2610 + 74 part                                                                   
#predicted genes (unique)      2762                                                                             
#predicted genes (>= 0 bp)     2762                                                                             
#predicted genes (>= 300 bp)   2330                                                                             
#predicted genes (>= 1500 bp)  284                                                                              
#predicted genes (>= 3000 bp)  23                                                                               
Largest alignment              82122                                                                            
NA50                           35323                                                                            
NGA50                          34215                                                                            
NA75                           25834                                                                            
NGA75                          25607                                                                            
