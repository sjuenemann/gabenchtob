All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.46coverage.scf
#Contigs                                   424                                                                         
#Contigs (>= 0 bp)                         424                                                                         
#Contigs (>= 1000 bp)                      424                                                                         
Largest contig                             50718                                                                       
Total length                               2740454                                                                     
Total length (>= 0 bp)                     2740454                                                                     
Total length (>= 1000 bp)                  2740454                                                                     
Reference length                           2813862                                                                     
N50                                        12257                                                                       
NG50                                       11805                                                                       
N75                                        5496                                                                        
NG75                                       4731                                                                        
L50                                        63                                                                          
LG50                                       66                                                                          
L75                                        146                                                                         
LG75                                       157                                                                         
#local misassemblies                       4                                                                           
#misassemblies                             4                                                                           
#misassembled contigs                      4                                                                           
Misassembled contigs length                57162                                                                       
Misassemblies inter-contig overlap         1736                                                                        
#unaligned contigs                         0 + 5 part                                                                  
Unaligned contigs length                   287                                                                         
#ambiguously mapped contigs                0                                                                           
Extra bases in ambiguously mapped contigs  0                                                                           
#N's                                       0                                                                           
#N's per 100 kbp                           0.00                                                                        
Genome fraction (%)                        95.354                                                                      
Duplication ratio                          1.022                                                                       
#genes                                     2335 + 291 part                                                             
#predicted genes (unique)                  2925                                                                        
#predicted genes (>= 0 bp)                 2925                                                                        
#predicted genes (>= 300 bp)               2375                                                                        
#predicted genes (>= 1500 bp)              258                                                                         
#predicted genes (>= 3000 bp)              22                                                                          
#mismatches                                153                                                                         
#indels                                    505                                                                         
Indels length                              542                                                                         
#mismatches per 100 kbp                    5.70                                                                        
#indels per 100 kbp                        18.82                                                                       
GC (%)                                     32.60                                                                       
Reference GC (%)                           32.81                                                                       
Average %IDY                               99.246                                                                      
Largest alignment                          50715                                                                       
NA50                                       12257                                                                       
NGA50                                      11805                                                                       
NA75                                       5496                                                                        
NGA75                                      4731                                                                        
LA50                                       63                                                                          
LGA50                                      66                                                                          
LA75                                       146                                                                         
LGA75                                      157                                                                         
#Mis_misassemblies                         4                                                                           
#Mis_relocations                           3                                                                           
#Mis_translocations                        0                                                                           
#Mis_inversions                            1                                                                           
#Mis_misassembled contigs                  4                                                                           
Mis_Misassembled contigs length            57162                                                                       
#Mis_local misassemblies                   4                                                                           
#Mis_short indels (<= 5 bp)                504                                                                         
#Mis_long indels (> 5 bp)                  1                                                                           
#Una_fully unaligned contigs               0                                                                           
Una_Fully unaligned length                 0                                                                           
#Una_partially unaligned contigs           5                                                                           
#Una_with misassembly                      0                                                                           
#Una_both parts are significant            0                                                                           
Una_Partially unaligned length             287                                                                         
GAGE_Contigs #                             424                                                                         
GAGE_Min contig                            1004                                                                        
GAGE_Max contig                            50718                                                                       
GAGE_N50                                   11805 COUNT: 66                                                             
GAGE_Genome size                           2813862                                                                     
GAGE_Assembly size                         2740454                                                                     
GAGE_Chaff bases                           0                                                                           
GAGE_Missing reference bases               121080(4.30%)                                                               
GAGE_Missing assembly bases                598(0.02%)                                                                  
GAGE_Missing assembly contigs              0(0.00%)                                                                    
GAGE_Duplicated reference bases            377                                                                         
GAGE_Compressed reference bases            14353                                                                       
GAGE_Bad trim                              598                                                                         
GAGE_Avg idy                               99.97                                                                       
GAGE_SNPs                                  62                                                                          
GAGE_Indels < 5bp                          395                                                                         
GAGE_Indels >= 5                           5                                                                           
GAGE_Inversions                            0                                                                           
GAGE_Relocation                            2                                                                           
GAGE_Translocation                         0                                                                           
GAGE_Corrected contig #                    431                                                                         
GAGE_Corrected assembly size               2741486                                                                     
GAGE_Min correct contig                    285                                                                         
GAGE_Max correct contig                    50724                                                                       
GAGE_Corrected N50                         11578 COUNT: 67                                                             
