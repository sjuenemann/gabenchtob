All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       MIRA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms.91coverage_out.unpadded
#Contigs (>= 0 bp)             974                                                                
#Contigs (>= 1000 bp)          146                                                                
Total length (>= 0 bp)         5968548                                                            
Total length (>= 1000 bp)      5594764                                                            
#Contigs                       919                                                                
Largest contig                 472271                                                             
Total length                   5959892                                                            
Reference length               5594470                                                            
GC (%)                         50.31                                                              
Reference GC (%)               50.48                                                              
N50                            169341                                                             
NG50                           172469                                                             
N75                            73026                                                              
NG75                           94710                                                              
#misassemblies                 71                                                                 
#local misassemblies           25                                                                 
#unaligned contigs             0 + 3 part                                                         
Unaligned contigs length       350                                                                
Genome fraction (%)            98.758                                                             
Duplication ratio              1.076                                                              
#N's per 100 kbp               0.96                                                               
#mismatches per 100 kbp        29.81                                                              
#indels per 100 kbp            19.98                                                              
#genes                         5322 + 84 part                                                     
#predicted genes (unique)      6551                                                               
#predicted genes (>= 0 bp)     6629                                                               
#predicted genes (>= 300 bp)   4986                                                               
#predicted genes (>= 1500 bp)  652                                                                
#predicted genes (>= 3000 bp)  58                                                                 
Largest alignment              364859                                                             
NA50                           128711                                                             
NGA50                          131810                                                             
NA75                           53971                                                              
NGA75                          65614                                                              
