All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         CLC_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.1coverage.result
GAGE_Contigs #                   1318                                                                  
GAGE_Min contig                  203                                                                   
GAGE_Max contig                  4440                                                                  
GAGE_N50                         0 COUNT: 0                                                            
GAGE_Genome size                 2813862                                                               
GAGE_Assembly size               881043                                                                
GAGE_Chaff bases                 0                                                                     
GAGE_Missing reference bases     1933373(68.71%)                                                       
GAGE_Missing assembly bases      15866(1.80%)                                                          
GAGE_Missing assembly contigs    6(0.46%)                                                              
GAGE_Duplicated reference bases  21703                                                                 
GAGE_Compressed reference bases  45121                                                                 
GAGE_Bad trim                    11310                                                                 
GAGE_Avg idy                     99.03                                                                 
GAGE_SNPs                        1298                                                                  
GAGE_Indels < 5bp                6586                                                                  
GAGE_Indels >= 5                 6                                                                     
GAGE_Inversions                  34                                                                    
GAGE_Relocation                  22                                                                    
GAGE_Translocation               0                                                                     
GAGE_Corrected contig #          1342                                                                  
GAGE_Corrected assembly size     843552                                                                
GAGE_Min correct contig          200                                                                   
GAGE_Max correct contig          3818                                                                  
GAGE_Corrected N50               0 COUNT: 0                                                            
