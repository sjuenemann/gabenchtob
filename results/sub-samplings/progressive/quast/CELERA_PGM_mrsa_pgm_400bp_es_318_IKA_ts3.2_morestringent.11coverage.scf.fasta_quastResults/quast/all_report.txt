All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.11coverage.scf
#Contigs                                   297                                                                    
#Contigs (>= 0 bp)                         297                                                                    
#Contigs (>= 1000 bp)                      297                                                                    
Largest contig                             157775                                                                 
Total length                               2696828                                                                
Total length (>= 0 bp)                     2696828                                                                
Total length (>= 1000 bp)                  2696828                                                                
Reference length                           2813862                                                                
N50                                        15464                                                                  
NG50                                       15112                                                                  
N75                                        8258                                                                   
NG75                                       7371                                                                   
L50                                        48                                                                     
LG50                                       51                                                                     
L75                                        109                                                                    
LG75                                       120                                                                    
#local misassemblies                       6                                                                      
#misassemblies                             2                                                                      
#misassembled contigs                      2                                                                      
Misassembled contigs length                49701                                                                  
Misassemblies inter-contig overlap         1265                                                                   
#unaligned contigs                         0 + 1 part                                                             
Unaligned contigs length                   122                                                                    
#ambiguously mapped contigs                0                                                                      
Extra bases in ambiguously mapped contigs  0                                                                      
#N's                                       0                                                                      
#N's per 100 kbp                           0.00                                                                   
Genome fraction (%)                        95.632                                                                 
Duplication ratio                          1.003                                                                  
#genes                                     2388 + 239 part                                                        
#predicted genes (unique)                  3003                                                                   
#predicted genes (>= 0 bp)                 3005                                                                   
#predicted genes (>= 300 bp)               2392                                                                   
#predicted genes (>= 1500 bp)              225                                                                    
#predicted genes (>= 3000 bp)              22                                                                     
#mismatches                                132                                                                    
#indels                                    1295                                                                   
Indels length                              1328                                                                   
#mismatches per 100 kbp                    4.91                                                                   
#indels per 100 kbp                        48.12                                                                  
GC (%)                                     32.65                                                                  
Reference GC (%)                           32.81                                                                  
Average %IDY                               98.879                                                                 
Largest alignment                          157775                                                                 
NA50                                       15383                                                                  
NGA50                                      14813                                                                  
NA75                                       8258                                                                   
NGA75                                      7369                                                                   
LA50                                       49                                                                     
LGA50                                      52                                                                     
LA75                                       110                                                                    
LGA75                                      121                                                                    
#Mis_misassemblies                         2                                                                      
#Mis_relocations                           2                                                                      
#Mis_translocations                        0                                                                      
#Mis_inversions                            0                                                                      
#Mis_misassembled contigs                  2                                                                      
Mis_Misassembled contigs length            49701                                                                  
#Mis_local misassemblies                   6                                                                      
#Mis_short indels (<= 5 bp)                1295                                                                   
#Mis_long indels (> 5 bp)                  0                                                                      
#Una_fully unaligned contigs               0                                                                      
Una_Fully unaligned length                 0                                                                      
#Una_partially unaligned contigs           1                                                                      
#Una_with misassembly                      0                                                                      
#Una_both parts are significant            0                                                                      
Una_Partially unaligned length             122                                                                    
GAGE_Contigs #                             297                                                                    
GAGE_Min contig                            1003                                                                   
GAGE_Max contig                            157775                                                                 
GAGE_N50                                   15112 COUNT: 51                                                        
GAGE_Genome size                           2813862                                                                
GAGE_Assembly size                         2696828                                                                
GAGE_Chaff bases                           0                                                                      
GAGE_Missing reference bases               113568(4.04%)                                                          
GAGE_Missing assembly bases                157(0.01%)                                                             
GAGE_Missing assembly contigs              0(0.00%)                                                               
GAGE_Duplicated reference bases            583                                                                    
GAGE_Compressed reference bases            14789                                                                  
GAGE_Bad trim                              157                                                                    
GAGE_Avg idy                               99.94                                                                  
GAGE_SNPs                                  89                                                                     
GAGE_Indels < 5bp                          1249                                                                   
GAGE_Indels >= 5                           5                                                                      
GAGE_Inversions                            1                                                                      
GAGE_Relocation                            2                                                                      
GAGE_Translocation                         0                                                                      
GAGE_Corrected contig #                    305                                                                    
GAGE_Corrected assembly size               2698644                                                                
GAGE_Min correct contig                    776                                                                    
GAGE_Max correct contig                    157819                                                                 
GAGE_Corrected N50                         14212 COUNT: 56                                                        
