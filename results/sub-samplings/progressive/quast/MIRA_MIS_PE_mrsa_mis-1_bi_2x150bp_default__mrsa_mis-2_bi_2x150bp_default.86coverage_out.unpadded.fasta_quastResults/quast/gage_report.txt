All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.86coverage_out.unpadded
GAGE_Contigs #                   235                                                                                             
GAGE_Min contig                  201                                                                                             
GAGE_Max contig                  362302                                                                                          
GAGE_N50                         207274 COUNT: 5                                                                                 
GAGE_Genome size                 2813862                                                                                         
GAGE_Assembly size               2909326                                                                                         
GAGE_Chaff bases                 0                                                                                               
GAGE_Missing reference bases     782(0.03%)                                                                                      
GAGE_Missing assembly bases      6075(0.21%)                                                                                     
GAGE_Missing assembly contigs    2(0.85%)                                                                                        
GAGE_Duplicated reference bases  100148                                                                                          
GAGE_Compressed reference bases  10179                                                                                           
GAGE_Bad trim                    103                                                                                             
GAGE_Avg idy                     99.99                                                                                           
GAGE_SNPs                        58                                                                                              
GAGE_Indels < 5bp                12                                                                                              
GAGE_Indels >= 5                 3                                                                                               
GAGE_Inversions                  8                                                                                               
GAGE_Relocation                  3                                                                                               
GAGE_Translocation               0                                                                                               
GAGE_Corrected contig #          59                                                                                              
GAGE_Corrected assembly size     2827818                                                                                         
GAGE_Min correct contig          214                                                                                             
GAGE_Max correct contig          336636                                                                                          
GAGE_Corrected N50               112883 COUNT: 6                                                                                 
