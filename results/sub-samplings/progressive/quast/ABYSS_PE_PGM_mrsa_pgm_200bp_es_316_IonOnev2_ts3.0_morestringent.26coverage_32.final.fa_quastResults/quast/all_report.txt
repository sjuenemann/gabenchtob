All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.26coverage_32.final
#Contigs                                   520                                                                                
#Contigs (>= 0 bp)                         4301                                                                               
#Contigs (>= 1000 bp)                      358                                                                                
Largest contig                             56310                                                                              
Total length                               2726682                                                                            
Total length (>= 0 bp)                     2916253                                                                            
Total length (>= 1000 bp)                  2654348                                                                            
Reference length                           2813862                                                                            
N50                                        12014                                                                              
NG50                                       11405                                                                              
N75                                        5895                                                                               
NG75                                       5382                                                                               
L50                                        71                                                                                 
LG50                                       75                                                                                 
L75                                        152                                                                                
LG75                                       163                                                                                
#local misassemblies                       2                                                                                  
#misassemblies                             1                                                                                  
#misassembled contigs                      1                                                                                  
Misassembled contigs length                12864                                                                              
Misassemblies inter-contig overlap         218                                                                                
#unaligned contigs                         0 + 0 part                                                                         
Unaligned contigs length                   0                                                                                  
#ambiguously mapped contigs                12                                                                                 
Extra bases in ambiguously mapped contigs  -6380                                                                              
#N's                                       0                                                                                  
#N's per 100 kbp                           0.00                                                                               
Genome fraction (%)                        96.526                                                                             
Duplication ratio                          1.002                                                                              
#genes                                     2380 + 240 part                                                                    
#predicted genes (unique)                  2902                                                                               
#predicted genes (>= 0 bp)                 2902                                                                               
#predicted genes (>= 300 bp)               2353                                                                               
#predicted genes (>= 1500 bp)              252                                                                                
#predicted genes (>= 3000 bp)              18                                                                                 
#mismatches                                53                                                                                 
#indels                                    271                                                                                
Indels length                              277                                                                                
#mismatches per 100 kbp                    1.95                                                                               
#indels per 100 kbp                        9.98                                                                               
GC (%)                                     32.59                                                                              
Reference GC (%)                           32.81                                                                              
Average %IDY                               99.920                                                                             
Largest alignment                          56310                                                                              
NA50                                       12014                                                                              
NGA50                                      11405                                                                              
NA75                                       5895                                                                               
NGA75                                      5382                                                                               
LA50                                       71                                                                                 
LGA50                                      75                                                                                 
LA75                                       152                                                                                
LGA75                                      163                                                                                
#Mis_misassemblies                         1                                                                                  
#Mis_relocations                           1                                                                                  
#Mis_translocations                        0                                                                                  
#Mis_inversions                            0                                                                                  
#Mis_misassembled contigs                  1                                                                                  
Mis_Misassembled contigs length            12864                                                                              
#Mis_local misassemblies                   2                                                                                  
#Mis_short indels (<= 5 bp)                271                                                                                
#Mis_long indels (> 5 bp)                  0                                                                                  
#Una_fully unaligned contigs               0                                                                                  
Una_Fully unaligned length                 0                                                                                  
#Una_partially unaligned contigs           0                                                                                  
#Una_with misassembly                      0                                                                                  
#Una_both parts are significant            0                                                                                  
Una_Partially unaligned length             0                                                                                  
GAGE_Contigs #                             520                                                                                
GAGE_Min contig                            201                                                                                
GAGE_Max contig                            56310                                                                              
GAGE_N50                                   11405 COUNT: 75                                                                    
GAGE_Genome size                           2813862                                                                            
GAGE_Assembly size                         2726682                                                                            
GAGE_Chaff bases                           0                                                                                  
GAGE_Missing reference bases               71741(2.55%)                                                                       
GAGE_Missing assembly bases                28(0.00%)                                                                          
GAGE_Missing assembly contigs              0(0.00%)                                                                           
GAGE_Duplicated reference bases            0                                                                                  
GAGE_Compressed reference bases            20364                                                                              
GAGE_Bad trim                              28                                                                                 
GAGE_Avg idy                               99.99                                                                              
GAGE_SNPs                                  62                                                                                 
GAGE_Indels < 5bp                          270                                                                                
GAGE_Indels >= 5                           2                                                                                  
GAGE_Inversions                            0                                                                                  
GAGE_Relocation                            1                                                                                  
GAGE_Translocation                         0                                                                                  
GAGE_Corrected contig #                    522                                                                                
GAGE_Corrected assembly size               2727025                                                                            
GAGE_Min correct contig                    201                                                                                
GAGE_Max correct contig                    56316                                                                              
GAGE_Corrected N50                         11406 COUNT: 75                                                                    
