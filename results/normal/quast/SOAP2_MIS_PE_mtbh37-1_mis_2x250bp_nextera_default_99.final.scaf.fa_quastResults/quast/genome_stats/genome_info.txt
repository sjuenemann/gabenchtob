reference chromosomes:
	gi_57116681_ref_NC_000962.2__Mycobacterium_tuberculosis_H37Rv_chromosome__complete_genome (4411532 bp)

total genome size: 4411532

gap min size: 50
partial gene/operon min size: 100

genes loaded: 4111


  assembly              | genome fraction (%) | duplication ratio | gaps        | genes     | partial   | operons   | partial   |
                        |                     |                   | number      |           | genes     |           | operons   |
================================================================================================================================
  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_99.final.scaf_broken  | 97.7017281072       | 1.01110728809     | 429         | 3602      | 448       | None      | None      |
  SOAP2_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default_99.final.scaf  | 97.8858591528       | 1.01576651369     | 372         | 3692      | 360       | None      | None      |
