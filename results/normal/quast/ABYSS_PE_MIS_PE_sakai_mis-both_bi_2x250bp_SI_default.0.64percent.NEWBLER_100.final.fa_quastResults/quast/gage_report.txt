All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_100.final_broken  ABYSS_PE_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER_100.final
GAGE_Contigs #                   324                                                                                        274                                                                               
GAGE_Min contig                  200                                                                                        200                                                                               
GAGE_Max contig                  362241                                                                                     362241                                                                            
GAGE_N50                         95367 COUNT: 17                                                                            142281 COUNT: 15                                                                  
GAGE_Genome size                 5594470                                                                                    5594470                                                                           
GAGE_Assembly size               5512047                                                                                    5514542                                                                           
GAGE_Chaff bases                 0                                                                                          0                                                                                 
GAGE_Missing reference bases     10672(0.19%)                                                                               10516(0.19%)                                                                      
GAGE_Missing assembly bases      14(0.00%)                                                                                  2302(0.04%)                                                                       
GAGE_Missing assembly contigs    0(0.00%)                                                                                   0(0.00%)                                                                          
GAGE_Duplicated reference bases  25018                                                                                      24495                                                                             
GAGE_Compressed reference bases  228613                                                                                     228307                                                                            
GAGE_Bad trim                    2                                                                                          552                                                                               
GAGE_Avg idy                     99.98                                                                                      99.98                                                                             
GAGE_SNPs                        464                                                                                        464                                                                               
GAGE_Indels < 5bp                29                                                                                         36                                                                                
GAGE_Indels >= 5                 13                                                                                         33                                                                                
GAGE_Inversions                  2                                                                                          9                                                                                 
GAGE_Relocation                  4                                                                                          10                                                                                
GAGE_Translocation               0                                                                                          1                                                                                 
GAGE_Corrected contig #          308                                                                                        306                                                                               
GAGE_Corrected assembly size     5496666                                                                                    5497393                                                                           
GAGE_Min correct contig          200                                                                                        200                                                                               
GAGE_Max correct contig          245378                                                                                     245378                                                                            
GAGE_Corrected N50               78945 COUNT: 21                                                                            78945 COUNT: 21                                                                   
