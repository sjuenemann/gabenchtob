All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SPADES_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent.K99.final-contigs
GAGE_Contigs #                   422                                                                         
GAGE_Min contig                  200                                                                         
GAGE_Max contig                  352169                                                                      
GAGE_N50                         142122 COUNT: 13                                                            
GAGE_Genome size                 5594470                                                                     
GAGE_Assembly size               5441188                                                                     
GAGE_Chaff bases                 0                                                                           
GAGE_Missing reference bases     7446(0.13%)                                                                 
GAGE_Missing assembly bases      34(0.00%)                                                                   
GAGE_Missing assembly contigs    0(0.00%)                                                                    
GAGE_Duplicated reference bases  67961                                                                       
GAGE_Compressed reference bases  283269                                                                      
GAGE_Bad trim                    34                                                                          
GAGE_Avg idy                     99.98                                                                       
GAGE_SNPs                        223                                                                         
GAGE_Indels < 5bp                393                                                                         
GAGE_Indels >= 5                 13                                                                          
GAGE_Inversions                  1                                                                           
GAGE_Relocation                  9                                                                           
GAGE_Translocation               0                                                                           
GAGE_Corrected contig #          440                                                                         
GAGE_Corrected assembly size     5376417                                                                     
GAGE_Min correct contig          201                                                                         
GAGE_Max correct contig          238374                                                                      
GAGE_Corrected N50               96225 COUNT: 18                                                             
