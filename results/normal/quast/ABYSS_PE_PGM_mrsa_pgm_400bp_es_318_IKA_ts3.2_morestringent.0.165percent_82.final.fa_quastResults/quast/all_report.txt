All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   ABYSS_PE_PGM_mrsa_pgm_400bp_es_318_IKA_ts3.2_morestringent.0.165percent_82.final
#Contigs                                   437                                                                             
#Contigs (>= 0 bp)                         3775                                                                            
#Contigs (>= 1000 bp)                      299                                                                             
Largest contig                             67202                                                                           
Total length                               2755366                                                                         
Total length (>= 0 bp)                     3157502                                                                         
Total length (>= 1000 bp)                  2690218                                                                         
Reference length                           2813862                                                                         
N50                                        14605                                                                           
NG50                                       14303                                                                           
N75                                        7634                                                                            
NG75                                       7229                                                                            
L50                                        58                                                                              
LG50                                       60                                                                              
L75                                        122                                                                             
LG75                                       128                                                                             
#local misassemblies                       1                                                                               
#misassemblies                             1                                                                               
#misassembled contigs                      1                                                                               
Misassembled contigs length                9089                                                                            
Misassemblies inter-contig overlap         150                                                                             
#unaligned contigs                         0 + 0 part                                                                      
Unaligned contigs length                   0                                                                               
#ambiguously mapped contigs                11                                                                              
Extra bases in ambiguously mapped contigs  -3717                                                                           
#N's                                       0                                                                               
#N's per 100 kbp                           0.00                                                                            
Genome fraction (%)                        97.630                                                                          
Duplication ratio                          1.002                                                                           
#genes                                     2437 + 242 part                                                                 
#predicted genes (unique)                  2878                                                                            
#predicted genes (>= 0 bp)                 2884                                                                            
#predicted genes (>= 300 bp)               2374                                                                            
#predicted genes (>= 1500 bp)              267                                                                             
#predicted genes (>= 3000 bp)              23                                                                              
#mismatches                                30                                                                              
#indels                                    110                                                                             
Indels length                              111                                                                             
#mismatches per 100 kbp                    1.09                                                                            
#indels per 100 kbp                        4.00                                                                            
GC (%)                                     32.60                                                                           
Reference GC (%)                           32.81                                                                           
Average %IDY                               99.259                                                                          
Largest alignment                          67202                                                                           
NA50                                       14605                                                                           
NGA50                                      14303                                                                           
NA75                                       7634                                                                            
NGA75                                      7229                                                                            
LA50                                       58                                                                              
LGA50                                      60                                                                              
LA75                                       122                                                                             
LGA75                                      128                                                                             
#Mis_misassemblies                         1                                                                               
#Mis_relocations                           1                                                                               
#Mis_translocations                        0                                                                               
#Mis_inversions                            0                                                                               
#Mis_misassembled contigs                  1                                                                               
Mis_Misassembled contigs length            9089                                                                            
#Mis_local misassemblies                   1                                                                               
#Mis_short indels (<= 5 bp)                110                                                                             
#Mis_long indels (> 5 bp)                  0                                                                               
#Una_fully unaligned contigs               0                                                                               
Una_Fully unaligned length                 0                                                                               
#Una_partially unaligned contigs           0                                                                               
#Una_with misassembly                      0                                                                               
#Una_both parts are significant            0                                                                               
Una_Partially unaligned length             0                                                                               
GAGE_Contigs #                             437                                                                             
GAGE_Min contig                            204                                                                             
GAGE_Max contig                            67202                                                                           
GAGE_N50                                   14303 COUNT: 60                                                                 
GAGE_Genome size                           2813862                                                                         
GAGE_Assembly size                         2755366                                                                         
GAGE_Chaff bases                           0                                                                               
GAGE_Missing reference bases               51865(1.84%)                                                                    
GAGE_Missing assembly bases                8(0.00%)                                                                        
GAGE_Missing assembly contigs              0(0.00%)                                                                        
GAGE_Duplicated reference bases            1034                                                                            
GAGE_Compressed reference bases            17773                                                                           
GAGE_Bad trim                              8                                                                               
GAGE_Avg idy                               99.99                                                                           
GAGE_SNPs                                  31                                                                              
GAGE_Indels < 5bp                          104                                                                             
GAGE_Indels >= 5                           1                                                                               
GAGE_Inversions                            0                                                                               
GAGE_Relocation                            1                                                                               
GAGE_Translocation                         0                                                                               
GAGE_Corrected contig #                    435                                                                             
GAGE_Corrected assembly size               2754588                                                                         
GAGE_Min correct contig                    204                                                                             
GAGE_Max correct contig                    67203                                                                           
GAGE_Corrected N50                         14303 COUNT: 60                                                                 
