All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K127.final-scaffolds_broken  SPADES_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.K127.final-scaffolds
#Una_fully unaligned contigs      97                                                                                          97                                                                                 
Una_Fully unaligned length        47170                                                                                       47170                                                                              
#Una_partially unaligned contigs  1                                                                                           1                                                                                  
#Una_with misassembly             0                                                                                           0                                                                                  
#Una_both parts are significant   0                                                                                           0                                                                                  
Una_Partially unaligned length    105                                                                                         105                                                                                
#N's                              0                                                                                           0                                                                                  
