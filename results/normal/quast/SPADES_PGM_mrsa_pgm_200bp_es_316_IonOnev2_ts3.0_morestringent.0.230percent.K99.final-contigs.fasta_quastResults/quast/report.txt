All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SPADES_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent.K99.final-contigs
#Contigs (>= 0 bp)             96                                                                                          
#Contigs (>= 1000 bp)          69                                                                                          
Total length (>= 0 bp)         2771413                                                                                     
Total length (>= 1000 bp)      2766180                                                                                     
#Contigs                       76                                                                                          
Largest contig                 241437                                                                                      
Total length                   2769055                                                                                     
Reference length               2813862                                                                                     
GC (%)                         32.64                                                                                       
Reference GC (%)               32.81                                                                                       
N50                            67980                                                                                       
NG50                           67980                                                                                       
N75                            45883                                                                                       
NG75                           44183                                                                                       
#misassemblies                 11                                                                                          
#local misassemblies           15                                                                                          
#unaligned contigs             0 + 2 part                                                                                  
Unaligned contigs length       48                                                                                          
Genome fraction (%)            98.255                                                                                      
Duplication ratio              1.002                                                                                       
#N's per 100 kbp               0.00                                                                                        
#mismatches per 100 kbp        10.42                                                                                       
#indels per 100 kbp            22.32                                                                                       
#genes                         2625 + 42 part                                                                              
#predicted genes (unique)      2748                                                                                        
#predicted genes (>= 0 bp)     2751                                                                                        
#predicted genes (>= 300 bp)   2330                                                                                        
#predicted genes (>= 1500 bp)  281                                                                                         
#predicted genes (>= 3000 bp)  26                                                                                          
Largest alignment              197804                                                                                      
NA50                           64639                                                                                       
NGA50                          64639                                                                                       
NA75                           43629                                                                                       
NGA75                          40854                                                                                       
