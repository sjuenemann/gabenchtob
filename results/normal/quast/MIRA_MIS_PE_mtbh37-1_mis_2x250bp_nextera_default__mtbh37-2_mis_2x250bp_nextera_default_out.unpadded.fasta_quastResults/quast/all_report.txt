All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default_out.unpadded
#Contigs                                   224                                                                                                
#Contigs (>= 0 bp)                         237                                                                                                
#Contigs (>= 1000 bp)                      100                                                                                                
Largest contig                             519847                                                                                             
Total length                               4457609                                                                                            
Total length (>= 0 bp)                     4459125                                                                                            
Total length (>= 1000 bp)                  4389287                                                                                            
Reference length                           4411532                                                                                            
N50                                        90078                                                                                              
NG50                                       90078                                                                                              
N75                                        53776                                                                                              
NG75                                       54398                                                                                              
L50                                        14                                                                                                 
LG50                                       14                                                                                                 
L75                                        30                                                                                                 
LG75                                       29                                                                                                 
#local misassemblies                       18                                                                                                 
#misassemblies                             40                                                                                                 
#misassembled contigs                      33                                                                                                 
Misassembled contigs length                2128492                                                                                            
Misassemblies inter-contig overlap         48468                                                                                              
#unaligned contigs                         1 + 10 part                                                                                        
Unaligned contigs length                   1230                                                                                               
#ambiguously mapped contigs                3                                                                                                  
Extra bases in ambiguously mapped contigs  -1543                                                                                              
#N's                                       207                                                                                                
#N's per 100 kbp                           4.64                                                                                               
Genome fraction (%)                        99.629                                                                                             
Duplication ratio                          1.025                                                                                              
#genes                                     4017 + 93 part                                                                                     
#predicted genes (unique)                  4205                                                                                               
#predicted genes (>= 0 bp)                 4232                                                                                               
#predicted genes (>= 300 bp)               3662                                                                                               
#predicted genes (>= 1500 bp)              567                                                                                                
#predicted genes (>= 3000 bp)              78                                                                                                 
#mismatches                                519                                                                                                
#indels                                    96                                                                                                 
Indels length                              289                                                                                                
#mismatches per 100 kbp                    11.81                                                                                              
#indels per 100 kbp                        2.18                                                                                               
GC (%)                                     65.63                                                                                              
Reference GC (%)                           65.61                                                                                              
Average %IDY                               99.693                                                                                             
Largest alignment                          326625                                                                                             
NA50                                       67461                                                                                              
NGA50                                      67461                                                                                              
NA75                                       33325                                                                                              
NGA75                                      36210                                                                                              
LA50                                       19                                                                                                 
LGA50                                      19                                                                                                 
LA75                                       41                                                                                                 
LGA75                                      40                                                                                                 
#Mis_misassemblies                         40                                                                                                 
#Mis_relocations                           37                                                                                                 
#Mis_translocations                        0                                                                                                  
#Mis_inversions                            3                                                                                                  
#Mis_misassembled contigs                  33                                                                                                 
Mis_Misassembled contigs length            2128492                                                                                            
#Mis_local misassemblies                   18                                                                                                 
#Mis_short indels (<= 5 bp)                91                                                                                                 
#Mis_long indels (> 5 bp)                  5                                                                                                  
#Una_fully unaligned contigs               1                                                                                                  
Una_Fully unaligned length                 242                                                                                                
#Una_partially unaligned contigs           10                                                                                                 
#Una_with misassembly                      0                                                                                                  
#Una_both parts are significant            1                                                                                                  
Una_Partially unaligned length             988                                                                                                
GAGE_Contigs #                             224                                                                                                
GAGE_Min contig                            206                                                                                                
GAGE_Max contig                            519847                                                                                             
GAGE_N50                                   90078 COUNT: 14                                                                                    
GAGE_Genome size                           4411532                                                                                            
GAGE_Assembly size                         4457609                                                                                            
GAGE_Chaff bases                           0                                                                                                  
GAGE_Missing reference bases               11578(0.26%)                                                                                       
GAGE_Missing assembly bases                2832(0.06%)                                                                                        
GAGE_Missing assembly contigs              1(0.45%)                                                                                           
GAGE_Duplicated reference bases            60141                                                                                              
GAGE_Compressed reference bases            14108                                                                                              
GAGE_Bad trim                              2590                                                                                               
GAGE_Avg idy                               99.98                                                                                              
GAGE_SNPs                                  215                                                                                                
GAGE_Indels < 5bp                          34                                                                                                 
GAGE_Indels >= 5                           11                                                                                                 
GAGE_Inversions                            14                                                                                                 
GAGE_Relocation                            9                                                                                                  
GAGE_Translocation                         0                                                                                                  
GAGE_Corrected contig #                    173                                                                                                
GAGE_Corrected assembly size               4426970                                                                                            
GAGE_Min correct contig                    212                                                                                                
GAGE_Max correct contig                    230879                                                                                             
GAGE_Corrected N50                         66148 COUNT: 21                                                                                    
