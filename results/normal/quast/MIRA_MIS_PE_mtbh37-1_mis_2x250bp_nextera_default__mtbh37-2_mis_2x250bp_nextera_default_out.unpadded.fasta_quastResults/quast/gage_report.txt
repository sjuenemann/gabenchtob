All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_MIS_PE_mtbh37-1_mis_2x250bp_nextera_default__mtbh37-2_mis_2x250bp_nextera_default_out.unpadded
GAGE_Contigs #                   224                                                                                                
GAGE_Min contig                  206                                                                                                
GAGE_Max contig                  519847                                                                                             
GAGE_N50                         90078 COUNT: 14                                                                                    
GAGE_Genome size                 4411532                                                                                            
GAGE_Assembly size               4457609                                                                                            
GAGE_Chaff bases                 0                                                                                                  
GAGE_Missing reference bases     11578(0.26%)                                                                                       
GAGE_Missing assembly bases      2832(0.06%)                                                                                        
GAGE_Missing assembly contigs    1(0.45%)                                                                                           
GAGE_Duplicated reference bases  60141                                                                                              
GAGE_Compressed reference bases  14108                                                                                              
GAGE_Bad trim                    2590                                                                                               
GAGE_Avg idy                     99.98                                                                                              
GAGE_SNPs                        215                                                                                                
GAGE_Indels < 5bp                34                                                                                                 
GAGE_Indels >= 5                 11                                                                                                 
GAGE_Inversions                  14                                                                                                 
GAGE_Relocation                  9                                                                                                  
GAGE_Translocation               0                                                                                                  
GAGE_Corrected contig #          173                                                                                                
GAGE_Corrected assembly size     4426970                                                                                            
GAGE_Min correct contig          212                                                                                                
GAGE_Max correct contig          230879                                                                                             
GAGE_Corrected N50               66148 COUNT: 21                                                                                    
