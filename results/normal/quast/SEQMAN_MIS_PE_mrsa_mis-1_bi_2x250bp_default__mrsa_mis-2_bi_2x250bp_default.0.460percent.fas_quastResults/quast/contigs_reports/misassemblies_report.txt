All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SEQMAN_MIS_PE_mrsa_mis-1_bi_2x250bp_default__mrsa_mis-2_bi_2x250bp_default.0.460percent
#Mis_misassemblies               3                                                                                      
#Mis_relocations                 3                                                                                      
#Mis_translocations              0                                                                                      
#Mis_inversions                  0                                                                                      
#Mis_misassembled contigs        3                                                                                      
Mis_Misassembled contigs length  231887                                                                                 
#Mis_local misassemblies         1                                                                                      
#mismatches                      95                                                                                     
#indels                          13                                                                                     
#Mis_short indels (<= 5 bp)      12                                                                                     
#Mis_long indels (> 5 bp)        1                                                                                      
Indels length                    29                                                                                     
