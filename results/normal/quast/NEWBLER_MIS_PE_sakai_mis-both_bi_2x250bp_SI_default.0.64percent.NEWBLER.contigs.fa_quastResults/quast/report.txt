All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER.contigs_broken  NEWBLER_MIS_PE_sakai_mis-both_bi_2x250bp_SI_default.0.64percent.NEWBLER.contigs
#Contigs (>= 0 bp)             161                                                                                     94                                                                             
#Contigs (>= 1000 bp)          140                                                                                     94                                                                             
Total length (>= 0 bp)         5265858                                                                                 5307060                                                                        
Total length (>= 1000 bp)      5249642                                                                                 5307060                                                                        
#Contigs                       161                                                                                     94                                                                             
Largest contig                 374758                                                                                  374758                                                                         
Total length                   5265858                                                                                 5307060                                                                        
Reference length               5594470                                                                                 5594470                                                                        
GC (%)                         50.25                                                                                   50.25                                                                          
Reference GC (%)               50.48                                                                                   50.48                                                                          
N50                            133090                                                                                  149161                                                                         
NG50                           124153                                                                                  148121                                                                         
N75                            61099                                                                                   76604                                                                          
NG75                           44606                                                                                   73386                                                                          
#misassemblies                 2                                                                                       8                                                                              
#local misassemblies           3                                                                                       63                                                                             
#unaligned contigs             1 + 0 part                                                                              1 + 0 part                                                                     
Unaligned contigs length       5386                                                                                    5386                                                                           
Genome fraction (%)            93.125                                                                                  93.674                                                                         
Duplication ratio              1.000                                                                                   1.008                                                                          
#N's per 100 kbp               0.00                                                                                    776.36                                                                         
#mismatches per 100 kbp        0.88                                                                                    2.02                                                                           
#indels per 100 kbp            0.25                                                                                    0.53                                                                           
#genes                         4926 + 114 part                                                                         4945 + 125 part                                                                
#predicted genes (unique)      5087                                                                                    5068                                                                           
#predicted genes (>= 0 bp)     5087                                                                                    5068                                                                           
#predicted genes (>= 300 bp)   4356                                                                                    4361                                                                           
#predicted genes (>= 1500 bp)  665                                                                                     664                                                                            
#predicted genes (>= 3000 bp)  67                                                                                      67                                                                             
Largest alignment              260358                                                                                  362278                                                                         
NA50                           133090                                                                                  148121                                                                         
NGA50                          124153                                                                                  147875                                                                         
NA75                           61099                                                                                   75038                                                                          
NGA75                          44606                                                                                   66121                                                                          
