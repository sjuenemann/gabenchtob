All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SEQMAN_MIS_PE_mrsa_mis-1_bi_2x150bp_default__mrsa_mis-2_bi_2x150bp_default.0.527percent
GAGE_Contigs #                   296                                                                                    
GAGE_Min contig                  335                                                                                    
GAGE_Max contig                  273342                                                                                 
GAGE_N50                         24992 COUNT: 19                                                                        
GAGE_Genome size                 2813862                                                                                
GAGE_Assembly size               2734784                                                                                
GAGE_Chaff bases                 0                                                                                      
GAGE_Missing reference bases     98776(3.51%)                                                                           
GAGE_Missing assembly bases      5811(0.21%)                                                                            
GAGE_Missing assembly contigs    1(0.34%)                                                                               
GAGE_Duplicated reference bases  4768                                                                                   
GAGE_Compressed reference bases  16580                                                                                  
GAGE_Bad trim                    22                                                                                     
GAGE_Avg idy                     99.98                                                                                  
GAGE_SNPs                        44                                                                                     
GAGE_Indels < 5bp                9                                                                                      
GAGE_Indels >= 5                 5                                                                                      
GAGE_Inversions                  2                                                                                      
GAGE_Relocation                  2                                                                                      
GAGE_Translocation               0                                                                                      
GAGE_Corrected contig #          302                                                                                    
GAGE_Corrected assembly size     2727581                                                                                
GAGE_Min correct contig          335                                                                                    
GAGE_Max correct contig          273342                                                                                 
GAGE_Corrected N50               24992 COUNT: 19                                                                        
