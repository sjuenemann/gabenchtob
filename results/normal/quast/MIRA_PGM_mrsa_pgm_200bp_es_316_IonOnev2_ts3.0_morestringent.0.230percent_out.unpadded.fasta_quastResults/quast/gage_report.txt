All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent_out.unpadded
GAGE_Contigs #                   307                                                                                  
GAGE_Min contig                  204                                                                                  
GAGE_Max contig                  367176                                                                               
GAGE_N50                         83333 COUNT: 10                                                                      
GAGE_Genome size                 2813862                                                                              
GAGE_Assembly size               2896037                                                                              
GAGE_Chaff bases                 0                                                                                    
GAGE_Missing reference bases     1388(0.05%)                                                                          
GAGE_Missing assembly bases      6840(0.24%)                                                                          
GAGE_Missing assembly contigs    6(1.95%)                                                                             
GAGE_Duplicated reference bases  78747                                                                                
GAGE_Compressed reference bases  16422                                                                                
GAGE_Bad trim                    4540                                                                                 
GAGE_Avg idy                     99.98                                                                                
GAGE_SNPs                        52                                                                                   
GAGE_Indels < 5bp                177                                                                                  
GAGE_Indels >= 5                 10                                                                                   
GAGE_Inversions                  8                                                                                    
GAGE_Relocation                  1                                                                                    
GAGE_Translocation               0                                                                                    
GAGE_Corrected contig #          127                                                                                  
GAGE_Corrected assembly size     2826960                                                                              
GAGE_Min correct contig          204                                                                                  
GAGE_Max correct contig          221531                                                                               
GAGE_Corrected N50               62586 COUNT: 15                                                                      
