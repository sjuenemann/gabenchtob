All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         MIRA_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent_out.unpadded
GAGE_Contigs #                   706                                                                     
GAGE_Min contig                  202                                                                     
GAGE_Max contig                  198585                                                                  
GAGE_N50                         75460 COUNT: 19                                                         
GAGE_Genome size                 4411532                                                                 
GAGE_Assembly size               4628712                                                                 
GAGE_Chaff bases                 0                                                                       
GAGE_Missing reference bases     56676(1.28%)                                                            
GAGE_Missing assembly bases      2314(0.05%)                                                             
GAGE_Missing assembly contigs    1(0.14%)                                                                
GAGE_Duplicated reference bases  274893                                                                  
GAGE_Compressed reference bases  23296                                                                   
GAGE_Bad trim                    2024                                                                    
GAGE_Avg idy                     99.97                                                                   
GAGE_SNPs                        134                                                                     
GAGE_Indels < 5bp                471                                                                     
GAGE_Indels >= 5                 16                                                                      
GAGE_Inversions                  10                                                                      
GAGE_Relocation                  17                                                                      
GAGE_Translocation               0                                                                       
GAGE_Corrected contig #          209                                                                     
GAGE_Corrected assembly size     4378115                                                                 
GAGE_Min correct contig          207                                                                     
GAGE_Max correct contig          179665                                                                  
GAGE_Corrected N50               53793 COUNT: 26                                                         
