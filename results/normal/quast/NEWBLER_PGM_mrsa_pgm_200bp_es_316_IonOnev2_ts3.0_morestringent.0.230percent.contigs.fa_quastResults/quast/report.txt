All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent.contigs
#Contigs (>= 0 bp)             149                                                                                
#Contigs (>= 1000 bp)          76                                                                                 
Total length (>= 0 bp)         2771338                                                                            
Total length (>= 1000 bp)      2755759                                                                            
#Contigs                       101                                                                                
Largest contig                 150312                                                                             
Total length                   2765004                                                                            
Reference length               2813862                                                                            
GC (%)                         32.64                                                                              
Reference GC (%)               32.81                                                                              
N50                            58281                                                                              
NG50                           57531                                                                              
N75                            37889                                                                              
NG75                           37380                                                                              
#misassemblies                 2                                                                                  
#local misassemblies           6                                                                                  
#unaligned contigs             0 + 0 part                                                                         
Unaligned contigs length       0                                                                                  
Genome fraction (%)            97.957                                                                             
Duplication ratio              1.000                                                                              
#N's per 100 kbp               0.00                                                                               
#mismatches per 100 kbp        2.47                                                                               
#indels per 100 kbp            8.53                                                                               
#genes                         2646 + 48 part                                                                     
#predicted genes (unique)      2711                                                                               
#predicted genes (>= 0 bp)     2711                                                                               
#predicted genes (>= 300 bp)   2327                                                                               
#predicted genes (>= 1500 bp)  287                                                                                
#predicted genes (>= 3000 bp)  27                                                                                 
Largest alignment              150312                                                                             
NA50                           57531                                                                              
NGA50                          57424                                                                              
NA75                           37380                                                                              
NGA75                          34863                                                                              
