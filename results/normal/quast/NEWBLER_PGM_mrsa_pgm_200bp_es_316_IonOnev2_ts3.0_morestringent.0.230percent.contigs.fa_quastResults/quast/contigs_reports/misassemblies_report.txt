All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         NEWBLER_PGM_mrsa_pgm_200bp_es_316_IonOnev2_ts3.0_morestringent.0.230percent.contigs
#Mis_misassemblies               2                                                                                  
#Mis_relocations                 2                                                                                  
#Mis_translocations              0                                                                                  
#Mis_inversions                  0                                                                                  
#Mis_misassembled contigs        2                                                                                  
Mis_Misassembled contigs length  121854                                                                             
#Mis_local misassemblies         6                                                                                  
#mismatches                      68                                                                                 
#indels                          235                                                                                
#Mis_short indels (<= 5 bp)      233                                                                                
#Mis_long indels (> 5 bp)        2                                                                                  
Indels length                    386                                                                                
