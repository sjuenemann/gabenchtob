All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                         SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_67.final.scaf_broken  SOAP2_MIS_PE_sakai_mis-1_bi_2x150bp_default.0.826percent_67.final.scaf
GAGE_Contigs #                   725                                                                            343                                                                   
GAGE_Min contig                  200                                                                            200                                                                   
GAGE_Max contig                  130053                                                                         374525                                                                
GAGE_N50                         27309 COUNT: 56                                                                148737 COUNT: 11                                                      
GAGE_Genome size                 5594470                                                                        5594470                                                               
GAGE_Assembly size               5290189                                                                        5355925                                                               
GAGE_Chaff bases                 0                                                                              0                                                                     
GAGE_Missing reference bases     158355(2.83%)                                                                  111051(1.99%)                                                         
GAGE_Missing assembly bases      5535(0.10%)                                                                    41484(0.77%)                                                          
GAGE_Missing assembly contigs    1(0.14%)                                                                       2(0.58%)                                                              
GAGE_Duplicated reference bases  2367                                                                           5468                                                                  
GAGE_Compressed reference bases  182936                                                                         214333                                                                
GAGE_Bad trim                    77                                                                             5369                                                                  
GAGE_Avg idy                     100.00                                                                         99.97                                                                 
GAGE_SNPs                        65                                                                             70                                                                    
GAGE_Indels < 5bp                23                                                                             103                                                                   
GAGE_Indels >= 5                 3                                                                              382                                                                   
GAGE_Inversions                  0                                                                              3                                                                     
GAGE_Relocation                  4                                                                              29                                                                    
GAGE_Translocation               0                                                                              0                                                                     
GAGE_Corrected contig #          720                                                                            688                                                                   
GAGE_Corrected assembly size     5282102                                                                        5281280                                                               
GAGE_Min correct contig          200                                                                            200                                                                   
GAGE_Max correct contig          130053                                                                         131212                                                                
GAGE_Corrected N50               27309 COUNT: 56                                                                28336 COUNT: 54                                                       
