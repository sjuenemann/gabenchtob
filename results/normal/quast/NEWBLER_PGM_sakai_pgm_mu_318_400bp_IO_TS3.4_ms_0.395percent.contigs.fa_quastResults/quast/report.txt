All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent.contigs
#Contigs (>= 0 bp)             477                                                                
#Contigs (>= 1000 bp)          144                                                                
Total length (>= 0 bp)         5380575                                                            
Total length (>= 1000 bp)      5288640                                                            
#Contigs                       291                                                                
Largest contig                 327102                                                             
Total length                   5353951                                                            
Reference length               5594470                                                            
GC (%)                         50.29                                                              
Reference GC (%)               50.48                                                              
N50                            103792                                                             
NG50                           99569                                                              
N75                            53547                                                              
NG75                           45427                                                              
#misassemblies                 4                                                                  
#local misassemblies           6                                                                  
#unaligned contigs             0 + 0 part                                                         
Unaligned contigs length       0                                                                  
Genome fraction (%)            94.093                                                             
Duplication ratio              1.001                                                              
#N's per 100 kbp               0.00                                                               
#mismatches per 100 kbp        1.63                                                               
#indels per 100 kbp            3.86                                                               
#genes                         4974 + 137 part                                                    
#predicted genes (unique)      5353                                                               
#predicted genes (>= 0 bp)     5353                                                               
#predicted genes (>= 300 bp)   4490                                                               
#predicted genes (>= 1500 bp)  657                                                                
#predicted genes (>= 3000 bp)  65                                                                 
Largest alignment              327102                                                             
NA50                           103792                                                             
NGA50                          99569                                                              
NA75                           53547                                                              
NGA75                          45427                                                              
