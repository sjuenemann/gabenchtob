All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   CELERA_PGM_sakai_pgm_mu_318_400bp_IO_TS3.4_ms_0.395percent.scf
#Contigs                                   241                                                           
#Contigs (>= 0 bp)                         241                                                           
#Contigs (>= 1000 bp)                      241                                                           
Largest contig                             258747                                                        
Total length                               5248056                                                       
Total length (>= 0 bp)                     5248056                                                       
Total length (>= 1000 bp)                  5248056                                                       
Reference length                           5594470                                                       
N50                                        57926                                                         
NG50                                       52182                                                         
N75                                        30759                                                         
NG75                                       27154                                                         
L50                                        26                                                            
LG50                                       29                                                            
L75                                        57                                                            
LG75                                       67                                                            
#local misassemblies                       2                                                             
#misassemblies                             1                                                             
#misassembled contigs                      1                                                             
Misassembled contigs length                15885                                                         
Misassemblies inter-contig overlap         225                                                           
#unaligned contigs                         0 + 0 part                                                    
Unaligned contigs length                   0                                                             
#ambiguously mapped contigs                13                                                            
Extra bases in ambiguously mapped contigs  -15527                                                        
#N's                                       0                                                             
#N's per 100 kbp                           0.00                                                          
Genome fraction (%)                        92.873                                                        
Duplication ratio                          1.007                                                         
#genes                                     4897 + 185 part                                               
#predicted genes (unique)                  5265                                                          
#predicted genes (>= 0 bp)                 5267                                                          
#predicted genes (>= 300 bp)               4443                                                          
#predicted genes (>= 1500 bp)              632                                                           
#predicted genes (>= 3000 bp)              62                                                            
#mismatches                                112                                                           
#indels                                    405                                                           
Indels length                              410                                                           
#mismatches per 100 kbp                    2.16                                                          
#indels per 100 kbp                        7.79                                                          
GC (%)                                     50.30                                                         
Reference GC (%)                           50.48                                                         
Average %IDY                               98.651                                                        
Largest alignment                          258747                                                        
NA50                                       57926                                                         
NGA50                                      52182                                                         
NA75                                       30756                                                         
NGA75                                      27154                                                         
LA50                                       26                                                            
LGA50                                      29                                                            
LA75                                       57                                                            
LGA75                                      67                                                            
#Mis_misassemblies                         1                                                             
#Mis_relocations                           1                                                             
#Mis_translocations                        0                                                             
#Mis_inversions                            0                                                             
#Mis_misassembled contigs                  1                                                             
Mis_Misassembled contigs length            15885                                                         
#Mis_local misassemblies                   2                                                             
#Mis_short indels (<= 5 bp)                405                                                           
#Mis_long indels (> 5 bp)                  0                                                             
#Una_fully unaligned contigs               0                                                             
Una_Fully unaligned length                 0                                                             
#Una_partially unaligned contigs           0                                                             
#Una_with misassembly                      0                                                             
#Una_both parts are significant            0                                                             
Una_Partially unaligned length             0                                                             
GAGE_Contigs #                             241                                                           
GAGE_Min contig                            1001                                                          
GAGE_Max contig                            258747                                                        
GAGE_N50                                   52182 COUNT: 29                                               
GAGE_Genome size                           5594470                                                       
GAGE_Assembly size                         5248056                                                       
GAGE_Chaff bases                           0                                                             
GAGE_Missing reference bases               297113(5.31%)                                                 
GAGE_Missing assembly bases                54(0.00%)                                                     
GAGE_Missing assembly contigs              0(0.00%)                                                      
GAGE_Duplicated reference bases            2348                                                          
GAGE_Compressed reference bases            134881                                                        
GAGE_Bad trim                              54                                                            
GAGE_Avg idy                               99.99                                                         
GAGE_SNPs                                  92                                                            
GAGE_Indels < 5bp                          383                                                           
GAGE_Indels >= 5                           3                                                             
GAGE_Inversions                            0                                                             
GAGE_Relocation                            2                                                             
GAGE_Translocation                         0                                                             
GAGE_Corrected contig #                    245                                                           
GAGE_Corrected assembly size               5247079                                                       
GAGE_Min correct contig                    573                                                           
GAGE_Max correct contig                    258745                                                        
GAGE_Corrected N50                         51966 COUNT: 30                                               
