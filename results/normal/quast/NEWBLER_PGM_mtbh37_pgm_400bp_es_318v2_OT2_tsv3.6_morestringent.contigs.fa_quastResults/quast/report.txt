All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       NEWBLER_PGM_mtbh37_pgm_400bp_es_318v2_OT2_tsv3.6_morestringent.contigs
#Contigs (>= 0 bp)             252                                                                   
#Contigs (>= 1000 bp)          143                                                                   
Total length (>= 0 bp)         4307450                                                               
Total length (>= 1000 bp)      4269871                                                               
#Contigs                       209                                                                   
Largest contig                 182749                                                                
Total length                   4301249                                                               
Reference length               4411532                                                               
GC (%)                         65.41                                                                 
Reference GC (%)               65.61                                                                 
N50                            62682                                                                 
NG50                           60624                                                                 
N75                            34939                                                                 
NG75                           33589                                                                 
#misassemblies                 0                                                                     
#local misassemblies           8                                                                     
#unaligned contigs             2 + 3 part                                                            
Unaligned contigs length       1955                                                                  
Genome fraction (%)            97.000                                                                
Duplication ratio              1.005                                                                 
#N's per 100 kbp               0.00                                                                  
#mismatches per 100 kbp        3.48                                                                  
#indels per 100 kbp            16.94                                                                 
#genes                         3939 + 112 part                                                       
#predicted genes (unique)      4348                                                                  
#predicted genes (>= 0 bp)     4348                                                                  
#predicted genes (>= 300 bp)   3692                                                                  
#predicted genes (>= 1500 bp)  510                                                                   
#predicted genes (>= 3000 bp)  58                                                                    
Largest alignment              182749                                                                
NA50                           62682                                                                 
NGA50                          60624                                                                 
NA75                           34939                                                                 
NGA75                          33589                                                                 
