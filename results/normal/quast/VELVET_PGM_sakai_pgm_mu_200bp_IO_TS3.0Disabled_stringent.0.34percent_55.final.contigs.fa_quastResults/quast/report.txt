All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       VELVET_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent_55.final.contigs
#Contigs (>= 0 bp)             1778                                                                                 
#Contigs (>= 1000 bp)          767                                                                                  
Total length (>= 0 bp)         5372329                                                                              
Total length (>= 1000 bp)      5054534                                                                              
#Contigs                       1344                                                                                 
Largest contig                 40538                                                                                
Total length                   5306998                                                                              
Reference length               5594470                                                                              
GC (%)                         50.26                                                                                
Reference GC (%)               50.48                                                                                
N50                            9905                                                                                 
NG50                           9254                                                                                 
N75                            5179                                                                                 
NG75                           4363                                                                                 
#misassemblies                 11                                                                                   
#local misassemblies           19                                                                                   
#unaligned contigs             1 + 8 part                                                                           
Unaligned contigs length       1697                                                                                 
Genome fraction (%)            93.124                                                                               
Duplication ratio              1.004                                                                                
#N's per 100 kbp               7.16                                                                                 
#mismatches per 100 kbp        3.61                                                                                 
#indels per 100 kbp            53.38                                                                                
#genes                         4322 + 722 part                                                                      
#predicted genes (unique)      6704                                                                                 
#predicted genes (>= 0 bp)     6707                                                                                 
#predicted genes (>= 300 bp)   4993                                                                                 
#predicted genes (>= 1500 bp)  430                                                                                  
#predicted genes (>= 3000 bp)  25                                                                                   
Largest alignment              40538                                                                                
NA50                           9846                                                                                 
NGA50                          9206                                                                                 
NA75                           5159                                                                                 
NGA75                          4356                                                                                 
