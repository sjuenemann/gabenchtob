All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       SEQMAN_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.0.826percent
#Contigs (>= 0 bp)             201                                                                                      
#Contigs (>= 1000 bp)          140                                                                                      
Total length (>= 0 bp)         5410882                                                                                  
Total length (>= 1000 bp)      5375764                                                                                  
#Contigs                       201                                                                                      
Largest contig                 374953                                                                                   
Total length                   5410882                                                                                  
Reference length               5594470                                                                                  
GC (%)                         50.32                                                                                    
Reference GC (%)               50.48                                                                                    
N50                            151188                                                                                   
NG50                           148471                                                                                   
N75                            72897                                                                                    
NG75                           46914                                                                                    
#misassemblies                 29                                                                                       
#local misassemblies           10                                                                                       
#unaligned contigs             1 + 2 part                                                                               
Unaligned contigs length       6795                                                                                     
Genome fraction (%)            96.255                                                                                   
Duplication ratio              1.005                                                                                    
#N's per 100 kbp               0.54                                                                                     
#mismatches per 100 kbp        31.35                                                                                    
#indels per 100 kbp            1.47                                                                                     
#genes                         5107 + 165 part                                                                          
#predicted genes (unique)      5283                                                                                     
#predicted genes (>= 0 bp)     5294                                                                                     
#predicted genes (>= 300 bp)   4492                                                                                     
#predicted genes (>= 1500 bp)  676                                                                                      
#predicted genes (>= 3000 bp)  68                                                                                       
Largest alignment              368909                                                                                   
NA50                           148018                                                                                   
NGA50                          145779                                                                                   
NA75                           66571                                                                                    
NGA75                          45237                                                                                    
