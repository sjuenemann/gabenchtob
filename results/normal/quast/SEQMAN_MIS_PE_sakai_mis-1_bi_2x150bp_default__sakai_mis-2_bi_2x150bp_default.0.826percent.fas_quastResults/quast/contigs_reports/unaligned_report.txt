All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                          SEQMAN_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.0.826percent
#Una_fully unaligned contigs      1                                                                                        
Una_Fully unaligned length        5820                                                                                     
#Una_partially unaligned contigs  2                                                                                        
#Una_with misassembly             0                                                                                        
#Una_both parts are significant   1                                                                                        
Una_Partially unaligned length    975                                                                                      
#N's                              29                                                                                       
