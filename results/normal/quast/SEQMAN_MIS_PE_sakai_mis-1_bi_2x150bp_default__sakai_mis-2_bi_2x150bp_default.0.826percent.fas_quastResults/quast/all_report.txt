All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                                   SEQMAN_MIS_PE_sakai_mis-1_bi_2x150bp_default__sakai_mis-2_bi_2x150bp_default.0.826percent
#Contigs                                   201                                                                                      
#Contigs (>= 0 bp)                         201                                                                                      
#Contigs (>= 1000 bp)                      140                                                                                      
Largest contig                             374953                                                                                   
Total length                               5410882                                                                                  
Total length (>= 0 bp)                     5410882                                                                                  
Total length (>= 1000 bp)                  5375764                                                                                  
Reference length                           5594470                                                                                  
N50                                        151188                                                                                   
NG50                                       148471                                                                                   
N75                                        72897                                                                                    
NG75                                       46914                                                                                    
L50                                        10                                                                                       
LG50                                       11                                                                                       
L75                                        23                                                                                       
LG75                                       26                                                                                       
#local misassemblies                       10                                                                                       
#misassemblies                             29                                                                                       
#misassembled contigs                      25                                                                                       
Misassembled contigs length                1362283                                                                                  
Misassemblies inter-contig overlap         22717                                                                                    
#unaligned contigs                         1 + 2 part                                                                               
Unaligned contigs length                   6795                                                                                     
#ambiguously mapped contigs                21                                                                                       
Extra bases in ambiguously mapped contigs  -14897                                                                                   
#N's                                       29                                                                                       
#N's per 100 kbp                           0.54                                                                                     
Genome fraction (%)                        96.255                                                                                   
Duplication ratio                          1.005                                                                                    
#genes                                     5107 + 165 part                                                                          
#predicted genes (unique)                  5283                                                                                     
#predicted genes (>= 0 bp)                 5294                                                                                     
#predicted genes (>= 300 bp)               4492                                                                                     
#predicted genes (>= 1500 bp)              676                                                                                      
#predicted genes (>= 3000 bp)              68                                                                                       
#mismatches                                1688                                                                                     
#indels                                    79                                                                                       
Indels length                              90                                                                                       
#mismatches per 100 kbp                    31.35                                                                                    
#indels per 100 kbp                        1.47                                                                                     
GC (%)                                     50.32                                                                                    
Reference GC (%)                           50.48                                                                                    
Average %IDY                               98.488                                                                                   
Largest alignment                          368909                                                                                   
NA50                                       148018                                                                                   
NGA50                                      145779                                                                                   
NA75                                       66571                                                                                    
NGA75                                      45237                                                                                    
LA50                                       12                                                                                       
LGA50                                      13                                                                                       
LA75                                       26                                                                                       
LGA75                                      29                                                                                       
#Mis_misassemblies                         29                                                                                       
#Mis_relocations                           28                                                                                       
#Mis_translocations                        1                                                                                        
#Mis_inversions                            0                                                                                        
#Mis_misassembled contigs                  25                                                                                       
Mis_Misassembled contigs length            1362283                                                                                  
#Mis_local misassemblies                   10                                                                                       
#Mis_short indels (<= 5 bp)                78                                                                                       
#Mis_long indels (> 5 bp)                  1                                                                                        
#Una_fully unaligned contigs               1                                                                                        
Una_Fully unaligned length                 5820                                                                                     
#Una_partially unaligned contigs           2                                                                                        
#Una_with misassembly                      0                                                                                        
#Una_both parts are significant            1                                                                                        
Una_Partially unaligned length             975                                                                                      
GAGE_Contigs #                             201                                                                                      
GAGE_Min contig                            236                                                                                      
GAGE_Max contig                            374953                                                                                   
GAGE_N50                                   148471 COUNT: 11                                                                         
GAGE_Genome size                           5594470                                                                                  
GAGE_Assembly size                         5410882                                                                                  
GAGE_Chaff bases                           0                                                                                        
GAGE_Missing reference bases               43153(0.77%)                                                                             
GAGE_Missing assembly bases                6446(0.12%)                                                                              
GAGE_Missing assembly contigs              1(0.50%)                                                                                 
GAGE_Duplicated reference bases            29592                                                                                    
GAGE_Compressed reference bases            244748                                                                                   
GAGE_Bad trim                              626                                                                                      
GAGE_Avg idy                               99.95                                                                                    
GAGE_SNPs                                  900                                                                                      
GAGE_Indels < 5bp                          43                                                                                       
GAGE_Indels >= 5                           9                                                                                        
GAGE_Inversions                            9                                                                                        
GAGE_Relocation                            10                                                                                       
GAGE_Translocation                         1                                                                                        
GAGE_Corrected contig #                    199                                                                                      
GAGE_Corrected assembly size               5404776                                                                                  
GAGE_Min correct contig                    292                                                                                      
GAGE_Max correct contig                    368909                                                                                   
GAGE_Corrected N50                         124436 COUNT: 14                                                                         
