All statistics are based on contigs of size >= 200 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                       CLC_PGM_sakai_pgm_mu_200bp_IO_TS3.0Disabled_stringent.0.34percent.result
#Contigs (>= 0 bp)             779                                                                     
#Contigs (>= 1000 bp)          333                                                                     
Total length (>= 0 bp)         5361632                                                                 
Total length (>= 1000 bp)      5211533                                                                 
#Contigs                       779                                                                     
Largest contig                 162751                                                                  
Total length                   5361632                                                                 
Reference length               5594470                                                                 
GC (%)                         50.27                                                                   
Reference GC (%)               50.48                                                                   
N50                            31341                                                                   
NG50                           30424                                                                   
N75                            15536                                                                   
NG75                           12529                                                                   
#misassemblies                 9                                                                       
#local misassemblies           3                                                                       
#unaligned contigs             0 + 24 part                                                             
Unaligned contigs length       923                                                                     
Genome fraction (%)            93.692                                                                  
Duplication ratio              1.006                                                                   
#N's per 100 kbp               0.84                                                                    
#mismatches per 100 kbp        1.62                                                                    
#indels per 100 kbp            30.56                                                                   
#genes                         4780 + 311 part                                                         
#predicted genes (unique)      6201                                                                    
#predicted genes (>= 0 bp)     6206                                                                    
#predicted genes (>= 300 bp)   4796                                                                    
#predicted genes (>= 1500 bp)  541                                                                     
#predicted genes (>= 3000 bp)  34                                                                      
Largest alignment              162751                                                                  
NA50                           30816                                                                   
NGA50                          28783                                                                   
NA75                           15116                                                                   
NGA75                          12529                                                                   
